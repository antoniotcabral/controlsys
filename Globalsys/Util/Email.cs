﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Globalsys.Exceptions;

namespace Globalsys.Util
{
    public static class Email
    {
        public static void Enviar(string assunto, string corpo, params string[] para)
        {
            Enviar(assunto, null, null, corpo, null, para);
        }

        public static void Enviar(string assunto, string localTemplate, Dictionary<string, string> valoresSubst, string corpo, Attachment[] atalhos, params string[] para)
        {
            string readFile = null;

            if (!string.IsNullOrEmpty(localTemplate))
            {
                StreamReader reader = new StreamReader(localTemplate);
                readFile = reader.ReadToEnd();
            }

            if (valoresSubst != null)
            {
                foreach (var item in valoresSubst)
                    readFile = readFile.Replace(string.Format("{0}", item.Key), item.Value);
            }

            readFile += string.Format("<br /> {0}", corpo);

            MailMessage mail = new MailMessage();

            AlternateView htmlView = AlternateView.CreateAlternateViewFromString(readFile, null, "text/html");
            /*LinkedResource imagelink = new LinkedResource("~/includes/images/logoVix.png", "image/jpeg");
              imagelink.ContentId = "imageId";
              imagelink.TransferEncoding = System.Net.Mime.TransferEncoding.Base64;
              htmlView.LinkedResources.Add(imagelink);*/

            mail.AlternateViews.Add(htmlView);

            for (int i = 0; i < para.Length; i++)
            {
                if (para[i] == null)
                {
                    throw new CoreException("O destinatário não possui endereço de email.");
                }

                mail.To.Add(para[i]);
            }


            if (atalhos != null)
                foreach (Attachment atalho in atalhos)
                    mail.Attachments.Add(atalho);
            try
            {
                mail.Subject = assunto;
                mail.Body = corpo;
                mail.Priority = MailPriority.High;
                mail.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Send(mail);
                smtp.Dispose();
            }
            catch (Exception ex)
            {
                string mensagemErro = ex.Message;

                if (ex.InnerException != null)
                    mensagemErro += " " + ex.InnerException.Message;

                throw new CoreException("Erro: " + mensagemErro);
            }
        }
    }
}
