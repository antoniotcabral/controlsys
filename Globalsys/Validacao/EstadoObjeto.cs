﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Globalsys.Validacao
{
    public enum EstadoObjeto
    {
        Novo,
        Alterado,
        Removido,
        Ativado,
        Inativado
    }
}
