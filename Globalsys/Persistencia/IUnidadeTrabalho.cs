﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Globalsys
{
    public interface IUnidadeTrabalho
    {
        void BeginTransaction();

        void Commit();

        void Rollback();

        void Dispose();

        IList<T> ExecuteSql<T>(string sql, Dictionary<string, object> parameters);

        IList<T> ExecuteSqlHHT<T>(string sql, Dictionary<string, object> parameters);

        IList<T> ExecuteSql<T>(string sql, Dictionary<string, object> parameters, List<Type> listaEntities);

        void ExecuteProcedure(string procedure, Dictionary<string, object> parameters = null);
    }
}
