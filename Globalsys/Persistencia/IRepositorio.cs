﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Globalsys
{
    public interface IRepositorio<T>
    {
        T ObterPorId(object id);

        T ObterPorId(T entidade, string[] propModificadas);

        IQueryable<T> ObterTodos();

        void Salvar (T entidade);

        void Atualizar(T entidade);        

        void Remover (int id);

        void Remover (T id);
    }
}
