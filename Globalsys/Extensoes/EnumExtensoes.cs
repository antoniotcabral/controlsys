﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Globalsys
{
    public static class EnumExtensoes
    {
        public static string ObterDescricaoEnum(this Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            if (fi == null)
                return string.Format("Código {0} - Não mapeado no sistema", value);

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }
        /// <summary>
        /// Método que obtém o valor do atributo de descrição para eumeradors
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ObterDescricaoEnum<T>(this T? value) where T : struct
        { 
            if (!typeof(T).IsEnum)
                throw new Exception("Must be an enum.");

            if (!value.HasValue) 
                return string.Empty;

            FieldInfo fi = value.GetType().GetField(value.Value.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }

        public static T ParseEnum<T>(this string value)
        {
            try
            {
                var type = typeof(T);
                if (!type.IsEnum) throw new InvalidOperationException();

                foreach (var field in type.GetFields())
                {
                    var attribute = Attribute.GetCustomAttribute(field,
                        typeof(DescriptionAttribute)) as DescriptionAttribute;

                    if (attribute != null)
                    {
                        if (attribute.Description == value)
                            return (T)field.GetValue(null);
                    }
                    else
                    {
                        if (field.Name == value)
                            return (T)field.GetValue(null);
                    }
                }

                throw new ArgumentException("Não encontrado.", value);
            }
            catch (Exception ex)
            {

                throw ex;
            }
          
        }

    }

}
