﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Dominio.Acesso
{
    public class Camera
    {
        public int IdCamera { get; set; }
        public string Nome { get; set; }
        public string Modelo { get; set; }
        public string Descricao { get; set; }
        public bool Status { get; set; }
        public DateTime DataCadastro { get; set; }
        public int Precisao { get; set; }
        


    }
}