﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// Representa um(a) ParametroModelView.
    /// </summary>
    public class ParametroModelView
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Nome.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Nome.
        /// </value>
        public string Nome { get; set; }

        /// <summary>
        /// Gets/Sets valor para Valor.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Valor.
        /// </value>
        public string Valor { get; set; }

        /// <summary>
        /// Gets/Sets valor para Tipo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Tipo.
        /// </value>
        public string Tipo { get; set; }

        /// <summary>
        /// Gets/Sets valor para URL.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) URL.
        /// </value>
        public string Url { get; set; }
    }
}