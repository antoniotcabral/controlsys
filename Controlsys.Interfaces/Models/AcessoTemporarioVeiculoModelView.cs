﻿namespace Controlsys.Interfaces.Models
{
    public class AcessoTemporarioVeiculoModelView
    {
        public int Codigo { get; set; }
        public int CodigoVeiculoCredencialVeiculo { get; set; }
        public string Placa { get; set; }        
        public string Fabricante { get; set; }
        public string Empresa { get; set; }
        public string Modelo { get; set; }
        public string Cor { get; set; }        
        public string GrupoLeitoraNome  { get; set; }
        public string DataHoraValidadeInicio { get; set; }
        public string DataHoraValidadeFim { get; set; }
        public string DataRegistro { get; set; }        
        public bool AtivoTemp { get; set; }
    }
}