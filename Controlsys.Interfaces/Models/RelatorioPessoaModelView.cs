﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// Representa um(a) RelatorioPessoaModelView.
    /// </summary>
    public class RelatorioPessoaModelView
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public string Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para CPF.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CPF.
        /// </value>
        public string CPF { get; set; }

        /// <summary>
        /// Gets/Sets valor para Passaporte.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Passaporte.
        /// </value>
        public string Passaporte { get; set; }

        /// <summary>
        /// Gets/Sets valor para Nome.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Nome.
        /// </value>
        public string Nome { get; set; }

        /// <summary>
        /// Gets/Sets valor para Apelido.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Apelido.
        /// </value>
        public string Apelido { get; set; }

        /// <summary>
        /// Gets/Sets valor para Email.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Email.
        /// </value>
        public string Email { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataNascimento.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataNascimento.
        /// </value>
        public string DataNascimento { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataVisto.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataVisto.
        /// </value>
        public string DataVisto { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataValidadeVisto.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataValidadeVisto.
        /// </value>
        public string DataValidadeVisto { get; set; }

        /// <summary>
        /// Gets/Sets valor para Sexo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Sexo.
        /// </value>
        public string Sexo { get; set; }

        /// <summary>
        /// Gets/Sets valor para NomeMae.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) NomeMae.
        /// </value>
        public string NomeMae { get; set; }

        /// <summary>
        /// Gets/Sets valor para NomePai.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) NomePai.
        /// </value>
        public string NomePai { get; set; }

        /// <summary>
        /// Gets/Sets valor para RNE.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) RNE.
        /// </value>
        public string RNE { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataExpedicaoRNE.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataExpedicaoRNE.
        /// </value>
        public string DataExpedicaoRNE { get; set; }

        /// <summary>
        /// Gets/Sets valor para EstadoCivil.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) EstadoCivil.
        /// </value>
        public string EstadoCivil { get; set; }

        /// <summary>
        /// Gets/Sets valor para Escolaridade.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Escolaridade.
        /// </value>
        public string Escolaridade { get; set; }

        /// <summary>
        /// Gets/Sets valor para TipoSanguineo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TipoSanguineo.
        /// </value>
        public string TipoSanguineo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Nacionalidade.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Nacionalidade.
        /// </value>
        public string Nacionalidade { get; set; }

        /// <summary>
        /// Gets/Sets valor para NaturalidadeUF.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) NaturalidadeUF.
        /// </value>
        public string NaturalidadeUF { get; set; }

        /// <summary>
        /// Gets/Sets valor para Naturalidade.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Naturalidade.
        /// </value>
        public string Naturalidade { get; set; }

        /// <summary>
        /// Gets/Sets valor para RG.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) RG.
        /// </value>
        public string RG { get; set; }

        /// <summary>
        /// Gets/Sets valor para RGOrgaoEmissor.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) RGOrgaoEmissor.
        /// </value>
        public string RGOrgaoEmissor { get; set; }

        /// <summary>
        /// Gets/Sets valor para RGOrgaoEmissorUF.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) RGOrgaoEmissorUF.
        /// </value>
        public string RGOrgaoEmissorUF { get; set; }

        /// <summary>
        /// Gets/Sets valor para RGDataEmissao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) RGDataEmissao.
        /// </value>
        public string RGDataEmissao { get; set; }

        /// <summary>
        /// Gets/Sets valor para TituloEleitor.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TituloEleitor.
        /// </value>
        public string TituloEleitor { get; set; }

        /// <summary>
        /// Gets/Sets valor para TituloEleitorSecao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TituloEleitorSecao.
        /// </value>
        public string TituloEleitorSecao { get; set; }

        /// <summary>
        /// Gets/Sets valor para TituloEleitorZona.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TituloEleitorZona.
        /// </value>
        public string TituloEleitorZona { get; set; }

        /// <summary>
        /// Gets/Sets valor para TituloEleitorCidade.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TituloEleitorCidade.
        /// </value>
        public string TituloEleitorCidade { get; set; }

        /// <summary>
        /// Gets/Sets valor para TituloEleitorEstado.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TituloEleitorEstado.
        /// </value>
        public string TituloEleitorEstado { get; set; }

        /// <summary>
        /// Gets/Sets valor para CTPS.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CTPS.
        /// </value>
        public string CTPS { get; set; }

        /// <summary>
        /// Gets/Sets valor para CTPSData.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CTPSData.
        /// </value>
        public string CTPSData { get; set; }

        /// <summary>
        /// Gets/Sets valor para CTPSSerie.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CTPSSerie.
        /// </value>
        public string CTPSSerie { get; set; }

        /// <summary>
        /// Gets/Sets valor para CTPSEstado.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CTPSEstado.
        /// </value>
        public string CTPSEstado { get; set; }

        /// <summary>
        /// Gets/Sets valor para CNH.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CNH.
        /// </value>
        public string CNH { get; set; }

        /// <summary>
        /// Gets/Sets valor para CNHCategoria.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CNHCategoria.
        /// </value>
        public string CNHCategoria { get; set; }

        /// <summary>
        /// Gets/Sets valor para CNHDataValidade.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CNHDataValidade.
        /// </value>
        public string CNHDataValidade { get; set; }

        /// <summary>
        /// Gets/Sets valor para CertificadoReservista.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CertificadoReservista.
        /// </value>
        public string CertificadoReservista { get; set; }

        /// <summary>
        /// Gets/Sets valor para CertificadoReservistaCat.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CertificadoReservistaCat.
        /// </value>
        public string CertificadoReservistaCat { get; set; }

        /// <summary>
        /// Gets/Sets valor para TelefoneResidencial.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TelefoneResidencial.
        /// </value>
        public string TelefoneResidencial { get; set; }

        /// <summary>
        /// Gets/Sets valor para TelefoneCelular.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TelefoneCelular.
        /// </value>
        public string TelefoneCelular { get; set; }

        /// <summary>
        /// Gets/Sets valor para TelefoneEmergencial.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TelefoneEmergencial.
        /// </value>
        public string TelefoneEmergencial { get; set; }

        /// <summary>
        /// Gets/Sets valor para CEP.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CEP.
        /// </value>
        public string CEP { get; set; }

        /// <summary>
        /// Gets/Sets valor para Pais.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Pais.
        /// </value>
        public string Pais { get; set; }

        /// <summary>
        /// Gets/Sets valor para Estado.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Estado.
        /// </value>
        public string Estado { get; set; }

        /// <summary>
        /// Gets/Sets valor para Cidade.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Cidade.
        /// </value>
        public string Cidade { get; set; }

        /// <summary>
        /// Gets/Sets valor para Bairro.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Bairro.
        /// </value>
        public string Bairro { get; set; }

        /// <summary>
        /// Gets/Sets valor para Logradouro.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Logradouro.
        /// </value>
        public string Logradouro { get; set; }

        /// <summary>
        /// Gets/Sets valor para Numero.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Numero.
        /// </value>
        public string Numero { get; set; }

        /// <summary>
        /// Gets/Sets valor para Complemento.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Complemento.
        /// </value>
        public string Complemento { get; set; }

        /// <summary>
        /// Gets/Sets valor para ConselhoProfissional.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) ConselhoProfissional.
        /// </value>
        public string ConselhoProfissional { get; set; }

        /// <summary>
        /// Gets/Sets valor para RegistroProfissional.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) RegistroProfissional.
        /// </value>
        public string RegistroProfissional { get; set; }

        /// <summary>
        /// Gets/Sets valor para PessoaJuridica.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) PessoaJuridica.
        /// </value>
        public string PessoaJuridica { get; set; }

        /// <summary>
        /// Gets/Sets valor para Observacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Observacao.
        /// </value>
        public string Observacao { get; set; }

        /// <summary>
        /// Gets/Sets valor para HoristaMensalista.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) HoristaMensalista.
        /// </value>
        public string HoristaMensalista { get; set; }

        /// <summary>
        /// Gets/Sets valor para Salario.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Salario.
        /// </value>
        public string Salario { get; set; }

        /// <summary>
        /// Gets/Sets valor para StatusPapel.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) StatusPapel.
        /// </value>
        public string StatusPapel { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataStatusPapel.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataStatusPapel.
        /// </value>
        public string DataStatusPapel { get; set; }

        /// <summary>
        /// Gets/Sets valor para Empresa.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Empresa.
        /// </value>
        public string Empresa { get; set; }

        /// <summary>
        /// Gets/Sets valor para Cargo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Cargo.
        /// </value>
        public string Cargo { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataAdmissao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataAdmissao.
        /// </value>
        public string DataAdmissao { get; set; }

        /// <summary>
        /// Gets/Sets valor para ModeloCracha.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) ModeloCracha.
        /// </value>
        public string ModeloCracha { get; set; }

        /// <summary>
        /// Gets/Sets valor para PIS.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) PIS.
        /// </value>
        public string PIS { get; set; }

        /// <summary>
        /// Gets/Sets valor para PedidoCompra.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) PedidoCompra.
        /// </value>
        public string PedidoCompra { get; set; }

        /// <summary>
        /// Gets/Sets valor para Projeto.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Projeto.
        /// </value>
        public string Projeto { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataStatus.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataStatus.
        /// </value>
        public string DataStatus { get; set; }

        /// <summary>
        /// Gets/Sets valor para Status.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Status.
        /// </value>
        public string Status { get; set; }

        /// <summary>
        /// Gets/Sets valor para Papel.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Papel.
        /// </value>
        public string Papel { get; set; }

        /// <summary>
        /// Gets/Sets valor para Contratada.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Contratada.
        /// </value>
        public string Contratada { get; set; }

        /// <summary>
        /// Gets/Sets valor para SetorCusto.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) SetorCusto.
        /// </value>
        public string SetorCusto { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataRegistroCracha.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataRegistroCracha.
        /// </value>
        public string DataRegistroCracha { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataRegistroPapel.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataRegistroPapel.
        /// </value>
        public string DataRegistroPapel { get; set; }

        /// <summary>
        /// Gets/Sets valor para RFID.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) RFID.
        /// </value>
        public string RFID { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataValidadeASO.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataValidadeASO.
        /// </value>
        public string DataValidadeASO { get; set; }

        /// <summary>
        /// Gestor de ponto do colaborador
        /// </summary>
        public string GestorPonto { get; set; }

        /// <summary>
        /// Superior imediato do colaborador
        /// </summary>
        public string SuperiorImediato { get; set; }

        /// <summary>
        /// Número do colaborador no SAP
        /// </summary>
        public string EmpregadoSAP { get; set; }

        /// <summary>
        /// Área responsável pelo colaborador
        /// </summary>
        public string Area { get; set; }

        /// <summary>
        /// Número PHT do colaborador
        /// </summary>
        public string NumerosPHT { get; set; }

        /// <summary>
        /// Data de validade do Colaborador
        /// </summary>
        public string DataValidadeColaborador { get; set; }

        /// <summary>
        /// Data de validade do Prestador
        /// </summary>
        public string DataValidadePrestador { get; set; }

        /// <summary>
        /// Data de início do Visitante
        /// </summary>
        public string DataInicioVisitante { get; set; }

        /// <summary>
        /// Data de fim do Visitante
        /// </summary>
        public string DataFimVisitante { get; set; }

        /// <summary>
        /// valor para MaoDeObra
        /// </summary>
        public string MaoDeObra { get; set; }
    }
}