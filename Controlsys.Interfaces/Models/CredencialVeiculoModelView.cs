﻿using Controlsys.Dominio.CredenciamentoVeiculos;
using Controlsys.Dominio.Empresas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Controlsys.Dominio.Parametros;

namespace Controlsys.Interfaces.Models
{
    public class CredencialVeiculoModelView
    {
        public int Codigo { get; set; }
        public DateTime DataRegistro { get; set; }
        public DateTime? Vencimento { get; set; }
        public string Usuario { get; set; }
        
        public string Status { get; set; }
        public DateTime DataStatus { get; set; }
        public string ObservacaoStatus { get; set; }
        
        public EmpresaModelView Empresa { get; set; }
        public PedidoCompraModelView PedidoCompra { get; set; }
        public Area Area { get; set; }
        public EmpresaModelView SubContratada { get; set; }
        public string Fornecedor { get; set; }

        public CondutorModelView Condutor { get; set; }

        public ModeloCredencialModelView ModeloCredencial { get; set; }
        public TipoValidade TipoValidade { get; set; }
        public int? ValidadeDiasCredencial { get; set; }
        public DateTime? DataValidadeLit { get; set; }
        public DateTime? DataVencimentoSeguro { get; set; }
        public bool Corporativo { get; set; }
        public string Observacao { get; set; }

        public string Veiculo1 { get; set; }
        public string Veiculo2 { get; set; }

        public List<VeiculoCredencialVeiculoModelView> Veiculos { get; set; }
        public List<SituacaoCredencialVeiculoModelView> Situacoes { get; set; }

        public bool PodeInspecionar { get; set; }
        public bool PodeAprovar { get; set; }
        public bool PodeImprimir { get; set; }
        public bool PodeDevolver { get; set; }
        public string MotivoNaoPodeDevolver { get; set; }

        public bool ImprimirVerso { get; set; }
    }
}