﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    public class RelatorioAgendamentoVisitanteModelView
    {
            public virtual int Codigo {get; set;}
            public virtual string CPFPassaporte { get; set;}
            public virtual string Nome {get; set;}
            public virtual string NomeGuerra { get; set; }
            public virtual string NomeResponsavel { get; set; }
            public virtual string Email { get; set; }
            public virtual string Sexo { get; set; }
            public virtual string TelEmergencial { get; set; }
            public virtual string Celular { get; set; }
            public virtual string RG { get; set; }
            public virtual string OrgaoEmissor { get; set; }
            public virtual string UF { get; set; }
            public virtual string DataEmissao { get; set; }

            public virtual char AcessoPorto { get; set; }
            public virtual string CPFResponsavel { get; set; }
            public virtual string PassaporteResponsavel { get; set; }

            public virtual string Empresa { get; set; }
            public virtual string Funcao { get; set; }
            public virtual string Motivo { get; set; }
            public virtual DateTime? DataInicio { get; set; }
            public virtual DateTime? DataFinal { get; set; }

            public virtual string HorarioVisita { get; set; }
            public virtual string NomeSolicitante { get; set; }
            public virtual string CPFSolicitante { get; set; }
        public virtual string PassaporteSolicitante { get; set; }
        public virtual string AreaVisita { get; set; }
            public virtual string HoraAcesso { get; set; }

    }
}