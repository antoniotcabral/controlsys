﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    public class OcorrenciaSegurancaAgenteModelView
    {
        public int Codigo { get; set; }
        public int CodigoOcorrencia { get; set; }
        public string CodigoPessoa { get; set; }
        public string CPF { get; set; }
        public string Passaporte { get; set; }
        public string Empresa { get; set; }
        public string Contratada { get; set; }
        public string Nome { get; set; }
        public string Cracha { get; set; }
        public string Telefone { get; set; }
        public string Celular { get; set; }
        public string TipoPapel { get; set; }
        public string TipoAgente { get; set; }
    }
}