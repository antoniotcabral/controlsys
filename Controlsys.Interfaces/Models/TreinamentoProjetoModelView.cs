﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// Representa um(a) TreinamentoProjetoModelView.
    /// </summary>
    public class TreinamentoProjetoModelView
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Nome.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Nome.
        /// </value>
        public string Nome { get; set; }

        /// <summary>
        /// Gets/Sets valor para Obrigatorio.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Obrigatorio.
        /// </value>
        public bool Obrigatorio { get; set; }

        /// <summary>
        /// Gets/Sets valor para ImpressoCracha.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) ImpressoCracha.
        /// </value>
        public bool ImpressoCracha { get; set; }

        /// <summary>
        /// Gets/Sets valor para Observacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Observacao.
        /// </value>
        public string Observacao { get; set; }

    }
}