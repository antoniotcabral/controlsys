﻿namespace Controlsys.Interfaces.Models
{
    public class ArquivoPapelModelView
    {
        public int CodigoPapel { get; set; }
        public string Nome { get; set; }
        public string CPF { get; set; }
        public string Passaporte { get; set; }
        public string Empresa { get; set; }
        public string Cargo { get; set; }
        public string StatusPapel { get; set; }
    }
}