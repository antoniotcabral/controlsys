﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// Representa um(a) AlocacaoColaboradorModelView.
    /// </summary>
    public class AlocacaoColaboradorModelView
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para CPFPassaporte.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CPFPassaporte.
        /// </value>
        public string CPFPassaporte { get; set; }

        /// <summary>
        /// Gets/Sets valor para SetorCusto.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) SetorCusto.
        /// </value>
        public string SetorCusto { get; set; }

        /// <summary>
        /// Gets/Sets valor para Nome.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Nome.
        /// </value>
        public string Nome { get; set; }

        /// <summary>
        /// Gets/Sets valor para Status.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Status.
        /// </value>
        public string Status { get; set; }
    }
}