﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Controlsys.Dominio.Acesso;
using Ext.Net;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// Representa um(a) GrupoModelView.
    /// </summary>
    public class GrupoModelView
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Nome.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Nome.
        /// </value>
        public string Nome { get; set; }

        /// <summary>
        /// Gets/Sets valor para Descricao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Descricao.
        /// </value>
        public string Descricao { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataDesativacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataDesativacao.
        /// </value>
        public DateTime? DataDesativacao { get; set; }

        /// <summary>
        /// Gets/Sets a situação do objetivo do tipo GrupoModelView.
        /// </summary>
        ///
        /// <value>
        /// <see cref="DataDesativacao"/> nula retorna true, <see cref="DataDesativacao"/> com valor
        /// retorna false.
        /// </value>
        public bool Ativo { get; set; }

        /// <summary>
        /// Gets/Sets valor para GruposLeitoras.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) GruposLeitoras.
        /// </value>
        public GrupoLeitoraModelView[] GruposLeitoras { get; set; }

        /// <summary>
        /// Gets/Sets valor para Usuarios.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Usuarios.
        /// </value>
        public GrupoUsuarioModelView[] Usuarios { get; set; }

        /// <summary>
        /// Gets/Sets valor para Paginas.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Paginas.
        /// </value>
        public GrupoPermissoesModelView[] Paginas { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Models.GrupoModelView.
        /// </summary>
        public GrupoModelView()
        {
            GruposLeitoras = new GrupoLeitoraModelView[] { };
            Usuarios = new GrupoUsuarioModelView[] { };
            Paginas = new GrupoPermissoesModelView[] { };
        }
    }
}