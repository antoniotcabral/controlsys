﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    public class PedidosContratadaModelView
    {
        public virtual int CD_PEDIDO { get; set; }

        public virtual string CONTRATADA { get; set; }
                
    }
}