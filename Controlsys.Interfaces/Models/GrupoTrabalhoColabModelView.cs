﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// Representa um(a) GrupoTrabalhoColabModelView.
    /// </summary>
    public class GrupoTrabalhoColabModelView
    {
        /// <summary>
        /// Gets/Sets valor para GrupoTrabalho.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) GrupoTrabalho.
        /// </value>
        public string GrupoTrabalho { get; set; }

        /// <summary>
        /// Gets/Sets valor para ColaboradorCodigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) ColaboradorCodigo.
        /// </value>
        public string ColaboradorCodigo { get; set; }
    }
}