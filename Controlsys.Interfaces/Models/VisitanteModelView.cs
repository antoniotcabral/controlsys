﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Controlsys.Dominio.Pessoas;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Empresas;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// Representa um(a) VisitanteModelView.
    /// </summary>
    public class VisitanteModelView
    {
        /// <summary>
        /// Gets/Sets valor para CodigoVisitante.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CodigoVisitante.
        /// </value>
        public int CodigoVisitante { get; set; }

        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para CodigoPessoa.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CodigoPessoa.
        /// </value>
        public string CodigoPessoa { get; set; }

        /// <summary>
        /// Gets/Sets valor para CPF.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CPF.
        /// </value>
        public string CPF { get; set; }

        /// <summary>
        /// Gets/Sets valor para Passaporte.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Passaporte.
        /// </value>
        public string Passaporte { get; set; }

        /// <summary>
        /// Gets/Sets valor para Nome.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Nome.
        /// </value>
        public string Nome { get; set; }

        /// <summary>
        /// Gets/Sets valor para Apelido.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Apelido.
        /// </value>
        public string Apelido { get; set; }

        /// <summary>
        /// Gets/Sets valor para Email.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Email.
        /// </value>
        public string Email { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataNascimento.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataNascimento.
        /// </value>
        public DateTime? DataNascimento { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataVisto.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataVisto.
        /// </value>
        public DateTime? DataVisto { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataValidadeVisto.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataValidadeVisto.
        /// </value>
        public DateTime? DataValidadeVisto { get; set; }

        /// <summary>
        /// Gets/Sets valor para Sexo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Sexo.
        /// </value>
        public string Sexo { get; set; }

        /// <summary>
        /// Gets/Sets valor para NomeMae.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) NomeMae.
        /// </value>
        public string NomeMae { get; set; }

        /// <summary>
        /// Gets/Sets valor para NomePai.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) NomePai.
        /// </value>
        public string NomePai { get; set; }

        /// <summary>
        /// Gets/Sets valor para RNE.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) RNE.
        /// </value>
        public string RNE { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataExpedicaoRNE.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataExpedicaoRNE.
        /// </value>
        public DateTime? DataExpedicaoRNE { get; set; }

        /// <summary>
        /// Gets/Sets valor para EstadoCivil.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) EstadoCivil.
        /// </value>
        public EstadoCivil? EstadoCivil { get; set; }

        /// <summary>
        /// Gets/Sets valor para Escolaridade.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Escolaridade.
        /// </value>
        public Escolaridade? Escolaridade { get; set; }

        /// <summary>
        /// Gets/Sets valor para TipoSanguineo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TipoSanguineo.
        /// </value>
        public TipoSanguineo? TipoSanguineo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Img.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Img.
        /// </value>
        public string Img { get; set; }

        /// <summary>
        /// Gets/Sets valor para Nacionalidade.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Nacionalidade.
        /// </value>
        public string Nacionalidade { get; set; }

        /// <summary>
        /// Gets/Sets valor para NaturalidadeUF.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) NaturalidadeUF.
        /// </value>
        public int? NaturalidadeUF { get; set; }

        /// <summary>
        /// Gets/Sets valor para Naturalidade.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Naturalidade.
        /// </value>
        public int? Naturalidade { get; set; }

        /// <summary>
        /// Gets/Sets valor para RG.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) RG.
        /// </value>
        public string RG { get; set; }

        /// <summary>
        /// Gets/Sets valor para RGOrgaoEmissor.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) RGOrgaoEmissor.
        /// </value>
        public string RGOrgaoEmissor { get; set; }

        /// <summary>
        /// Gets/Sets valor para RGOrgaoEmissorUF.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) RGOrgaoEmissorUF.
        /// </value>
        public int? RGOrgaoEmissorUF { get; set; }

        /// <summary>
        /// Gets/Sets valor para RGDataEmissao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) RGDataEmissao.
        /// </value>
        public DateTime? RGDataEmissao { get; set; }

        /// <summary>
        /// Gets/Sets valor para TituloEleitor.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TituloEleitor.
        /// </value>
        public long? TituloEleitor { get; set; }

        /// <summary>
        /// Gets/Sets valor para TituloEleitorSecao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TituloEleitorSecao.
        /// </value>
        public int? TituloEleitorSecao { get; set; }

        /// <summary>
        /// Gets/Sets valor para TituloEleitorZona.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TituloEleitorZona.
        /// </value>
        public int? TituloEleitorZona { get; set; }

        /// <summary>
        /// Gets/Sets valor para TituloEleitorCidade.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TituloEleitorCidade.
        /// </value>
        public int? TituloEleitorCidade { get; set; }

        /// <summary>
        /// Gets/Sets valor para TituloEleitorEstado.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TituloEleitorEstado.
        /// </value>
        public int? TituloEleitorEstado { get; set; }

        /// <summary>
        /// Gets/Sets valor para CTPS.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CTPS.
        /// </value>
        public long? CTPS { get; set; }

        /// <summary>
        /// Gets/Sets valor para CTPSData.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CTPSData.
        /// </value>
        public DateTime? CTPSData { get; set; }

        /// <summary>
        /// Gets/Sets valor para CTPSSerie.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CTPSSerie.
        /// </value>
        public string CTPSSerie { get; set; }

        /// <summary>
        /// Gets/Sets valor para CTPSEstado.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CTPSEstado.
        /// </value>
        public int? CTPSEstado { get; set; }

        /// <summary>
        /// Gets/Sets valor para CNH.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CNH.
        /// </value>
        public string CNH { get; set; }

        /// <summary>
        /// Gets/Sets valor para CNHCategoria.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CNHCategoria.
        /// </value>
        public CategoriaCNH? CNHCategoria { get; set; }

        /// <summary>
        /// Gets/Sets valor para CNHDataValidade.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CNHDataValidade.
        /// </value>
        public DateTime? CNHDataValidade { get; set; }

        /// <summary>
        /// Gets/Sets valor para CertificadoReservista.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CertificadoReservista.
        /// </value>
        public string CertificadoReservista { get; set; }

        /// <summary>
        /// Gets/Sets valor para CertificadoReservistaCat.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CertificadoReservistaCat.
        /// </value>
        public string CertificadoReservistaCat { get; set; }

        /// <summary>
        /// Gets/Sets valor para TelefoneNumResidencial.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TelefoneNumResidencial.
        /// </value>
        public string TelefoneNumResidencial { get; set; }

        /// <summary>
        /// Gets/Sets valor para TelefoneNumEmergencial.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TelefoneNumEmergencial.
        /// </value>
        public string TelefoneNumEmergencial { get; set; }

        /// <summary>
        /// Gets/Sets valor para TelefoneNumCelular.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TelefoneNumCelular.
        /// </value>
        public string TelefoneNumCelular { get; set; }

        /// <summary>
        /// Gets/Sets valor para Endereco.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Endereco.
        /// </value>
        public EnderecoMovelView Endereco { get; set; }

        /// <summary>
        /// Gets/Sets valor para AntiPassBack.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) AntiPassBack.
        /// </value>
        public bool AntiPassBack { get; set; }

        /// <summary>
        /// Gets/Sets valor para AcessoPorto.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) AcessoPorto.
        /// </value>
        public bool AcessoPorto { get; set; }

        /// <summary>
        /// Gets/Sets valor para TipoVisitante.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TipoVisitante.
        /// </value>
        public string TipoVisitante { get; set; }

        /// <summary>
        /// Gets/Sets valor para Funcao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Funcao.
        /// </value>
        public string Funcao { get; set; }

        /// <summary>
        /// Gets/Sets valor para Motivo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Motivo.
        /// </value>
        public string Motivo { get; set; }

        /// <summary>
        /// Gets/Sets valor para StatusPapel.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) StatusPapel.
        /// </value>
        public string StatusPapel { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataStatusPapel.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataStatusPapel.
        /// </value>
        public DateTime? DataStatusPapel { get; set; }

        /// <summary>
        /// Gets/Sets valor para IdUsuario.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) IdUsuario.
        /// </value>
        public string IdUsuario { get; set; }

        /// <summary>
        /// Gets/Sets valor para Empresa.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Empresa.
        /// </value>
        public string Empresa { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataInicio.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataInicio.
        /// </value>
        public DateTime DataInicio { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataFim.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataFim.
        /// </value>
        public DateTime DataFim { get; set; }

        /// <summary>
        /// Gets/Sets valor para ModeloCracha.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) ModeloCracha.
        /// </value>
        public string ModeloCracha { get; set; }

        /// <summary>
        /// Gets/Sets valor para Solicitante.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Solicitante.
        /// </value>
        public ColaboradorModelView Solicitante { get; set; }

        /// <summary>
        /// Gets/Sets valor para ResponsavelVisita.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) ResponsavelVisita.
        /// </value>
        public ColaboradorModelView ResponsavelVisita { get; set; }

        /// <summary>
        /// Gets/Sets valor para crachar do visitante.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Código referente ao crachar.
        /// </value>

        public virtual string CodigoCracha { get; set; }

        /// <summary>
        /// Gets/Sets valor para Documentos.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Documentos.
        /// </value>
        public List<DocumentoPapelModelView> Documentos { get; set; }

        /// <summary>
        /// Gets/Sets valor para Treinamentos.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Treinamentos.
        /// </value>
        public List<TreinamentoPessoaModelView> Treinamentos { get; set; }
        public List<TreinamentoPessoaModelView> TreinamentosPessoa { get; set; }
        public List<AtendimentoReceptivoModelView> AtendimentosReceptivo { get; set; }

        /// <summary>
        /// Gets/Sets valor para HistoricoEmpresa.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) HistoricoEmpresa.
        /// </value>
        public List<Visitante> HistoricoEmpresa { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Models.VisitanteModelView.
        /// </summary>
        public VisitanteModelView()
        {
            Documentos = new List<DocumentoPapelModelView>();
            Treinamentos = new List<TreinamentoPessoaModelView>();
            HistoricoEmpresa = new List<Visitante>();
            TreinamentosPessoa = new List<TreinamentoPessoaModelView>(); ////Aqui histórico da pessoa física.
        }
    }
}