﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class RelatorioFullTimeEquivalenteModelView
    {
        /// <summary>
        /// 
        /// </summary>
        public int CodPapel { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int CodEmpresa { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int CodPedido { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int CodSetor { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? TipoCapex { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? TipoOpex { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal ValorFteCapex { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal ValorFteOpex { get; set; }
        /// <summary>
        /// Gets/Sets valor para QtdColaboradores.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) QtdColaboradores.
        /// </value>
        public int? QtdColaboradoresCapex { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? QtdColaboradoresOpex { get; set; }
        /// <summary>
        /// Gets/Sets valor para NomeSetor.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) NomeSetor.
        /// </value>
        public string NomeSetor { get; set; }
        /// <summary>
        /// Gets/Sets valor para NomeEmpresa.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) NomeEmpresa.
        /// </value>
        public string NomeContratada { get; set; }

        /// <summary>
        /// Gets/Sets valor para MaoDeObra.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) MaoDeObra.
        /// </value>
        public string MaoDeObra { get; set; }

        /// <summary>
        /// Gets/Sets valor para HorasTrabalhadas.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) HorasTrabalhadas.
        /// </value>
        //public decimal HorasTrabalhadas { get; set; }
    }
}