﻿using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Parametros;
using System.Collections.Generic;

namespace Controlsys.Interfaces.Models
{
    public class ItemClassificacaoJuridicaModelView
    {
        public ClassificacaoJuridica ClassificacaoJuridica { get; set; }
        public int Quantidade { get; set; }
        public double? Pontuacao { get; set; }
    }
}