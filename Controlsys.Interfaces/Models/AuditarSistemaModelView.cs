﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// Representa um(a) AuditarSistemaModelView.
    /// </summary>
    public class AuditarSistemaModelView
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Usuario.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Usuario.
        /// </value>
        public string Usuario { get; set; }

        /// <summary>
        /// Gets/Sets valor para CPFPassaporte.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CPFPassaporte.
        /// </value>
        public string CPFPassaporte { get; set; }

        /// <summary>
        /// Gets/Sets valor para Acao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Acao.
        /// </value>
        public string Acao { get; set; }

        /// <summary>
        /// Gets/Sets valor para URL.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) URL.
        /// </value>
        public string Url { get; set; }

        /// <summary>
        /// Gets/Sets valor para Descricao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Descricao.
        /// </value>
        public string Descricao { get; set; }

        /// <summary>
        /// Gets/Sets valor para Interface.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Interface.
        /// </value>
        public string Interface { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataHora.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataHora.
        /// </value>
        public DateTime DataHora { get; set; }

        /// <summary>
        /// Gets/Sets valor para CodigoUsuario.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CodigoUsuario.
        /// </value>
        public string CodigoUsuario { get; set; }
    }
}