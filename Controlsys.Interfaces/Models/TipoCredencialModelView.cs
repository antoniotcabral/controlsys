﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Controlsys.Dominio.Parametros;

namespace Controlsys.Interfaces.Models
{
    public class TipoCredencialModelView
    {
        public TipoCredencial Codigo { get; set; }
        public string Nome { get; set; }
    }
}