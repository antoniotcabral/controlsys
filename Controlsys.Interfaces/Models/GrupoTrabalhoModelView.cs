﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// Representa um(a) GrupoTrabalhoModelView.
    /// </summary>
    public class GrupoTrabalhoModelView
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Nome.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Nome.
        /// </value>
        public string Nome { get; set; }

        /// <summary>
        /// Gets/Sets a situação do objetivo do tipo GrupoTrabalhoModelView.
        /// </summary>
        ///
        /// <value>
        /// Retorna true or false.
        /// </value>
        public bool Ativo { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataInicio.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataInicio.
        /// </value>
        public DateTime DataInicio { get; set; }

        /// <summary>
        /// Gets/Sets valor para Turno.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Turno.
        /// </value>
        public string Turno { get; set; }

        /// <summary>
        /// Gets/Sets valor para TurnoTrabalho.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TurnoTrabalho.
        /// </value>
        public int TurnoTrabalho { get; set; }

        /// <summary>
        /// Gets/Sets valor para GrupoTrabColab.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) GrupoTrabColab.
        /// </value>
        public List<ColaboradorModelView> GrupoTrabColab { get; set; } 
    }
}