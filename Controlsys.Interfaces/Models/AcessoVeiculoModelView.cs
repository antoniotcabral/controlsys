﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// Representa um(a) AcessoVeiculoModelView.
    /// </summary>
    public class AcessoVeiculoModelView
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public string Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para NumCracha.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) NumCracha.
        /// </value>
        public string NumCracha { get; set; }

        /// <summary>
        /// Gets/Sets valor para Nome.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Nome.
        /// </value>
        public string Nome { get; set; }
        /// <summary>
        /// Gets/Sets valor para o código do Papel.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a)código do Papel.
        /// </value>
        public string CodPapel { get; set; }
        /// <summary>
        /// Gets/Sets valor para Papel.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Papel.
        /// </value>
        public string Papel { get; set; }

        /// <summary>
        /// Gets/Sets valor para CPF.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CPF.
        /// </value>
        public string CPF { get; set; }

        /// <summary>
        /// Gets/Sets valor para Empresa.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Empresa.
        /// </value>
        public string Empresa { get; set; }

        /// <summary>
        /// Gets/Sets valor para Contratada.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Contratada.
        /// </value>
        public string Contratada { get; set; }

        /// <summary>
        /// Gets/Sets valor para Subcontratada.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Subcontratada.
        /// </value>
        public string Subcontratada { get; set; }

        /// <summary>
        /// Gets/Sets valor para Cargo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Cargo.
        /// </value>
        public string Cargo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Leitora.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Leitora.
        /// </value>
        public string Leitora { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataHora.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataHora.
        /// </value>
        public DateTime DataHora { get; set; }

        /// <summary>
        /// Gets/Sets valor para StatusAcesso.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) StatusAcesso.
        /// </value>
        public string StatusAcesso { get; set; }

        /// <summary>
        /// Gets/Sets valor para Local.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Local.
        /// </value>
        public string Local { get; set; }

        /// <summary>
        /// Gets/Sets valor para Direcao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Direcao.
        /// </value>
        public string Direcao { get; set; }

        /// <summary>
        /// Gets/Sets valor para TipoSetor.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TipoSetor.
        /// </value>
        public string TipoSetor { get; set; }

        /// <summary>
        /// Gets/Sets valor para MaoDeObra.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) MaoDeObra.
        /// </value>
        public string MaoDeObra { get; set; }

        /// <summary>
        /// Gets/Sets valor para Setor.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Setor.
        /// </value>
        public string Setor { get; set; }

        public string MatriculaEmpregadoSAP { get; set; }

        public string MatriculaGestorPonto { get; set; }

        public string MatriculaSuperiorImediato { get; set; }

        public string NumeroPHT { get; set; }

        public string Placa { get; set; }

        public string ModeloCredencial { get; set; }
        public string Cor { get; set; }
        public string Modelo { get; set; }
        public string Fabricante { get; set; } 
        public string Imagens { get; set; }
        public string Camera { get; set; }
    }
}