﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    public class SindicatoModelView
    {
        public int Codigo { get; set; }
        public string Nome { get; set; }
        public string Apelido { get; set; } ////Razão Social
        public string CNPJ { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }
        public string TelEmergencia { get; set; }
        public string Celular { get; set; }
        public string DataRegistro { get; set; }
        public string DataDesativacao { get; set; }
        public bool Ativo { get; set; }
    }
}