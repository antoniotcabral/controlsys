﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Controlsys.Dominio.Empresas;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// Representa um(a) PedidoCompraModelView.
    /// </summary>
    public class PedidoCompraModelView
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public int Codigo { get; set; }

        public string NumeroNome { get; set; }

        /// <summary>
        /// Gets/Sets valor para Numero.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Numero.
        /// </value>
        public string Numero { get; set; }

        /// <summary>
        /// Gets/Sets valor para Nome.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Nome.
        /// </value>
        public string Nome { get; set; }

        /// <summary>
        /// Gets/Sets valor para Observacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Observacao.
        /// </value>
        public string Observacao { get; set; }

        /// <summary>
        /// Gets/Sets valor para Projeto.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Projeto.
        /// </value>
        public string Projeto { get; set; }

        /// <summary>
        /// Gets/Sets valor para PedidoCompraPai.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) PedidoCompraPai.
        /// </value>
        public int? PedidoCompraPai { get; set; }

        /// <summary>
        /// Gets/Sets valor para PedidoCompraPaiNome.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) PedidoCompraPaiNome.
        /// </value>
        public string PedidoCompraPaiNome { get; set; }

        /// <summary>
        /// Gets/Sets valor para CNAE.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CNAE.
        /// </value>
        public string CNAE { get; set; }

        /// <summary>
        /// Gets/Sets valor para CNAEDesc.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CNAEDesc.
        /// </value>
        public string CNAEDesc { get; set; }

        /// <summary>
        /// Gets/Sets valor para Empresa.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Empresa.
        /// </value>
        public string Empresa { get; set; }

        /// <summary>
        /// Gets/Sets valor para EmpresaDesc.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) EmpresaDesc.
        /// </value>
        public string EmpresaDesc { get; set; }

        /// <summary>
        /// Gets/Sets valor para Prazos.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Prazos.
        /// </value>
        public Prazo[] Prazos { get; set; }

        /// <summary>
        /// Gets/Sets valor para Setores.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Setores.
        /// </value>
        public SetorCustoPedidoModelView[] Setores { get; set; }

        /// <summary>
        /// Gets/Sets valor para GestoresResponsaveis.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) GestoresResponsaveis.
        /// </value>
        public ColaboradorPedidoModelView[] GestoresResponsaveis { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Models.PedidoCompraModelView.
        /// </summary>
        public PedidoCompraModelView()
        {
            Prazos = new Prazo[] { };
            Setores = new SetorCustoPedidoModelView[] { };
            GestoresResponsaveis = new ColaboradorPedidoModelView[] { };
        }
    }
}