﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    public class StatusSolicitacaoMaterialModelVIew
    {
        public int CodigoStatusSolicitacaoMaterial { get; set; }
        public string SituacaoSolicitacao { get; set; }
        public string DataRegistro { get; set; }
        public DateTime Data { get; set; }
        public string Observacao { get; set; }
    }
}