﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    public class EmpresaClassificacaoJuridicaModelView
    {
        public string Codigo { get; set; }
        public string CNPJ { get; set; }
        public string Apelido { get; set; }
        public ItemEmpresaClassificacaoJuridicaModelView[] Items { get; set; }
        public double Nota { get; set; }
        public string Classificacao { get; set; }
    }
}