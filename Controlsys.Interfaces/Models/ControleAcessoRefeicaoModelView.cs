﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    public class ControleAcessoRefeicaoModelView
    {
        public int Codigo { get; set; }

        public string Cracha { get; set; }

        [Description("CPF/Passaporte")]
        public string CPFPassaporte { get; set; }

        public string Nome { get; set; }

        [Description("Empresa CNPJ")]
        public string EmpresaCNPJ { get; set; }

        [Description("Empresa Nome Fantasia")]
        public string EmpresaNomeFantasia { get; set; }

        [Description("Contratada CNPJ")]
        public string ContratadaCNPJ { get; set; }

        [Description("Contratada Nome Fantasia")]
        public string ContratadaNomeFantasia { get; set; }

        [Description("Número PO")]
        public string NumeroPO { get; set; }

        //public string PODescricao { get; set; }

        public string Restaurante { get; set; }

        [Description("Tipo Alimentação")]
        public string TipoAlimentacao { get; set; }

        [Description("Setor Custo")]
        public string SetorCusto { get; set; }

        public string Controladora { get; set; }

        public string Leitora { get; set; }

        public DateTime Acesso { get; set; }

        [Description("Data Hora Acesso")]
        public string DataHoraAcesso { get; set; }
    }
}