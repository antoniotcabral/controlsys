﻿using Controlsys.Dominio.AgendamentosPessoa;
using Controlsys.Dominio.Pessoas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    public class VisitanteAgendamentoModelView
    {
        public int Codigo { get; set; }

        public int CodigoVisitante { get; set; }

        public int CodigoPessoa { get; set; }

        public string CPF { get; set; }

        public string Passaporte { get; set; }

        public string Nome { get; set; }

        public string Empresa { get; set; }

        public string Funcao { get; set; }

        public string StatusAgendamento { get; set; }

        public string Apelido { get; set; }

        public string Email { get; set; }

        public string Sexo { get; set; }

        public string TelefoneNumEmergencial { get; set; }

        public string TelefoneNumCelular { get; set; }

        public bool AcessoPorto { get; set; }

        public ColaboradorModelView Responsavel { get; set; }

        public ColaboradorModelView SolicitanteResp { get; set; }

        public string Motivo { get; set; }

        public DateTime DataInicio { get; set; }

        public DateTime DataFim { get; set; }

        public string Solicitante { get; set; }

        public string RG { get; set; }

        public string RGOrgaoEmissor { get; set; }

        public int? RGOrgaoEmissorUF { get; set; }

        public DateTime? RGDataEmissao { get; set; }

        public List<StatusPapelAgendamentoModelView> HistoricoStatus { get; set; }

    }
}