﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    public class RelatorioVisitantePresenteModelView
    {
            public virtual int Codigo {get; set;}
            public virtual string CPF {get; set;}
            public virtual string Passaporte {get; set;}
            public virtual string Nome {get; set;}
            public virtual string NomeSolicitante { get; set; }
            public virtual string CPFSolicitante {get; set;}
            public virtual string PassaporteSolicitante {get; set;}
            public virtual string EmpresaSolicitante {get; set;}
            public virtual string Local {get; set;}
            public virtual string GrupoLeitoraExterna {get; set;}
            public virtual DateTime DataAcesso {get; set;}
            public virtual DateTime HoraAcesso { get; set; }
    }
}