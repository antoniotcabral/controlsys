﻿using Controlsys.Dominio.Acesso;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    public class RestauranteModelView
    {
        public int Codigo { get; set; }

        public string Nome { get; set; }

        public int GrupoLeitora { get; set; }
        public string descGrupoLeitora { get; set; }
        public bool Ativo { get; set; }

        public List<TipoAlimentacaoRestModelView> TiposAlimentacao { get; set; }

        public RestauranteModelView()
        {
            TiposAlimentacao = new List<TipoAlimentacaoRestModelView>();
        }
    }
}