﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    public class PessoaAgendamentoModelView
    {
        
        public int CodigoPessoa { get; set; }

        public string CPF { get; set; }

        public string Passaporte { get; set; }

        public string Nome { get; set; }

        public string Apelido { get; set; }

        public string Email { get; set; }

        public string Sexo { get; set; }

        public string TelefoneNumEmergencial { get; set; }

        public string TelefoneNumCelular { get; set; }

        public string RG { get; set; }

        public string RGOrgaoEmissor { get; set; }

        public int? RGOrgaoEmissorUF { get; set; }

        public DateTime? RGDataEmissao { get; set; }

    }
}