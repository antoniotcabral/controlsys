﻿using Controlsys.Dominio.Acesso;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    public class ControladoraModelView
    {
        public LeitoraModelView[] Leitoras { get; set; }

        public string UltimaSincronizacao { get; set; }

        public int QtdListaOrdenada { get; set; }

        public int QtdListaDesordenada { get; set; }

        public int QtdMaxListaOrdenada { get; set; }

        public int QtdMaxListaDesordenada { get; set; }

        public string Status { get; set; }

        public string Online { get; set; }

        public decimal Corrente { get; set; }

        public int Temperatura { get; set; }

        public decimal Tensao { get; set; }

        public string HoraSincronizacao { get; set; }

        public string TipoControladoraDesc { get; set; }

        public Dominio.Acesso.TipoControladora TipoControladora { get; set; }

        public string IP { get; set; }

        public string Modelo { get; set; }

        public DateTime DataRegistro { get; set; }

        public bool Ativo { get; set; }

        public int Codigo { get; set; }
        
        public CameraModelView[] cameras { get; set; }

        public bool ValidaOCR { get; set; }
    }
}