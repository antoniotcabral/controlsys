﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// Representa um(a) RelatorioHomemHoraModelView.
    /// </summary>
    public class RelatorioHomemHoraModelView
    {

        /// <summary>
        /// Gets/Sets valor para ResponsavelSetor.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) ResponsavelSetor.
        /// </value>
        public string ResponsavelSetor { get; set; }

        /// <summary>
        /// Gets/Sets valor para NomeSetor.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) NomeSetor.
        /// </value>
        public string NomeSetor { get; set; }

        /// <summary>
        /// Gets/Sets valor para NomeEmpresa.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) NomeEmpresa.
        /// </value>
        public string NomeEmpresa { get; set; }

        /// <summary>
        /// Gets/Sets valor para NomeEmpresa.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) NomeEmpresa.
        /// </value>
        public string NomeContratada { get; set; }

        /// <summary>
        /// Gets/Sets valor para QtdColaboradores.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) QtdColaboradores.
        /// </value>
        public int QtdColaboradores { get; set; }

        /// <summary>
        /// Gets/Sets valor para HorasTrabalhadas.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) HorasTrabalhadas.
        /// </value>
        public decimal HorasTrabalhadas { get; set; }

    }
}