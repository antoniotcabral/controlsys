﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    public class ArquivoVeiculoModelView
    {

        public int CodigoVeiculoCredencialVeiculo { get; set; }
        public string Placa { get; set; }
        public string Modelo { get; set; }
        public string Condutor { get; set; }
        public string CPF { get; set; }
        public string Passaporte { get; set; }
        public string Empresa { get; set; }
        public string Status { get; set; }
    }
}