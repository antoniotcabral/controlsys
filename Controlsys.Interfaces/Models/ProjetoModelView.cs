﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Controlsys.Dominio.Empresas;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// Representa um(a) ProjetoModelView.
    /// </summary>
    public class ProjetoModelView
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Nome.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Nome.
        /// </value>
        public string Nome { get; set; }

        /// <summary>
        /// Gets/Sets valor para Descricao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Descricao.
        /// </value>
        public string Descricao { get; set; }

        /// <summary>
        /// Gets/Sets a situação do objetivo do tipo ProjetoModelView.
        /// </summary>
        ///
        /// <value>
        /// Retorna true or false.
        /// </value>
        public bool Ativo { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataInicio.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataInicio.
        /// </value>
        public DateTime DataInicio { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataFim.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataFim.
        /// </value>
        public DateTime DataFim { get; set; }

        /// <summary>
        /// Gets/Sets valor para Area.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Area.
        /// </value>
        public List<Area> Area { get; set; }

        /// <summary>
        /// Gets/Sets valor para Treinamento.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Treinamento.
        /// </value>
        public List<TreinamentoProjetoModelView> Treinamento { get; set; }

        /// <summary>
        /// Gets/Sets valor para GrupoTrabalho.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) GrupoTrabalho.
        /// </value>
        public List<GrupoTrabalhoModelView> GrupoTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Models.ProjetoModelView.
        /// </summary>
        public ProjetoModelView()
        {
            Area = new List<Area>();
            Treinamento = new List<TreinamentoProjetoModelView>();
            GrupoTrabalho = new List<GrupoTrabalhoModelView>();
        }
    }
}