﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// Representa um(a) EscalaModelView.
    /// </summary>
    public class EscalaModelView
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataEscala.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataEscala.
        /// </value>
        public DateTime DataEscala { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataRegistro.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataRegistro.
        /// </value>
        public DateTime DataRegistro { get; set; }

        /// <summary>
        /// Gets/Sets valor para GrupoTrabalho.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) GrupoTrabalho.
        /// </value>
        public string GrupoTrabalho { get; set; }

        /// <summary>
        /// Gets/Sets valor para HoraTurno.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) HoraTurno.
        /// </value>
        public string HoraTurno { get; set; }

    }
}