﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// Representa um(a) ColaboradorPedidoModelView.
    /// </summary>
    public class ColaboradorPedidoModelView
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para CPF.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CPF.
        /// </value>
        public string CPF { get; set; }

        /// <summary>
        /// Gets/Sets valor para Nome.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Nome.
        /// </value>
        public string Nome { get; set; }

        /// <summary>
        /// Gets/Sets valor para Empresa.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Empresa.
        /// </value>
        public string Empresa { get; set; }
    }
}