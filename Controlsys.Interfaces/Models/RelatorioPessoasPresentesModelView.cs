﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    public class RelatorioPessoasPresentesModelView
    {
        public virtual int Codigo { get; set; }
        public virtual string CPF { get; set; }
        public virtual string Passaporte { get; set; }
        public virtual string Nome { get; set; }
        public virtual string TipoPapel { get; set; }
        public virtual string Empresa { get; set; }
        public virtual string Local { get; set; }
        public virtual string GrupoLeitoraExterna { get; set; }
        public virtual DateTime DataAcesso { get; set; }
        public virtual DateTime HoraAcesso { get; set; }
    }
}