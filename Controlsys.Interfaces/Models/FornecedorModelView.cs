﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// Representa um(a) FornecedorModelView.
    /// </summary>
    public class FornecedorModelView
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para CNPJ.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CNPJ.
        /// </value>
        public string CNPJ { get; set; }

        /// <summary>
        /// Gets/Sets valor para NomeFantasia.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) NomeFantasia.
        /// </value>
        public string NomeFantasia { get; set; }

        /// <summary>
        /// Gets/Sets valor para RazaoSocial.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) RazaoSocial.
        /// </value>
        public string RazaoSocial { get; set; }

        /// <summary>
        /// Gets/Sets valor para NomeContato.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) NomeContato.
        /// </value>
        public string NomeContato { get; set; }

        /// <summary>
        /// Gets/Sets valor para TelefoneContato.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TelefoneContato.
        /// </value>
        public string TelefoneContato { get; set; }

        /// <summary>
        /// Gets/Sets valor para EmailContato.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) EmailContato.
        /// </value>
        public string EmailContato { get; set; }

        /// <summary>
        /// Gets/Sets a situação do objetivo do tipo FornecedorModelView.
        /// </summary>
        ///
        /// <value>
        /// Retorna true or false.
        /// </value>
        public bool Ativo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Empresas.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Empresas.
        /// </value>
        public List<EmpresaModelView> Empresas { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Models.FornecedorModelView.
        /// </summary>
        public FornecedorModelView()
        {
            Empresas = new List<EmpresaModelView>();
        }
    }
}