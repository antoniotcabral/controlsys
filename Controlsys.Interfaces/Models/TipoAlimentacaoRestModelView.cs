﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    public class TipoAlimentacaoRestModelView
    {
        public int Codigo { get; set; }
        public string Nome { get; set; }
        public string HoraInicio { get; set;}
        public string HoraFim { get; set; }


    }
}