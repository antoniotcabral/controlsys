﻿using Controlsys.Dominio.CredenciamentoVeiculos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    public class VeiculoCredencialVeiculoModelView
    {
        public VeiculoCredencialVeiculoModelView()
        {
            Midias = new List<MidiaModelView>();
        }

        public virtual int Codigo { get; set; }

        public virtual VeiculoModelView Veiculo { get; set; }

        public virtual CredencialVeiculoModelView CredencialVeiculo { get; set; }

        public virtual bool Ativo { get; set; }

        public virtual DateTime DataRegistro { get; set; }

        public virtual DateTime? DataDesativacao { get; set; }

        public virtual List<MidiaModelView> Midias { get; set; }
    }
}