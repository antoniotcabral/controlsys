﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    public class InfracaoModelView
    {
        public int Codigo { get; set; }
        public string Nome { get; set; }
        public string Sigla { get; set; }
        public string Grau { get; set; }
        public bool Ativo { get; set; }
    }
}