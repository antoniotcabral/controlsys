﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    public class MidiaModelView
    {
        public int CodigoMidia { get; set; }
        public string NomeOriginal { get; set; }
        public string NomeDisco { get; set; }
        public DateTime DataRegistro { get; set; }
        public string Arquivo { get; set; }
        public string MIME { get; set; }
        public string Extensao { get; set; }
        public string URL { get; set; }
    }
}