﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    public class VeiculoModelView
    {
        public int Codigo { get; set; }
        public int? CodigoCredencialVeiculo { get; set; }
        public string Placa { get; set; }
        public int FabricanteCodigo { get; set; }
        public string FabricanteNome { get; set; }
        public int ModeloCodigo { get; set; }
        public string ModeloNome { get; set; }
        public int AnoModelo { get; set; }
        public int AnoFabricacao { get; set; }
        public string Cor { get; set; }
    }
}