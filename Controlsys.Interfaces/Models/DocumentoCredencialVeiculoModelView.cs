﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    public class DocumentoCredencialVeiculoModelView
    {
        public int Codigo { get; set; }
        public string Nome { get; set; }
        public string Status { get; set; }
        public DateTime? DataEmissao { get; set; }
        public DateTime? DataValidade { get; set; }
        public string Descricao { get; set; }
    }
}