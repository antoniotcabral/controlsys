﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Controlsys.Dominio.Pessoas;

namespace Controlsys.Interfaces.Models
{
    public class ColaboradorAgendamentoModelView
    {
        public bool CadGestor { get; set; }
        public bool PodePreAprovar { get; set; }
        public bool PodeSalvarStatusPreAprovado { get; set; }        
        public bool PodeAprovar { get; set; }
        public bool PodeReprovar { get; set; }
        public bool PodeAlterarGestor { get; set; }
        
        public int CodigoColaborador { get; set; }
        public int Codigo { get; set; }
        public int CodigoPessoa { get; set; }
        public string CPF { get; set; }
        public string Passaporte { get; set; }
        public string Nome { get; set; }
        public string Apelido { get; set; }
        public string Email { get; set; }
        public DateTime? DataNascimento { get; set; }
        public DateTime? DataVisto { get; set; }
        public DateTime? DataValidadeVisto { get; set; }
        public string Sexo { get; set; }
        public string NomeMae { get; set; }
        public string NomePai { get; set; }
        public string RNE { get; set; }
        public DateTime? DataExpedicaoRNE { get; set; }
        public EstadoCivil? EstadoCivil { get; set; }
        public Escolaridade? Escolaridade { get; set; }
        public TipoSanguineo? TipoSanguineo { get; set; }
        public string Img { get; set; }
        public string Nacionalidade { get; set; }
        public int? NaturalidadeUF { get; set; }
        public int? Naturalidade { get; set; }
        public int? Area { get; set; }
        public string RG { get; set; }
        public string RGOrgaoEmissor { get; set; }
        public int? RGOrgaoEmissorUF { get; set; }
        public DateTime? RGDataEmissao { get; set; }
        public long? TituloEleitor { get; set; }
        public int? TituloEleitorSecao { get; set; }
        public int? TituloEleitorZona { get; set; }
        public int? TituloEleitorCidade { get; set; }
        public int? TituloEleitorEstado { get; set; }
        public long? CTPS { get; set; }
        public DateTime? CTPSData { get; set; }
        public string CTPSSerie { get; set; }
        public int? CTPSEstado { get; set; }
        public string CNH { get; set; }
        public CategoriaCNH? CNHCategoria { get; set; }
        public DateTime? CNHDataValidade { get; set; }
        public string CertificadoReservista { get; set; }
        public string CertificadoReservistaCat { get; set; }
        public string TelefoneNumResidencial { get; set; }
        public string TelefoneNumEmergencial { get; set; }
        public string TelefoneNumCelular { get; set; }
        public EnderecoMovelView Endereco { get; set; }
        public bool AntiPassBack { get; set; }
        public string ConselhoProfissional { get; set; }
        public string RegistroProfissional { get; set; }
        public bool PessoaJuridica { get; set; }
        public string PIS { get; set; }
        public string Observacao { get; set; }
        public HoristaMensalista HoristaMensalista { get; set; }
        public decimal Salario { get; set; }
        public string StatusAgendamento { get; set; }
        //public string StatusAlocacao { get; set; }
        //public DateTime DataStatusPapel { get; set; }
        public EmpresaModelView Empresa { get; set; }
        public string EmpresaDesc { get; set; }
        public int Cargo { get; set; }
        public string CargoDesc { get; set; }
        public DateTime? DataAdmissao { get; set; }
        public int PedidoCompra { get; set; }
        public string PedidoCompraNome { get; set; }
        public int Gestor { get; set; }
        public string GestorNome { get; set; }

        //public string SetorCusto { get; set; }
        //public string Projeto { get; set; }
        //public List<GrupoAcessoModelView> GruposAcesso { get; set; }
        public string ModeloCracha { get; set; }
        //public string Contratada { get; set; }
        public string GrupoTrabalho { get; set; }

        public string MotivoNegado { get; set; }

        public DateTime DataRegistro { get; set; }

        public long? EmpregadoSAP { get; set; }
        public long? GestorPonto { get; set; }
        public long? SuperiorImediato { get; set; }

        public string SolicitanteNome { get; set; }

        /// <summary>
        /// Gets/Sets valor para Documentos.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Documentos.
        /// </value>
        public List<DocumentoPapelModelView> Documentos { get; set; }

        public List<StatusPapelAgendamentoModelView> HistoricoStatus { get; set; }

    }
}