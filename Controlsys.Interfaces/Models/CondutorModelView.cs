﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    public class CondutorModelView
    {
        public int CodigoPapel { get; set; }
        public string Nome { get; set; }
        public string CPF { get; set; }
        public string Passaporte { get; set; }
        public string Cargo { get; set; }
        public string Empresa { get; set; }
        public string CNH { get; set; }
        public DateTime? CNHVencimento { get; set; }
    }
}