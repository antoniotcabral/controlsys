﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    public class ClassificacaoJuridicaModelView
    {

        public int Codigo { get; set; }

        public string ItemClassificacao { get; set; }

        public string Letra { get; set; }

        public string Formula { get; set; }

        public int? Peso { get; set; }

        public bool Pontua { get; set; }

        public bool Ativo { get; set; }
    }
}