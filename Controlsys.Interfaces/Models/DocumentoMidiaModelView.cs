﻿using Controlsys.Dominio.Parametros;
using System.Collections.Generic;

namespace Controlsys.Interfaces.Models
{
    public class DocumentoMidiaModelView
    {
        /// <summary>
        /// Gets/Sets valor para Documento.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Documento.
        /// </value>
        public Documento Documento { get; set; }

        /// <summary>
        /// Gets/Sets valor para Midias.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Midias.
        /// </value>
        public List<MidiaModelView> Midias { get; set; }
    }
}