﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// Representa um(a) GrupoLeitoraModelView.
    /// </summary>
    public class GrupoLeitoraModelView
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Nome.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Nome.
        /// </value>
        public string Nome { get; set; }

        /// <summary>
        /// Gets/Sets valor para CodigoGrupoLeitoraAcesso.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CodigoGrupoLeitoraAcesso.
        /// </value>
        public int CodigoGrupoLeitoraAcesso { get; set; }

        public string HoraInicio { get; set; }

        public string HoraFim { get; set; }

        public bool Ativo { get; set; }

        public bool AreaAlfandegada { get; set; }
    }
}