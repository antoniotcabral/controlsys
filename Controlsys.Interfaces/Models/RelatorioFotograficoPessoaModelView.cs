﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    public class RelatorioFotograficoPessoaModelView
    {
        public int Codigo { get; set; }
        public string Foto { get; set; }
        public string CPF { get; set; }
        public string Passaporte { get; set; }
        public string Nome { get; set; }
        public string Apelido { get; set; }
        public string Sexo { get; set; }
        [Description("País")]
        public string Pais { get; set; }
        public string Estado { get; set; }
        public string Cidade { get; set; }
        public string Empresa { get; set; }
        public string Cargo { get; set; }
        [Description("Data Status")]
        public DateTime DataStatus { get; set; }
        public string Status { get; set; }
        public string Papel { get; set; }
        public string Contratada { get; set; }
        public string RFID { get; set; }
        public int CodigoPapel { get; set; }
    }
}