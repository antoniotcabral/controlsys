﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    public class ArquivoEmpresaModelView
    {
        public string CodigoEmpresa { get; set; }
        public string Nome { get; set; }
        public string Apelido { get; set; }
        public string CNPJ { get; set; }
    }
}