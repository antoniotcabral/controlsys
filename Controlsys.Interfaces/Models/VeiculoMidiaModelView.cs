﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    public class VeiculoMidiaModelView
    {
        public virtual int Codigo { get; set; }

        public virtual VeiculoModelView Veiculo { get; set; }

        public virtual MidiaModelView Midia { get; set; }

        public virtual DateTime DataRegistro { get; set; }

        public virtual DateTime? DataDesativacao { get; set; }

        public virtual bool Ativo { get; set; }
    }
}