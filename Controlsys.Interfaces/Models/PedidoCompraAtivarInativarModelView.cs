﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// Representa um(a) PedidoCompraAtivarInativarModelView.
    /// </summary>
    public class PedidoCompraAtivarInativarModelView
    {
        /// <summary>
        /// Gets/Sets valor para IdsInativar.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) IdsInativar.
        /// </value>
        public List<int> IdsInativar { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Models.PedidoCompraAtivarInativarModelView.
        /// </summary>
        public PedidoCompraAtivarInativarModelView()
        {
            IdsInativar = new List<int>();
        }
    }
}