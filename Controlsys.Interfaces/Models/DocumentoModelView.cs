﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// Representa um(a) DocumentoModelView.
    /// </summary>
    public class DocumentoModelView
    {
        /// <summary>
        /// Gets/Sets valor para CodigoDocumento.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CodigoDocumento.
        /// </value>
        public int CodigoDocumento { get; set; }

        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Sigla.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Sigla.
        /// </value>
        public string Sigla { get; set; }

        /// <summary>
        /// Gets/Sets valor para Descricao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Descricao.
        /// </value>
        public string Descricao { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataEmissao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataEmissao.
        /// </value>
        public DateTime? DataEmissao { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataValidade.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataValidade.
        /// </value>
        public DateTime? DataValidade { get; set; }
        //public int DiasValidade { get; set; }

        /// <summary>
        /// Gets/Sets valor para Status.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Status.
        /// </value>
        public string Status { get; set; }

        public int QtdMidias { get; set; }

        /// <summary>
        /// Gets/Sets valor para Midias.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Midias.
        /// </value>
        public List<MidiaModelView> Midias { get; set; }
    }
}