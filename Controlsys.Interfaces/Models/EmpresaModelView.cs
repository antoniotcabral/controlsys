﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Controlsys.Dominio.Empresas;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// Representa um(a) EmpresaModelView.
    /// </summary>
    public class EmpresaModelView
    {   
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public string Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Nome.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Nome.
        /// </value>
        public string Nome { get; set; }

        /// <summary>
        /// Gets/Sets valor para Apelido.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Apelido.
        /// </value>
        public string Apelido { get; set; }

        /// <summary>
        /// Gets/Sets valor para CNPJ.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CNPJ.
        /// </value>
        public string CNPJ { get; set; }

        /// <summary>
        /// Gets/Sets valor para InscEstadual.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) InscEstadual.
        /// </value>
        public string InscEstadual { get; set; }

        /// <summary>
        /// Gets/Sets valor para InscMunicipal.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) InscMunicipal.
        /// </value>
        public string InscMunicipal { get; set; }

        /// <summary>
        /// Gets/Sets valor para Email.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Email.
        /// </value>
        public string Email { get; set; }
        //public string NaturezaJuridica { get; set; }

        /// <summary>
        /// Gets/Sets a situação do objetivo do tipo EmpresaModelView.
        /// </summary>
        ///
        /// <value>
        /// Retorna true or false.
        /// </value>
        public bool Ativo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool CalculoFTE { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool ExibirRelatorioFTE { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool BloqColabSemPedido { get; set; }

        /// <summary>
        /// Gets/Sets valor para CNAEs.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CNAEs.
        /// </value>
        public IList<CNAEModelView> CNAEs { get; set; }

        /// <summary>
        /// Gets/Sets valor para Documentos.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Documentos.
        /// </value>
        public IList<DocumentoModelView> Documentos { get; set; }

        public IList<SindicatoModelView> Sindicatos { get; set; }

        /// <summary>
        /// telefone.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TelefoneResidencial.
        /// </value>
        public string TelefoneResidencial { get; set; }

        /// <summary>
        /// Gets/Sets valor para TelefoneEmergencial.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TelefoneEmergencial.
        /// </value>
        public string TelefoneEmergencial { get; set; }

        /// <summary>
        /// Gets/Sets valor para TelefoneCelular.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TelefoneCelular.
        /// </value>
        public string TelefoneCelular { get; set; }

        /// <summary>
        /// endereço.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Endereco.
        /// </value>
        public EnderecoMovelView Endereco { get; set; }

        /// <summary>
        /// Gets/Sets valor para EmpresaSAP.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) EmpresaSAP.
        /// </value>
        public long? EmpresaSAP { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Models.EmpresaModelView.
        /// </summary>
        public EmpresaModelView()
        {
            CNAEs = new List<CNAEModelView>();
            Documentos = new List<DocumentoModelView>();
            Sindicatos = new List<SindicatoModelView>();
        }
    }
}