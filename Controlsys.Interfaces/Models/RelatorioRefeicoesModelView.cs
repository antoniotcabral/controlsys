﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    public class RelatorioRefeicoesModelView
    {
        public int CodigoPapel { get; set; }

        public string CPF { get; set; }

        public string Passaporte { get; set; }

        public string Nome { get; set; }

        public string Cracha { get; set; }

        public string NomeFansatiaEmpresa { get; set; }

        public string NomeFantasiaContratada { get; set; }

        public string PO { get; set; }

        public string Restaurante { get; set; }

        public string TipoAlimentacao { get; set; }

        public string Controladora { get; set; }

        public string Leitora { get; set; }

        public DateTime DataHoraEntrada { get; set; }
    }
}