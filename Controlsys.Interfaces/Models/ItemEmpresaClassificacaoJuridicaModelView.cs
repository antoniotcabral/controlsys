﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    public class ItemEmpresaClassificacaoJuridicaModelView
    {
        public string Nome { get; set; }

        public virtual string Letra { get; set; }

        public virtual string Formula { get; set; }

        public virtual int? Peso { get; set; }

        public virtual bool Pontua { get; set; }

        public virtual int Quantidade { get; set; }

        public virtual double? Pontuacao { get; set; }
    }
}