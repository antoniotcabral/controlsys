﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// Representa um(a) ModeloCrachaModelView.
    /// </summary>
    public class ModeloCrachaModelView 
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para TipoPapel.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TipoPapel.
        /// </value>
        public string TipoPapel { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataRegistro.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataRegistro.
        /// </value>
        public DateTime DataRegistro { get; set; }

        /// <summary>
        /// Gets/Sets a situação do objetivo do tipo ModeloCrachaModelView.
        /// </summary>
        ///
        /// <value>
        /// <see cref="DataDesativacao"/> nula retorna true, <see cref="DataDesativacao"/> com valor
        /// retorna false.
        /// </value>
        public bool Ativo { get; set; }
        public bool? Horizontal { get; set; }
        /// <summary>
        /// Gets/Sets valor para DataDesativacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataDesativacao.
        /// </value>
        public DateTime? DataDesativacao { get; set; }

        /// <summary>
        /// Gets/Sets valor para Nome.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Nome.
        /// </value>
        public string Nome { get; set; }

        /// <summary>
        /// Gets/Sets valor para Descricao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Descricao.
        /// </value>
        public string Descricao { get; set; }

        /// <summary>
        /// Gets/Sets valor para HTML.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) HTML.
        /// </value>
        public string Html { get; set; }

        /// <summary>
        /// Gets/Sets valor para Fundo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Fundo.
        /// </value>
        public string Fundo { get; set; }

    }
}
