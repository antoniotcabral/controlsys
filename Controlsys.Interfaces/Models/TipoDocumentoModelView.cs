﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// Representa um(a) TipoDocumentoModelView.
    /// </summary>
    public class TipoDocumentoModelView
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Sigla.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Sigla.
        /// </value>
        public string Sigla { get; set; }
    }
}