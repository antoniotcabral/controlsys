﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// Representa um(a) CrachaModelView.
    /// </summary>
    public class CrachaModelView
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para CodigoPessoa.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CodigoPessoa.
        /// </value>
        public string CodigoPessoa { get; set; }

        /// <summary>
        /// Gets/Sets valor para Nome.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Nome.
        /// </value>
        public string Nome { get; set; }

        /// <summary>
        /// Gets/Sets valor para Modelo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Modelo.
        /// </value>
        public string Modelo { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataImpressao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataImpressao.
        /// </value>
        public DateTime? DataImpressao { get; set; }

        /// <summary>
        /// Gets/Sets a situação do objetivo do tipo CrachaModelView.
        /// </summary>
        ///
        /// <value>
        /// Retorna true or false.
        /// </value>
        public bool Ativo { get; set; }
        public bool? HorizontalTemp { get; set; }
        /// <summary>
        /// Gets/Sets valor para RFID.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) RFID.
        /// </value>
        public string RFID { get; set; }
        public string NumeroCracha { get; set; }

        /// <summary>
        /// Gets/Sets valor para CodigoPapel.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CodigoPapel.
        /// </value>
        public int CodigoPapel { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataRegistro.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataRegistro.
        /// </value>
        public DateTime DataRegistro { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataDescarte.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataDescarte.
        /// </value>
        public DateTime? DataDescarte { get; set; }

        /// <summary>
        /// Gets/Sets valor para TipoPapel.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TipoPapel.
        /// </value>
        public string TipoPapel { get; set; }

        /// <summary>
        /// Gets/Sets valor para Empresa.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Empresa.
        /// </value>
        public string Empresa { get; set; }

        /// <summary>
        /// Gets/Sets valor para StatusPapel.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) StatusPapel.
        /// </value>
        public string StatusPapel { get; set; }

        /// <summary>
        /// Gets/Sets valor para Cpf.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Cpf.
        /// </value>
        public string Cpf { get; set; }
    }
}
