﻿using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// Representa um(a) PapelModelView.
    /// </summary>
    public class PapelModelView
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para PessoaFisica.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) PessoaFisica.
        /// </value>
        public PessoaFisica PessoaFisica { get; set; }

        /// <summary>
        /// Gets/Sets valor para Cargo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Cargo.
        /// </value>
        public string Cargo { get; set; }

        public string EmpresaNome { get; set; }

        /// <summary>
        /// Gets/Sets valor para AntiPassBack.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) AntiPassBack.
        /// </value>
        public bool AntiPassBack { get; set; }

        /// <summary>
        /// Gets/Sets valor para SenhaLeitora.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) SenhaLeitora.
        /// </value>
        public string SenhaLeitora { get; set; }

        /// <summary>
        /// Gets/Sets valor para Supervisor.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Supervisor.
        /// </value>
        public bool Supervisor { get; set; }

        /// <summary>
        /// Gets/Sets valor para Supervisionado.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Supervisionado.
        /// </value>
        public bool Supervisionado { get; set; }

        /// <summary>
        /// Gets/Sets valor para SobAnalise.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) SobAnalise.
        /// </value>
        public bool SobAnalise { get; set; }

        /// <summary>
        /// Gets/Sets valor para PessoaJuridica.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) PessoaJuridica.
        /// </value>
        public bool PessoaJuridica { get; set; }

        /// <summary>
        /// Gets/Sets valor para CodigoPapel.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CodigoPapel.
        /// </value>
        public int CodigoPapel { get; set; }
    }
}