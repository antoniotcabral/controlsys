﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// Representa um(a) LogradouroModelView.
    /// </summary>
    public class LogradouroModelView
    {
        /// <summary>
        /// Gets/Sets valor para CodigoLog.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CodigoLog.
        /// </value>
        public int CodigoLog { get; set; }

        /// <summary>
        /// Gets/Sets valor para Bairro.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Bairro.
        /// </value>
        public String Bairro { get; set; }

        /// <summary>
        /// Gets/Sets valor para Cidade.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Cidade.
        /// </value>
        public String Cidade { get; set; }

        /// <summary>
        /// Gets/Sets valor para Estado.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Estado.
        /// </value>
        public String Estado { get; set; }

        /// <summary>
        /// Gets/Sets valor para Pais.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Pais.
        /// </value>
        public String Pais { get; set; }

        /// <summary>
        /// Gets/Sets valor para Logradouro.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Logradouro.
        /// </value>
        public String Logradouro { get; set; }
    }
}