﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    public class GraficoAcessosDiaModelView
    {
        public int Codigo { get; set; }
        public DateTime Data { get; set; }
        public int Entradas { get; set; }
        public int Saidas { get; set; }
    }
}