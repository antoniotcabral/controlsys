﻿using Controlsys.Dominio.Ocorrencias;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// Representa um(a) RelatorioPessoaModelView.
    /// </summary>
    public class RelatorioOcorrenciaSegurancaModelView
    {
        public RelatorioOcorrenciaSegurancaModelView()
        {            
            this.Agentes = new List<OcorrenciaSegurancaAgenteModelView>();         
        }

        public string ID { get; set; }

        public int Codigo { get; set; }        

        public string Descricao { get; set; }

        public string Procedimentos { get; set; }

        public string AcoesRecomendadas { get; set; }

        public string DataRegistro { get; set; }

        public string DataOcorrencia { get; set; }

        public string HoraOcorrencia { get; set; }

        public string TipoOcorrencia { get; set; }

        public string TipoOcorrenciaDesc { get; set; }

        public string Vulnerabilidade { get; set; }

        public string Tentativa { get; set; }

        public string Efetiva { get; set; }

        public GravidadeOcorrencia? Gravidade { get; set; }
        
        public string GravidadeDesc { get; set; }

        public string Supervisor { get; set; }

        public string SupervisorLogin { get; set; }

        public string SupervisorNome { get; set; }        

        public string Local { get; set; }

        public string DataDesativacao { get; set; }

        public string Ativo { get; set; }

        public string Graduacoes { get; set; }

        public string AgentesPassivo { get; set; }

        public string AgentesAtivo { get; set; }

        public string Veiculos { get; set; }

        public string CPFAgentesPassivo { get; set; }

        public string PassaporteAgentesPassivo { get; set; }

        public string EmpresaAgentesPassivo { get; set; }

        public IList<OcorrenciaSegurancaAgenteModelView> Agentes { get; set; }
    }
}