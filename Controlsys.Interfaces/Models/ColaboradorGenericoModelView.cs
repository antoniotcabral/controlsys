﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    public class ColaboradorGenericoModelView
    {
        public virtual int Codigo { get; set; }

        public virtual string CPF { get; set; }

        public virtual string Passaporte { get; set; }

        public virtual string CodigoEmpresa { get; set; }

        public virtual string NomeEmpresa { get; set; }

        public virtual string CodigoPessoa { get; set; }

        public virtual string NomePessoa { get; set; }

        public virtual string Status { get; set; }

        public virtual string Cargo { get; set; }
    }
}