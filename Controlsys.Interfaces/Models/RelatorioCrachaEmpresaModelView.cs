﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Pessoas;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// Representa um(a) RelatorioCrachaEmpresaModelView.
    /// </summary>
    public class RelatorioCrachaEmpresaModelView
    {
        /// <summary>
        /// Gets/Sets valor para Contratada.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Contratada.
        /// </value>
        public string Contratada { get; set; }

        /// <summary>
        /// Gets/Sets valor para Subcontratada.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Subcontratada.
        /// </value>
        public string Subcontratada { get; set; }

        /// <summary>
        /// Gets/Sets valor para Empresa.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Empresa.
        /// </value>
        public string Empresa { get; set; }

        /// <summary>
        /// Gets/Sets valor para Papel.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Papel.
        /// </value>
        public string Papel { get; set; }

        /// <summary>
        /// Gets/Sets valor para Cracha.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Cracha.
        /// </value>
        public string Cracha { get; set; }

        /// <summary>
        /// Gets/Sets valor para CPF.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CPF.
        /// </value>
        public string CPF { get; set; }

        /// <summary>
        /// Gets/Sets valor para Passaporte.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Passaporte.
        /// </value>
        public string Passaporte { get; set; }

        /// <summary>
        /// Gets/Sets valor para Nome.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Nome.
        /// </value>
        public string Nome { get; set; }

        /// <summary>
        /// Gets/Sets valor para Funcao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Funcao.
        /// </value>
        public string Funcao { get; set; }

        /// <summary>
        /// Gets/Sets valor para Setor.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Setor.
        /// </value>
        public string Setor { get; set; }

        /// <summary>
        /// Gets/Sets valor para TipoSetor.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TipoSetor.
        /// </value>
        public string TipoSetor { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataVencimentoASO.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataVencimentoASO.
        /// </value>
        public DateTime? DataVencimentoASO { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataCadastro.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataCadastro.
        /// </value>
        public DateTime DataCadastro { get; set; }


    }

    /// <summary>
    /// Representa um(a) RelatorioCrachaEmpresaAuxModelView.
    /// </summary>
    public class RelatorioCrachaEmpresaAuxModelView
    {
        /// <summary>
        /// Gets/Sets valor para Cracha.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Cracha.
        /// </value>
        public Cracha Cracha { get; set; }

        /// <summary>
        /// Gets/Sets valor para Alocacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Alocacao.
        /// </value>
        public AlocacaoColaborador Alocacao { get; set; }  
    }
}