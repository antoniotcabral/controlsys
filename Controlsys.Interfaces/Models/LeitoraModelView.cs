﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Controlsys.Dominio.Acesso;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// Representa um(a) LeitoraModelView.
    /// </summary>
    public class LeitoraModelView
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para IDLeitora.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) IDLeitora.
        /// </value>
        public string IDLeitora { get; set; }

        /// <summary>
        /// Gets/Sets valor para Local.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Local.
        /// </value>
        public string Local { get; set; }

        /// <summary>
        /// Gets/Sets valor para Senha.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Senha.
        /// </value>
        public bool Senha { get; set; }

        /// <summary>
        /// Gets/Sets valor para Descricao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Descricao.
        /// </value>
        public string Descricao { get; set; }

        /// <summary>
        /// Gets/Sets valor para TipoLeitora.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TipoLeitora.
        /// </value>
        public string TipoLeitora { get; set; }

        /// <summary>
        /// Gets/Sets valor para Direcao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Direcao.
        /// </value>
        public DirecaoLeitora Direcao { get; set; }

        /// <summary>
        /// Gets/Sets a situação do objetivo do tipo LeitoraModelView.
        /// </summary>
        ///
        /// <value>
        /// Retorna true or false.
        /// </value>
        public bool Ativo { get; set; }

        /// <summary>
        /// Gets/Sets valor para IP.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) IP.
        /// </value>
        public string IP { get; set; }

        /// <summary>
        /// Gets/Sets valor para TipoControladora.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TipoControladora.
        /// </value>
        public string TipoControladora { get; set; }

        public string LocalLeitora { get; set; }

        public string DirecaoDesc { get; set; }

        public Dominio.Acesso.Fabricante Fabricante { get; set; }

        public string FabricanteDesc { get; set; }

        public int Ordem { get; set; }

        public bool EmiteBaixaCracha { get; set; }
    }
}