﻿using Controlsys.Dominio.CredenciamentoVeiculos;
//using Controlsys.Dominio.Empresas;
//using Controlsys.Dominio.Parametros;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    public class RelatorioVeiculosModelView
    {
        public virtual int Codigo { get; set; }
        public virtual DateTime DataRegistro { get; set; }
        public virtual string UsuarioNome { get; set; }
        public virtual DateTime? DataValidade { get; set; }

        //Empresa
        public virtual string EmpresaNome { get; set; }
        public virtual string PedidoCompraNumeroNome { get; set; }
        public virtual string AreaNome { get; set; }
        public virtual string SubContratadaNome { get; set; }
        public virtual string Fornecedor { get; set; }

        //Condutor
        public virtual string CondutorNome { get; set; }
        public virtual string CondutorCPF { get; set; }
        public virtual string CondutorPassaporte { get; set; }
        public virtual string CondutorCargoNome { get; set; }
        public virtual string CondutorEmpresaNome { get; set; }
        public virtual string CondutorCNH { get; set; }
        public virtual DateTime? CondutorCNHVencimento { get; set; }

        //Modelo Credencial
        public virtual string TipoCredencialNome { get; set; }
        public virtual string ModeloCredencialNome { get; set; }
        public virtual string TipoValidadeNome { get; set; }
        public virtual int? ValidadeDiasCredencial { get; set; }
        public virtual DateTime? DataValidadeLit { get; set; }
        public virtual DateTime DataVencimentoSeguro { get; set; }
        public virtual string Corporativo { get; set; }
        public virtual string Observacao { get; set; }

        //Veículo 1
        public virtual string Veiculo1Placa { get; set; }
        public virtual string Veiculo1Fabricante { get; set; }
        public virtual string Veiculo1Modelo { get; set; }
        public virtual int Veiculo1AnoFabricacao { get; set; }
        public virtual int Veiculo1AnoModelo { get; set; }
        public virtual string Veiculo1Cor { get; set; }

        //Veículo 2
        public virtual string Veiculo2Placa { get; set; }
        public virtual string Veiculo2Fabricante { get; set; }
        public virtual string Veiculo2Modelo { get; set; }
        public virtual int? Veiculo2AnoModelo { get; set; }
        public virtual int? Veiculo2AnoFabricacao { get; set; }
        public virtual string Veiculo2Cor { get; set; }

        //Situação
        public virtual string SituacaoStatus { get; set; }
        public virtual DateTime SituacaoData { get; set; }
        public virtual string SituacaoObservacao { get; set; }


        //Data Situações
        public virtual DateTime? DataRecebida { get; set; }
        public virtual DateTime? DataInspecionada { get; set; }
        public virtual DateTime? DataAprovada { get; set; }
        public virtual DateTime? DataImpressa { get; set; }
        public virtual DateTime? DataDevolvida { get; set; }

        //Documento
        //public virtual int DocumentoNome { get; set; }
        //public virtual string DocumentoStatus { get; set; }
        //public virtual DateTime? DocumentoDataEmissao { get; set; }
        //public virtual DateTime? DocumentoDataValidade { get; set; }
        //public virtual string DocumentoDescricao { get; set; }
    }
}