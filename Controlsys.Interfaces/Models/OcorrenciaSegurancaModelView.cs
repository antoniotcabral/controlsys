﻿using Controlsys.Dominio.CredenciamentoVeiculos;
using Controlsys.Dominio.Ocorrencias;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Seguranca;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// Representa um(a) OcorrenciaSegurancaModelView.
    /// </summary>
    public class OcorrenciaSegurancaModelView
    {
        public OcorrenciaSegurancaModelView()
        {
            this.Midias = new List<MidiaModelView>();
            this.Agentes = new List<OcorrenciaSegurancaAgenteModelView>();
            this.Veiculos = new List<OcorrenciaSegurancaVeiculoModelView>();
        }

        public int Codigo { get; set; }

        public string Descricao { get; set; }

        public string Procedimentos { get; set; }

        public string AcoesRecomendadas { get; set; }

        public DateTime DataRegistro { get; set; }

        public string DataRegistroFormatada { get; set; }

        public DateTime DataOcorrencia { get; set; }

        public double HoraOcorrencia { get; set; }

        public string HoraOcorrenciaFormatada { get; set; }

        public int TipoOcorrencia { get; set; }

        public string TipoOcorrenciaDesc { get; set; }

        public bool Vulnerabilidade { get; set; }

        public bool Tentativa { get; set; }

        public bool Efetiva { get; set; }

        public int CodigoGravidade { get; set; }
        public string Gravidade { get; set; }

        public IList<GraduacaoOcorrencia> Graduacao { get; set; }

        public string Supervisor { get; set; }

        public string SupervisorLogin { get; set; }

        public int CodigoLocal { get; set; }

        public string Local { get; set; }

        public DateTime? DataDesativacao { get; set; }

        public bool Ativo { get; set; }

        public IList<OcorrenciaSegurancaAgenteModelView> Agentes { get; set; }
        public IList<MidiaModelView> Midias { get; set; }
        public IList<OcorrenciaSegurancaVeiculoModelView> Veiculos { get; set; }

        public virtual List<GraduacaoOcorrencia> Graduacoes(bool tentativa, bool vulnerabilidade, bool efetiva)
        {            
            List<GraduacaoOcorrencia> graduacoes = new List<GraduacaoOcorrencia>();
            
            if (tentativa)
                graduacoes.Add(GraduacaoOcorrencia.Tentativa);

            if (vulnerabilidade)
                graduacoes.Add(GraduacaoOcorrencia.Vulnerabilidade);

            if (efetiva)
                graduacoes.Add(GraduacaoOcorrencia.Efetiva);

            return graduacoes;
        }
    }
}