﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// Representa um(a) TreinamentoPessoaModelView.
    /// </summary>
    public class TreinamentoPessoaModelView
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataRealizacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataRealizacao.
        /// </value>
        public DateTime DataRealizacao { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataValidade.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataValidade.
        /// </value>
        public DateTime DataValidade { get; set; }

        /// <summary>
        /// Gets/Sets valor para Nome.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Nome.
        /// </value>
        public string Nome { get; set; }

        /// <summary>
        /// Gets/Sets valor para CodigoTreinamento.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CodigoTreinamento.
        /// </value>
        public int CodigoTreinamento { get; set; }
    }
}