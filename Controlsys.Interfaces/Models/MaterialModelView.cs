﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    public class MaterialModelView
    {
        public int CodigoMaterial { get; set; }
        public string Descricao { get; set; }
        public string Patrimonio { get; set; }
        public string NotaServico { get; set; }
        public string NotaFiscalEntrada { get; set; }
        public string NotaFiscalSaida { get; set; }
        public int Quantidade { get; set; }
        public bool? AutorizacaoVigilante { get; set; }
    }
}