﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Controlsys.Dominio.Pessoas;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// Representa um(a) PessoaFisicaModelView.
    /// </summary>
    public class PessoaFisicaModelView
    {
        public List<TreinamentoPessoaModelView> TreinamentosPessoa { get; set; }
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public string Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para CodigoPessoa.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CodigoPessoa.
        /// </value>
        public string CodigoPessoa { get; set; }

        /// <summary>
        /// Gets/Sets valor para TipoPapel.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TipoPapel.
        /// </value>
        public string TipoPapel { get; set; }

        /// <summary>
        /// Gets/Sets valor para Papel.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Papel.
        /// </value>
        public Papel Papel { get; set;}

        /// <summary>
        /// Gets/Sets valor para CPF.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CPF.
        /// </value>
        public string CPF { get; set; }

        /// <summary>
        /// Gets/Sets valor para Passaporte.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Passaporte.
        /// </value>
        public string Passaporte { get; set; }

        /// <summary>
        /// Gets/Sets valor para Nome.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Nome.
        /// </value>
        public string Nome { get; set; }

        /// <summary>
        /// Gets/Sets valor para Apelido.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Apelido.
        /// </value>
        public string Apelido { get; set; }

        /// <summary>
        /// Gets/Sets valor para Apelido o ultimo..
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Apelido o ultimo.
        /// </value>
        public string ApelidoLast { get; set; }

        /// <summary>
        /// Gets/Sets valor para Email.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Email.
        /// </value>
        public string Email { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataNascimento.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataNascimento.
        /// </value>
        public DateTime? DataNascimento { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataVisto.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataVisto.
        /// </value>
        public DateTime? DataVisto { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataValidadeVisto.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataValidadeVisto.
        /// </value>
        public DateTime? DataValidadeVisto { get; set; }

        /// <summary>
        /// Gets/Sets valor para Sexo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Sexo.
        /// </value>
        public Sexo Sexo { get; set; }

        /// <summary>
        /// Gets/Sets valor para NomeMae.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) NomeMae.
        /// </value>
        public string NomeMae { get; set; }

        /// <summary>
        /// Gets/Sets valor para NomePai.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) NomePai.
        /// </value>
        public string NomePai { get; set; }

        /// <summary>
        /// Gets/Sets valor para RNE.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) RNE.
        /// </value>
        public string RNE { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataExpedicaoRNE.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataExpedicaoRNE.
        /// </value>
        public DateTime? DataExpedicaoRNE { get; set; }

        /// <summary>
        /// Gets/Sets valor para EstadoCivil.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) EstadoCivil.
        /// </value>
        public EstadoCivil? EstadoCivil { get; set; }

        /// <summary>
        /// Gets/Sets valor para Escolaridade.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Escolaridade.
        /// </value>
        public Escolaridade? Escolaridade { get; set; }

        /// <summary>
        /// Gets/Sets valor para TipoSanguineo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TipoSanguineo.
        /// </value>
        public TipoSanguineo? TipoSanguineo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Nacionalidade.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Nacionalidade.
        /// </value>
        public string Nacionalidade { get; set; }

        /// <summary>
        /// Gets/Sets valor para NaturalidadeUF.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) NaturalidadeUF.
        /// </value>
        public int? NaturalidadeUF { get; set; }

        /// <summary>
        /// Gets/Sets valor para Naturalidade.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Naturalidade.
        /// </value>
        public int? Naturalidade { get; set; }

        /// <summary>
        /// RG.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) RG.
        /// </value>
        public string RG { get; set; }

        /// <summary>
        /// Gets/Sets valor para RGOrgaoEmissor.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) RGOrgaoEmissor.
        /// </value>
        public string RGOrgaoEmissor { get; set; }

        /// <summary>
        /// Gets/Sets valor para RGOrgaoEmissorUF.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) RGOrgaoEmissorUF.
        /// </value>
        public int? RGOrgaoEmissorUF { get; set; }

        /// <summary>
        /// Gets/Sets valor para RGDataEmissao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) RGDataEmissao.
        /// </value>
        public DateTime? RGDataEmissao { get; set; }

        /// <summary>
        /// Titulo de eleitor.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TituloEleitor.
        /// </value>
        public long? TituloEleitor { get; set; }

        /// <summary>
        /// Gets/Sets valor para TituloEleitorSecao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TituloEleitorSecao.
        /// </value>
        public int? TituloEleitorSecao { get; set; }

        /// <summary>
        /// Gets/Sets valor para TituloEleitorZona.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TituloEleitorZona.
        /// </value>
        public int? TituloEleitorZona { get; set; }

        /// <summary>
        /// Gets/Sets valor para TituloEleitorEstado.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TituloEleitorEstado.
        /// </value>
        public int? TituloEleitorEstado { get; set; }

        /// <summary>
        /// Gets/Sets valor para TituloEleitorCidade.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TituloEleitorCidade.
        /// </value>
        public int? TituloEleitorCidade { get; set; }

        /// <summary>
        /// CTPS.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CTPS.
        /// </value>
        public long? CTPS { get; set; }

        /// <summary>
        /// Gets/Sets valor para CTPSData.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CTPSData.
        /// </value>
        public DateTime? CTPSData { get; set; }

        /// <summary>
        /// Gets/Sets valor para CTPSSerie.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CTPSSerie.
        /// </value>
        public string CTPSSerie { get; set; }

        /// <summary>
        /// Gets/Sets valor para CTPSEstado.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CTPSEstado.
        /// </value>
        public int? CTPSEstado { get; set; }

        /// <summary>
        /// CNH.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CNH.
        /// </value>
        public string CNH { get; set; }

        /// <summary>
        /// Gets/Sets valor para CNHCategoria.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CNHCategoria.
        /// </value>
        public CategoriaCNH? CNHCategoria { get; set; }

        /// <summary>
        /// Gets/Sets valor para CNHDataValidade.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CNHDataValidade.
        /// </value>
        public DateTime? CNHDataValidade { get; set; }

        /// <summary>
        /// Certificado de Reservista.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CertificadoReservista.
        /// </value>
        public string CertificadoReservista { get; set; }

        /// <summary>
        /// Gets/Sets valor para CertificadoReservistaCat.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CertificadoReservistaCat.
        /// </value>
        public string CertificadoReservistaCat { get; set; }

        /// <summary>
        /// Telefones.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TelefoneNumResidencial.
        /// </value>
        public string TelefoneNumResidencial { get; set; }

        /// <summary>
        /// Gets/Sets valor para TelefoneNumEmergencial.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TelefoneNumEmergencial.
        /// </value>
        public string TelefoneNumEmergencial { get; set; }

        /// <summary>
        /// Gets/Sets valor para TelefoneNumCelular.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TelefoneNumCelular.
        /// </value>
        public string TelefoneNumCelular { get; set; }

        /// <summary>
        /// Foto.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) img.
        /// </value>
        public string img { get; set; }

        /// <summary>
        /// Gets/Sets valor para Endereco.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Endereco.
        /// </value>
        public EnderecoMovelView Endereco { get; set; }

        /// <summary>
        /// Gets/Sets valor para PapelModelView.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) PapelModelView.
        /// </value>
        public PapelModelView PapelModelView { get; set; }

        /// <summary>
        /// Gets/Sets valor para PapelLog.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) PapelLog.
        /// </value>
        public PapelLog PapelLog { get; set; }

        /// <summary>
        /// Gets/Sets valor para CodigoPapel.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CodigoPapel.
        /// </value>
        public int CodigoPapel { get; set; }

        /// <summary>
        /// Gets/Sets valor para Img.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Img.
        /// </value>
        public string Img { get; set; }
        public UsuarioModelView Usuario { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Models.PessoaFisicaModelView.
        /// </summary>
        public PessoaFisicaModelView()
        {
            TreinamentosPessoa = new List<TreinamentoPessoaModelView>(); ////Aqui histórico da pessoa física. 
        }
    }
}