﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// Representa um(a) AuditoriaModelView.
    /// </summary>
    public class AuditoriaModelView
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Nome.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Nome.
        /// </value>
        public string Nome { get; set; }

        /// <summary>
        /// Gets/Sets valor para Documento.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Documento.
        /// </value>
        public string Documento { get; set; }

        /// <summary>
        /// Gets/Sets valor para Pagina.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Pagina.
        /// </value>
        public string Pagina { get; set; }

        /// <summary>
        /// Gets/Sets valor para Acao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Acao.
        /// </value>
        public string Acao { get; set; }

        /// <summary>
        /// Gets/Sets valor para Descricao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Descricao.
        /// </value>
        public string Descricao { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataHora.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataHora.
        /// </value>
        public DateTime DataHora { get; set; }
    }
}