﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Enderecos;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// Representa um(a) ColaboradorModelView.
    /// </summary>
    public class ColaboradorModelView
    {
        /// <summary>
        /// Gets/Sets valor para CodigoColaborador.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CodigoColaborador.
        /// </value>
        public int CodigoColaborador { get; set; }

        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para CodigoPessoa.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CodigoPessoa.
        /// </value>
        public string CodigoPessoa { get; set; }

        /// <summary>
        /// Gets/Sets valor para CPF.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CPF.
        /// </value>
        public string CPF { get; set; }

        /// <summary>
        /// Gets/Sets valor para Passaporte.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Passaporte.
        /// </value>
        public string Passaporte { get; set; }

        /// <summary>
        /// Gets/Sets valor para Nome.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Nome.
        /// </value>
        public string Nome { get; set; }

        /// <summary>
        /// Gets/Sets valor para Apelido.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Apelido.
        /// </value>
        public string Apelido { get; set; }

        /// <summary>
        /// Gets/Sets valor para Email.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Email.
        /// </value>
        public string Email { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataNascimento.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataNascimento.
        /// </value>
        public DateTime? DataNascimento { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataVisto.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataVisto.
        /// </value>
        public DateTime? DataVisto { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataValidadeVisto.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataValidadeVisto.
        /// </value>
        public DateTime? DataValidadeVisto { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataValidade.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataValidade.
        /// </value>
        public DateTime? DataValidade { get; set; }

        /// <summary>
        /// Gets/Sets valor para Sexo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Sexo.
        /// </value>
        public string Sexo { get; set; }

        /// <summary>
        /// Gets/Sets valor para NomeMae.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) NomeMae.
        /// </value>
        public string NomeMae { get; set; }

        /// <summary>
        /// Gets/Sets valor para NomePai.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) NomePai.
        /// </value>
        public string NomePai { get; set; }

        /// <summary>
        /// Gets/Sets valor para RNE.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) RNE.
        /// </value>
        public string RNE { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataExpedicaoRNE.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataExpedicaoRNE.
        /// </value>
        public DateTime? DataExpedicaoRNE { get; set; }

        /// <summary>
        /// Gets/Sets valor para EstadoCivil.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) EstadoCivil.
        /// </value>
        public EstadoCivil? EstadoCivil { get; set; }

        /// <summary>
        /// Gets/Sets valor para Escolaridade.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Escolaridade.
        /// </value>
        public Escolaridade? Escolaridade { get; set; }

        /// <summary>
        /// Gets/Sets valor para TipoSanguineo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TipoSanguineo.
        /// </value>
        public TipoSanguineo? TipoSanguineo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Img.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Img.
        /// </value>
        public string Img { get; set; }

        /// <summary>
        /// Gets/Sets valor para Nacionalidade.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Nacionalidade.
        /// </value>
        public string Nacionalidade { get; set; }

        /// <summary>
        /// Gets/Sets valor para NaturalidadeUF.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) NaturalidadeUF.
        /// </value>
        public int? NaturalidadeUF { get; set; }

        /// <summary>
        /// Gets/Sets valor para Naturalidade.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Naturalidade.
        /// </value>
        public int? Naturalidade { get; set; }
        /// <summary>
        /// Gets/Sets valor para Área.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Área.
        /// </value>
        public int? Area { get; set; }

        /// <summary>
        /// Gets/Sets valor para RG.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) RG.
        /// </value>
        public string RG { get; set; }

        /// <summary>
        /// Gets/Sets valor para RGOrgaoEmissor.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) RGOrgaoEmissor.
        /// </value>
        public string RGOrgaoEmissor { get; set; }

        /// <summary>
        /// Gets/Sets valor para RGOrgaoEmissorUF.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) RGOrgaoEmissorUF.
        /// </value>
        public int? RGOrgaoEmissorUF { get; set; }

        /// <summary>
        /// Gets/Sets valor para RGDataEmissao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) RGDataEmissao.
        /// </value>
        public DateTime? RGDataEmissao { get; set; }

        /// <summary>
        /// Gets/Sets valor para TituloEleitor.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TituloEleitor.
        /// </value>
        public long? TituloEleitor { get; set; }

        /// <summary>
        /// Gets/Sets valor para TituloEleitorSecao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TituloEleitorSecao.
        /// </value>
        public int? TituloEleitorSecao { get; set; }

        /// <summary>
        /// Gets/Sets valor para TituloEleitorZona.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TituloEleitorZona.
        /// </value>
        public int? TituloEleitorZona { get; set; }

        /// <summary>
        /// Gets/Sets valor para TituloEleitorCidade.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TituloEleitorCidade.
        /// </value>
        public int? TituloEleitorCidade { get; set; }

        /// <summary>
        /// Gets/Sets valor para TituloEleitorEstado.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TituloEleitorEstado.
        /// </value>
        public int? TituloEleitorEstado { get; set; }

        /// <summary>
        /// Gets/Sets valor para CTPS.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CTPS.
        /// </value>
        public long? CTPS { get; set; }

        /// <summary>
        /// Gets/Sets valor para CTPSData.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CTPSData.
        /// </value>
        public DateTime? CTPSData { get; set; }

        /// <summary>
        /// Gets/Sets valor para CTPSSerie.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CTPSSerie.
        /// </value>
        public string CTPSSerie { get; set; }

        /// <summary>
        /// Gets/Sets valor para CTPSEstado.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CTPSEstado.
        /// </value>
        public int? CTPSEstado { get; set; }

        /// <summary>
        /// Gets/Sets valor para CNH.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CNH.
        /// </value>
        public string CNH { get; set; }

        /// <summary>
        /// Gets/Sets valor para CNHCategoria.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CNHCategoria.
        /// </value>
        public CategoriaCNH? CNHCategoria { get; set; }

        /// <summary>
        /// Gets/Sets valor para CNHDataValidade.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CNHDataValidade.
        /// </value>
        public DateTime? CNHDataValidade { get; set; }

        /// <summary>
        /// Gets/Sets valor para CertificadoReservista.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CertificadoReservista.
        /// </value>
        public string CertificadoReservista { get; set; }

        /// <summary>
        /// Gets/Sets valor para CertificadoReservistaCat.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CertificadoReservistaCat.
        /// </value>
        public string CertificadoReservistaCat { get; set; }

        /// <summary>
        /// Gets/Sets valor para TelefoneNumResidencial.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TelefoneNumResidencial.
        /// </value>
        public string TelefoneNumResidencial { get; set; }

        /// <summary>
        /// Gets/Sets valor para TelefoneNumEmergencial.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TelefoneNumEmergencial.
        /// </value>
        public string TelefoneNumEmergencial { get; set; }

        /// <summary>
        /// Gets/Sets valor para TelefoneNumCelular.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TelefoneNumCelular.
        /// </value>
        public string TelefoneNumCelular { get; set; }

        /// <summary>
        /// Gets/Sets valor para Endereco.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Endereco.
        /// </value>
        public EnderecoMovelView Endereco { get; set; }

        /// <summary>
        /// Gets/Sets valor para AntiPassBack.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) AntiPassBack.
        /// </value>
        public bool AntiPassBack { get; set; }

        /// <summary>
        /// Gets/Sets valor para ConselhoProfissional.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) ConselhoProfissional.
        /// </value>
        public string ConselhoProfissional { get; set; }

        /// <summary>
        /// Gets/Sets valor para RegistroProfissional.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) RegistroProfissional.
        /// </value>
        public string RegistroProfissional { get; set; }

        /// <summary>
        /// Gets/Sets valor para PessoaJuridica.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) PessoaJuridica.
        /// </value>
        public bool PessoaJuridica { get; set; }

        /// <summary>
        /// Gets/Sets valor para AcessoPorto.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) AcessoPorto.
        /// </value>
        public bool AcessoPorto { get; set; }

        /// <summary>
        /// Gets/Sets valor para PIS.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) PIS.
        /// </value>
        public string PIS { get; set; }

        /// <summary>
        /// Gets/Sets valor para Observacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Observacao.
        /// </value>
        public string Observacao { get; set; }

        /// <summary>
        /// Gets/Sets valor para HoristaMensalista.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) HoristaMensalista.
        /// </value>
        public HoristaMensalista HoristaMensalista { get; set; }

        /// <summary>
        /// Gets/Sets valor para Salario.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Salario.
        /// </value>
        public decimal Salario { get; set; }

        /// <summary>
        /// Gets/Sets valor para StatusPapel.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) StatusPapel.
        /// </value>
        public string StatusPapel { get; set; }

        /// <summary>
        /// Gets/Sets valor para MaoDeObra.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) MaoDeObra.
        /// </value>
        public int MaoDeObra { get; set; }

        /// <summary>
        /// Gets/Sets valor para GrupoPessoa.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) GrupoPessoa.
        /// </value>
        public int GrupoPessoa { get; set; }

        /// <summary>
        /// Gets/Sets valor para IdUsuario.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) IdUsuario.
        /// </value>
        public string IdUsuario { get; set; }

        /// <summary>
        /// Gets/Sets valor para SetorCusto.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) SetorCusto.
        /// </value>
        public int? SetorCusto { get; set; }

        /// <summary>
        /// Gets/Sets valor para StatusAlocacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) StatusAlocacao.
        /// </value>
        public string StatusAlocacao { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataStatusPapel.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataStatusPapel.
        /// </value>
        public DateTime DataStatusPapel { get; set; }

        /// <summary>
        /// Gets/Sets valor para Empresa.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Empresa.
        /// </value>
        public string Empresa { get; set; }

        public string SubContratista { get; set; }
        public string Cnpj_SubContratista { get; set; }
        /// <summary>
        /// Gets/Sets valor para Cargo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Cargo.
        /// </value>

        public int Cargo { get; set; }

        /// <summary>
        /// Gets/Sets valor para CargoDesc.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CargoDesc.
        /// </value>
        public string CargoDesc { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataAdmissao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataAdmissao.
        /// </value>
        public DateTime? DataAdmissao { get; set; }

        /// <summary>
        /// Gets/Sets valor para PedidoCompra.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) PedidoCompra.
        /// </value>
        public string PedidoCompra { get; set; }

        /// <summary>
        /// Gets/Sets valor para SetorCusto.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) SetorCusto.
        /// </value>
        public string SetorCustoPed { get; set; }

        /// <summary>
        /// Gets/Sets valor para Projeto.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Projeto.
        /// </value>
        public string Projeto { get; set; }

        /// <summary>
        /// Gets/Sets valor para Documentos.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Documentos.
        /// </value>
        public List<DocumentoPapelModelView> Documentos { get; set; }

        /// <summary>
        /// Gets/Sets valor para Treinamentos.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Treinamentos.
        /// </value>
        public List<TreinamentoPessoaModelView> Treinamentos { get; set; }

        /// <summary>
        /// Gets/Sets valor para Asos.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Asos.
        /// </value>
        public List<ASO> Asos { get; set; }

        /// <summary>
        /// Gets/Sets valor para HistoricoEmpresa.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) HistoricoEmpresa.
        /// </value>
        public List<Empresa> HistoricoEmpresa { get; set; }

        /// <summary>
        /// Gets/Sets valor para GruposAcesso.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) GruposAcesso.
        /// </value>
        public List<GrupoAcessoModelView> GruposAcesso { get; set; }

        /// <summary>
        /// Gets/Sets valor para ExcecoesAcesso.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) ExcecoesAcesso.
        /// </value>
        public List<ExcecaoAcessoModelView> ExcecoesAcesso { get; set; }

        /// <summary>
        /// Gets/Sets valor para ModeloCracha.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) ModeloCracha.
        /// </value>
        public string ModeloCracha { get; set; }

        /// <summary>
        /// Gets/Sets valor para CodigoCracha.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CodigoCracha.
        /// </value>
        public string CodigoCracha { get; set; }

        /// <summary>
        /// Gets/Sets valor para EmpresaDesc.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) EmpresaDesc.
        /// </value>
        public string EmpresaDesc { get; set; }

        /// <summary>
        /// Gets/Sets valor para Contratada.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Contratada.
        /// </value>
        public string Contratada { get; set; }

        /// <summary>
        /// Gets/Sets valor para GrupoTrabalho.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) GrupoTrabalho.
        /// </value>
        public string GrupoTrabalho { get; set; }

        /// <summary>
        /// Gets/Sets valor para Código GrupoTrabalho.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Código GrupoTrabalho.
        /// </value>
        public int CodigoGrupoTrabalho { get; set; }

        /// <summary>
        /// Gets/Sets valor para IdMotivo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) IdMotivo.
        /// </value>
        public virtual string IdMotivo { get; set; }

        /// <summary>
        /// Gets/Sets valor para DescMotivo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DescMotivo.
        /// </value>
        public virtual string DescMotivo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Processado.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Processado.
        /// </value>
        public virtual DateTime? Processado { get; set; }

        /// <summary>
        /// Gets/Sets valor para TipoAcesso.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TipoAcesso.
        /// </value>
        public virtual Int32 TipoAcesso { get; set; }


        public long? EmpregadoSAP { get; set; }
        public long? GestorPonto { get; set; }
        public long? SuperiorImediato { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Models.ColaboradorModelView.
        /// </summary>
        public ColaboradorModelView()
        {
            Documentos = new List<DocumentoPapelModelView>();
            Treinamentos = new List<TreinamentoPessoaModelView>();
            Asos = new List<ASO>();
            HistoricoEmpresa = new List<Empresa>();
            GruposAcesso = new List<GrupoAcessoModelView>();
            ExcecoesAcesso = new List<ExcecaoAcessoModelView>();
        }
    }
}