﻿using Controlsys.Dominio.Acesso;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// Representa um(a) MonitorarAcessosModelView.
    /// </summary>
    public class MonitorarAcessosModelView
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Nome.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Nome.
        /// </value>
        public string Nome { get; set; }

        /// <summary>
        /// Gets/Sets valor para Apelido.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Apelido.
        /// </value>
        public string Apelido { get; set; }

        /// <summary>
        /// Gets/Sets valor para CPF.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CPF.
        /// </value>
        public string CPF { get; set; }

        /// <summary>
        /// Gets/Sets valor para NumCracha.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) NumCracha.
        /// </value>
        public string NumCracha { get; set; }

        /// <summary>
        /// Gets/Sets valor para Papel.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Papel.
        /// </value>
        public string Papel { get; set; }

        /// <summary>
        /// Gets/Sets valor para Passaporte.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Passaporte.
        /// </value>
        public string Passaporte { get; set; }

        /// <summary>
        /// Gets/Sets valor para Empresa.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Empresa.
        /// </value>
        public string Empresa { get; set; }

        /// <summary>
        /// Gets/Sets valor para Cargo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Cargo.
        /// </value>
        public string Cargo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Telefone.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Telefone.
        /// </value>
        public string Telefone { get; set; }

        /// <summary>
        /// Gets/Sets valor para Email.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Email.
        /// </value>
        public string Email { get; set; }

        /// <summary>
        /// Gets/Sets valor para Sobanalise.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Sobanalise.
        /// </value>
        public string Sobanalise { get; set; }

        /// <summary>
        /// Gets/Sets valor para Antipassback.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Antipassback.
        /// </value>
        public string Antipassback { get; set; }

        /// <summary>
        /// Gets/Sets valor para Supervisionado.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Supervisionado.
        /// </value>
        public string Supervisionado { get; set; }

        /// <summary>
        /// Gets/Sets valor para StatusAcesso.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) StatusAcesso.
        /// </value>
        public string StatusAcesso { get; set; }

        /// <summary>
        /// Gets/Sets valor para Direcao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Direcao.
        /// </value>
        public string Direcao { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataHora.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataHora.
        /// </value>
        public string DataHora { get; set; }

        /// <summary>
        /// Gets/Sets valor para Local.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Local.
        /// </value>
        public string Local { get; set; }

        /// <summary>
        /// Gets/Sets valor para Leitora.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Leitora.
        /// </value>
        public string Leitora { get; set; }

        /// <summary>
        /// Gets/Sets valor para Img.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Img.
        /// </value>
        public string Img { get; set; }

        /// <summary>
        /// Gets/Sets valor para PermitidoSobAnalise.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) PermitidoSobAnalise.
        /// </value>
        public bool  PermitidoSobAnalise { get; set; }

        /// <summary>
        /// Gets/Sets valor para CodigoPagina.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CodigoPagina.
        /// </value>
        public int CodigoPagina { get; set; }

        /// <summary>
        /// Gets/Sets valor para CodigoPapel.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CodigoPapel.
        /// </value>
        public int CodigoPapel { get; set; }
    }
}