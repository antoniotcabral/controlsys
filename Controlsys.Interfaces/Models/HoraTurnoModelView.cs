﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// Representa um(a) HoraTurnoModelView.
    /// </summary>
    public class HoraTurnoModelView
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para HoraInicio.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) HoraInicio.
        /// </value>
        public TimeSpan HoraInicio { get; set; }

        /// <summary>
        /// Gets/Sets valor para HoraFim.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) HoraFim.
        /// </value>
        public TimeSpan HoraFim { get; set; }

        /// <summary>
        /// Gets/Sets valor para Ordem.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Ordem.
        /// </value>
        public int Ordem { get; set; }

        /// <summary>
        /// Gets/Sets valor para DiaSemana.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DiaSemana.
        /// </value>
        public string DiaSemana { get; set; }

        /// <summary>
        /// Gets/Sets valor para Trabalha.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Trabalha.
        /// </value>
        public bool Trabalha { get; set; }

        /// <summary>
        /// Gets/Sets valor para Maior24h.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Maior24h.
        /// </value>
        public bool Maior24h { get; set; }

        /// <summary>
        /// Gets/Sets a situação do objetivo do tipo HoraTurnoModelView.
        /// </summary>
        ///
        /// <value>
        /// Retorna true or false.
        /// </value>
        public bool Ativo { get; set; }
    }
}