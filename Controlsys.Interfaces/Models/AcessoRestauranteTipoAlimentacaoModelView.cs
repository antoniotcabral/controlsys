﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    public class AcessoRestauranteTipoAlimentacaoModelView
    {
        public int Codigo { get; set; }
        public int CodRestaurante { get; set; }
        public int CodTipoAlimento { get; set; }
        public string TipoAlimentacao { get; set; }
        public string Restaurante { get; set; }

    }
}