﻿using System;

namespace Controlsys.Interfaces.Models
{
    public class NotificaValidadeDocumentosModelView
    {
        public string TipoDocumento { get; set; }
        public DateTime DataValidade { get; set; }

    }
}