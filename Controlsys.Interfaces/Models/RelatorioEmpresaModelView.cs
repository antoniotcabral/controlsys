﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// Representa um(a) RelatorioEmpresaModelView.
    /// </summary>
    public class RelatorioEmpresaModelView
    {
         /// <summary>
         /// Gets/Sets valor para Codigo.
         /// </summary>
         ///
         /// <value>
         /// Retorna o(a) Codigo.
         /// </value>
         public string Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Nome.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Nome.
        /// </value>
        public string Nome { get; set; }

        /// <summary>
        /// Gets/Sets valor para Apelido.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Apelido.
        /// </value>
        public string Apelido { get; set; }

        /// <summary>
        /// Gets/Sets valor para CNPJ.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CNPJ.
        /// </value>
        public string CNPJ { get; set; }

        /// <summary>
        /// Gets/Sets valor para InscEstadual.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) InscEstadual.
        /// </value>
        public string InscEstadual { get; set; }

        /// <summary>
        /// Gets/Sets valor para InscMunicipal.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) InscMunicipal.
        /// </value>
        public string InscMunicipal { get; set; }

        /// <summary>
        /// Gets/Sets valor para Email.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Email.
        /// </value>
        public string Email { get; set; }

        /// <summary>
        /// Gets/Sets a situação do objetivo do tipo RelatorioEmpresaModelView.
        /// </summary>
        ///
        /// <value>
        /// Retorna true or false.
        /// </value>
        public bool Ativo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Status.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Status.
        /// </value>
        public string Status { get; set; }

        /// <summary>
        /// telefone.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TelefoneResidencial.
        /// </value>
        public string TelefoneResidencial { get; set; }

        /// <summary>
        /// Gets/Sets valor para TelefoneEmergencial.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TelefoneEmergencial.
        /// </value>
        public string TelefoneEmergencial { get; set; }

        /// <summary>
        /// Gets/Sets valor para TelefoneCelular.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TelefoneCelular.
        /// </value>
        public string TelefoneCelular { get; set; }

        /// <summary>
        /// endereço.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CEP.
        /// </value>
        public string CEP { get; set; }

        /// <summary>
        /// Gets/Sets valor para Pais.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Pais.
        /// </value>
        public string Pais { get; set; }

        /// <summary>
        /// Gets/Sets valor para Estado.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Estado.
        /// </value>
        public string Estado { get; set; }

        /// <summary>
        /// Gets/Sets valor para Cidade.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Cidade.
        /// </value>
        public string Cidade { get; set; }

        /// <summary>
        /// Gets/Sets valor para Bairro.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Bairro.
        /// </value>
        public string Bairro { get; set; }

        /// <summary>
        /// Gets/Sets valor para Logradouro.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Logradouro.
        /// </value>
        public string Logradouro { get; set; }

        /// <summary>
        /// Gets/Sets valor para Numero.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Numero.
        /// </value>
        public string Numero { get; set; }

        /// <summary>
        /// Gets/Sets valor para Complemento.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Complemento.
        /// </value>
        public string Complemento { get; set; }

        /// <summary>
        /// Gets/Sets valor para Complemento.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Complemento.
        /// </value>
        public DateTime DataRegistro { get; set; }

        /// <summary>
        /// Gets/Sets valor para Complemento.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Complemento.
        /// </value>
        public string DataDesativacao { get; set; }



    }
}