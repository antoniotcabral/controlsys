﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services.Description;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// Representa um(a) GrupoAcessoModelView.
    /// </summary>
    public class GrupoAcessoModelView
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Nome.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Nome.
        /// </value>
        public string Nome { get; set; }

        /// <summary>
        /// Gets/Sets a situação do objetivo do tipo GrupoAcessoModelView.
        /// </summary>
        ///
        /// <value>
        /// Retorna true or false.
        /// </value>
        public bool Ativo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Pessoas.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Pessoas.
        /// </value>
        public List<PessoaModelView> Pessoas { get; set; }

        /// <summary>
        /// Gets/Sets valor para GruposLeitoras.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) GruposLeitoras.
        /// </value>
        public List<GrupoLeitoraModelView> GruposLeitoras { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Models.GrupoAcessoModelView.
        /// </summary>
        public GrupoAcessoModelView()
        {
            Pessoas = new List<PessoaModelView>();
            GruposLeitoras = new List<GrupoLeitoraModelView>();
        }
    }
}