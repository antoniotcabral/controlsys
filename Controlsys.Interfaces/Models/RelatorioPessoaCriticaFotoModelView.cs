﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    public class RelatorioPessoaCriticaFotoModelView
    {
        public virtual string Foto { get; set; }
        public virtual string Cpf { get; set; }
        public virtual string Passaporte { get; set; }
        public virtual string Nome { get; set; }
        public virtual string RG { get; set; }
        public virtual string Papel { get; set; }
        public virtual string CodigoPapel { get; set; }
        public virtual string NomeEmpresa { get; set; }
        public virtual string CnpjEmpresa { get; set; }
        public virtual string NomeContratada { get; set; }
        public virtual string CnpjContratada { get; set; }
        public virtual string MotivoBloqueio { get; set; }
        public virtual DateTime DataBloqueio { get; set; }
        public virtual string Observacao { get; set; }
    }
}