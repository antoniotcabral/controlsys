﻿using Controlsys.Dominio.Acesso;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    public class AcessoRestauranteModelView
    {
        public int Codigo { get; set; }

        public string Nome { get; set; }

        public string TipoPapel { get; set; }

        public string StatusPapel { get; set; }

        public bool CrachaMestre { get; set; }

        public bool NaoValidaAcessoHierarquico { get; set; }

        public bool Ativo { get; set; }

        public string Cracha { get; set; }

        public string CpfPassaporte { get; set; }

        public string Empresa { get; set; }

        public List<AcessoRestauranteTipoAlimentacaoModelView> TipoAlimentacaoRest { get; set; }

        public List<LogAcessoRestaurante> HistCrachaMestre { get; set; }
        

        public AcessoRestauranteModelView()
        {
            TipoAlimentacaoRest = new List<AcessoRestauranteTipoAlimentacaoModelView>();
            HistCrachaMestre = new List<LogAcessoRestaurante>();
        }
    }
}