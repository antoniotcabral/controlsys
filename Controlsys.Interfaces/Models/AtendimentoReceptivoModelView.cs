﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    public class AtendimentoReceptivoModelView
    {
        public int Codigo { get; set; }
        public int CodigoMidia { get; set; }
        public string NomeArquivo { get; set; }
        public DateTime DataRegistro { get; set; }
        public string DataView { get; set; }
        public string Descricao { get; set; }
    }
}