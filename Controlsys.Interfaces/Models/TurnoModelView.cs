﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Controlsys.Dominio.Parametros;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// Representa um(a) TurnoModelView.
    /// </summary>
    public class TurnoModelView
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Nome.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Nome.
        /// </value>
        public string Nome { get; set; }

        /// <summary>
        /// Gets/Sets valor para ObedeceEscala.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) ObedeceEscala.
        /// </value>
        public bool ObedeceEscala { get; set; }

        /// <summary>
        /// Gets/Sets valor para Tolerancia.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public string Tolerancia { get; set; }

        /// <summary>
        /// Gets/Sets valor para ConsideraFeriado.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) ConsideraFeriado.
        /// </value>
        public bool ConsideraFeriado { get; set; }

        /// <summary>
        /// Gets/Sets a situação do objetivo do tipo TurnoModelView.
        /// </summary>
        ///
        /// <value>
        /// Retorna true or false.
        /// </value>
        public bool Ativo { get; set; }

        /// <summary>
        /// Gets/Sets valor para HorasTurno.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) HorasTurno.
        /// </value>
        public List<HoraTurnoModelView> HorasTurno { get; set; }

        /// <summary>
        /// Gets/Sets valor para FeriadosTurno.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) FeriadosTurno.
        /// </value>
        public List<FeriadoTurnoModelView> FeriadosTurno { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Models.TurnoModelView.
        /// </summary>
        public TurnoModelView()
        {
            HorasTurno = new List<HoraTurnoModelView>();
            FeriadosTurno = new List<FeriadoTurnoModelView>();
        }
    }
}