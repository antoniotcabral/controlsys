﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    public class SolicitacaoMaterialModelView
    {
        public int CodigoSolicitacaoMaterial { get; set; }
        public string Contratada { get; set; }
        public string TipoSolicitacao { get; set; }
        public string DataEntradaSaída { get; set; }
        public string DataPrevisaoRetorno { get; set; }
        public string ColaboradorResponsavel { get; set; }
        public string Destino { get; set; }
        public string GestorResponsavel { get; set; }
        public string Responsavel { get; set; }
        public string Modalidade { get; set; }
        public string Motivo { get; set; }
        public string PedidoCompra { get; set; }
        public string PlacaVeiculo { get; set; }
        public string Portador { get; set; }
        public string SetorCusto { get; set; }
        public string Solicitante { get; set; }
        public string SolicitanteCargo { get; set; }
        public string DataRegistro { get; set; }

        public string StatusAtual { get; set; }
        public string DataAguardandoAprovacao { get; set; }
        public string DataAprovacaoReprovacao { get; set; }
        public string DataRegistrado { get; set; }
        public string RegistradoPor { get; set; }

        public int CodigoTipoSolicitacao { get; set; }
        public int? CodigoModalidade { get; set; }
        public string CodigoContratada { get; set; }
        public string CodigoSubcontratada { get; set; }
        public int CodigoSetorCusto { get; set; }
        public int? CodigoPedidoCompra { get; set; }
        public int? CodigoGestorResponsavel { get; set; }
        public int? CodigoColaboradorResponsavel { get; set; }



        public MidiaModelView[] Midias { get; set; }
        public MaterialModelView[] Materiais { get; set; }
        public StatusSolicitacaoMaterialModelVIew[] Status { get; set; }
    }
}