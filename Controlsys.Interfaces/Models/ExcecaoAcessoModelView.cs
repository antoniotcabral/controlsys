﻿using Controlsys.Dominio.Pessoas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// Representa um(a) ExcecaoAcessoModelView.
    /// </summary>
    public class ExcecaoAcessoModelView 
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para CriterioBloqueio.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CriterioBloqueio.
        /// </value>
        public CriterioBloqueio CriterioBloqueio { get; set; }

        /// <summary>
        /// Gets/Sets a situação do objetivo do tipo ExcecaoAcessoModelView.
        /// </summary>
        ///
        /// <value>
        /// Retorna true or false.
        /// </value>
        public bool Ativo { get; set; }

        /// <summary>
        /// Gets/Sets valor para DescricaoBloqueio.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DescricaoBloqueio.
        /// </value>
        public string DescricaoBloqueio { get; set; }

        /// <summary>
        /// Gets/Sets valor para CodigoCriterio.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CodigoCriterio.
        /// </value>
        public CriterioBloqueio CodigoCriterio { get; set; }

    }
}
