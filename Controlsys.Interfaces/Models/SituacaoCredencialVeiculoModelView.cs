﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    public class SituacaoCredencialVeiculoModelView
    {
        public int Codigo { get; set; }
        public DateTime DataRegistro { get; set; }
        public string Status { get; set; }
        public string Observacao { get; set; }
    }
}