﻿using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Pessoas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// Representa um(a) PessoaCriticaModelView.
    /// </summary>
    public class PessoaCriticaModelView
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para MotivoBloqueio.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) MotivoBloqueio.
        /// </value>
        public string MotivoBloqueio { get; set; }

        /// <summary>
        /// Gets/Sets valor para MotivoBloqueioCod.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) MotivoBloqueioCod.
        /// </value>
        public int MotivoBloqueioCod { get; set; }

        /// <summary>
        /// Gets/Sets valor para Responsavel.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Responsavel.
        /// </value>
        public string Responsavel { get; set; }

        /// <summary>
        /// Gets/Sets valor para ResponsavelEmpresa.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) ResponsavelEmpresa.
        /// </value>
        public string ResponsavelEmpresa { get; set; }

        /// <summary>
        /// Gets/Sets valor para ResponsavelFuncao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) ResponsavelFuncao.
        /// </value>
        public string ResponsavelFuncao { get; set; }

        /// <summary>
        /// Gets/Sets valor para PessoaFisica.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) PessoaFisica.
        /// </value>
        public PessoaFisica PessoaFisica { get; set; }

        /// <summary>
        /// Gets/Sets valor para Observacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Observacao.
        /// </value>
        public string Observacao { get; set; }

        /// <summary>
        /// Gets/Sets valor para CPF.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CPF.
        /// </value>
        public string CPF { get; set; }

        /// <summary>
        /// Gets/Sets valor para Passaporte.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Passaporte.
        /// </value>
        public string Passaporte { get; set; }

        /// <summary>
        /// Gets/Sets valor para Nome.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Nome.
        /// </value>
        public string Nome { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataBloqueio.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataBloqueio.
        /// </value>
        public DateTime DataBloqueio { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataInativacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataInativacao.
        /// </value>
        public DateTime DataInativacao { get; set; }

        /// <summary>
        /// Gets/Sets a situação do objetivo do tipo PessoaCriticaModelView.
        /// </summary>
        ///
        /// <value>
        /// Retorna true or false.
        /// </value>
        public bool Ativo { get; set; }

        /// <summary>
        /// Gets/Sets a situação do objetivo do tipo PessoaCriticaModelView.
        /// </summary>
        ///
        /// <value>
        /// Retorna true or false.
        /// </value>
        public List<MidiaModelView> Midias { get; set; }
    }
}
