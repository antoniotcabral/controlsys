﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Controlsys.Dominio.Parametros;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// Representa um(a) ChecklistDocumentoModelView.
    /// </summary>
    public class ChecklistDocumentoModelView
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Nome.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Nome.
        /// </value>
        public string Nome { get; set; }

        /// <summary>
        /// Gets/Sets valor para Descricao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Descricao.
        /// </value>
        public string Descricao { get; set; }
        //public TipoCadastro TipoCadastro { get; set; }

        /// <summary>
        /// Gets/Sets valor para TipoCadastro.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TipoCadastro.
        /// </value>
        public string TipoCadastro { get; set; }

        /// <summary>
        /// Gets/Sets a situação do objetivo do tipo ChecklistDocumentoModelView.
        /// </summary>
        ///
        /// <value>
        /// Retorna true or false.
        /// </value>
        public bool Ativo { get; set; }

        /// <summary>
        /// Gets/Sets valor para TiposDocumento.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TiposDocumento.
        /// </value>
        public List<TipoDocumentoModelView> TiposDocumento { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Models.ChecklistDocumentoModelView.
        /// </summary>
        public ChecklistDocumentoModelView()
        {
            TiposDocumento = new List<TipoDocumentoModelView>();
        }

        /// <summary>
        /// Obter checklist documento.
        /// </summary>
        ///
        /// <param name="modelView">
        /// O(a) model view.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ChecklistDocumento.
        /// </returns>
        public static ChecklistDocumento ObterChecklistDocumento(ChecklistDocumentoModelView modelView)
        {
            var value = new ChecklistDocumento();
            value.Codigo = modelView.Codigo;
            value.Nome = modelView.Nome;
            value.Descricao = modelView.Descricao;
            value.Ativo = modelView.Ativo;
            value.TipoCadastro = Global.GetValueFromDescription<TipoCadastro>(modelView.TipoCadastro);
            //value.TipoCadastro = modelView.TipoCadastro;
            return value;
        }
    }
}