﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// Representa um(a) Global.
    /// </summary>
    public class Global
    {
        /// <summary>
        /// Get value from description.
        /// </summary>
        ///
        /// <exception cref="InvalidOperationException">
        /// Lançada quando an operação requerida é inválida.
        /// </exception>
        /// <exception cref="ArgumentException">
        /// Lançada quando um ou mais argumentos tem valores não suportados ou ilegais.
        /// </exception>
        ///
        /// <typeparam name="T">
        /// Generic type parameter.
        /// </typeparam>
        /// <param name="description">
        /// The description.
        /// </param>
        ///
        /// <returns>
        /// Um(a) T.
        /// </returns>
        public static T GetValueFromDescription<T>(string description)
        {
            var type = typeof(T);
            if (!type.IsEnum) throw new InvalidOperationException();
            foreach (var field in type.GetFields())
            {
                var attribute = Attribute.GetCustomAttribute(field,
                    typeof(DescriptionAttribute)) as DescriptionAttribute;
                if (attribute != null)
                {
                    if (attribute.Description == description)
                        return (T)field.GetValue(null);
                }
                else
                {
                    if (field.Name == description)
                        return (T)field.GetValue(null);
                }
            }
            throw new ArgumentException("Not found.", "description");
            // or return default(T);
        }
    }
}