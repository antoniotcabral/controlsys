﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    public class CameraModelView
    {
        public int Codigo { get; set; }

        public string Nome { get; set; }

        public string Modelo { get; set; }        

        public string Descricao { get; set; }        

        public DateTime DataRegistro { get; set; }

        public bool Ativo { get; set; }

        public DateTime? DataStatus { get; set; }

        public string Precisao { get; set; }
    }
}