﻿using Controlsys.Dominio.Pessoas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.Interfaces.Models
{
    public class OcorrenciaTransitoModelView
    {
        public int CodigoOcorrencia { get; set; }
        public int CodigoPapel { get; set; }
        public string CodigoPessoa { get; set; }        
        public string Nome { get; set; }
        public string CPF { get; set; }
        public string Passaporte { get; set; }
        public string NumeroCracha { get; set; }
        public string TipoPapel { get; set; }
        public string Empresa { get; set; }
        public string Contratada { get; set; }
        public string DataOcorrencia { get; set; }
        public string HoraOcorrencia { get; set; }
        public string DataHoraOcorrencia { get; set; }
        public int CodigoLocal { get; set; }
        public int CodigoInfracao { get; set; }
        public string Local { get; set; }
        public string Infracao { get; set; }
        public string Gravidade { get; set; }
        public string Penalidade { get; set; }
        public int QuantidadeInfracoes { get; set; }
        public int QuantidadeInfracoesMesmaGravidade { get; set; }
        public int CodigoPenalidade { get; set; }
        public bool CompareceuNotificacao { get; set; }
        public DateTime? TreinamentoAgendado { get; set; }
        public bool? CompareceuTreinamento { get; set; }
        public bool? Apto { get; set; }
        public bool Ativo { get; set; }
        public bool Anulada { get; set; }
    }
}