﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Controlsys.Dominio.Pessoas;
using Org.BouncyCastle.Bcpg;

namespace Controlsys.Interfaces.Models
{
    public class RelatorioPermanenciaDiariaModelView
    {
        public string Cpf { get; set; }
        public string Passaporte { get; set; }
        public string Nome { get; set; }
        public string TipoPapel { get; set; }
        public string Empresa { get; set; }
        public string Cnpj { get; set; }
        public DateTime Tempo { get; set; }
        public TimeSpan TempoAux { get; set; }
    }
}