﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Controlsys.Infra;
using Globalsys;
using Controlsys.Interfaces.App_Start;
using Globalsys;
using System.Threading;

namespace Controlsys.Interfaces
{
    /// <summary>
    /// Note: For instructions on enabling IIS6 or IIS7 classic mode, visit
    /// http://go.microsoft.com/?LinkId=9394801.
    /// </summary>
    public class MvcApplication : System.Web.HttpApplication
    {
        /// <summary>
        /// Application start.
        /// </summary>
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            
            //Fabrica.Instancia.Kernel.Bind<IFilterProvider>().To<NinjectFilterAttribute>().InSingletonScope();

            //GlobalConfiguration.Configuration.DependencyResolver = new NinjectDependencyResolver(Fabrica.Instancia.Kernel);

            DependencyResolver.SetResolver(new NinjectDependencyResolver(Fabrica.Instancia.Kernel));

            //HibernatingRhinos.Profiler.Appender.NHibernate.NHibernateProfiler.Initialize();

            AlterarJsonValueProviderFactory();
        }

        /// <summary>
        /// Muda o JsonValueProviderFactory original para um customizado. 
        /// </summary>
        private static void AlterarJsonValueProviderFactory()
        {
            //find the default JsonVAlueProviderFactory
            JsonValueProviderFactory jsonValueProviderFactory = null;

            foreach (var factory in ValueProviderFactories.Factories)
            {
                if (factory is JsonValueProviderFactory)
                {
                    jsonValueProviderFactory = factory as JsonValueProviderFactory;
                }
            }

            //remove the default JsonVAlueProviderFactory
            if (jsonValueProviderFactory != null) ValueProviderFactories.Factories.Remove(jsonValueProviderFactory);

            //add the custom one
            ValueProviderFactories.Factories.Add(new CustomJsonValueProviderFactory());
        }


    }
}