﻿using Controlsys.Dominio.Parametros;
using Controlsys.Infra;
using Controlsys.Repositorio.Parametros;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{

    [ExtendController]
    public class TipoAlimentacaoController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /Acao/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.AcaoController.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public TipoAlimentacaoController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ObterTodos(bool apenasAtivos = false, string descricao = null, int restaurante = 0)
        {
            IRepositorioTipoAlimentacao repTipoA = Fabrica.Instancia.ObterRepositorio<IRepositorioTipoAlimentacao>(UnidadeTrabalho);

            IQueryable<TipoAlimentacao> tiposA = repTipoA.ObterTodos();

            if (apenasAtivos)
                tiposA = tiposA.Where(t => t.Ativo == true);

           
            if (!string.IsNullOrEmpty(descricao))
                tiposA = tiposA.Where(ta => ta.Nome.ToLower().Contains(descricao.ToLower()));

            if (restaurante != 0)
            {
                IRepositorioRestaurante repRest = Fabrica.Instancia.ObterRepositorio<IRepositorioRestaurante>(UnidadeTrabalho);
                var rest = repRest.ObterPorId(restaurante).TiposAlimentacao.Where(r => r.Ativo);
                return Json(rest.Select(a => new
                {
                    a.TipoAlimentacao.Codigo,
                    a.TipoAlimentacao.Nome
                }).ToList());
            }

            tiposA = tiposA.OrderBy(ta => ta.Nome);

            return Json(tiposA.Select(a => new
            {
                a.Codigo,
                a.Nome,
                a.Ativo
            })
            .ToList());
        }

        [Transaction]
        public ActionResult Incluir(TipoAlimentacao tipoA)
        {
            IRepositorioTipoAlimentacao repTipoA = Fabrica.Instancia.ObterRepositorio<IRepositorioTipoAlimentacao>(UnidadeTrabalho);
            tipoA.DataRegistro = DateTime.Now;
            tipoA.Ativar();
            repTipoA.Validar(tipoA, EstadoObjeto.Novo);
            repTipoA.Salvar(tipoA);
            return Json(tipoA);
        }

        [Transaction]
        public ActionResult Alterar(TipoAlimentacao tipoA)
        {
            IRepositorioTipoAlimentacao repTipoA = Fabrica.Instancia.ObterRepositorio<IRepositorioTipoAlimentacao>(UnidadeTrabalho);
            TipoAlimentacao tipoAlimentacao = repTipoA.ObterPorId(tipoA.Codigo);
            tipoAlimentacao.Nome = tipoA.Nome;
            repTipoA.Validar(tipoAlimentacao, EstadoObjeto.Alterado);
            repTipoA.Atualizar(tipoAlimentacao);
            return Json(tipoAlimentacao);
        }

        [Transaction]
        public ActionResult AtivarInativar(int codigo)
        {
            IRepositorioTipoAlimentacao repTipoA = Fabrica.Instancia.ObterRepositorio<IRepositorioTipoAlimentacao>(UnidadeTrabalho);
            TipoAlimentacao tipoA = repTipoA.ObterPorId(codigo);
            if (tipoA.Ativo)
            {
                repTipoA.Validar(tipoA, EstadoObjeto.Inativado);
                tipoA.Inativar();
            }
            else
            {
                repTipoA.Validar(tipoA, EstadoObjeto.Ativado);
                tipoA.Ativar();
            }

            repTipoA.Atualizar(tipoA);
            return Json(tipoA);
        }

        public ActionResult Selecionar(int codigo)
        {
            IRepositorioTipoAlimentacao repTipoA = Fabrica.Instancia.ObterRepositorio<IRepositorioTipoAlimentacao>(UnidadeTrabalho);
            TipoAlimentacao tipoA = repTipoA.ObterPorId(codigo);
            return Json(tipoA);
        }

    }
}
