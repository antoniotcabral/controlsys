﻿using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Pessoas;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Parametros;
using Controlsys.Repositorios.Empresas;
using Controlsys.Repositorios.Pessoas;
using Ext.Net;
using Ext.Net.MVC;
using Globalsys;
using Globalsys.Relatorios;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar relatorio cracha empresas.
    /// </summary>
    [ExtendController]
    public class RelatorioCrachaEmpresaController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /RelatorioCrachaEmpresa/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.RelatorioCrachaEmpresaController.
        /// </summary>
        ///
        /// <param name="unidade">
        /// O(a) unidade.
        /// </param>
        public RelatorioCrachaEmpresaController(IUnidadeTrabalho unidade) { UnidadeTrabalho = unidade; }

        /// <summary>
        /// Retorna a página de relatorioCrachaEmpresa.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Exportar.
        /// </summary>
        ///
        /// <param name="tipoSetor">
        /// O(a) tipo setor.
        /// </param>
        /// <param name="selectedFields">
        /// The selected fields.
        /// </param>
        /// <param name="tipoRelatorio">
        /// O(a) tipo relatorio.
        /// </param>
        /// <param name="empresa">
        /// O(a) empresa.
        /// </param>
        /// <param name="nome">
        /// O(a) nome.
        /// </param>
        /// <param name="cracha">
        /// O(a) cracha.
        /// </param>
        /// <param name="cpf">
        /// O(a) cpf.
        /// </param>
        /// <param name="passaporte">
        /// O(a) passaporte.
        /// </param>
        /// <param name="dataCadastro">
        /// O(a) data cadastro.
        /// </param>
        /// <param name="setor">
        /// O(a) setor.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Exportar(TipoSetorCusto? tipoSetor, List<string> selectedFields, string tipoRelatorio, string empresa = null, string contratada = null, string nome = null, string cracha = null, string cpf = null, string passaporte = null, DateTime? dataCadastro = null, int setor = 0, TipoPapel? papel = null)
        {
            var list = FiltrarCrachasEmpresa(tipoSetor, empresa, contratada, nome, cracha, cpf, passaporte, dataCadastro, setor, papel);
            var crachas = montaObj(list, null, true);

            ReportConfig reportConfig = ReportHelper.RenderReport<RelatorioCrachaEmpresaModelView>(selectedFields, tipoRelatorio, crachas);

            string fileName = string.Empty;

            switch (tipoRelatorio)
            {
                case "PDF":
                    fileName = "download.pdf";
                    break;
                case "Excel":
                    fileName = "download.xls";
                    break;
                case "Word":
                    fileName = "download.doc";
                    break;
            }

            return File(reportConfig.Bytes, reportConfig.MimeType, fileName);
        }

        /// <summary>
        /// Pesquisar.
        /// </summary>
        ///
        /// <param name="parameters">
        /// Options for controlling the operation.
        /// </param>
        /// <param name="empresa">
        /// O(a) empresa.
        /// </param>
        /// <param name="nome">
        /// O(a) nome.
        /// </param>
        /// <param name="cracha">
        /// O(a) cracha.
        /// </param>
        /// <param name="cpf">
        /// O(a) cpf.
        /// </param>
        /// <param name="passaporte">
        /// O(a) passaporte.
        /// </param>
        /// <param name="tipoSetor">
        /// O(a) tipo setor.
        /// </param>
        /// <param name="setor">
        /// O(a) setor.
        /// </param>
        /// <param name="dataCadastro">
        /// O(a) data cadastro.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Pesquisar(StoreRequestParameters parameters, string empresa = null, string contratada = null, string nome = null, string cracha = null, string cpf = null, string passaporte = null, TipoSetorCusto? tipoSetor = null, int setor = 0, /*string setorCusto = null,*/ DateTime? dataCadastro = null, TipoPapel? papel = null, string fornecedor = null)
        {
            IQueryable<RelatorioCrachaEmpresaAuxModelView> regs = FiltrarCrachasEmpresa(tipoSetor, empresa, contratada, nome, cracha, cpf, passaporte, dataCadastro, setor, papel, fornecedor);

            int total = regs.Count();
            List<RelatorioCrachaEmpresaModelView> data = montaObj(regs, parameters);

            return new StoreResult(data, total);
        }

        /// <summary>
        /// Filtrar crachas empresa.
        /// </summary>
        ///
        /// <param name="tipoSetor">
        /// O(a) tipo setor.
        /// </param>
        /// <param name="empresa">
        /// O(a) empresa.
        /// </param>
        /// <param name="nome">
        /// O(a) nome.
        /// </param>
        /// <param name="cracha">
        /// O(a) cracha.
        /// </param>
        /// <param name="cpf">
        /// O(a) cpf.
        /// </param>
        /// <param name="passaporte">
        /// O(a) passaporte.
        /// </param>
        /// <param name="dataCadastro">
        /// O(a) data cadastro.
        /// </param>
        /// <param name="setor">
        /// O(a) setor.
        /// </param>
        ///
        /// <returns>
        /// Um(a) IQueryable&lt;RelatorioCrachaEmpresaAuxModelView&gt;
        /// </returns>
        private IQueryable<RelatorioCrachaEmpresaAuxModelView> FiltrarCrachasEmpresa(TipoSetorCusto? tipoSetor, string empresa, string contratada, string nome, string cracha, string cpf, string passaporte, DateTime? dataCadastro, int setor = 0, TipoPapel? papel = null, string fornecedor = null)
        {
            IRepositorioCracha rep = Fabrica.Instancia.ObterRepositorio<IRepositorioCracha>(UnidadeTrabalho);
            IRepositorioAlocacaoColaborador repAlocacao = Fabrica.Instancia.ObterRepositorio<IRepositorioAlocacaoColaborador>(UnidadeTrabalho);
            IRepositorioPrestadorServico repPrest = Fabrica.Instancia.ObterRepositorio<IRepositorioPrestadorServico>(UnidadeTrabalho);
            IRepositorioColaborador repColaborador = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);
            IRepositorioVisitante repVisitante = Fabrica.Instancia.ObterRepositorio<IRepositorioVisitante>(UnidadeTrabalho);

            if (!string.IsNullOrEmpty(cpf))
                cpf = cpf.Replace(".", "").Replace("-", "");

            IQueryable<Cracha> listCracha = rep.ObterTodos().Where(t => t.Ativo && t.Papel.PapelLogs.OrderByDescending(pl => pl.Codigo)
                                                                         .Select(pl => pl.Status)
                                                                         .FirstOrDefault() != StatusPapelLog.BaixaDefinitiva);
            IQueryable<AlocacaoColaborador> aloc = repAlocacao.ObterTodos();

            if (!string.IsNullOrEmpty(nome))
                listCracha = listCracha.Where(p => p.Papel.PessoaFisica.Nome.Contains(nome));

            if (!string.IsNullOrEmpty(cracha))
                listCracha = listCracha.Where(p => p.Numero == cracha);

            if (!string.IsNullOrEmpty(cpf))
                listCracha = listCracha.Where(p => p.Papel.PessoaFisica.CPF == cpf);

            if (!string.IsNullOrEmpty(passaporte))
                listCracha = listCracha.Where(p => p.Papel.PessoaFisica.Passaporte == passaporte);

            //if (setorCusto != 0)
            //{
            //    aloc = aloc.Where(p => p.SetorCusto.Codigo == setorCusto);
            //    listCracha = listCracha.Where(k => aloc.Any(a => a.Papel.Codigo == k.Papel.Codigo));
            //}

            if (dataCadastro != null)
                listCracha = listCracha.Where(l => l.DataRegistro.Date == dataCadastro);

            if (tipoSetor != null || setor != 0)
            {
                if (tipoSetor != null)
                    aloc = aloc.Where(p => p.SetorCusto.TipoSetor == tipoSetor);

                if (setor != 0)
                    aloc = aloc.Where(p => p.SetorCusto.Codigo == setor);

                listCracha = listCracha.Where(k => aloc.Any(a => a.Papel.Codigo == k.Papel.Codigo));
            }

            if (papel.HasValue)
            {
                switch (papel.Value)
                {
                    case TipoPapel.Visitante:
                        listCracha = listCracha.Where(x => x.Papel is Visitante);
                        break;
                    case TipoPapel.Colaborador:
                        listCracha = listCracha.Where(x => x.Papel is Colaborador);
                        break;
                    case TipoPapel.PrestadorServico:
                        listCracha = listCracha.Where(x => x.Papel is PrestadorServico);
                        break;
                    default:
                        break;
                }
            }

            if (!string.IsNullOrEmpty(empresa) || !string.IsNullOrEmpty(contratada) || !string.IsNullOrEmpty(fornecedor))
            {
                List<int> codigosPapel = new List<int>();

                if (!string.IsNullOrEmpty(empresa))
                {
                    codigosPapel = codigosPapel.Union(repColaborador.ObterTodos().Where(p => p.Empresa.Nome == empresa).Select(c => c.Codigo)).ToList();
                    codigosPapel = codigosPapel.Union(repPrest.ObterTodos().Where(p => p.Empresa.Nome == empresa).Select(c => c.Codigo)).ToList();
                    codigosPapel = codigosPapel.Union(repVisitante.ObterTodos().Where(p => p.Empresa == empresa).Select(c => c.Codigo)).ToList();
                }
                
                if (!string.IsNullOrEmpty(fornecedor))
                {                    
                    codigosPapel = codigosPapel.Union(repPrest.ObterTodos().Where(p => p.Fornecedor.Codigo == Convert.ToInt32(fornecedor)).Select(c => c.Codigo)).ToList();                    
                }

                if (!string.IsNullOrEmpty(contratada))
                {
                    var xxx = aloc.Where(a => a.Ativo).Select(a => a.ObterContratada()).ToArray().Where(pe => pe.Empresa.Nome == contratada);
                    var listaContratadas = aloc.Where(a => a.Ativo).ToArray().Where(a => xxx.Contains(a.ObterContratada())).ToArray();
                    List<int> papeis = listaContratadas.Select(ac => ac.Papel.Codigo).ToList();

                    if (string.IsNullOrEmpty(empresa))
                        codigosPapel = papeis;
                    else
                        codigosPapel = codigosPapel.Intersect(papeis).ToList();
                }

                int qtdcolab = codigosPapel.Count();

                List<Cracha> _listaCracha = new List<Cracha>();
                string s = string.Empty;
                foreach (int i in codigosPapel)
                {
                    s = s + i.ToString() + ", ";
                }

                for (int i = 0; i < qtdcolab; i += 2000)
                {
                    var page = codigosPapel.Skip(i).Take(2000).ToArray();
                    _listaCracha.AddRange(listCracha.Where(c => page.Contains(c.Papel.Codigo)));
                }

                listCracha = _listaCracha.AsQueryable();
            }

            var listaCracha = listCracha.Select(c => new RelatorioCrachaEmpresaAuxModelView
            {
                Cracha = c,
                Alocacao = aloc.Where(a => a.Ativo && a.Papel.Codigo == c.Papel.Codigo).OrderByDescending(ac => ac.DataRegistro).FirstOrDefault()
            });

            return listaCracha;
        }

        /// <summary>
        /// Cria um objeto contendo informações relacionadas ao conteudo gerenciado por
        /// RelatorioCrachaEmpresaController.
        /// </summary>
        ///
        /// <param name="regs">
        /// O(a) regs.
        /// </param>
        /// <param name="parameters">
        /// Options for controlling the operation.
        /// </param>
        /// <param name="ignoreParameters">
        /// true to ignore parameters.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;RelatorioCrachaEmpresaModelView&gt;
        /// </returns>
        private List<RelatorioCrachaEmpresaModelView> montaObj(IQueryable<RelatorioCrachaEmpresaAuxModelView> regs, StoreRequestParameters parameters = null, bool ignoreParameters = false)
        {
            if (!ignoreParameters)
            {
                if (parameters == null)
                    parameters = new StoreRequestParameters();
                regs = regs.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 25);
            }


            List<RelatorioCrachaEmpresaModelView> lista = new List<RelatorioCrachaEmpresaModelView>();

            foreach (var s in regs.ToList())
            {
                PedidoCompra alocada = s.Alocacao != null ? s.Alocacao.ObterContratada() : null;

                var item = new RelatorioCrachaEmpresaModelView
                {
                    Empresa = s.Cracha.Papel is Colaborador ? (s.Cracha.Papel as Colaborador).Empresa.Apelido :
                                                             (s.Cracha.Papel is PrestadorServico) ? (s.Cracha.Papel as PrestadorServico).TipoPrestador == TipoPrestador.Temporario ? (s.Cracha.Papel as PrestadorServico).Empresa.Apelido : (s.Cracha.Papel as PrestadorServico).Fornecedor.NomeFantasia :
                                                             (s.Cracha.Papel as Visitante).Empresa,
                    Papel = s.Cracha.Papel != null ? (s.Cracha.Papel is Colaborador) ? "Colaborador" : (s.Cracha.Papel is PrestadorServico) ? "Prestador de Serviço" : "Visitante" : "",
                    Cracha = s.Cracha.Numero,
                    CPF = s.Cracha.Papel.PessoaFisica.CPFFormatado(),
                    Passaporte = s.Cracha.Papel.PessoaFisica.Passaporte,
                    Nome = s.Cracha.Papel.PessoaFisica.Nome,
                    //Funcao = (s.Cracha.Papel != null) ? s.Cracha.Papel.Cargo != null ? s.Cracha.Papel.Cargo.Nome : "" : "",
                    Funcao = (s.Cracha.Papel is Colaborador) ? (s.Cracha.Papel as Colaborador).Cargo != null ? s.Cracha.Papel.Cargo.Nome : "" :
                                                             (s.Cracha.Papel is PrestadorServico) ? (s.Cracha.Papel as PrestadorServico).Cargo != null ? s.Cracha.Papel.Cargo.Nome : "" :
                                                             (s.Cracha.Papel as Visitante).Funcao,
                    //DataVencimentoASO = s.Cracha.Papel.ASOs.OrderByDescending(f => f.DataRealizacao).Select(f => f.DataVencimento).FirstOrDefault(),
                    DataVencimentoASO = s.Cracha.Papel.ASOs.Count > 0 ? s.Cracha.Papel.ASOs.OrderByDescending(f => f.DataRealizacao).Select(f => f.DataVencimento).FirstOrDefault() : (DateTime?)null,
                    TipoSetor = s.Alocacao != null ? s.Alocacao.SetorCusto != null ? s.Alocacao.SetorCusto.TipoSetor.ObterDescricaoEnum() : "" : "",
                    Setor = s.Alocacao != null ? (s.Alocacao.SetorCusto != null ? s.Alocacao.SetorCusto.Nome : "") : "",
                    DataCadastro = s.Cracha.DataRegistro
                };

                if (alocada != null)
                {
                    if (alocada.Numero == s.Alocacao.PedidoCompra.Numero)
                    {
                        item.Contratada = "";// s.Alocacao.PedidoCompra.Empresa.Nome;
                        //item.Subcontratada = "";
                    }
                    else
                    {
                        item.Contratada = alocada.Empresa.Nome;
                        //item.Subcontratada = s.Alocacao.PedidoCompra.Empresa.Nome;
                    }
                }

                lista.Add(item);

            }
            return lista;
        }
    }



}
