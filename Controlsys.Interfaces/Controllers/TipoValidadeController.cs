﻿using System.Diagnostics.CodeAnalysis;
using Controlsys.Dominio.Parametros;
using Controlsys.Infra;
using Controlsys.Repositorio.Parametros;
using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Globalsys.Validacao;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class TipoValidadeController : Controller, IControllerBase
    {

        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        public TipoValidadeController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        [ExcludeFromCodeCoverage]
        public ActionResult Index()
        {
            return View();
        }

        [Transaction]
        public ActionResult Incluir(TipoValidade tipoValidade)
        {
            IRepositorioTipoValidade repTipoValidade = Fabrica.Instancia.ObterRepositorio<IRepositorioTipoValidade>(UnidadeTrabalho);
            tipoValidade.DataRegistro = DateTime.Now;
            tipoValidade.Ativar();
            repTipoValidade.Validar(tipoValidade, EstadoObjeto.Novo);
            repTipoValidade.Salvar(tipoValidade);
            return Json(tipoValidade);
        }

        [Transaction]
        public ActionResult Alterar(TipoValidade tipoValidade)
        {
            IRepositorioTipoValidade repTipoValidade = Fabrica.Instancia.ObterRepositorio<IRepositorioTipoValidade>(UnidadeTrabalho);
            TipoValidade tipoValidadeA = repTipoValidade.ObterPorId(tipoValidade.Codigo);
            tipoValidadeA.Nome = tipoValidade.Nome;
            tipoValidadeA.DiasValidade = tipoValidade.DiasValidade;
            tipoValidadeA.IdadeInicial = tipoValidade.IdadeInicial;
            tipoValidadeA.IdadeFinal = tipoValidade.IdadeFinal;
            repTipoValidade.Validar(tipoValidadeA, EstadoObjeto.Alterado);
            repTipoValidade.Atualizar(tipoValidadeA);
            return Json(tipoValidadeA);
        }

        [Transaction]
        public ActionResult AtivarInativar(int codigo)
        {
            IRepositorioTipoValidade repTipoValidade = Fabrica.Instancia.ObterRepositorio<IRepositorioTipoValidade>(UnidadeTrabalho);
            TipoValidade tipoValidade = repTipoValidade.ObterPorId(codigo);
            if (tipoValidade.Ativo)
            {
                repTipoValidade.Validar(tipoValidade,EstadoObjeto.Inativado);
                tipoValidade.Inativar();
            }
            else
            {
                repTipoValidade.Validar(tipoValidade, EstadoObjeto.Ativado);
                tipoValidade.Ativar();
            }
            repTipoValidade.Atualizar(tipoValidade);
            return Json(tipoValidade);
        }

        public ActionResult Selecionar(int codigo)
        {
            IRepositorioTipoValidade repTipoValidade = Fabrica.Instancia.ObterRepositorio<IRepositorioTipoValidade>(UnidadeTrabalho);
            TipoValidade tipoValidade = repTipoValidade.ObterPorId(codigo);
            return Json(tipoValidade);
        }

        public ActionResult ObterTodos(bool apenasAtivos = false)
        {
            IRepositorioTipoValidade repTipoValidade = Fabrica.Instancia.ObterRepositorio<IRepositorioTipoValidade>(UnidadeTrabalho);

            IQueryable<TipoValidade> locais = repTipoValidade.ObterTodos();

            if (apenasAtivos)
                locais = locais.Where(t => t.Ativo == true);

            return Json(locais.Select(l => new
            {
                l.Codigo,
                l.Nome,
                l.DiasValidade,
                l.IdadeInicial,
                l.IdadeFinal,
                l.Ativo,
            })
            .ToList().OrderBy(l => l.Nome));
        }

    }
}
