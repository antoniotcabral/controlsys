﻿using Controlsys.Dominio.Ocorrencias;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Controlsys.Infra;
using Controlsys.Repositorio.Ocorrencias;
using Controlsys.Repositorio.Parametros;
using Globalsys;
using Globalsys.Exceptions;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class RelatorioGraficosOcorrenciaTransitoController : Controller, IControllerBase
    {
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        public RelatorioGraficosOcorrenciaTransitoController(IUnidadeTrabalho unidade) { UnidadeTrabalho = unidade; }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Exportar(string tipoRelatorio, string tipoGrafico, DateTime? dtInicio, DateTime? dtFim, string empresa = null, string cpfPassaporte = null)
        {
            tipoGrafico = tipoGrafico.ToUpper();
            if (tipoGrafico != "INFRACOESCLASSIFICACAO" && tipoGrafico != "INFRACOESPORPENALIDADE" && tipoGrafico != "RESULTADOSTREINAMENTO" && tipoGrafico != "INFRACOESPORPESSOA" && tipoGrafico != "EMPRESASMAISNOTIFICACOES")
                throw new CoreException("Não foi possível identificar o tipo de relatório informado.");

            var resultado = Filtrar(tipoGrafico, dtInicio, dtFim, empresa, cpfPassaporte);

            LocalReport relatorio = new LocalReport();
            relatorio.DataSources.Add(new ReportDataSource("DTOGraficosOcorrenciaTransito", resultado));
            switch (tipoGrafico)
            {
                case "INFRACOESCLASSIFICACAO":
                    relatorio.ReportPath = Server.MapPath("~/Reports/GraficoInfracoesPorClassificacao.rdlc");
                    break;
                case "INFRACOESPORPENALIDADE":
                    relatorio.ReportPath = Server.MapPath("~/Reports/GraficoInfracoesPorPenalidade.rdlc");
                    break;
                case "RESULTADOSTREINAMENTO":
                    relatorio.ReportPath = Server.MapPath("~/Reports/GraficoResultadoDosTreinamentos.rdlc");
                    break;
                case "INFRACOESPORPESSOA":
                    relatorio.ReportPath = Server.MapPath("~/Reports/GraficoInfracoesPorPessoa.rdlc");
                    break;
                case "EMPRESASMAISNOTIFICACOES":
                    relatorio.ReportPath = Server.MapPath("~/Reports/GraficoEmpresaMaisNotificacoes.rdlc");
                    break;
                default:
                    break;
            }
            relatorio.DataSources.Add(new ReportDataSource("DTOGraficoOcorrenciaSeguranca", resultado));
            //relatorio.SetParameters(new ReportParameter("Empresa", empresa));
            //relatorio.SetParameters(new ReportParameter("CpfPassaporte", cpfPassaporte));
            //relatorio.SetParameters(new ReportParameter("DataInicio", dtInicio.ToString()));
            //relatorio.SetParameters(new ReportParameter("DataFinal", dtFim.ToString()));

            string mimeType;
            string fileName = string.Empty;
            switch (tipoRelatorio)
            {
                case "PDF":
                    mimeType = "application/pdf";
                    fileName = "download.pdf";
                    break;
                case "Excel":
                    mimeType = "application/vnd.ms-excel";
                    fileName = "download.xls";
                    break;
                case "Word":
                    mimeType = "application/msword";
                    fileName = "download.doc";
                    break;
            }

            string encoding;
            string fileNameExtension;

            Warning[] warnings;
            string[] streams;
            byte[] bytes;

            bytes = relatorio.Render(
            tipoRelatorio,
            null,
            out mimeType,
            out encoding,
            out fileNameExtension,
            out streams,
            out warnings);

            return File(bytes, mimeType, fileName);
        }

        private object Filtrar(string tipoGrafico, DateTime? dtInicio, DateTime? dtFim, string empresa = null, string cpfPassaporte = null)
        {

            IQueryable<OcorrenciaTransito> ocorrencias = Fabrica.Instancia.ObterRepositorio<IRepositorioOcorrenciaTransito>(UnidadeTrabalho).ObterTodos();

            ocorrencias = ocorrencias.Where(o => !o.Anulada);

            if (dtInicio.HasValue)
            {
                ocorrencias = ocorrencias.Where(o => o.DataOcorrencia >= dtInicio.Value);
            }

            if (dtFim.HasValue)
            {
                ocorrencias = ocorrencias.Where(o => o.DataOcorrencia <= dtFim.Value);
            }

            if (!string.IsNullOrEmpty(empresa))
            {
                var papeis = ocorrencias.Select(o => o.Papel);
                var auxCodPapeis = new List<int>();
                empresa = empresa.ToUpper();

                foreach (var item in papeis)
                {
                    if (item is Colaborador)
                        if ((item as Colaborador).Empresa != null && (item as Colaborador).Empresa.Nome.ToUpper().Contains(empresa))
                            auxCodPapeis.Add(item.Codigo);

                    if (item is PrestadorServico)
                    {
                        if ((item as PrestadorServico).TipoPrestador == TipoPrestador.Temporario)
                        {
                            if ((item as PrestadorServico).Empresa != null && (item as PrestadorServico).Empresa.Nome.ToUpper().Contains(empresa))
                                auxCodPapeis.Add(item.Codigo);
                        }
                        else
                        {
                            if ((item as PrestadorServico).Fornecedor != null && (item as PrestadorServico).Fornecedor.RazaoSocial.ToUpper().Contains(empresa))
                                auxCodPapeis.Add(item.Codigo);
                        }
                    }

                    if (item is Visitante)
                        if ((item as Visitante).Empresa != null && (item as Visitante).Empresa.ToUpper().Contains(empresa))
                            auxCodPapeis.Add(item.Codigo);

                }

                ocorrencias = ocorrencias.Where(o => auxCodPapeis.Contains(o.Papel.Codigo));
            }

            if (!string.IsNullOrEmpty(cpfPassaporte))
            {
                string cpfpassaporteformatado = cpfPassaporte.Replace("-", string.Empty).Replace(".", string.Empty).Replace("/", string.Empty);
                ocorrencias = ocorrencias.Where(o => (o.PessoaFisica.CPF != null && o.PessoaFisica.CPF.Contains(cpfpassaporteformatado)) || (o.PessoaFisica.Passaporte != null && o.PessoaFisica.Passaporte.Contains(cpfpassaporteformatado)));
            }

            Dictionary<string, int> valores = new Dictionary<string, int>();

            switch (tipoGrafico)
            {
                case "INFRACOESCLASSIFICACAO":

                    foreach (GrauInfracao item in Enum.GetValues(typeof(GrauInfracao)))
                    {
                        IQueryable<OcorrenciaTransito> ocorrenciasAux = ocorrencias.Where(o => o.Infracao.Grau == item);
                        string enumNome = string.Empty;
                        if (item == GrauInfracao.Leve)
                        {
                            enumNome = "01#" + item.ObterDescricaoEnum();
                        }
                        else if (item == GrauInfracao.Media)
                        {
                            enumNome = "02#" + item.ObterDescricaoEnum();
                        }
                        else if (item == GrauInfracao.Grave)
                        {
                            enumNome = "03#" + item.ObterDescricaoEnum();
                        }
                        else if (item == GrauInfracao.Gravissima)
                        {
                            enumNome = "04#" + item.ObterDescricaoEnum();
                        }
                        else 
                        {
                            enumNome = "05#" + item.ObterDescricaoEnum();
                        }
                        valores.Add(enumNome, ocorrenciasAux.Count());
                    }

                    break;
                case "INFRACOESPORPENALIDADE":

                    IQueryable<Penalidade> penalidades = Fabrica.Instancia.ObterRepositorio<IRepositorioPenalidade>(UnidadeTrabalho).ObterTodos();
                    penalidades = penalidades.Where(i => i.Ativo);

                    foreach (Penalidade item in penalidades)
                    {
                        IQueryable<OcorrenciaTransito> ocorrenciasAux = ocorrencias.Where(o => o.Penalidade.Codigo == item.Codigo);

                        valores.Add(item.Sigla, ocorrenciasAux.Count());
                    }

                    break;
                case "RESULTADOSTREINAMENTO":
                    List<OcorrenciaTransito> ocorrenciasAuxSim = ocorrencias.ToList().Where(o => o.Treinamentos[0].Apto.HasValue && o.Treinamentos[0].Apto.Value).ToList();
                    List<OcorrenciaTransito> ocorrenciasAuxNao = ocorrencias.ToList().Where(o => o.Treinamentos[0].Apto.HasValue && !o.Treinamentos[0].Apto.Value).ToList();

                    valores.Add("Apto", ocorrenciasAuxSim.Count());
                    valores.Add("Inapto", ocorrenciasAuxNao.Count());

                    break;
                case "INFRACOESPORPESSOA":

                    foreach (OcorrenciaTransito item in ocorrencias)
                    {
                        if (!valores.Any(v => v.Key.Contains(item.PessoaFisica.Nome)))
                            valores.Add(item.PessoaFisica.Nome, 1);
                        else
                            valores[item.PessoaFisica.Nome] += 1;
                    }

                    break;
                case "EMPRESASMAISNOTIFICACOES":
                    List<string> empresas = new List<string>();

                    foreach (var item in ocorrencias.Select(o => o.Papel))
                    {
                        if (item is Colaborador)
                            if (!valores.Any(v => v.Key.Contains((item as Colaborador).Empresa.Nome)))
                                valores.Add((item as Colaborador).Empresa.Nome, 1);
                            else
                                valores[(item as Colaborador).Empresa.Nome] += 1;
                        else if (item is PrestadorServico && (item as PrestadorServico).Empresa != null)
                            if (!valores.Any(v => v.Key.Contains((item as PrestadorServico).Empresa.Nome)))
                                valores.Add((item as PrestadorServico).Empresa.Nome, 1);
                            else
                                valores[(item as PrestadorServico).Empresa.Nome] += 1;
                        else if (item is Visitante)
                            if (!valores.Any(v => v.Key.Contains((item as Visitante).Empresa)))
                                valores.Add((item as Visitante).Empresa, 1);
                            else
                                valores[(item as Visitante).Empresa] += 1;
                    }
                    break;
                default:
                    break;
            }

            return valores.Select(v => new { Nome = v.Key, Valor = v.Value }).OrderByDescending(v => v.Valor);
        }

    }
}
