﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Controlsys.Dominio.Empresas;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorios.Empresas;
using Ext.Net;
using Ext.Net.MVC;
using Globalsys;
using Globalsys.Relatorios;
using Controlsys.Repositorios.Parametros;
using System.Globalization;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar relatorio empresas.
    /// </summary>
    [ExtendController]
    public class RelatorioEmpresasController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /RelatorioEmpresas/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.RelatorioEmpresasController.
        /// </summary>
        ///
        /// <param name="unidade">
        /// O(a) unidade.
        /// </param>
        public RelatorioEmpresasController(IUnidadeTrabalho unidade) { UnidadeTrabalho = unidade; }

        /// <summary>
        /// Retorna a página de relatorioEmpresas.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Exportar.
        /// </summary>
        ///
        /// <param name="tipoRelatorio">
        /// O(a) tipo relatorio.
        /// </param>
        /// <param name="selectedFields">
        /// The selected fields.
        /// </param>
        /// <param name="razaoSocial">
        /// O(a) razao social.
        /// </param>
        /// <param name="nomeFantasia">
        /// O(a) nome fantasia.
        /// </param>
        /// <param name="cnpj">
        /// O(a) cnpj.
        /// </param>
        /// <param name="status">
        /// O(a) status.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Exportar(string tipoRelatorio, List<string> selectedFields, string razaoSocial, string nomeFantasia, string cnpj, bool? status, int setor = 0, int area = 0)
        {
            IQueryable<Empresa> empresas = FiltrarEmpresas(razaoSocial, nomeFantasia, cnpj, status, setor, area);

            List<RelatorioEmpresaModelView> lista = new List<RelatorioEmpresaModelView>();
            foreach (var item in empresas)
            {
                RelatorioEmpresaModelView rel = new RelatorioEmpresaModelView();
                rel.Codigo = item.Codigo;
                rel.Nome = item.Nome;
                rel.Apelido = item.Apelido;

                rel.CNPJ = item.CNPJ;
                rel.InscEstadual = item.InscEstadual;
                rel.InscMunicipal = item.InscMunicipal;
                rel.Email = item.Email;
                rel.Status = item.Ativo == true ? "Ativo" : "Inativo";

                rel.TelefoneCelular = item.Telefones.Where(p => p.TipoTelefone == Dominio.Pessoas.TipoTelefone.Celular).Select(x => x.TelefoneNum).FirstOrDefault();
                rel.TelefoneEmergencial = item.Telefones.Where(p => p.TipoTelefone == Dominio.Pessoas.TipoTelefone.Celular).Select(x => x.TelefoneNum).FirstOrDefault();
                rel.TelefoneResidencial = item.Telefones.Where(p => p.TipoTelefone == Dominio.Pessoas.TipoTelefone.Residencial).Select(x => x.TelefoneNum).FirstOrDefault();

                if (item.Endereco != null)
                {
                    rel.Complemento = item.Endereco.Complemento;
                    rel.Numero = item.Endereco.Numero;
                    if (rel.Logradouro != null)
                    {
                        rel.Logradouro = item.Endereco.Logradouro.Nome;
                        rel.Bairro = item.Endereco.Logradouro.Bairro.Nome;
                        rel.Cidade = item.Endereco.Logradouro.Bairro.Cidade.Nome;
                        rel.Estado = item.Endereco.Logradouro.Bairro.Cidade.Estado.Nome;
                        rel.Pais = item.Endereco.Logradouro.Bairro.Cidade.Estado.Pais.Nome;
                    }
                }
                rel.DataRegistro = item.DataRegistro;
                rel.DataDesativacao = item.DataDesativacao.HasValue ? item.DataDesativacao.Value.ToString(new CultureInfo("pt-BR")) : string.Empty;
                lista.Add(rel);
            }

            ReportConfig reportConfig = ReportHelper.RenderReport<RelatorioEmpresaModelView>(selectedFields, tipoRelatorio, lista);

            string fileName = string.Empty;

            switch (tipoRelatorio)
            {
                case "PDF":
                    fileName = "download.pdf";
                    break;
                case "Excel":
                    fileName = "download.xls";
                    break;
                case "Word":
                    fileName = "download.doc";
                    break;
            }

            return File(reportConfig.Bytes, reportConfig.MimeType, fileName);
        }

        /// <summary>
        /// Pesquisar.
        /// </summary>
        ///
        /// <param name="parameters">
        /// Options for controlling the operation.
        /// </param>
        /// <param name="razaoSocial">
        /// O(a) razao social.
        /// </param>
        /// <param name="nomeFantasia">
        /// O(a) nome fantasia.
        /// </param>
        /// <param name="cnpj">
        /// O(a) cnpj.
        /// </param>
        /// <param name="status">
        /// O(a) status.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Pesquisar(StoreRequestParameters parameters, string razaoSocial, string nomeFantasia, string cnpj, bool? status, int setor = 0, int area = 0)
        {
            IQueryable<Empresa> emps = FiltrarEmpresas(razaoSocial, nomeFantasia, cnpj, status, setor, area);

            int total = emps.Count();
            List<RelatorioEmpresaModelView> data = montaObj(emps, parameters);

            return new StoreResult(data, total);
        }

        /// <summary>
        /// Filtrar empresas.
        /// </summary>
        ///
        /// <param name="razaoSocial">
        /// O(a) razao social.
        /// </param>
        /// <param name="nomeFantasia">
        /// O(a) nome fantasia.
        /// </param>
        /// <param name="cnpj">
        /// O(a) cnpj.
        /// </param>
        /// <param name="status">
        /// O(a) status.
        /// </param>
        ///
        /// <returns>
        /// Um(a) IQueryable&lt;Empresa&gt;
        /// </returns>
        private IQueryable<Empresa> FiltrarEmpresas(string razaoSocial, string nomeFantasia, string cnpj, bool? status, int setor = 0, int area = 0)
        {
            IRepositorioEmpresa rep = Fabrica.Instancia.ObterRepositorio<IRepositorioEmpresa>(UnidadeTrabalho);
            IRepositorioSetorCustoPedido repSetorCusto = Fabrica.Instancia.ObterRepositorio<IRepositorioSetorCustoPedido>(UnidadeTrabalho);
            IRepositorioPedidoCompra repPedido = Fabrica.Instancia.ObterRepositorio<IRepositorioPedidoCompra>(UnidadeTrabalho);

            IQueryable<Empresa> emps = rep.ObterTodos();
            IQueryable<SetorCustoPedido> setoresCusto = repSetorCusto.ObterTodos();
            IQueryable<PedidoCompra> pedidos = repPedido.ObterTodos();


            if (!string.IsNullOrWhiteSpace(razaoSocial))
                emps = emps.Where(e => e.Nome.Contains(razaoSocial));

            if (!string.IsNullOrWhiteSpace(nomeFantasia))
                emps = emps.Where(e => e.Apelido.Contains(nomeFantasia));

            if (!string.IsNullOrWhiteSpace(cnpj))
            {
                cnpj = cnpj.Replace(".", "").Replace("/", "").Replace("-", "");
                emps = emps.Where(e => e.CNPJ == cnpj);
            }

            if (status != null)
                emps = emps.Where(e => e.Ativo == status);

            if (setor != 0)
                emps = emps.Where(e => setoresCusto.Any(sc => sc.Ativo && sc.SetorCusto.Ativo && sc.Pedido.Empresa.Codigo == e.Codigo && sc.SetorCusto.Codigo == setor));

            if (area != 0)
                emps = emps.Where(e => pedidos.Where(pe => pe.Empresa.Codigo == e.Codigo).SelectMany(pe => pe.Projeto.Areas).Where(ap => ap.Area.Codigo == area).Any());            

            return emps;
        }

        /// <summary>
        /// Cria um objeto contendo informações relacionadas ao conteudo gerenciado por
        /// RelatorioEmpresasController.
        /// </summary>
        ///
        /// <param name="empresas">
        /// O(a) empresas.
        /// </param>
        /// <param name="parameters">
        /// Options for controlling the operation.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;RelatorioEmpresaModelView&gt;
        /// </returns>
        private List<RelatorioEmpresaModelView> montaObj(IQueryable<Empresa> empresas, StoreRequestParameters parameters = null)
        {
            if (parameters == null)
                parameters = new StoreRequestParameters();
            empresas = empresas.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 25);

            return empresas.Select(empresa => new RelatorioEmpresaModelView()
            {
                Codigo = empresa.Codigo,
                CNPJ = empresa.CNPJFormatado(),
                Nome = empresa.Nome,
                Apelido = empresa.Apelido,
                Ativo = empresa.Ativo
            }).ToList();
        }
    }
}
