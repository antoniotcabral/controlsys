﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Seguranca;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Parametros;
using Controlsys.Repositorios.Seguranca;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class AtendimentoReceptivoController : Controller, IControllerBase
    {

        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        public AtendimentoReceptivoController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        [Transaction]
        public ActionResult Salvar(string[] gravacoes, string descricao)
        {
            if (gravacoes == null || !gravacoes.Any())
                throw new CoreException("Não foi possível salvar. Gravação não identificada.");

            string path = ConfigurationManager.AppSettings.Get("pathUploads");
            if (!Directory.Exists(path))
                throw new CoreException("O diretório onde a midia seria armazanada não está acessivel.");

            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            Usuario usuarioLogado = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name);
            if (usuarioLogado == null)
                throw new CoreException("Não foi possível identificar o usuário logado.");

            IRepositorioMidia repMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioMidia>(UnidadeTrabalho);
            IRepositorioAtendimentoReceptivo repAtendimentoReceptivo = Fabrica.Instancia.ObterRepositorio<IRepositorioAtendimentoReceptivo>(UnidadeTrabalho);

            int count = repAtendimentoReceptivo.ObterTodos().Count();
            foreach (var gravacao in gravacoes)
            {
                count++;
                var instante = DateTime.Now;
                string nomeDisco = string.Format("PF-AR-{0}-{1}-{2}.wav", instante.ToString("yyyyMMddHHmmss"), usuarioLogado.Login, count);

                Midia m = new Midia();
                m.DataRegistro = instante;
                m.Extensao = "wav";
                m.MimeType = "audio/wav";
                m.NomeArquivo = nomeDisco;

                repMidia.Validar(m, EstadoObjeto.Novo);
                repMidia.Salvar(m);

                AtendimentoReceptivo aR = new AtendimentoReceptivo();
                aR.DataRegistro = instante;
                aR.Midia = m;
                aR.Papel = null;
                aR.Descricao = descricao;
                aR.Usuario = usuarioLogado;
                aR.Ativar();
                repAtendimentoReceptivo.Validar(aR, EstadoObjeto.Novo);
                repAtendimentoReceptivo.Salvar(aR);

                var edt = gravacao.Split(',')[1];
                System.IO.File.WriteAllBytes(m.Url, Convert.FromBase64String(edt));
            }

            return Json(true);
        }

        public ActionResult Baixar(int codAR)
        {
            IRepositorioAtendimentoReceptivo repAtendimentoReceptivo = Fabrica.Instancia.ObterRepositorio<IRepositorioAtendimentoReceptivo>(UnidadeTrabalho);

            var ar = repAtendimentoReceptivo.ObterPorId(codAR);
            if (ar == null)
                throw new CoreException("Não foi possível identificar o atendimento receptivo informado.");

            var arquivo = System.IO.File.ReadAllBytes(ar.Midia.Url);

            return File(arquivo, ar.Midia.MimeType, ar.Midia.NomeArquivo);
        }

    }
}
