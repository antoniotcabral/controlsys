﻿using Globalsys;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar webcams.
    /// </summary>
    [ExtendController]
    public class WebcamController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /Webcam/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.WebcamController.
        /// </summary>
        ///
        /// <param name="unidade">
        /// O(a) unidade.
        /// </param>
        public WebcamController(IUnidadeTrabalho unidade)
        {
            UnidadeTrabalho = unidade;
        }

        /// <summary>
        /// Webcam.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Webcam()
        {
            return PartialView();
        }

        /// <summary>
        /// Upload.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [HttpPost]
        public ActionResult Upload()
        {
            //Random randNum = new Random();
            //int number = randNum.Next(DateTime.Now.Millisecond);

            //string caminhoTemp = Server.MapPath("/Content/img/webcam/" + number);

            System.Web.Helpers.WebImage webImage = new System.Web.Helpers.WebImage(Request.InputStream);
            
            //webImage.Save(caminhoTemp);

            return Json(Convert.ToBase64String(webImage.GetBytes()));
        }

    }
}
