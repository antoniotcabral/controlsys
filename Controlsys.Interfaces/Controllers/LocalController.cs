﻿using Controlsys.Dominio.Parametros;
using Controlsys.Infra;
using Controlsys.Repositorio.Parametros;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar os Locais
    /// </summary>
    [ExtendController]
    public class LocalController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /Local/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.LocalController.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade.
        /// </param>
        public LocalController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        /// <summary>
        /// Retorna a página de local.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [ExcludeFromCodeCoverage]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Inclui um novo local.
        /// </summary>
        ///
        /// <param name="local">
        /// O local.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Incluir(Local local)
        {
            IRepositorioLocal repLocal = Fabrica.Instancia.ObterRepositorio<IRepositorioLocal>(UnidadeTrabalho);
            local.DataRegistro = DateTime.Now;
            local.Ativar();
            repLocal.Validar(local, EstadoObjeto.Novo);
            repLocal.Salvar(local);
            return Json(local);
        }

        /// <summary>
        /// Altera um registro de local.
        /// </summary>
        ///
        /// <param name="local">
        /// O(a) area.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Alterar(Local local)
        {
            IRepositorioLocal repLocal = Fabrica.Instancia.ObterRepositorio<IRepositorioLocal>(UnidadeTrabalho);
            Local localA = repLocal.ObterPorId(local.Codigo);
            localA.Sigla = local.Sigla;
            localA.Nome = local.Nome;
            repLocal.Validar(localA, EstadoObjeto.Alterado);
            repLocal.Atualizar(localA);
            return Json(localA);
        }

        /// <summary>
        /// Ativar inativar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult AtivarInativar(int codigo)
        {
            IRepositorioLocal repLocal = Fabrica.Instancia.ObterRepositorio<IRepositorioLocal>(UnidadeTrabalho);
            Local local = repLocal.ObterPorId(codigo);
            if (local.Ativo)
            {
                repLocal.Validar(local, EstadoObjeto.Inativado);
                local.Inativar();
            }
            else
            {
                repLocal.Validar(local, EstadoObjeto.Ativado);
                local.Ativar();
            }
            repLocal.Atualizar(local);
            return Json(local);
        }

        /// <summary>
        /// Selecionar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Selecionar(int codigo)
        {
            IRepositorioLocal repLocal = Fabrica.Instancia.ObterRepositorio<IRepositorioLocal>(UnidadeTrabalho);
            Local local = repLocal.ObterPorId(codigo);
            return Json(local);
        }

        /// <summary>
        /// Retorna todos os objetos do tipo Local.
        /// </summary>
        ///
        /// <param name="apenasAtivos">
        /// true to apenas ativos.
        /// </param>
        ///
        /// <returns>
        /// Detalhes do tipo de objeto ligado ao controller no formato JSON.
        /// </returns>
        public ActionResult ObterTodos(bool apenasAtivos = false)
        {
            IRepositorioLocal repLocal = Fabrica.Instancia.ObterRepositorio<IRepositorioLocal>(UnidadeTrabalho);

            IQueryable<Local> locais = repLocal.ObterTodos();

            if (apenasAtivos)
            {
                locais = locais.Where(t => t.Ativo == true);
            }

            return Json(locais.Select(l => new
            {
                l.Codigo,
                l.Nome,
                l.Sigla,
                l.Ativo
            })
                                .ToList().OrderBy(l => l.Nome));
        }

    }
}
