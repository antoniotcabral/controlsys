﻿using Controlsys.Dominio.CredenciamentoVeiculos;
using Controlsys.Dominio.Pessoas;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Parametros;
using Controlsys.Repositorios.Pessoas;
using Ext.Net;
using Ext.Net.MVC;
using Globalsys;
using Globalsys.Exceptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class ArquivoVeiculoController : Controller, IControllerBase
    {

        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        public ArquivoVeiculoController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        public ActionResult Index()
        {
            return View();
        }

        public FileResult ExportarSelecao(int[] codigosVeiculoCredencialVeiculo = null)
        {
            if (codigosVeiculoCredencialVeiculo == null)
                throw new CoreException("Selecione ao menos um veículo.");

            return GerarZip(FiltrarVeiculoCredencialVeiculo(null, null, null, null, 0, null, null, codigosVeiculoCredencialVeiculo));
        }

        public FileResult ExportarPesquisa(string codigoEmpresa = null, string cnpjEmpresa = null, string nomePessoa = null, string cpfPassaporte = null, string placa = null, int codigoModelo = 0, StatusCredencial? status = null)
        {
            return GerarZip(FiltrarVeiculoCredencialVeiculo(codigoEmpresa, cnpjEmpresa, nomePessoa, cpfPassaporte, codigoModelo, placa, status));
        }

        public ActionResult Pesquisar(StoreRequestParameters parameters, string codigoEmpresa = null, string cnpjEmpresa = null, string nomePessoa = null, string cpfPassaporte = null, string placa = null, int codigoModelo = 0, StatusCredencial? status = null)
        {
            IQueryable<VeiculoCredencialVeiculo> resultadoFiltro = FiltrarVeiculoCredencialVeiculo(codigoEmpresa, cnpjEmpresa, nomePessoa, cpfPassaporte, codigoModelo, placa, status);

            int total = resultadoFiltro.Count();
            ArquivoVeiculoModelView[] retorno = this.MontaObjetos(resultadoFiltro, parameters);

            return new StoreResult(retorno, total);
        }

        public IQueryable<VeiculoCredencialVeiculo> FiltrarVeiculoCredencialVeiculo(string codigoEmpresa = null, string cnpjEmpresa = null, string nomePessoa = null, string cpfPassaporte = null, int codigoModelo = 0, string placa = null, StatusCredencial? status = null, int[] codigosVeiculoCredencialVeiculo = null)
        {
            IQueryable<VeiculoCredencialVeiculo> veiculoCredencialVeiculos = Fabrica.Instancia.ObterRepositorio<IRepositorioVeiculoCredencialVeiculo>(UnidadeTrabalho).ObterTodos().Where(vcv => vcv.Veiculo.Midias.Any(m => m.Ativo));
            
            if (codigosVeiculoCredencialVeiculo == null || codigosVeiculoCredencialVeiculo.Length == 0)
            {
                if (!string.IsNullOrEmpty(codigoEmpresa) || !string.IsNullOrEmpty(cnpjEmpresa) || !string.IsNullOrEmpty(nomePessoa) || !string.IsNullOrEmpty(cpfPassaporte))
                {
                    IQueryable<Papel> papeis = Fabrica.Instancia.ObterRepositorio<IRepositorioPapel>(UnidadeTrabalho).ObterTodos();

                    if (!string.IsNullOrEmpty(cnpjEmpresa))
                    {
                        cnpjEmpresa = cnpjEmpresa.Replace(".", "").Replace("-", "").Replace("/", "");
                        veiculoCredencialVeiculos = veiculoCredencialVeiculos.Where(vcv => vcv.CredencialVeiculo.Empresa.CNPJ.Contains(cnpjEmpresa));
                    }

                    if (!string.IsNullOrEmpty(codigoEmpresa))
                        veiculoCredencialVeiculos = veiculoCredencialVeiculos.Where(vcv => vcv.CredencialVeiculo.Empresa.Codigo.Equals(codigoEmpresa));

                    if (!string.IsNullOrEmpty(nomePessoa))
                        papeis = papeis.Where(p => p.PessoaFisica.Nome.ToUpper().Contains(nomePessoa.ToUpper()));

                    if (!string.IsNullOrEmpty(cpfPassaporte))
                    {
                        string cpfpassaporteformatado = cpfPassaporte.Replace("-", string.Empty).Replace(".", string.Empty).Replace("/", string.Empty);
                        papeis = papeis.Where(p => (p.PessoaFisica.CPF != null && p.PessoaFisica.CPF.Contains(cpfpassaporteformatado)) || (p.PessoaFisica.Passaporte != null && p.PessoaFisica.Passaporte.Contains(cpfpassaporteformatado)));
                    }

                    List<int> codigoPapeis = papeis.Select(p => p.Codigo).ToList();

                    int qtdcolab = codigoPapeis.Count;

                    List<VeiculoCredencialVeiculo> listaVCV = new List<VeiculoCredencialVeiculo>();

                    for (int i = 0; i < qtdcolab; i += 2000)
                    {
                        var page = codigoPapeis.Skip(i).Take(2000).ToArray();
                        listaVCV.AddRange(veiculoCredencialVeiculos.Where(vcv => page.Contains(vcv.CredencialVeiculo.Condutor.Codigo)));
                    }

                    veiculoCredencialVeiculos = listaVCV.AsQueryable();
                }

                if (!string.IsNullOrEmpty(placa))
                {
                    veiculoCredencialVeiculos = veiculoCredencialVeiculos.Where(vcv => vcv.Veiculo.Placa.Contains(placa));
                }

                if (codigoModelo > 0)
                {
                    veiculoCredencialVeiculos = veiculoCredencialVeiculos.Where(vcv => vcv.Veiculo.Modelo.Codigo == codigoModelo);
                }

                if (status.HasValue)
                {
                    //veiculoCredencialVeiculos = veiculoCredencialVeiculos.Where(vcv => vcv.CredencialVeiculo.ObterSituacao().Status == status.Value);
                    veiculoCredencialVeiculos = veiculoCredencialVeiculos.Where(vcv => 
                        vcv.CredencialVeiculo.Situacoes.OrderByDescending(s => s.Codigo).Select(sv => sv.Status).FirstOrDefault() == status);
                }
            }
            else
            {
                veiculoCredencialVeiculos = veiculoCredencialVeiculos.Where(vcv => codigosVeiculoCredencialVeiculo.Contains(vcv.Codigo));
            }

            return veiculoCredencialVeiculos;
        }

        public ArquivoVeiculoModelView[] MontaObjetos(IQueryable<VeiculoCredencialVeiculo> veiculoCredencialVeiculos, StoreRequestParameters parameters = null, bool ignoreParameters = false)
        {

            if (!ignoreParameters)
            {
                if (parameters == null)
                    parameters = new StoreRequestParameters();

                veiculoCredencialVeiculos = veiculoCredencialVeiculos.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 30);
            }

            ArquivoVeiculoModelView[] papeisModeVIew = veiculoCredencialVeiculos.ToList().Select(vcv => new ArquivoVeiculoModelView
            {
                CodigoVeiculoCredencialVeiculo = vcv.Codigo,
                Condutor = vcv.CredencialVeiculo.Condutor.PessoaFisica.Nome,
                CPF = vcv.CredencialVeiculo.Condutor.PessoaFisica.CPFFormatado() ?? string.Empty,
                Passaporte = vcv.CredencialVeiculo.Condutor.PessoaFisica.Passaporte ?? string.Empty,
                Empresa = ((vcv.CredencialVeiculo.Condutor is Colaborador) && (vcv.CredencialVeiculo.Condutor as Colaborador).Empresa != null ? (vcv.CredencialVeiculo.Condutor as Colaborador).Empresa.Nome : (vcv.CredencialVeiculo.Condutor is PrestadorServico) && (vcv.CredencialVeiculo.Condutor as PrestadorServico).Empresa != null ? (vcv.CredencialVeiculo.Condutor as PrestadorServico).Empresa.Nome : (vcv.CredencialVeiculo.Condutor is Visitante) ? (vcv.CredencialVeiculo.Condutor as Visitante).Empresa : string.Empty),
                Status = vcv.CredencialVeiculo.ObterSituacao().Status.ObterDescricaoEnum(),
                Modelo = vcv.Veiculo.Modelo.Nome,
                Placa = vcv.Veiculo.Placa
            }).OrderBy(vcv => vcv.Placa).ToArray();

            return papeisModeVIew;
        }

        private FileContentResult GerarZip(IQueryable<VeiculoCredencialVeiculo> veiculoCredencialVeiculos)
        {
            //Cria Stream para ser usada pelo ZupArchive sendo que este a utilizará para gerar o arquivo que será enviado para download
            using (var streamArquivoFinalCompactado = new MemoryStream())
            {
                //Cria um ZipArchive que é responsavel por anexar as pastas e arquivos ao arquivo final a ser exportado
                using (var zipArchive = new ZipArchive(streamArquivoFinalCompactado, ZipArchiveMode.Update, false))
                {
                    foreach (VeiculoCredencialVeiculo vcv in veiculoCredencialVeiculos)
                    {
                        
                        string nomePasta = vcv.Veiculo.Placa + "-" + vcv.Codigo + @"\";

                        IEnumerable<string> urls = vcv.Veiculo.Midias.Select(m => m.Midia.Url);

                        if (urls.Count() > 0)
                        {
                            //Cria uma nova entrada no zip. caminhos terminados com '\' se tornam pastas. 
                            var zipPasta = zipArchive.CreateEntry(nomePasta, CompressionLevel.Optimal);

                            string arquivosNaoEncontrados = string.Empty;

                            foreach (string url in urls)
                            {
                                if (System.IO.File.Exists(url))
                                {
                                    string nomeArquivo = System.IO.Path.Combine(new string[] { nomePasta, System.IO.Path.GetFileName(url) });

                                    var zipArquivo = zipArchive.CreateEntryFromFile(url, nomeArquivo, CompressionLevel.Optimal);
                                }
                                else
                                {
                                    arquivosNaoEncontrados = url + Environment.NewLine;
                                }
                            }
                        }
                    }

                }

                return new FileContentResult(streamArquivoFinalCompactado.ToArray(), "application/zip") { FileDownloadName = "Download.zip" };
            }
        }

    }
}
