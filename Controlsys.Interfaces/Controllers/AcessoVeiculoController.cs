﻿using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Controlsys.Dominio.Seguranca;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Acesso;
using Controlsys.Repositorios.Empresas;
using Controlsys.Repositorios.Parametros;
using Controlsys.Repositorios.Pessoas;
using Controlsys.Repositorios.Seguranca;
using Ext.Net;
using Ext.Net.MVC;
using Globalsys;
using Globalsys.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{/// <summary>
/// Um controlador para tratar consultar acessos de veículos
/// </summary>
    [ExtendController]
    public class AcessoVeiculoController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /ConsultarAcessos/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.AcessoVeiculoController.
        /// </summary>
        ///
        /// <param name="unidade">
        /// O(a) unidade.
        /// </param>
        public AcessoVeiculoController(IUnidadeTrabalho unidade)
        {
            UnidadeTrabalho = unidade;
        }


        //
        // GET: /AcessoVeiculo/

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            string retorno = "false";
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            Usuario usuarioLogado = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name, somenteAtivos: true);
            if (usuarioLogado != null)
            {
                var grupos = usuarioLogado.GruposUsuarios.Select(g => g.Grupo);
                var acoes = grupos.SelectMany(g => g.Acoes);
                retorno = acoes.Where(a => a.Url == "/AcessoVeiculo/Pesquisar").Any().ToString();
            }
            return View("Index", model: retorno);
        }

        public ActionResult Pesquisar(StoreRequestParameters parameters, TipoSetorCusto? tipoSetor, List<StatusAcesso> status, string cracha = null, string cpf = null, string empresa = null, string nome = null, int cargo = 0, DateTime? dataInicio = null, DateTime? dataFim = null, int setor = 0, List<int> leitoras = null, TipoPapel? papel = null, bool validaGestor = false, long? empregadoSap = null, long? gestorPonto = null, long? superiorImediato = null, string codigoPht = null, IList<string> localLeitora = null, string passaporte = null, string maodeobra = null, string fornecedor = null, string contratada = null, string placa = null, string modeloCredencial = null, string cor = null, string modeloVeiculo = null, string fabricante = null, string nomeCamera = null)
        {
            string retorno = "false";
            string pesquisar = "false";
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            Usuario usuarioLogado = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name, somenteAtivos: true);
            if (usuarioLogado != null)
            {
                var grupos = usuarioLogado.GruposUsuarios.Select(g => g.Grupo);
                var acoes = grupos.SelectMany(g => g.Acoes);
                pesquisar = acoes.Where(a => a.Url == "/AcessoTemporarioVeiculo/Pesquisar").Any().ToString();
            }


            if (pesquisar != "True")
            {
                throw new CoreException("Não é possível pesquisar. Não permitido o retorno das informações solicitadas.");
            }

            empresa = Empresa.RetornaApenasNomeEmpresa(empresa);

            var aloc = FiltrarAlocacaoColaboradores(tipoSetor, setor, contratada);
            IQueryable<AcessoVeiculo> listControleAc = FiltrarAcessos(tipoSetor, status, aloc, nome, cpf, cracha, empresa, cargo, dataInicio, dataFim, setor, leitoras,
                papel, validaGestor: validaGestor, empregadoSap: empregadoSap, gestorPonto: gestorPonto, superiorImediato: superiorImediato, codigoPht: codigoPht, localLeitora: localLeitora, passaporte: passaporte, maodeobra: maodeobra, fornecedor: fornecedor, contratada: contratada, placa: placa, modeloCredencial: modeloCredencial, cor: cor, modeloVeiculo: modeloVeiculo, fabricante: fabricante, nomeCamera: nomeCamera);
            int total = listControleAc.Count();
            List<AcessoVeiculoModelView> data = montaObj(listControleAc, aloc, parameters);

            return new StoreResult(data, total);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="codigo"></param>
        /// <returns></returns>
        public ActionResult ExibirImagensCapturadas(int codigo)
        {
            IRepositorioAcessoVeiculo repositorioAcessoVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioAcessoVeiculo>(UnidadeTrabalho);
            
            var acessoVeiculo = repositorioAcessoVeiculo.ObterPorId(codigo);

            IRepositorioCameraImagem repositorioCameraImagem = Fabrica.Instancia.ObterRepositorio<IRepositorioCameraImagem>(UnidadeTrabalho);

            List<CameraImagem> imagens = new List<CameraImagem>();

            if(acessoVeiculo.CamerasImagens == null)
            {
                 imagens = repositorioCameraImagem.ObterTodos().Where(ci => ci.AcessoVeiculo.Codigo == acessoVeiculo.Codigo).ToList();

            }

            var imgErro = "";

            var imagemString = "";
            try
            {
                string path = Server.MapPath("/");
                byte[] imageArray = System.IO.File.ReadAllBytes(path + "\\Imagens\\indisponivel.png");
                string base64ImageRepresentation = Convert.ToBase64String(imageArray);
                imgErro = "<img src=\"data:image/png;base64," + base64ImageRepresentation + "\">";
            }
            catch (Exception imgError)
            {
                imgErro = "";
            }
            //Pegar placa com maior precisão
            if (imagens != null && imagens.Count > 0)
            {
                IRepositorioParametro repositorioParametro = Fabrica.Instancia.ObterRepositorio<IRepositorioParametro>(UnidadeTrabalho);
                //var caminhoImagem = repositorioParametro.ObterTodos().Where(p => p.Nome == ParametroSistema.CAMINHO_IMAGEM).SingleOrDefault().Valor;
                var caminhoImagem = repositorioParametro.ObterTodos().Where(p => p.Nome == ParametroSistema.CAMINHO_IMAGEM_FISICO).SingleOrDefault().Valor;
                float precisaoAnterior = 0;

                for (int i = 0; i < imagens.Count; i++)
                {
                    if (imagens[i].CaminhoImagem != null)
                    {
                        //System.Drawing.Image image1 = System.Drawing.Image.FromFile(caminhoImagem + s.CamerasImagens[i].CaminhoImagem);
                        try
                        {
                            var caminhoCompleto = caminhoImagem + imagens[i].CaminhoImagem;
                            byte[] imageArray = System.IO.File.ReadAllBytes(caminhoImagem + imagens[i].CaminhoImagem);
                            string base64ImageRepresentation = Convert.ToBase64String(imageArray);
                            var imagem = "<img src=\"data:image/jpeg;base64," + base64ImageRepresentation + "\">";
                            imagemString += "<img src=\"data:image/jpeg;base64," + base64ImageRepresentation + "\">";
                        }
                        catch (Exception eImgFisico)
                        {
                            Console.WriteLine(eImgFisico);

                            //byte[] imageArray = System.IO.File.ReadAllBytes(path + "\\Imagens\\indisponivel.png");
                            //string base64ImageRepresentation = Convert.ToBase64String(imageArray);
                            //var imagem = "<img src=\"data:image/jpeg;base64," + base64ImageRepresentation + "\">";
                            imagemString = imagemString + imgErro;
                            //"<img src=\"data:image/png;base64," + base64ImageRepresentation + "\">divisao";
                        }
                    }
                }
            }
            else
            {
                imagemString += imgErro;
            }
            var obj = new AcessoVeiculoModelView();
            
            obj.Imagens = imagemString;
           
            var imgSerializada = serializaObjeto(obj);
            return new StoreResult(obj,1);
        }


        private string serializaObjeto(object obj)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(obj);
        }



        /// <summary>
        /// Cria um objeto contendo informações relacionadas ao conteudo gerenciado por
        /// ConsultarAcessosController.
        /// </summary>
        ///
        /// <param name="controleAcessos">
        /// O(a) controle acessos.
        /// </param>
        /// <param name="aloc">
        /// O(a) aloc.
        /// </param>
        /// <param name="parameters">
        /// Options for controlling the operation.
        /// </param>
        /// <param name="ignoreParameters">
        /// true to ignore parameters.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;ControleAcessoModelView&gt;
        /// </returns>
        public List<AcessoVeiculoModelView> montaObj(IQueryable<AcessoVeiculo> controleAcessos, IQueryable<AlocacaoColaborador> aloc, StoreRequestParameters parameters = null, bool ignoreParameters = false)
        {
            if (!ignoreParameters)
            {
                if (parameters == null)
                    parameters = new StoreRequestParameters();
                controleAcessos = controleAcessos.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 25);
            }

            List<AcessoVeiculoModelView> list = new List<AcessoVeiculoModelView>();
            var imgErro = "";

            try
            {

                string path = Server.MapPath("/");
                byte[] imageArray = System.IO.File.ReadAllBytes(path + "\\Imagens\\indisponivel.png");
                string base64ImageRepresentation = Convert.ToBase64String(imageArray);
                imgErro = "<img  style=\"width: 45%;margin-left: 3.5 %;margin-top: 7%;\" src=\"data:image/png;base64," + base64ImageRepresentation + "\">divisao";
            }
            catch (Exception imgError)
            {
                imgErro = "";
            }

            foreach (var s in controleAcessos)
            {
                //if (s.CamerasImagens == null || s.CamerasImagens.Count == 0)
                //{
                //    IRepositorioCameraImagem repositorio = Fabrica.Instancia.Obter<IRepositorioCameraImagem>();
                //    s.CamerasImagens = repositorio.ObterTodos().Where(ci => ci.AcessoVeiculo.Codigo == s.Codigo).ToList();
                //}

             

                AcessoVeiculoModelView item = new AcessoVeiculoModelView();
                //item.Nome = (s.Papel != null ? s.Papel.Codigo.ToString() : "");
                item.Codigo = s.Codigo.ToString();

                item.NumCracha = (s.Cracha != null) ? s.Cracha.Numero : null;
                var status = "";
                if (s.Leitora != null)
                {
                    item.Leitora = s.Leitora.Descricao;
                    item.Local = s.Leitora.Local;
                    item.Direcao = s.Leitora.Direcao.ObterDescricaoEnum();
                }

                item.DataHora = s.DataRegistro;
                item.StatusAcesso = s.StatusAcesso.ObterDescricaoEnum();

                //item.Direcao = s.Leitora.Direcao.ObterDescricaoEnum();


                //switch (status)
                //{
                //    case "OCO_UR_CONTROLE_ACESSO_AUTORIZADO_LPR_ATIVO_ACIMA":

                //    case "OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_LPR_INATIVO":

                //    case "OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_INATIVA":

                //    case "OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_ATIVA_LPR_INATIVO":

                //    case "OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_OFFLINE":

                //    case "OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERAS_NAO_VINCULADAS":

                //    case "OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_ATIVA_LPR_INATIVO_PARAMETRO":

                //    case "OCO_UR_CONTROLE_ACESSO_VEICULO_VALIDACAO_DESATIVADA":
                //        item.StatusAcesso = "Acesso Liberado";
                //        break;

                //    default:
                //        item.StatusAcesso = "Acesso negado";
                //        break;
                //}



                if (s.VeiculoCredencialVeiculo != null)
                {
                    if (s.VeiculoCredencialVeiculo.Veiculo != null)
                    {
                        if (s.VeiculoCredencialVeiculo.CredencialVeiculo != null)
                        {
                            if (s.VeiculoCredencialVeiculo.CredencialVeiculo.ModeloCredencial != null)
                            {
                                item.ModeloCredencial = s.VeiculoCredencialVeiculo.CredencialVeiculo.ModeloCredencial.Nome;//.TipoCredencial.ObterDescricaoEnum();
                            }
                        }

                        item.Cor = s.VeiculoCredencialVeiculo.Veiculo.Cor;
                        if (s.VeiculoCredencialVeiculo.Veiculo.Modelo != null)
                        {
                            item.Modelo = s.VeiculoCredencialVeiculo.Veiculo.Modelo.Nome;
                            if (s.VeiculoCredencialVeiculo.Veiculo.Modelo.Fabricante != null)
                            {
                                item.Fabricante = s.VeiculoCredencialVeiculo.Veiculo.Modelo.Fabricante.Nome;
                            }
                            else
                            {
                                item.Fabricante = "";
                            }
                        }
                        else
                        {
                            item.Modelo = "";
                        }
                    }
                    else
                    {
                        item.Cor = "";
                    }
                }
                else
                {
                    item.Fabricante = "";
                    item.Modelo = "Modelo Não identificado";
                    item.Cor = "";
                }



              
                //Pegar placa com maior precisão
                if (s.CamerasImagens != null && s.CamerasImagens.Count > 0)
                {
                    IRepositorioParametro repositorioParametro = Fabrica.Instancia.ObterRepositorio<IRepositorioParametro>(UnidadeTrabalho);
                    //var caminhoImagem = repositorioParametro.ObterTodos().Where(p => p.Nome == ParametroSistema.CAMINHO_IMAGEM).SingleOrDefault().Valor;
                    var caminhoImagem = repositorioParametro.ObterTodos().Where(p => p.Nome == ParametroSistema.CAMINHO_IMAGEM_FISICO).SingleOrDefault().Valor;
                    float precisaoAnterior = 0;

                    for (int i = 0; i < s.CamerasImagens.Count; i++)
                    {
                        if (s.CamerasImagens != null && s.CamerasImagens.Count > 0)
                        {
                            if (s.CamerasImagens[i].Camera != null)
                            {
                                item.Camera += s.CamerasImagens[i].Camera.Nome + " ";
                            }
                            else
                            {
                                item.Camera += "";
                            }
                        }

                        string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority +
    Request.ApplicationPath.TrimEnd('/') + "/";
                        if (s.CamerasImagens[i].CaminhoImagem == null || s.CamerasImagens[i].CaminhoImagem == "")
                        {
                            item.Imagens += imgErro;
                        }else
                        if (s.CamerasImagens[i].CaminhoImagem != null)
                        {
                            System.Drawing.Image image1 = System.Drawing.Image.FromFile(caminhoImagem + s.CamerasImagens[i].CaminhoImagem);
                            try
                            {
                            
                                var caminhoCompleto = caminhoImagem + s.CamerasImagens[i].CaminhoImagem;
                                byte[] imageArray = System.IO.File.ReadAllBytes(caminhoImagem + s.CamerasImagens[i].CaminhoImagem);
                                string base64ImageRepresentation = Convert.ToBase64String(imageArray);
                                var imagem = "<img style='width: 45%;margin-left: 3.5 %;margin-top: 7%;' src=\"data:image/jpeg;base64," + base64ImageRepresentation + "\">";
                                item.Imagens += "<img  style=\"width: 45%;margin-left: 3.5 %;margin-top: 7%;\" src=\"data:image/jpeg;base64," + base64ImageRepresentation + "\">divisao";
                            }
                            catch (Exception eImgFisico)
                            {
                                Console.WriteLine(eImgFisico);

                                //byte[] imageArray = System.IO.File.ReadAllBytes(path + "\\Imagens\\indisponivel.png");
                                //string base64ImageRepresentation = Convert.ToBase64String(imageArray);
                                //var imagem = "<img src=\"data:image/jpeg;base64," + base64ImageRepresentation + "\">";
                                item.Imagens = item.Imagens + imgErro;
                                //"<img src=\"data:image/png;base64," + base64ImageRepresentation + "\">divisao";
                            }
                        }
                        if (s.CamerasImagens[i].PrecisaoImage > precisaoAnterior)
                        {
                            item.Placa = s.CamerasImagens[i].PlacaVeiculo;
                            precisaoAnterior = s.CamerasImagens[i].PrecisaoImage;
                        }
                    }
                }
                else
                {
                    item.Imagens +=  imgErro;
                }

                if (s.Papel != null)
                {
                    item.Nome = s.Papel.PessoaFisica.Nome;
                    //item.Papel = s.Papel is Colaborador ? "Colaborador" : (s.Papel is PrestadorServico ? "Prestador de Serviço" : (s.Papel is Visitante ? "Visitante" : ""));
                    item.CPF = s.Papel.PessoaFisica.CPFFormatado();

                    if (s.Papel.GetType().Equals(typeof(Colaborador)))
                    {
                        item.Papel = "Colaborador";
                    }

                    if (s.Papel.GetType() == typeof(Colaborador))
                    {

                    }
                    var papelClasse = s.Papel.GetType().ToString();
                    var papelClasse2 = s.Papel.GetType().Name;
                    if (s.Papel is Colaborador)
                    {
                        item.Papel = "Colaborador";
                        item.Empresa = ((Colaborador)s.Papel).Empresa.Apelido;

                        IRepositorioColaborador repColaborador = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);
                        item.MaoDeObra = repColaborador.ObterPorId(s.Papel.Codigo).MaoDeObra.ToString();

                        if (s.Papel.EmpregadoSAP != null)
                        {
                            item.MatriculaEmpregadoSAP = string.Format("{0}", s.Papel.EmpregadoSAP.Codigo.ToString());
                            item.MatriculaGestorPonto = s.Papel.EmpregadoSAP.GestorPonto != null ? s.Papel.EmpregadoSAP.GestorPonto.Codigo.ToString() : string.Empty;
                            item.MatriculaSuperiorImediato = s.Papel.EmpregadoSAP.SuperiorImediato != null ? s.Papel.EmpregadoSAP.SuperiorImediato.Codigo.ToString() : string.Empty;
                        }
                        IRepositorioGrupoTrabalhoColab repGrpTrabColab = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoTrabalhoColab>(UnidadeTrabalho);
                        var grpTrabColab = repGrpTrabColab.ObterTodos().OrderByDescending(gtc => gtc.GrupoTrabalho.Codigo)
                            .FirstOrDefault(gtc => gtc.Ativo && gtc.Papel.Codigo.Equals(s.Papel.Codigo));
                        if (grpTrabColab != null && grpTrabColab.Codigo > 0)
                            item.NumeroPHT = grpTrabColab.GrupoTrabalho.CodigoPHT;
                    }

                    if (s.Papel is PrestadorServico)
                    {
                        IRepositorioPrestadorServico repPrestadorServico = Fabrica.Instancia.ObterRepositorio<IRepositorioPrestadorServico>(UnidadeTrabalho);
                        item.MaoDeObra = repPrestadorServico.ObterPorId(s.Papel.Codigo).MaoDeObra.ToString();

                        item.Papel = "Prestador de Serviço";
                        if (((PrestadorServico)s.Papel).TipoPrestador == TipoPrestador.Temporario)
                        {
                            if (((PrestadorServico)s.Papel).Empresa != null)
                                item.Empresa = ((PrestadorServico)s.Papel).Empresa.Apelido;
                        }
                        else
                        {
                            if (((PrestadorServico)s.Papel).Fornecedor != null)
                                item.Empresa = ((PrestadorServico)s.Papel).Fornecedor.NomeFantasia;
                        }
                    }

                    if (s.Papel is Visitante)
                    {
                        item.MaoDeObra = null;
                        item.Papel = "Visitante";
                        item.Empresa = ((Visitante)s.Papel).Empresa;
                    }

                    item.Cargo = (s.Papel is Visitante) ?
                                    (s.Papel as Visitante).Funcao : s.Papel.Cargo != null ?
                                                                        s.Papel.Cargo.Nome : null;

                    AlocacaoColaborador alocacao = aloc.Where(a => a.Papel.Codigo == s.Papel.Codigo).OrderByDescending(a => a.DataRegistro).FirstOrDefault();
                    PedidoCompra alocada = alocacao != null ? alocacao.ObterContratada() : null;

                    if (alocacao != null)
                    {
                        item.TipoSetor = alocacao.SetorCusto != null ? alocacao.SetorCusto.TipoSetor.ObterDescricaoEnum() : "";
                        item.Setor = alocacao.SetorCusto != null ? alocacao.SetorCusto.Nome : "";
                        item.Contratada = alocada.Empresa != null ? alocada.Empresa.Nome : "";
                        /*item.Contratada = alocacao.PedidoCompra.PedidoCompraPai != null ? alocacao.PedidoCompra.PedidoCompraPai.Empresa.Apelido :
                                            alocacao.PedidoCompra.PedidoCompraPai == null ? alocacao.PedidoCompra.Empresa.Nome : "";*/
                    }
                }

                list.Add(item);
            }

            return list;
            //        if (!ignoreParameters)
            //        {
            //            if (parameters == null)
            //                parameters = new StoreRequestParameters();
            //            controleAcessos = controleAcessos.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 25);
            //        }

            //        List<AcessoVeiculoModelView> list = new List<AcessoVeiculoModelView>();

            //        foreach (var s in controleAcessos)
            //        {
            //            if (s.CamerasImagens == null || s.CamerasImagens.Count == 0)
            //            {
            //                IRepositorioCameraImagem repositorio = Fabrica.Instancia.Obter<IRepositorioCameraImagem>();
            //                s.CamerasImagens = repositorio.ObterTodos().Where(ci => ci.AcessoVeiculo.Codigo == s.Codigo).ToList();
            //            }

            //            AcessoVeiculoModelView item = new AcessoVeiculoModelView();
            //            item.Nome = (s.Papel != null ? s.Papel.PessoaFisica.Nome : "");

            //            item.Codigo = (s.Papel != null ? s.Papel.Codigo.ToString() : "");
            //            item.NumCracha = (s.Cracha != null) ? s.Cracha.Numero : null;
            //            var status = "";
            //            if (s.Leitora != null)
            //            {
            //                item.Leitora = s.Leitora.Descricao;
            //                item.Local = s.Leitora.Local;
            //                item.Direcao = s.Leitora.Direcao.ObterDescricaoEnum();
            //            }
            //            item.DataHora = s.DataRegistro;
            //            // status = 
            //            item.StatusAcesso = s.StatusAcesso.ObterDescricaoEnum();
            //            //switch (status)
            //            //{
            //            //    case "OCO_UR_CONTROLE_ACESSO_AUTORIZADO_LPR_ATIVO_ACIMA":

            //            //    case "OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_LPR_INATIVO":

            //            //    case "OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_INATIVA":

            //            //    case "OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_ATIVA_LPR_INATIVO":

            //            //    case "OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_OFFLINE":

            //            //    case "OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERAS_NAO_VINCULADAS":

            //            //    case "OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_ATIVA_LPR_INATIVO_PARAMETRO":

            //            //    case "OCO_UR_CONTROLE_ACESSO_VEICULO_VALIDACAO_DESATIVADA":
            //            //        item.StatusAcesso = "Acesso Liberado";
            //            //        break;

            //            //    default:
            //            //        item.StatusAcesso = "Acesso negado";
            //            //        break;
            //            //}



            //            //
            //            if (s.VeiculoCredencialVeiculo != null)
            //            {
            //                if (s.VeiculoCredencialVeiculo.Veiculo != null)
            //                {
            //                    item.Cor = s.VeiculoCredencialVeiculo.Veiculo.Cor;
            //                    if (s.VeiculoCredencialVeiculo.Veiculo.Modelo != null)
            //                    {
            //                        item.Modelo = s.VeiculoCredencialVeiculo.Veiculo.Modelo.Nome;
            //                        if (s.VeiculoCredencialVeiculo.Veiculo.Modelo.Fabricante != null)
            //                        {
            //                            item.Fabricante = s.VeiculoCredencialVeiculo.Veiculo.Modelo.Fabricante.Nome;
            //                        }
            //                        else
            //                        {
            //                            item.Fabricante = "";
            //                        }
            //                    }
            //                    else
            //                    {
            //                        item.Modelo = "";
            //                    }
            //                }
            //                else
            //                {
            //                    item.Cor = "";
            //                }
            //            }

            //            //Pegar placa com maior precisão
            //            if (s.CamerasImagens != null && s.CamerasImagens.Count > 0)
            //            {
            //                IRepositorioParametro repositorioParametro = Fabrica.Instancia.ObterRepositorio<IRepositorioParametro>(UnidadeTrabalho);
            //                var caminhoImagem = repositorioParametro.ObterTodos().Where(p => p.Nome == ParametroSistema.CAMINHO_IMAGEM_FISICO).SingleOrDefault().Valor;
            //                float precisaoAnterior = 0;

            //                for (int i = 0; i < s.CamerasImagens.Count; i++)
            //                {
            //                    if (String.IsNullOrEmpty(s.CamerasImagens[i].Camera.Nome) || s.CamerasImagens[i].Camera.Nome != "")
            //                    {
            //                        item.Camera = s.CamerasImagens[i].Camera.Nome;
            //                    }

            //                    string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority +
            //Request.ApplicationPath.TrimEnd('/') + "/";
            //                    if (s.CamerasImagens[i].CaminhoImagem != null)
            //                    {

            //                        //s.CamerasImagens[i].CaminhoImagem.Replace("C:\\Users\\denis.nogueira\\Downloads\\", baseUrl + "\\Imagens\\"
            //                        //Cam_LPR FRONTAL ENTRADA TESTE_09_05_2019_05_17_02
            //                        //var caminho = @"<img src='" + baseUrl + "\\Imagens\\Cam_LPR FRONTAL ENTRADA TESTE_09_05_2019_04_27_50.jpg\">";

            //                        //item.Imagens += @"<img src='" + baseUrl + "\\Imagens\\Cam_LPR FRONTAL ENTRADA TESTE_09_05_2019_04_27_50.jpg\">";

            //                        item.Imagens += "<img style=\"margin-left 10%;\" src=\"" + caminhoImagem + s.CamerasImagens[i].CaminhoImagem + "\">"; ;
            //                        //item.Imagens += "<img style=\"margin-left 10%;\" src=\"" + baseUrl + "Imagens/Cam_LPR FRONTAL ENTRADA TESTE_09_05_2019_04_27_50.jpg\">";
            //                    }




            //                    if (s.CamerasImagens[i].PrecisaoImage > precisaoAnterior)
            //                    {
            //                        if (String.IsNullOrEmpty(s.CamerasImagens[i].Camera.Nome) || s.CamerasImagens[i].Camera.Nome != "")
            //                        {
            //                            item.Camera = s.CamerasImagens[i].Camera.Nome;
            //                        }

            //                        item.Placa = s.CamerasImagens[i].PlacaVeiculo;
            //                        precisaoAnterior = s.CamerasImagens[i].PrecisaoImage;

            //                    }
            //                }
            //            }
            //            else
            //            {
            //                item.Placa = "";
            //            }



            //            //if (s.Papel != null)
            //            //{
            //            //    item.Nome = s.Papel.PessoaFisica.Nome;
            //            //    item.Papel = s.Papel is Colaborador ? "Colaborador" : (s.Papel is PrestadorServico ? "Prestador de Serviço" : (s.Papel is Visitante ? "Visitante" : ""));
            //            //    item.CPF = s.Papel.PessoaFisica.CPFFormatado();

            //            //    if (s.Papel is Colaborador)
            //            //    {
            //            //        item.Papel = "Colaborador";
            //            //        item.Empresa = ((Colaborador)s.Papel).Empresa.Apelido;

            //            //        IRepositorioColaborador repColaborador = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);
            //            //        item.MaoDeObra = repColaborador.ObterPorId(s.Papel.Codigo).MaoDeObra.ToString();

            //            //        if (s.Papel.EmpregadoSAP != null)
            //            //        {
            //            //            item.MatriculaEmpregadoSAP = string.Format("{0}", s.Papel.EmpregadoSAP.Codigo.ToString());
            //            //            item.MatriculaGestorPonto = s.Papel.EmpregadoSAP.GestorPonto != null ? s.Papel.EmpregadoSAP.GestorPonto.Codigo.ToString() : string.Empty;
            //            //            item.MatriculaSuperiorImediato = s.Papel.EmpregadoSAP.SuperiorImediato != null ? s.Papel.EmpregadoSAP.SuperiorImediato.Codigo.ToString() : string.Empty;
            //            //        }
            //            //        IRepositorioGrupoTrabalhoColab repGrpTrabColab = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoTrabalhoColab>(UnidadeTrabalho);
            //            //        var grpTrabColab = repGrpTrabColab.ObterTodos().OrderByDescending(gtc => gtc.GrupoTrabalho.Codigo)
            //            //            .FirstOrDefault(gtc => gtc.Ativo && gtc.Papel.Codigo.Equals(s.Papel.Codigo));
            //            //        if (grpTrabColab != null && grpTrabColab.Codigo > 0)
            //            //            item.NumeroPHT = grpTrabColab.GrupoTrabalho.CodigoPHT;
            //            //    }

            //            //    if (s.Papel is PrestadorServico)
            //            //    {
            //            //        IRepositorioPrestadorServico repPrestadorServico = Fabrica.Instancia.ObterRepositorio<IRepositorioPrestadorServico>(UnidadeTrabalho);
            //            //        item.MaoDeObra = repPrestadorServico.ObterPorId(s.Papel.Codigo).MaoDeObra.ToString();

            //            //        item.Papel = "Prestador de Serviço";
            //            //        if (((PrestadorServico)s.Papel).TipoPrestador == TipoPrestador.Temporario)
            //            //        {
            //            //            if (((PrestadorServico)s.Papel).Empresa != null)
            //            //                item.Empresa = ((PrestadorServico)s.Papel).Empresa.Apelido;
            //            //        }
            //            //        else
            //            //        {
            //            //            if (((PrestadorServico)s.Papel).Fornecedor != null)
            //            //                item.Empresa = ((PrestadorServico)s.Papel).Fornecedor.NomeFantasia;
            //            //        }
            //            //    }

            //            //    if (s.Papel is Visitante)
            //            //    {
            //            //        item.MaoDeObra = null;
            //            //        item.Papel = "Visitante";
            //            //        item.Empresa = ((Visitante)s.Papel).Empresa;
            //            //    }

            //            //    item.Cargo = (s.Papel is Visitante) ?
            //            //                    (s.Papel as Visitante).Funcao : s.Papel.Cargo != null ?
            //            //                                                        s.Papel.Cargo.Nome : null;

            //            //    AlocacaoColaborador alocacao = aloc.Where(a => a.Papel.Codigo == s.Papel.Codigo).OrderByDescending(a => a.DataRegistro).FirstOrDefault();
            //            //    PedidoCompra alocada = alocacao != null ? alocacao.ObterContratada() : null;

            //            //    if (alocacao != null)
            //            //    {
            //            //        item.TipoSetor = alocacao.SetorCusto != null ? alocacao.SetorCusto.TipoSetor.ObterDescricaoEnum() : "";
            //            //        item.Setor = alocacao.SetorCusto != null ? alocacao.SetorCusto.Nome : "";
            //            //        item.Contratada = alocada.Empresa != null ? alocada.Empresa.Nome : "";
            //            //        /*item.Contratada = alocacao.PedidoCompra.PedidoCompraPai != null ? alocacao.PedidoCompra.PedidoCompraPai.Empresa.Apelido :
            //            //                            alocacao.PedidoCompra.PedidoCompraPai == null ? alocacao.PedidoCompra.Empresa.Nome : "";*/
            //            //    }
            //            //}

            //            if (s.Papel != null)
            //            {
            //                item.Nome = s.Papel.PessoaFisica.Nome;
            //                item.Papel = s.Papel is Colaborador ? "Colaborador" : (s.Papel is PrestadorServico ? "Prestador de Serviço" : (s.Papel is Visitante ? "Visitante" : ""));
            //                item.CPF = s.Papel.PessoaFisica.CPFFormatado();
            //                var papelzin = s.Papel.PapelLogs.LastOrDefault().Papel;
            //                var papelClasse = papelzin.GetType().ToString();
            //                var papelClasse2 = papelzin.GetType().Name;
            //                if (s.Papel.PapelLogs.LastOrDefault().Papel is Colaborador)
            //                {
            //                    item.Papel = "Colaborador";
            //                    item.Empresa = ((Colaborador)s.Papel).Empresa.Apelido;
            //                    //item.EmpresaCNPJ = ((Colaborador)s.Papel).Empresa.CNPJ;

            //                    IRepositorioColaborador repColaborador = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);
            //                    item.MaoDeObra = repColaborador.ObterPorId(s.Papel.Codigo).MaoDeObra.ToString();

            //                    if (s.Papel.EmpregadoSAP != null)
            //                    {
            //                        item.MatriculaEmpregadoSAP = string.Format("{0}", s.Papel.EmpregadoSAP.Codigo.ToString());
            //                        item.MatriculaGestorPonto = s.Papel.EmpregadoSAP.GestorPonto != null ? s.Papel.EmpregadoSAP.GestorPonto.Codigo.ToString() : string.Empty;
            //                        item.MatriculaSuperiorImediato = s.Papel.EmpregadoSAP.SuperiorImediato != null ? s.Papel.EmpregadoSAP.SuperiorImediato.Codigo.ToString() : string.Empty;
            //                    }
            //                    IRepositorioGrupoTrabalhoColab repGrpTrabColab = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoTrabalhoColab>(UnidadeTrabalho);
            //                    var grpTrabColab = repGrpTrabColab.ObterTodos().OrderByDescending(gtc => gtc.GrupoTrabalho.Codigo)
            //                        .FirstOrDefault(gtc => gtc.Ativo && gtc.Papel.Codigo.Equals(s.Papel.Codigo));
            //                    if (grpTrabColab != null && grpTrabColab.Codigo > 0)
            //                        item.NumeroPHT = grpTrabColab.GrupoTrabalho.CodigoPHT;
            //                }

            //                if (s.Papel is PrestadorServico)
            //                {
            //                    IRepositorioPrestadorServico repPrestadorServico = Fabrica.Instancia.ObterRepositorio<IRepositorioPrestadorServico>(UnidadeTrabalho);
            //                    item.MaoDeObra = repPrestadorServico.ObterPorId(s.Papel.Codigo).MaoDeObra.ToString();

            //                    item.Papel = "Prestador de Serviço";
            //                    if (((PrestadorServico)s.Papel).TipoPrestador == TipoPrestador.Temporario)
            //                    {
            //                        if (((PrestadorServico)s.Papel).Empresa != null)
            //                            item.Empresa = ((PrestadorServico)s.Papel).Empresa.Apelido;
            //                        // item.EmpresaCNPJ = ((PrestadorServico)s.Papel).Empresa.CNPJ;
            //                    }
            //                    else
            //                    {
            //                        if (((PrestadorServico)s.Papel).Fornecedor != null)
            //                            item.Empresa = ((PrestadorServico)s.Papel).Fornecedor.NomeFantasia;
            //                    }
            //                }

            //                if (s.Papel is Visitante)
            //                {
            //                    item.MaoDeObra = null;
            //                    item.Papel = "Visitante";
            //                    item.Empresa = ((Visitante)s.Papel).Empresa;
            //                }

            //                item.Cargo = (s.Papel is Visitante) ?
            //                                (s.Papel as Visitante).Funcao : s.Papel.Cargo != null ?
            //                                                                    s.Papel.Cargo.Nome : null;

            //                AlocacaoColaborador alocacao = aloc.Where(a => a.Papel.Codigo == s.Papel.Codigo).OrderByDescending(a => a.DataRegistro).FirstOrDefault();
            //                PedidoCompra alocada = alocacao != null ? alocacao.ObterContratada() : null;

            //                if (alocacao != null)
            //                {
            //                    item.TipoSetor = alocacao.SetorCusto != null ? alocacao.SetorCusto.TipoSetor.ObterDescricaoEnum() : "";
            //                    item.Setor = alocacao.SetorCusto != null ? alocacao.SetorCusto.Nome : "";
            //                    item.Contratada = alocada.Empresa != null ? alocada.Empresa.Nome : "";
            //                    /*item.Contratada = alocacao.PedidoCompra.PedidoCompraPai != null ? alocacao.PedidoCompra.PedidoCompraPai.Empresa.Apelido :
            //                                        alocacao.PedidoCompra.PedidoCompraPai == null ? alocacao.PedidoCompra.Empresa.Nome : "";*/
            //                    //item.ContratadaCNPJ = alocacao.PedidoCompra.PedidoCompraPai != null ? alocacao.PedidoCompra.PedidoCompraPai.Empresa.CNPJ :
            //                    // alocacao.PedidoCompra.PedidoCompraPai == null ? alocacao.PedidoCompra.Empresa.CNPJ : "";
            //                }
            //            }

            //            list.Add(item);
            //        }

            //        return list;
        }
        //    {
        //        if (!ignoreParameters)
        //        {
        //            if (parameters == null)
        //                parameters = new StoreRequestParameters();
        //            controleAcessos = controleAcessos.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 25);
        //        }

        //        List<AcessoVeiculoModelView> list = new List<AcessoVeiculoModelView>();

        //        foreach (var s in controleAcessos)
        //        {
        //            if (s.CamerasImagens == null || s.CamerasImagens.Count == 0)
        //            {
        //                IRepositorioCameraImagem repositorio = Fabrica.Instancia.Obter<IRepositorioCameraImagem>();
        //                s.CamerasImagens = repositorio.ObterTodos().Where(ci => ci.AcessoVeiculo.Codigo == s.Codigo).ToList();
        //            }

        //            AcessoVeiculoModelView item = new AcessoVeiculoModelView();
        //            item.Nome = (s.Papel != null ? s.Papel.PessoaFisica.Nome : "");

        //            item.Codigo = (s.Papel != null ? s.Papel.Codigo.ToString() : "");
        //            item.NumCracha = (s.Cracha != null) ? s.Cracha.Numero: null;
        //            var status = "";
        //            if (s.Leitora != null)
        //            {
        //                item.Leitora = s.Leitora.Descricao;
        //                item.Local = s.Leitora.Local;
        //                item.Direcao = s.Leitora.Direcao.ObterDescricaoEnum();
        //            }
        //            item.DataHora = s.DataRegistro;
        //           // status = 
        //            item.StatusAcesso = s.StatusAcesso.ObterDescricaoEnum();
        //            //switch (status)
        //            //{
        //            //    case "OCO_UR_CONTROLE_ACESSO_AUTORIZADO_LPR_ATIVO_ACIMA":

        //            //    case "OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_LPR_INATIVO":

        //            //    case "OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_INATIVA":

        //            //    case "OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_ATIVA_LPR_INATIVO":

        //            //    case "OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_OFFLINE":

        //            //    case "OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERAS_NAO_VINCULADAS":

        //            //    case "OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_ATIVA_LPR_INATIVO_PARAMETRO":

        //            //    case "OCO_UR_CONTROLE_ACESSO_VEICULO_VALIDACAO_DESATIVADA":
        //            //        item.StatusAcesso = "Acesso Liberado";
        //            //        break;

        //            //    default:
        //            //        item.StatusAcesso = "Acesso negado";
        //            //        break;
        //            //}



        //            //
        //            if (s.VeiculoCredencialVeiculo != null)
        //            {
        //                if (s.VeiculoCredencialVeiculo.Veiculo != null)
        //                {
        //                    item.Cor = s.VeiculoCredencialVeiculo.Veiculo.Cor;
        //                    if (s.VeiculoCredencialVeiculo.Veiculo.Modelo != null)
        //                    {
        //                        item.Modelo = s.VeiculoCredencialVeiculo.Veiculo.Modelo.Nome;
        //                        if (s.VeiculoCredencialVeiculo.Veiculo.Modelo.Fabricante != null)
        //                        {
        //                            item.Fabricante = s.VeiculoCredencialVeiculo.Veiculo.Modelo.Fabricante.Nome;
        //                        }
        //                        else
        //                        {
        //                            item.Fabricante = "";
        //                        }
        //                    }
        //                    else
        //                    {
        //                        item.Modelo = "";
        //                    }
        //                }
        //                else
        //                {
        //                    item.Cor = "";
        //                }
        //            }

        //            //Pegar placa com maior precisão
        //            if (s.CamerasImagens != null && s.CamerasImagens.Count > 0)
        //            {
        //                IRepositorioParametro repositorioParametro = Fabrica.Instancia.ObterRepositorio<IRepositorioParametro>(UnidadeTrabalho);
        //                var caminhoImagem = repositorioParametro.ObterTodos().Where(p => p.Nome == ParametroSistema.CAMINHO_IMAGEM).SingleOrDefault().Valor;
        //                float precisaoAnterior = 0;

        //                for (int i = 0; i < s.CamerasImagens.Count; i++)
        //                {
        //                    if (String.IsNullOrEmpty(s.CamerasImagens[i].Camera.Nome) || s.CamerasImagens[i].Camera.Nome != "")
        //                    {
        //                        item.Camera = s.CamerasImagens[i].Camera.Nome;
        //                    }

        //                    string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority +
        //Request.ApplicationPath.TrimEnd('/') + "/";
        //                    if(s.CamerasImagens[i].CaminhoImagem  != null)
        //                    {

        //                        //s.CamerasImagens[i].CaminhoImagem.Replace("C:\\Users\\denis.nogueira\\Downloads\\", baseUrl + "\\Imagens\\"
        //                        //Cam_LPR FRONTAL ENTRADA TESTE_09_05_2019_05_17_02
        //                        //var caminho = @"<img src='" + baseUrl + "\\Imagens\\Cam_LPR FRONTAL ENTRADA TESTE_09_05_2019_04_27_50.jpg\">";

        //                        //item.Imagens += @"<img src='" + baseUrl + "\\Imagens\\Cam_LPR FRONTAL ENTRADA TESTE_09_05_2019_04_27_50.jpg\">";

        //                        item.Imagens += "<img style=\"margin-left 10%;\" src=\"" + caminhoImagem + s.CamerasImagens[i].CaminhoImagem + "\">"; ;
        //                        //item.Imagens += "<img style=\"margin-left 10%;\" src=\"" + baseUrl + "Imagens/Cam_LPR FRONTAL ENTRADA TESTE_09_05_2019_04_27_50.jpg\">";
        //                    }




        //                    if (s.CamerasImagens[i].PrecisaoImage > precisaoAnterior)
        //                    {
        //                        if(String.IsNullOrEmpty(s.CamerasImagens[i].Camera.Nome) || s.CamerasImagens[i].Camera.Nome != "")
        //                        {
        //                            item.Camera = s.CamerasImagens[i].Camera.Nome;
        //                        }

        //                        item.Placa = s.CamerasImagens[i].PlacaVeiculo;
        //                        precisaoAnterior = s.CamerasImagens[i].PrecisaoImage;

        //                    }
        //                }
        //            }
        //            else
        //            {
        //                item.Placa = "";
        //            }



        //            //if (s.Papel != null)
        //            //{
        //            //    item.Nome = s.Papel.PessoaFisica.Nome;
        //            //    item.Papel = s.Papel is Colaborador ? "Colaborador" : (s.Papel is PrestadorServico ? "Prestador de Serviço" : (s.Papel is Visitante ? "Visitante" : ""));
        //            //    item.CPF = s.Papel.PessoaFisica.CPFFormatado();

        //            //    if (s.Papel is Colaborador)
        //            //    {
        //            //        item.Papel = "Colaborador";
        //            //        item.Empresa = ((Colaborador)s.Papel).Empresa.Apelido;

        //            //        IRepositorioColaborador repColaborador = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);
        //            //        item.MaoDeObra = repColaborador.ObterPorId(s.Papel.Codigo).MaoDeObra.ToString();

        //            //        if (s.Papel.EmpregadoSAP != null)
        //            //        {
        //            //            item.MatriculaEmpregadoSAP = string.Format("{0}", s.Papel.EmpregadoSAP.Codigo.ToString());
        //            //            item.MatriculaGestorPonto = s.Papel.EmpregadoSAP.GestorPonto != null ? s.Papel.EmpregadoSAP.GestorPonto.Codigo.ToString() : string.Empty;
        //            //            item.MatriculaSuperiorImediato = s.Papel.EmpregadoSAP.SuperiorImediato != null ? s.Papel.EmpregadoSAP.SuperiorImediato.Codigo.ToString() : string.Empty;
        //            //        }
        //            //        IRepositorioGrupoTrabalhoColab repGrpTrabColab = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoTrabalhoColab>(UnidadeTrabalho);
        //            //        var grpTrabColab = repGrpTrabColab.ObterTodos().OrderByDescending(gtc => gtc.GrupoTrabalho.Codigo)
        //            //            .FirstOrDefault(gtc => gtc.Ativo && gtc.Papel.Codigo.Equals(s.Papel.Codigo));
        //            //        if (grpTrabColab != null && grpTrabColab.Codigo > 0)
        //            //            item.NumeroPHT = grpTrabColab.GrupoTrabalho.CodigoPHT;
        //            //    }

        //            //    if (s.Papel is PrestadorServico)
        //            //    {
        //            //        IRepositorioPrestadorServico repPrestadorServico = Fabrica.Instancia.ObterRepositorio<IRepositorioPrestadorServico>(UnidadeTrabalho);
        //            //        item.MaoDeObra = repPrestadorServico.ObterPorId(s.Papel.Codigo).MaoDeObra.ToString();

        //            //        item.Papel = "Prestador de Serviço";
        //            //        if (((PrestadorServico)s.Papel).TipoPrestador == TipoPrestador.Temporario)
        //            //        {
        //            //            if (((PrestadorServico)s.Papel).Empresa != null)
        //            //                item.Empresa = ((PrestadorServico)s.Papel).Empresa.Apelido;
        //            //        }
        //            //        else
        //            //        {
        //            //            if (((PrestadorServico)s.Papel).Fornecedor != null)
        //            //                item.Empresa = ((PrestadorServico)s.Papel).Fornecedor.NomeFantasia;
        //            //        }
        //            //    }

        //            //    if (s.Papel is Visitante)
        //            //    {
        //            //        item.MaoDeObra = null;
        //            //        item.Papel = "Visitante";
        //            //        item.Empresa = ((Visitante)s.Papel).Empresa;
        //            //    }

        //            //    item.Cargo = (s.Papel is Visitante) ?
        //            //                    (s.Papel as Visitante).Funcao : s.Papel.Cargo != null ?
        //            //                                                        s.Papel.Cargo.Nome : null;

        //            //    AlocacaoColaborador alocacao = aloc.Where(a => a.Papel.Codigo == s.Papel.Codigo).OrderByDescending(a => a.DataRegistro).FirstOrDefault();
        //            //    PedidoCompra alocada = alocacao != null ? alocacao.ObterContratada() : null;

        //            //    if (alocacao != null)
        //            //    {
        //            //        item.TipoSetor = alocacao.SetorCusto != null ? alocacao.SetorCusto.TipoSetor.ObterDescricaoEnum() : "";
        //            //        item.Setor = alocacao.SetorCusto != null ? alocacao.SetorCusto.Nome : "";
        //            //        item.Contratada = alocada.Empresa != null ? alocada.Empresa.Nome : "";
        //            //        /*item.Contratada = alocacao.PedidoCompra.PedidoCompraPai != null ? alocacao.PedidoCompra.PedidoCompraPai.Empresa.Apelido :
        //            //                            alocacao.PedidoCompra.PedidoCompraPai == null ? alocacao.PedidoCompra.Empresa.Nome : "";*/
        //            //    }
        //            //}

        //            if (s.Papel != null)
        //            {
        //                item.Nome = s.Papel.PessoaFisica.Nome;
        //                item.Papel = s.Papel is Colaborador ? "Colaborador" : (s.Papel is PrestadorServico ? "Prestador de Serviço" : (s.Papel is Visitante ? "Visitante" : ""));
        //                item.CPF = s.Papel.PessoaFisica.CPFFormatado();
        //                var papelzin = s.Papel.PapelLogs.LastOrDefault().Papel;
        //                var papelClasse = papelzin.GetType().ToString();
        //                var papelClasse2 = papelzin.GetType().Name;
        //                if (s.Papel.PapelLogs.LastOrDefault().Papel is Colaborador)
        //                {
        //                    item.Papel = "Colaborador";
        //                    item.Empresa = ((Colaborador)s.Papel).Empresa.Apelido;
        //                    //item.EmpresaCNPJ = ((Colaborador)s.Papel).Empresa.CNPJ;

        //                    IRepositorioColaborador repColaborador = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);
        //                    item.MaoDeObra = repColaborador.ObterPorId(s.Papel.Codigo).MaoDeObra.ToString();

        //                    if (s.Papel.EmpregadoSAP != null)
        //                    {
        //                        item.MatriculaEmpregadoSAP = string.Format("{0}", s.Papel.EmpregadoSAP.Codigo.ToString());
        //                        item.MatriculaGestorPonto = s.Papel.EmpregadoSAP.GestorPonto != null ? s.Papel.EmpregadoSAP.GestorPonto.Codigo.ToString() : string.Empty;
        //                        item.MatriculaSuperiorImediato = s.Papel.EmpregadoSAP.SuperiorImediato != null ? s.Papel.EmpregadoSAP.SuperiorImediato.Codigo.ToString() : string.Empty;
        //                    }
        //                    IRepositorioGrupoTrabalhoColab repGrpTrabColab = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoTrabalhoColab>(UnidadeTrabalho);
        //                    var grpTrabColab = repGrpTrabColab.ObterTodos().OrderByDescending(gtc => gtc.GrupoTrabalho.Codigo)
        //                        .FirstOrDefault(gtc => gtc.Ativo && gtc.Papel.Codigo.Equals(s.Papel.Codigo));
        //                    if (grpTrabColab != null && grpTrabColab.Codigo > 0)
        //                        item.NumeroPHT = grpTrabColab.GrupoTrabalho.CodigoPHT;
        //                }

        //                if (s.Papel is PrestadorServico)
        //                {
        //                    IRepositorioPrestadorServico repPrestadorServico = Fabrica.Instancia.ObterRepositorio<IRepositorioPrestadorServico>(UnidadeTrabalho);
        //                    item.MaoDeObra = repPrestadorServico.ObterPorId(s.Papel.Codigo).MaoDeObra.ToString();

        //                    item.Papel = "Prestador de Serviço";
        //                    if (((PrestadorServico)s.Papel).TipoPrestador == TipoPrestador.Temporario)
        //                    {
        //                        if (((PrestadorServico)s.Papel).Empresa != null)
        //                            item.Empresa = ((PrestadorServico)s.Papel).Empresa.Apelido;
        //                       // item.EmpresaCNPJ = ((PrestadorServico)s.Papel).Empresa.CNPJ;
        //                    }
        //                    else
        //                    {
        //                        if (((PrestadorServico)s.Papel).Fornecedor != null)
        //                            item.Empresa = ((PrestadorServico)s.Papel).Fornecedor.NomeFantasia;
        //                    }
        //                }

        //                if (s.Papel is Visitante)
        //                {
        //                    item.MaoDeObra = null;
        //                    item.Papel = "Visitante";
        //                    item.Empresa = ((Visitante)s.Papel).Empresa;
        //                }

        //                item.Cargo = (s.Papel is Visitante) ?
        //                                (s.Papel as Visitante).Funcao : s.Papel.Cargo != null ?
        //                                                                    s.Papel.Cargo.Nome : null;

        //                AlocacaoColaborador alocacao = aloc.Where(a => a.Papel.Codigo == s.Papel.Codigo).OrderByDescending(a => a.DataRegistro).FirstOrDefault();
        //                PedidoCompra alocada = alocacao != null ? alocacao.ObterContratada() : null;

        //                if (alocacao != null)
        //                {
        //                    item.TipoSetor = alocacao.SetorCusto != null ? alocacao.SetorCusto.TipoSetor.ObterDescricaoEnum() : "";
        //                    item.Setor = alocacao.SetorCusto != null ? alocacao.SetorCusto.Nome : "";
        //                    item.Contratada = alocada.Empresa != null ? alocada.Empresa.Nome : "";
        //                    /*item.Contratada = alocacao.PedidoCompra.PedidoCompraPai != null ? alocacao.PedidoCompra.PedidoCompraPai.Empresa.Apelido :
        //                                        alocacao.PedidoCompra.PedidoCompraPai == null ? alocacao.PedidoCompra.Empresa.Nome : "";*/
        //                    //item.ContratadaCNPJ = alocacao.PedidoCompra.PedidoCompraPai != null ? alocacao.PedidoCompra.PedidoCompraPai.Empresa.CNPJ :
        //                   // alocacao.PedidoCompra.PedidoCompraPai == null ? alocacao.PedidoCompra.Empresa.CNPJ : "";
        //                }
        //            }

        //            list.Add(item);
        //        }

        //        return list;
        //    }

        /// <summary>
        /// Filtrar acessos.
        /// </summary>
        ///
        /// <param name="tipoSetor">
        /// O(a) tipo setor.
        /// </param>
        /// <param name="status">
        /// O(a) status.
        /// </param>
        /// <param name="nome">
        /// O(a) nome.
        /// </param>
        /// <param name="cpf">
        /// O(a) cpf.
        /// </param>
        /// <param name="cracha">
        /// O(a) cracha.
        /// </param>
        /// <param name="empresa">
        /// O(a) empresa.
        /// </param>
        /// <param name="cargo">
        /// O(a) cargo.
        /// </param>
        /// <param name="dtInicio">
        /// O(a) dt inicio.
        /// </param>
        /// <param name="dtFim">
        /// O(a) dt fim.
        /// </param>
        /// <param name="setor">
        /// O(a) setor.
        /// </param>
        ///
        /// <returns>
        /// Um(a) IQueryable&lt;ControleAcesso&gt;
        /// </returns>
        public IQueryable<AcessoVeiculo> FiltrarAcessos(TipoSetorCusto? tipoSetor, List<StatusAcesso> status, IQueryable<AlocacaoColaborador> aloc, string nome = null, string cpf = null, string cracha = null, string empresa = null, int cargo = 0, DateTime? dtInicio = null, DateTime? dtFim = null, int setor = 0, List<int> leitoras = null, TipoPapel? papel = null, string userLogado = null, bool validaGestor = false, long? empregadoSap = null, long? gestorPonto = null, long? superiorImediato = null, string codigoPht = null, IList<string> localLeitora = null, string passaporte = null, string maodeobra = null, string fornecedor = null, string contratada = null, string placa = null, string modeloCredencial = null, string cor = null, string modeloVeiculo = null, string fabricante = null, string nomeCamera = null)
        {
            IRepositorioAcessoVeiculo rep = Fabrica.Instancia.ObterRepositorio<IRepositorioAcessoVeiculo>(UnidadeTrabalho);

            if (HttpContext != null && HttpContext.User != null && HttpContext.User.Identity != null)
                userLogado = userLogado ?? HttpContext.User.Identity.Name;

            cpf = cpf.Replace(".", "").Replace("-", "");
            IQueryable<AcessoVeiculo> listControleAc = (tipoSetor != null || setor != 0 || (!string.IsNullOrEmpty(contratada))) ?
                rep.ObterListaAcesso(status, cracha, cpf, empresa, nome, cargo, dtInicio, dtFim, userLogado, leitoras, aloc, validaGestor, empregadoSap, gestorPonto, superiorImediato, codigoPht, localLeitora: localLeitora, passaporte: passaporte, maodeobra: maodeobra, fornecedor: fornecedor, contratada: contratada, placa: placa, modeloCredencial: modeloCredencial, cor: cor, modeloVeiculo: modeloVeiculo, fabricante: fabricante, nomeCamera: nomeCamera) :
                rep.ObterListaAcesso(status, cracha, cpf, empresa, nome, cargo, dtInicio, dtFim, userLogado, leitoras, validaGestor: validaGestor, empregadoSap: empregadoSap, gestorPonto: gestorPonto, superiorImediato: superiorImediato, codigoPht: codigoPht, localLeitora: localLeitora, passaporte: passaporte, maodeobra: maodeobra, fornecedor: fornecedor, contratada: contratada, placa: placa, modeloCredencial: modeloCredencial, cor: cor, modeloVeiculo: modeloVeiculo, fabricante: fabricante, nomeCamera: nomeCamera);

            if (papel.HasValue)
            {
                switch (papel.Value)
                {
                    case TipoPapel.Visitante:
                        listControleAc = listControleAc.Where(x => x.Papel is Visitante);
                        break;
                    case TipoPapel.Colaborador:
                        listControleAc = listControleAc.Where(x => x.Papel is Colaborador);
                        break;
                    case TipoPapel.PrestadorServico:
                        listControleAc = listControleAc.Where(x => x.Papel is PrestadorServico);
                        break;
                    default:
                        break;
                }
            }

            return listControleAc.OrderByDescending(c => c.DataRegistro);
        }

        /// <summary>
        /// Filtrar alocacao colaboradores.
        /// </summary>
        ///
        /// <param name="tipoSetor">
        /// O(a) tipo setor.
        /// </param>
        /// <param name="setor">
        /// O(a) setor.
        /// </param>
        ///
        /// <returns>
        /// Um(a) IQueryable&lt;AlocacaoColaborador&gt;
        /// </returns>
        public IQueryable<AlocacaoColaborador> FiltrarAlocacaoColaboradores(TipoSetorCusto? tipoSetor, int setor = 0, string contratada = null)
        {
            IRepositorioAlocacaoColaborador repAlocacao = Fabrica.Instancia.ObterRepositorio<IRepositorioAlocacaoColaborador>(UnidadeTrabalho);
            IQueryable<AlocacaoColaborador> aloc = repAlocacao.ObterTodos();

            if (tipoSetor != null || setor != 0 || !string.IsNullOrEmpty(contratada))
            {
                if (tipoSetor != null)
                    aloc = aloc.Where(p => p.SetorCusto.TipoSetor == tipoSetor);

                if (setor != 0)
                    aloc = aloc.Where(p => p.SetorCusto.Codigo == setor);

                if (!string.IsNullOrEmpty(contratada))
                {
                    var query = this.UnidadeTrabalho.ExecuteSql<Models.PedidosContratadaModelView>("SELECT * FROM VW_PedidosContratada ", new Dictionary<string, object>()).AsQueryable();

                    List<int> listaAux = query.Where(x => x.CONTRATADA == contratada).Select(pc => pc.CD_PEDIDO).ToList();

                    aloc = aloc.Where(p => listaAux.Contains(p.PedidoCompra.Codigo));
                }
            }
            return aloc;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult ListaStatusAcesso()
        {
            Dictionary<int, string> dic = new Dictionary<int, string>();

            foreach (int item in Enum.GetValues(typeof(StatusAcesso)))
            {
                dic.Add(item, ((StatusAcesso)item).ObterDescricaoEnum());
            }
            return Json(dic.Select(d => new
            {
                Codigo = d.Key,
                Descricao = d.Value
            }).OrderBy(x => x.Descricao).ToList());

        }
    }
}
