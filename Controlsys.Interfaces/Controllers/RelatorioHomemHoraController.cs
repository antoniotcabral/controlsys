﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Empresas;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Acesso;
using Controlsys.Repositorios.Empresas;
using Ext.Net;
using Globalsys;
using Globalsys.Relatorios;
using Ext.Net.MVC;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar relatorio homem horas.
    /// </summary>
    [ExtendController]
    public class RelatorioHomemHoraController : Controller, IControllerBase
    {
        /// <summary>
        /// Gets/Sets valor para UnidadeTrabalho.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.RelatorioHomemHoraController.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RelatorioHomemHoraController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        /// <summary>
        /// GET: /RelatorioHomemHora/.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Exportar.
        /// </summary>
        ///
        /// <param name="selectedFields">
        /// The selected fields.
        /// </param>
        /// <param name="tipoRelatorio">
        /// O(a) tipo relatorio.
        /// </param>
        /// <param name="responsavel">
        /// O(a) responsavel.
        /// </param>
        /// <param name="empresa">
        /// O(a) empresa.
        /// </param>
        /// <param name="setor">
        /// O(a) setor.
        /// </param>
        /// <param name="dataInicio">
        /// O(a) data inicio.
        /// </param>
        /// <param name="dataFim">
        /// O(a) data fim.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Exportar(List<string> selectedFields, string tipoRelatorio, string responsavel, string empresa, int? setor, DateTime? dataInicio, DateTime? dataFim)
        {
            var relatorios = FiltrarRelatorioHomemHora(responsavel, empresa, setor, dataInicio, dataFim);
            var regs = montaObj(relatorios, null, true);

            ReportConfig reportConfig = ReportHelper.RenderReport<RelatorioHomemHoraModelView>(selectedFields, tipoRelatorio, regs);

            string fileName = string.Empty;

            switch (tipoRelatorio)
            {
                case "PDF":
                    fileName = "download.pdf";
                    break;
                case "Excel":
                    fileName = "download.xls";
                    break;
                case "Word":
                    fileName = "download.doc";
                    break;
            }

            return File(reportConfig.Bytes, reportConfig.MimeType, fileName);
        }

        /// <summary>
        /// Pesquisar.
        /// </summary>
        ///
        /// <param name="parameters">
        /// Options for controlling the operation.
        /// </param>
        /// <param name="responsavel">
        /// O(a) responsavel.
        /// </param>
        /// <param name="empresa">
        /// O(a) empresa.
        /// </param>
        /// <param name="setor">
        /// O(a) setor.
        /// </param>
        /// <param name="dataInicio">
        /// O(a) data inicio.
        /// </param>
        /// <param name="dataFim">
        /// O(a) data fim.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Pesquisar(StoreRequestParameters parameters, string responsavel, string empresa, int? setor, DateTime? dataInicio, DateTime? dataFim)
        {
            IEnumerable<RelatorioHomemHoraModelView> regs = FiltrarRelatorioHomemHora(responsavel, empresa, setor, dataInicio, dataFim);

            int total = regs.Count();
            List<RelatorioHomemHoraModelView> data = montaObj(regs, parameters);
            
            return new StoreResult(data, total);
        }

        /// <summary>
        /// Filtrar relatorio homem hora.
        /// </summary>
        ///
        /// <param name="responsavel">
        /// O(a) responsavel.
        /// </param>
        /// <param name="empresa">
        /// O(a) empresa.
        /// </param>
        /// <param name="setor">
        /// O(a) setor.
        /// </param>
        /// <param name="dataInicio">
        /// O(a) data inicio.
        /// </param>
        /// <param name="dataFim">
        /// O(a) data fim.
        /// </param>
        ///
        /// <returns>
        /// Um(a) IEnumerable&lt;RelatorioHomemHoraModelView&gt;
        /// </returns>
        private IEnumerable<RelatorioHomemHoraModelView> FiltrarRelatorioHomemHora(string responsavel, string empresa, int? setor, DateTime? dataInicio, DateTime? dataFim)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();

            parameters.Add("dtInicio", dataInicio.Value);
            parameters.Add("dtFim", dataFim.Value);
            parameters.Add("responsavel", responsavel);
            parameters.Add("setor", setor ?? 0);
            parameters.Add("sempresa", empresa);

            var relHHT = UnidadeTrabalho.ExecuteSqlHHT<RelatorioHomemHoraModelView>("EXEC sp_relatorio_hht :dtInicio, :dtFim, :responsavel, :setor, :sempresa", parameters);

            return relHHT;
        }

        /// <summary>
        /// Cria um objeto contendo informações relacionadas ao conteudo gerenciado por
        /// RelatorioHomemHoraController.
        /// </summary>
        ///
        /// <param name="regs">
        /// O(a) regs.
        /// </param>
        /// <param name="parameters">
        /// Options for controlling the operation.
        /// </param>
        /// <param name="ignoreParameters">
        /// true to ignore parameters.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;RelatorioHomemHoraModelView&gt;
        /// </returns>
        private List<RelatorioHomemHoraModelView> montaObj(IEnumerable<RelatorioHomemHoraModelView> regs, StoreRequestParameters parameters = null, bool ignoreParameters = false)
        {
            if (!ignoreParameters)
            {
                if (parameters == null)
                    parameters = new StoreRequestParameters();
                regs = regs.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 25);
            }
            
            return regs.ToList();
        }

    }
}
