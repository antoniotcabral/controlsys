﻿using Controlsys.Dominio.Parametros;
using Controlsys.Infra;
using Controlsys.Repositorio.Parametros;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class PenalidadeController : Controller, IControllerBase
    {

        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        public PenalidadeController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Inclui um novo penalidade.
        /// </summary>
        ///
        /// <param name="penalidade">
        /// O(a) penalidade.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Incluir(Penalidade penalidade)
        {
            IRepositorioPenalidade repPenalidade = Fabrica.Instancia.ObterRepositorio<IRepositorioPenalidade>(UnidadeTrabalho);
            penalidade.DataRegistro = DateTime.Now;
            penalidade.Ativar();
            repPenalidade.Validar(penalidade, EstadoObjeto.Novo);
            repPenalidade.Salvar(penalidade);
            return Json(penalidade);

        }

        /// <summary>
        /// Altera um registro de penalidade.
        /// </summary>
        ///
        /// <param name="penalidade">
        /// O(a) penalidade.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Alterar(Penalidade penalidade)
        {
            IRepositorioPenalidade repPenalidade = Fabrica.Instancia.ObterRepositorio<IRepositorioPenalidade>(UnidadeTrabalho);
            Penalidade penalidadeA = repPenalidade.ObterPorId(penalidade.Codigo);
            penalidadeA.Nome = penalidade.Nome;
            penalidadeA.Sigla = penalidade.Sigla;
            repPenalidade.Validar(penalidadeA, EstadoObjeto.Alterado);
            repPenalidade.Atualizar(penalidadeA);

            return Json(penalidadeA);
        }

        /// <summary>
        /// Ativar inativar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult AtivarInativar(int codigo)
        {
            IRepositorioPenalidade repPenalidade = Fabrica.Instancia.ObterRepositorio<IRepositorioPenalidade>(UnidadeTrabalho);
            Penalidade penalidade = repPenalidade.ObterPorId(codigo);
            if (penalidade.Ativo)
            {
                repPenalidade.Validar(penalidade, EstadoObjeto.Inativado);
                penalidade.Inativar();
            }
            else
            {
                repPenalidade.Validar(penalidade, EstadoObjeto.Ativado);
                penalidade.Ativar();
            }
            repPenalidade.Atualizar(penalidade);
            return Json(penalidade);
        }

        /// <summary>
        /// Selecionar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Selecionar(int codigo)
        {
            IRepositorioPenalidade repPenalidade = Fabrica.Instancia.ObterRepositorio<IRepositorioPenalidade>(UnidadeTrabalho);
            Penalidade penalidade = repPenalidade.ObterPorId(codigo);
            return Json(penalidade);
        }

        /// <summary>
        /// Retorna todos os objetos do tipo penalidade.
        /// </summary>
        ///
        /// <param name="apenasAtivos">
        /// true to apenas ativos.
        /// </param>
        ///
        /// <returns>
        /// Detalhes do tipo de objeto ligado ao controller no formato JSON.
        /// </returns>
        public ActionResult ObterTodos(bool apenasAtivos = false)
        {
            IRepositorioPenalidade repPenalidade = Fabrica.Instancia.ObterRepositorio<IRepositorioPenalidade>(UnidadeTrabalho);

            IQueryable<Penalidade> penalidades = repPenalidade.ObterTodos();

            if (apenasAtivos)
            {
                penalidades = penalidades.Where(t => t.Ativo == true);
            }

            return Json(penalidades.ToList().Select(i => new
            {
                Codigo = i.Codigo,
                Nome = i.Nome,
                Sigla = i.Sigla,
                SiglaNome = (i.Sigla + " - " + i.Nome),
                Ativo = i.Ativo
            }).ToList().OrderBy(p => p.SiglaNome));
        }
    }
}
