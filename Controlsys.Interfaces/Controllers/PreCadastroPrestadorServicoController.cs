﻿using Controlsys.Dominio.AgendamentosPessoa;
using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Enderecos;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Controlsys.Dominio.Seguranca;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Agendamentos;
using Controlsys.Repositorio.Parametros;
using Controlsys.Repositorios.Empresas;
using Controlsys.Repositorios.Enderecos;
using Controlsys.Repositorios.Parametros;
using Controlsys.Repositorios.Pessoas;
using Controlsys.Repositorios.Seguranca;
using Ext.Net;
using Ext.Net.MVC;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class PreCadastroPrestadorServicoController : Controller, IControllerBase
    {

        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        public PreCadastroPrestadorServicoController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        public ActionResult Index()
        {
            IRepositorioPermissao repPermissao = Fabrica.Instancia.ObterRepositorio<IRepositorioPermissao>(UnidadeTrabalho);
            List<GrupoUsuario> gruposUsuarioPermissaoImportar = repPermissao.ObterTodos().Where(p => p.Acao.Url == "/PreCadastroPrestadorServico/ImportarModelo").SelectMany(p => p.Grupo.GruposUsuarios).ToList();
            List<string> pessoasFisicasPermissaoImportar = gruposUsuarioPermissaoImportar.Select(u => u.Usuario.PessoaFisica.Codigo).ToList();

            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            Usuario usuarioLogado = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name);

            if (pessoasFisicasPermissaoImportar.Contains(usuarioLogado.Codigo))
                return View("Index", model: true);

            return View("Index", model: false);
        }

        public ActionResult Selecionar(int codigo)
        {
            IRepositorioPreCadastroPrestador rep = Fabrica.Instancia.ObterRepositorio<IRepositorioPreCadastroPrestador>(UnidadeTrabalho);
            PrestadorServicoAgendamento preCad = rep.ObterPorId(codigo);

            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            Usuario usuarioLogado = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name, somenteAtivos: true);
            return Json(montaObj(preCad, usuarioLogado));
        }

        [Transaction]
        public ActionResult Incluir(PrestadorServicoAgendamento precadastro, string[] propModificadasPessoa, /*int codPedido, */int codGestor, List<DocumentoMidiaModelView> documentos, string path = null)
        {
            try
            {
                var repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
                var usuarioLogado = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name, somenteAtivos: true);

                var repPreCad = Fabrica.Instancia.ObterRepositorio<IRepositorioPreCadastroPrestador>(UnidadeTrabalho);
                var repPessoaAg = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaAgendamento>(UnidadeTrabalho);
                var repPapelAg = Fabrica.Instancia.ObterRepositorio<IRepositorioPapelAgendamento>(UnidadeTrabalho);
                var repColabPed = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);
                var repFornecedor = Fabrica.Instancia.ObterRepositorio<IRepositorioFornecedor>(UnidadeTrabalho);

                IRepositorioDocumento repDoc = Fabrica.Instancia.ObterRepositorio<IRepositorioDocumento>(UnidadeTrabalho);

                var responsavel = repColabPed.ObterTodos().Where(v => v.Codigo == codGestor).FirstOrDefault();
                if (responsavel == null) throw new CoreException("Não foi possível encontrar o cadastro do gestor informado.");
                precadastro.GestorResponsavel = responsavel;

                //var fornecedor = precadastro.Fornecedor != null
                //    ? repFornecedor.ObterPorId(precadastro.Fornecedor.Codigo)
                //    : null;

                precadastro.Fornecedor = precadastro.Fornecedor != null
                    ? repFornecedor.ObterPorId(precadastro.Fornecedor.Codigo)
                    : null;

                //precadastro.PessoaAg.Endereco = repLogradouro.ObterPorId(precadastro.PessoaAg.Endereco.Codigo);
                //precadastro.PessoaAg.Endereco = repEndereco.VerificarEndereco(end);
                //precadastro.PessoaAg.EnderecoNumero = precadastro.PessoaAg.EnderecoNumero;
                //precadastro.PessoaAg.EnderecoComplemento = precadastro.PessoaAg.EnderecoComplemento;
                //precadastro.PessoaAg.Endereco.DataRegistro = DateTime.Now;
                //precadastro.PessoaAg.Endereco.Ativar();

                if (precadastro.PessoaAg.CPF != null)
                    precadastro.PessoaAg.CPF = precadastro.PessoaAg.CPF.Replace("-", "").Replace(".", "");

                if (precadastro.PessoaAg.Codigo != 0)
                {
                    precadastro.PessoaAg = repPessoaAg.ObterPorId(precadastro.PessoaAg, propModificadasPessoa);
                    repPessoaAg.Atualizar(precadastro.PessoaAg);
                }
                else
                {
                    precadastro.PessoaAg.DataRegistro = DateTime.Now;
                    repPessoaAg.Salvar(precadastro.PessoaAg);
                }

                if (precadastro.Solicitante == null)                
                  precadastro.Solicitante = usuarioLogado;
                

                repPreCad.Validar(EstadoObjeto.Novo, precadastro);
                precadastro.DataRegistro = DateTime.Now;
                repPreCad.Salvar(precadastro);

                repPapelAg.SalvarStatusPapelAg(precadastro.InserirPapelAg(StatusAgendamento.AguardandoAutorizacao, null));

                #region Documentos
                if (documentos != null)
                { 
                foreach (var item in documentos)
                {
                    DocumentoPapel docPapel = repDoc.SalvarDocPapel(null, item.Documento, precadastro);
                    string pathUpload = ConfigurationManager.AppSettings.Get("pathUploads");

                    if (!System.IO.Directory.Exists(pathUpload))
                        throw new CoreException("O diretório onde a midia seria armazanada não está acessivel.");

                    IRepositorioDocumentoMidia repDocumentoMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioDocumentoMidia>(UnidadeTrabalho);
                    IRepositorioMidia repMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioMidia>(UnidadeTrabalho);

                    int cont = 1;

                    foreach (MidiaModelView midia in item.Midias ?? Enumerable.Empty<MidiaModelView>())
                    {
                        if (midia == null)
                            continue;
                        string nomeDisco = "PF-" + (string.IsNullOrEmpty(precadastro.PessoaAg.CPF) ? precadastro.PessoaAg.Passaporte : precadastro.PessoaAg.CPF) + "-" + precadastro.Codigo.ToString() + "-" + docPapel.Documento.TipoDocumento.Sigla + "-" + cont + "." + midia.Extensao;
                        Midia m = new Midia();
                        m.DataRegistro = midia.DataRegistro;
                        m.Extensao = midia.Extensao;
                        m.MimeType = midia.MIME;
                        m.NomeArquivo = nomeDisco;
                        repMidia.Salvar(m);

                        repDocumentoMidia.SalvarDocMidia(docPapel.Documento, m);

                        System.IO.File.WriteAllBytes(m.Url, Convert.FromBase64String(midia.Arquivo));

                        cont++;
                    }
                }
                }
                #endregion                

                NotificarGestor(precadastro);

                return Json(montaObj(precadastro));
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao tentar incluir o Pré-Cadastro de Prestador: " + ex.Message);                
            }
            
        }

        [Transaction]
        public ActionResult Alterar(PrestadorServicoAgendamento preCad, string[] propModificadasPessoa, string[] propModificadasDadosColab, /*int codPedido, */int codGestor, List<DocumentoMidiaModelView> documentosMidias)
        {
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            IRepositorioPapelAgendamento repPapelAg = Fabrica.Instancia.ObterRepositorio<IRepositorioPapelAgendamento>(UnidadeTrabalho);
            IRepositorioPessoaAgendamento repPessoaAg = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaAgendamento>(UnidadeTrabalho);
            IRepositorioPreCadastroPrestador repPreCad = Fabrica.Instancia.ObterRepositorio<IRepositorioPreCadastroPrestador>(UnidadeTrabalho);
            IRepositorioDocumento repDoc = Fabrica.Instancia.ObterRepositorio<IRepositorioDocumento>(UnidadeTrabalho);
            IRepositorioDocumentoMidia repDocumentoMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioDocumentoMidia>(UnidadeTrabalho);
            IRepositorioMidia repMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioMidia>(UnidadeTrabalho);

            var repColaborador = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);

            var gestor = repColaborador.ObterTodos().Where(v => (v.Codigo == codGestor)).FirstOrDefault();
            if(gestor == null) throw new CoreException("Não foi possível encontrar o cadastro do gestor informado.");
            preCad.GestorResponsavel = gestor;

            if (preCad.PessoaAg.CPF != null)
                preCad.PessoaAg.CPF = preCad.PessoaAg.CPF.Replace("-", "").Replace(".", "");

            var preCadBase = repPreCad.ObterPorId(preCad.Codigo);
            var cpfAnterior = preCadBase.PessoaAg.CPF;
            var nomeAnterior = preCadBase.PessoaAg.Nome;

            //var pedidoCompraAnterior = preCadBase.GestorPO.Pedido.Numero.Replace(" ", "");
            var pessoaFisicaAnterior = preCadBase.GestorResponsavel.PessoaFisica.Codigo;
            var cargoAnterior = preCadBase.Cargo.Codigo;
            var cargoAnteriorNome = preCadBase.Cargo.Nome;

            if (propModificadasPessoa != null && propModificadasPessoa.Any())
            {
                ///TODO: COrrigir gambiarra abaixo para alteração de endereço
                for (int i = 0; i < propModificadasPessoa.Length; i++)
                {
                    if (propModificadasPessoa[i].Equals("CodigoLog"))
                        propModificadasPessoa[i] = "Endereco";
                    else if (propModificadasPessoa[i].Equals("Numero"))
                        propModificadasPessoa[i] = "EnderecoNumero";
                    else if (propModificadasPessoa[i].Equals("Complemento"))
                        propModificadasPessoa[i] = "EnderecoComplemento";
                }

                PessoaAgendamento pessoaAg = preCad.PessoaAg;
                pessoaAg = repPessoaAg.ObterPorId(pessoaAg, propModificadasPessoa);
                repPessoaAg.Validar(EstadoObjeto.Alterado, pessoaAg);
                repPessoaAg.Atualizar(pessoaAg);
            }

            var responsavel = preCad.GestorResponsavel;

            if (propModificadasDadosColab != null && propModificadasDadosColab.Any())
                preCad = repPreCad.ObterPorId(preCad, propModificadasDadosColab);
            else
                preCad = repPreCad.ObterPorId(preCad.Codigo);

            if (preCad.GestorResponsavel.Codigo != responsavel.Codigo)
            {
                preCad.GestorResponsavel = responsavel;
            }

            repPreCad.Validar(EstadoObjeto.Alterado, preCad);

            Usuario usuarioLogado = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name, somenteAtivos: true);

            if (preCad.Solicitante != null && preCad.Solicitante.Codigo != usuarioLogado.Codigo)
            {
                var papel = usuarioLogado.PessoaFisica.ObterPapel(true);
                if (papel == null)
                    throw new CoreException("Não foi possível identificar o usuário logado.");
                Empresa empresa = (papel is Colaborador) ? (papel as Colaborador).Empresa :
                                    ((papel is PrestadorServico) ? (papel as PrestadorServico).Empresa : null);
                if (empresa == null) throw new CoreException("Você não possui permissão para alterar esse registro.");

                IRepositorioPermissao repPermissao = Fabrica.Instancia.ObterRepositorio<IRepositorioPermissao>(UnidadeTrabalho);
                List<GrupoUsuario> gruposUsuarioSalvarEmpresaDifSolic = repPermissao.ObterTodos().Where(p => p.Acao.Url == "/PreCadastroPrestadorServico/SalvarEmpresaDifSolic").SelectMany(p => p.Grupo.GruposUsuarios).ToList();
                List<string> UsuarioSalvarEmpresaDifSolic = gruposUsuarioSalvarEmpresaDifSolic.Select(u => u.Usuario.PessoaFisica.Codigo).ToList();

                if ((!UsuarioSalvarEmpresaDifSolic.Contains(usuarioLogado.Codigo)) && (preCad.Empresa.Codigo != empresa.Codigo))
                    throw new CoreException("Não é possíve salvar, você pertence a uma empresa diferente da regisrada nesse pré cadastro.");
            }

            #region Documentos

            string path = ConfigurationManager.AppSettings.Get("pathUploads");

            if (documentosMidias != null)
            {
                List<DocumentoMidiaModelView> docsPapel = new List<DocumentoMidiaModelView>();
                docsPapel = documentosMidias.Where(dm => preCad.Documentos.Any(cd => dm.Documento.Codigo == cd.Documento.Codigo)).ToList();

                foreach (var item in documentosMidias.Where(dm => preCad.Documentos.Any(cd => dm.Documento.Codigo == cd.Documento.Codigo)))
                {

                    Documento doc = repDoc.ObterPorId(item.Documento.Codigo);
                    doc.Status = item.Documento.Status;
                    doc.Descricao = item.Documento.Descricao;
                    doc.DataValidade = item.Documento.DataValidade;
                    repDoc.Atualizar(doc);

                    if (item.Midias == null)
                    {
                        DocumentoMidiaModelView item1 = item;
                        var docMidias =
                            repDocumentoMidia.ObterTodos().Where(dm => dm.Documento.Codigo == item1.Documento.Codigo && dm.Ativo);
                        foreach (var documentoMidia in docMidias)
                        {
                            documentoMidia.Inativar();
                            repDocumentoMidia.Atualizar(documentoMidia);
                        }
                        continue;
                    }
                    else
                    {
                        DocumentoMidiaModelView item1 = item;
                        List<int> codigosMidia = item.Midias.Select(i => i.CodigoMidia).ToList();
                        IQueryable<DocumentoMidia> listaDocMidia = repDocumentoMidia.ObterTodos().Where(dm =>
                            dm.Documento.Codigo == item1.Documento.Codigo &&
                            dm.Ativo && !codigosMidia.Contains(dm.Midia.Codigo));

                        foreach (DocumentoMidia docMidia in listaDocMidia)
                        {
                            docMidia.Inativar();
                            repDocumentoMidia.Salvar(docMidia);
                        }
                    }

                    int cont = repDocumentoMidia.ObterTodos().Where(dm => dm.Documento.Codigo == item.Documento.Codigo).Count() + 1;

                    foreach (MidiaModelView midia in item.Midias.Where(m => m.CodigoMidia == 0))
                    {
                        if (!System.IO.Directory.Exists(path))
                            throw new CoreException("O diretório onde a midia seria armazanada não está acessivel.");

                        string nomeDisco = string.Format("PF-{0}-{1}-{2}-{3}.{4}",
                            (string.IsNullOrEmpty(preCad.PessoaAg.CPF) ? preCad.PessoaAg.Passaporte : preCad.PessoaAg.CPF),
                            preCad.Codigo, doc.TipoDocumento.Sigla, cont, midia.Extensao);

                        Midia m = new Midia();
                        m.DataRegistro = midia.DataRegistro;
                        m.Extensao = midia.Extensao;
                        m.MimeType = midia.MIME;
                        m.NomeArquivo = nomeDisco;

                        repMidia.Salvar(m);

                        repDocumentoMidia.SalvarDocMidia(doc, m);
                        System.IO.File.WriteAllBytes(m.Url, Convert.FromBase64String(midia.Arquivo));

                        cont++;

                    }
                }

                foreach (var item in documentosMidias.Where(dm => !preCad.Documentos.Any(cd => dm.Documento.Codigo == cd.Documento.Codigo)))
                {
                    DocumentoPapel docPapel = repDoc.SalvarDocPapel(null, item.Documento, preCad);

                    int cont = repDocumentoMidia.ObterTodos().Where(dm => dm.Documento.Codigo == item.Documento.Codigo).Count() + 1;

                    foreach (MidiaModelView midia in item.Midias ?? Enumerable.Empty<MidiaModelView>())
                    {
                        if (midia == null)
                            continue;

                        if (!System.IO.Directory.Exists(path))
                            throw new CoreException("O diretório onde a midia seria armazanada não está acessivel.");

                        string nomeDisco = "PF-" + (string.IsNullOrEmpty(preCad.PessoaAg.CPF) ? preCad.PessoaAg.Passaporte : preCad.PessoaAg.CPF) + "-" + preCad.Codigo.ToString() + "-" + docPapel.Documento.TipoDocumento.Sigla + "-" + cont + "." + midia.Extensao;
                        Midia m = new Midia();
                        m.DataRegistro = midia.DataRegistro;
                        m.Extensao = midia.Extensao;
                        m.MimeType = midia.MIME;
                        m.NomeArquivo = nomeDisco;

                        repMidia.Salvar(m);

                        repDocumentoMidia.SalvarDocMidia(docPapel.Documento, m);

                        System.IO.File.WriteAllBytes(m.Url, Convert.FromBase64String(midia.Arquivo));

                        cont++;
                    }
                }
            }

            #endregion
            repPreCad.Atualizar(preCad);

            //Verifica se atualizou CPF, Passaporte ou Nome, caso ocorra, volta para o status Aguardando Autorização
            #region [Valida alteração de dado crítico]

            string observacao = "";
            bool dadoAlterado = false;
            if (!preCad.PessoaAg.CPF.Equals(cpfAnterior))
            {
                dadoAlterado = true;
                var preCadTemp = new ColaboradorAgendamento { PessoaAg = new PessoaAgendamento() { CPF = cpfAnterior } };
                observacao += string.Format("CPF alterado de '{0}' para '{1}'. ", preCadTemp.PessoaAg.CPFFormatado(), preCad.PessoaAg.CPFFormatado());
            }
            if (!preCad.PessoaAg.Nome.Equals(nomeAnterior))
            {
                dadoAlterado = true;
                observacao += string.Format("Nome alterado de '{0}' para '{1}'. ", nomeAnterior, preCad.PessoaAg.Nome);
            }
            //if (!preCad.GestorPO.Pedido.Numero.Replace(" ", "").Equals(pedidoCompraAnterior))
            //{
            //    dadoAlterado = true;
            //    observacao += string.Format("PO alterada de '{0}' para '{1}'. ", pedidoCompraAnterior, preCad.GestorPO.Pedido.Numero);
            //}
            if (!preCad.GestorResponsavel.Codigo.Equals(pessoaFisicaAnterior))
            {
                dadoAlterado = true;
                observacao += string.Format("Gestor alterado de '{0}' para '{1}'. ", pessoaFisicaAnterior, preCad.GestorResponsavel.PessoaFisica.Nome);
            }
            if (!preCad.Cargo.Codigo.Equals(cargoAnterior))
            {
                dadoAlterado = true;
                observacao += string.Format("Cargo alterado de '{0}' para '{1}'. ", cargoAnteriorNome, preCad.Cargo.Nome);
            }

            var status = preCad.ObterStatusAtual().StatusAgendamento;
            if (dadoAlterado)
            {
                repPapelAg.SalvarStatusPapelAg(preCad.InserirPapelAg(StatusAgendamento.AguardandoAutorizacao, observacao));
                NotificarGestor(preCad);
            }
            else
            {
                if (status == StatusAgendamento.ContratacaoNaoAutorizada)
                {
                    repPapelAg.SalvarStatusPapelAg(preCad.InserirPapelAg(StatusAgendamento.AguardandoAutorizacao, observacao));
                    NotificarGestor(preCad);
                }
                else if (status == StatusAgendamento.CadastroNaoAprovado)
                    repPapelAg.SalvarStatusPapelAg(preCad.InserirPapelAg(StatusAgendamento.AguardandoAprovacao, observacao));
            }

            #endregion

            return Json(montaObj(preCad, usuarioLogado));
        }

        [Transaction]
        public ActionResult Autorizar(int codPreCad)
        {
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            IRepositorioPapelAgendamento repPapelAg = Fabrica.Instancia.ObterRepositorio<IRepositorioPapelAgendamento>(UnidadeTrabalho);
            IRepositorioPreCadastroPrestador repPreCad = Fabrica.Instancia.ObterRepositorio<IRepositorioPreCadastroPrestador>(UnidadeTrabalho);

            var preCad = repPreCad.ObterPorId(codPreCad);
            repPreCad.Validar(EstadoObjeto.Alterado, preCad);

            var status = preCad.ObterStatusAtual().StatusAgendamento;
            if ((status != StatusAgendamento.AguardandoAutorizacao) && (status != StatusAgendamento.ContratacaoNaoAutorizada))
                throw new CoreException("Não é possível salvar, esse cadastro já foi autorizado.");

            Usuario usuarioLogado = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name, somenteAtivos: true);
            if (preCad.GestorResponsavel.PessoaFisica.Codigo != usuarioLogado.PessoaFisica.Codigo)
                throw new CoreException("Não é possível salvar. Somente o gestor relacionado poderá Autorizar esse pré cadastro.");

            repPapelAg.SalvarStatusPapelAg(preCad.InserirPapelAg(StatusAgendamento.ContratacaoAutorizada, null));
            repPapelAg.SalvarStatusPapelAg(preCad.InserirPapelAg(StatusAgendamento.AguardandoAprovacao, null));

            NotificarSolicitante(preCad);

            return Json(montaObj(preCad, usuarioLogado));
        }

        [Transaction]
        public ActionResult NaoAutorizar(int codPreCad, string observacao, string path = null)
        {
            if (!(codPreCad > 0))
                throw new CoreException("Não foi possível salvar. código de pré cadastro inválido.");

            if (string.IsNullOrEmpty(observacao))
                throw new CoreException("Não foi possível salvar, falta informar a justificativa.");

            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            IRepositorioPapelAgendamento repPapelAg = Fabrica.Instancia.ObterRepositorio<IRepositorioPapelAgendamento>(UnidadeTrabalho);
            IRepositorioPreCadastroPrestador repPreCad = Fabrica.Instancia.ObterRepositorio<IRepositorioPreCadastroPrestador>(UnidadeTrabalho);

            var preCad = repPreCad.ObterPorId(codPreCad);
            repPreCad.Validar(EstadoObjeto.Alterado, preCad);

            var status = preCad.ObterStatusAtual().StatusAgendamento;
            if ((status != StatusAgendamento.AguardandoAutorizacao) && (status != StatusAgendamento.ContratacaoNaoAutorizada))
                throw new CoreException("Não é possível salvar, esse cadastro já foi autorizado.");

            Usuario usuarioLogado = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name, somenteAtivos: true);
            if (preCad.GestorResponsavel.PessoaFisica.Codigo != usuarioLogado.PessoaFisica.Codigo)
                throw new CoreException("Não é possível salvar. Somente o gestor relacionado poderá Não Autorizar esse pré cadastro.");

            repPapelAg.SalvarStatusPapelAg(preCad.InserirPapelAg(StatusAgendamento.ContratacaoNaoAutorizada, observacao));

            if (string.IsNullOrEmpty(path))
                NotificarSolicitante(preCad);

            return Json(montaObj(preCad, usuarioLogado));
        }
                
        public ActionResult PreAprovar(int codPreCad)
        {
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            IRepositorioPapelAgendamento repPapelAg = Fabrica.Instancia.ObterRepositorio<IRepositorioPapelAgendamento>(UnidadeTrabalho);
            IRepositorioPreCadastroPrestador repPreCad = Fabrica.Instancia.ObterRepositorio<IRepositorioPreCadastroPrestador>(UnidadeTrabalho);

            var preCad = repPreCad.ObterPorId(codPreCad);
            repPreCad.Validar(EstadoObjeto.Alterado, preCad);

            var status = preCad.ObterStatusAtual().StatusAgendamento;            
            if ((status != StatusAgendamento.AguardandoAprovacao) && (status != StatusAgendamento.ContratacaoAutorizada) && (status != StatusAgendamento.CadastroNaoAprovado))
                throw new CoreException("Não é possível salvar, esse cadastro já foi pré-aprovado.");

            Usuario usuarioLogado = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name, somenteAtivos: true);
            //if (preCad.GestorResponsavel.PessoaFisica.Codigo != usuarioLogado.PessoaFisica.Codigo)
            //    throw new CoreException("Não é possível salvar. Somente o gestor relacionado poderá Autorizar esse pré cadastro.");

            repPapelAg.SalvarStatusPapelAg(preCad.InserirPapelAg(StatusAgendamento.CadastroPreAprovado, null));
            //repPapelAg.SalvarStatusPapelAg(preCad.InserirPapelAg(StatusAgendamento.AguardandoAprovacao, null));

            NotificarSolicitante(preCad);

            return Json(montaObj(preCad, usuarioLogado));
        }

        public ActionResult Aprovar(int codPreCad)
        {
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            IRepositorioPreCadastroPrestador repPreCad = Fabrica.Instancia.ObterRepositorio<IRepositorioPreCadastroPrestador>(UnidadeTrabalho);

            var preCad = repPreCad.ObterPorId(codPreCad);
            repPreCad.Validar(EstadoObjeto.Alterado, preCad);

            var status = preCad.ObterStatusAtual().StatusAgendamento;
            if ((status != StatusAgendamento.AguardandoAprovacao) && (status != StatusAgendamento.CadastroNaoAprovado) && (status != StatusAgendamento.ContratacaoAutorizada) && (status != StatusAgendamento.CadastroPreAprovado))
                throw new CoreException("Não é possível salvar, antes de aprovar, esse cadastro precisa ser autorizado pelo gestor responsável.");

            Usuario usuarioLogado = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name, somenteAtivos: true);

            // Redirecionar para página de Prestador de Serviço com o parametro de preCadastro, para Criar um papel;
            //repPapelAg.SalvarStatusPapelAg(preCad.InserirPapelAg(StatusAgendamento.CadastroAprovado, null));

            return Json(montaObj(preCad, usuarioLogado));
        }

        [Transaction]
        public ActionResult Reprovar(int codPreCad, string observacao, string path = null)
        {
            if (!(codPreCad > 0))
                throw new CoreException("Não foi possível salvar. Código de pré cadastro inválido.");

            if (string.IsNullOrEmpty(observacao))
                throw new CoreException("Não foi possível salvar, falta informar a justificativa.");

            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            IRepositorioPapelAgendamento repPapelAg = Fabrica.Instancia.ObterRepositorio<IRepositorioPapelAgendamento>(UnidadeTrabalho);
            IRepositorioPreCadastroPrestador repPreCad = Fabrica.Instancia.ObterRepositorio<IRepositorioPreCadastroPrestador>(UnidadeTrabalho);

            var preCad = repPreCad.ObterPorId(codPreCad);
            if (preCad == null)
                repPreCad.Validar(EstadoObjeto.Alterado, preCad);

            var status = preCad.ObterStatusAtual().StatusAgendamento;
            if ((status != StatusAgendamento.AguardandoAprovacao) && (status != StatusAgendamento.CadastroNaoAprovado) && (status != StatusAgendamento.ContratacaoAutorizada) && (status != StatusAgendamento.CadastroPreAprovado))            
                throw new CoreException("Não é possível salvar, esse cadastro precisa primeiro ser autorizado pelo gestor responsável.");

            repPapelAg.SalvarStatusPapelAg(preCad.InserirPapelAg(StatusAgendamento.CadastroNaoAprovado, observacao));

            if (string.IsNullOrEmpty(path))
                NotificarSolicitante(preCad);

            Usuario usuarioLogado = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name, somenteAtivos: true);
            return Json(montaObj(preCad, usuarioLogado));
        }

        private void NotificarSolicitante(PrestadorServicoAgendamento precadastro, string path = null)
        {
            if (precadastro.Solicitante == null)
                return;

            try
            {
                var status = precadastro.ObterStatusAtual();
                if ((status.StatusAgendamento != StatusAgendamento.ContratacaoNaoAutorizada) &&
                    (status.StatusAgendamento != StatusAgendamento.AguardandoAutorizacao) &&
                    (status.StatusAgendamento != StatusAgendamento.AguardandoAprovacao) &&
                    (status.StatusAgendamento != StatusAgendamento.CadastroNaoAprovado) &&
                    (status.StatusAgendamento != StatusAgendamento.CadastroPreAprovado))
                    throw new CoreException("Status do pré cadastro diferente do permitido para notificar solicitante.");

                Dictionary<string, string> valoresSubs = new Dictionary<string, string>();
                valoresSubs.Add("$$Status$$", status.StatusAgendamento.ObterDescricaoEnum());
                valoresSubs.Add("$$InfoComplementar1$$", status.StatusAgendamento == StatusAgendamento.ContratacaoNaoAutorizada ?
                    "Informações complementares" : "Lista de pendências");
                valoresSubs.Add("$$InfoComplementar2$$", status.Observacao);

                #region [Informações do Prestador do Pré Cadastro]

                var u = precadastro.PessoaAg;
                valoresSubs.Add("$$Nome2$$", u.Nome);
                valoresSubs.Add("$$CPFPPassaporte$$", !string.IsNullOrEmpty(u.CPF) ? u.CPFFormatado() : u.Passaporte);
                valoresSubs.Add("$$Empresa2$$", precadastro.Empresa.Nome);
                valoresSubs.Add("$$Cargo$$", precadastro.Cargo.Nome);

                #endregion

                #region [Informações da inclusão da Pessoa ]

                valoresSubs.Add("$$Observacao$$", precadastro.Observacao);
                valoresSubs.Add("$$DataAdmissao$$", precadastro.DataAdmissao.HasValue ? precadastro.DataAdmissao.Value.ToString("dd/MM/yyyy") : "");
                valoresSubs.Add("$$DataInclusao$$", precadastro.DataRegistro.ToString("dd/MM/yyyy"));
                valoresSubs.Add("$$TipoPreCadastro$$", "Prestador de Serviço");

                #endregion

                #region [Enviar email]

                if (!string.IsNullOrEmpty(path)) return;

                string AssuntoEmail = "[CONTROLSYS] Status de Pré Cadastro de Prestador atualizado";
                string Path_htmlEmail = "/Content/templates/preCadastroMotivo.htm";

                if (status.StatusAgendamento == StatusAgendamento.CadastroPreAprovado)
                {
                    AssuntoEmail = "[CONTROLSYS] Status de Pré Cadastro de Prestador esta Pré Aprovado.";                    
                    Path_htmlEmail = "/Content/templates/preCadastroPreAprovado.htm";
                }

                Globalsys.Util.Email.Enviar(AssuntoEmail,
                    Server.MapPath(Path_htmlEmail),
                    valoresSubs,
                    string.Empty,
                    null,
                    precadastro.Solicitante.PessoaFisica.Email);

                #endregion


            }
            catch (Exception ex)
            {
                return;
            }
        }

        private void NotificarGestor(PrestadorServicoAgendamento precadastro, string path = null)
        {
            try
            {
                Dictionary<string, string> valoresSubs = new Dictionary<string, string>();

                valoresSubs.Add("$$Papel$$", "Prestador de Serviço");

                #region [Dados da Pessoa que está cadastrando]

                var repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
                Usuario usuarioLogado = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name, somenteAtivos: true);
                PessoaFisica pfLogada = usuarioLogado.PessoaFisica;
                Papel papelPLog = pfLogada.ObterPapel();

                valoresSubs.Add("$$Nome$$", pfLogada.Nome);
                valoresSubs.Add("$$Empresa$$", (papelPLog is Colaborador
                    ? (papelPLog as Colaborador).Empresa.Nome
                    : (papelPLog is PrestadorServico ? (papelPLog as PrestadorServico).Empresa.Nome : "")));
                valoresSubs.Add("$$Funcao$$", (papelPLog is Colaborador
                    ? (papelPLog as Colaborador).Cargo.Nome
                    : (papelPLog is PrestadorServico ? (papelPLog as PrestadorServico).Cargo.Nome : "")));

                #endregion

                #region [Informações do Prestador de Serviço]

                var u = precadastro.PessoaAg;
                valoresSubs.Add("$$Nome2$$", u.Nome);
                valoresSubs.Add("$$CPFPPassaporte$$", !string.IsNullOrEmpty(u.CPF) ? u.CPFFormatado() : u.Passaporte);
                valoresSubs.Add("$$Empresa2$$", precadastro.Empresa.Nome);
                valoresSubs.Add("$$Cargo$$", precadastro.Cargo.Nome);

                #endregion

                #region [Informações da inclusão da Pessoa ]

                valoresSubs.Add("$$Observacao$$", precadastro.Observacao);
                valoresSubs.Add("$$DataAdmissao$$", precadastro.DataAdmissao.HasValue ? precadastro.DataAdmissao.Value.ToString("dd/MM/yyyy") : "");
                valoresSubs.Add("$$DataInclusao$$", precadastro.DataRegistro.ToString("dd/MM/yyyy"));

                #endregion

                #region [Enviar email]

                if (!string.IsNullOrEmpty(path)) return;

                var gestor = precadastro.GestorResponsavel;
                if (string.IsNullOrEmpty(gestor.PessoaFisica.Email))
                    throw new CoreException(string.Format("O gestor '{0}' não possui email cadastrado para ser notificado.", gestor.PessoaFisica.Nome));

                Globalsys.Util.Email.Enviar("[CONTROLSYS] Pré Cadastro de Prestador",
                    Server.MapPath("/Content/templates/preCadastro.htm"),
                    valoresSubs,
                    string.Empty,
                    null,
                    gestor.PessoaFisica.Email);

                #endregion


            }
            catch (Exception ex)
            {
                return;
            }
        }

        public ActionResult Pesquisar(StoreRequestParameters parameters = null, string nome = null, string cpf = null, string passaporte = null, string codEmpresa = null, string solicitante = null, StatusAgendamento? status = null, DateTime? dtInicio = null, DateTime? dtFim = null)
        {
            IQueryable<PrestadorServicoAgendamento> preCadColabs = FiltrarRegistros(nome, cpf, passaporte, codEmpresa, solicitante, status, dtInicio, dtFim);

            #region [Filtro de usuário]
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            Usuario usuarioLogado = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name, somenteAtivos: true);
            #endregion

            #region [Filtro Administrador]
            var grupos = usuarioLogado.GruposUsuarios.Select(g => g.Grupo);
            var acoes = grupos.SelectMany(g => g.Acoes);
            //var enumerable = acoes as Acao[] ?? acoes.ToArray();
            var podePreAprovar = (acoes.Where(a => a.Url == "/PreCadastroPrestadorServico/PreAprovar")).Any();
            var podeAprovar = (acoes.Where(a => a.Url == "/PreCadastroPrestadorServico/Aprovar")).Any();
            var podeReprovar = (acoes.Where(a => a.Url == "/PreCadastroPrestadorServico/Reprovar")).Any();
            #endregion

            if (!(podeAprovar && podeReprovar))
            {
                #region [Filtrar empresa]
                var papel = usuarioLogado.PessoaFisica.ObterPapel(true);
                if (papel == null) throw new CoreException("Não foi possível identificar o usuário logado.");

                Empresa empresa = (papel is Colaborador) ? (papel as Colaborador).Empresa :
                                    ((papel is PrestadorServico) ? (papel as PrestadorServico).Empresa : null);
                if (empresa == null) throw new CoreException("Não será possível consultar, visitante não possui empresa relacionada.");
                #endregion

                preCadColabs = preCadColabs.Where(p =>
                    (p.Solicitante != null && p.Solicitante.Codigo == usuarioLogado.Codigo) || (p.Empresa.Codigo == empresa.Codigo) ||
                    (p.GestorResponsavel.PessoaFisica.Codigo == usuarioLogado.PessoaFisica.Codigo)
                );
            }

            int total = preCadColabs.Count();
            List<PrestadorServicoAgendamentoModelView> data = montaListaObj(preCadColabs, parameters);

            return new StoreResult(data, total);
        }

        private IQueryable<PrestadorServicoAgendamento> FiltrarRegistros(string nome = null, string cpf = null, string passaporte = null, string empresa = null, string solicitante = null, StatusAgendamento? status = null, DateTime? dtInicio = null, DateTime? dtFim = null)
        {
            IRepositorioPreCadastroPrestador repPreCadastroColab = Fabrica.Instancia.ObterRepositorio<IRepositorioPreCadastroPrestador>(UnidadeTrabalho);
            IQueryable<PrestadorServicoAgendamento> preCads = repPreCadastroColab.ObterTodos();

            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            Usuario usuarioLogado = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name);

            if (!string.IsNullOrEmpty(nome))
                preCads = preCads.Where(p => p.PessoaAg.Nome.Contains(nome));

            if (!string.IsNullOrEmpty(cpf))
            {
                cpf = cpf.Replace(".", "").Replace("-", "");
                preCads = preCads.Where(p => p.PessoaAg.CPF == cpf);
            }

            if (!string.IsNullOrEmpty(passaporte))
                preCads = preCads.Where(p => p.PessoaAg.Passaporte == passaporte);

            if (!string.IsNullOrEmpty(empresa))
                preCads = preCads.Where(p => p.Empresa.Codigo == empresa);

            if (!string.IsNullOrEmpty(solicitante))
                preCads = preCads.Where(p => p.Solicitante != null && p.Solicitante.PessoaFisica.Nome.Contains(solicitante));

            if (dtInicio.HasValue)
                preCads = preCads.Where(p => p.DataRegistro >= dtInicio.Value);

            if (dtFim.HasValue)
                preCads = preCads.Where(p => p.DataRegistro <= dtFim.Value);

            if (status != null)
                preCads = preCads.Where(p => p.Status.OrderByDescending(s => s.Codigo).Select(s => s.StatusAgendamento).FirstOrDefault() == status);

            if (!usuarioLogado.GruposUsuarios.Any(gu => gu.Grupo.IsAdmin()))
            {
                var grupos = usuarioLogado.GruposUsuarios.Select(g => g.Grupo);
                var acoes = grupos.SelectMany(g => g.Acoes);
                var papel = usuarioLogado.PessoaFisica.ObterPapel(true);
                //PrestadorServico ps = papel != null ? papel as PrestadorServico : null;
                //Colaborador co = papel != null ? papel as Colaborador : null;
                int codigoPapel = papel != null ? papel.Codigo : 0;

                bool podePreAprovar = (acoes.Where(a => a.Url == "/PreCadastroPrestadorServico/PreAprovar")).Any();
                bool podeAprovar = (acoes.Where(a => a.Url == "/PreCadastroPrestadorServico/Aprovar")).Any();
                bool podeReprovar = (acoes.Where(a => a.Url == "/PreCadastroPrestadorServico/Reprovar")).Any();

                //if (!usuarioLogado.GruposUsuarios.Any(gu => gu.Grupo.IsAdmin()))
                //    preCads = preCads.Where(pc => (pc.Solicitante.Codigo == usuarioLogado.Codigo && ((ps != null && ps.Empresa.Codigo == pc.Empresa.Codigo) || (co != null && co.Empresa.Codigo == pc.Empresa.Codigo))) || pc.GestorPO.Colaborador.Codigo == codigoPapel || pc.Solicitante.Codigo == usuarioLogado.Codigo || (podeAprovar && podeReprovar));

                if (papel is Colaborador)
                {
                    preCads = preCads.ToList().Where(pc => pc.GestorResponsavel != null && pc.GestorResponsavel.Codigo == codigoPapel
                        || (pc.Solicitante != null && pc.Solicitante.Codigo == usuarioLogado.Codigo)
                        || (podeAprovar && podeReprovar)
                        || (pc.Solicitante != null && (papel as Colaborador).Empresa.Codigo == (pc.Solicitante.PessoaFisica != null && pc.Solicitante.PessoaFisica.ObterPapel(true) != null
                                                                        ? pc.Solicitante.PessoaFisica.ObterPapel(true) is Colaborador
                                                                            ? (pc.Solicitante.PessoaFisica.ObterPapel(true) as Colaborador).Empresa.Codigo
                                                                            : (pc.Solicitante.PessoaFisica.ObterPapel(true) as PrestadorServico).Empresa.Codigo
                                                                        : string.Empty))).AsQueryable();
                }
                else if (papel is PrestadorServico)
                {
                    preCads = preCads.ToList().Where(pc => pc.GestorResponsavel != null && pc.GestorResponsavel.Codigo == codigoPapel
                        || (pc.Solicitante != null && pc.Solicitante.Codigo == usuarioLogado.Codigo)
                        || (podeAprovar && podeReprovar)
                        || (pc.Solicitante != null && (papel as PrestadorServico).Empresa.Codigo == (pc.Solicitante.PessoaFisica != null && pc.Solicitante.PessoaFisica.ObterPapel(true) != null
                                                                            ? pc.Solicitante.PessoaFisica.ObterPapel(true) is Colaborador
                                                                                ? (pc.Solicitante.PessoaFisica.ObterPapel(true) as Colaborador).Empresa.Codigo
                                                                                : (pc.Solicitante.PessoaFisica.ObterPapel(true) as PrestadorServico).Empresa.Codigo
                                                                        : string.Empty))).AsQueryable();
                }
                else
                {
                    preCads = preCads.Where(pc => pc.GestorResponsavel != null && pc.GestorResponsavel.Codigo == codigoPapel || (pc.Solicitante != null && pc.Solicitante.Codigo == usuarioLogado.Codigo) || (podeAprovar && podeReprovar));
                }

            }

            return preCads;
        }

        private List<PrestadorServicoAgendamentoModelView> montaListaObj(IQueryable<PrestadorServicoAgendamento> preCadColabs, StoreRequestParameters parameters = null)
        {
            if (parameters == null)
                parameters = new StoreRequestParameters();
            preCadColabs = preCadColabs.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 25);

            var lista = new List<PrestadorServicoAgendamentoModelView>();
            foreach (var p in preCadColabs)
            {
                var newReg = new PrestadorServicoAgendamentoModelView();
                newReg.Codigo = p.Codigo;
                newReg.CPF = !string.IsNullOrEmpty(p.PessoaAg.CPF) ? p.PessoaAg.CPFFormatado() : p.PessoaAg.Passaporte;
                newReg.Nome = p.PessoaAg.Nome;
                newReg.EmpresaDesc = p.Empresa.Nome;
                newReg.CargoDesc = p.Cargo.Nome;
                newReg.StatusAgendamento = p.ObterStatusAtual() != null ? p.ObterStatusAtual().StatusAgendamento.ObterDescricaoEnum() : null;
                newReg.DataRegistro = p.DataRegistro;
                lista.Add(newReg);
            }

            return lista;
            //return preCadColabs.ToList().Select(p => new PrestadorServicoAgendamentoModelView
            //{
            //    Codigo = p.Codigo,
            //    CPF = !string.IsNullOrEmpty(p.PessoaAg.CPF) ? p.PessoaAg.CPFFormatado() : p.PessoaAg.Passaporte,
            //    Nome = p.PessoaAg.Nome,
            //    EmpresaDesc = p.Empresa.Nome,
            //    CargoDesc = p.Cargo.Nome,
            //    StatusAgendamento = p.ObterStatusAtual() != null ? p.ObterStatusAtual().StatusAgendamento.ObterDescricaoEnum() : null
            //}).ToList();
        }

        private PrestadorServicoAgendamentoModelView montaObj(PrestadorServicoAgendamento preCad, Usuario usuarioLogado = null)
        {
            try
            {
            var reg = new PrestadorServicoAgendamentoModelView();

            if (usuarioLogado != null)
            {
                reg.CadGestor = preCad.GestorResponsavel.PessoaFisica.Codigo == usuarioLogado.PessoaFisica.Codigo;

                var grupos = usuarioLogado.GruposUsuarios.Select(g => g.Grupo);
                var acoes = grupos.SelectMany(g => g.Acoes);
                //var enumerable = acoes as Acao[] ?? acoes.ToArray();
                reg.PodeSalvarStatusPreAprovado = (preCad.ObterStatusAtual().StatusAgendamento != StatusAgendamento.CadastroPreAprovado) || (acoes.Where(a => a.Url == "/PreCadastroPrestadorServico/PreAprovar").Any());
                reg.PodePreAprovar = (preCad.ObterStatusAtual().StatusAgendamento == StatusAgendamento.AguardandoAprovacao || preCad.ObterStatusAtual().StatusAgendamento == StatusAgendamento.CadastroPreAprovado || preCad.ObterStatusAtual().StatusAgendamento == StatusAgendamento.CadastroNaoAprovado) && (acoes.Where(a => a.Url == "/PreCadastroPrestadorServico/PreAprovar")).Any();
                reg.PodeAprovar = (preCad.ObterStatusAtual().StatusAgendamento == StatusAgendamento.AguardandoAprovacao || preCad.ObterStatusAtual().StatusAgendamento == StatusAgendamento.CadastroPreAprovado || preCad.ObterStatusAtual().StatusAgendamento == StatusAgendamento.CadastroNaoAprovado) && (acoes.Where(a => a.Url == "/PreCadastroPrestadorServico/Aprovar")).Any();
                reg.PodeReprovar = (preCad.ObterStatusAtual().StatusAgendamento == StatusAgendamento.AguardandoAprovacao || preCad.ObterStatusAtual().StatusAgendamento == StatusAgendamento.CadastroPreAprovado || preCad.ObterStatusAtual().StatusAgendamento == StatusAgendamento.CadastroNaoAprovado) && (acoes.Where(a => a.Url == "/PreCadastroPrestadorServico/Reprovar")).Any();
            }

            reg.PodeAlterarGestor = preCad.ObterStatusAtual().StatusAgendamento != StatusAgendamento.CadastroAprovado;

            //reg.PedidoCompra = preCad.GestorResponsavel.Pedido.Codigo;
            //reg.PedidoCompraNome = string.Format("{0,12} {1}", preCad.GestorPO.Pedido.Numero, preCad.GestorPO.Pedido.Nome);
            reg.Gestor = preCad.GestorResponsavel.Codigo;
            reg.GestorNome = preCad.GestorResponsavel.PessoaFisica.Nome;
            reg.GestorCPF = preCad.GestorResponsavel.PessoaFisica.CPF;
            reg.GestorEmpresa = preCad.GestorResponsavel?.Empresa.Nome;
            var status = preCad.ObterStatusAtual();
            reg.StatusAgendamento = status != null ? status.StatusAgendamento.ObterDescricaoEnum() : null;
            reg.DataRegistro = preCad.DataRegistro;

            if (status != null && (status.StatusAgendamento == StatusAgendamento.ContratacaoNaoAutorizada ||
                status.StatusAgendamento == StatusAgendamento.CadastroNaoAprovado))
                reg.MotivoNegado = status.Observacao;

            #region [Informações Pessoais]
            reg.Codigo = preCad.Codigo;
            reg.CodigoPessoa = preCad.PessoaAg.Codigo;
            reg.CPF = !string.IsNullOrEmpty(preCad.PessoaAg.CPF) ? preCad.PessoaAg.CPFFormatado() : null;
            reg.Passaporte = preCad.PessoaAg.Passaporte;
            reg.Nome = preCad.PessoaAg.Nome;
            reg.Apelido = preCad.PessoaAg.Apelido;
            reg.Email = preCad.PessoaAg.Email;
            reg.DataNascimento = preCad.PessoaAg.DataNascimento;
            reg.DataVisto = preCad.PessoaAg.DataVisto;
            reg.DataValidadeVisto = preCad.PessoaAg.DataValidadeVisto;
            reg.Sexo = preCad.PessoaAg.Sexo.ObterDescricaoEnum();
            reg.TelefoneNumResidencial = preCad.PessoaAg.TelefoneNumResidencial;
            reg.TelefoneNumEmergencial = preCad.PessoaAg.TelefoneNumEmergencial;
            reg.TelefoneNumCelular = preCad.PessoaAg.TelefoneNumCelular;
            reg.NomeMae = preCad.PessoaAg.NomeMae;
            reg.NomePai = preCad.PessoaAg.NomePai;
            reg.RNE = preCad.PessoaAg.RNE;
            reg.DataExpedicaoRNE = preCad.PessoaAg.DataExpedicaoRNE;
            reg.EstadoCivil = preCad.PessoaAg.EstadoCivil;
            reg.Escolaridade = preCad.PessoaAg.Escolaridade;
            reg.TipoSanguineo = preCad.PessoaAg.TipoSanguineo;
            reg.Nacionalidade = preCad.PessoaAg.Nacionalidade;
            reg.NaturalidadeUF = preCad.PessoaAg.Naturalidade != null && preCad.PessoaAg.Naturalidade.Estado != null ? preCad.PessoaAg.Naturalidade.Estado.Codigo : (int?)null;
            reg.Naturalidade = preCad.PessoaAg.Naturalidade != null ? preCad.PessoaAg.Naturalidade.Codigo : (int?)null;
            reg.PessoaJuridica = preCad.PessoaJuridica;
            #endregion

            #region [RG]
            reg.RG = preCad.PessoaAg.RG;
            reg.RGOrgaoEmissor = preCad.PessoaAg.RGOrgaoEmissor;
            reg.RGOrgaoEmissorUF = preCad.PessoaAg.RGOrgaoEmissorUF != null ? preCad.PessoaAg.RGOrgaoEmissorUF.Codigo : (int?)null;
            reg.RGDataEmissao = preCad.PessoaAg.RGDataEmissao;
            #endregion

            #region [Titulo de Eleitor]
            reg.TituloEleitor = preCad.PessoaAg.TituloEleitor;
            reg.TituloEleitorZona = preCad.PessoaAg.TituloEleitorZona;
            reg.TituloEleitorSecao = preCad.PessoaAg.TituloEleitorSecao;
            reg.TituloEleitorCidade = preCad.PessoaAg.TituloEleitorCidade != null ? preCad.PessoaAg.TituloEleitorCidade.Codigo : (int?)null;
            reg.TituloEleitorEstado = (preCad.PessoaAg.TituloEleitorCidade != null && preCad.PessoaAg.TituloEleitorCidade.Estado != null) ? preCad.PessoaAg.TituloEleitorCidade.Estado.Codigo : (int?)null;
            #endregion

            #region [CTPS]
            reg.CTPS = preCad.PessoaAg.CTPS;
            reg.CTPSSerie = preCad.PessoaAg.CTPSSerie;
            reg.CTPSEstado = preCad.PessoaAg.CTPSEstado != null ? preCad.PessoaAg.CTPSEstado.Codigo : (int?)null;
            reg.CTPSData = preCad.PessoaAg.CTPSData;
            #endregion

            #region [CNH]
            reg.CNH = preCad.PessoaAg.CNH;
            reg.CNHCategoria = preCad.PessoaAg.CNHCategoria;
            reg.CNHDataValidade = preCad.PessoaAg.CNHDataValidade;
            #endregion

            #region [Certificado Reservista]
            reg.CertificadoReservista = preCad.PessoaAg.CertificadoReservista;
            reg.CertificadoReservistaCat = preCad.PessoaAg.CertificadoReservistaCat;
            #endregion

            #region [Endereço]
            var endereco = preCad.PessoaAg.Endereco;
            reg.Endereco = new EnderecoMovelView()
            {
                CodigoEndereco = endereco != null ? endereco.Codigo: 0,
                CEP = endereco != null ? endereco.CEP: null,
                Logradouro = endereco != null ? endereco.Nome: null,
                Bairro = endereco != null ? endereco.Bairro.Nome: null,
                Cidade = endereco != null ? endereco.Bairro.Cidade.Nome: null,
                Estado = endereco != null ? endereco.Bairro.Cidade.Estado.Nome: null,
                Pais = endereco != null ? endereco.Bairro.Cidade.Estado.Pais.Nome: null,
                Numero = preCad.PessoaAg.EnderecoNumero != null ? preCad.PessoaAg.EnderecoNumero: null,
                Complemento = preCad.PessoaAg.EnderecoComplemento != null ? preCad.PessoaAg.EnderecoComplemento: null
            };
            #endregion

            #region Documentos
            reg.Documentos =
                preCad.Documentos.Select(d => new DocumentoPapelModelView()
                {
                    CodigoDocumento = d.Documento.Codigo,
                    Codigo = d.Documento.TipoDocumento.Codigo,
                    Sigla = d.Documento.TipoDocumento.Sigla,
                    DataValidade = d.Documento.DataValidade,
                    Descricao = d.Documento.Descricao,
                    QtdMidias = d.Documento.Midias.Count(m => m.Ativo),
                    Status = EnumExtensoes.ObterDescricaoEnum(d.Documento.Status),
                    Midias = d.Documento.Midias.Select(dm => new MidiaModelView()
                    {
                        CodigoMidia = dm.Midia.Codigo,
                        Arquivo = string.Empty,
                        DataRegistro = dm.Midia.DataRegistro,
                        Extensao = dm.Midia.Extensao,
                        MIME = dm.Midia.MimeType,
                        NomeDisco = dm.Midia.NomeArquivo,
                        NomeOriginal = string.Empty,
                        URL = dm.Midia.Url
                    }).ToList()

                }).ToList();

            #endregion
            #region [Histórico Status]
            reg.HistoricoStatus = preCad.Status.Select(h => new StatusPapelAgendamentoModelView()
            {
                Codigo = h.Codigo,
                Data = h.DataRegistro,
                Status = h.StatusAgendamento.ObterDescricaoEnum(),
                Observacao = h.Observacao
            }).ToList();
            #endregion

            reg.TipoPrestador = obterDescTipoPrestador(preCad.TipoPrestador);

            if (preCad.Solicitante != null)
            {
                Papel papelSolicitante = preCad.Solicitante.PessoaFisica.ObterPapel();

                if (papelSolicitante != null && papelSolicitante is Colaborador)
                {
                    Colaborador solic = papelSolicitante as Colaborador;
                    string solicitante = solic.PessoaFisica.Nome + " / " + solic.PessoaFisica.CPFFormatado() + " / " + solic.Empresa.Apelido;

                    reg.Solicitante = new ColaboradorModelView()
                    {
                        Codigo = papelSolicitante.Codigo,
                        Nome = solicitante
                    };
                }

                reg.SolicitanteNome = papelSolicitante.PessoaFisica.Nome;
            }

            reg.Observacao = preCad.Observacao;
            //reg.Empresa = preCad.Empresa != null ? preCad.Empresa.Codigo : null;
            reg.Empresa = preCad.Empresa != null ? new EmpresaModelView() { Codigo = preCad.Empresa.Codigo, Nome = preCad.Empresa.Nome + " (" + preCad.Empresa.CNPJ + ")" } : null;
            reg.EmpresaDesc = preCad.Empresa != null ? preCad.Empresa.Nome : null;
            //reg.Fornecedor = preCad.Fornecedor != null ? preCad.Fornecedor.Codigo : (int)null;
            //reg.FornecedoDesc = preCad.Fornecedor != null ? preCad.Fornecedor.RazaoSocial : null;
            reg.Fornecedor = preCad.TipoPrestador == TipoPrestador.Emergencial && preCad.Fornecedor != null? preCad.Fornecedor.RazaoSocial : null;
            reg.EmpresaFornecedor = preCad.TipoPrestador == TipoPrestador.Temporario && preCad.Empresa != null ? preCad.Empresa.Nome : preCad.Fornecedor != null ? preCad.Fornecedor.RazaoSocial : null;
            reg.DataAdmissao = preCad.DataAdmissao;
            reg.Cargo = preCad.Cargo != null ? preCad.Cargo.Codigo : (int?)null;
            reg.CargoDesc = preCad.Cargo != null ? preCad.Cargo.Nome : null;
            reg.DataAdmissao = preCad.DataAdmissao ?? (DateTime?)null;

            return reg;
            }
            catch (Exception ex)
            {
                throw new CoreException("Erro ao montar objeto: " + ex.Message);
            }

        }

        private string obterDescTipoPrestador(TipoPrestador tipoPrestador)
        {
            return EnumExtensoes.ObterDescricaoEnum(tipoPrestador);
        }


        private void IncluirExcel(PrestadorServicoAgendamento prestadorServicoAgendamento)
        {
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            IRepositorioPreCadastroPrestador repPrest = Fabrica.Instancia.ObterRepositorio<IRepositorioPreCadastroPrestador>(UnidadeTrabalho);
            IRepositorioColaborador repColab = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);
            IRepositorioPessoaAgendamento repPessoaAg = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaAgendamento>(UnidadeTrabalho);
            IRepositorioPapelAgendamento repPapelAg = Fabrica.Instancia.ObterRepositorio<IRepositorioPapelAgendamento>(UnidadeTrabalho);

            prestadorServicoAgendamento.PessoaAg.DataRegistro = DateTime.Now;
            repPessoaAg.Salvar(prestadorServicoAgendamento.PessoaAg);

            //cadastrar papel
            repPapelAg.SalvarStatusPapelAg(prestadorServicoAgendamento.InserirPapelAg(StatusAgendamento.AguardandoAprovacao, null));
            prestadorServicoAgendamento.DataRegistro = DateTime.Now;
            repPrest.Salvar(prestadorServicoAgendamento);
        }

        public ActionResult ImportarModelo()
        {
            var fileUpload = this.GetCmp<FileUploadField>("baixarArq");
            var extension = Path.GetExtension(fileUpload.FileName);
            var mensagem = string.Empty;
            try
            {

                if (!fileUpload.HasFile) throw new CoreException("Arquivo nulo inválido.");
                if (!(extension == ".xlsx" || extension == ".xls")) throw new CoreException("Formato do arquivo inválido.");

                IRepositorioPessoaFisica repPessoaFisica = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaFisica>(UnidadeTrabalho);
                IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
                if (repPessoaFisica == null)
                {
                    throw new CoreException("repPessoaFisica");
                }
                if (repUsuario == null)
                {
                    throw new CoreException("repUsuario");
                }

                bool linhaVazia = true;
                bool importouAlgum = false;
                string messagemResult = string.Empty;

                XSSFWorkbook hssfwb = new XSSFWorkbook(fileUpload.PostedFile.InputStream);
                ISheet sheet = hssfwb.GetSheet("Plan1");

                int totalRows = 0;
                for (int row = 1; row <= sheet.LastRowNum; row++)
                {
                    if (sheet.GetRow(row) == null) continue;
                    totalRows++;
                }

                string maskNum = "";
                if (totalRows < 10) maskNum = "0";
                else if (totalRows < 100) maskNum = "00";
                else if (totalRows < 1000) maskNum = "000";
                //else if (totalRows < 10000) maskNum = "0000";
                else throw new CoreException("Não é possível importar mais de 999 registros simultaneamente.");

                List<string> listaErro = new List<string>();
                for (int row = 1; row <= sheet.LastRowNum; row++)
                {
                    if (sheet.GetRow(row) == null) continue;

                    int numAux = 0;
                    string textAux = "";
                    DateTime dateAux = new DateTime();
                    List<string> errosNaLinha = new List<string>();

                    var pessoaAg = new PessoaAgendamento();
                    pessoaAg.Codigo = 0;
                    pessoaAg.CPF = sheet.GetRow(row).GetCell(0) != null ? sheet.GetRow(row).GetCell(0).StringCellValue.Trim() : null; //CPF
                    if (string.IsNullOrEmpty(pessoaAg.CPF))
                        pessoaAg.Passaporte = sheet.GetRow(row).GetCell(1) != null ? sheet.GetRow(row).GetCell(1).StringCellValue.Trim() : null; //Passaporte

                    if (string.IsNullOrEmpty(pessoaAg.CPF) && string.IsNullOrEmpty(pessoaAg.Passaporte)) errosNaLinha.Add("O CPF e Passaporte não podem ser nulos. Informe o CPF ou Passaporte.");

                    pessoaAg.Nome = sheet.GetRow(row).GetCell(2) != null && sheet.GetRow(row).GetCell(2).StringCellValue.Trim().ToString() != "" && sheet.GetRow(row).GetCell(2).StringCellValue.Trim().ToString() != string.Empty ? sheet.GetRow(row).GetCell(2).StringCellValue.Trim() : null; //Nome

                    mensagem = mensagem + "Nome";

                    pessoaAg.Apelido = sheet.GetRow(row).GetCell(3) != null && sheet.GetRow(row).GetCell(3).StringCellValue.Trim().ToString() != "" && sheet.GetRow(row).GetCell(3).StringCellValue.Trim().ToString() != string.Empty ? sheet.GetRow(row).GetCell(3).StringCellValue.Trim() : null; //Nome de Guerra
                    if (string.IsNullOrEmpty(pessoaAg.Apelido) && pessoaAg.Nome != null)
                    {
                        var textInfo = new CultureInfo("pt-BR", false).TextInfo;
                        var splitNome = pessoaAg.Nome.Split(' ');
                        pessoaAg.Apelido = pessoaAg.Nome != null ? textInfo.ToTitleCase($"{splitNome[0]} {splitNome[splitNome.Length - 1]}") : null;
                    }

                    mensagem = mensagem + "Apelido";

                    pessoaAg.Email = sheet.GetRow(row).GetCell(4) != null && sheet.GetRow(row).GetCell(4).StringCellValue.Trim().ToString() != "" && sheet.GetRow(row).GetCell(4).StringCellValue.Trim().ToString() != string.Empty ? sheet.GetRow(row).GetCell(4).StringCellValue.Trim() : null; //Email
                    if (string.IsNullOrEmpty(pessoaAg.Email)) errosNaLinha.Add("O E-mail não pode ser nulo.");

                    textAux = sheet.GetRow(row).GetCell(5) != null && sheet.GetRow(row).GetCell(5).StringCellValue.Trim().ToString() != "" && sheet.GetRow(row).GetCell(5).StringCellValue.Trim().ToString() != string.Empty ? sheet.GetRow(row).GetCell(5).StringCellValue.Trim() : null;
                    if (!string.IsNullOrEmpty(textAux))
                    {
                        dateAux = DateTime.MinValue;
                        DateTime.TryParseExact(sheet.GetRow(row).GetCell(5).StringCellValue.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateAux);
                        if (dateAux == DateTime.MinValue) errosNaLinha.Add("Formato da Data de Emissão do RG inválido, requerido: dd/MM/yyyy.");
                        else pessoaAg.DataNascimento = dateAux; //Data Nascimento (dd/mm/aaaa)
                    }

                    textAux = sheet.GetRow(row).GetCell(6) != null && sheet.GetRow(row).GetCell(6).StringCellValue.Trim().ToString() != "" && sheet.GetRow(row).GetCell(6).StringCellValue.Trim().ToString() != string.Empty ? sheet.GetRow(row).GetCell(6).StringCellValue.Trim().ToUpper() : null;
                    if (string.IsNullOrEmpty(textAux)) errosNaLinha.Add("Sexo não informado.");
                    else if (!(textAux == "MASCULINO" || textAux == "FEMININO")) errosNaLinha.Add("Sexo informado inválido.");
                    else pessoaAg.Sexo = textAux.ToUpper() == "MASCULINO" ? Sexo.Masculino : Sexo.Feminino; //Sexo (Masculino ou Feminino)

                    mensagem = mensagem + "Sexo";

                    string telEmergencial = sheet.GetRow(row).GetCell(7) != null && sheet.GetRow(row).GetCell(7).StringCellValue.Trim().ToString() != "" && sheet.GetRow(row).GetCell(7).StringCellValue.Trim().ToString() != string.Empty ? sheet.GetRow(row).GetCell(7).StringCellValue.Trim() : null;
                    if (string.IsNullOrWhiteSpace(telEmergencial)) errosNaLinha.Add("Tel. Emergencial não informado.");
                    pessoaAg.TelefoneNumEmergencial = telEmergencial; //Tel. De Emergencia

                    ///
                    pessoaAg.NomeMae = sheet.GetRow(row).GetCell(8) != null && sheet.GetRow(row).GetCell(8).StringCellValue.Trim().ToString() != "" && sheet.GetRow(row).GetCell(8).StringCellValue.Trim().ToString() != string.Empty ? sheet.GetRow(row).GetCell(8).StringCellValue.Trim() : null; //Nome Mãe

                    mensagem = mensagem + "NomeMae";

                    pessoaAg.NomePai = sheet.GetRow(row).GetCell(9) != null && sheet.GetRow(row).GetCell(9).StringCellValue.Trim().ToString() != "" && sheet.GetRow(row).GetCell(9).StringCellValue.Trim().ToString() != string.Empty ? sheet.GetRow(row).GetCell(9).StringCellValue.Trim() : null; //Nome Pai

                    mensagem = mensagem + "NomePai";

                    textAux = sheet.GetRow(row).GetCell(10) != null ? sheet.GetRow(row).GetCell(10).StringCellValue.Trim() : null;
                    if (!string.IsNullOrEmpty(textAux))
                    {
                        IRepositorioEndereco repEndereco = Fabrica.Instancia.ObterRepositorio<IRepositorioEndereco>(UnidadeTrabalho);
                        Estado estado = repEndereco.ObterEstadosSigla(textAux);
                        if (estado == null) errosNaLinha.Add("Não foi possível identifcar a UF do Nascimento informado.");
                        pessoaAg.RGOrgaoEmissorUF = estado; //UF Nascimento (Ex.: ES)
                    }

                    mensagem = mensagem + "RGOrgaoEmissorUF";

                    textAux = sheet.GetRow(row).GetCell(11) != null ? sheet.GetRow(row).GetCell(11).StringCellValue.Trim() : null;
                    if (!string.IsNullOrEmpty(textAux))
                    {
                        IRepositorioCidade repCidade = Fabrica.Instancia.ObterRepositorio<IRepositorioCidade>(UnidadeTrabalho);
                        Cidade cidade = repCidade.ObterTodos().Where(X => X.Nome == textAux).FirstOrDefault();
                        if (cidade == null) errosNaLinha.Add("Não foi possível identifcar a Cidade Nascimento informado.");
                        pessoaAg.Naturalidade = cidade; //Cidade Nascimento 
                    }

                    mensagem = mensagem + "Naturalidade";

                    pessoaAg.RG = sheet.GetRow(row).GetCell(12) != null && sheet.GetRow(row).GetCell(12).StringCellValue.Trim().ToString() != "" && sheet.GetRow(row).GetCell(12).StringCellValue.Trim().ToString() != string.Empty ? sheet.GetRow(row).GetCell(12).StringCellValue.Trim() : null; //RG Numero
                    if (string.IsNullOrEmpty(pessoaAg.RG)) errosNaLinha.Add("O RG não pode ser nulo.");

                    mensagem = mensagem + "pessoaAg.RG";

                    pessoaAg.RGOrgaoEmissor = sheet.GetRow(row).GetCell(13) != null && sheet.GetRow(row).GetCell(13).StringCellValue.Trim().ToString() != "" && sheet.GetRow(row).GetCell(13).StringCellValue.Trim().ToString() != string.Empty ? sheet.GetRow(row).GetCell(13).StringCellValue.Trim() : null; //RG Orgao Emissor
                    if (string.IsNullOrEmpty(pessoaAg.RG)) errosNaLinha.Add("O Orgão emissor não pode ser nulo.");

                    mensagem = mensagem + "RGOrgaoEmissor";

                    textAux = sheet.GetRow(row).GetCell(14) != null ? sheet.GetRow(row).GetCell(14).StringCellValue.Trim() : null;
                    if (!string.IsNullOrEmpty(textAux))
                    {
                        IRepositorioEndereco repEndereco = Fabrica.Instancia.ObterRepositorio<IRepositorioEndereco>(UnidadeTrabalho);
                        Estado estado = repEndereco.ObterEstadosSigla(textAux);
                        if (estado == null) errosNaLinha.Add("Não foi possível identifcar a RG UF informado.");
                        pessoaAg.RGOrgaoEmissorUF = estado; //RG UF (Ex.: ES)
                    }
                    if (pessoaAg.RGOrgaoEmissorUF==null) errosNaLinha.Add("O RG UF não pode ser nulo.");

                    mensagem = mensagem + "pessoaAg.RGOrgaoEmissorUF";

                    textAux = sheet.GetRow(row).GetCell(15) != null && sheet.GetRow(row).GetCell(15).StringCellValue.Trim().ToString() != "" && sheet.GetRow(row).GetCell(15).StringCellValue.Trim().ToString() != string.Empty ? sheet.GetRow(row).GetCell(15).StringCellValue.Trim() : null;
                    if (!string.IsNullOrEmpty(textAux))
                    {
                        dateAux = DateTime.MinValue;
                        DateTime.TryParseExact(sheet.GetRow(row).GetCell(15).StringCellValue.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateAux);
                        if (dateAux == DateTime.MinValue) errosNaLinha.Add("Formato da Data de Emissão do RG inválido, requerido: dd/MM/yyyy.");
                        else pessoaAg.RGDataEmissao = dateAux; //RG Data Emissao (dd/mm/aaaa)
                    }
                    if (pessoaAg.RGDataEmissao == null) errosNaLinha.Add("O RG Data Emissao não pode ser nulo.");

                    mensagem = mensagem + "pessoaAg.RGDataEmissao";

                    pessoaAg.CTPS = Convert.ToInt64(sheet.GetRow(row).GetCell(16) != null && sheet.GetRow(row).GetCell(16).StringCellValue.Trim().ToString() != "" && sheet.GetRow(row).GetCell(16).StringCellValue.Trim().ToString() != string.Empty ? sheet.GetRow(row).GetCell(16).StringCellValue.Trim() : null); //CTPS Número
                    if (!(pessoaAg.CTPS > 0)) errosNaLinha.Add("O CTPS Número não pode ser nulo.");

                    mensagem = mensagem + "pessoaAg.CTPS";

                    pessoaAg.CTPSSerie = sheet.GetRow(row).GetCell(17) != null && sheet.GetRow(row).GetCell(17).StringCellValue.Trim().ToString() != "" && sheet.GetRow(row).GetCell(17).StringCellValue.Trim().ToString() != string.Empty ? sheet.GetRow(row).GetCell(17).StringCellValue.Trim() : null; //CTPS Série
                    if (pessoaAg.CTPSSerie == null) errosNaLinha.Add("O CTPS Série não pode ser nulo.");
                    
                    mensagem = mensagem + "pessoaAg.CTPSSerie";

                    textAux = sheet.GetRow(row).GetCell(18) != null ? sheet.GetRow(row).GetCell(18).StringCellValue.Trim() : null;
                    if (!string.IsNullOrEmpty(textAux))
                    {
                        IRepositorioEndereco repEndereco = Fabrica.Instancia.ObterRepositorio<IRepositorioEndereco>(UnidadeTrabalho);
                        Estado estado = repEndereco.ObterEstadosSigla(textAux);
                        if (estado == null) errosNaLinha.Add("Não foi possível identifcar a UF do RG informado.");
                        pessoaAg.CTPSEstado = estado; //CTPS UF (Ex.: ES)
                    }
                    if (pessoaAg.CTPSEstado == null) errosNaLinha.Add("O CTPS UF não pode ser nulo.");

                    mensagem = mensagem + "pessoaAg.CTPSEstado";

                    textAux = sheet.GetRow(row).GetCell(19) != null && sheet.GetRow(row).GetCell(19).StringCellValue.Trim().ToString() != "" && sheet.GetRow(row).GetCell(19).StringCellValue.Trim().ToString() != string.Empty ? sheet.GetRow(row).GetCell(19).StringCellValue.Trim() : null;
                    if (!string.IsNullOrEmpty(textAux))
                    {
                        dateAux = DateTime.MinValue;
                        DateTime.TryParseExact(sheet.GetRow(row).GetCell(19).StringCellValue.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateAux);
                        if (dateAux == DateTime.MinValue) errosNaLinha.Add("Formato da Data de Emissão do RG inválido, requerido: dd/MM/yyyy.");
                        else pessoaAg.CTPSData = dateAux; //CTPS Data Emissão (dd/mm/aaaa)
                    }
                    if (pessoaAg.CTPSData == null) errosNaLinha.Add("O CTPS Data Emissão não pode ser nulo.");

                    mensagem = mensagem + "pessoaAg.CTPSData";

                    var prestadorAgendamento = new PrestadorServicoAgendamento();
                    pessoaAg.DataRegistro = DateTime.Now;
                    prestadorAgendamento.PessoaAg = pessoaAg;

                    mensagem = mensagem + "prestadorAgendamento.PessoaAg = pessoaAg";

                    string textTipoPrestador = sheet.GetRow(row).GetCell(20) != null && sheet.GetRow(row).GetCell(20).StringCellValue.Trim().ToString() != "" && sheet.GetRow(row).GetCell(20).StringCellValue.Trim().ToString() != string.Empty ? sheet.GetRow(row).GetCell(20).StringCellValue.Trim() : null; //Tipo Prestador de Serviço
                    if (textTipoPrestador == "Temporário" || textTipoPrestador == "Emergencial")
                    {
                        prestadorAgendamento.TipoPrestador = EnumExtensoes.ParseEnum<TipoPrestador>(textTipoPrestador);
                    }
                    else
                    {
                       errosNaLinha.Add("O  Tipo de Prestador é diferente de Temporario Ou TipoPrestador.");
                    }

                    mensagem = mensagem + "prestadorAgendamento.TipoPrestador";

                    // CPF do Solicitante
                    textAux = sheet.GetRow(row).GetCell(21) != null ? sheet.GetRow(row).GetCell(21).StringCellValue.Trim() : null;
                    string cpfSolicitante = textAux;
                    if (string.IsNullOrEmpty(cpfSolicitante))
                        errosNaLinha.Add("CPF do Solicitante não informado.");

                    else
                    {
                        cpfSolicitante = cpfSolicitante.Replace("-", "").Replace(".", "");
                        Papel papelSolicitante = null;
                        var pf = repPessoaFisica.ObterPorCpfPassaporte(false, cpfSolicitante);
                        if (pf == null) errosNaLinha.Add("Não foi possível identificar o CPF do Solicitante informado.");
                        else
                        {
                            papelSolicitante = pf.ObterPapel(true);                            
                            if (papelSolicitante == null) errosNaLinha.Add("Solicitante informado não possui papel Ativo.");
                            else
                            { 
                                Usuario UsuarioSolicitante = UsuarioSolicitante = repUsuario.ObterTodos().Where(X => X.Codigo == papelSolicitante.PessoaFisica.Codigo).FirstOrDefault();
                                if (UsuarioSolicitante == null) errosNaLinha.Add("Solicitante informado não possui usuário Ativo.");
                                else
                                if (papelSolicitante == null) errosNaLinha.Add("Solicitante informado não possui papel Ativo.");
                                else if (!(papelSolicitante is Colaborador)) errosNaLinha.Add("Solicitante informado não é um Colaborador.");
                                else
                                {
                                    //Colaborador colabSolicitante = (Colaborador)papelSolicitante;

                                    prestadorAgendamento.Solicitante = UsuarioSolicitante;
                                    /*
                                    prestadorAgendamento.Solicitante = new Usuario()
                                    {
                                        Codigo = UsuarioSolicitante != null ? UsuarioSolicitante.Codigo: null,
                                        PessoaFisica = new PessoaFisica()
                                        {
                                            Codigo = colabSolicitante.PessoaFisica.Codigo,
                                            CPF = cpfSolicitante
                                        }
                                    };*/ //CPF do Solicitante
                                }
                            }
                        }
                    }

                    mensagem = mensagem + "CPF do Solicitante";

                    textAux = sheet.GetRow(row).GetCell(22) != null && sheet.GetRow(row).GetCell(22).StringCellValue.Trim().ToString() != "" && sheet.GetRow(row).GetCell(22).StringCellValue.Trim().ToString() != string.Empty ? sheet.GetRow(row).GetCell(22).StringCellValue.Trim() : null; //Pessoa Jurídica
                    if (textAux == "1")
                        prestadorAgendamento.PessoaJuridica = true; //Pessoa Jurídica
                    else if (textAux == "0")
                    {
                        prestadorAgendamento.PessoaJuridica = false; //Pessoa Jurídica
                    }
                    else
                    {
                        if (pessoaAg.CTPSData == null) errosNaLinha.Add("O valoe do campo Pessoa Jurídica esta diferente de 0 ou 1.");
                    }

                    mensagem = mensagem + "prestadorAgendamento.PessoaJuridica";

                    textAux = sheet.GetRow(row).GetCell(23) != null ? sheet.GetRow(row).GetCell(23).StringCellValue.Trim() : null;
                    if (!string.IsNullOrEmpty(textAux))
                    {
                        IRepositorioEmpresa repEmpresa = Fabrica.Instancia.ObterRepositorio<IRepositorioEmpresa>(UnidadeTrabalho);
                        Empresa empresa = repEmpresa.ObterTodos().Where(X => X.CNPJ == textAux).FirstOrDefault();
                        if (empresa == null) errosNaLinha.Add("Não foi possível identifcar a Empresa informada.");
                        prestadorAgendamento.Empresa = empresa; //CNPJ Empresa
                    }
                    if (prestadorAgendamento.Empresa == null) errosNaLinha.Add("O CNPJ Empresa não pode ser nulo.");

                    mensagem = mensagem + "prestadorAgendamento.Empresa";

                    //(item 24) Não será editável na planilha. Será preenchida automaticamente no sistema com o valor “Não informado”.
                    textAux = "Não informado";
                    if (!string.IsNullOrEmpty(textAux))
                    {
                        IRepositorioCargo repCargo = Fabrica.Instancia.ObterRepositorio<IRepositorioCargo>(UnidadeTrabalho);
                        Cargo cargo = repCargo.ObterTodos().Where(X => X.Nome == textAux).FirstOrDefault();
                        if (cargo == null) errosNaLinha.Add("Não foi possível identifcar o Cargo informado.");
                        prestadorAgendamento.Cargo = cargo; //Cargo 
                    }
                    if (prestadorAgendamento.Cargo == null) errosNaLinha.Add("O Cargo não pode ser nulo.");

                    mensagem = mensagem + "prestadorAgendamento.Cargo";

                    textAux = sheet.GetRow(row).GetCell(25) != null && sheet.GetRow(row).GetCell(25).StringCellValue.Trim().ToString() != "" && sheet.GetRow(row).GetCell(25).StringCellValue.Trim().ToString() != string.Empty ? sheet.GetRow(row).GetCell(25).StringCellValue.Trim() : null;
                    if (!string.IsNullOrEmpty(textAux))
                    {
                        dateAux = DateTime.MinValue;
                        DateTime.TryParseExact(sheet.GetRow(row).GetCell(25).StringCellValue.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateAux);
                        if (dateAux == DateTime.MinValue) errosNaLinha.Add("Formato da Data Admissão inválido, requerido: dd/MM/yyyy.");
                        else prestadorAgendamento.DataAdmissao = dateAux; //Data Admissão (dd/mm/aaaa)
                    }
                    if (prestadorAgendamento.DataAdmissao == null) errosNaLinha.Add("A Data Admissão não pode ser nula.");

                    mensagem = mensagem + "prestadorAgendamento.DataAdmissao";

                    textAux = sheet.GetRow(row).GetCell(26) != null && sheet.GetRow(row).GetCell(26).StringCellValue.Trim().ToString() != "" && sheet.GetRow(row).GetCell(26).StringCellValue.Trim().ToString() != string.Empty ? sheet.GetRow(row).GetCell(26).StringCellValue.Trim() : null;
                    if (!string.IsNullOrEmpty(textAux))
                    {
                        dateAux = DateTime.MinValue;
                        DateTime.TryParseExact(sheet.GetRow(row).GetCell(26).StringCellValue.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateAux);
                        if (dateAux == DateTime.MinValue) errosNaLinha.Add("Formato da Data Admissão inválido, requerido: dd/MM/yyyy.");
                        else prestadorAgendamento.PessoaAg.DataValidadeVisto = dateAux; //Data Validade (dd/mm/aaaa)
                    }
                    if (prestadorAgendamento.PessoaAg.DataValidadeVisto == null) errosNaLinha.Add("A Data Validade não pode ser nula.");

                    mensagem = mensagem + "prestadorAgendamento.PessoaAg.DataValidadeVisto";

                    //var repPreCad = Fabrica.Instancia.ObterRepositorio<IRepositorioPreCadastroPrestador>(UnidadeTrabalho);
                    //var x = repPreCad.ObterTodos().Where(X => X.PessoaAg.CPF == "97341137630").FirstOrDefault();

                    prestadorAgendamento.PessoaAg.EnderecoComplemento = sheet.GetRow(row).GetCell(27) != null && sheet.GetRow(row).GetCell(27).StringCellValue.Trim().ToString() != "" && sheet.GetRow(row).GetCell(27).StringCellValue.Trim().ToString() != string.Empty ? sheet.GetRow(row).GetCell(27).StringCellValue.Trim() : null; //Endereço
                    if (prestadorAgendamento.PessoaAg.EnderecoComplemento == null) errosNaLinha.Add("O Endereço não pode ser nulo.");

                    mensagem = mensagem + "prestadorAgendamento.PessoaAg.EnderecoComplemento";

                    /*
                    textAux = sheet.GetRow(row).GetCell(0) != null ? sheet.GetRow(row).GetCell(0).StringCellValue.Trim() : null;
                    if (!string.IsNullOrEmpty(textAux))
                    {
                        var repColabPed = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);
                        var gestorResponsavel = repColabPed.ObterTodos().Where(v => v.PessoaFisica.CPF == textAux && v.).FirstOrDefault();                        
                        if (gestorResponsavel == null) errosNaLinha.Add("Não foi possível identifcar o Gestor Responsável informado.");
                        prestadorAgendamento.GestorResponsavel = gestorResponsavel; //CPF Gestor 
                    }*/

                    // CPF do Gestor Responsável
                    textAux = sheet.GetRow(row).GetCell(28) != null ? sheet.GetRow(row).GetCell(28).StringCellValue.Trim() : null;
                    string cpfGestorResponsavel = textAux;
                    if (string.IsNullOrEmpty(cpfGestorResponsavel))
                        errosNaLinha.Add("CPF do Gestor Responsável não informado.");
                    else
                    {
                        cpfGestorResponsavel = cpfGestorResponsavel.Replace("-", "").Replace(".", "");
                        Papel papelGestorResponsavel = null;
                        var pf = repPessoaFisica.ObterPorCpfPassaporte(false, cpfGestorResponsavel);
                        if (pf == null) errosNaLinha.Add("Não foi possível identificar o CPF do Gestor Responsável informado.");
                        else
                        {
                            papelGestorResponsavel = pf.ObterPapel(true);
                            if (papelGestorResponsavel == null) errosNaLinha.Add("Gestor Responsável informado não possui papel Ativo.");
                            else if (!(papelGestorResponsavel is Colaborador)) errosNaLinha.Add("Gestor Responsável informado não é um Colaborador.");
                            else
                            {
                                prestadorAgendamento.GestorResponsavel = (Colaborador)papelGestorResponsavel;//CPF do Gestor Responsável                                
                            }
                        }

                    }

                    mensagem = mensagem + "CPF do Gestor Responsável";
                    mensagem = mensagem + "Sucesso passou !!!";

                    try
                    {
                        if (errosNaLinha.Any()) throw new Exception(string.Join(" ", errosNaLinha.ToArray()));
                        List<DocumentoMidiaModelView> documentos = null;
                        Incluir(prestadorAgendamento, new string[] { }, prestadorAgendamento.GestorResponsavel.Codigo, documentos);

                        importouAlgum = true;
                    }
                    catch (Exception e)
                    {
                        for (int i = 0; i <= 18; i++)
                        {
                            if (!linhaVazia)
                                break;
                            try
                            {
                                string hasValue = sheet.GetRow(row).GetCell(8).StringCellValue.Trim();
                                linhaVazia = false;
                            }
                            catch (Exception ex) {
                                //throw new Exception("teste" + ex);
                            }
                        }
                        if (!linhaVazia) listaErro.Add($"Linha {row.ToString(maskNum)}: {e.Message}");
                    }
                }

                this.GetCmp<Panel>("panelImportacao").Hidden = true;
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.INFO,
                    Title = "Sucesso",
                    Message = "Ação executada com sucesso." //+ mensagem
                });

                if (listaErro.Any() || !importouAlgum)
                {
                    if (!importouAlgum)
                        messagemResult = $"Nenhum registro foi importado{(listaErro.Any() ? ":" : ".")}";
                    else
                        messagemResult = listaErro.Count > 1 ?
                            $"Atenção: {listaErro.Count} registros não foram importados:" :
                            "O seguinte registro não foi importado:";

                    messagemResult += "<BR/>" + string.Join("<BR/>", listaErro.ToArray());
                    this.GetCmp<Panel>("panelImportacao").Html = messagemResult;
                    this.GetCmp<Panel>("panelImportacao").Show();
                }
            }
            catch (CoreException cEx)
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.ERROR,
                    Title = "Erro",
                    Message = cEx.Message //+ mensagem
                });
            }
            catch (Exception ex)
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.ERROR,
                    Title = "Erro",
                    Message = "Não foi possível realizar importação." + ex.Message //+ "/n" +mensagem
                });
            }
            finally
            {
                X.Mask.Hide();
            }

            DirectResult result = new DirectResult();
            result.IsUpload = true;
            return result;
        }


    }
}
