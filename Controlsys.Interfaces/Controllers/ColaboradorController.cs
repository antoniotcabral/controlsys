﻿using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.AgendamentosPessoa;
using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Enderecos;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Controlsys.Dominio.Seguranca;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Acesso;
using Controlsys.Repositorio.Agendamentos;
using Controlsys.Repositorio.Parametros;
using Controlsys.Repositorios.Acesso;
using Controlsys.Repositorios.Empresas;
using Controlsys.Repositorios.Enderecos;
using Controlsys.Repositorios.Parametros;
using Controlsys.Repositorios.Pessoas;
using Controlsys.Repositorios.Seguranca;
using Ext.Net;
using Ext.Net.MVC;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar colaboradors.
    /// </summary>
    [ExtendController]
    public class ColaboradorController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /Colaborador/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.ColaboradorController.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public ColaboradorController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        ///  <summary>
        ///  Retorna a página de colaborador.
        ///  </summary>
        /// 
        ///  <param name="codigo">
        ///  O(a) codigo.
        ///  </param>
        /// <param name="codPreCad">
        /// Código do Pré Cadastro
        /// </param>
        /// <returns>
        ///  Um(a) ActionResult.
        ///  </returns>
        public ActionResult Index(int? codigo, int? codPreCad)
        {
            #region Regra Edição Empregado SAP


            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            Usuario usuarioLogado = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name, somenteAtivos: true);

            if (usuarioLogado != null)
            {
                var grupos = usuarioLogado.GruposUsuarios.Select(g => g.Grupo);
                var acoes = grupos.SelectMany(g => g.Acoes);
                ViewBag.PodeIncluirAlterarEmpregadoSAP = (acoes.Where(a => a.Url == "/Colaborador/IncluirAlterarEmpregadoSAP")).Any();
                ViewBag.PodeAlterarGrupoTrabColaborador = (acoes.Where(a => a.Url == "/Colaborador/AlterarGrupoTrabColaborador")).Any();
            }

            #endregion

            if (codigo.HasValue)
                X.GetCmp<Hidden>("recordId").SetValue(codigo);

            if (codPreCad.HasValue)
                return View("Index", model: codPreCad.ToString());

            return View();
        }

        /// <summary>
        /// Inclui um novo colaborador.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="colaborador">
        /// O(a) colaborador.
        /// </param>
        /// <param name="propModificadasPessoa">
        /// O(a) property modificadas pessoa.
        /// </param>
        /// <param name="treinamentos">
        /// O(a) treinamentos.
        /// </param>
        /// <param name="documentos">
        /// O(a) documentos.
        /// </param>
        /// <param name="excecoes">
        /// O(a) excecoes.
        /// </param>
        /// <param name="imagem">
        /// O(a) imagem.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Incluir(Colaborador colaborador, string[] propModificadasPessoa, List<TreinamentoPessoa> treinamentos, List<DocumentoMidiaModelView> documentos, List<ExcecaoAcesso> excecoes, string imagem, int? codPreCad, int CodigoGrupoTrabalho = 0)
        {
            colaborador.Empresa.Nome = Empresa.RetornaApenasNomeEmpresa(colaborador.Empresa.Nome);
            IRepositorioGrupo repGrupo = null;
            IRepositorioParametro repParam = null;
            try
            {
                UnidadeTrabalho.BeginTransaction();

                //repositorios
                IRepositorioColaborador repColab = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);
                IRepositorioPessoaFisica repPessoaFisica = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaFisica>(UnidadeTrabalho);
                IRepositorioEndereco repEndereco = Fabrica.Instancia.ObterRepositorio<IRepositorioEndereco>(UnidadeTrabalho);
                IRepositorioDocumento repDoc = Fabrica.Instancia.ObterRepositorio<IRepositorioDocumento>(UnidadeTrabalho);
                IRepositorioTreinamento repTreinamento = Fabrica.Instancia.ObterRepositorio<IRepositorioTreinamento>(UnidadeTrabalho);
                IRepositorioPapel repPapel = Fabrica.Instancia.ObterRepositorio<IRepositorioPapel>(UnidadeTrabalho);
                IRepositorioSincronizaPessoaAcesso repSincronizaPessoaAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioSincronizaPessoaAcesso>(UnidadeTrabalho);
                IRepositorioPreCadastroColaborador repPreCadColab = Fabrica.Instancia.ObterRepositorio<IRepositorioPreCadastroColaborador>(UnidadeTrabalho);
                IRepositorioEmpresa repEmpresa = Fabrica.Instancia.ObterRepositorio<IRepositorioEmpresa>(UnidadeTrabalho);
                IRepositorioArea repArea = Fabrica.Instancia.ObterRepositorio<IRepositorioArea>(UnidadeTrabalho);
                IRepositorioSetorCusto repSetorCusto = Fabrica.Instancia.ObterRepositorio<IRepositorioSetorCusto>(UnidadeTrabalho);
                IRepositorioAlocacaoColaborador repAlocacao = Fabrica.Instancia.ObterRepositorio<IRepositorioAlocacaoColaborador>(UnidadeTrabalho);
                IRepositorio<EmpregadoSAP> repEmpSap = Fabrica.Instancia.ObterRepositorio<IRepositorio<EmpregadoSAP>>(UnidadeTrabalho);
                repGrupo = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupo>(UnidadeTrabalho);
                repParam = Fabrica.Instancia.ObterRepositorio<IRepositorioParametro>(UnidadeTrabalho);

                if (colaborador.Area != null)
                    colaborador.Area = repArea.ObterPorId(colaborador.Area.Codigo);

                if (colaborador.SetorCusto != null)
                    colaborador.SetorCusto = repSetorCusto.ObterPorId(colaborador.SetorCusto.Codigo);

                PessoaFisica pessoaFisica = colaborador.PessoaFisica;

                SetorCusto setorCusto = null;
                var preCad = new ColaboradorAgendamento();
                if (codPreCad.HasValue)
                {
                    preCad = repPreCadColab.ObterPorId(codPreCad);
                    setorCusto = preCad.SetorCusto != null && preCad.SetorCusto.Codigo > 0 ? repSetorCusto.ObterPorId(preCad.SetorCusto.Codigo) : null;

                    repPreCadColab.Validar(EstadoObjeto.Alterado, preCad);

                    var statusAtual = preCad.ObterStatusAtual().StatusAgendamento;
                    if (statusAtual != StatusAgendamento.AguardandoAprovacao && statusAtual != StatusAgendamento.CadastroNaoAprovado &&
                        !(preCad.Status.Any(s => s.StatusAgendamento == StatusAgendamento.ContratacaoAutorizada)))
                        throw new CoreException("Não foi possível aprovar este pré cadastro, ele precisa primeiro ser autorizado pelo gestor.");

                    colaborador.EmpregadoSAP = preCad.EmpregadoSap;

                    PessoaFisica pfAux = repPessoaFisica.ObterPorCpfPassaporte(false, preCad.PessoaAg.CPF);
                    if (pfAux != null)
                    {
                        if (colaborador.PessoaFisica == null)
                            colaborador.PessoaFisica = pfAux;
                        if (string.IsNullOrEmpty(colaborador.PessoaFisica.Codigo))
                            colaborador.PessoaFisica.Codigo = pfAux.Codigo;
                    }
                }

                if (colaborador.EmpregadoSAP != null && colaborador.EmpregadoSAP.Codigo > 0)
                {
                    AplicarRegraEmpregadoSap(colaborador);
                }

                colaborador.Empresa = repEmpresa.ObterTodos().FirstOrDefault(e => e.Codigo.Equals(colaborador.Empresa.Codigo) || e.Nome.Equals(colaborador.Empresa.Nome));
                Endereco end = colaborador.PessoaFisica.Endereco;

                //foto
                if (!string.IsNullOrEmpty(imagem))
                    pessoaFisica.Foto = Convert.FromBase64String(imagem);

                if (pessoaFisica.CPF != null)
                    pessoaFisica.CPF = pessoaFisica.CPF.Replace(".", "").Replace("-", "");

                if (!string.IsNullOrEmpty(pessoaFisica.Codigo))
                {
                    pessoaFisica = repPessoaFisica.ObterPorId(pessoaFisica, propModificadasPessoa);
                    repPessoaFisica.Atualizar(pessoaFisica);
                }

                #region [ENDEREÇO]
                if (end != null)
                {
                    //endereço
                    pessoaFisica.Endereco = repEndereco.VerificarEndereco(end);
                    pessoaFisica.Endereco.DataRegistro = DateTime.Now;
                    pessoaFisica.Endereco.Ativar();
                    repEndereco.Salvar(pessoaFisica.Endereco);
                }
                #endregion

                //validações referentes à pessoa
                if (string.IsNullOrEmpty(pessoaFisica.Codigo))
                {
                    repPessoaFisica.Salvar(pessoaFisica, null);
                }

                if (propModificadasPessoa != null)
                {
                    pessoaFisica = insereTelefone(propModificadasPessoa, pessoaFisica, colaborador);
                }

                repColab.Validar(EstadoObjeto.Novo, colaborador);

                //cadastrar papel
                Papel papel = repPapel.SalvarPapelLog(colaborador.InserirPapelLog(DateTime.Now, null, StatusPapelLog.Ativo));

                repColab.Salvar(colaborador);

                if (preCad.Codigo > 0 && setorCusto != null)
                {
                    AlocacaoColaborador alocacaoColaborador = new AlocacaoColaborador();
                    alocacaoColaborador.PedidoCompra = preCad.GestorPO.Pedido;
                    alocacaoColaborador.Papel = colaborador;
                    alocacaoColaborador.SetorCusto = setorCusto;
                    alocacaoColaborador.Ativar();
                    alocacaoColaborador.DataRegistro = DateTime.Now;
                    repAlocacao.Validar(alocacaoColaborador, EstadoObjeto.Novo);
                    repAlocacao.Salvar(alocacaoColaborador);
                }

                //cadastrar asos
                foreach (var item in colaborador.ASOs)
                {
                    repPapel.SalvarAso(item, colaborador);
                    //repSincronizaPessoaAcesso.Sincronizar(colaborador, TipoSincronizacaoPessoa.Conceder);
                }

                //cadastrar treinamentos
                if (treinamentos != null)
                {
                    foreach (var item in treinamentos)
                        repTreinamento.SalvarTreinamentoPessoa(colaborador, item);
                }

                if (excecoes != null)
                {
                    foreach (var item in excecoes)
                        repPapel.SalvarExcecaoAcesso(colaborador, item);
                }

                // cadastrar documentos
                foreach (var item in documentos)
                {
                    // Se Documentos com status completo não tiverem midia gera erro.
                    if (item.Documento.Status == StatusDocumento.Completo && (item.Midias == null || !item.Midias.Any(m => m != null)))
                    {
                        throw new CoreException("Não é possível salvar documento com status 'Completo' sem mídia anexada.");
                    }

                    DocumentoPapel docPapel = repDoc.SalvarDocPapel(papel, item.Documento);
                    if (item.Documento.Codigo <= 0)
                    {
                        string path = ConfigurationManager.AppSettings.Get("pathUploads");

                        if (!System.IO.Directory.Exists(path))
                            throw new CoreException("O diretório onde a midia seria armazanada não está acessivel.");

                        IRepositorioDocumentoMidia repDocumentoMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioDocumentoMidia>(UnidadeTrabalho);
                        IRepositorioMidia repMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioMidia>(UnidadeTrabalho);

                        int cont = 1;

                        foreach (MidiaModelView midia in item.Midias ?? Enumerable.Empty<MidiaModelView>())
                        {
                            if (midia == null)
                                continue;
                            string nomeDisco = "PF-" + (string.IsNullOrEmpty(colaborador.PessoaFisica.CPF) ? colaborador.PessoaFisica.Passaporte : colaborador.PessoaFisica.CPF) + "-" + colaborador.Codigo.ToString() + "-" + docPapel.Documento.TipoDocumento.Sigla + "-" + cont + "." + midia.Extensao;
                            Midia m = new Midia();
                            m.DataRegistro = midia.DataRegistro;
                            m.Extensao = midia.Extensao;
                            m.MimeType = midia.MIME;
                            m.NomeArquivo = nomeDisco;
                            repMidia.Salvar(m);

                            repDocumentoMidia.SalvarDocMidia(docPapel.Documento, m);

                            System.IO.File.WriteAllBytes(m.Url, Convert.FromBase64String(midia.Arquivo));

                            cont++;
                        }
                    }

                }

                if (codPreCad.HasValue)
                {
                    preCad.Papel = papel;
                    preCad.EmpregadoSap = preCad.EmpregadoSap != null ? repEmpSap.ObterPorId(preCad.EmpregadoSap.Codigo) : null;
                    repPreCadColab.Salvar(preCad);

                    var repPapelAg = Fabrica.Instancia.ObterRepositorio<IRepositorioPapelAgendamento>(UnidadeTrabalho);
                    repPapelAg.SalvarStatusPapelAg(preCad.InserirPapelAg(StatusAgendamento.CadastroAprovado, null));

                    if (preCad.GrupoTrabalho != null)
                    {
                        GrupoTrabalhoColab gtc = new GrupoTrabalhoColab()
                        {
                            DataRegistro = DateTime.Now,
                            GrupoTrabalho = preCad.GrupoTrabalho,
                            Papel = colaborador
                        };
                        gtc.Ativar();

                        IRepositorioGrupoTrabalhoColab repgtc = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoTrabalhoColab>(UnidadeTrabalho);
                        repgtc.Salvar(gtc);
                    }
                }

                if (colaborador != null)
                {
                    IRepositorioGrupoTrabalho repGrupoTrabalho = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoTrabalho>(UnidadeTrabalho);
                    GrupoTrabalho grupoTrabalho = CodigoGrupoTrabalho > 0 ? repGrupoTrabalho.ObterPorId(CodigoGrupoTrabalho) : null;
                    AlterarGrupoTrabColaborador(grupoTrabalho, colaborador);
                }

                UnidadeTrabalho.Commit();
                return Json(montaObj(colaborador));
            }
            catch (CoreException msg)
            {
                try
                {
                    string msgCadastroPessoa = repParam.RecuperarParametro(ParametroSistema.MensagemCadastroPessoaFisica).Valor;
                    if (msg.Message == msgCadastroPessoa)
                    {
                        Grupo g = repGrupo.ObterPorId(int.Parse(repParam.RecuperarParametro(ParametroSistema.GrupoUsuarioResponsavelPessoaCritica).Valor));
                        string msgEmailGrupoUsuario = repParam.RecuperarParametro(ParametroSistema.MensagemEmailNotificaGrupoCadastroPessoaCritica).Valor;

                        Dictionary<string, string> valoresSubs = new Dictionary<string, string>();

                        IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
                        string userLogado = HttpContext.User.Identity.Name;
                        Usuario u = repUsuario.ObterPorLogin(userLogado);

                        IRepositorioPessoaCritica repPC = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaCritica>(UnidadeTrabalho);
                        PessoaCritica pc = repPC.ObterPorCPF(colaborador.PessoaFisica.CPF, true);

                        //Informações da Pessoa que está cadastrando
                        valoresSubs.Add("$$Nome$$", u.PessoaFisica.Nome);
                        valoresSubs.Add("$$Empresa$$", (u.PessoaFisica.ObterPapel(true) is Colaborador ? (u.PessoaFisica.ObterPapel(true) as Colaborador).Empresa.Nome : ""));
                        valoresSubs.Add("$$Funcao$$", (u.PessoaFisica.ObterPapel(true) is Colaborador ? (u.PessoaFisica.ObterPapel(true) as Colaborador).Cargo.Nome : ""));

                        //Informações da Pessoa que está cadastrando
                        valoresSubs.Add("$$Nome2$$", colaborador.PessoaFisica.Nome);
                        valoresSubs.Add("$$CPF$$", colaborador.PessoaFisica.CPFFormatado());


                        //Informações da inclusão da Pessoa 
                        valoresSubs.Add("$$Motivo$$", (!string.IsNullOrEmpty(pc.MotivoBloqueio.Nome)) ? pc.MotivoBloqueio.Nome : "");
                        valoresSubs.Add("$$Observacao$$", (!string.IsNullOrEmpty(pc.Observacao)) ? pc.Observacao : "");
                        valoresSubs.Add("$$DataInclusao$$", pc.DataBloqueio.ToString("dd/MM/yyyy"));
                        valoresSubs.Add("$$Nome3$$", pc.Responsavel.PessoaFisica.Nome);
                        valoresSubs.Add("$$Funcao2$$", (pc.Responsavel.PessoaFisica.ObterPapel() is Colaborador ? (u.PessoaFisica.ObterPapel() as Colaborador).Cargo.Nome : ""));


                        valoresSubs.Add("$$Msg$$", msgEmailGrupoUsuario);


                        foreach (var item in g.GruposUsuarios.Where(t => t.Ativo))
                        {
                            Globalsys.Util.Email.Enviar("[CONTROLSYS] Pessoa Crítica",
                                                          Server.MapPath("/Content/templates/pessoaCritica.htm"),
                                                          valoresSubs,
                                                          string.Empty,
                                                          null,
                                                          item.Usuario.PessoaFisica.Email);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new CoreException(msg.Message, ex);
                }
                finally
                {
                    UnidadeTrabalho.Rollback();
                }
                throw msg;
            }
            finally
            {
                UnidadeTrabalho.Dispose();
            }
        }

        /// <summary>
        /// Altera um registro de colaborador.
        /// </summary>
        ///
        /// <param name="colaborador">
        /// O(a) colaborador.
        /// </param>
        /// <param name="propModificadasColab">
        /// O(a) property modificadas colab.
        /// </param>
        /// <param name="propModificadasDadosColab">
        /// O(a) property modificadas dados colab.
        /// </param>
        /// <param name="documentos">
        /// O(a) documentos.
        /// </param>
        /// <param name="treinamentoDel">
        /// O(a) treinamento delete.
        /// </param>
        /// <param name="treinamentoNew">
        /// O(a) treinamento new.
        /// </param>
        /// <param name="asosDel">
        /// O(a) asos delete.
        /// </param>
        /// <param name="asosNew">
        /// O(a) asos new.
        /// </param>
        /// <param name="excecoesDel">
        /// O(a) excecoes delete.
        /// </param>
        /// <param name="excecoesNew">
        /// O(a) excecoes new.
        /// </param>
        /// <param name="imagem">
        /// O(a) imagem.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Alterar(Colaborador colaborador, string[] propModificadasColab, string[] propModificadasDadosColab, List<DocumentoMidiaModelView> documentosMidias, TreinamentoPessoa[] treinamentoDel, TreinamentoPessoa[] treinamentoNew, ASO[] asosDel, ASO[] asosNew, ExcecaoAcesso[] excecoesDel, ExcecaoAcesso[] excecoesNew, string imagem, int CodigoGrupoTrabalho = 0)
        {
            colaborador.Empresa.Nome = Empresa.RetornaApenasNomeEmpresa(colaborador.Empresa.Nome);

            IRepositorioColaborador repColab = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);
            IRepositorioPessoaFisica repPessoaFisica = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaFisica>(UnidadeTrabalho);
            IRepositorioDocumento repDoc = Fabrica.Instancia.ObterRepositorio<IRepositorioDocumento>(UnidadeTrabalho);
            IRepositorioTreinamento repTrein = Fabrica.Instancia.ObterRepositorio<IRepositorioTreinamento>(UnidadeTrabalho);
            IRepositorioEndereco repEndereco = Fabrica.Instancia.ObterRepositorio<IRepositorioEndereco>(UnidadeTrabalho);
            IRepositorioPapel repPapel = Fabrica.Instancia.ObterRepositorio<IRepositorioPapel>(UnidadeTrabalho);
            IRepositorioSincronizaPessoaAcesso repSincronizaPessoaAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioSincronizaPessoaAcesso>(UnidadeTrabalho);
            IRepositorioTelefone repTelefone = Fabrica.Instancia.ObterRepositorio<IRepositorioTelefone>(UnidadeTrabalho);
            IRepositorioGrupoAcesso repGrupoAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoAcesso>(UnidadeTrabalho);
            IRepositorioDocumentoMidia repDocumentoMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioDocumentoMidia>(UnidadeTrabalho);
            IRepositorioMidia repMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioMidia>(UnidadeTrabalho);
            IRepositorioArea repArea = Fabrica.Instancia.ObterRepositorio<IRepositorioArea>(UnidadeTrabalho);
            IRepositorioSetorCusto repSetorCusto = Fabrica.Instancia.ObterRepositorio<IRepositorioSetorCusto>(UnidadeTrabalho);


            List<Telefone> telefones = new List<Telefone>(colaborador.PessoaFisica.Telefones);

            //PESSOAFISCA
            PessoaFisica pessoaFisica = colaborador.PessoaFisica;
            Endereco enderecoNovo = pessoaFisica.Endereco;

            pessoaFisica = repPessoaFisica.ObterPorId(pessoaFisica, propModificadasColab);

            if (colaborador.EmpregadoSAP != null && colaborador.EmpregadoSAP.Codigo > 0)
            {
                AplicarRegraEmpregadoSap(colaborador);
            }

            if (colaborador.Area != null)
                colaborador.Area = repArea.ObterPorId(colaborador.Area.Codigo);

            if (colaborador.SetorCusto != null)
                colaborador.SetorCusto = repSetorCusto.ObterPorId(colaborador.SetorCusto.Codigo);

            #region [ENDEREÇO]
            if (enderecoNovo != null && !string.IsNullOrEmpty(enderecoNovo.Logradouro.CEP))
            {
                //endereço
                if (pessoaFisica.Endereco != null)
                    repEndereco.Remover(pessoaFisica.Endereco.Codigo);

                pessoaFisica.Endereco = repEndereco.VerificarEndereco(enderecoNovo);
                pessoaFisica.Endereco.DataRegistro = DateTime.Now;
                pessoaFisica.Endereco.Ativar();
                repEndereco.Salvar(pessoaFisica.Endereco);
            }
            #endregion

            //foto
            if (!string.IsNullOrEmpty(imagem))
                pessoaFisica.Foto = Convert.FromBase64String(imagem);

            if (propModificadasColab == null)
                propModificadasColab = new string[1];

            repPessoaFisica.Validar(EstadoObjeto.Alterado, pessoaFisica);
            repPessoaFisica.Atualizar(pessoaFisica);

            if (propModificadasColab != null)
            {
                pessoaFisica = insereTelefone(propModificadasColab, pessoaFisica, colaborador);
            }

            //COLABORADOR
            colaborador = repColab.ObterPorId(colaborador, propModificadasDadosColab);

            if (propModificadasDadosColab == null)
                propModificadasDadosColab = new string[1];

            string path = ConfigurationManager.AppSettings.Get("pathUploads");

            //DOCUMENTOS
            if (documentosMidias != null)
            {
                List<DocumentoMidiaModelView> docsPapel = new List<DocumentoMidiaModelView>();
                docsPapel = documentosMidias.Where(dm => colaborador.Documentos.Any(cd => dm.Documento.Codigo == cd.Documento.Codigo)).ToList();

                foreach (var item in documentosMidias.Where(dm => colaborador.Documentos.Any(cd => dm.Documento.Codigo == cd.Documento.Codigo)))
                {
                    if (item.Documento.Status == StatusDocumento.Completo && (item.Midias == null || !item.Midias.Any(m => m != null)))
                    {
                        throw new CoreException("Não é possível salvar documento com status 'Completo' sem mídia anexada.");
                    }

                    Documento doc = repDoc.ObterPorId(item.Documento.Codigo);
                    doc.Status = item.Documento.Status;
                    doc.Descricao = item.Documento.Descricao;
                    doc.DataValidade = item.Documento.DataValidade;
                    repDoc.Atualizar(doc);

                    if (item.Midias == null)
                    {
                        DocumentoMidiaModelView item1 = item;
                        var docMidias =
                            repDocumentoMidia.ObterTodos().Where(dm => dm.Documento.Codigo == item1.Documento.Codigo && dm.Ativo);
                        foreach (var documentoMidia in docMidias)
                        {
                            documentoMidia.Inativar();
                            repDocumentoMidia.Atualizar(documentoMidia);
                        }
                        continue;
                    }
                    else
                    {
                        DocumentoMidiaModelView item1 = item;
                        List<int> codigosMidia = item.Midias.Select(i => i.CodigoMidia).ToList();
                        IQueryable<DocumentoMidia> listaDocMidia = repDocumentoMidia.ObterTodos().Where(dm =>
                            dm.Documento.Codigo == item1.Documento.Codigo &&
                            dm.Ativo && !codigosMidia.Contains(dm.Midia.Codigo));

                        foreach (DocumentoMidia docMidia in listaDocMidia)
                        {
                            docMidia.Inativar();
                            repDocumentoMidia.Salvar(docMidia);
                        }
                    }

                    int cont = repDocumentoMidia.ObterTodos().Where(dm => dm.Documento.Codigo == item.Documento.Codigo).Count() + 1;

                    foreach (MidiaModelView midia in item.Midias.Where(m => m.CodigoMidia == 0))
                    {
                        if (!System.IO.Directory.Exists(path))
                            throw new CoreException("O diretório onde a midia seria armazanada não está acessivel.");

                        string nomeDisco = string.Format("PF-{0}-{1}-{2}-{3}.{4}",
                            (string.IsNullOrEmpty(colaborador.PessoaFisica.CPF) ? colaborador.PessoaFisica.Passaporte : colaborador.PessoaFisica.CPF),
                            colaborador.Codigo, doc.TipoDocumento.Sigla, cont, midia.Extensao);

                        Midia m = new Midia();
                        m.DataRegistro = midia.DataRegistro;
                        m.Extensao = midia.Extensao;
                        m.MimeType = midia.MIME;
                        m.NomeArquivo = nomeDisco;

                        repMidia.Salvar(m);

                        repDocumentoMidia.SalvarDocMidia(doc, m);
                        System.IO.File.WriteAllBytes(m.Url, Convert.FromBase64String(midia.Arquivo));

                        cont++;

                    }
                }

                foreach (var item in documentosMidias.Where(dm => !colaborador.Documentos.Any(cd => dm.Documento.Codigo == cd.Documento.Codigo)))
                {

                    if (item.Documento.Status == StatusDocumento.Completo && (item.Midias == null || !item.Midias.Any(m => m != null)))
                    {
                        throw new CoreException("Não é possível salvar documento com status 'Completo' sem mídia anexada.");
                    }

                    DocumentoPapel docPapel = repDoc.SalvarDocPapel(colaborador, item.Documento);

                    int cont = repDocumentoMidia.ObterTodos().Where(dm => dm.Documento.Codigo == item.Documento.Codigo).Count() + 1;

                    foreach (MidiaModelView midia in item.Midias ?? Enumerable.Empty<MidiaModelView>())
                    {
                        if (midia == null)
                            continue;

                        if (!System.IO.Directory.Exists(path))
                            throw new CoreException("O diretório onde a midia seria armazanada não está acessivel.");

                        string nomeDisco = "PF-" + (string.IsNullOrEmpty(colaborador.PessoaFisica.CPF) ? colaborador.PessoaFisica.Passaporte : colaborador.PessoaFisica.CPF) + "-" + colaborador.Codigo.ToString() + "-" + docPapel.Documento.TipoDocumento.Sigla + "-" + cont + "." + midia.Extensao;
                        Midia m = new Midia();
                        m.DataRegistro = midia.DataRegistro;
                        m.Extensao = midia.Extensao;
                        m.MimeType = midia.MIME;
                        m.NomeArquivo = nomeDisco;

                        repMidia.Salvar(m);

                        repDocumentoMidia.SalvarDocMidia(docPapel.Documento, m);

                        System.IO.File.WriteAllBytes(m.Url, Convert.FromBase64String(midia.Arquivo));

                        cont++;
                    }
                }
            }

            //ASOS
            if (asosDel != null)
            {
                foreach (ASO aso in new List<ASO>(colaborador.ASOs.Where(t => asosDel.Any(tdd => tdd.Codigo == t.Codigo))))
                {
                    colaborador.ASOs.Remove(aso);
                    repPapel.RemoverAso(aso);
                }
            }

            if (asosNew != null)
            {
                foreach (var item in asosNew)
                {
                    ASO aso = new ASO();
                    aso.Papel = colaborador;
                    aso.DataRealizacao = item.DataRealizacao;
                    aso.DataVencimento = item.DataVencimento;
                    colaborador.ASOs.Add(aso);
                    repPapel.SalvarAso(aso, colaborador);
                }
            }

            if (asosNew != null || asosDel != null)
            {
                ASO aso1 = colaborador.ASOs.OrderByDescending(a => a.Codigo).FirstOrDefault();
                if (aso1 != null)
                {
                    repSincronizaPessoaAcesso.Sincronizar(colaborador, TipoSincronizacaoPessoa.Conceder, null);

                }
                else
                {
                    repSincronizaPessoaAcesso.Sincronizar(colaborador, TipoSincronizacaoPessoa.Conceder, null);
                }

            }

            //DATA DE VALIDADE COLABORADOR            
            //if (propModificadasDadosColab.Contains("DataValidade"))
            //{
            //    colaborador = repColab.ObterPorId(colaborador, propModificadasDadosColab);
            //    if (colaborador.DataValidade >= DateTime.Today)
            //        repSincronizaPessoaAcesso.Sincronizar(colaborador, TipoSincronizacaoPessoa.Conceder, null);
            //    else
            //        repSincronizaPessoaAcesso.Sincronizar(colaborador, TipoSincronizacaoPessoa.Suspender, null);
            //}
            //TREINAMENTO
            if (treinamentoDel != null)
            {
                foreach (var item in colaborador.Treinamentos.Where(t => treinamentoDel.Any(tdd => tdd.Treinamento.Codigo == t.Treinamento.Codigo)))
                {
                    TreinamentoPessoa treinamento = item;
                    treinamento.Inativar();
                }
            }
            if (treinamentoNew != null)
            {
                foreach (var item in treinamentoNew)
                {
                    TreinamentoPessoa tp = new TreinamentoPessoa();
                    tp.Papel = colaborador;
                    tp.DataRealizacao = item.DataRealizacao;
                    tp.DataValidade = item.DataValidade;
                    repTrein.SalvarTreinamentoPessoa(colaborador, item);
                }
            }

            //Exceção de Bloqueio
            if (excecoesDel != null)
            {
                foreach (var item in colaborador.ExcecaoAcesso.Where(t => excecoesDel.Any(tdd => tdd.Codigo == t.Codigo)))
                {
                    ExcecaoAcesso excecao = item;
                    excecao.Inativar();
                }
            }
            if (excecoesNew != null)
            {
                foreach (var item in excecoesNew)
                {
                    repPapel.SalvarExcecaoAcesso(colaborador, item);
                }
            }

            repColab.Validar(EstadoObjeto.Alterado, colaborador);

            if (propModificadasColab != null && propModificadasColab.Any(a => a == "CNHDataValidade"))
            {
                List<Controladora> controladorasCNH = repGrupoAcesso.ObterControladorasCNH(colaborador);

                if (colaborador.PessoaFisica.CNHDataValidade >= DateTime.Today)
                    repSincronizaPessoaAcesso.Sincronizar(colaborador, TipoSincronizacaoPessoa.Conceder, null, controladorasCNH);
                else
                    repSincronizaPessoaAcesso.Sincronizar(colaborador, TipoSincronizacaoPessoa.Suspender, null, controladorasCNH);
            }

            if (colaborador != null && propModificadasDadosColab.Any(a => a == "CodigoGrupoTrabalho"))
            {
                IRepositorioGrupoTrabalho repGrupoTrabalho = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoTrabalho>(UnidadeTrabalho);
                GrupoTrabalho grupoTrabalho = CodigoGrupoTrabalho > 0 ? repGrupoTrabalho.ObterPorId(CodigoGrupoTrabalho) : null;
                AlterarGrupoTrabColaborador(grupoTrabalho, colaborador);
            }

            return Json(montaObj(colaborador));
        }

        public void AlterarGrupoTrabColaborador(GrupoTrabalho novoGrupoTrabalho, Colaborador col)
        {
            //if (novoGrupoTrabalho == null)                                    
            //    throw new CoreException("Não é possível alterar Grupo de Trabalho. O Grupo de Trabalho esta nulo.");
            if (col == null)
                throw new CoreException("Não é possível alterar Grupo de Trabalho. O Colaborador esta nulo.");
            try
            {
                IRepositorioGrupoTrabalho repGrupoTrabalho = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoTrabalho>(UnidadeTrabalho);
                IRepositorioPapel repPapel = Fabrica.Instancia.ObterRepositorio<IRepositorioPapel>(UnidadeTrabalho);

                if (col != null)
                {
                    GrupoTrabalhoColab grupoTrabalhoColabRemover = col.ObterGrupoTrabalho();
                    if (grupoTrabalhoColabRemover != null)
                    {
                        grupoTrabalhoColabRemover.Inativar();
                    }

                    if (novoGrupoTrabalho != null)
                    {
                        GrupoTrabalhoColab grupoTrabColab = new GrupoTrabalhoColab();
                        grupoTrabColab.GrupoTrabalho = novoGrupoTrabalho;
                        grupoTrabColab.Papel = repPapel.ObterPorId(col.Codigo);
                        grupoTrabColab.DataRegistro = DateTime.Now;
                        grupoTrabColab.Ativar();
                        repGrupoTrabalho.SalvarGrupoTrabalhoColaborador(grupoTrabColab);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new CoreException("Erro ao tentar alterar o grupo de trabalho  do colaborador: " + ex.Message);
            }
        }

        private void AplicarRegraEmpregadoSap(Colaborador colaborador)
        {
            IRepositorio<EmpregadoSAP> repEmpregadoSap = Fabrica.Instancia.ObterRepositorio<IRepositorio<EmpregadoSAP>>(UnidadeTrabalho);

            var empregadoSap = repEmpregadoSap.ObterPorId(colaborador.EmpregadoSAP.Codigo);

            if (colaborador.EmpregadoSAP.GestorPonto != null)
            {
                colaborador.EmpregadoSAP.GestorPonto = colaborador.EmpregadoSAP.GestorPonto != null ? repEmpregadoSap.ObterPorId(colaborador.EmpregadoSAP.GestorPonto.Codigo) : null;
                if (colaborador.EmpregadoSAP.GestorPonto == null)
                    throw new CoreException("Não foi possível encontrar o código do Gestor de Ponto no Controlsys.");
            }

            if (colaborador.EmpregadoSAP.SuperiorImediato != null)
            {
                colaborador.EmpregadoSAP.SuperiorImediato = colaborador.EmpregadoSAP.SuperiorImediato != null ? repEmpregadoSap.ObterPorId(colaborador.EmpregadoSAP.SuperiorImediato.Codigo) : null;
                if (colaborador.EmpregadoSAP.SuperiorImediato == null)
                    throw new CoreException("Não foi possível encontrar o código do Supervisor Imediato no Controlsys.");
            }

            try
            {
                if (empregadoSap == null)
                {
                    empregadoSap = new EmpregadoSAP
                    {
                        Codigo = colaborador.EmpregadoSAP.Codigo,
                        GestorPonto = colaborador.EmpregadoSAP.GestorPonto,
                        SuperiorImediato = colaborador.EmpregadoSAP.SuperiorImediato
                    };

                    repEmpregadoSap.Salvar(empregadoSap);
                }
                else
                {
                    empregadoSap.GestorPonto = colaborador.EmpregadoSAP.GestorPonto;
                    empregadoSap.SuperiorImediato = colaborador.EmpregadoSAP.SuperiorImediato;
                    repEmpregadoSap.Atualizar(empregadoSap);
                }

                colaborador.EmpregadoSAP = empregadoSap;
            }
            catch (Exception ex)
            {

                throw new CoreException("Erro ao tentar aplicar as regras do Empregado SAP: " + ex.Message);
            }
        }

        /// <summary>
        /// Insere telefone.
        /// </summary>
        ///
        /// <param name="propModificadas">
        /// O(a) property modificadas.
        /// </param>
        /// <param name="pessoaFisica">
        /// O(a) pessoa fisica.
        /// </param>
        /// <param name="colaborador">
        /// O(a) colaborador.
        /// </param>
        ///
        /// <returns>
        /// Um(a) PessoaFisica.
        /// </returns>
        private PessoaFisica insereTelefone(string[] propModificadas, PessoaFisica pessoaFisica, Colaborador colaborador)
        {
            IRepositorioTelefone repTelefone = Fabrica.Instancia.ObterRepositorio<IRepositorioTelefone>(UnidadeTrabalho);
            List<Telefone> telefones = new List<Telefone>(colaborador.PessoaFisica.Telefones);

            if (propModificadas.Any(a => a == "TelefoneNumResidencial"))
            {
                Telefone telefoneResidencial = pessoaFisica.Telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Residencial);

                if (telefoneResidencial == null || (telefoneResidencial.Codigo == 0 && !string.IsNullOrEmpty(telefoneResidencial.TelefoneNum)))
                {
                    telefoneResidencial = new Telefone
                    {
                        DataRegistro = DateTime.Now,
                        Ativo = true,
                        TelefoneNum = (telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Residencial) != null) ? telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Residencial).TelefoneNum : "",
                        TipoTelefone = TipoTelefone.Residencial,
                        Pessoa = pessoaFisica
                    };

                    pessoaFisica.Telefones.Add(telefoneResidencial);
                    repTelefone.Salvar(telefoneResidencial);
                }
                else
                {
                    telefoneResidencial.TelefoneNum = (telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Residencial) != null) ? telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Residencial).TelefoneNum : "";
                    repTelefone.Atualizar(telefoneResidencial);
                }
            }

            if (propModificadas.Any(a => a == "TelefoneNumEmergencial"))
            {
                Telefone telefoneResidencial = pessoaFisica.Telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Emergencial);

                if (telefoneResidencial == null || (telefoneResidencial.Codigo == 0 && !string.IsNullOrEmpty(telefoneResidencial.TelefoneNum)))
                {
                    telefoneResidencial = new Telefone
                    {
                        DataRegistro = DateTime.Now,
                        Ativo = true,
                        TelefoneNum = (telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Emergencial) != null) ? telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Emergencial).TelefoneNum : "",
                        TipoTelefone = TipoTelefone.Emergencial,
                        Pessoa = pessoaFisica
                    };

                    repTelefone.Salvar(telefoneResidencial);
                    pessoaFisica.Telefones.Add(telefoneResidencial);
                }
                else
                {
                    telefoneResidencial.TelefoneNum = (telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Emergencial) != null) ? telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Emergencial).TelefoneNum : "";
                    repTelefone.Atualizar(telefoneResidencial);
                }
            }

            if (propModificadas.Any(a => a == "TelefoneNumCelular"))
            {
                Telefone telefoneResidencial = pessoaFisica.Telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Celular);

                if (telefoneResidencial == null || (telefoneResidencial.Codigo == 0 && !string.IsNullOrEmpty(telefoneResidencial.TelefoneNum)))
                {
                    telefoneResidencial = new Telefone
                    {
                        DataRegistro = DateTime.Now,
                        Ativo = true,
                        TelefoneNum = (telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Celular) != null) ? telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Celular).TelefoneNum : "",
                        TipoTelefone = TipoTelefone.Celular,
                        Pessoa = pessoaFisica
                    };

                    repTelefone.Salvar(telefoneResidencial);
                    pessoaFisica.Telefones.Add(telefoneResidencial);
                }
                else
                {
                    telefoneResidencial.TelefoneNum = (telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Celular) != null) ? telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Celular).TelefoneNum : "";
                    repTelefone.Atualizar(telefoneResidencial);
                }
            }

            return pessoaFisica;
        }

        /// <summary>
        /// Cria um objeto contendo informações relacionadas ao conteudo gerenciado por
        /// ColaboradorController.
        /// </summary>
        ///
        /// <param name="colaborador">
        /// O(a) colaborador.
        /// </param>
        /// <param name="alocacao">
        /// O(a) alocacao.
        /// </param>
        ///
        /// <returns>
        /// Um(a) object.
        /// </returns>
        private object montaObj(Colaborador colaborador, AlocacaoColaborador alocacao = null)
        {
            Endereco endereco = colaborador.PessoaFisica.Endereco;

            Telefone telefoneRes = colaborador.PessoaFisica.Telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Residencial);
            Telefone telefoneEmer = colaborador.PessoaFisica.Telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Emergencial);
            Telefone telefoneCel = colaborador.PessoaFisica.Telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Celular);

            var colaboradorModelView = new ColaboradorModelView();
            colaboradorModelView.CodigoColaborador = colaborador.Codigo;
            colaboradorModelView.Codigo = colaborador.Codigo;

            colaboradorModelView.CodigoPessoa = colaborador.PessoaFisica.Codigo;
            colaboradorModelView.CPF = colaborador.PessoaFisica.CPF != null ? colaborador.PessoaFisica.CPFFormatado() : null;
            colaboradorModelView.Passaporte = colaborador.PessoaFisica.Passaporte;
            colaboradorModelView.Nome = colaborador.PessoaFisica.Nome;
            colaboradorModelView.Apelido = colaborador.PessoaFisica.Apelido;
            colaboradorModelView.Email = colaborador.PessoaFisica.Email;
            colaboradorModelView.DataNascimento = colaborador.PessoaFisica.DataNascimento;
            colaboradorModelView.DataVisto = colaborador.PessoaFisica.DataVisto;
            colaboradorModelView.DataValidadeVisto = colaborador.PessoaFisica.DataValidadeVisto;
            colaboradorModelView.Sexo = colaborador.PessoaFisica.Sexo.ObterDescricaoEnum();
            colaboradorModelView.NomeMae = colaborador.PessoaFisica.NomeMae;
            colaboradorModelView.NomePai = colaborador.PessoaFisica.NomePai;
            colaboradorModelView.RNE = colaborador.PessoaFisica.RNE;
            colaboradorModelView.DataExpedicaoRNE = colaborador.PessoaFisica.DataExpedicaoRNE;
            colaboradorModelView.EstadoCivil = colaborador.PessoaFisica.EstadoCivil;
            colaboradorModelView.Escolaridade = colaborador.PessoaFisica.Escolaridade;
            colaboradorModelView.TipoSanguineo = colaborador.PessoaFisica.TipoSanguineo;
            colaboradorModelView.Area = colaborador.Area != null ? colaborador.Area.Codigo : (int?)null;
            colaboradorModelView.DataValidade = (colaborador.DataValidade != null ? colaborador.DataValidade : null);
            colaboradorModelView.IdUsuario = colaborador.IdUsuario != null ? colaborador.IdUsuario.Trim() : null;
            colaboradorModelView.SetorCusto = colaborador.SetorCusto != null ? colaborador.SetorCusto.Codigo : (int?)null;

            colaboradorModelView.IdMotivo = colaborador.IdMotivo;
            colaboradorModelView.DescMotivo = colaborador.DescMotivo;
            colaboradorModelView.Processado = colaborador?.Processado;
            colaboradorModelView.TipoAcesso = (Int32)(colaborador.TipoAcesso);

            if (colaborador.PessoaFisica.Foto != null)
                colaboradorModelView.Img = Convert.ToBase64String(colaborador.PessoaFisica.Foto);

            colaboradorModelView.Nacionalidade = colaborador.PessoaFisica.Nacionalidade;
            colaboradorModelView.NaturalidadeUF = colaborador.PessoaFisica.Naturalidade != null && colaborador.PessoaFisica.Naturalidade.Estado != null ? colaborador.PessoaFisica.Naturalidade.Estado.Codigo : (int?)null;
            colaboradorModelView.Naturalidade = colaborador.PessoaFisica.Naturalidade != null ? colaborador.PessoaFisica.Naturalidade.Codigo : (int?)null;
            colaboradorModelView.Area = colaborador.Area != null ? colaborador.Area.Codigo : (int?)null;
            //RG
            colaboradorModelView.RG = colaborador.PessoaFisica.RG != null ? colaborador.PessoaFisica.RG : null;
            colaboradorModelView.RGOrgaoEmissor = colaborador.PessoaFisica.RGOrgaoEmissor != null ? colaborador.PessoaFisica.RGOrgaoEmissor : null;
            colaboradorModelView.RGOrgaoEmissorUF = colaborador.PessoaFisica.RGOrgaoEmissorUF != null ? colaborador.PessoaFisica.RGOrgaoEmissorUF.Codigo : (int?)null;
            colaboradorModelView.RGDataEmissao = colaborador.PessoaFisica.RGDataEmissao != null ? colaborador.PessoaFisica.RGDataEmissao : (DateTime?)null;

            //Titulo de eleitor
            colaboradorModelView.TituloEleitor = colaborador.PessoaFisica.TituloEleitor != null ? colaborador.PessoaFisica.TituloEleitor : (int?)null;
            colaboradorModelView.TituloEleitorSecao = colaborador.PessoaFisica.TituloEleitorSecao != null ? colaborador.PessoaFisica.TituloEleitorSecao : (int?)null;
            colaboradorModelView.TituloEleitorZona = colaborador.PessoaFisica.TituloEleitorZona != null ? colaborador.PessoaFisica.TituloEleitorZona : (int?)null;
            colaboradorModelView.TituloEleitorCidade = colaborador.PessoaFisica.TituloEleitorCidade != null ? colaborador.PessoaFisica.TituloEleitorCidade.Codigo : (int?)null;
            colaboradorModelView.TituloEleitorEstado = (colaborador.PessoaFisica.TituloEleitorCidade != null && colaborador.PessoaFisica.TituloEleitorCidade.Estado != null) ? colaborador.PessoaFisica.TituloEleitorCidade.Estado.Codigo : (int?)null;

            //CTPS
            colaboradorModelView.CTPS = colaborador.PessoaFisica.CTPS != null ? colaborador.PessoaFisica.CTPS : (int?)null;
            colaboradorModelView.CTPSData = colaborador.PessoaFisica.CTPSData != null ? colaborador.PessoaFisica.CTPSData : (DateTime?)null;
            colaboradorModelView.CTPSSerie = colaborador.PessoaFisica.CTPSSerie != null ? colaborador.PessoaFisica.CTPSSerie : null;
            colaboradorModelView.CTPSEstado = colaborador.PessoaFisica.CTPSEstado != null ? colaborador.PessoaFisica.CTPSEstado.Codigo : (int?)null;

            //CNH
            colaboradorModelView.CNH = colaborador.PessoaFisica.CNH != null ? colaborador.PessoaFisica.CNH : null;
            colaboradorModelView.CNHCategoria = colaborador.PessoaFisica.CNHCategoria != null ? colaborador.PessoaFisica.CNHCategoria : null;
            colaboradorModelView.CNHDataValidade = colaborador.PessoaFisica.CNHDataValidade != null ? colaborador.PessoaFisica.CNHDataValidade : (DateTime?)null;

            //Certificado de Reservista
            colaboradorModelView.CertificadoReservista = colaborador.PessoaFisica.CertificadoReservista != null ? colaborador.PessoaFisica.CertificadoReservista : null;
            colaboradorModelView.CertificadoReservistaCat = colaborador.PessoaFisica.CertificadoReservistaCat != null ? colaborador.PessoaFisica.CertificadoReservistaCat : null;

            //Telefones
            colaboradorModelView.TelefoneNumResidencial = telefoneRes != null ? telefoneRes.TelefoneNum : null;
            colaboradorModelView.TelefoneNumEmergencial = telefoneEmer != null ? telefoneEmer.TelefoneNum : null;
            colaboradorModelView.TelefoneNumCelular = telefoneCel != null ? telefoneCel.TelefoneNum : null;


            if (endereco != null)
            {
                if (!string.IsNullOrEmpty(endereco.Complemento) && endereco.Logradouro == null)
                {
                    colaboradorModelView.Endereco = new EnderecoMovelView()
                    {
                        CodigoEndereco = endereco.Codigo,
                        Complemento = endereco.Complemento
                    };
                }
                else
                {
                    colaboradorModelView.Endereco = new EnderecoMovelView()
                    {
                        CodigoEndereco = endereco.Codigo,
                        CodigoLog = endereco.Logradouro.Codigo,
                        Pais = endereco.Logradouro.Bairro.Cidade.Estado.Pais.Nome,
                        Estado = endereco.Logradouro.Bairro.Cidade.Estado.Nome,
                        Cidade = endereco.Logradouro.Bairro.Cidade.Nome,
                        Bairro = endereco.Logradouro.Bairro.Nome,
                        Logradouro = endereco.Logradouro.Nome,
                        Numero = endereco.Numero,
                        Complemento = endereco.Complemento,
                        CEP = endereco.Logradouro.CEP //,
                        //TipoLogradouro = endereco.Logradouro.TipoLogradouro.Nome
                    };
                }
            }
            //dados de colaborador
            //colaboradorModelView.AntiPassBack = colaborador.AntiPassBack;
            colaboradorModelView.ConselhoProfissional = colaborador.ConselhoProfissional;
            colaboradorModelView.RegistroProfissional = colaborador.RegistroProfissional;
            colaboradorModelView.PessoaJuridica = colaborador.PessoaJuridica;
            colaboradorModelView.AcessoPorto = colaborador.AcessoPorto;
            colaboradorModelView.Observacao = colaborador.Observacao;
            colaboradorModelView.HoristaMensalista = colaborador.HoristaMensalista;
            colaboradorModelView.Salario = colaborador.Salario;
            colaboradorModelView.StatusPapel = colaborador.ObterLogPapel().Status.ObterDescricaoEnum();
            colaboradorModelView.DataStatusPapel = colaborador.ObterLogPapel().DataInicio;
            //colaboradorModelView.colaborador.Empresa.Codigo,
            colaboradorModelView.Empresa = colaborador.Empresa.Codigo;
            colaboradorModelView.EmpresaDesc = colaborador.Empresa.Nome;
            //Alterado por Antonio Cabral 15/04/20, atendimento chamado-16828-tx1362323-item-289-mudança-webservice-novidadespessoasap
            colaboradorModelView.SubContratista = colaborador.SubContratista;
            colaboradorModelView.Cnpj_SubContratista = colaborador.Cnpj_SubContratista;
            //----------------------------------------------------------------------------------------------------
            //colaboradorModelView.MaoDeObra = colaborador.MaoDeObra.ObterDescricaoEnum();
            colaboradorModelView.MaoDeObra = (Int32)(colaborador.MaoDeObra);
            colaboradorModelView.GrupoPessoa = (Int32)(colaborador.GrupoPessoa);

            if (colaborador.Cargo != null)
            {
                colaboradorModelView.Cargo = colaborador.Cargo.Codigo;
                colaboradorModelView.CargoDesc = colaborador.Cargo.Nome;
            }
            colaboradorModelView.DataAdmissao = colaborador.DataAdmissao;
            colaboradorModelView.ModeloCracha = colaborador.ModeloCracha != null ? colaborador.ModeloCracha.Nome : null;
            colaboradorModelView.PIS = colaborador.PIS;

            if (colaborador.EmpregadoSAP != null)
            {
                colaboradorModelView.EmpregadoSAP = colaborador.EmpregadoSAP.Codigo;
                colaboradorModelView.GestorPonto = colaborador.EmpregadoSAP.GestorPonto != null ? colaborador.EmpregadoSAP.GestorPonto.Codigo : (long?)null;
                colaboradorModelView.SuperiorImediato = colaborador.EmpregadoSAP.SuperiorImediato != null ? colaborador.EmpregadoSAP.SuperiorImediato.Codigo : (long?)null;
            }

            GrupoTrabalhoColab grupoTrabalho = colaborador.ObterGrupoTrabalho();

            if (grupoTrabalho != null)
            {
                colaboradorModelView.CodigoGrupoTrabalho = grupoTrabalho.GrupoTrabalho.Codigo;
                colaboradorModelView.GrupoTrabalho = grupoTrabalho.GrupoTrabalho.Nome;
            }

            if (alocacao != null)
            {
                //exibe sempre o segundo nível do pedido de compra, mesmo que o colaborador esteja alocado no quinto, por exemplo.
                //solicitado pelo Vanderson em 20-10-2014
                PedidoCompra pedidoCompra = alocacao.PedidoCompra;

                if (pedidoCompra.PedidoCompraPai != null)
                {
                    //while (pedidoCompra.PedidoCompraPai.PedidoCompraPai != null)
                    //    pedidoCompra = pedidoCompra.PedidoCompraPai;
                    pedidoCompra = alocacao.ObterContratada();
                    colaboradorModelView.PedidoCompra = string.Format("{0} - {1}", pedidoCompra.Numero, pedidoCompra.Nome);

                    if (alocacao.SetorCusto != null)
                        colaboradorModelView.SetorCustoPed = string.Format("{0} - {1}", alocacao.SetorCusto.Numero, alocacao.SetorCusto.Nome);

                    colaboradorModelView.Projeto = alocacao.PedidoCompra.Projeto.Nome;
                    colaboradorModelView.StatusAlocacao = alocacao.Ativo ? "Ativo" : "Inativo";
                    colaboradorModelView.Contratada = pedidoCompra.Empresa.Apelido;
                }
            }



            //documentos
            colaboradorModelView.Documentos =
                colaborador.Documentos.Select(d => new DocumentoPapelModelView()
                {
                    CodigoDocumento = d.Documento.Codigo,
                    Codigo = d.Documento.TipoDocumento.Codigo,
                    Sigla = d.Documento.TipoDocumento.Sigla,
                    DataValidade = d.Documento.DataValidade,
                    Descricao = d.Documento.Descricao,
                    //DiasValidade = d.Documento.TipoDocumento.DiasValidade,
                    Status = obterDesc(d.Documento.Status).ToString(),
                    QtdMidias = d.Documento.Midias.Count(m => m.Ativo),
                    Midias = d.Documento.Midias.Select(dm => new MidiaModelView()
                    {
                        CodigoMidia = dm.Midia.Codigo,
                        Arquivo = string.Empty,
                        DataRegistro = dm.Midia.DataRegistro,
                        Extensao = dm.Midia.Extensao,
                        MIME = dm.Midia.MimeType,
                        NomeDisco = dm.Midia.NomeArquivo,
                        NomeOriginal = string.Empty,
                        URL = dm.Midia.Url
                    }).ToList()

                }).ToList();

            //treinamentos
            colaboradorModelView.Treinamentos =
                colaborador.Treinamentos.Select(d => new TreinamentoPessoaModelView()
                {
                    Codigo = d.Codigo,
                    DataRealizacao = d.DataRealizacao,
                    DataValidade = d.DataValidade,
                    Nome = d.Treinamento.Nome,
                    CodigoTreinamento = d.Treinamento.Codigo
                }).ToList();

            //asos
            colaboradorModelView.Asos = colaborador.ASOs.Select(a => new ASO()
            {
                Codigo = a.Codigo,
                DataRealizacao = a.DataRealizacao,
                DataVencimento = a.DataVencimento
            }).ToList();

            colaboradorModelView.ExcecoesAcesso = colaborador.ExcecaoAcesso.Select(a => new ExcecaoAcessoModelView()
            {
                Codigo = a.Codigo,
                CodigoCriterio = a.CriterioBloqueio,
                DescricaoBloqueio = obterDesc(a.CriterioBloqueio),
                Ativo = a.Ativo

            }).Where(t => t.Ativo).ToList();

            colaboradorModelView.HistoricoEmpresa = ObterHistoricoEmpresa(colaborador.PessoaFisica.CPF);

            return colaboradorModelView;
        }

        /// <summary>
        /// Obter historico empresa.
        /// </summary>
        ///
        /// <param name="cpf">
        /// O(a) cpf.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;Empresa&gt;
        /// </returns>
        private List<Empresa> ObterHistoricoEmpresa(string cpf)
        {
            IRepositorioColaborador rep = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);
            return rep.ObterTodos().Where(p => p.PessoaFisica.CPF == cpf).Select(p => new Empresa()
            {
                Codigo = p.Empresa.Codigo,
                CNPJ = p.Empresa.CNPJFormatado(),
                Nome = p.Empresa.Nome
            }).ToList();
        }

        /// <summary>
        /// Obter description.
        /// </summary>
        ///
        /// <param name="criterio">
        /// O(a) criterio.
        /// </param>
        ///
        /// <returns>
        /// Um(a) string.
        /// </returns>
        private string obterDesc(CriterioBloqueio criterio)
        {
            return EnumExtensoes.ObterDescricaoEnum(criterio);
        }

        /// <summary>
        /// Obter description.
        /// </summary>
        ///
        /// <param name="statusDocumento">
        /// O(a) status documento.
        /// </param>
        ///
        /// <returns>
        /// Um(a) string.
        /// </returns>
        private object obterDesc(StatusDocumento statusDocumento)
        {
            return EnumExtensoes.ObterDescricaoEnum(statusDocumento);
        }

        /// <summary>
        /// Retorna todos os objetos do tipo Colaborador.
        /// </summary>
        ///
        /// <param name="apenasValidos">
        /// true to apenas validos.
        /// </param>
        ///
        /// <returns>
        /// Detalhes do tipo de objeto ligado ao controller no formato JSON.
        /// </returns>
        public ActionResult ObterTodos(bool apenasValidos = false)
        {
            IRepositorioColaborador rep = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);

            IQueryable<Colaborador> colaboradores = rep.ObterTodos();

            return Json(colaboradores.Select(u => new
            {
                u.Codigo,
                CodigoPessoa = u.PessoaFisica.Codigo,
                CPF = u.PessoaFisica.CPF != null ? u.PessoaFisica.CPFFormatado() : null,
                u.PessoaFisica.Passaporte,
                u.PessoaFisica.Nome,
                Empresa = u.Empresa.Nome,
                EmpresaDesc = u.Empresa.Nome,
                SubContratista = u.SubContratista != null ? u.SubContratista : "",
                Cargo = u.Cargo.Nome,
                StatusPapel = u.ObterLogPapel() != null ? obterDesc(u.ObterLogPapel().Status) : obterDesc(StatusPapelLog.Ativo)
            })
                                .ToArray());
        }

        /// <summary>
        /// Pesquisar.
        /// </summary>
        ///
        /// <param name="parameters">
        /// Options for controlling the operation.
        /// </param>
        /// <param name="status">
        /// O(a) status.
        /// </param>
        /// <param name="cpf">
        /// O(a) cpf.
        /// </param>
        /// <param name="empresa">
        /// O(a) empresa.
        /// </param>
        /// <param name="nome">
        /// O(a) nome.
        /// </param>
        /// <param name="cargo">
        /// O(a) cargo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>        
        public ActionResult Pesquisar(StoreRequestParameters parameters = null, StatusPapelLog? status = null, string cpf = null, string empresa = null, string nome = null, int cargo = 0, string id = null, string passaporte = null, MaoDeObra? maodeobra = null, string area = null, long? empregadoSap = null, string crachaativo = null)
        {
            try
            {
                IQueryable<Colaborador> colaboradores = FiltrarColaborador(status, cpf, empresa, nome, cargo, id, passaporte, maodeobra, area, empregadoSap, crachaativo);
                int total = colaboradores.Count();
                List<ColaboradorModelView> data = montaObjColab(colaboradores, parameters);
                return new StoreResult(data, total);
            }
            catch (Exception e)
            {
                int a = 10;
            }
            return new StoreResult(null, 0);

        }

        /// <summary>
        /// Filtrar colaborador.
        /// </summary>
        ///
        /// <param name="status">
        /// O(a) status.
        /// </param>
        /// <param name="cpf">
        /// O(a) cpf.
        /// </param>
        /// <param name="empresa">
        /// O(a) empresa.
        /// </param>
        /// <param name="nome">
        /// O(a) nome.
        /// </param>
        /// <param name="cargo">
        /// O(a) cargo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) IQueryable&lt;Colaborador&gt;
        /// </returns>        
        private IQueryable<Colaborador> FiltrarColaborador(StatusPapelLog? status = null, string cpf = null, string empresa = null, string nome = null, int cargo = 0, string id = null, string passaporte = null, MaoDeObra? maodeobra = null, string area = null, long? empregadoSap = null, string crachaativo = null)
        {
            IRepositorioColaborador repColaborador = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);
            return repColaborador.FiltrarColaborador(status, cpf, empresa, nome, cargo, id, passaporte, maodeobra, area, empregadoSap, crachaativo).AsQueryable();
        }

        /// <summary>
        /// Monta object colab.
        /// </summary>
        ///
        /// <param name="colaboradores">
        /// O(a) colaboradores.
        /// </param>
        /// <param name="parameters">
        /// Options for controlling the operation.
        /// </param>
        /// <param name="ignoreParameters">
        /// true to ignore parameters.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;ColaboradorModelView&gt;
        /// </returns>
        private List<ColaboradorModelView> montaObjColab(IQueryable<Colaborador> colaboradores, StoreRequestParameters parameters = null, bool ignoreParameters = false)
        {
            if (!ignoreParameters)
            {
                if (parameters == null)
                    parameters = new StoreRequestParameters();
                colaboradores = colaboradores.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 25);
            }

            return colaboradores.Select(s => new ColaboradorModelView()
            {
                Codigo = s.Codigo,
                CodigoCracha = obterCodigoCracha(s.PapelLogs.OrderByDescending(x => x.DataRegistro).FirstOrDefault()),
                CodigoPessoa = s.PessoaFisica.Codigo,
                Nome = s.PessoaFisica.Nome,
                Passaporte = s.PessoaFisica.Passaporte,
                CPF = s.PessoaFisica.CPFFormatado(),
                EmpresaDesc = (s.Empresa != null) ? s.Empresa.Apelido : "",
                SubContratista = s.SubContratista != null ? s.SubContratista : "",
                CargoDesc = (s.Cargo != null) ? s.Cargo.Nome : "",
                StatusPapel = obterDesc(s.ObterLogPapel().Status),
                MaoDeObra = (Int32)s.MaoDeObra,
                IdUsuario = s.ObterLogPapel().Papel.IdUsuario
            }).ToList();
        }

        /// <summary>
        /// Obter description.
        /// </summary>
        ///
        /// <param name="cracha">
        /// O(a) status log.
        /// </param>
        ///
        /// <returns>
        /// Um(a) string with cracha number.
        /// </returns>
        /// 

        private string obterCodigoCracha(PapelLog cracha)
        {
            IRepositorioCracha repCracha = Fabrica.Instancia.ObterRepositorio<IRepositorioCracha>(UnidadeTrabalho);
            var listCracha = repCracha.ObterTodos().Where(x => x.Papel.Codigo == cracha.Papel.Codigo);
            if (listCracha.Count() > 0)
                return listCracha.OrderByDescending(x => x.DataRegistro).First().Numero.ToString();
            else return null;
        }

        /// <summary>
        /// Obter description.
        /// </summary>
        ///
        /// <param name="statusLog">
        /// O(a) status log.
        /// </param>
        ///
        /// <returns>
        /// Um(a) string.
        /// </returns>
        /// 
        private string obterDesc(StatusPapelLog statusLog)
        {
            return EnumExtensoes.ObterDescricaoEnum(statusLog);
        }

        /// <summary>
        /// Obter horista mensalista.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ObterHoristaMensalista()
        {
            Dictionary<int, string> dic = new Dictionary<int, string>();

            foreach (HoristaMensalista horistaMensalista in Enum.GetValues(typeof(HoristaMensalista)))
            {
                dic.Add(Convert.ToInt32(horistaMensalista), horistaMensalista.ObterDescricaoEnum());
            }

            return Json(dic.Select(d => new
            {
                Codigo = d.Key,
                Nome = d.Value
            }).ToArray());
        }

        /// <summary>
        /// Selecionar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Selecionar(int codigo)
        {
            IRepositorioColaborador rep = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);
            IRepositorioAlocacaoColaborador repAlocacao = Fabrica.Instancia.ObterRepositorio<IRepositorioAlocacaoColaborador>(UnidadeTrabalho);

            Colaborador col = rep.ObterPorId(codigo);
            AlocacaoColaborador alocacao = repAlocacao.ObterAlocacao(codigo, true);

            return Json(montaObj(col, alocacao));
        }

        /// <summary>
        /// Obter lista colaborador.
        /// </summary>
        ///
        /// <param name="cpf">
        /// O(a) cpf.
        /// </param>
        /// <param name="empresa">
        /// O(a) empresa.
        /// </param>
        /// <param name="nome">
        /// O(a) nome.
        /// </param>
        /// <param name="consideraAlocados">
        /// true to considera alocados.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ObterListaColaborador(string cpf, string empresa, string nome, bool consideraAlocados = false)
        {
            IRepositorioColaborador rep = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);

            Colaborador colaborador = new Colaborador();

            cpf = cpf.Replace(".", "").Replace("-", "");
            colaborador.PessoaFisica = new PessoaFisica { CPF = cpf, Nome = nome };
            colaborador.Empresa = new Empresa { Codigo = empresa };

            List<Colaborador> colaboradores = rep.ObterListaColaborador(colaborador, consideraAlocados).ToList();

            return Json(colaboradores.Select(u => new
            {
                Codigo = u.Codigo,
                CPF = u.PessoaFisica.CPF != null ? u.PessoaFisica.CPFFormatado() : null,
                Passaporte = u.PessoaFisica.Passaporte,
                Nome = u.PessoaFisica.Nome,
                Empresa = u.Empresa.Nome,
                SubContratista = u.SubContratista != null ? u.SubContratista : "",
                Cargo = u.Cargo != null ? u.Cargo.Nome : "",
                Status = u.PapelLogs.OrderByDescending(pl => pl.DataRegistro).FirstOrDefault().Status.ObterDescricaoEnum()
            }).ToList());
        }

        [AllowAnonymous]
        public ActionResult ObterColaboradoresAtivos(string cpf = null, string empresa = null, string nome = null)
        {
            var criterio = "";

            if (!string.IsNullOrEmpty(cpf))
                criterio = " and CPF = '" + cpf.Replace(".", "").Replace("-", "") + "'";

            if (!string.IsNullOrEmpty(empresa))
                criterio = " and CodigoEmpresa = '" + empresa + "'";

            if (!string.IsNullOrEmpty(nome))
                criterio += " and NomePessoa like '%" + nome + "%'";


            var query = this.UnidadeTrabalho.ExecuteSql<Models.ColaboradorGenericoModelView>("SELECT * FROM VW_COLABORADOR WHERE Status = 'Ativo' " + criterio, new Dictionary<string, object>()).AsQueryable();

            return Json(query.OrderBy(x => x.NomePessoa).Select(x => new
            {
                Codigo = x.Codigo,
                Nome = x.NomePessoa,
                Empresa = x.NomeEmpresa,
                CPF = string.IsNullOrEmpty(x.CPF) ? "-" : Convert.ToUInt64(x.CPF).ToString(@"000\.000\.000\-00")
            }).ToList());
        }

        /// <summary>
        /// Lista mão de obra.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ListaMaoDeObra()
        {
            Dictionary<int, string> dic = new Dictionary<int, string>();

            foreach (int item in Enum.GetValues(typeof(MaoDeObra)))
            {
                dic.Add(item, ((MaoDeObra)item).ObterDescricaoEnum());
            }
            return Json(dic.Select(d => new
            {
                Codigo = d.Key,
                Descricao = d.Value
            }).ToList());

        }

        /// <summary>
        /// Lista mão de obra.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ListaGrupoPessoa()
        {
            Dictionary<int, string> dic = new Dictionary<int, string>();

            foreach (int item in Enum.GetValues(typeof(GrupoPessoa)))
            {
                dic.Add(item, ((GrupoPessoa)item).ObterDescricaoEnum());
            }
            return Json(dic.Select(d => new
            {
                Codigo = d.Key,
                Descricao = d.Value
            }).ToList());

        }


        /// <summary>
        /// Lista mão de obra.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ListaTipoAcesso()
        {
            Dictionary<int, string> dic = new Dictionary<int, string>();

            foreach (int item in Enum.GetValues(typeof(TipoAcesso)))
            {
                dic.Add(item, ((TipoAcesso)item).ObterDescricaoEnum());
            }
            return Json(dic.Select(d => new
            {
                Codigo = d.Key,
                Descricao = d.Value
            }).ToList());

        }


        #region Webcam / Jcrop

        /// <summary>
        /// Edit.
        /// </summary>
        ///
        /// <param name="x">
        /// The x coordinate.
        /// </param>
        /// <param name="y">
        /// The y coordinate.
        /// </param>
        /// <param name="w">
        /// The width.
        /// </param>
        /// <param name="h">
        /// The height.
        /// </param>
        /// <param name="imageUrl">
        /// URL of the image.
        /// </param>
        /// <param name="modeloCracha">
        /// true to modelo cracha.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [HttpPost]
        public ActionResult Edit(int x, int y, int w, int h, string imageUrl, bool modeloCracha = false)
        {
            WebImage image = new WebImage(Convert.FromBase64String(imageUrl));

            var height = image.Height;
            var width = image.Width;

            image.Crop((int)x, (int)y, (int)(height - w), (int)(width - h));
            //return Json(Convert.ToBase64String(image.GetBytes()), "text/html");
            return Json(Convert.ToBase64String(image.GetBytes()), "text/html");

            //int newWidth = image.Width;
            //int newHeight = image.Height;
            //Bitmap newImage = new Bitmap(newWidth, newHeight);
            //using (Graphics gr = Graphics.FromImage(newImage))
            //{
            //    gr.SmoothingMode = SmoothingMode.HighQuality;
            //    gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
            //    gr.PixelOffsetMode = PixelOffsetMode.HighQuality;
            //    gr.DrawImage(new Bitmap(imageUrl), new Rectangle(0, 0, newWidth, newHeight));
            //}
            //
            //System.IO.MemoryStream stream = new System.IO.MemoryStream();
            //newImage.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
            //return Json(Convert.ToBase64String(stream.ToArray()), "text/html");
        }

        /// <summary>
        /// Upload.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="modeloCracha">
        /// true to modelo cracha.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Upload(bool modeloCracha = false)
        {
            if (Request.ContentLength > 1048576)
                throw new CoreException("Não é permitido importar imagens com mais de 1MB.");

            HttpPostedFileBase postedFile = Request.Files[0];
            if (postedFile == null)
                throw new CoreException("Não foi possível recuperar o arquivo.");

            WebImage image = new WebImage(postedFile.InputStream);
            int w = image.Width;
            int h = image.Height;
            if ((w < 128) || (h < 128))
                throw new CoreException("Tamanho da imagem inferior a 128px.");

            const int maxW = 720;
            const int maxH = 720;
            if ((w > maxW) || (h > maxH))
            {
                double ratio = 0;
                if (w > maxW)
                {
                    ratio = maxW / (w * 1.0);
                    h = Convert.ToInt32((h * 1.0) * ratio);
                    w = Convert.ToInt32((w * 1.0) * ratio);
                }
                if (h > maxH)
                {
                    ratio = maxH / (h * 1.0);
                    w = Convert.ToInt32((w * 1.0) * ratio);
                    h = Convert.ToInt32((h * 1.0) * ratio);
                }
                image.Resize(w, h);
            }

            return Json(Convert.ToBase64String(image.GetBytes()), "text/html");
        }

        /// <summary>
        /// Carregar imagem.
        /// </summary>
        ///
        /// <param name="imagem">
        /// O(a) imagem.
        /// </param>
        ///
        /// <returns>
        /// Um(a) byte[].
        /// </returns>
        private byte[] CarregarImagem(string imagem)
        {
            byte[] imagemBytes = null;
            string caminhoCompletoImagem = imagem;

            int posicao = caminhoCompletoImagem.IndexOf("/Content");
            string caminho = caminhoCompletoImagem.Substring(posicao);
            imagemBytes = System.IO.File.ReadAllBytes(Server.MapPath("~" + caminho));
            return imagemBytes;
        }

        //private string GravarTemporario(byte[] foto)
        //{
        //    if (foto != null)
        //    {
        //        Random randNum = new Random();
        //        int number = randNum.Next(DateTime.Now.Millisecond);

        //        string nomeArquivo = number + ".jpeg";

        //        byte[] b = foto;

        //        using (FileStream fs = new FileStream(Server.MapPath("~/Content/img/webcam/") + nomeArquivo, FileMode.Create))
        //            fs.Write(b, 0, b.Length);

        //        return "/Content/img/webcam/" + nomeArquivo;
        //    }
        //    return "/Content/img/avatar.png";
        //}

        #endregion
    }

    /// <summary>
    /// Valores que representam TipoConsulta.
    /// </summary>
    public enum TipoConsulta
    {
        /// <summary>
        /// An enum constant representing the alocado option.
        /// </summary>
        Alocado,
        /// <summary>
        /// An enum constant representing the desalocado option.
        /// </summary>
        Desalocado,
        /// <summary>
        /// An enum constant representing the nao alocado option.
        /// </summary>
        NaoAlocado
    }
}
