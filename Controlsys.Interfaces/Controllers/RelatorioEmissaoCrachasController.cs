﻿using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Pessoas;
using Controlsys.Dominio.Relatorio;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Parametros;
using Controlsys.Repositorios.Empresas;
using Controlsys.Repositorios.Pessoas;
using Ext.Net;
using Ext.Net.MVC;
using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Globalsys.Relatorios;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class RelatorioEmissaoCrachasController : Controller, IControllerBase
    {

        public IUnidadeTrabalho UnidadeTrabalho { get; set; }
        
        public RelatorioEmissaoCrachasController(IUnidadeTrabalho unidade) { UnidadeTrabalho = unidade; }
        
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Exportar(List<string> selectedFields, string tipoRelatorio, DateTime dataInicio, DateTime dataFim, string cnpjEmpresa = null, string nomeEmpresa = null, string cnpjContratada = null, string nomeContratada = null)
        {
            var list = FiltrarCrachasEmpresa(dataInicio, dataFim, cnpjEmpresa, nomeEmpresa, cnpjContratada, nomeContratada);
            var crachas = montaObj(list, null, true);

            ReportConfig reportConfig = ReportHelper.RenderReport<RelatorioEmissaoCrachas>(selectedFields, tipoRelatorio, crachas);
        
            string fileName = string.Empty;
        
            switch (tipoRelatorio)
            {
                case "PDF":
                    fileName = "download.pdf";
                    break;
                case "EXCELOPENXML":
                    fileName = "download.xlsx";
                    break;
                case "WORDOPENXML":
                    fileName = "download.docx";
                    break;
            }
        
            return File(reportConfig.Bytes, reportConfig.MimeType, fileName);
        }
        
        public ActionResult Pesquisar(StoreRequestParameters parameters, DateTime dataInicio, DateTime dataFim, string cnpjEmpresa = null, string nomeEmpresa = null, string cnpjContratada = null, string nomeContratada = null)
        {
            IQueryable<RelatorioEmissaoCrachas> regs = FiltrarCrachasEmpresa(dataInicio, dataFim, cnpjEmpresa, nomeEmpresa, cnpjContratada, nomeContratada);
        
            int total = regs.Count();
            List<RelatorioEmissaoCrachas> data = montaObj(regs, parameters);
        
            return new StoreResult(data, total);
        }

        private IQueryable<RelatorioEmissaoCrachas> FiltrarCrachasEmpresa(DateTime dataInicio, DateTime dataFim, string cnpjEmpresa = null, string nomeEmpresa = null, string cnpjContratada = null, string nomeContratada = null)
        {
            var sDtaInicio = string.Format("CAST(CAST({0} AS varchar) + '-' + CAST({1} AS varchar) + '-' + CAST({2} AS varchar)" +
                                           " + ' ' + CAST({3} AS varchar) + ':' + CAST({4} AS varchar) AS DATETIME)",
                                           dataInicio.Year, dataInicio.Month, dataInicio.Day, dataInicio.Hour, dataInicio.Minute);

            dataFim = dataFim.AddDays(1);
            var sDtaFim = string.Format("CAST(CAST({0} AS varchar) + '-' + CAST({1} AS varchar) + '-' + CAST({2} AS varchar)" +
                                           " + ' ' + CAST({3} AS varchar) + ':' + CAST({4} AS varchar) AS DATETIME)",
                                           dataFim.Year, dataFim.Month, dataFim.Day, 0, 0);

            string query = string.Format(@"SELECT * FROM fn_get_qtd_crachas_emitidos({0},{1})", sDtaInicio, sDtaFim);
            
            var parameters = new Dictionary<string, object>();
            //
            //parameters.Add("dtInicio", dataInicio);
            //parameters.Add("dtFim", dataFim);
            //
            //string query = @"SELECT * FROM fn_get_qtd_crachas_emitidos(:dtInicio,:dtFim)";
            //
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var registros = unidTrabalho.ExecuteSql<RelatorioEmissaoCrachas>(query, parameters).AsQueryable();

            if (!string.IsNullOrEmpty(cnpjEmpresa))
            {
                cnpjEmpresa = cnpjEmpresa.Replace(".", "").Replace("/", "").Replace("-", "");
                registros = registros.Where(r => r.CNPJEmpresa == cnpjEmpresa);
            }

            if (!string.IsNullOrEmpty(nomeEmpresa))
            {
                registros = registros.Where(r=>r.NomeEmpresa.Contains(nomeEmpresa));
            }

            if (!string.IsNullOrEmpty(cnpjContratada))
            {
                cnpjContratada = cnpjContratada.Replace(".", "").Replace("/", "").Replace("-", "");
                registros = registros.Where(r => r.CNPJEmpresa == cnpjContratada);
            }

            if (!string.IsNullOrEmpty(nomeContratada))
            {
                registros = registros.Where(r => r.NomeEmpresa.Contains(nomeContratada));
            }

            return registros;

            #region [Disabled]
            //IRepositorioCracha rep = Fabrica.Instancia.ObterRepositorio<IRepositorioCracha>(UnidadeTrabalho);
            //IRepositorioAlocacaoColaborador repAlocacao = Fabrica.Instancia.ObterRepositorio<IRepositorioAlocacaoColaborador>(UnidadeTrabalho);
            //IRepositorioPrestadorServico repPrest = Fabrica.Instancia.ObterRepositorio<IRepositorioPrestadorServico>(UnidadeTrabalho);
            //IRepositorioColaborador repColaborador = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);
            //IRepositorioVisitante repVisitante = Fabrica.Instancia.ObterRepositorio<IRepositorioVisitante>(UnidadeTrabalho);
            //
            //IQueryable<Cracha> listCracha = rep.ObterTodos();
            //
            //if (dataInicio != null)
            //    listCracha = listCracha.Where(c => c.DataRegistro >= dataInicio);
            //
            //if (dataFim != null)
            //    listCracha = listCracha.Where(c => c.DataRegistro < dataFim);
            //
            //
            //if (!string.IsNullOrEmpty(cnpjEmpresa))
            //{
            //    cnpjEmpresa = cnpjContratada.Replace(".", "").Replace("/", "").Replace("-", "");
            //    List<int> codColab = repColaborador.ObterTodos().Where(p => p.Empresa.CNPJ == cnpjEmpresa).Select(c => c.Codigo).ToList();
            //    List<int> codPres = repPrest.ObterTodos().Where(p => p.Empresa.CNPJ == cnpjEmpresa).Select(c => c.Codigo).ToList();
            //
            //    codColab = codColab.Union(codPres).ToList();
            //
            //    listCracha = listCracha.Where(pf => codColab.Contains(pf.Papel.Codigo));
            //}
            //
            //if (!string.IsNullOrEmpty(nomeEmpresa))
            //{
            //    List<int> cods;
            //    List<int> codColab = repColaborador.ObterTodos().Where(p => p.Empresa.Nome.Contains(nomeEmpresa)).Select(c => c.Codigo).ToList();
            //    List<int> codPres = repPrest.ObterTodos().Where(p => p.Empresa.Nome.Contains(nomeEmpresa)).Select(c => c.Codigo).ToList();
            //    List<int> codVis = repVisitante.ObterTodos().Where(p => p.Empresa.Contains(nomeEmpresa)).Select(c => c.Codigo).ToList();
            //
            //    cods = codColab.Union(codPres).Union(codVis).ToList();
            //
            //    listCracha = listCracha.Where(pf => cods.Contains(pf.Papel.Codigo));
            //}
            //
            //IQueryable<AlocacaoColaborador> aloc = repAlocacao.ObterTodos();
            //if (!string.IsNullOrEmpty(cnpjContratada))
            //{
            //    cnpjContratada = cnpjContratada.Replace(".", "").Replace("/", "").Replace("-", "");
            //
            //    var alocs = aloc.Where(a => a.ObterContratada().Empresa.CNPJ == cnpjContratada).Select(a=> a.Papel.Codigo);
            //    listCracha = listCracha.Where(c => alocs.Contains(c.Papel.Codigo));
            //}
            //
            //if (!string.IsNullOrEmpty(nomeEmpresa))
            //{
            //    nomeEmpresa = nomeEmpresa.ToUpper();
            //
            //    var papeisAlocados = aloc.Where(a => a.ObterContratada().Empresa.Nome == nomeEmpresa).Select(a => a.Papel.Codigo);
            //    listCracha = listCracha.Where(c => papeisAlocados.Contains(c.Papel.Codigo));
            //}
            //
            //var listaCracha = listCracha.Select(c => new RelatorioCrachaEmpresaAuxModelView
            //{
            //    Cracha = c,
            //    Alocacao = aloc.Where(a => a.Papel.Codigo == c.Papel.Codigo).OrderByDescending(ac => ac.DataRegistro).FirstOrDefault()
            //});
            //
            //var regs = listaCracha.GroupBy(l=> l.Cracha)
            //
            //return listaCracha; 
            #endregion
        }

        private List<RelatorioEmissaoCrachas> montaObj(IQueryable<RelatorioEmissaoCrachas> regs, StoreRequestParameters parameters = null, bool ignoreParameters = false)
        {
            if (!ignoreParameters)
            {
                if (parameters == null)
                    parameters = new StoreRequestParameters();
                regs = regs.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 25);
            }

            return regs.ToList();
        }
        

    }
}
