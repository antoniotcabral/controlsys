﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Pessoas;
using Controlsys.Infra;
using Controlsys.Repositorios.Acesso;
using Ext.Net;
using Ext.Net.MVC;
using Globalsys;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar sincroniza acessoes.
    /// </summary>
    [ExtendController]
    public class SincronizaAcessoController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /SincronizaAcesso/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.SincronizaAcessoController.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public SincronizaAcessoController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        /// <summary>
        /// Retorna a página de sincronizaAcesso.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index()
        {
            return View();
        }

        /*
        /// <summary>
        /// Retorna todos os objetos do tipo SincronizaAcesso.
        /// </summary>
        ///
        /// <returns>
        /// Detalhes do tipo de objeto ligado ao controller no formato JSON.
        /// </returns>
        public ActionResult ObterTodos()
        {
            IRepositorioSincronizaPessoaAcesso repSincroniza = Fabrica.Instancia.ObterRepositorio<IRepositorioSincronizaPessoaAcesso>(UnidadeTrabalho);

            var regs = repSincroniza.ObterTodos().OrderByDescending(sa => sa.DataRegistro);
            var lista = regs.Select(sa => new
            {
                Codigo = sa.Codigo,
                IP = sa.Controladora.IP,
                Papel = sa.Cracha.Papel is Colaborador
                    ? "Colaborador"
                    : sa.Cracha.Papel is PrestadorServico
                        ? "Prestador de serviço"
                        : sa.Cracha.Papel is Visitante
                            ? "Visitante" : null,
                Cracha = sa.Cracha.Numero,
                Pessoa = sa.Cracha.Papel.PessoaFisica.Nome,
                DataRegistro = sa.DataRegistro.ToString("dd/MM/yyyy HH:mm:ss"),
                TipoSincronizacao = obterDescricaoEnum(sa.TipoSincronizacao),
                DataSincronizacao = sa.DataSincronizacao.HasValue ? sa.DataSincronizacao.Value.ToString("dd/MM/yyyy HH:mm:ss") : null,
                Porcentagem = sa.Porcentagem + "%",
                DataInicio = sa.DataInicio.HasValue ? sa.DataInicio.Value.ToString("dd/MM/yyyy HH:mm") : null,
                DataInicioDate = sa.DataInicio,
                Status = obterDescricaoEnum(sa.Status),
                sa.Observacoes
            });

            return Json(lista.ToList());
        }
        */

        public ActionResult ObterTodos(StoreRequestParameters parameters, string dsCracha, TipoPapel? dsPapel, string dsPessoa, string dsIp)
        {
            IQueryable<SincronizaPessoaAcesso> regs = filtrarAcessos(dsCracha, dsPapel, dsPessoa, dsIp);

            int total = regs.Count();
            var data = montaObjs(regs, parameters);

            return new StoreResult(data, total);
        }

        private IQueryable<SincronizaPessoaAcesso> filtrarAcessos(string dsCracha, TipoPapel? dsPapel, string dsPessoa, string dsIp)
        {
            IRepositorioSincronizaPessoaAcesso repSincroniza =
                Fabrica.Instancia.ObterRepositorio<IRepositorioSincronizaPessoaAcesso>(UnidadeTrabalho);

            IQueryable<SincronizaPessoaAcesso> regs = repSincroniza.ObterTodos().OrderByDescending(sa => sa.DataRegistro);

            if (!string.IsNullOrEmpty(dsCracha))
                regs = regs.Where(sa => sa.Cracha.Numero == dsCracha);

            if (!string.IsNullOrEmpty(dsIp))
                regs = regs.Where(sa => sa.Controladora.IP == dsIp);

            switch (dsPapel)
            {
                case TipoPapel.Colaborador:
                    regs = regs.Where(sa => sa.Cracha.Papel is Colaborador);
                    break;
                case TipoPapel.PrestadorServico:
                    regs = regs.Where(sa => sa.Cracha.Papel is PrestadorServico);
                    break;
                case TipoPapel.Visitante:
                    regs = regs.Where(sa => sa.Cracha.Papel is Visitante);
                    break;
            }

            if (!string.IsNullOrEmpty(dsPessoa))
                regs = regs.Where(sa => sa.Cracha.Papel.PessoaFisica.Nome.ToLower().Contains(dsPessoa.ToLower()));

            regs = regs.OrderByDescending(sa => sa.DataRegistro);

            return regs;
        }

        /// <summary>
        /// Retransmitir.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        /// <param name="dataInicio">
        /// O(a) data inicio.
        /// </param>
        /// <param name="horaInicio">
        /// O(a) hora inicio.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Retransmitir(int codigo, DateTime? dataInicio, TimeSpan? horaInicio)
        {
            IRepositorioSincronizaPessoaAcesso repSinc = Fabrica.Instancia.ObterRepositorio<IRepositorioSincronizaPessoaAcesso>(UnidadeTrabalho);

            if (dataInicio.HasValue)
            {
                if (horaInicio.HasValue)
                    dataInicio = dataInicio.Value.Add(horaInicio.Value);
            }

            SincronizaPessoaAcesso sincroniza = repSinc.Retransmitir(codigo, dataInicio);
            return Json(montaObj(sincroniza));
        }

        public ActionResult ExecutarSP()
        {
            IRepositorioSincronizaPessoaAcesso repEscala = Fabrica.Instancia.ObterRepositorio<IRepositorioSincronizaPessoaAcesso>(UnidadeTrabalho);
            UnidadeTrabalho.ExecuteProcedure("SP_ALTERARSTATUS_SINCRONIZACAO");
            return null;
        }

        /// <summary>
        /// Obter descricao enum.
        /// </summary>
        ///
        /// <param name="tipoSincronizacaoPessoa">
        /// O(a) tipo sincronizacao pessoa.
        /// </param>
        ///
        /// <returns>
        /// Um(a) string.
        /// </returns>
        private string obterDescricaoEnum(Dominio.Acesso.TipoSincronizacaoPessoa tipoSincronizacaoPessoa)
        {
            return tipoSincronizacaoPessoa.ObterDescricaoEnum();
        }

        /// <summary>
        /// Obter descricao enum.
        /// </summary>
        ///
        /// <param name="status">
        /// O(a) status.
        /// </param>
        ///
        /// <returns>
        /// Um(a) string.
        /// </returns>
        private string obterDescricaoEnum(StatusSincronizacao status)
        {
            return status.ObterDescricaoEnum();
        }

        /// <summary>
        /// Cria um objeto contendo informações relacionadas ao conteudo gerenciado por
        /// SincronizaAcessoController.
        /// </summary>
        ///
        /// <param name="sa">
        /// O(a) sa.
        /// </param>
        ///
        /// <returns>
        /// Um(a) object.
        /// </returns>
        private object montaObj(SincronizaPessoaAcesso sa)
        {
            return new
            {
                Codigo = sa.Codigo,
                IP = sa.Controladora.IP,
                Papel = sa.Cracha != null ? sa.Cracha.Papel is Colaborador
                                            ? "Colaborador"
                                            : sa.Cracha.Papel is PrestadorServico
                                                ? "Prestador de serviço"
                                                : sa.Cracha.Papel is Visitante
                                                    ? "Visitante" : null : null,
                Cracha = sa.Cracha != null ? sa.Cracha.Numero : null,
                Pessoa = sa.Cracha != null ? sa.Cracha.Papel.PessoaFisica.Nome : null,
                DataRegistro = sa.DataRegistro.ToString("dd/MM/yyyy HH:mm:ss"),
                TipoSincronizacao = obterDescricaoEnum(sa.TipoSincronizacao),
                DataSincronizacao = sa.DataSincronizacao.HasValue ? sa.DataSincronizacao.Value.ToString("dd/MM/yyyy HH:mm:ss") : null,
                Porcentagem = sa.Porcentagem + "%",
                DataInicio = sa.DataInicio.HasValue ? sa.DataInicio.Value.ToString("dd/MM/yyyy HH:mm") : null,
                DataInicioDate = sa.DataInicio,
                Status = obterDescricaoEnum(sa.Status),
                sa.Observacoes
            };
        }


        /// <summary>
        /// Cria um lista de objetos contendo informações relacionadas ao conteudo gerenciado por SincronizaAcessoController
        /// </summary>
        /// <param name="regs">Registros de SincronizaPessoaAcesso</param>
        /// <param name="parameters">Parametros de filtro</param>
        /// <param name="ignoreParameters">Ignorar pametros de filtro</param>
        /// <returns></returns>
        private object montaObjs(IQueryable<SincronizaPessoaAcesso> regs, StoreRequestParameters parameters = null, bool ignoreParameters = false)
        {
            if (!ignoreParameters)
            {
                if (parameters == null)
                    parameters = new StoreRequestParameters();
                regs = regs.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 25);
            }

            //return new List<object> {
                return regs.Select(sa => new 
                {
                    Codigo = sa.Codigo,
                    IP = sa.Controladora.IP,
                    Papel = sa.Cracha.Papel is Colaborador
                        ? "Colaborador"
                        : sa.Cracha.Papel is PrestadorServico
                            ? "Prestador de serviço"
                            : sa.Cracha.Papel is Visitante
                                ? "Visitante"
                                : null,
                    Cracha = sa.Cracha.Numero,
                    Pessoa = sa.Cracha.Papel.PessoaFisica.Nome,
                    DataRegistro = sa.DataRegistro.ToString("dd/MM/yyyy HH:mm:ss"),
                    TipoSincronizacao = obterDescricaoEnum(sa.TipoSincronizacao),
                    DataSincronizacao =
                        sa.DataSincronizacao.HasValue ? sa.DataSincronizacao.Value.ToString("dd/MM/yyyy HH:mm:ss") : null,
                    Porcentagem = sa.Porcentagem + "%",
                    DataInicio = sa.DataInicio.HasValue ? sa.DataInicio.Value.ToString("dd/MM/yyyy HH:mm") : null,
                    DataInicioDate = sa.DataInicio,
                    Status = obterDescricaoEnum(sa.Status),
                    sa.Observacoes
                }).ToList();
            //};
        }

    }
}
