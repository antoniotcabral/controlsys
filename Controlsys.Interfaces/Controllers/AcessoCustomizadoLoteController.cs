﻿using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Pessoas;
using Controlsys.Dominio.Seguranca;
using Controlsys.Infra;
using Controlsys.Repositorios.Acesso;
using Controlsys.Repositorios.Pessoas;
using Controlsys.Repositorios.Seguranca;
using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar acesso customizado lotes.
    /// </summary>
    [ExtendController]
    public class AcessoCustomizadoLoteController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /AcessoCustomizadoLote/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.AcessoCustomizadoLoteController.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public AcessoCustomizadoLoteController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        /// <summary>
        /// Retorna a página de acessoCustomizadoLote.
        /// </summary>
        ///
        /// <param name="selecionados">
        /// O(a) selecionados.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index(int[] selecionados)
        {
            return View(model: Newtonsoft.Json.JsonConvert.SerializeObject(selecionados));
        }

        /// <summary>
        /// Obter selecionados.
        /// </summary>
        ///
        /// <param name="selecionados">
        /// O(a) selecionados.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ObterSelecionados(int[] selecionados)
        {
            IRepositorioPapel repPapel = Fabrica.Instancia.ObterRepositorio<IRepositorioPapel>(UnidadeTrabalho);
            List<Papel> listaPapel = repPapel.ObterListaPapel(selecionados);

            return Json(listaPapel.Select(u => new
            {
                Codigo = u.Codigo,
                Nome = u.PessoaFisica.Nome,
                CPF = u.PessoaFisica.CPF != null ? u.PessoaFisica.CPFFormatado() : null,
                Passaporte = u.PessoaFisica.Passaporte != null ? u.PessoaFisica.Passaporte : null
            }).ToList());
        }

        /// <summary>
        /// Alterar em lote.
        /// </summary>
        ///
        /// <param name="listaPapel">
        /// O(a) lista papel.
        /// </param>
        /// <param name="papelLog">
        /// O(a) papel log.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult AlterarEmLote(int[] listaPapel, PapelLog papelLog)
        {
            IRepositorioPapel repPapel = Fabrica.Instancia.ObterRepositorio<IRepositorioPapel>(UnidadeTrabalho);
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            IRepositorioSincronizaPessoaAcesso repSincronizaPessoaAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioSincronizaPessoaAcesso>(UnidadeTrabalho);

            for (int i = 0; i < listaPapel.Count(); i++)
            {
                //PapelLog papelLogA = new PapelLog();

                Papel papelA = repPapel.ObterPorId(listaPapel[i]);
                PapelLog papelLogA = papelA.ObterLogPapel();

                papelLog.Papel = papelA;
                papelLog.DataRegistro = DateTime.Now;

                repPapel.ValidarPapelLog(papelLogA, papelLog);
                repPapel.SalvarPapelLog(papelLog);

                DateTime? dataInicio = null;

                if (papelLog.DataInicio != DateTime.Today)
                    dataInicio = papelLog.DataInicio;

                if (papelLog.Status != StatusPapelLog.Ativo)
                {
                    Usuario usuario = repUsuario.ObterPorPessoa(papelLog.Papel.PessoaFisica.Codigo);
                    if (usuario != null)
                    {
                        usuario.Inativar();
                        repUsuario.Atualizar(usuario);
                    }

                    repSincronizaPessoaAcesso.Sincronizar(papelA, TipoSincronizacaoPessoa.Suspender, dataInicio);
                }
                else repSincronizaPessoaAcesso.Sincronizar(papelA, TipoSincronizacaoPessoa.Conceder, dataInicio);

                if (papelLog.Status == StatusPapelLog.BaixaDefinitiva)
                    repPapel.DarBaixaDefinitiva(papelLog);
            }

            return null;
        }
    }
}
