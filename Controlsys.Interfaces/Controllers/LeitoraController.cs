﻿using Controlsys.Dominio.Acesso;
using Controlsys.Infra;
using Controlsys.Repositorios.Acesso;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Controlsys.Interfaces.Models;
using Controlsys.Dominio.Seguranca;
using Controlsys.Repositorios.Seguranca;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar leitoras.
    /// </summary>
    [ExtendController]
    public class LeitoraController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /Leitora/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.LeitoraController.
        /// </summary>
        ///
        /// <param name="unidade">
        /// O(a) unidade.
        /// </param>
        public LeitoraController(IUnidadeTrabalho unidade)
        {
            UnidadeTrabalho = unidade;
        }

        /// <summary>
        /// Inclui um novo leitora.
        /// </summary>
        ///
        /// <param name="leitora">
        /// O(a) leitora.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Incluir(Leitora leitora)
        {
            IRepositorioLeitora repLeitora = Fabrica.Instancia.ObterRepositorio<IRepositorioLeitora>(UnidadeTrabalho);
            IRepositorioControladora repControladora = Fabrica.Instancia.ObterRepositorio<IRepositorioControladora>(UnidadeTrabalho);
            leitora.DataRegistro = DateTime.Now;
            leitora.Ativar();
            leitora.Controladora = repControladora.ObterPorId(leitora.Controladora.Codigo);
            repLeitora.Validar(leitora, EstadoObjeto.Novo);
            repLeitora.Salvar(leitora);
            return Json(montaObj(leitora));
        }

        /// <summary>
        /// Altera um registro de leitora.
        /// </summary>
        ///
        /// <param name="leitora">
        /// O(a) leitora.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Alterar(Leitora leitora)
        {
            IRepositorioLeitora repLeitora = Fabrica.Instancia.ObterRepositorio<IRepositorioLeitora>(UnidadeTrabalho);
            Leitora LeitoraA = repLeitora.ObterPorId(leitora.Codigo);

            LeitoraA.IDLeitora = leitora.IDLeitora;
            //LeitoraA.TipoLeitora = leitora.TipoLeitora;
            LeitoraA.Direcao = leitora.Direcao;
            LeitoraA.Local = leitora.Local;
            LeitoraA.Senha = leitora.Senha;
            LeitoraA.Descricao = leitora.Descricao;
            LeitoraA.EmiteBaixaCracha = leitora.EmiteBaixaCracha;
            LeitoraA.ExigeSupervisao = leitora.ExigeSupervisao;
            LeitoraA.Fabricante = leitora.Fabricante;
            LeitoraA.Ordem = leitora.Ordem;            

            repLeitora.Validar(LeitoraA, EstadoObjeto.Alterado);
            repLeitora.Atualizar(LeitoraA);
            return Json(montaObj(LeitoraA));
        }

        /// <summary>
        /// Cria um objeto contendo informações relacionadas ao conteudo gerenciado por
        /// LeitoraController.
        /// </summary>
        ///
        /// <param name="leitora">
        /// O(a) leitora.
        /// </param>
        ///
        /// <returns>
        /// Um(a) object.
        /// </returns>
        private object montaObj(Leitora leitora)
        {
            return new LeitoraModelView
            {
                Codigo = leitora.Codigo,
                IDLeitora = leitora.IDLeitora,
                Local = leitora.Local,
                Senha = leitora.Senha,
                Descricao = leitora.Descricao,
                //TipoLeitora = obterDesc(leitora.TipoLeitora),
                DirecaoDesc = obterDesc(leitora.Direcao),
                Ativo = leitora.Ativo,
                IP = leitora.Controladora.IP,
                TipoControladora = obterDesc(leitora.Controladora.TipoControladora)
            };
        }

        /// <summary>
        /// Ativar inativar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult AtivarInativar(int codigo)
        {
            IRepositorioLeitora repLeitora = Fabrica.Instancia.ObterRepositorio<IRepositorioLeitora>(UnidadeTrabalho);
            Leitora leitora = repLeitora.ObterPorId(codigo);
            if (leitora.Ativo)
            {
                leitora.Inativar();
            }
            else
            {
                repLeitora.Validar(leitora, EstadoObjeto.Ativado);
                leitora.Ativar();
            }
            repLeitora.Atualizar(leitora);
            return Json(montaObj(leitora));

        }

        /// <summary>
        /// Selecionar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Selecionar(int codigo)
        {
            IRepositorioLeitora repSetor = Fabrica.Instancia.ObterRepositorio<IRepositorioLeitora>(UnidadeTrabalho);
            Leitora setor = repSetor.ObterPorId(codigo);
            return Json(setor);
        }

        /// <summary>
        /// Retorna todos os objetos do tipo Leitora.
        /// </summary>
        ///
        /// <param name="apenasAtivos">
        /// true to apenas ativos.
        /// </param>
        /// <param name="apenasLeitorasUsuario">
        /// true to apenas Leitoras Usuario.
        /// </param>
        ///
        /// <returns>
        /// Detalhes do tipo de objeto ligado ao controller no formato JSON.
        /// </returns>
        public ActionResult ObterTodos(bool apenasAtivos = false, bool apenasLeitorasUsuario = false)
        {
            IRepositorioLeitora repLeitora = Fabrica.Instancia.ObterRepositorio<IRepositorioLeitora>(UnidadeTrabalho);
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);

            IQueryable<Leitora> leitoras = null;
            if (apenasLeitorasUsuario)
            {
                Usuario usuario = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name);

                leitoras = usuario.GruposUsuarios
                                        .SelectMany(gu => gu.Grupo.GruposLeitoras.Select(gr => gr.GrupoLeitora))
                                        .SelectMany(gl => gl.Leitoras.Select(lgl => lgl.Leitora))
                                        .Distinct().AsQueryable();
            }
            else
                leitoras = repLeitora.ObterTodos();

            if (apenasAtivos)
                leitoras = leitoras.Where(l => l.Ativo && l.Controladora.Ativo);

            return Json(leitoras.OrderBy(l => l.Descricao).Select(s => montaObj(s)).ToList());
        }

        public ActionResult ObterTodosLocais()
        {
            IRepositorioLeitora repLeitora = Fabrica.Instancia.ObterRepositorio<IRepositorioLeitora>(UnidadeTrabalho);

            IQueryable<Leitora> leitoras = repLeitora.ObterTodos();

            var aux = leitoras.Select(s => s.Local).Distinct().ToList();

            return Json(aux.Select(t => new LeitoraModelView { Local = t}));

            
        }

        /// <summary>
        /// Obter description.
        /// </summary>
        ///
        /// <param name="tipoLeitora">
        /// O(a) tipo leitora.
        /// </param>
        ///
        /// <returns>
        /// Um(a) string.
        /// </returns>
        private string obterDesc(TipoLeitora tipoLeitora)
        {
            return EnumExtensoes.ObterDescricaoEnum(tipoLeitora);
        }

        /// <summary>
        /// Obter description.
        /// </summary>
        ///
        /// <param name="tipoControladora">
        /// O(a) tipo controladora.
        /// </param>
        ///
        /// <returns>
        /// Um(a) string.
        /// </returns>
        private string obterDesc(TipoControladora tipoControladora)
        {
            return EnumExtensoes.ObterDescricaoEnum(tipoControladora);
        }

        /// <summary>
        /// Obter description.
        /// </summary>
        ///
        /// <param name="direcao">
        /// O(a) direcao.
        /// </param>
        ///
        /// <returns>
        /// Um(a) string.
        /// </returns>
        private string obterDesc(DirecaoLeitora direcao)
        {
            return EnumExtensoes.ObterDescricaoEnum(direcao);
        }

        /// <summary>
        /// Lista tipo leitora.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ListaTipoLeitora()
        {
            Dictionary<int, string> dic = new Dictionary<int, string>();

            foreach (int item in Enum.GetValues(typeof(TipoLeitora)))
            {
                dic.Add(item, ((TipoLeitora)item).ObterDescricaoEnum());
            }
            return Json(dic.Select(d => new
            {
                Codigo = d.Key,
                Descricao = d.Value
            }).ToList());

        }

        /// <summary>
        /// Lista direcao.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ListaDirecao()
        {
            Dictionary<int, string> dic = new Dictionary<int, string>();

            foreach (int item in Enum.GetValues(typeof(DirecaoLeitora)))
            {
                dic.Add(item, ((DirecaoLeitora)item).ObterDescricaoEnum());
            }
            return Json(dic.Select(d => new
            {
                Codigo = d.Key,
                Descricao = d.Value
            }).ToList());
        }

        /// <summary>
        /// Lista fabricante.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ListaFabricante()
        {
            Dictionary<int, string> dic = new Dictionary<int, string>();

            foreach (Fabricante item in Enum.GetValues(typeof(Fabricante)))            
                dic.Add((int)item, item.ObterDescricaoEnum());            

            return Json(dic.Select(d => new { Codigo = d.Key, Descricao = d.Value }).ToArray());
        }
    }
}
