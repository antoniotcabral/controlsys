﻿using Controlsys.Dominio.Seguranca;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorios.Seguranca;
using Ext.Net.MVC;
using Globalsys;
using Globalsys.Exceptions;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class RelatorioFullTimeEquivalentePrestadorController : Controller
    {
        //
        // GET: /RelatorioFullTimeEquivalente/
        /// <summary>
        /// Gets/Sets valor para UnidadeTrabalho.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="unidadeTrabalho"></param>
        public RelatorioFullTimeEquivalentePrestadorController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;            
        }

        public ActionResult Index()
        {            
            #region [Filtro de usuário]
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            Usuario usuarioLogado = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name, somenteAtivos: true);
            #endregion

            #region [Filtro Administrador]
            var grupos = usuarioLogado.GruposUsuarios.Select(g => g.Grupo);
            var acoes = grupos.SelectMany(g => g.Acoes);            
            var permissaoExportar = (acoes.Where(a => a.Url == "/RelatorioFullTimeEquivalentePrestador/Exportar")).Any();            
            #endregion

            if (permissaoExportar)
            {
                ViewBag.permissaoExportar = 1;
            }
            else
            {
                ViewBag.permissaoExportar = 2;
            }            

            return View();
        }

        public ActionResult Pesquisar(string empresa, DateTime? dataInicio, string dia = "")
        {
            if (dia != null && dia != "")
            {
                String[] dtTemp = dataInicio.ToString().Split('/');
                string dtTemp2 = dia + "/" + dtTemp[1] + "/" + dtTemp[2];
                try
                {
                    dataInicio = Convert.ToDateTime(dtTemp2);
                }
                catch (FormatException)
                {
                    throw new CoreException("Data/formato inválido!");
                }

            }

            var dados = FiltrarRelatorioFTE(empresa, dataInicio, dia).ToList();
            int total = dados.Count();
            List<RelatorioFullTimeEquivalenteModelView> data = montaObj(dados);

            return new StoreResult(data, total);
        }

        private List<RelatorioFullTimeEquivalenteModelView> montaObj(List<RelatorioFullTimeEquivalenteModelView> dados)
        {
            List<RelatorioFullTimeEquivalenteModelView> list = new List<RelatorioFullTimeEquivalenteModelView>();

            foreach (var item in dados)
            {

                RelatorioFullTimeEquivalenteModelView report = new RelatorioFullTimeEquivalenteModelView();
                report.CodEmpresa = item.CodEmpresa;
                report.CodPapel = item.CodPapel;
                report.CodPedido = item.CodPedido;
                report.CodSetor = item.CodSetor;
                report.NomeContratada = item.NomeContratada;
                report.NomeSetor = item.NomeSetor;
                report.QtdColaboradoresCapex = item.QtdColaboradoresCapex > 0 ? item.QtdColaboradoresCapex : (int?)0.00;
                report.QtdColaboradoresOpex = item.QtdColaboradoresOpex > 0 ? item.QtdColaboradoresOpex : (int?)0.00;
                report.TipoCapex = item.TipoCapex;
                report.TipoOpex = item.TipoOpex;
                report.ValorFteCapex = item.ValorFteCapex;
                report.ValorFteOpex = item.ValorFteOpex;
                report.MaoDeObra = item.MaoDeObra;
                list.Add(report);
            }
            return list;
        }

        private string validarDouble(string valorConverter)
        {
            Decimal valor = 0;
            if (valorConverter.Length > 0)
            {
                valor = Convert.ToDecimal(valorConverter);
            }
            return Convert.ToString(valor);
        }
        //[AllowAnonymous]
        public ActionResult Exportar(string tipoRelatorio, string empresa, DateTime dataInicio, string dia = "")
        {
            if (dia != null && dia != "")
            {
                String[] dtTemp = dataInicio.ToString().Split('/');
                string dtTemp2 = dia + "/" + dtTemp[1] + "/" + dtTemp[2];
                dataInicio = DateTime.Parse(dtTemp2);
            }

            LocalReport relatorio = new LocalReport();

            relatorio.ReportPath = Server.MapPath("~/Reports/RelatorioFTEPrestador.rdlc");

            List<RelatorioFullTimeEquivalenteModelView> dados = montaObj(FiltrarRelatorioFTE(empresa, dataInicio, dia).ToList());
            if (dados.Count > 0)
            {
                relatorio.DataSources.Add(new ReportDataSource("DTORelatorioFTE", dados));
                relatorio.SetParameters(new ReportParameter("DataInicio", dataInicio.ToString()));
                tipoRelatorio = "EXCELOPENXML";
                string mimeType;
                string fileName = string.Empty;
                switch (tipoRelatorio)
                {
                    case "EXCELOPENXML":
                        fileName = "Relatorio_Controlsys_FTE_Prestador.xlsx";
                        break;
                }
                string encoding;
                string fileNameExtension;

                Warning[] warnings;
                string[] streams;
                byte[] bytes;

                bytes = relatorio.Render(
                tipoRelatorio,
                null,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);
                return File(bytes, mimeType, fileName);
            }
            else return null;
        }

        //[AllowAnonymous]
        //public ActionResult Exportar(List<string> selectedFields, string tipoRelatorio, string empresa, DateTime dataInicio, string dia = "")
        //{
        //    if (dia != null && dia != "")
        //    {
        //        String[] dtTemp = dataInicio.ToString().Split('/');
        //        string dtTemp2 = dia + "/" + dtTemp[1] + "/" + dtTemp[2];
        //        dataInicio = DateTime.Parse(dtTemp2);
        //    }


        //    List<RelatorioFullTimeEquivalenteModelView> dados = montaObj(FiltrarRelatorioFTE(empresa, dataInicio, dia).ToList());

        //    ReportConfig reportConfig = ReportHelper.RenderReport<RelatorioFullTimeEquivalenteModelView>(selectedFields, tipoRelatorio, dados);

        //    string fileName = string.Empty;

        //    switch (tipoRelatorio)
        //    {
        //        case "PDF":
        //            fileName = "Relatorio_Controlsys_FTE.pdf";
        //            break;
        //        case "Excel":
        //            fileName = "Relatorio_Controlsys_FTE.xls";
        //            break;
        //        case "Word":
        //            fileName = "Relatorio_Controlsys_FTE.doc";
        //            break;
        //    }

        //    return File(reportConfig.Bytes, reportConfig.MimeType, fileName);
        //}

        private IEnumerable<RelatorioFullTimeEquivalenteModelView> FiltrarRelatorioFTE(string empresa, DateTime? dataInicio, string dia)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();

            parameters.Add("dtInicio", dataInicio);
            parameters.Add("dia", dia);
            parameters.Add("sempresa", empresa);

            var relHHT = UnidadeTrabalho.ExecuteSqlHHT<RelatorioFullTimeEquivalenteModelView>("EXEC SP_RELATORIO_FTE_PREST :dtInicio, :dia, :sempresa", parameters);

            return relHHT;
        }
        public void ExecutarCarga(DateTime? dataInicio)
        {
            Dictionary<string, object> dictionary = new Dictionary<string, object>();
            dictionary.Add("dtInicio_carga", dataInicio);

            UnidadeTrabalho.ExecuteProcedure("SP_REL_FTE_CARGA :dtInicio_carga", dictionary);
        }

        public void ExecutarCargaPrestador(DateTime? dataInicio)
        {
            Dictionary<string, object> dictionary = new Dictionary<string, object>();
            dictionary.Add("dtInicio_carga", dataInicio);
            
            UnidadeTrabalho.ExecuteProcedure("SP_REL_FTE_PREST_CARGA :dtInicio_carga", dictionary);
        }
    }
}
