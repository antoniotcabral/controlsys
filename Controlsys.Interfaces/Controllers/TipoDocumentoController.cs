﻿using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Controlsys.Infra;
using Controlsys.Repositorios.Parametros;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar tipo documentoes.
    /// </summary>
    [ExtendController]
    public class TipoDocumentoController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /TipoDocumento/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.TipoDocumentoController.
        /// </summary>
        ///
        /// <param name="unidade">
        /// O(a) unidade.
        /// </param>
        public TipoDocumentoController(IUnidadeTrabalho unidade)
        {
            UnidadeTrabalho = unidade;
        }

        /// <summary>
        /// Retorna a página de tipoDocumento.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Inclui um novo tipoDocumento.
        /// </summary>
        ///
        /// <param name="tipoDoc">
        /// O(a) tipo document.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Incluir(TipoDocumento tipoDoc)
        {
            IRepositorioTipoDocumento repTipoDoc = Fabrica.Instancia.ObterRepositorio<IRepositorioTipoDocumento>(UnidadeTrabalho);
            tipoDoc.DataRegistro = DateTime.Now;
            repTipoDoc.Validar(tipoDoc, EstadoObjeto.Novo);
            tipoDoc.Ativar();
            repTipoDoc.Salvar(tipoDoc);
            return Json(tipoDoc);
        }

        /// <summary>
        /// Altera um registro de tipoDocumento.
        /// </summary>
        ///
        /// <param name="tipoDoc">
        /// O(a) tipo document.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Alterar(TipoDocumento tipoDoc)
        {
            IRepositorioTipoDocumento repTipoDoc = Fabrica.Instancia.ObterRepositorio<IRepositorioTipoDocumento>(UnidadeTrabalho);
            TipoDocumento tipoDocumento = repTipoDoc.ObterPorId(tipoDoc.Codigo);
            tipoDocumento.Nome = tipoDoc.Nome;
            tipoDocumento.Bloqueio = tipoDoc.Bloqueio;
            //tipoDocumento.DiasValidade = tipoDoc.DiasValidade;
            tipoDocumento.Sigla = tipoDoc.Sigla;
            repTipoDoc.Validar(tipoDocumento, EstadoObjeto.Alterado);
            repTipoDoc.Atualizar(tipoDocumento);
            return Json(tipoDocumento);
        }

        /// <summary>
        /// Ativar inativar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult AtivarInativar(int codigo)
        {
            IRepositorioTipoDocumento repTipoDoc = Fabrica.Instancia.ObterRepositorio<IRepositorioTipoDocumento>(UnidadeTrabalho);
            TipoDocumento tipoDoc = repTipoDoc.ObterPorId(codigo);
            if (tipoDoc.Ativo)
            {
                tipoDoc.Inativar();
            }
            else
            {
                repTipoDoc.Validar(tipoDoc, EstadoObjeto.Ativado);
                tipoDoc.Ativar();

            }
            repTipoDoc.Atualizar(tipoDoc);
            return Json(tipoDoc);

        }

        /// <summary>
        /// Selecionar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Selecionar(int codigo)
        {
            IRepositorioTipoDocumento repTipoDoc = Fabrica.Instancia.ObterRepositorio<IRepositorioTipoDocumento>(UnidadeTrabalho);
            TipoDocumento tipoDoc = repTipoDoc.ObterPorId(codigo);
            return Json(tipoDoc);
        }

        /// <summary>
        /// Retorna todos os objetos do tipo TipoDocumento.
        /// </summary>
        ///
        /// <param name="apenasAtivos">
        /// true to apenas ativos.
        /// </param>
        ///
        /// <returns>
        /// Detalhes do tipo de objeto ligado ao controller no formato JSON.
        /// </returns>
        public ActionResult ObterTodos(bool apenasAtivos = false, TipoPapel[] tiposPapel = null, bool isEmpresa = false)
        {
            IRepositorioTipoDocumento repTipoDoc = Fabrica.Instancia.ObterRepositorio<IRepositorioTipoDocumento>(UnidadeTrabalho);
            IRepositorioChecklistDocumento repCheck = Fabrica.Instancia.ObterRepositorio<IRepositorioChecklistDocumento>(UnidadeTrabalho);
            IQueryable<TipoDocumento> tipos = null;
            if (isEmpresa)
            {
                tipos = repCheck.ObterTodos().ToList().Where(c => c.TipoCadastro == TipoCadastro.Empresa).SelectMany(c => c.TiposDocumento.Select(cd => cd.TipoDocumento)).Distinct().AsQueryable();
            }
            else if (tiposPapel == null || tiposPapel.Length == 0)
            {
                tipos = repTipoDoc.ObterTodos();
            }
            else
            {
                List<TipoCadastro> tiposCadastro = new List<TipoCadastro>();

                foreach (TipoPapel item in tiposPapel)
                {
                    if (item == TipoPapel.Colaborador)
                    {
                        tiposCadastro.Add(TipoCadastro.Colaborador);
                    }
                    else if (item == TipoPapel.PrestadorServico)
                    {
                        tiposCadastro.Add(TipoCadastro.PrestadorServicoEmergencial);
                        tiposCadastro.Add(TipoCadastro.PrestadorServicoTemp);
                    }
                    else if (item == TipoPapel.Visitante)
                    {
                        tiposCadastro.Add(TipoCadastro.Visitante);
                    }

                }

                tipos = repCheck.ObterTodos().ToList().Where(c => tiposCadastro.Contains(c.TipoCadastro)).SelectMany(c => c.TiposDocumento.Select(cd => cd.TipoDocumento)).Distinct().AsQueryable();
            }

            if (apenasAtivos)
            {
                tipos = tipos.Where(t => t.Ativo == true);
            }

            return Json(tipos.Select(t => new
                               {
                                   t.Codigo,
                                   t.Nome,
                                   t.Sigla,
                                   //t.DiasValidade,
                                   t.Ativo
                               })
                               .ToList().OrderBy(t => t.Sigla));
        }
    }
}
