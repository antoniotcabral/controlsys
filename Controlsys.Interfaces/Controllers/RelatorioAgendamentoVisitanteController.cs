﻿using Controlsys.Dominio.Pessoas;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Agendamentos;
using Ext.Net;
using Ext.Net.MVC;
using Globalsys;
using Globalsys.Relatorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Controlsys.Dominio.AgendamentosPessoa;
using Controlsys.Repositorios.Agendamentos;
using Controlsys.Repositorios.Seguranca;
using Controlsys.Dominio.Seguranca;
using Controlsys.Repositorios.Pessoas;
using Controlsys.Repositorio.Acesso;
using Controlsys.Dominio.Acesso;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class RelatorioAgendamentoVisitanteController : Controller, IControllerBase
    {
    
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        public RelatorioAgendamentoVisitanteController(IUnidadeTrabalho unidade) { UnidadeTrabalho = unidade; }

        public ActionResult Index()
        {
            string retorno = "false";
            //IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            //Usuario usuarioLogado = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name, somenteAtivos: true);
            //if (usuarioLogado != null)
            //{
            //    var grupos = usuarioLogado.GruposUsuarios.Select(g => g.Grupo);
            //    var acoes = grupos.SelectMany(g => g.Acoes);
            //    //retorno = acoes.Where(a => a.Url == "/RelatorioAgendamentoVisitante/Exportar").Any().ToString();
            //}
            Pesquisar();
            return View("Index", model: retorno);
            //return View();
        }

      [AllowAnonymous]
        public ActionResult Pesquisar(StoreRequestParameters parameters = null, string nome = null, string CPFPassaporte = null, string areaVisitada = null, string Solicitante = null, DateTime? dataInicio = null, TimeSpan? horaInicial = null, DateTime? dataFinal = null, TimeSpan? horaFinal = null)
        {
            IQueryable<VisitanteAgendamento> regs = filtrarRegistros(dataInicio, horaInicial, dataFinal, horaFinal, Solicitante, areaVisitada, CPFPassaporte, nome);

            int total = regs.Count();
            if (total == 0)
                return null;
            List<RelatorioAgendamentoVisitanteModelView> datas = montaObj(regs, parameters);

            return new StoreResult(datas, total);
        }

        [AllowAnonymous]
        public ActionResult Exportar(List<string> selectedFields, string tipoRelatorio, string nome, string CPFPassaporte, string areaVisitada, string Solicitante, DateTime? dataInicio, TimeSpan? horaInicial, DateTime? dataFinal, TimeSpan? horaFinal)
        {
            var list = filtrarRegistros(dataInicio, horaInicial, dataFinal, horaFinal, Solicitante, areaVisitada, CPFPassaporte, nome);
            var visitantes = montaObj(list, null, true);

            ReportConfig reportConfig = ReportHelper.RenderReport<RelatorioAgendamentoVisitanteModelView>(selectedFields, tipoRelatorio, visitantes);

            string fileName = string.Empty;

            switch (tipoRelatorio)
            {
                case "PDF":
                    fileName = "RelatorioAgendamentoVisitante.pdf";
                    break;
                case "Excel":
                    fileName = "RelatorioAgendamentoVisitante.xls";
                    break;
                case "Word":
                    fileName = "RelatorioAgendamentoVisitante.doc";
                    break;
            }

            return File(reportConfig.Bytes, reportConfig.MimeType, fileName);
        }


        private IQueryable<VisitanteAgendamento> filtrarRegistros(DateTime? dataInicial, TimeSpan? horaInicial, DateTime? dataFinal, TimeSpan? horaFinal, string solicitante, string areaVisitada, string CPFPassaporte, string nome)
        {

            IRepositorioAgendamentoVisitante repPessoaAgendamento = Fabrica.Instancia.ObterRepositorio<IRepositorioAgendamentoVisitante>(UnidadeTrabalho);
            
            IQueryable<VisitanteAgendamento> listPessoaAgendada = repPessoaAgendamento.ObterTodos();

            if (!string.IsNullOrEmpty(CPFPassaporte))
            {
                var auxCodPapeis = new List<int>();
                CPFPassaporte = CPFPassaporte.Replace(".", "").Replace("-", "");
                listPessoaAgendada = listPessoaAgendada.Where(x => x.PessoaAg.CPF.Equals(CPFPassaporte) || x.PessoaAg.Passaporte.Equals(CPFPassaporte));
            }

            if (!string.IsNullOrEmpty(nome))
            {
                nome = nome.ToUpper();
                listPessoaAgendada = listPessoaAgendada.Where(x => x.PessoaAg.Nome.ToUpper().Contains(nome));
            }

            if (!string.IsNullOrEmpty(solicitante))
            {
                listPessoaAgendada = listPessoaAgendada.Where(x => x.ResponsavelVisita.PessoaFisica.Nome.ToUpper().Equals(solicitante.ToUpper().Substring(0, solicitante.IndexOf('-'))));
            }

            if (horaInicial.ToString().Length > 0 && horaInicial.ToString() != "00:00:00")
            {
                listPessoaAgendada = listPessoaAgendada.Where(x => x.DataInicio.ToShortTimeString().CompareTo(horaInicial) > 0);
            }

            if (dataInicial.ToString().Length > 0)
            {

                listPessoaAgendada = listPessoaAgendada.Where(x => x.DataInicio >= dataInicial);
            }

            if (horaFinal.ToString().Length > 0 && horaFinal.ToString() != "23:59:59")
            {
                listPessoaAgendada = listPessoaAgendada.Where(x => x.DataFim.ToShortTimeString().CompareTo(horaFinal) > 0);
            }

            if (dataFinal.ToString().Length > 0)
            {
                listPessoaAgendada = listPessoaAgendada.Where(x => x.DataFim <= dataFinal);
            }

            return listPessoaAgendada.AsQueryable<VisitanteAgendamento>();
        }

        private List<RelatorioAgendamentoVisitanteModelView> montaObj(IQueryable<VisitanteAgendamento> regs, StoreRequestParameters parameters = null, bool ignoreParameters = false)
        {
            IRepositorioControleAcesso repAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioControleAcesso>(UnidadeTrabalho);

            if (!ignoreParameters)
            {
                if (parameters == null)
                    parameters = new StoreRequestParameters();
                regs = regs.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? (parameters.Start + parameters.Limit) : 25);
            }

            if (regs.ToList().Count() == 0)
                return null;
            else
                return regs.Select(item => new RelatorioAgendamentoVisitanteModelView()
                {

                    Codigo = item.Codigo,
                    Nome = item.PessoaAg.Nome,
                    CPFPassaporte = item.PessoaAg.CPF != null ? item.PessoaAg.CPF : item.PessoaAg.Passaporte,
                    CPFResponsavel = item.ResponsavelVisita.PessoaFisica.CPF != null ? item.ResponsavelVisita.PessoaFisica.CPF : item.ResponsavelVisita.PessoaFisica.Passaporte,
                    NomeResponsavel = item.ResponsavelVisita.PessoaFisica.Nome,
                    Empresa = item.Empresa,
                    Funcao = item.Funcao,
                    DataInicio = item.DataInicio,
                    DataFinal = item.DataFim,
                    NomeSolicitante = item.Solicitante.PessoaFisica.Nome,
                    AreaVisita = (item.Area != null ? item.Area.Nome : item.AcessoPorto == true ? "Acesso ao Porto" : null),
                    HorarioVisita = (item.DataInicio != null ? item.DataInicio.ToString().Substring(11) : ""),
                    Email = (item.PessoaAg.Email != null ? item.PessoaAg.Email : null),
                    TelEmergencial = (item.PessoaAg.TelefoneNumEmergencial != null ? item.PessoaAg.TelefoneNumEmergencial : null),
                    NomeGuerra = item.PessoaAg != null? item.PessoaAg.Apelido : item.PessoaAg.Nome,
                    Sexo = item.PessoaAg.Sexo.ToString(),
                    Celular = item.PessoaAg.TelefoneNumCelular,
                    AcessoPorto = (item.AcessoPorto == true ? 'S' : 'N'),
                    PassaporteResponsavel = (item.ResponsavelVisita.PessoaFisica.Passaporte != null ? item.ResponsavelVisita.PessoaFisica.Passaporte : null),
                    Motivo = item.Motivo,
                    CPFSolicitante = (item.Solicitante.PessoaFisica.CPF != null ? item.Solicitante.PessoaFisica.CPF : null),
                    PassaporteSolicitante = (item.Solicitante.PessoaFisica.Passaporte != null ? item.Solicitante.PessoaFisica.Passaporte : null),
                    RG = (item.PessoaAg.RG != null ? item.PessoaAg.RG : null),
                    OrgaoEmissor = (item.PessoaAg.RGOrgaoEmissor != null ? item.PessoaAg.RGOrgaoEmissor : null),
                    UF = (item.PessoaAg.RGOrgaoEmissorUF != null ? item.PessoaAg.RGOrgaoEmissorUF.ToString() : null),
                    DataEmissao = (item.PessoaAg.RGDataEmissao != null ? item.PessoaAg.RGDataEmissao.ToString() : null)
                }).ToList();
        }

    }
}
