﻿using Controlsys.Dominio.Enderecos;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorios.Enderecos;
using Ext.Net;
using Ext.Net.MVC;
using Globalsys;
using Globalsys.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar enderecoes.
    /// </summary>
    [ExtendController]
    public class EnderecoController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /Endereco/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.EnderecoController.
        /// </summary>
        ///
        /// <param name="unidade">
        /// O(a) unidade.
        /// </param>
        public EnderecoController(IUnidadeTrabalho unidade)
        {
            UnidadeTrabalho = unidade;
        }

        /// <summary>
        /// Endereco.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Endereco()
        {
            return PartialView();
        }

        /// <summary>
        /// Pesquisar.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="cep">
        /// O(a) cep.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Pesquisar(string cep)
        {
            IRepositorioEndereco repEnd = Fabrica.Instancia.ObterRepositorio<IRepositorioEndereco>(UnidadeTrabalho);
            cep = cep.Replace(".", "").Replace("-", "");
            Logradouro l = repEnd.Pesquisar(cep);

            if (l != null)
                return Json(new
                {
                    CodigoLog = l.Codigo,
                    Bairro = l.Bairro.Nome,
                    Cidade = l.Bairro.Cidade.Nome,
                    Estado = l.Bairro.Cidade.Estado.Nome,
                    Pais = l.Bairro.Cidade.Estado.Pais.Nome,
                    //TipoLogradouro = l.TipoLogradouro.Nome,
                    Logradouro = l.Nome
                });
            else
            {
                //throw new CoreException("CEP não existe. Favor cadastrar o endereço completo.");
                throw new CoreException("CEP não existe. Para cadastrar o novo endereço completo, entre em contato com a Meta.");
            }
        }

        /// <summary>
        /// Obter pais.
        /// </summary>
        ///
        /// <param name="query">
        /// O(a) query.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ObterPais(string query)
        {
            IRepositorioEndereco repEndereco = Fabrica.Instancia.ObterRepositorio<IRepositorioEndereco>(UnidadeTrabalho);

            return Json(repEndereco.ObterPaises(query).OrderBy(p => p.Nome).ToList());
        }

        /// <summary>
        /// Obter estados.
        /// </summary>
        ///
        /// <param name="query">
        /// O(a) query.
        /// </param>
        /// <param name="codigoPais">
        /// O(a) codigo pais.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ObterEstados(string query, int? codigoPais)
        {
            IRepositorioEndereco repEndereco = Fabrica.Instancia.ObterRepositorio<IRepositorioEndereco>(UnidadeTrabalho);

            var regs = repEndereco.ObterEstados(query, codigoPais);
            regs = regs.OrderBy(p => p.Nome);

            return Json(regs.ToList());
        }

        /// <summary>
        /// Obter cidades.
        /// </summary>
        ///
        /// <param name="query">
        /// O(a) query.
        /// </param>
        /// <param name="codigoEstado">
        /// O(a) codigo estado.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ObterCidades(string query, int? codigoEstado, bool apenasAtivos = false)
        {
            IRepositorioEndereco repEndereco = Fabrica.Instancia.ObterRepositorio<IRepositorioEndereco>(UnidadeTrabalho);
            List<Cidade> cidades = repEndereco.ObterCidades(query, codigoEstado, apenasAtivos).OrderBy(p => p.Nome).ToList();
            return Json(cidades.Select(p => new CidadeModelView
                {
                    Codigo = p.Codigo,
                    Estado = p.Estado.Nome,
                    Nome = p.Nome,
                    Ativo = p.Ativo
                }));
        }

        //public ActionResult ObterTiposLogradouro(string query)
        //{
        //    IRepositorioEndereco repEndereco = Fabrica.Instancia.ObterRepositorio<IRepositorioEndereco>(UnidadeTrabalho);

        //    return Json(repEndereco.ObterTiposLogradouro(query).OrderBy(p => p.Nome).ToList());
        //}
    }
}
