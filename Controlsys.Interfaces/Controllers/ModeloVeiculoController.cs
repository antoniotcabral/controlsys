﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Controlsys.Dominio.Parametros;
using Controlsys.Infra;
using Controlsys.Repositorio.Parametros;
using Globalsys;
using Globalsys.Validacao;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class ModeloVeiculoController : Controller, IControllerBase
    {
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        public ModeloVeiculoController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        [ExcludeFromCodeCoverage]
        public ActionResult Index()
        {
            return View();
        }

        [Transaction]
        public ActionResult Incluir(ModeloVeiculo modeloVeiculo, int codigoFabricanteVeiculo)
        {
            IRepositorioFabricanteVeiculo repFabricanteVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioFabricanteVeiculo>(UnidadeTrabalho);
            FabricanteVeiculo fabricanteVeiculo = repFabricanteVeiculo.ObterPorId(codigoFabricanteVeiculo);
            modeloVeiculo.Fabricante = fabricanteVeiculo;

            IRepositorioModeloVeiculo repModeloVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioModeloVeiculo>(UnidadeTrabalho);
            modeloVeiculo.DataRegistro = DateTime.Now;
            modeloVeiculo.Ativar();
            repModeloVeiculo.Validar(modeloVeiculo, EstadoObjeto.Novo);
            repModeloVeiculo.Salvar(modeloVeiculo);
            return Json(montaObj(modeloVeiculo));
        }

        [Transaction]
        public ActionResult Alterar(ModeloVeiculo modeloVeiculo, int codigoFabricanteVeiculo = 0)
        {
            IRepositorioModeloVeiculo repModeloVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioModeloVeiculo>(UnidadeTrabalho);
            ModeloVeiculo modeloVeiculoA = repModeloVeiculo.ObterPorId(modeloVeiculo.Codigo);
            modeloVeiculoA.Nome = modeloVeiculo.Nome;

            if (codigoFabricanteVeiculo > 0)
            {
                IRepositorioFabricanteVeiculo repFabricanteVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioFabricanteVeiculo>(UnidadeTrabalho);
                FabricanteVeiculo fabricanteVeiculo = repFabricanteVeiculo.ObterPorId(codigoFabricanteVeiculo);
                modeloVeiculoA.Fabricante = fabricanteVeiculo;
            }

            repModeloVeiculo.Validar(modeloVeiculoA, EstadoObjeto.Alterado);
            repModeloVeiculo.Atualizar(modeloVeiculoA);
            return Json(montaObj(modeloVeiculoA));
        }

        [Transaction]
        public ActionResult AtivarInativar(int codigo)
        {
            IRepositorioModeloVeiculo repModeloVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioModeloVeiculo>(UnidadeTrabalho);
            ModeloVeiculo modeloVeiculo = repModeloVeiculo.ObterPorId(codigo);
            if (modeloVeiculo.Ativo)
            {
                repModeloVeiculo.Validar(modeloVeiculo, EstadoObjeto.Inativado);
                modeloVeiculo.Inativar();
            }
            else
            {
                repModeloVeiculo.Validar(modeloVeiculo, EstadoObjeto.Ativado);
                modeloVeiculo.Ativar();
            }
            repModeloVeiculo.Atualizar(modeloVeiculo);
            return Json(montaObj(modeloVeiculo));
        }

        public ActionResult Selecionar(int codigo)
        {
            IRepositorioModeloVeiculo repModeloVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioModeloVeiculo>(UnidadeTrabalho);
            ModeloVeiculo modeloVeiculo = repModeloVeiculo.ObterPorId(codigo);
            return Json(montaObj(modeloVeiculo));
        }

        public ActionResult ObterTodos(bool apenasAtivos = false, string nome = null, int? codFabricante = null)
        {
            IRepositorioModeloVeiculo repModeloVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioModeloVeiculo>(UnidadeTrabalho);

            IQueryable<ModeloVeiculo> locais = repModeloVeiculo.ObterTodos();

            if (apenasAtivos)
                locais = locais.Where(t => t.Ativo == true);

            if (!string.IsNullOrEmpty(nome))
                locais = locais.Where(t => t.Nome.Contains(nome));

            if (codFabricante.HasValue)
                locais = locais.Where(t => t.Fabricante.Codigo == codFabricante.Value);

            return Json(locais.ToList());
        }

        public object montaObj(ModeloVeiculo objeto)
        {
            return new
            {
                Codigo = objeto.Codigo,
                Nome = objeto.Nome,
                Ativo = objeto.Ativo,
                Fabricante = new
                {
                    Codigo = objeto.Fabricante.Codigo,
                    Nome = objeto.Fabricante.Nome
                }
            };
        }

    }
}
