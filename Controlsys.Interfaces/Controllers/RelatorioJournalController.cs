﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Pessoas;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Globalsys;
using Globalsys.Relatorios;
using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Seguranca;
using Controlsys.Repositorios.Seguranca;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar relatorio journals.
    /// </summary>
    [ExtendController]
    public class RelatorioJournalController : Controller, IControllerBase
    {
        /// <summary>
        /// Gets/Sets valor para UnidadeTrabalho.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.RelatorioJournalController.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RelatorioJournalController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        /// <summary>
        /// GET: /RelatorioJournal/.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index()
        {
            string retorno = "false";
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            Usuario usuarioLogado = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name, somenteAtivos: true);
            if (usuarioLogado != null)
            {
                var grupos = usuarioLogado.GruposUsuarios.Select(g => g.Grupo);
                var acoes = grupos.SelectMany(g => g.Acoes);
                retorno = acoes.Where(a => a.Url == "/RelatorioJournal/Exportar").Any().ToString();
            }
            return View("Index", model: retorno);
        }

        /// <summary>
        /// Exportar.
        /// </summary>
        ///
        /// <param name="tipoSetor">
        /// O(a) tipo setor.
        /// </param>
        /// <param name="status">
        /// O(a) status.
        /// </param>
        /// <param name="selectedFields">
        /// The selected fields.
        /// </param>
        /// <param name="tipoRelatorio">
        /// O(a) tipo relatorio.
        /// </param>
        /// <param name="cracha">
        /// O(a) cracha.
        /// </param>
        /// <param name="cpf">
        /// O(a) cpf.
        /// </param>
        /// <param name="empresa">
        /// O(a) empresa.
        /// </param>
        /// <param name="nome">
        /// O(a) nome.
        /// </param>
        /// <param name="cargo">
        /// O(a) cargo.
        /// </param>
        /// <param name="dataInicio">
        /// O(a) data inicio.
        /// </param>
        /// <param name="dataFim">
        /// O(a) data fim.
        /// </param>
        /// <param name="setor">
        /// O(a) setor.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Exportar(TipoSetorCusto? tipoSetor, List<StatusAcesso> status, List<string> selectedFields, string tipoRelatorio, string cracha = null, 
            string cpf = null, string passaporte = null, string empresa = null, string nome = null, int cargo = 0, DateTime? dataInicio = null, DateTime? dataFim = null, int setor = 0, 
            List<int> leitoras = null, TipoPapel? papel = null, long? empregadoSap = null, long? gestorPonto = null, long? superiorImediato = null, string codigoPht = null, IList<string> localLeitora = null, string maodeobra = null, string fornecedor = null, string contratada = null)
        {
            empresa = Empresa.RetornaApenasNomeEmpresa(empresa);

            ConsultarAcessosController controller = new ConsultarAcessosController(UnidadeTrabalho);

            var aloc = controller.FiltrarAlocacaoColaboradores(tipoSetor, setor, contratada);
            IQueryable<ControleAcesso> listControleAc = controller.FiltrarAcessos(tipoSetor, status, aloc, nome, cpf, cracha, empresa, cargo, dataInicio, dataFim, 
                setor, leitoras, papel, HttpContext.User.Identity.Name, empregadoSap: empregadoSap, gestorPonto: gestorPonto, superiorImediato: superiorImediato, codigoPht: codigoPht, validaGestor: true, localLeitora: localLeitora, passaporte: passaporte, maodeobra: maodeobra, fornecedor: fornecedor, contratada: contratada);
            int total = listControleAc.Count();
            List<ControleAcessoModelView> data = controller.montaObj(listControleAc, aloc, null, true);


            ReportConfig reportConfig = ReportHelper.RenderReport<ControleAcessoModelView>(selectedFields, tipoRelatorio, data);
            
            string fileName = string.Empty;

            switch (tipoRelatorio)
            {
                case "PDF":
                    fileName = "download.pdf";
                    break;
                case "EXCELOPENXML":
                    fileName = "download.xlsx";
                    break;
                case "WORDOPENXML":
                    fileName = "download.docx";
                    break;
            }

            return File(reportConfig.Bytes, reportConfig.MimeType, fileName);
        }
    }
}
