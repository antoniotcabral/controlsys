﻿using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Ocorrencias;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Ocorrencias;
using Controlsys.Repositorio.Parametros;
using Controlsys.Repositorios.Empresas;
using Controlsys.Repositorios.Pessoas;
using Ext.Net;
using Ext.Net.MVC;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class OcorrenciaTransitoController : Controller, IControllerBase
    {

        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        public OcorrenciaTransitoController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        #region Views ou JSON

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Pesquisar(StoreRequestParameters parameters, string numeroCracha = null, string cpfPassaporte = null, string nome = null, string empresa = null, string contratada = null, DateTime? dataInicioOcorrencia = null, DateTime? dataFimOcorrencia = null, string tipoPapel = null, int codigoInfracao = 0, string grau = null, int codigoPenalidade = 0, DateTime? dataInicioTreinamento = null, DateTime? dataFimTreinamento = null, bool? compareceuNotificacao = null, bool? compareceuTreinamento = null, bool? apto = null, int codigoLocal = 0)
        {
            empresa = Empresa.RetornaApenasNomeEmpresa(empresa);

            IQueryable<OcorrenciaTransito> resultadoFiltro = Filtrar(numeroCracha, cpfPassaporte, nome, empresa, contratada, dataInicioOcorrencia, dataFimOcorrencia, tipoPapel, codigoInfracao, grau, codigoPenalidade, dataInicioTreinamento, dataFimTreinamento, compareceuNotificacao, compareceuTreinamento, apto, codigoLocal);

            int total = resultadoFiltro.Count();
            OcorrenciaTransitoModelView[] retorno = this.MontaObjetos(resultadoFiltro, parameters);

            return new StoreResult(retorno, total);
        }
        
        [Transaction]
        public ActionResult Incluir(int codigoOcorrencia = 0, string codigoPessoa = null, string nome = null, string cpf = null, string passaporte = null, string numeroCracha = null, DateTime? dataOcorrencia = null, DateTime? horaOcorrencia = null, bool anulada = false, int codigoLocal = 0, int codigoInfracao = 0, int codigoPenalidade = 0, bool compareceuNotificacao = false, DateTime? dataAgendamentoTreinamento = null, bool? compareceuTreinamento = null, bool? apto = null)
        {
            if (codigoOcorrencia != 0)
                throw new CoreException("Ocorrência de Trânsito já existente.");
            
            IRepositorioPessoaFisica repPessoaFisica = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaFisica>(UnidadeTrabalho);
            PessoaFisica _pessoaFisica = new PessoaFisica();

            // Incluir uma nova Pessoa Fisica.
            if (!string.IsNullOrWhiteSpace(codigoPessoa))
                _pessoaFisica = repPessoaFisica.ObterPorId(codigoPessoa);
            else            
            {
                var pFisica = new PessoaFisica()
                {
                    Apelido = Globalsys.Util.Tools.ObtemApelido(nome),
                    Nome = nome,
                    CPF = !string.IsNullOrWhiteSpace(cpf) ? cpf.Replace(".", "").Replace("-", "") : null,
                    Passaporte = !string.IsNullOrWhiteSpace(passaporte) ? passaporte : null,
                    DataRegistro = DateTime.Now
                };

                pFisica.GerarCodigoPessoa(repPessoaFisica.ObterTodos());
                repPessoaFisica.Salvar(pFisica);
                repPessoaFisica.Validar(EstadoObjeto.Novo, pFisica);
                _pessoaFisica = pFisica;
            }
            //

            Papel papelAtivo = _pessoaFisica.ObterPapel(true);
            int codigoPapel = papelAtivo != null ? papelAtivo.Codigo : 0;
            
            Local _local = Fabrica.Instancia.ObterRepositorio<IRepositorioLocal>(UnidadeTrabalho).ObterPorId(codigoLocal);
            Infracao _infracao = Fabrica.Instancia.ObterRepositorio<IRepositorioInfracao>(UnidadeTrabalho).ObterPorId(codigoInfracao);
            Penalidade _penalidade = Fabrica.Instancia.ObterRepositorio<IRepositorioPenalidade>(UnidadeTrabalho).ObterPorId(codigoPenalidade);
            Papel _papel = Fabrica.Instancia.ObterRepositorio<IRepositorioPapel>(UnidadeTrabalho).ObterPorId(codigoPapel);

            if (!dataOcorrencia.HasValue || !horaOcorrencia.HasValue)
                throw new CoreException("Daa Ocorrência não preenchida");

            DateTime dataHoraAtual = DateTime.Now;

            OcorrenciaTransito ocorrenciaNova = new OcorrenciaTransito();
            ocorrenciaNova.Ativar();
            ocorrenciaNova.Anulada = anulada;
            ocorrenciaNova.DataRegistro = dataHoraAtual;
            ocorrenciaNova.Infracao = _infracao;
            ocorrenciaNova.Local = _local;
            ocorrenciaNova.NumeroCracha = numeroCracha;
            ocorrenciaNova.Papel = _papel==null? null: _papel;
            ocorrenciaNova.PessoaFisica = _pessoaFisica;
            ocorrenciaNova.Penalidade = _penalidade;
            ocorrenciaNova.DataOcorrencia = new DateTime(dataOcorrencia.Value.Year, dataOcorrencia.Value.Month, dataOcorrencia.Value.Day, horaOcorrencia.Value.Hour, horaOcorrencia.Value.Minute, dataOcorrencia.Value.Second);

            IRepositorioOcorrenciaTransito repOcorrencia = Fabrica.Instancia.ObterRepositorio<IRepositorioOcorrenciaTransito>(UnidadeTrabalho);
            repOcorrencia.Validar(ocorrenciaNova, EstadoObjeto.Novo);
            repOcorrencia.Salvar(ocorrenciaNova);

            TreinamentoOcorrenciaTransito treinamentoNovo = new TreinamentoOcorrenciaTransito();
            treinamentoNovo.Apto = apto;
            treinamentoNovo.Ativar();
            treinamentoNovo.CompareceuNotificacao = compareceuNotificacao;
            treinamentoNovo.CompareceuTreinamento = compareceuTreinamento;
            treinamentoNovo.DataAgendamento = dataAgendamentoTreinamento;
            treinamentoNovo.DataRegistro = dataHoraAtual;
            treinamentoNovo.OcorrenciaTransito = ocorrenciaNova;

            IRepositorioTreinamentoOcorrenciaTransito repTreinamentoOcorrenciaTransito = Fabrica.Instancia.ObterRepositorio<IRepositorioTreinamentoOcorrenciaTransito>(UnidadeTrabalho);
            repTreinamentoOcorrenciaTransito.Validar(treinamentoNovo, EstadoObjeto.Novo);
            repTreinamentoOcorrenciaTransito.Salvar(treinamentoNovo);

            return MontaObjeto(ocorrenciaNova, treinamentoNovo);
        }

        [Transaction]
        public ActionResult Alterar(int codigoOcorrencia = 0, string codigoPessoa = null, int codigoPapel = 0, string numeroCracha = null, DateTime? dataOcorrencia = null, DateTime? horaOcorrencia = null, bool anulada = false, int codigoLocal = 0, int codigoInfracao = 0, int codigoPenalidade = 0, bool compareceuNotificacao = false, DateTime? dataAgendamentoTreinamento = null, bool? compareceuTreinamento = null, bool? apto = null)
        {
            if (codigoOcorrencia <= 0)
                throw new CoreException("Ocorrência de Trânsito não existente.");

            PessoaFisica _pessoaFisica = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaFisica>(UnidadeTrabalho).ObterPorId(codigoPessoa);

            Local _local = Fabrica.Instancia.ObterRepositorio<IRepositorioLocal>(UnidadeTrabalho).ObterPorId(codigoLocal);
            Infracao _infracao = Fabrica.Instancia.ObterRepositorio<IRepositorioInfracao>(UnidadeTrabalho).ObterPorId(codigoInfracao);
            Penalidade _penalidade = Fabrica.Instancia.ObterRepositorio<IRepositorioPenalidade>(UnidadeTrabalho).ObterPorId(codigoPenalidade);
            Papel _papel = Fabrica.Instancia.ObterRepositorio<IRepositorioPapel>(UnidadeTrabalho).ObterPorId(codigoPapel);

            if (!dataOcorrencia.HasValue || !horaOcorrencia.HasValue)
                throw new CoreException("Daa Ocorrência não preenchida");


            IRepositorioOcorrenciaTransito repOcorrencia = Fabrica.Instancia.ObterRepositorio<IRepositorioOcorrenciaTransito>(UnidadeTrabalho);
            IRepositorioTreinamentoOcorrenciaTransito repTreinamentoOcorrenciaTransito = Fabrica.Instancia.ObterRepositorio<IRepositorioTreinamentoOcorrenciaTransito>(UnidadeTrabalho);

            OcorrenciaTransito ocorrenciaAntiga = repOcorrencia.ObterPorId(codigoOcorrencia);
            ocorrenciaAntiga.Anulada = anulada;
            ocorrenciaAntiga.DataOcorrencia = dataOcorrencia.Value;
            ocorrenciaAntiga.Infracao = _infracao;
            ocorrenciaAntiga.Local = _local;
            ocorrenciaAntiga.NumeroCracha = numeroCracha;
            ocorrenciaAntiga.Papel = _papel;
            ocorrenciaAntiga.PessoaFisica = _pessoaFisica;
            ocorrenciaAntiga.Penalidade = _penalidade;


            repOcorrencia.Validar(ocorrenciaAntiga, EstadoObjeto.Alterado);
            repOcorrencia.Salvar(ocorrenciaAntiga);

            ocorrenciaAntiga.Treinamentos[0].Apto = apto;
            ocorrenciaAntiga.Treinamentos[0].CompareceuNotificacao = compareceuNotificacao;
            ocorrenciaAntiga.Treinamentos[0].CompareceuTreinamento = compareceuTreinamento;
            ocorrenciaAntiga.Treinamentos[0].DataAgendamento = dataAgendamentoTreinamento;

            repTreinamentoOcorrenciaTransito.Validar(ocorrenciaAntiga.Treinamentos[0], EstadoObjeto.Alterado);
            repTreinamentoOcorrenciaTransito.Salvar(ocorrenciaAntiga.Treinamentos[0]);

            return MontaObjeto(ocorrenciaAntiga, null);
        }

        [Transaction]
        public ActionResult AtivarInativar(int codigoOcorrencia = 0)
        {
            if (codigoOcorrencia <= 0)
                throw new CoreException("Ocorrência de Trânsito não existente.");

            IRepositorioOcorrenciaTransito repOcorrencia = Fabrica.Instancia.ObterRepositorio<IRepositorioOcorrenciaTransito>(UnidadeTrabalho);
            IRepositorioTreinamentoOcorrenciaTransito repTreinamentoOcorrenciaTransito = Fabrica.Instancia.ObterRepositorio<IRepositorioTreinamentoOcorrenciaTransito>(UnidadeTrabalho);

            OcorrenciaTransito ot = repOcorrencia.ObterPorId(codigoOcorrencia);
            if (ot.Ativo)
                ot.Inativar();
            else
                ot.Ativar();

            ot.Treinamentos[0].Ativo = ot.Ativo;

            repOcorrencia.Validar(ot, EstadoObjeto.Ativado);
            repOcorrencia.Salvar(ot);

            repTreinamentoOcorrenciaTransito.Validar(ot.Treinamentos[0], EstadoObjeto.Ativado);
            repTreinamentoOcorrenciaTransito.Salvar(ot.Treinamentos[0]);

            return MontaObjeto(ot, null);
        }

        
        public ActionResult Selecionar(int codigo)
        {
            IRepositorioOcorrenciaTransito repOT = Fabrica.Instancia.ObterRepositorio<IRepositorioOcorrenciaTransito>(UnidadeTrabalho);
            OcorrenciaTransito ot = repOT.ObterPorId(codigo);

            return MontaObjeto(ot, null);
        }
                
        public ActionResult ObterDetalhesPapel(string codigoPessoa)
        {
            if (!string.IsNullOrEmpty(codigoPessoa))
            {
                PessoaFisica pessoaFisica = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaFisica>(UnidadeTrabalho).ObterPorId(codigoPessoa);
                Papel papelAtivo = pessoaFisica.ObterPapel(true);                
                int codigoPapel = papelAtivo != null ? papelAtivo.Codigo : 0;

                string nome = pessoaFisica.Nome;
                string cpf = pessoaFisica.CPFFormatado();
                string passaporte = pessoaFisica.Passaporte;                

                Papel papel = Fabrica.Instancia.ObterRepositorio<IRepositorioPapel>(UnidadeTrabalho).ObterPorId(codigoPapel);  
                Cracha cracha = Fabrica.Instancia.ObterRepositorio<IRepositorioCracha>(UnidadeTrabalho).ObterCracha(codigoPapel);                
                AlocacaoColaborador alocacao = Fabrica.Instancia.ObterRepositorio<IRepositorioAlocacaoColaborador>(UnidadeTrabalho).ObterAlocacao(codigoPapel);
                //string tipoPapel = papel is Colaborador ? "Colaborador" : papel is PrestadorServico ? "Prestador de Serviço" : "Visitante";
                string tipoPapel = papel is Colaborador ? "Colaborador" : papel is PrestadorServico ? "Prestador de Serviço" : papel is Visitante ? "Visitante" : null;
                string empresa = papel == null ? "" : papel is Colaborador ? (papel as Colaborador).Empresa.Nome : papel is PrestadorServico ? (papel as PrestadorServico).Empresa.Nome : (papel as Visitante).Empresa;                
                string contratada = alocacao != null && alocacao.PedidoCompra != null & alocacao.PedidoCompra.Empresa != null ? alocacao.PedidoCompra.Empresa.Nome : string.Empty;                

                return Json(new { CodigoPapel = codigoPapel, CodigoPessoa = codigoPessoa, NumeroCracha = cracha == null ? string.Empty : cracha.Numero, Nome = nome, CPF = cpf, Passaporte = passaporte/*, numeroCracha*/, TipoPapel = tipoPapel, Empresa = empresa, Contratada = contratada });
            }

            return Json(null);
        }
        
        public ActionResult ObterQuantidadeInfracoes(string codigoPessoa, int codigoInfracao = 0)
        {
            if (!string.IsNullOrWhiteSpace(codigoPessoa))
            {
                int quantidade = GetQuantidadeInfracoes(codigoPessoa, codigoInfracao);
                return Json(new { quantidade });
            }

            return Json(null);
        }


        #endregion

        #region Métodos de Suporte

        private ActionResult MontaObjeto(OcorrenciaTransito ocorrencia, TreinamentoOcorrenciaTransito treinamento)
        {
            if (treinamento != null)
            {
                List<TreinamentoOcorrenciaTransito> auxListaTreinamentoOcorrencia = new List<TreinamentoOcorrenciaTransito>();
                auxListaTreinamentoOcorrencia.Add(treinamento);
                ocorrencia.Treinamentos = auxListaTreinamentoOcorrencia;
            }

            List<OcorrenciaTransito> auxListaOcorrencia = new List<OcorrenciaTransito>();
            auxListaOcorrencia.Add(ocorrencia);

            return Json(MontaObjetos(auxListaOcorrencia.AsQueryable(), null, true));
        }

        public string ObterContratada(int codigoPapel)
        {
            IQueryable<AlocacaoColaborador> alocacoes = Fabrica.Instancia.ObterRepositorio<IRepositorioAlocacaoColaborador>(UnidadeTrabalho).ObterTodos();
            AlocacaoColaborador alocacao = alocacoes.Where(a => a.Papel.Codigo == codigoPapel).FirstOrDefault();

            return alocacao == null ? string.Empty : alocacao.PedidoCompra.Empresa.Nome;
        }

        private int GetQuantidadeInfracoes(string codigoPessoa, int codigoInfracao = 0)
        {
            //Papel papel = Fabrica.Instancia.ObterRepositorio<IRepositorioPapel>(UnidadeTrabalho).ObterPorId(codigoPapel);
            PessoaFisica pessoaFisica = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaFisica>(UnidadeTrabalho).ObterPorId(codigoPessoa);

            Infracao infracao = null;
            if (codigoInfracao > 0)
                infracao = Fabrica.Instancia.ObterRepositorio<IRepositorioInfracao>(UnidadeTrabalho).ObterPorId(codigoInfracao);

            return this.GetQuantidadeInfracoes(pessoaFisica, infracao);
        }

        private int GetQuantidadeInfracoes(PessoaFisica pessoaFisica, Infracao infracao = null)
        {
            IQueryable<OcorrenciaTransito> ocorrencias = Fabrica.Instancia.ObterRepositorio<IRepositorioOcorrenciaTransito>(UnidadeTrabalho).ObterTodos().Where(ot => ot.PessoaFisica.Codigo == pessoaFisica.Codigo && !ot.Anulada);
            if (infracao != null)
                ocorrencias = ocorrencias.Where(ot => ot.Infracao.Grau == infracao.Grau);

            int quantidade = ocorrencias.Count();
            return quantidade;
        }

        public OcorrenciaTransitoModelView[] MontaObjetos(IQueryable<OcorrenciaTransito> ocorrencias, StoreRequestParameters parameters = null, bool ignoreParameters = false)
        {

            if (!ignoreParameters)
            {
                if (parameters == null)
                    parameters = new StoreRequestParameters();

                ocorrencias = ocorrencias.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 30);
            }
            if (ocorrencias == null) //|| ocorrencias.Count() <= 0
                return null;
            
            OcorrenciaTransitoModelView[] otsModelView = ocorrencias.ToList().Select(ot => new OcorrenciaTransitoModelView()
            {
                Anulada = ot.Anulada,
                Ativo = ot.Ativo,
                Apto = ot.Treinamentos[0].Apto,
                CodigoOcorrencia = ot.Codigo,
                CodigoInfracao = ot.Infracao.Codigo,
                CodigoLocal = ot.Local.Codigo,
                CodigoPapel = ot.Papel==null ? 0: ot.Papel.Codigo,                
                CodigoPessoa = ot.PessoaFisica != null ? ot.PessoaFisica.Codigo : null,
                CodigoPenalidade = ot.Penalidade.Codigo,
                CompareceuNotificacao = ot.Treinamentos[0].CompareceuNotificacao,
                CompareceuTreinamento = ot.Treinamentos[0].CompareceuTreinamento,
                Contratada = ot.Papel == null ? null: ObterContratada(ot.Papel.Codigo),
                CPF = ot.PessoaFisica.CPFFormatado(),
                DataHoraOcorrencia = ot.DataOcorrencia.ToString("dd/MM/yyyy HH:mm"),
                DataOcorrencia = ot.DataOcorrencia.ToString("dd/MM/yyyy"),
                Empresa = ot.Papel == null ? null : (ot.Papel is Colaborador) ? (ot.Papel as Colaborador).Empresa.Nome : (ot.Papel is PrestadorServico) ? (ot.Papel as PrestadorServico).Empresa.Nome : (ot.Papel as Visitante).Empresa,
                Gravidade = ot.Infracao.Grau.ObterDescricaoEnum(),
                HoraOcorrencia = ot.DataOcorrencia.ToString("HH:mm"),
                Infracao = ot.Infracao.Nome,
                Local = ot.Local.Nome,
                Nome = ot.PessoaFisica.Nome,
                NumeroCracha = ot.NumeroCracha,
                Passaporte = ot.PessoaFisica.Passaporte,
                Penalidade = ot.Penalidade.Nome,
                QuantidadeInfracoes = GetQuantidadeInfracoes(ot.PessoaFisica, null),
                QuantidadeInfracoesMesmaGravidade = GetQuantidadeInfracoes(ot.PessoaFisica, ot.Infracao),
                TipoPapel = ot.Papel == null ? null : (ot.Papel is Colaborador) ? "Colaborador" : (ot.Papel is PrestadorServico) ? "Prestador de Serviço" : "Visitante",
                TreinamentoAgendado = ot.Treinamentos[0].DataAgendamento

            }).ToArray();

            return otsModelView;
        }

        public IQueryable<OcorrenciaTransito> Filtrar(string numeroCracha = null, string cpfPassaporte = null, string nome = null, string empresa = null, string contratada = null, DateTime? dataInicioOcorrencia = null, DateTime? dataFimOcorrencia = null, string tipoPapel = null, int codigoInfracao = 0, string grau = null, int codigoPenalidade = 0, DateTime? dataInicioTreinamento = null, DateTime? dataFimTreinamento = null, bool? compareceuNotificacao = null, bool? compareceuTreinamento = null, bool? apto = null, int codigoLocal = 0)
        {
            IRepositorioOcorrenciaTransito repOcorrencia = Fabrica.Instancia.ObterRepositorio<IRepositorioOcorrenciaTransito>(UnidadeTrabalho);

            IQueryable<OcorrenciaTransito> ocorrencias = repOcorrencia.ObterTodos();

            if (!string.IsNullOrEmpty(numeroCracha))
                ocorrencias = ocorrencias.Where(o => o.NumeroCracha.Contains(numeroCracha));

            if (!string.IsNullOrEmpty(cpfPassaporte))
            {
                string cpfpassaporteformatado = cpfPassaporte.Replace("-", string.Empty).Replace(".", string.Empty).Replace("/", string.Empty);
                ocorrencias = ocorrencias = ocorrencias.Where(o => (o.PessoaFisica.CPF != null && o.PessoaFisica.CPF.Contains(cpfpassaporteformatado)) || (o.PessoaFisica.Passaporte != null && o.PessoaFisica.Passaporte.Contains(cpfpassaporteformatado)));
            }

            if (!string.IsNullOrEmpty(nome))
                ocorrencias = ocorrencias.Where(o => o.PessoaFisica.Nome.Contains(nome));

            //if (!string.IsNullOrEmpty(empresa))
            //    ocorrencias = ocorrencias.Where(o => (o.Papel is Colaborador && (o.Papel as Colaborador).Empresa.Nome.Equals(empresa)) || (o.Papel is PrestadorServico && (o.Papel as PrestadorServico).Empresa != null && (o.Papel as PrestadorServico).Empresa.Nome.Equals(empresa)) || (o.Papel is Visitante && (o.Papel as Visitante).Empresa.Equals(empresa)));

            if (!string.IsNullOrEmpty(empresa))
            {
                var papeis = ocorrencias.Select(o => o.Papel);              

                var auxCodPapeis = new List<int>();
                empresa = empresa.ToUpper();

                foreach (var item in papeis)
                {
                    if (item is Colaborador)
                        if ((item as Colaborador).Empresa != null && (item as Colaborador).Empresa.Nome.ToUpper().Contains(empresa))
                            auxCodPapeis.Add(item.Codigo);

                    if (item is PrestadorServico)
                    {
                        if ((item as PrestadorServico).TipoPrestador == TipoPrestador.Temporario)
                        {
                            if ((item as PrestadorServico).Empresa != null && (item as PrestadorServico).Empresa.Nome.ToUpper().Contains(empresa))
                                auxCodPapeis.Add(item.Codigo);
                        }
                        else
                        {
                            if ((item as PrestadorServico).Fornecedor != null && (item as PrestadorServico).Fornecedor.RazaoSocial.ToUpper().Contains(empresa))
                                auxCodPapeis.Add(item.Codigo);
                        }
                    }

                    if (item is Visitante)
                        if ((item as Visitante).Empresa != null && (item as Visitante).Empresa.ToUpper().Contains(empresa))
                            auxCodPapeis.Add(item.Codigo);

                }

                ocorrencias = ocorrencias.Where(o => auxCodPapeis.Contains(o.Papel.Codigo));
            }

            if (!string.IsNullOrEmpty(contratada))
            {
                IQueryable<AlocacaoColaborador> alocacoes = Fabrica.Instancia.ObterRepositorio<IRepositorioAlocacaoColaborador>(UnidadeTrabalho).ObterTodos();
                alocacoes = alocacoes.Where(a => a.PedidoCompra.Empresa.Nome.Equals(contratada));
                int[] codigosPapelAlocacao = alocacoes.Select(a => a.Papel.Codigo).ToArray();

                ocorrencias = ocorrencias.Where(ot => codigosPapelAlocacao.Contains(ot.Papel.Codigo));
            }

            if (!string.IsNullOrEmpty(numeroCracha))
                ocorrencias = ocorrencias.Where(o => o.NumeroCracha.Contains(numeroCracha));

            if (dataInicioOcorrencia.HasValue)
                ocorrencias = ocorrencias.Where(o => o.DataOcorrencia >= dataInicioOcorrencia.Value);

            if (dataFimOcorrencia.HasValue)
                ocorrencias = ocorrencias.Where(o => o.DataOcorrencia <= dataFimOcorrencia.Value);

            if (dataInicioTreinamento.HasValue)
                ocorrencias = ocorrencias.ToList().Where(o => o.Treinamentos[0].DataAgendamento >= dataInicioTreinamento).AsQueryable();

            if (dataFimTreinamento.HasValue)
                ocorrencias = ocorrencias.ToList().Where(o => o.Treinamentos[0].DataAgendamento <= dataFimTreinamento).AsQueryable();

            if (!string.IsNullOrEmpty(tipoPapel))
                switch (tipoPapel)
                {
                    case "Colaborador":
                        ocorrencias = ocorrencias.Where(o => o.Papel is Colaborador);
                        break;
                    case "Visitante":
                        ocorrencias = ocorrencias.Where(o => o.Papel is Visitante);
                        break;
                    case "Prestador Serviço":
                        ocorrencias = ocorrencias.Where(o => o.Papel is PrestadorServico);
                        break;
                    default:
                        break;
                }

            if (codigoInfracao > 0)
                ocorrencias = ocorrencias.Where(o => o.Infracao.Codigo == codigoInfracao);

            if (!string.IsNullOrEmpty(grau))
            {
                foreach (GrauInfracao item in Enum.GetValues(typeof(GrauInfracao)))
                {
                    if (item.ObterDescricaoEnum().Equals(grau))
                    {
                        ocorrencias = ocorrencias.Where(o => o.Infracao.Grau == item);
                        break;
                    }
                }
            }

            if (codigoPenalidade > 0)
                ocorrencias = ocorrencias.Where(o => o.Penalidade.Codigo == codigoPenalidade);

            if (compareceuNotificacao.HasValue)
                ocorrencias = ocorrencias.ToList().Where(o => o.Treinamentos[0].CompareceuNotificacao == compareceuNotificacao).AsQueryable();

            if (compareceuTreinamento.HasValue)
                ocorrencias = ocorrencias.ToList().Where(o => o.Treinamentos[0].CompareceuTreinamento == compareceuTreinamento).AsQueryable();

            if (apto.HasValue)
                ocorrencias = ocorrencias.ToList().Where(o => o.Treinamentos[0].Apto == apto.Value).AsQueryable();

            if (codigoLocal > 0)
                ocorrencias = ocorrencias.Where(o => o.Local.Codigo == codigoLocal);

            return ocorrencias;
        }

        #endregion

    }
}
