﻿using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Ocorrencias;
using Controlsys.Interfaces.Models;
using Ext.Net;
using Ext.Net.MVC;
using Globalsys;
using Globalsys.Relatorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class RelatorioOcorrenciaTransitoController : Controller, IControllerBase
    {

        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        public RelatorioOcorrenciaTransitoController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Exportar(List<string> selectedFields, string tipoRelatorio, string numeroCracha = null, string cpfPassaporte = null, string nome = null, string empresa = null, string contratada = null, DateTime? dataInicioOcorrencia = null, DateTime? dataFimOcorrencia = null, string tipoPapel = null, int codigoInfracao = 0, string grau = null, int codigoPenalidade = 0, DateTime? dataInicioTreinamento = null, DateTime? dataFimTreinamento = null, bool? compareceuNotificacao = null, bool? compareceuTreinamento = null, bool? apto = null, int codigoLocal = 0)
        {
            empresa = Empresa.RetornaApenasNomeEmpresa(empresa);

            OcorrenciaTransitoController controller = new OcorrenciaTransitoController(UnidadeTrabalho);

            IQueryable<OcorrenciaTransito> resultadoFiltro = controller.Filtrar(numeroCracha, cpfPassaporte, nome, empresa, contratada, dataInicioOcorrencia, dataFimOcorrencia, tipoPapel, codigoInfracao, grau, codigoPenalidade, dataInicioTreinamento, dataFimTreinamento, compareceuNotificacao, compareceuTreinamento, apto, codigoLocal);

            int total = resultadoFiltro.Count();
            OcorrenciaTransitoModelView[] retorno = controller.MontaObjetos(resultadoFiltro, null, true);

            ReportConfig reportConfig = ReportHelper.RenderReport<OcorrenciaTransitoModelView>(selectedFields, tipoRelatorio, retorno);

            string fileName = string.Empty;

            switch (tipoRelatorio)
            {
                case "PDF":
                    fileName = "download.pdf";
                    break;
                case "Excel":
                    fileName = "download.xls";
                    break;
                case "Word":
                    fileName = "download.doc";
                    break;
            }

            return File(reportConfig.Bytes, reportConfig.MimeType, fileName);
        }

        public ActionResult Pesquisar(StoreRequestParameters parameters, string numeroCracha = null, string cpfPassaporte = null, string nome = null, string empresa = null, string contratada = null, DateTime? dataInicioOcorrencia = null, DateTime? dataFimOcorrencia = null, string tipoPapel = null, int codigoInfracao = 0, string grau = null, int codigoPenalidade = 0, DateTime? dataInicioTreinamento = null, DateTime? dataFimTreinamento = null, bool? compareceuNotificacao = null, bool? compareceuTreinamento = null, bool? apto = null, int codigoLocal = 0)
        {
            empresa = Empresa.RetornaApenasNomeEmpresa(empresa);

            OcorrenciaTransitoController controller = new OcorrenciaTransitoController(UnidadeTrabalho);

            IQueryable<OcorrenciaTransito> resultadoFiltro = controller.Filtrar(numeroCracha, cpfPassaporte, nome, empresa, contratada, dataInicioOcorrencia, dataFimOcorrencia, tipoPapel, codigoInfracao, grau, codigoPenalidade, dataInicioTreinamento, dataFimTreinamento, compareceuNotificacao, compareceuTreinamento, apto, codigoLocal);

            int total = resultadoFiltro.Count();
            OcorrenciaTransitoModelView[] retorno = controller.MontaObjetos(resultadoFiltro, parameters);

            return new StoreResult(retorno, total);
        }

    }
}
