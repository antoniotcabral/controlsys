﻿using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Seguranca;
using Controlsys.Infra;
using Controlsys.Repositorios.Acesso;
using Controlsys.Repositorios.Parametros;
using Controlsys.Repositorios.Seguranca;
using Globalsys;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class NotificaControladoraOfflineController : Controller, IControllerBase
    {
        /// <summary>
        /// Gets/Sets valor para UnidadeTrabalho.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.NotificaValidadeDocumentosController.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public NotificaControladoraOfflineController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        [AllowAnonymous]
        [HttpPost]
        public string Index()
        {
            if (Request.HttpMethod != "POST")
            {
                Response.StatusCode = 403;
                Response.StatusDescription = "Requisição inválida.";
                throw new InvalidOperationException("Requisição inválida.");
            }

            DateTime dtInicio = DateTime.Now;

            IRepositorioParametro repParam = Fabrica.Instancia.ObterRepositorio<IRepositorioParametro>(UnidadeTrabalho);

            Parametro parametroTempoNotificaControladoraOffline = repParam.RecuperarParametro(ParametroSistema.TempoNotificaControladoraOffline);
            Parametro parametroGrupoControladoraOffline = repParam.RecuperarParametro(ParametroSistema.GrupoControladoraOffline);
            double tempo = 0;
            int codigoGrupo = 0;

            double.TryParse(parametroTempoNotificaControladoraOffline.Valor, out tempo);
            int.TryParse(parametroGrupoControladoraOffline.Valor, out codigoGrupo);

            IQueryable<GrupoUsuario> gruposUsuarios = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoUsuario>(UnidadeTrabalho).ObterTodos().Where(gu => gu.Grupo.Codigo == codigoGrupo);

            IEnumerable<Controladora> controladorasOffline = Fabrica.Instancia.ObterRepositorio<IRepositorioControladora>(UnidadeTrabalho).ObterTodos().ToList()
                .Where(c => c.Ativo
                && c.Status != null
                && c.Status.Value == Dominio.Acesso.StatusControladora.Ativo
                && !c.Online && (c.DataUltimaAlteracaoOnOff.HasValue && c.DataUltimaAlteracaoOnOff.Value.AddHours(tempo) < DateTime.Now.AddMinutes(1)));

            string corpoEmail = string.Empty;

            if (controladorasOffline != null && controladorasOffline.Count() > 0)
            {
                string ips = controladorasOffline.Select(c => c.IP).Aggregate((i, j) => i + ", " + j);
                corpoEmail = string.Format("A(s) controladora(s) de IP(s) {0} está(ão) offline.", ips);
            }
            else
            {
                corpoEmail = "Nenhuma controladora está offline.";
            }

            Dictionary<string, string> listaErrosEnviarEmail = new Dictionary<string, string>();

            if (gruposUsuarios != null && gruposUsuarios.Count() > 0)
            {
                foreach (GrupoUsuario grupoUsuario in gruposUsuarios)
                {
                    try
                    {
                        Globalsys.Util.Email.Enviar("Aviso de controladora offline", corpoEmail, grupoUsuario.Usuario.PessoaFisica.Email);
                    }
                    catch (Exception ex)
                    {
                        listaErrosEnviarEmail.Add(grupoUsuario.Usuario.PessoaFisica.Nome, ex.Message);
                    }
                }
            }


            DateTime dtfim = DateTime.Now;
            TimeSpan tempoDecorrido = dtfim - dtInicio;
            var json = new
            {
                Success = listaErrosEnviarEmail.Count == 0,
                Data = listaErrosEnviarEmail.Select(e => new { Empresa = e.Key, Erro = e.Value }),
                Message = "Tempo decorrido: " + tempoDecorrido.ToString(@"hh\:mm\:ss")
            };

            return JsonConvert.SerializeObject(json, Newtonsoft.Json.Formatting.Indented);
        }

    }
}
