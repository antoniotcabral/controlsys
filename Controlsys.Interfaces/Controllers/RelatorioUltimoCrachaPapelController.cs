﻿using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Relatorio;
using Controlsys.Infra;
using Controlsys.Repositorio.Relatorio;
using Ext.Net;
using Ext.Net.MVC;
using Globalsys;
using Globalsys.Relatorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class RelatorioUltimoCrachaPapelController : Controller, IControllerBase
    {
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        public RelatorioUltimoCrachaPapelController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        //
        // GET: /RelatorioUltimoCrachaPapel/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Pesquisar(StoreRequestParameters parameters, DateTime? dataInicio = null, DateTime? dataFim = null, string tipoPapel = null, string cnpjEmpresa = null, string nomeEmpresa = null, string cnpjContratada = null, string nomeContratada = null, bool ignoreParameters = false)
        {
            nomeEmpresa = Empresa.RetornaApenasNomeEmpresa(nomeEmpresa);
            nomeContratada = Empresa.RetornaApenasNomeEmpresa(nomeContratada);

            IQueryable<RelatorioUltimoCrachaPapel> regs = Filtrar(dataInicio, dataFim, tipoPapel, cnpjEmpresa, nomeEmpresa, cnpjContratada, nomeContratada);

            int total = regs.Count();
            List<RelatorioUltimoCrachaPapel> data = montaObj(regs, parameters, ignoreParameters);

            return new StoreResult(data, total);
        }

        public ActionResult Exportar(List<string> selectedFields, string tipoRelatorio, DateTime? dataInicio = null, DateTime? dataFim = null, string tipoPapel = null, string cnpjEmpresa = null, string nomeEmpresa = null, string cnpjContratada = null, string nomeContratada = null)
        {
            nomeEmpresa = Empresa.RetornaApenasNomeEmpresa(nomeEmpresa);
            nomeContratada = Empresa.RetornaApenasNomeEmpresa(nomeContratada);

            var list = Filtrar(dataInicio, dataFim, tipoPapel, cnpjEmpresa, nomeEmpresa, cnpjContratada, nomeContratada);
            var crachas = montaObj(list, null, true);

            ReportConfig reportConfig = ReportHelper.RenderReport<RelatorioUltimoCrachaPapel>(selectedFields, tipoRelatorio, crachas);

            string fileName = string.Empty;

            switch (tipoRelatorio)
            {
                case "PDF":
                    fileName = "download.pdf";
                    break;
                case "EXCELOPENXML":
                    fileName = "download.xlsx";
                    break;
                case "WORDOPENXML":
                    fileName = "download.docx";
                    break;
            }

            return File(reportConfig.Bytes, reportConfig.MimeType, fileName);
        }

        private IQueryable<RelatorioUltimoCrachaPapel> Filtrar(DateTime? dataInicio = null, DateTime? dataFim = null, string tipoPapel = null, string cnpjEmpresa = null, string nomeEmpresa = null, string cnpjContratada = null, string nomeContratada = null)
        {
            var sDtaInicio = dataInicio.HasValue ? string.Format("CAST(CAST({0} AS varchar) + '-' + CAST({1} AS varchar) + '-' + CAST({2} AS varchar)" +
                                           " + ' ' + CAST({3} AS varchar) + ':' + CAST({4} AS varchar) AS DATETIME)",
                                           dataInicio.Value.Year, dataInicio.Value.Month, dataInicio.Value.Day, dataInicio.Value.Hour, dataInicio.Value.Minute) : string.Empty;
            if (dataFim.HasValue)
                dataFim = dataFim.Value.AddDays(1);

            var sDtaFim = dataFim.HasValue ? string.Format("CAST(CAST({0} AS varchar) + '-' + CAST({1} AS varchar) + '-' + CAST({2} AS varchar)" +
                                           " + ' ' + CAST({3} AS varchar) + ':' + CAST({4} AS varchar) AS DATETIME)",
                                           dataFim.Value.Year, dataFim.Value.Month, dataFim.Value.Day, 0, 0) : string.Empty;

            string query = string.Format(@"SELECT * FROM fn_get_ultimo_cracha_papel({0},{1})", dataInicio.HasValue ? sDtaInicio : "NULL", dataFim.HasValue ? sDtaFim : "NULL");

            var parameters = new Dictionary<string, object>();
            //
            //parameters.Add("dtInicio", dataInicio);
            //parameters.Add("dtFim", dataFim);
            //
            //string query = @"SELECT * FROM fn_get_qtd_crachas_emitidos(:dtInicio,:dtFim)";
            //
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var lista = unidTrabalho.ExecuteSql<RelatorioUltimoCrachaPapel>(query, parameters).AsQueryable();

            if (!string.IsNullOrEmpty(tipoPapel))
                lista = lista.Where(rucp => rucp.TipoPapel == tipoPapel);

            if (!string.IsNullOrEmpty(cnpjContratada))
            {
                cnpjContratada = cnpjContratada.Replace(".", "").Replace("/", "").Replace("-", "");
                lista = lista.Where(rucp => rucp.CNPJContratada.Contains(cnpjContratada));
            }

            if (!string.IsNullOrEmpty(cnpjEmpresa))
            {
                cnpjEmpresa = cnpjEmpresa.Replace(".", "").Replace("/", "").Replace("-", "");
                lista = lista.Where(rucp => rucp.CNPJEmpresa.Contains(cnpjEmpresa));
            }

            if (!string.IsNullOrEmpty(nomeContratada))
                lista = lista.Where(rucp => rucp.NomeContratada != null && rucp.NomeContratada.Contains(nomeContratada));

            if (!string.IsNullOrEmpty(nomeEmpresa))
                lista = lista.Where(rucp => rucp.NomeEmpresa.Contains(nomeEmpresa));

            return lista;
        }

        private List<RelatorioUltimoCrachaPapel> montaObj(IQueryable<RelatorioUltimoCrachaPapel> regs, StoreRequestParameters parameters = null, bool ignoreParameters = false)
        {
            if (!ignoreParameters)
            {
                if (parameters == null)
                    parameters = new StoreRequestParameters();
                regs = regs.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 25);
            }

            return regs.ToList();
        }

    }
}
