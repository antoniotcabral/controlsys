﻿using Controlsys.Dominio.Parametros;
using Controlsys.Infra;
using Controlsys.Repositorio.Parametros;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class SindicatoController : Controller, IControllerBase
    {
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        public SindicatoController(IUnidadeTrabalho unidadeTrabalho)
        { UnidadeTrabalho = unidadeTrabalho; }

        public ActionResult Index()
        { return View(); }

        //IRepositorioSindicato repSindicato = Fabrica.Instancia.ObterRepositorio<IRepositorioSindicato>(UnidadeTrabalho);
        [Transaction]
        public ActionResult Incluir(Sindicato sindicato)
        {
            IRepositorioSindicato repSindicato = Fabrica.Instancia.ObterRepositorio<IRepositorioSindicato>(UnidadeTrabalho);
            sindicato.DataRegistro = DateTime.Now;
            sindicato.Ativar();
            sindicato.Nome = sindicato.Nome.ToUpper().Trim();
            sindicato.Razao = sindicato.Razao.ToUpper().Trim();
            sindicato.Cnpj = sindicato.Cnpj.Replace(".", "").Replace("/", "").Replace("-", "");
            sindicato.Telefone = sindicato.Telefone.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "");
            sindicato.TelEmergencia = sindicato.TelEmergencia.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "");
            sindicato.Celular = sindicato.Celular != null ? sindicato.Celular.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "") : null;
            repSindicato.Validar(sindicato, EstadoObjeto.Novo);
            repSindicato.Salvar(sindicato);
            return Json(new { Codigo = sindicato.Codigo, CNPJ = sindicato.CNPJFormatado(), Nome = sindicato.Razao, Apelido = sindicato.Nome, Ativo = sindicato.Ativo });
        }

        [Transaction]
        public ActionResult Alterar(Sindicato sindicato)
        {
            IRepositorioSindicato repSindicato = Fabrica.Instancia.ObterRepositorio<IRepositorioSindicato>(UnidadeTrabalho);
            Sindicato sindicatoT = repSindicato.ObterPorId(sindicato.Codigo);
            //sindicatoT.DataRegistro = sindicatoT.DataRegistro;
            sindicatoT.Nome = sindicato.Nome.ToUpper().Trim();
            sindicatoT.Razao = sindicato.Razao.ToUpper().Trim();
            sindicatoT.Cnpj = sindicato.Cnpj.Replace(".", "").Replace("/", "").Replace("-", "");
            sindicatoT.Telefone = sindicato.Telefone.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "");
            sindicatoT.TelEmergencia = sindicato.TelEmergencia.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "");
            sindicatoT.Celular = sindicato.Celular != null ? sindicato.Celular.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "") : null;
            repSindicato.Validar(sindicatoT, EstadoObjeto.Alterado);
            repSindicato.Atualizar(sindicatoT);

            return Json(new { Codigo = sindicatoT.Codigo, CNPJ = sindicatoT.CNPJFormatado(), Nome = sindicatoT.Razao, Apelido = sindicatoT.Nome, Ativo = sindicatoT.Ativo });
        }

        [Transaction]
        public ActionResult AtivarInativar(int codigo)
        {
            IRepositorioSindicato repSindicato = Fabrica.Instancia.ObterRepositorio<IRepositorioSindicato>(UnidadeTrabalho);
            Sindicato sindicato = repSindicato.ObterPorId(codigo);
            if (sindicato.Ativo)
            {
                repSindicato.Validar(sindicato, EstadoObjeto.Inativado);
                sindicato.Inativar();
            }
            else
            {
                repSindicato.Validar(sindicato, EstadoObjeto.Ativado);
                sindicato.Ativar();
            }
            repSindicato.Atualizar(sindicato);
            return Json(MontaObjeto(sindicato));
        }
        public ActionResult Selecionar(int codigo)
        {
            IRepositorioSindicato repSindicato = Fabrica.Instancia.ObterRepositorio<IRepositorioSindicato>(UnidadeTrabalho);
            Sindicato sindicato = repSindicato.ObterPorId(codigo);

            return Json(MontaObjeto(sindicato));
        }

        private object MontaObjeto(Sindicato sindicato)
        {
            return new
            {
                Codigo = sindicato.Codigo,
                Apelido = sindicato.Nome,
                Nome = sindicato.Razao,
                Telefone = TelefoneFormatado(sindicato.Telefone),
                TelEmergencia = TelefoneFormatado(sindicato.TelEmergencia),
                Email = sindicato.Email,
                Celular = TelefoneFormatado(sindicato.Celular),
                Ativo = sindicato.Ativo,
                CNPJ = sindicato.CNPJFormatado()
            };
        }
        public ActionResult ObterTodos(bool apenasAtivos = false, string nome = "")
        {
            IRepositorioSindicato repSindicato = Fabrica.Instancia.ObterRepositorio<IRepositorioSindicato>(UnidadeTrabalho);
            IQueryable<Sindicato> sindicato = repSindicato.ObterTodos();

            if (apenasAtivos)
                sindicato = sindicato.Where(t => t.Ativo == true);

            if (!string.IsNullOrEmpty(nome))
                sindicato = sindicato.Where(a => a.Nome.Contains(nome));

            return Json(sindicato.Select(a => new
            {
                Codigo = a.Codigo,
                Apelido = a.Nome,
                Nome = a.Razao,
                Telefone = TelefoneFormatado(a.Telefone),
                TelEmergencia = TelefoneFormatado(a.TelEmergencia),
                Email = a.Email,
                Celular = TelefoneFormatado(a.Celular),
                Ativo = a.Ativo,
                CNPJ = a.CNPJFormatado()
            }).OrderBy(a => a.Nome)
                                .ToList());
        }
        public ActionResult ObterPorCnpj(string cnpj)
        {
            IRepositorioSindicato repSindicato = Fabrica.Instancia.ObterRepositorio<IRepositorioSindicato>(UnidadeTrabalho);
            IQueryable<Sindicato> sindicato = repSindicato.ObterTodos();

            if (!string.IsNullOrEmpty(cnpj))
                sindicato = sindicato.Where(a => a.Cnpj == cnpj.Replace(".", "").Replace("/", "").Replace("-", ""));

            return Json(sindicato.Select(a => new
            {
                Codigo = a.Codigo,
                Apelido = a.Nome,
                Nome = a.Razao,
                Telefone = TelefoneFormatado(a.Telefone),
                TelEmergencia = TelefoneFormatado(a.TelEmergencia),
                Email = a.Email,
                Celular = TelefoneFormatado(a.Celular),
                Ativo = a.Ativo,
                CNPJ = a.CNPJFormatado()
            }).OrderBy(a => a.Nome).ToList());
        }

        public string TelefoneFormatado(string numero)
        {
            if (string.IsNullOrEmpty(numero))
                return null;

            Regex apenasDigitos = new Regex(@"[^\d]");
            string numeroFormatado = apenasDigitos.Replace(numero, "");
            numeroFormatado = string.Format(@"{0:(00) 0000-0000}{1}", Convert.ToInt64(numeroFormatado.Substring(0, 10)), numeroFormatado.Length > 10 ? Convert.ToInt32(numeroFormatado.Substring(10)).ToString() : string.Empty);
            return numeroFormatado;
        }
    }
}
