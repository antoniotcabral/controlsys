﻿using System.Globalization;
using Controlsys.Dominio.AgendamentosPessoa;
using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Enderecos;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Controlsys.Dominio.Seguranca;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Agendamentos;
using Controlsys.Repositorios.Empresas;
using Controlsys.Repositorios.Enderecos;
using Controlsys.Repositorios.Parametros;
using Controlsys.Repositorios.Pessoas;
using Controlsys.Repositorios.Seguranca;
using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using Ext.Net;
using Ext.Net.MVC;
using Microsoft.SqlServer.Server;
using Controlsys.Repositorio.Parametros;
using System.Configuration;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class PreCadastroColaboradorController : Controller, IControllerBase
    {

        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        public PreCadastroColaboradorController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Selecionar(int codigo)
        {
            IRepositorioPreCadastroColaborador rep = Fabrica.Instancia.ObterRepositorio<IRepositorioPreCadastroColaborador>(UnidadeTrabalho);
            ColaboradorAgendamento preCad = rep.ObterPorId(codigo);

            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            Usuario usuarioLogado = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name, somenteAtivos: true);
            return Json(montaObj(preCad, usuarioLogado));
        }

        [Transaction]
        public ActionResult Incluir(ColaboradorAgendamento precadastro, string[] propModificadasPessoa, int codPedido, int codGestor, List<DocumentoMidiaModelView> documentos, string path = null)
        {
            var repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            var repPreCad = Fabrica.Instancia.ObterRepositorio<IRepositorioPreCadastroColaborador>(UnidadeTrabalho);
            var repPessoaAg = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaAgendamento>(UnidadeTrabalho);
            var repPapelAg = Fabrica.Instancia.ObterRepositorio<IRepositorioPapelAgendamento>(UnidadeTrabalho);
            var repColabPed = Fabrica.Instancia.ObterRepositorio<IRepositorio<ColaboradorPedido>>(UnidadeTrabalho);

            IRepositorioDocumento repDoc = Fabrica.Instancia.ObterRepositorio<IRepositorioDocumento>(UnidadeTrabalho);

            var gestor = repColabPed.ObterTodos().Where(v => (v.Pedido.Codigo == codPedido) && (v.Colaborador.Codigo == codGestor)).FirstOrDefault();
            if (gestor == null)
                throw new CoreException("Não foi possível encontrar o cadastro do gestor informado.");
            precadastro.GestorPO = gestor;

            if (precadastro.PessoaAg.CPF != null)
                precadastro.PessoaAg.CPF = precadastro.PessoaAg.CPF.Replace("-", "").Replace(".", "");

            if (precadastro.PessoaAg.Codigo != 0)
            {
                precadastro.PessoaAg = repPessoaAg.ObterPorId(precadastro.PessoaAg, propModificadasPessoa);
                repPessoaAg.Atualizar(precadastro.PessoaAg);
            }
            else
            {
                precadastro.PessoaAg.DataRegistro = DateTime.Now;
                repPessoaAg.Salvar(precadastro.PessoaAg);
            }

            precadastro.Solicitante = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name);
            repPapelAg.SalvarStatusPapelAg(precadastro.InserirPapelAg(StatusAgendamento.AguardandoAutorizacao, null));

            repPreCad.Validar(EstadoObjeto.Novo, precadastro);
            precadastro.DataRegistro = DateTime.Now;
            repPreCad.Salvar(precadastro);

            #region Documentos

            foreach (var item in documentos)
            {
                DocumentoPapel docPapel = repDoc.SalvarDocPapel(null, item.Documento, precadastro);
                string pathUpload = ConfigurationManager.AppSettings.Get("pathUploads");

                if (!System.IO.Directory.Exists(pathUpload))
                    throw new CoreException("O diretório onde a midia seria armazanada não está acessivel.");

                IRepositorioDocumentoMidia repDocumentoMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioDocumentoMidia>(UnidadeTrabalho);
                IRepositorioMidia repMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioMidia>(UnidadeTrabalho);

                int cont = 1;

                foreach (MidiaModelView midia in item.Midias ?? Enumerable.Empty<MidiaModelView>())
                {
                    if (midia == null)
                        continue;
                    string nomeDisco = "PF-" + (string.IsNullOrEmpty(precadastro.PessoaAg.CPF) ? precadastro.PessoaAg.Passaporte : precadastro.PessoaAg.CPF) + "-" + precadastro.Codigo.ToString() + "-" + docPapel.Documento.TipoDocumento.Sigla + "-" + cont + "." + midia.Extensao;
                    Midia m = new Midia();
                    m.DataRegistro = midia.DataRegistro;
                    m.Extensao = midia.Extensao;
                    m.MimeType = midia.MIME;
                    m.NomeArquivo = nomeDisco;
                    repMidia.Salvar(m);

                    repDocumentoMidia.SalvarDocMidia(docPapel.Documento, m);

                    System.IO.File.WriteAllBytes(m.Url, Convert.FromBase64String(midia.Arquivo));

                    cont++;
                }
            }

            #endregion

            #region [Notificar gestor]

            Dictionary<string, string> valoresSubs = new Dictionary<string, string>();

            valoresSubs.Add("$$Papel$$", "Colaborador");
            //valoresSubs.Add("$$Gestor$$", gestor.Colaborador.PessoaFisica.Nome);

            #region [Dados da Pessoa que está cadastrando]

            Usuario usuarioLogado = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name, somenteAtivos: true);
            PessoaFisica pfLogada = usuarioLogado.PessoaFisica;
            Papel papelPLog = pfLogada.ObterPapel();

            valoresSubs.Add("$$Nome$$", pfLogada.Nome);
            valoresSubs.Add("$$Empresa$$", (papelPLog is Colaborador
                ? (papelPLog as Colaborador).Empresa.Nome
                : (papelPLog is PrestadorServico ? (papelPLog as PrestadorServico).Empresa.Nome : "")));
            valoresSubs.Add("$$Funcao$$", (papelPLog is Colaborador
                ? (papelPLog as Colaborador).Cargo.Nome
                : (papelPLog is PrestadorServico ? (papelPLog as PrestadorServico).Cargo.Nome : "")));

            #endregion

            #region [Informações do Colaborador]

            var u = precadastro.PessoaAg;
            valoresSubs.Add("$$Nome2$$", u.Nome);
            valoresSubs.Add("$$CPFPPassaporte$$", !string.IsNullOrEmpty(u.CPF) ? u.CPFFormatado() : u.Passaporte);
            valoresSubs.Add("$$Empresa2$$", precadastro.Empresa.Nome);
            valoresSubs.Add("$$Cargo$$", precadastro.Cargo.Nome);

            #endregion

            #region [Informações da inclusão da Pessoa ]

            valoresSubs.Add("$$Observacao$$", precadastro.Observacao);
            valoresSubs.Add("$$DataAdmissao$$", precadastro.DataAdmissao.HasValue ? precadastro.DataAdmissao.Value.ToString("dd/MM/yyyy") : "");
            valoresSubs.Add("$$DataInclusao$$", precadastro.DataRegistro.ToString("dd/MM/yyyy"));

            #endregion

            if (string.IsNullOrEmpty(path))
            {
                if (string.IsNullOrEmpty(gestor.Colaborador.PessoaFisica.Email))
                    throw new CoreException(string.Format("O gestor '{0}' não possui email cadastrado para ser notificado.", gestor.Colaborador.PessoaFisica.Nome));

                Globalsys.Util.Email.Enviar("[CONTROLSYS] Pré Cadastro de Colaborador",
                    path ?? Server.MapPath("/Content/templates/preCadastro.htm"),
                    valoresSubs,
                    string.Empty,
                    null,
                    gestor.Colaborador.PessoaFisica.Email);
            }

            #endregion

            return Json(montaObj(precadastro));
        }

        [Transaction]
        public ActionResult Alterar(ColaboradorAgendamento preCad, string[] propModificadasPessoa, string[] propModificadasDadosColab, int codPedido, int codGestor, List<DocumentoMidiaModelView> documentosMidias)
        {
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            IRepositorioPapelAgendamento repPapelAg = Fabrica.Instancia.ObterRepositorio<IRepositorioPapelAgendamento>(UnidadeTrabalho);
            IRepositorioPessoaAgendamento repPessoaAg = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaAgendamento>(UnidadeTrabalho);
            IRepositorioPreCadastroColaborador repPreCad = Fabrica.Instancia.ObterRepositorio<IRepositorioPreCadastroColaborador>(UnidadeTrabalho);
            IRepositorioDocumento repDoc = Fabrica.Instancia.ObterRepositorio<IRepositorioDocumento>(UnidadeTrabalho);
            IRepositorioDocumentoMidia repDocumentoMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioDocumentoMidia>(UnidadeTrabalho);
            IRepositorioMidia repMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioMidia>(UnidadeTrabalho);

            var repColabPed = Fabrica.Instancia.ObterRepositorio<IRepositorio<ColaboradorPedido>>(UnidadeTrabalho);

            var gestor = repColabPed.ObterTodos().Where(v => (v.Pedido.Codigo == codPedido) && (v.Colaborador.Codigo == codGestor)).FirstOrDefault();
            if (gestor == null)
                throw new CoreException("Não foi possível encontrar o cadastro do gestor informado.");
            preCad.GestorPO = gestor;

            if (preCad.PessoaAg.CPF != null)
                preCad.PessoaAg.CPF = preCad.PessoaAg.CPF.Replace("-", "").Replace(".", "");

            var preCadBase = repPreCad.ObterPorId(preCad.Codigo);
            var cpfAnterior = preCadBase.PessoaAg.CPF;
            var nomeAnterior = preCadBase.PessoaAg.Nome;
            var pedidoCompraAnterior = preCadBase.GestorPO.Pedido.Numero.Replace(" ", "");
            var pessoaFisicaAnterior = preCadBase.GestorPO.Colaborador.PessoaFisica.Codigo;
            var cargoAnterior = preCadBase.Cargo.Codigo;
            var cargoAnteriorNome = preCadBase.Cargo.Nome;

            if (propModificadasPessoa != null && propModificadasPessoa.Any())
            {
                ///TODO: COrrigir gambiarra abaixo para alteração de endereço
                for (int i = 0; i < propModificadasPessoa.Length; i++)
                {
                    if (propModificadasPessoa[i].Equals("CodigoLog"))
                        propModificadasPessoa[i] = "Endereco";
                    else if (propModificadasPessoa[i].Equals("Numero"))
                        propModificadasPessoa[i] = "EnderecoNumero";
                    else if (propModificadasPessoa[i].Equals("Complemento"))
                        propModificadasPessoa[i] = "EnderecoComplemento";
                }

                PessoaAgendamento pessoaAg = preCad.PessoaAg;
                pessoaAg = repPessoaAg.ObterPorId(pessoaAg, propModificadasPessoa);
                repPessoaAg.Validar(EstadoObjeto.Alterado, pessoaAg);
                repPessoaAg.Atualizar(pessoaAg);
            }

            if (propModificadasDadosColab != null && propModificadasDadosColab.Any())
                preCad = repPreCad.ObterPorId(preCad, propModificadasDadosColab);
            else
                preCad = repPreCad.ObterPorId(preCad.Codigo);
            repPreCad.Validar(EstadoObjeto.Alterado, preCad);

            Usuario usuarioLogado = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name, somenteAtivos: true);

            if (preCad.Solicitante != null && preCad.Solicitante.Codigo != usuarioLogado.Codigo)
            {
                var papel = usuarioLogado.PessoaFisica.ObterPapel(true);
                if (papel == null)
                    throw new CoreException("Não foi possível identificar o usuário logado.");
                Empresa empresa = (papel is Colaborador) ? (papel as Colaborador).Empresa :
                                    ((papel is PrestadorServico) ? (papel as PrestadorServico).Empresa : null);
                if (empresa == null) throw new CoreException("Você não possui permissão para alterar esse registro.");

                if (preCad.Empresa.Codigo != empresa.Codigo)
                    throw new CoreException("Não é possíve salvar, você pertence a uma empresa diferente da regisrada nesse pré cadastro.");
            }

            #region Documentos

            string path = ConfigurationManager.AppSettings.Get("pathUploads");

            if (documentosMidias != null)
            {
                List<DocumentoMidiaModelView> docsPapel = new List<DocumentoMidiaModelView>();
                docsPapel = documentosMidias.Where(dm => preCad.Documentos.Any(cd => dm.Documento.Codigo == cd.Documento.Codigo)).ToList();

                foreach (var item in documentosMidias.Where(dm => preCad.Documentos.Any(cd => dm.Documento.Codigo == cd.Documento.Codigo)))
                {

                    Documento doc = repDoc.ObterPorId(item.Documento.Codigo);
                    doc.Status = item.Documento.Status;
                    doc.Descricao = item.Documento.Descricao;
                    doc.DataValidade = item.Documento.DataValidade;
                    repDoc.Atualizar(doc);

                    if (item.Midias == null)
                    {
                        DocumentoMidiaModelView item1 = item;
                        var docMidias =
                            repDocumentoMidia.ObterTodos().Where(dm => dm.Documento.Codigo == item1.Documento.Codigo && dm.Ativo);
                        foreach (var documentoMidia in docMidias)
                        {
                            documentoMidia.Inativar();
                            repDocumentoMidia.Atualizar(documentoMidia);
                        }
                        continue;
                    }
                    else
                    {
                        DocumentoMidiaModelView item1 = item;
                        List<int> codigosMidia = item.Midias.Select(i => i.CodigoMidia).ToList();
                        IQueryable<DocumentoMidia> listaDocMidia = repDocumentoMidia.ObterTodos().Where(dm =>
                            dm.Documento.Codigo == item1.Documento.Codigo &&
                            dm.Ativo && !codigosMidia.Contains(dm.Midia.Codigo));

                        foreach (DocumentoMidia docMidia in listaDocMidia)
                        {
                            docMidia.Inativar();
                            repDocumentoMidia.Salvar(docMidia);
                        }
                    }

                    int cont = repDocumentoMidia.ObterTodos().Where(dm => dm.Documento.Codigo == item.Documento.Codigo).Count() + 1;

                    foreach (MidiaModelView midia in item.Midias.Where(m => m.CodigoMidia == 0))
                    {
                        if (!System.IO.Directory.Exists(path))
                            throw new CoreException("O diretório onde a midia seria armazanada não está acessivel.");

                        string nomeDisco = string.Format("PF-{0}-{1}-{2}-{3}.{4}",
                            (string.IsNullOrEmpty(preCad.PessoaAg.CPF) ? preCad.PessoaAg.Passaporte : preCad.PessoaAg.CPF),
                            preCad.Codigo, doc.TipoDocumento.Sigla, cont, midia.Extensao);

                        Midia m = new Midia();
                        m.DataRegistro = midia.DataRegistro;
                        m.Extensao = midia.Extensao;
                        m.MimeType = midia.MIME;
                        m.NomeArquivo = nomeDisco;

                        repMidia.Salvar(m);

                        repDocumentoMidia.SalvarDocMidia(doc, m);
                        System.IO.File.WriteAllBytes(m.Url, Convert.FromBase64String(midia.Arquivo));

                        cont++;

                    }
                }

                foreach (var item in documentosMidias.Where(dm => !preCad.Documentos.Any(cd => dm.Documento.Codigo == cd.Documento.Codigo)))
                {
                    DocumentoPapel docPapel = repDoc.SalvarDocPapel(null, item.Documento, preCad);

                    int cont = repDocumentoMidia.ObterTodos().Where(dm => dm.Documento.Codigo == item.Documento.Codigo).Count() + 1;

                    foreach (MidiaModelView midia in item.Midias ?? Enumerable.Empty<MidiaModelView>())
                    {
                        if (midia == null)
                            continue;

                        if (!System.IO.Directory.Exists(path))
                            throw new CoreException("O diretório onde a midia seria armazanada não está acessivel.");

                        string nomeDisco = "PF-" + (string.IsNullOrEmpty(preCad.PessoaAg.CPF) ? preCad.PessoaAg.Passaporte : preCad.PessoaAg.CPF) + "-" + preCad.Codigo.ToString() + "-" + docPapel.Documento.TipoDocumento.Sigla + "-" + cont + "." + midia.Extensao;
                        Midia m = new Midia();
                        m.DataRegistro = midia.DataRegistro;
                        m.Extensao = midia.Extensao;
                        m.MimeType = midia.MIME;
                        m.NomeArquivo = nomeDisco;

                        repMidia.Salvar(m);

                        repDocumentoMidia.SalvarDocMidia(docPapel.Documento, m);

                        System.IO.File.WriteAllBytes(m.Url, Convert.FromBase64String(midia.Arquivo));

                        cont++;
                    }
                }
            }

            #endregion

            repPreCad.Atualizar(preCad);

            //Verifica se atualizou CPF, Passaporte ou Nome, caso ocorra, volta para o status Aguardando Autorização
            #region [Valida alteração de dado crítico]

            string observacao = "";
            bool dadoAlterado = false;
            if (!preCad.PessoaAg.CPF.Equals(cpfAnterior))
            {
                dadoAlterado = true;
                var preCadTemp = new ColaboradorAgendamento { PessoaAg = new PessoaAgendamento() { CPF = cpfAnterior } };
                observacao += string.Format("CPF alterado de '{0}' para '{1}'. ", preCadTemp.PessoaAg.CPFFormatado(), preCad.PessoaAg.CPFFormatado());
            }
            if (!preCad.PessoaAg.Nome.Equals(nomeAnterior))
            {
                dadoAlterado = true;
                observacao += string.Format("Nome alterado de '{0}' para '{1}'. ", nomeAnterior, preCad.PessoaAg.Nome);
            }
            if (!preCad.GestorPO.Pedido.Numero.Replace(" ", "").Equals(pedidoCompraAnterior))
            {
                dadoAlterado = true;
                observacao += string.Format("PO alterada de '{0}' para '{1}'. ", pedidoCompraAnterior, preCad.GestorPO.Pedido.Numero);
            }
            if (!preCad.GestorPO.Colaborador.PessoaFisica.Codigo.Equals(pessoaFisicaAnterior))
            {
                dadoAlterado = true;
                observacao += string.Format("Gestor alterado de '{0}' para '{1}'. ", pessoaFisicaAnterior, preCad.GestorPO.Colaborador.PessoaFisica.Nome);
            }
            if (!preCad.Cargo.Codigo.Equals(cargoAnterior))
            {
                dadoAlterado = true;
                observacao += string.Format("Cargo alterado de '{0}' para '{1}'. ", cargoAnteriorNome, preCad.Cargo.Nome);
            }

            var status = preCad.ObterStatusAtual().StatusAgendamento;
            if (dadoAlterado)
            {
                repPapelAg.SalvarStatusPapelAg(preCad.InserirPapelAg(StatusAgendamento.AguardandoAutorizacao, observacao));
                NotificarGestor(preCad);
            }
            else
            {
                if (status == StatusAgendamento.ContratacaoNaoAutorizada)
                {
                    repPapelAg.SalvarStatusPapelAg(preCad.InserirPapelAg(StatusAgendamento.AguardandoAutorizacao, observacao));
                    NotificarGestor(preCad);
                }
                else if (status == StatusAgendamento.CadastroNaoAprovado)
                    repPapelAg.SalvarStatusPapelAg(preCad.InserirPapelAg(StatusAgendamento.AguardandoAprovacao, observacao));
            }

            #endregion

            return Json(montaObj(preCad, usuarioLogado));
        }        

        public ActionResult Autorizar(int codPreCad)
        {
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            IRepositorioPapelAgendamento repPapelAg = Fabrica.Instancia.ObterRepositorio<IRepositorioPapelAgendamento>(UnidadeTrabalho);
            IRepositorioPreCadastroColaborador repPreCad = Fabrica.Instancia.ObterRepositorio<IRepositorioPreCadastroColaborador>(UnidadeTrabalho);

            var preCad = repPreCad.ObterPorId(codPreCad);
            repPreCad.Validar(EstadoObjeto.Alterado, preCad);

            var status = preCad.ObterStatusAtual().StatusAgendamento;
            if ((status != StatusAgendamento.AguardandoAutorizacao) && (status != StatusAgendamento.ContratacaoNaoAutorizada))
                throw new CoreException("Não é possível salvar, esse cadastro já foi autorizado.");

            Usuario usuarioLogado = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name, somenteAtivos: true);
            if (preCad.GestorPO.Colaborador.PessoaFisica.Codigo != usuarioLogado.PessoaFisica.Codigo)
                throw new CoreException("Não é possível salvar. Somente o gestor relacionado poderá Autorizar esse pré cadastro.");

            repPapelAg.SalvarStatusPapelAg(preCad.InserirPapelAg(StatusAgendamento.ContratacaoAutorizada, null));
            repPapelAg.SalvarStatusPapelAg(preCad.InserirPapelAg(StatusAgendamento.AguardandoAprovacao, null));

            NotificarSolicitante(preCad);

            return Json(montaObj(preCad, usuarioLogado));
        }

        public ActionResult NaoAutorizar(int codPreCad, string observacao, string path = null)
        {
            if (!(codPreCad > 0))
                throw new CoreException("Não foi possível salvar. código de pré cadastro inválido.");

            if (string.IsNullOrEmpty(observacao))
                throw new CoreException("Não foi possível salvar, falta informar a justificativa.");

            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            IRepositorioPapelAgendamento repPapelAg = Fabrica.Instancia.ObterRepositorio<IRepositorioPapelAgendamento>(UnidadeTrabalho);
            IRepositorioPreCadastroColaborador repPreCad = Fabrica.Instancia.ObterRepositorio<IRepositorioPreCadastroColaborador>(UnidadeTrabalho);

            var preCad = repPreCad.ObterPorId(codPreCad);
            repPreCad.Validar(EstadoObjeto.Alterado, preCad);

            var status = preCad.ObterStatusAtual().StatusAgendamento;
            //if(status == StatusAgendamento.ContratacaoNaoAutorizada)
            //    throw new CoreException("Não é possível salvar, esse cadastro já foi Não Autorizado.");

            if ((status != StatusAgendamento.AguardandoAutorizacao) && (status != StatusAgendamento.ContratacaoNaoAutorizada))
                throw new CoreException("Não é possível salvar, esse cadastro já foi autorizado.");

            Usuario usuarioLogado = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name, somenteAtivos: true);
            if (preCad.GestorPO.Colaborador.PessoaFisica.Codigo != usuarioLogado.PessoaFisica.Codigo)
                throw new CoreException("Não é possível salvar. Somente o gestor relacionado poderá Não Autorizar esse pré cadastro.");

            repPapelAg.SalvarStatusPapelAg(preCad.InserirPapelAg(StatusAgendamento.ContratacaoNaoAutorizada, observacao));

            if (string.IsNullOrEmpty(path))
                NotificarSolicitante(preCad);

            return Json(montaObj(preCad, usuarioLogado));
        }

        public ActionResult PreAprovar(int codPreCad)
        {
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            IRepositorioPapelAgendamento repPapelAg = Fabrica.Instancia.ObterRepositorio<IRepositorioPapelAgendamento>(UnidadeTrabalho);
            IRepositorioPreCadastroColaborador repPreCad = Fabrica.Instancia.ObterRepositorio<IRepositorioPreCadastroColaborador>(UnidadeTrabalho);

            var preCad = repPreCad.ObterPorId(codPreCad);
            repPreCad.Validar(EstadoObjeto.Alterado, preCad);

            var status = preCad.ObterStatusAtual().StatusAgendamento;
            if ((status != StatusAgendamento.AguardandoAprovacao) && (status != StatusAgendamento.ContratacaoAutorizada) && (status != StatusAgendamento.CadastroNaoAprovado))
                throw new CoreException("Não é possível salvar, esse cadastro já foi pré-aprovado.");

            Usuario usuarioLogado = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name, somenteAtivos: true);
            //if (preCad.Solicitante.Codigo != usuarioLogado.PessoaFisica.Codigo)
            //    throw new CoreException("Não é possível salvar. Somente o solicitante relacionado poderá Pré-Aprovar esse pré cadastro.");

            repPapelAg.SalvarStatusPapelAg(preCad.InserirPapelAg(StatusAgendamento.CadastroPreAprovado, null));
            //repPapelAg.SalvarStatusPapelAg(preCad.InserirPapelAg(StatusAgendamento.AguardandoAprovacao, null));

            NotificarSolicitante(preCad);

            return Json(montaObj(preCad, usuarioLogado));
        }

        public ActionResult Aprovar(int codPreCad)
        {
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            IRepositorioPreCadastroColaborador repPreCad = Fabrica.Instancia.ObterRepositorio<IRepositorioPreCadastroColaborador>(UnidadeTrabalho);

            var preCad = repPreCad.ObterPorId(codPreCad);
            repPreCad.Validar(EstadoObjeto.Alterado, preCad);

            var status = preCad.ObterStatusAtual().StatusAgendamento;
            if ((status != StatusAgendamento.AguardandoAprovacao) && (status != StatusAgendamento.CadastroNaoAprovado) && (status != StatusAgendamento.ContratacaoAutorizada) && (status != StatusAgendamento.CadastroPreAprovado))
                throw new CoreException("Não é possível salvar, antes de aprovar, esse cadastro precisa ser autorizado pelo gestor responsável.");

            Usuario usuarioLogado = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name, somenteAtivos: true);

            // Redirecionar para página de Colaborador com o parametro de preCadastro, para Criar um papel;
            //repPapelAg.SalvarStatusPapelAg(preCad.InserirPapelAg(StatusAgendamento.CadastroAprovado, null));

            return Json(montaObj(preCad, usuarioLogado));
        }

        public ActionResult Reprovar(int codPreCad, string observacao, string path = null)
        {
            if (!(codPreCad > 0))
                throw new CoreException("Não foi possível salvar. Código de pré cadastro inválido.");

            if (string.IsNullOrEmpty(observacao))
                throw new CoreException("Não foi possível salvar, falta informar a justificativa.");

            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            IRepositorioPapelAgendamento repPapelAg = Fabrica.Instancia.ObterRepositorio<IRepositorioPapelAgendamento>(UnidadeTrabalho);
            IRepositorioPreCadastroColaborador repPreCad = Fabrica.Instancia.ObterRepositorio<IRepositorioPreCadastroColaborador>(UnidadeTrabalho);

            var preCad = repPreCad.ObterPorId(codPreCad);
            if (preCad == null)

                repPreCad.Validar(EstadoObjeto.Alterado, preCad);

            var status = preCad.ObterStatusAtual().StatusAgendamento;
            if ((status != StatusAgendamento.AguardandoAprovacao) && (status != StatusAgendamento.CadastroNaoAprovado) && (status != StatusAgendamento.ContratacaoAutorizada) && (status != StatusAgendamento.CadastroPreAprovado))
                throw new CoreException("Não é possível salvar, esse cadastro precisa primeiro ser autorizado pelo gestor responsável.");

            repPapelAg.SalvarStatusPapelAg(preCad.InserirPapelAg(StatusAgendamento.CadastroNaoAprovado, observacao));

            if (string.IsNullOrEmpty(path))
                NotificarSolicitante(preCad);

            Usuario usuarioLogado = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name, somenteAtivos: true);
            return Json(montaObj(preCad, usuarioLogado));
        }

        private void NotificarGestor(ColaboradorAgendamento precadastro, string path = null)
        {
            Dictionary<string, string> valoresSubs = new Dictionary<string, string>();

            valoresSubs.Add("$$Papel$$", "Colaborador");

            #region [Dados da Pessoa que está cadastrando]

            var repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            Usuario usuarioLogado = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name, somenteAtivos: true);
            PessoaFisica pfLogada = usuarioLogado.PessoaFisica;
            Papel papelPLog = pfLogada.ObterPapel();

            valoresSubs.Add("$$Nome$$", pfLogada.Nome);
            valoresSubs.Add("$$Empresa$$", (papelPLog is Colaborador
                ? (papelPLog as Colaborador).Empresa.Nome
                : (papelPLog is PrestadorServico ? (papelPLog as PrestadorServico).Empresa.Nome : "")));
            valoresSubs.Add("$$Funcao$$", (papelPLog is Colaborador
                ? (papelPLog as Colaborador).Cargo.Nome
                : (papelPLog is PrestadorServico ? (papelPLog as PrestadorServico).Cargo.Nome : "")));

            #endregion

            #region [Informações do Colaborador de Serviço]

            var u = precadastro.PessoaAg;
            valoresSubs.Add("$$Nome2$$", u.Nome);
            valoresSubs.Add("$$CPFPPassaporte$$", !string.IsNullOrEmpty(u.CPF) ? u.CPFFormatado() : u.Passaporte);
            valoresSubs.Add("$$Empresa2$$", precadastro.Empresa.Nome);
            valoresSubs.Add("$$Cargo$$", precadastro.Cargo.Nome);

            #endregion

            #region [Informações da inclusão da Pessoa ]

            valoresSubs.Add("$$Observacao$$", precadastro.Observacao);
            valoresSubs.Add("$$DataAdmissao$$", precadastro.DataAdmissao.HasValue ? precadastro.DataAdmissao.Value.ToString("dd/MM/yyyy") : "");
            valoresSubs.Add("$$DataInclusao$$", precadastro.DataRegistro.ToString("dd/MM/yyyy"));

            #endregion

            #region [Enviar email]

            if (!string.IsNullOrEmpty(path)) return;

            var gestor = precadastro.GestorPO;
            if (string.IsNullOrEmpty(gestor.Colaborador.PessoaFisica.Email))
                throw new CoreException(string.Format("O gestor '{0}' não possui email cadastrado para ser notificado.", gestor.Colaborador.PessoaFisica.Nome));

            Globalsys.Util.Email.Enviar("[CONTROLSYS] Pré Cadastro de Colaborador",
                Server.MapPath("/Content/templates/preCadastro.htm"),
                valoresSubs,
                string.Empty,
                null,
                gestor.Colaborador.PessoaFisica.Email);

            #endregion

        }

        private void NotificarSolicitante(ColaboradorAgendamento precadastro, string path = null)
        {
            if (precadastro.Solicitante == null)
                return;

            var status = precadastro.ObterStatusAtual();
            if ((status.StatusAgendamento != StatusAgendamento.ContratacaoNaoAutorizada) &&
                (status.StatusAgendamento != StatusAgendamento.AguardandoAutorizacao) &&
                (status.StatusAgendamento != StatusAgendamento.AguardandoAprovacao) &&
                (status.StatusAgendamento != StatusAgendamento.CadastroNaoAprovado) &&
                (status.StatusAgendamento != StatusAgendamento.CadastroPreAprovado))
                throw new CoreException("Status do pré cadastro diferente do permitido para notificar solicitante.");

            Dictionary<string, string> valoresSubs = new Dictionary<string, string>();
            valoresSubs.Add("$$Status$$", status.StatusAgendamento.ObterDescricaoEnum());
            valoresSubs.Add("$$InfoComplementar1$$", status.StatusAgendamento == StatusAgendamento.ContratacaoNaoAutorizada ?
                "Informações complementares" : "Lista de pendências");
            valoresSubs.Add("$$InfoComplementar2$$", status.Observacao);

            #region [Informações do Colaborador do Pré Cadastro]

            var u = precadastro.PessoaAg;
            valoresSubs.Add("$$Nome2$$", u.Nome);
            valoresSubs.Add("$$CPFPPassaporte$$", !string.IsNullOrEmpty(u.CPF) ? u.CPFFormatado() : u.Passaporte);
            valoresSubs.Add("$$Empresa2$$", precadastro.Empresa.Nome);
            valoresSubs.Add("$$Cargo$$", precadastro.Cargo.Nome);

            #endregion

            #region [Informações da inclusão da Pessoa ]

            valoresSubs.Add("$$Observacao$$", precadastro.Observacao);
            valoresSubs.Add("$$DataAdmissao$$", precadastro.DataAdmissao.HasValue ? precadastro.DataAdmissao.Value.ToString("dd/MM/yyyy") : "");
            valoresSubs.Add("$$DataInclusao$$", precadastro.DataRegistro.ToString("dd/MM/yyyy"));
            valoresSubs.Add("$$TipoPreCadastro$$", "Colaborador");

            #endregion

            #region [Enviar email]

            if (!string.IsNullOrEmpty(path)) return;

            string AssuntoEmail = "[CONTROLSYS] Status de Pré Cadastro do Colaborador atualizado";
            string Path_htmlEmail = "/Content/templates/preCadastroMotivo.htm";
            
            if (status.StatusAgendamento == StatusAgendamento.CadastroPreAprovado)
            {
                AssuntoEmail = "[CONTROLSYS] Status de Pré Cadastro do Colaborador esta Pré Aprovado.";
                Path_htmlEmail = "/Content/templates/preCadastroPreAprovado.htm";                
            }

            Globalsys.Util.Email.Enviar(AssuntoEmail,
                Server.MapPath(Path_htmlEmail),
                valoresSubs,
                string.Empty,
                null,
                precadastro.Solicitante.PessoaFisica.Email);

            #endregion
        }

        public ActionResult Pesquisar(StoreRequestParameters parameters = null, string nome = null, string cpf = null, string passaporte = null, string codEmpresa = null, string solicitante = null, StatusAgendamento? status = null, DateTime? dtInicio = null, DateTime? dtFim = null)
        {
            IQueryable<ColaboradorAgendamento> preCadColabs = FiltrarRegistros(nome, cpf, passaporte, codEmpresa, solicitante, status, dtInicio, dtFim);

            #region [Filtro de usuário]
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            Usuario usuarioLogado = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name, somenteAtivos: true);
            #endregion

            #region [Filtro Administrador]
            var grupos = usuarioLogado.GruposUsuarios.Select(g => g.Grupo);
            var acoes = grupos.SelectMany(g => g.Acoes);
            //var enumerable = acoes as Acao[] ?? acoes.ToArray();
            var podePreAprovar = (acoes.Where(a => a.Url == "/PreCadastroColaborador/PreAprovar")).Any();
            var podeAprovar = (acoes.Where(a => a.Url == "/PreCadastroColaborador/Aprovar")).Any();
            var podeReprovar = (acoes.Where(a => a.Url == "/PreCadastroColaborador/Reprovar")).Any();
            #endregion

            if (!(podeAprovar && podeReprovar))
            {
                #region [Filtrar empresa]
                var papel = usuarioLogado.PessoaFisica.ObterPapel(true);
                if (papel == null) throw new CoreException("Não foi possível identificar o usuário logado.");

                Empresa empresa = (papel is Colaborador) ? (papel as Colaborador).Empresa :
                                    ((papel is PrestadorServico) ? (papel as PrestadorServico).Empresa : null);
                if (empresa == null) throw new CoreException("Não será possível consultar, visitante não possui empresa relacionada.");
                #endregion

                preCadColabs = preCadColabs.Where(p =>
                    (p.Solicitante != null && p.Solicitante.Codigo == usuarioLogado.Codigo) || (p.Empresa.Codigo == empresa.Codigo) ||
                    (p.GestorPO.Colaborador.PessoaFisica.Codigo == usuarioLogado.PessoaFisica.Codigo)
                );
            }

            int total = preCadColabs.Count();
            List<ColaboradorAgendamentoModelView> data = montaListaObj(preCadColabs, parameters);

            return new StoreResult(data, total);
        }

        private IQueryable<ColaboradorAgendamento> FiltrarRegistros(string nome = null, string cpf = null, string passaporte = null, string empresa = null, string solicitante = null, StatusAgendamento? status = null, DateTime? dtInicio = null, DateTime? dtFim = null)
        {
            IRepositorioPreCadastroColaborador repPreCadastroColab = Fabrica.Instancia.ObterRepositorio<IRepositorioPreCadastroColaborador>(UnidadeTrabalho);
            IQueryable<ColaboradorAgendamento> preCads = repPreCadastroColab.ObterTodos();

            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            Usuario usuarioLogado = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name);

            if (!string.IsNullOrEmpty(nome))
                preCads = preCads.Where(p => p.PessoaAg.Nome.Contains(nome));

            if (!string.IsNullOrEmpty(cpf))
            {
                cpf = cpf.Replace(".", "").Replace("-", "");
                preCads = preCads.Where(p => p.PessoaAg.CPF == cpf);
            }

            if (!string.IsNullOrEmpty(passaporte))
                preCads = preCads.Where(p => p.PessoaAg.Passaporte == passaporte);

            if (!string.IsNullOrEmpty(empresa))
                preCads = preCads.Where(p => p.Empresa.Codigo == empresa);

            if (!string.IsNullOrEmpty(solicitante))
                preCads = preCads.Where(p => p.Solicitante != null && p.Solicitante.PessoaFisica.Nome.Contains(solicitante));

            if (dtInicio.HasValue)
                preCads = preCads.Where(p => p.DataRegistro >= dtInicio.Value);

            if (dtFim.HasValue)
                preCads = preCads.Where(p => p.DataRegistro <= dtFim.Value);

            if (status != null)
                preCads = preCads.Where(p => p.Status.OrderByDescending(s => s.Codigo).Select(s => s.StatusAgendamento).FirstOrDefault() == status);

            if (!usuarioLogado.GruposUsuarios.Any(gu => gu.Grupo.IsAdmin()))
            {
                var grupos = usuarioLogado.GruposUsuarios.Select(g => g.Grupo);
                var acoes = grupos.SelectMany(g => g.Acoes);
                var papel = usuarioLogado.PessoaFisica.ObterPapel(true);
                //PrestadorServico ps = papel != null ? papel as PrestadorServico : null;
                //Colaborador co = papel != null ? papel as Colaborador : null;
                int codigoPapel = papel != null ? papel.Codigo : 0;

                bool podePreAprovar = (acoes.Where(a => a.Url == "/PreCadastroColaborador/PreAprovar")).Any();
                bool podeAprovar = (acoes.Where(a => a.Url == "/PreCadastroColaborador/Aprovar")).Any();
                bool podeReprovar = (acoes.Where(a => a.Url == "/PreCadastroColaborador/Reprovar")).Any();

                //if (!usuarioLogado.GruposUsuarios.Any(gu => gu.Grupo.IsAdmin()))
                //    preCads = preCads.Where(pc => (pc.Solicitante.Codigo == usuarioLogado.Codigo && ((ps != null && ps.Empresa.Codigo == pc.Empresa.Codigo) || (co != null && co.Empresa.Codigo == pc.Empresa.Codigo))) || pc.GestorPO.Colaborador.Codigo == codigoPapel || pc.Solicitante.Codigo == usuarioLogado.Codigo || (podeAprovar && podeReprovar));

                if (papel is Colaborador)
                {
                    preCads = preCads.ToList().Where(pc => pc.GestorPO.Colaborador.Codigo == codigoPapel
                        || (pc.Solicitante != null && pc.Solicitante.Codigo == usuarioLogado.Codigo)
                        || (podeAprovar && podeReprovar)
                        || (pc.Solicitante != null && (papel as Colaborador).Empresa.Codigo == (pc.Solicitante.PessoaFisica != null && pc.Solicitante.PessoaFisica.ObterPapel(true) != null
                                                                        ? pc.Solicitante.PessoaFisica.ObterPapel(true) is Colaborador
                                                                            ? (pc.Solicitante.PessoaFisica.ObterPapel(true) as Colaborador).Empresa.Codigo
                                                                            : (pc.Solicitante.PessoaFisica.ObterPapel(true) as PrestadorServico).Empresa.Codigo
                                                                        : string.Empty))).AsQueryable();
                }
                else if (papel is PrestadorServico)
                {
                    preCads = preCads.ToList().Where(pc => pc.GestorPO.Colaborador.Codigo == codigoPapel
                        || (pc.Solicitante != null && pc.Solicitante.Codigo == usuarioLogado.Codigo)
                        || (podeAprovar && podeReprovar)
                        || (pc.Solicitante != null && (papel as PrestadorServico).Empresa.Codigo == (pc.Solicitante.PessoaFisica != null && pc.Solicitante.PessoaFisica.ObterPapel(true) != null
                                                                        ? pc.Solicitante.PessoaFisica.ObterPapel(true) is Colaborador
                                                                            ? (pc.Solicitante.PessoaFisica.ObterPapel(true) as Colaborador).Empresa.Codigo
                                                                            : (pc.Solicitante.PessoaFisica.ObterPapel(true) as PrestadorServico).Empresa.Codigo
                                                                        : string.Empty))).AsQueryable();
                }
                else
                {
                    preCads = preCads.Where(pc => pc.GestorPO.Colaborador.Codigo == codigoPapel || (pc.Solicitante != null && pc.Solicitante.Codigo == usuarioLogado.Codigo) || (podeAprovar && podeReprovar));
                }
            }

            return preCads;
        }

        private List<ColaboradorAgendamentoModelView> montaListaObj(IQueryable<ColaboradorAgendamento> preCadColabs, StoreRequestParameters parameters = null)
        {
            if (parameters == null)
                parameters = new StoreRequestParameters();
            preCadColabs = preCadColabs.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 25);

            var lista = new List<ColaboradorAgendamentoModelView>();
            foreach (var p in preCadColabs)
            {
                var newReg = new ColaboradorAgendamentoModelView();
                newReg.Codigo = p.Codigo;
                newReg.CPF = !string.IsNullOrEmpty(p.PessoaAg.CPF) ? p.PessoaAg.CPFFormatado() : p.PessoaAg.Passaporte;
                newReg.Nome = p.PessoaAg.Nome;
                newReg.EmpresaDesc = p.Empresa.Nome;
                newReg.CargoDesc = p.Cargo.Nome;
                newReg.StatusAgendamento = p.ObterStatusAtual() != null ? p.ObterStatusAtual().StatusAgendamento.ObterDescricaoEnum() : null;
                newReg.DataRegistro = p.DataRegistro;
                lista.Add(newReg);
            }

            return lista;
            //return preCadColabs.ToList().Select(p => new ColaboradorAgendamentoModelView
            //{
            //    Codigo = p.Codigo,
            //    CPF = !string.IsNullOrEmpty(p.PessoaAg.CPF) ? p.PessoaAg.CPFFormatado() : p.PessoaAg.Passaporte,
            //    Nome = p.PessoaAg.Nome,
            //    EmpresaDesc = p.Empresa.Nome,
            //    CargoDesc = p.Cargo.Nome,
            //    StatusAgendamento = p.ObterStatusAtual() != null ? p.ObterStatusAtual().StatusAgendamento.ObterDescricaoEnum() : null
            //}).ToList();
        }

        private ColaboradorAgendamentoModelView montaObj(ColaboradorAgendamento preCad, Usuario usuarioLogado = null)
        {
            var reg = new ColaboradorAgendamentoModelView();

            if (usuarioLogado != null)
            {
                reg.CadGestor = preCad.GestorPO.Colaborador.PessoaFisica.Codigo == usuarioLogado.PessoaFisica.Codigo;

                var grupos = usuarioLogado.GruposUsuarios.Select(g => g.Grupo);
                var acoes = grupos.SelectMany(g => g.Acoes);
                //var enumerable = acoes as Acao[] ?? acoes.ToArray();
                reg.PodeSalvarStatusPreAprovado = (preCad.ObterStatusAtual().StatusAgendamento != StatusAgendamento.CadastroPreAprovado) || (acoes.Where(a => a.Url == "/PreCadastroColaborador/PreAprovar").Any());
                reg.PodePreAprovar = (acoes.Where(a => a.Url == "/PreCadastroColaborador/PreAprovar")).Any();
                reg.PodeAprovar = (acoes.Where(a => a.Url == "/PreCadastroColaborador/Aprovar")).Any();
                reg.PodeReprovar = (acoes.Where(a => a.Url == "/PreCadastroColaborador/Reprovar")).Any();
            }

            reg.PodeAlterarGestor = preCad.ObterStatusAtual().StatusAgendamento != StatusAgendamento.CadastroAprovado;
            reg.PedidoCompra = preCad.GestorPO.Pedido.Codigo;
            reg.PedidoCompraNome = string.Format("{0,12} {1}", preCad.GestorPO.Pedido.Numero, preCad.GestorPO.Pedido.Nome);
            reg.Gestor = preCad.GestorPO.Colaborador.Codigo;
            reg.GestorNome = preCad.GestorPO.Colaborador.PessoaFisica.Nome;
            var status = preCad.ObterStatusAtual();
            reg.StatusAgendamento = status != null ? status.StatusAgendamento.ObterDescricaoEnum() : null;
            reg.DataRegistro = preCad.DataRegistro;

            if (status != null && (status.StatusAgendamento == StatusAgendamento.ContratacaoNaoAutorizada ||
                status.StatusAgendamento == StatusAgendamento.CadastroNaoAprovado))
                reg.MotivoNegado = status.Observacao;

            #region [Informações Pessoais]
            reg.Codigo = preCad.Codigo;
            reg.CodigoPessoa = preCad.PessoaAg.Codigo;
            reg.CPF = !string.IsNullOrEmpty(preCad.PessoaAg.CPF) ? preCad.PessoaAg.CPFFormatado() : null;
            reg.Passaporte = preCad.PessoaAg.Passaporte;
            reg.Nome = preCad.PessoaAg.Nome;
            reg.Apelido = preCad.PessoaAg.Apelido;
            reg.Email = preCad.PessoaAg.Email;
            reg.DataNascimento = preCad.PessoaAg.DataNascimento;
            reg.DataVisto = preCad.PessoaAg.DataVisto;
            reg.DataValidadeVisto = preCad.PessoaAg.DataValidadeVisto;
            reg.Sexo = preCad.PessoaAg.Sexo.ObterDescricaoEnum();
            reg.TelefoneNumResidencial = preCad.PessoaAg.TelefoneNumResidencial;
            reg.TelefoneNumEmergencial = preCad.PessoaAg.TelefoneNumEmergencial;
            reg.TelefoneNumCelular = preCad.PessoaAg.TelefoneNumCelular;
            reg.NomeMae = preCad.PessoaAg.NomeMae;
            reg.NomePai = preCad.PessoaAg.NomePai;
            reg.RNE = preCad.PessoaAg.RNE;
            reg.DataExpedicaoRNE = preCad.PessoaAg.DataExpedicaoRNE;
            reg.EstadoCivil = preCad.PessoaAg.EstadoCivil;
            reg.Escolaridade = preCad.PessoaAg.Escolaridade;
            reg.TipoSanguineo = preCad.PessoaAg.TipoSanguineo;
            reg.Nacionalidade = preCad.PessoaAg.Nacionalidade;
            reg.NaturalidadeUF = preCad.PessoaAg.Naturalidade != null && preCad.PessoaAg.Naturalidade.Estado != null ? preCad.PessoaAg.Naturalidade.Estado.Codigo : (int?)null;
            reg.Naturalidade = preCad.PessoaAg.Naturalidade != null ? preCad.PessoaAg.Naturalidade.Codigo : (int?)null;
            reg.Area = preCad.Area != null ? preCad.Area.Codigo : (int?)null;
            #endregion

            #region [RG]
            reg.RG = preCad.PessoaAg.RG;
            reg.RGOrgaoEmissor = preCad.PessoaAg.RGOrgaoEmissor;
            reg.RGOrgaoEmissorUF = preCad.PessoaAg.RGOrgaoEmissorUF != null ? preCad.PessoaAg.RGOrgaoEmissorUF.Codigo : (int?)null;
            reg.RGDataEmissao = preCad.PessoaAg.RGDataEmissao;
            #endregion

            #region [Titulo de Eleitor]
            reg.TituloEleitor = preCad.PessoaAg.TituloEleitor;
            reg.TituloEleitorZona = preCad.PessoaAg.TituloEleitorZona;
            reg.TituloEleitorSecao = preCad.PessoaAg.TituloEleitorSecao;
            reg.TituloEleitorCidade = preCad.PessoaAg.TituloEleitorCidade != null ? preCad.PessoaAg.TituloEleitorCidade.Codigo : (int?)null;
            reg.TituloEleitorEstado = (preCad.PessoaAg.TituloEleitorCidade != null && preCad.PessoaAg.TituloEleitorCidade.Estado != null) ? preCad.PessoaAg.TituloEleitorCidade.Estado.Codigo : (int?)null;
            #endregion

            #region [CTPS]
            reg.CTPS = preCad.PessoaAg.CTPS;
            reg.CTPSSerie = preCad.PessoaAg.CTPSSerie;
            reg.CTPSEstado = preCad.PessoaAg.CTPSEstado != null ? preCad.PessoaAg.CTPSEstado.Codigo : (int?)null;
            reg.CTPSData = preCad.PessoaAg.CTPSData;
            #endregion

            #region [CNH]
            reg.CNH = preCad.PessoaAg.CNH;
            reg.CNHCategoria = preCad.PessoaAg.CNHCategoria;
            reg.CNHDataValidade = preCad.PessoaAg.CNHDataValidade;
            #endregion

            #region [Certificado Reservista]
            reg.CertificadoReservista = preCad.PessoaAg.CertificadoReservista;
            reg.CertificadoReservistaCat = preCad.PessoaAg.CertificadoReservistaCat;
            #endregion

            #region [Endereço]
            var endereco = preCad.PessoaAg.Endereco;
            reg.Endereco = new EnderecoMovelView()
            {
                CodigoEndereco = endereco.Codigo,
                CEP = endereco.CEP,
                Logradouro = endereco.Nome,
                Bairro = endereco.Bairro.Nome,
                Cidade = endereco.Bairro.Cidade.Nome,
                Estado = endereco.Bairro.Cidade.Estado.Nome,
                Pais = endereco.Bairro.Cidade.Estado.Pais.Nome,
                Numero = preCad.PessoaAg.EnderecoNumero,
                Complemento = preCad.PessoaAg.EnderecoComplemento
            };
            #endregion

            if (preCad.EmpregadoSap != null)
            {
                reg.EmpregadoSAP = preCad.EmpregadoSap.Codigo;
                reg.GestorPonto = preCad.EmpregadoSap.GestorPonto != null ? preCad.EmpregadoSap.GestorPonto.Codigo : (long?) null;
                reg.SuperiorImediato = preCad.EmpregadoSap.SuperiorImediato != null ? preCad.EmpregadoSap.SuperiorImediato.Codigo : (long?)null;
            }

            reg.PIS = preCad.PIS;
            reg.ConselhoProfissional = preCad.ConselhoProfissional;
            reg.RegistroProfissional = preCad.RegistroProfissional;
            reg.Observacao = preCad.Observacao;
            reg.Empresa = new EmpresaModelView() { Codigo = preCad.Empresa.Codigo, Nome = preCad.Empresa.Nome +" (" + preCad.Empresa.CNPJ + ")" }; 
            //reg.Empresa = preCad.Empresa.Codigo;
            reg.EmpresaDesc = preCad.Empresa.Nome;
            reg.DataAdmissao = preCad.DataAdmissao;
            reg.HoristaMensalista = preCad.HoristaMensalista;
            reg.Cargo = preCad.Cargo.Codigo;
            reg.CargoDesc = preCad.Cargo.Nome;
            reg.Salario = preCad.Salario;

            #region Documentos
            reg.Documentos =
                preCad.Documentos.Select(d => new DocumentoPapelModelView()
                {
                    CodigoDocumento = d.Documento.Codigo,
                    Codigo = d.Documento.TipoDocumento.Codigo,
                    Sigla = d.Documento.TipoDocumento.Sigla,
                    DataValidade = d.Documento.DataValidade,
                    Descricao = d.Documento.Descricao,
                    QtdMidias = d.Documento.Midias.Count(m => m.Ativo),
                    Status = EnumExtensoes.ObterDescricaoEnum(d.Documento.Status),
                    Midias = d.Documento.Midias.Select(dm => new MidiaModelView()
                    {
                        CodigoMidia = dm.Midia.Codigo,
                        Arquivo = string.Empty,
                        DataRegistro = dm.Midia.DataRegistro,
                        Extensao = dm.Midia.Extensao,
                        MIME = dm.Midia.MimeType,
                        NomeDisco = dm.Midia.NomeArquivo,
                        NomeOriginal = string.Empty,
                        URL = dm.Midia.Url
                    }).ToList()

                }).ToList();

            #endregion

            #region [Histórico Status]
            reg.HistoricoStatus = preCad.Status.Select(h => new StatusPapelAgendamentoModelView()
            {
                Codigo = h.Codigo,
                Data = h.DataRegistro,
                Status = h.StatusAgendamento.ObterDescricaoEnum(),
                Observacao = h.Observacao
            }).ToList();
            #endregion

            reg.GrupoTrabalho = preCad.GrupoTrabalho == null ? null : preCad.GrupoTrabalho.Nome;
            reg.SolicitanteNome = preCad.Solicitante == null ? null : preCad.Solicitante.PessoaFisica.Nome;
            return reg;
        }

    }
}