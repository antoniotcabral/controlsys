﻿using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.AgendamentosPessoa;
using Controlsys.Dominio.Enderecos;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Controlsys.Dominio.Seguranca;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Acesso;
using Controlsys.Repositorio.Agendamentos;
using Controlsys.Repositorio.Parametros;
using Controlsys.Repositorios.Acesso;
using Controlsys.Repositorios.Empresas;
using Controlsys.Repositorios.Enderecos;
using Controlsys.Repositorios.Parametros;
using Controlsys.Repositorios.Pessoas;
using Controlsys.Repositorios.Seguranca;
using Ext.Net;
using Ext.Net.MVC;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar prestador servicoes.
    /// </summary>
    [ExtendController]
    public class PrestadorServicoController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /PrestadorServico/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.PrestadorServicoController.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public PrestadorServicoController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        /// <summary>
        /// Retorna a página de prestadorServico.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///  <param name="codPreCad">
        ///  Código do Pré Cadastro
        ///  </param>
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index(int? codigo, int? codPreCad)
        {
            if (codigo.HasValue)
                X.GetCmp<Hidden>("recordId").SetValue(codigo);

            if (codPreCad.HasValue)
                return View("Index", model: codPreCad.ToString());

            return View();
        }

        /// <summary>
        /// Inclui um novo prestadorServico.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="prestador">
        /// O(a) prestador.
        /// </param>
        /// <param name="propModificadasPessoa">
        /// O(a) property modificadas pessoa.
        /// </param>
        /// <param name="treinamentos">
        /// O(a) treinamentos.
        /// </param>
        /// <param name="documentos">
        /// O(a) documentos.
        /// </param>
        /// <param name="imagem">
        /// O(a) imagem.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Incluir(PrestadorServico prestador, string[] propModificadasPessoa, List<TreinamentoPessoa> treinamentos, List<DocumentoMidiaModelView> documentos, string imagem, int? codPreCad)
        {
            IRepositorioGrupo repGrupo = null;
            IRepositorioParametro repParam = null;

            try
            {
                UnidadeTrabalho.BeginTransaction();
                //repositorios
                IRepositorioEmpresa repEmpresa = Fabrica.Instancia.ObterRepositorio<IRepositorioEmpresa>(UnidadeTrabalho);
                IRepositorioPrestadorServico repPrestador = Fabrica.Instancia.ObterRepositorio<IRepositorioPrestadorServico>(UnidadeTrabalho);
                IRepositorioPessoaFisica repPessoaFisica = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaFisica>(UnidadeTrabalho);
                IRepositorioEndereco repEndereco = Fabrica.Instancia.ObterRepositorio<IRepositorioEndereco>(UnidadeTrabalho);
                IRepositorioDocumento repDoc = Fabrica.Instancia.ObterRepositorio<IRepositorioDocumento>(UnidadeTrabalho);
                IRepositorioTreinamento repTreinamento = Fabrica.Instancia.ObterRepositorio<IRepositorioTreinamento>(UnidadeTrabalho);
                IRepositorioColaborador repColab = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);
                IRepositorioPapel repPapel = Fabrica.Instancia.ObterRepositorio<IRepositorioPapel>(UnidadeTrabalho);
                IRepositorioSincronizaPessoaAcesso repSincronizaPessoaAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioSincronizaPessoaAcesso>(UnidadeTrabalho);
                IRepositorioPreCadastroPrestador repPreCad = Fabrica.Instancia.ObterRepositorio<IRepositorioPreCadastroPrestador>(UnidadeTrabalho);
                IRepositorioFornecedor repFornecedor = Fabrica.Instancia.ObterRepositorio<IRepositorioFornecedor>(UnidadeTrabalho);
                repGrupo = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupo>(UnidadeTrabalho);
                repParam = Fabrica.Instancia.ObterRepositorio<IRepositorioParametro>(UnidadeTrabalho);

                var preCad = new PrestadorServicoAgendamento();
                PessoaFisica pessoaFisica = prestador.PessoaFisica;

                if (codPreCad.HasValue)
                {
                    preCad = repPreCad.ObterPorId(codPreCad);
                    repPreCad.Validar(EstadoObjeto.Alterado, preCad);

                    var statusAtual = preCad.ObterStatusAtual().StatusAgendamento;
                    if (statusAtual != StatusAgendamento.AguardandoAprovacao && statusAtual != StatusAgendamento.CadastroNaoAprovado &&
                        !(preCad.Status.Any(s => s.StatusAgendamento == StatusAgendamento.ContratacaoAutorizada)))
                        throw new CoreException("Não foi possível aprovar este pré cadastro, ele precisa primeiro ser autorizado pelo gestor.");

                    PessoaFisica pfAux = repPessoaFisica.ObterPorCpfPassaporte(false, preCad.PessoaAg.CPF);
                    if (pfAux != null)
                    {
                        if (prestador.PessoaFisica == null)
                            prestador.PessoaFisica = pfAux;
                        if (string.IsNullOrEmpty(prestador.PessoaFisica.Codigo))
                            prestador.PessoaFisica.Codigo = pfAux.Codigo;
                    }
                }

                prestador.Empresa = repEmpresa.ObterTodos().Where(e => e.Codigo.Equals(prestador.Empresa.Codigo) || e.Nome.Equals(prestador.Empresa.Nome)).FirstOrDefault();
                Endereco end = prestador.PessoaFisica.Endereco;
                if (preCad.Codigo > 0)
                {
                    if (preCad.Fornecedor != null)
                        prestador.Fornecedor = preCad.Fornecedor;
                }

                //foto
                if (!string.IsNullOrEmpty(imagem))
                    pessoaFisica.Foto = Convert.FromBase64String(imagem);

                if (prestador.PessoaFisica.CPF != null)
                    prestador.PessoaFisica.CPF = prestador.PessoaFisica.CPF.Replace("-", "").Replace(".", "");

                if (!string.IsNullOrEmpty(pessoaFisica.Codigo))
                {
                    pessoaFisica = repPessoaFisica.ObterPorId(pessoaFisica, propModificadasPessoa);
                    repPessoaFisica.Atualizar(pessoaFisica);
                }


                #region [ENDEREÇO]
                if (end != null)
                {
                    pessoaFisica.Endereco = repEndereco.VerificarEndereco(end);
                    pessoaFisica.Endereco.DataRegistro = DateTime.Now;
                    pessoaFisica.Endereco.Ativar();
                    repEndereco.Salvar(pessoaFisica.Endereco);
                }
                #endregion

                //validações referentes à pessoa
                if (string.IsNullOrEmpty(pessoaFisica.Codigo))
                {
                    if (prestador.TipoPrestador == TipoPrestador.Temporario)
                        repPessoaFisica.Salvar(pessoaFisica, null);
                    else
                    {
                        repPessoaFisica.Salvar(pessoaFisica, prestador);
                    }
                }

                if (propModificadasPessoa != null)
                {
                    pessoaFisica = insereTelefone(propModificadasPessoa, pessoaFisica, prestador);
                }

                repPrestador.Validar(EstadoObjeto.Novo, prestador);

                //cadastrar papel
                Papel papel = repPapel.SalvarPapelLog(prestador.InserirPapelLog(DateTime.Now, null, StatusPapelLog.Ativo));

                prestador.Solicitante = repColab.ObterPorId(prestador.Solicitante.Codigo);
                repPrestador.Salvar(prestador);

                //cadastrar asos
                foreach (var item in prestador.ASOs)
                {
                    repPapel.SalvarAso(item, prestador);
                    repSincronizaPessoaAcesso.Sincronizar(prestador, TipoSincronizacaoPessoa.Conceder, null);
                }

                //cadastrar treinamentos
                if (treinamentos != null)
                {
                    foreach (var item in treinamentos)
                        repTreinamento.SalvarTreinamentoPessoa(prestador, item);
                }

                //cadastrar documentos
                foreach (var item in documentos)
                {
                    // Se Documentos com status completo não tiverem midia gera erro.
                    if (item.Documento.Status == StatusDocumento.Completo && (item.Midias == null || !item.Midias.Any(m => m != null)))
                    {
                        throw new CoreException("Não é possível salvar documento com status 'Completo' sem mídia anexada.");
                    }

                    DocumentoPapel docPapel = repDoc.SalvarDocPapel(papel, item.Documento);
                    if (item.Documento.Codigo <= 0)
                    {
                        string path = ConfigurationManager.AppSettings.Get("pathUploads");

                        if (!System.IO.Directory.Exists(path))
                            throw new CoreException("O diretório onde a midia seria armazanada não está acessivel.");

                        IRepositorioDocumentoMidia repDocumentoMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioDocumentoMidia>(UnidadeTrabalho);
                        IRepositorioMidia repMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioMidia>(UnidadeTrabalho);

                        int cont = 1;

                        foreach (MidiaModelView midia in item.Midias ?? Enumerable.Empty<MidiaModelView>())
                        {
                            if (midia == null)
                                continue;
                            string nomeDisco = "PF-" + (string.IsNullOrEmpty(prestador.PessoaFisica.CPF) ? prestador.PessoaFisica.Passaporte : prestador.PessoaFisica.CPF) + "-" + prestador.Codigo.ToString() + "-" + docPapel.Documento.TipoDocumento.Sigla + "-" + cont + "." + midia.Extensao;
                            Midia m = new Midia();
                            m.DataRegistro = midia.DataRegistro;
                            m.Extensao = midia.Extensao;
                            m.MimeType = midia.MIME;
                            m.NomeArquivo = nomeDisco;
                            repMidia.Salvar(m);

                            repDocumentoMidia.SalvarDocMidia(docPapel.Documento, m);

                            System.IO.File.WriteAllBytes(m.Url, Convert.FromBase64String(midia.Arquivo));

                            cont++;
                        }
                    }
                }

                if (codPreCad.HasValue)
                {
                    preCad.Papel = papel;
                    repPreCad.Atualizar(preCad);

                    var repPapelAg = Fabrica.Instancia.ObterRepositorio<IRepositorioPapelAgendamento>(UnidadeTrabalho);
                    repPapelAg.SalvarStatusPapelAg(preCad.InserirPapelAg(StatusAgendamento.CadastroAprovado, null));
                }

                UnidadeTrabalho.Commit();
                return Json(montaObj(prestador));
            }
            catch (CoreException msg)
            {
                string msgCadastroPessoa = repParam.RecuperarParametro(ParametroSistema.MensagemCadastroPessoaFisica).Valor;
                if (msg.Message == msgCadastroPessoa)
                {
                    Grupo g = repGrupo.ObterPorId(int.Parse(repParam.RecuperarParametro(ParametroSistema.GrupoUsuarioResponsavelPessoaCritica).Valor));
                    string msgEmailGrupoUsuario = repParam.RecuperarParametro(ParametroSistema.MensagemEmailNotificaGrupoCadastroPessoaCritica).Valor;

                    Dictionary<string, string> valoresSubs = new Dictionary<string, string>();

                    IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
                    string userLogado = HttpContext.User.Identity.Name;
                    Usuario u = repUsuario.ObterPorLogin(userLogado);

                    IRepositorioPessoaCritica repPC = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaCritica>(UnidadeTrabalho);
                    PessoaCritica pc = repPC.ObterPorCPF(prestador.PessoaFisica.CPF, true);

                    //Informações da Pessoa que está cadastrando
                    valoresSubs.Add("$$Nome$$", u.PessoaFisica.Nome);
                    valoresSubs.Add("$$Empresa$$", (u.PessoaFisica.ObterPapel(true) is Colaborador ? (u.PessoaFisica.ObterPapel() as Colaborador).Empresa.Nome : ""));
                    valoresSubs.Add("$$Funcao$$", (u.PessoaFisica.ObterPapel(true) is Colaborador ? (u.PessoaFisica.ObterPapel() as Colaborador).Cargo.Nome : ""));

                    //Informações da Pessoa que está cadastrando
                    valoresSubs.Add("$$Nome2$$", prestador.PessoaFisica.Nome);
                    valoresSubs.Add("$$CPF$$", prestador.PessoaFisica.CPFFormatado());

                    //Informações da inclusão da Pessoa 
                    valoresSubs.Add("$$Motivo$$", (!string.IsNullOrEmpty(pc.MotivoBloqueio.Nome)) ? pc.MotivoBloqueio.Nome : "");
                    valoresSubs.Add("$$Observacao$$", (!string.IsNullOrEmpty(pc.Observacao)) ? pc.Observacao : "");
                    valoresSubs.Add("$$DataInclusao$$", pc.DataBloqueio.ToString("dd/MM/yyyy"));
                    valoresSubs.Add("$$Nome3$$", pc.Responsavel.PessoaFisica.Nome);
                    valoresSubs.Add("$$Funcao2$$", (pc.Responsavel.PessoaFisica.ObterPapel(true) is Colaborador ? (u.PessoaFisica.ObterPapel() as Colaborador).Cargo.Nome : ""));


                    valoresSubs.Add("$$Msg$$", msgEmailGrupoUsuario);

                    foreach (var item in g.GruposUsuarios.Where(t => t.Ativo))
                    {
                        Globalsys.Util.Email.Enviar("[CONTROLSYS] Pessoa Crítica",
                                                      Server.MapPath("/Content/templates/pessoaCritica.htm"),
                                                      valoresSubs,
                                                      string.Empty,
                                                      null,
                                                      item.Usuario.PessoaFisica.Email);
                    }
                }
                UnidadeTrabalho.Rollback();

                throw msg;
            }
            finally
            {
                UnidadeTrabalho.Dispose();
            }
        }

        /// <summary>
        /// Altera um registro de prestadorServico.
        /// </summary>
        ///
        /// <param name="prestador">
        /// O(a) prestador.
        /// </param>
        /// <param name="propModificadasPrest">
        /// O(a) property modificadas prest.
        /// </param>
        /// <param name="propModificadasDadosPrest">
        /// O(a) property modificadas dados prest.
        /// </param>
        /// <param name="documentos">
        /// O(a) documentos.
        /// </param>
        /// <param name="treinamentoDel">
        /// O(a) treinamento delete.
        /// </param>
        /// <param name="treinamentoNew">
        /// O(a) treinamento new.
        /// </param>
        /// <param name="asosDel">
        /// O(a) asos delete.
        /// </param>
        /// <param name="asosNew">
        /// O(a) asos new.
        /// </param>
        /// <param name="imagem">
        /// O(a) imagem.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Alterar(PrestadorServico prestador, string[] propModificadasPrest, string[] propModificadasDadosPrest, List<DocumentoMidiaModelView> documentosMidias, TreinamentoPessoa[] treinamentoDel, TreinamentoPessoa[] treinamentoNew, ASO[] asosDel, ASO[] asosNew, string imagem)
        {
            IRepositorioPrestadorServico repPrest = Fabrica.Instancia.ObterRepositorio<IRepositorioPrestadorServico>(UnidadeTrabalho);
            IRepositorioPessoaFisica repPessoaFisica = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaFisica>(UnidadeTrabalho);
            IRepositorioDocumento repDoc = Fabrica.Instancia.ObterRepositorio<IRepositorioDocumento>(UnidadeTrabalho);
            IRepositorioTreinamento repTrein = Fabrica.Instancia.ObterRepositorio<IRepositorioTreinamento>(UnidadeTrabalho);
            IRepositorioEndereco repEndereco = Fabrica.Instancia.ObterRepositorio<IRepositorioEndereco>(UnidadeTrabalho);
            IRepositorioTelefone repTelefone = Fabrica.Instancia.ObterRepositorio<IRepositorioTelefone>(UnidadeTrabalho);
            IRepositorioPapel repPapel = Fabrica.Instancia.ObterRepositorio<IRepositorioPapel>(UnidadeTrabalho);
            IRepositorioColaborador repColaborador = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);
            IRepositorioSincronizaPessoaAcesso repSincronizaPessoaAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioSincronizaPessoaAcesso>(UnidadeTrabalho);
            IRepositorioDocumentoMidia repDocumentoMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioDocumentoMidia>(UnidadeTrabalho);
            IRepositorioMidia repMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioMidia>(UnidadeTrabalho);
            IRepositorioEmpresa repEmpresa = Fabrica.Instancia.ObterRepositorio<IRepositorioEmpresa>(UnidadeTrabalho);

            List<Telefone> telefones = new List<Telefone>(prestador.PessoaFisica.Telefones);

            //PESSOAFISCA
            PessoaFisica pessoaFisica = prestador.PessoaFisica;
            Endereco enderecoNovo = pessoaFisica.Endereco;

            //if (pessoaFisica.Endereco.Codigo != 0)
            //    repEndereco.Remover(pessoaFisica.Endereco.Codigo);
            pessoaFisica = repPessoaFisica.ObterPorId(pessoaFisica, propModificadasPrest);

            #region [ENDEREÇO]
            if (enderecoNovo != null && !string.IsNullOrEmpty(enderecoNovo.Logradouro.CEP))
            {
                if (pessoaFisica.Endereco != null)
                    repEndereco.Remover(pessoaFisica.Endereco.Codigo);

                //endereço
                pessoaFisica.Endereco = repEndereco.VerificarEndereco(enderecoNovo);
                pessoaFisica.Endereco.DataRegistro = DateTime.Now;
                pessoaFisica.Endereco.Ativar();
                repEndereco.Salvar(pessoaFisica.Endereco);

            }
            #endregion

            //foto
            if (!string.IsNullOrEmpty(imagem))
                pessoaFisica.Foto = Convert.FromBase64String(imagem);

            if (prestador.TipoPrestador == TipoPrestador.Temporario && string.IsNullOrEmpty(pessoaFisica.Codigo))
                repPessoaFisica.Salvar(pessoaFisica, null);
            else if (prestador.TipoPrestador != TipoPrestador.Temporario)
                repPessoaFisica.Validar(EstadoObjeto.Alterado, pessoaFisica, prestador);

            repPessoaFisica.Atualizar(pessoaFisica);

            if (propModificadasPrest != null)
            {
                pessoaFisica = insereTelefone(propModificadasPrest, pessoaFisica, prestador);
            }

            //PRESTADOR
            var Solicitante = prestador.Solicitante = repColaborador.ObterPorId(prestador.Solicitante.Codigo);


            prestador = repPrest.ObterPorId(prestador, propModificadasDadosPrest);

            if (propModificadasDadosPrest.Contains("DataValidade"))
            {
                if (prestador.DataValidade >= DateTime.Today)
                    repSincronizaPessoaAcesso.Sincronizar(prestador, TipoSincronizacaoPessoa.Conceder, null);
                else
                    repSincronizaPessoaAcesso.Sincronizar(prestador, TipoSincronizacaoPessoa.Suspender, null);
            }

            if (prestador.Solicitante != null && prestador.Solicitante.Codigo != Solicitante.Codigo)
            {
                prestador.Solicitante = new Colaborador()
                {
                    Codigo = Solicitante.Codigo,
                    PessoaFisica = new PessoaFisica()
                    {
                        Nome = Solicitante.PessoaFisica.Nome
                    }
                };
            }

            if (propModificadasDadosPrest == null)
                propModificadasDadosPrest = new string[1];


            //ASOS
            if (asosDel != null)
            {
                foreach (ASO aso in new List<ASO>(prestador.ASOs.Where(t => asosDel.Any(tdd => tdd.Codigo == t.Codigo))))
                {
                    prestador.ASOs.Remove(aso);
                    repPapel.RemoverAso(aso);
                }

            }
            if (asosNew != null)
            {
                foreach (var item in asosNew)
                {
                    ASO aso = new ASO();
                    aso.Papel = prestador;
                    aso.DataRealizacao = item.DataRealizacao;
                    aso.DataVencimento = item.DataVencimento;
                    prestador.ASOs.Add(aso);
                    repPapel.SalvarAso(aso, prestador);
                }
            }

            if (asosNew != null || asosDel != null)
            {
                ASO aso1 = prestador.ASOs.OrderByDescending(a => a.Codigo).FirstOrDefault();

                if (aso1.DataVencimento >= DateTime.Today)
                    repSincronizaPessoaAcesso.Sincronizar(prestador, TipoSincronizacaoPessoa.Conceder, null);
                else
                    repSincronizaPessoaAcesso.Sincronizar(prestador, TipoSincronizacaoPessoa.Suspender, null);
            }

            string path = ConfigurationManager.AppSettings.Get("pathUploads");

            //DOCUMENTOS
            if (documentosMidias != null)
            {
                List<DocumentoMidiaModelView> docsPapel = new List<DocumentoMidiaModelView>();
                docsPapel = documentosMidias.Where(dm => prestador.Documentos.Any(cd => dm.Documento.Codigo == cd.Documento.Codigo)).ToList();

                foreach (var item in documentosMidias.Where(dm => prestador.Documentos.Any(cd => dm.Documento.Codigo == cd.Documento.Codigo)))
                {
                    // Se Documentos com status completo não tiverem midia gera erro.
                    if (item.Documento.Status == StatusDocumento.Completo && (item.Midias == null || !item.Midias.Any(m => m != null)))
                    {
                        throw new CoreException("Não é possível salvar documento com status 'Completo' sem mídia anexada.");
                    }

                    Documento doc = repDoc.ObterPorId(item.Documento.Codigo);
                    doc.Status = item.Documento.Status;
                    doc.Descricao = item.Documento.Descricao;
                    doc.DataValidade = item.Documento.DataValidade;
                    repDoc.Atualizar(doc);

                    if (item.Midias == null)
                    {
                        DocumentoMidiaModelView item1 = item;
                        var docMidias =
                            repDocumentoMidia.ObterTodos().Where(dm => dm.Documento.Codigo == item1.Documento.Codigo && dm.Ativo);
                        foreach (var documentoMidia in docMidias)
                        {
                            documentoMidia.Inativar();
                            repDocumentoMidia.Atualizar(documentoMidia);
                        }
                        continue;
                    }
                    else
                    {
                        DocumentoMidiaModelView item1 = item;
                        List<int> codigosMidia = item.Midias.Select(i => i.CodigoMidia).ToList();
                        IQueryable<DocumentoMidia> listaDocMidia = repDocumentoMidia.ObterTodos().Where(dm =>
                            dm.Documento.Codigo == item1.Documento.Codigo &&
                            dm.Ativo && !codigosMidia.Contains(dm.Midia.Codigo));

                        foreach (DocumentoMidia docMidia in listaDocMidia)
                        {
                            docMidia.Inativar();
                            repDocumentoMidia.Salvar(docMidia);
                        }
                    }

                    int cont = repDocumentoMidia.ObterTodos().Where(dm => dm.Documento.Codigo == item.Documento.Codigo).Count() + 1;

                    foreach (MidiaModelView midia in item.Midias.Where(m => m.CodigoMidia == 0))
                    {
                        if (!System.IO.Directory.Exists(path))
                            throw new CoreException("O diretório onde a midia seria armazanada não está acessivel.");

                        string nomeDisco = "PF-" + (string.IsNullOrEmpty(prestador.PessoaFisica.CPF) ? prestador.PessoaFisica.Passaporte : prestador.PessoaFisica.CPF) + "-" + prestador.Codigo.ToString() + "-" + doc.TipoDocumento.Sigla + "-" + cont + "." + midia.Extensao;

                        Midia m = new Midia();
                        m.DataRegistro = midia.DataRegistro;
                        m.Extensao = midia.Extensao;
                        m.MimeType = midia.MIME;
                        m.NomeArquivo = nomeDisco;

                        repMidia.Salvar(m);

                        repDocumentoMidia.SalvarDocMidia(doc, m);
                        System.IO.File.WriteAllBytes(m.Url, Convert.FromBase64String(midia.Arquivo));

                        cont++;

                    }
                }

                foreach (var item in documentosMidias.Where(dm => !prestador.Documentos.Any(cd => dm.Documento.Codigo == cd.Documento.Codigo)))
                {
                    // Se Documentos com status completo não tiverem midia gera erro.
                    if (item.Documento.Status == StatusDocumento.Completo && (item.Midias == null || !item.Midias.Any(m => m != null)))
                    {
                        throw new CoreException("Não é possível salvar documento com status 'Completo' sem mídia anexada.");
                    }

                    DocumentoPapel docPapel = repDoc.SalvarDocPapel(prestador, item.Documento);

                    int cont = repDocumentoMidia.ObterTodos().Where(dm => dm.Documento.Codigo == item.Documento.Codigo).Count() + 1;

                    foreach (MidiaModelView midia in item.Midias ?? Enumerable.Empty<MidiaModelView>())
                    {
                        if (midia == null)
                            continue;

                        if (!System.IO.Directory.Exists(path))
                            throw new CoreException("O diretório onde a midia seria armazanada não está acessivel.");

                        string nomeDisco = "PF-" + (string.IsNullOrEmpty(prestador.PessoaFisica.CPF) ? prestador.PessoaFisica.Passaporte : prestador.PessoaFisica.CPF) + "-" + prestador.Codigo.ToString() + "-" + docPapel.Documento.TipoDocumento.Sigla + "-" + cont + "." + midia.Extensao;
                        Midia m = new Midia();
                        m.DataRegistro = midia.DataRegistro;
                        m.Extensao = midia.Extensao;
                        m.MimeType = midia.MIME;
                        m.NomeArquivo = nomeDisco;

                        repMidia.Salvar(m);

                        repDocumentoMidia.SalvarDocMidia(docPapel.Documento, m);

                        System.IO.File.WriteAllBytes(m.Url, Convert.FromBase64String(midia.Arquivo));

                        cont++;
                    }
                }
            }

            //TREINAMENTO
            if (treinamentoDel != null)
            {
                foreach (var item in prestador.Treinamentos.Where(t => treinamentoDel.Any(tdd => tdd.Treinamento.Codigo == t.Treinamento.Codigo)))
                {
                    TreinamentoPessoa treinamento = item;
                    treinamento.Inativar();
                }
            }

            if (treinamentoNew != null)
            {
                foreach (var item in treinamentoNew)
                {
                    TreinamentoPessoa tp = new TreinamentoPessoa();
                    tp.Papel = prestador;
                    tp.DataRealizacao = item.DataRealizacao;
                    tp.DataValidade = item.DataValidade;
                    repTrein.SalvarTreinamentoPessoa(prestador, item);
                }
            }

            repPrest.Validar(EstadoObjeto.Alterado, prestador);

            return Json(montaObj(prestador: prestador, repositorio: repPrest));
        }

        /// <summary>
        /// Insere telefone.
        /// </summary>
        ///
        /// <param name="propModificadas">
        /// O(a) property modificadas.
        /// </param>
        /// <param name="pessoaFisica">
        /// O(a) pessoa fisica.
        /// </param>
        /// <param name="prestador">
        /// O(a) prestador.
        /// </param>
        ///
        /// <returns>
        /// Um(a) PessoaFisica.
        /// </returns>
        private PessoaFisica insereTelefone(string[] propModificadas, PessoaFisica pessoaFisica, PrestadorServico prestador)
        {
            IRepositorioTelefone repTelefone = Fabrica.Instancia.ObterRepositorio<IRepositorioTelefone>(UnidadeTrabalho);
            List<Telefone> telefones = new List<Telefone>(prestador.PessoaFisica.Telefones);

            if (propModificadas.Any(a => a == "TelefoneNumResidencial"))
            {
                Telefone telefoneResidencial = pessoaFisica.Telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Residencial);

                if (telefoneResidencial == null || (telefoneResidencial.Codigo == 0 && !string.IsNullOrEmpty(telefoneResidencial.TelefoneNum)))
                {
                    telefoneResidencial = new Telefone
                    {
                        DataRegistro = DateTime.Now,
                        Ativo = true,
                        TelefoneNum = (telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Residencial) != null) ? telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Residencial).TelefoneNum : "",
                        TipoTelefone = TipoTelefone.Residencial,
                        Pessoa = pessoaFisica
                    };

                    pessoaFisica.Telefones.Add(telefoneResidencial);
                    repTelefone.Salvar(telefoneResidencial);
                }
                else
                {
                    telefoneResidencial.TelefoneNum = (telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Residencial) != null) ? telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Residencial).TelefoneNum : "";
                    repTelefone.Atualizar(telefoneResidencial);
                }
            }

            if (propModificadas.Any(a => a == "TelefoneNumEmergencial"))
            {
                Telefone telefoneResidencial = pessoaFisica.Telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Emergencial);

                if (telefoneResidencial == null || (telefoneResidencial.Codigo == 0 && !string.IsNullOrEmpty(telefoneResidencial.TelefoneNum)))
                {
                    telefoneResidencial = new Telefone
                    {
                        DataRegistro = DateTime.Now,
                        Ativo = true,
                        TelefoneNum = (telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Emergencial) != null) ? telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Emergencial).TelefoneNum : "",
                        TipoTelefone = TipoTelefone.Emergencial,
                        Pessoa = pessoaFisica
                    };

                    repTelefone.Salvar(telefoneResidencial);
                    pessoaFisica.Telefones.Add(telefoneResidencial);
                }
                else
                {
                    telefoneResidencial.TelefoneNum = (telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Emergencial) != null) ? telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Emergencial).TelefoneNum : "";
                    repTelefone.Atualizar(telefoneResidencial);
                }
            }

            if (propModificadas.Any(a => a == "TelefoneNumCelular"))
            {
                Telefone telefoneResidencial = pessoaFisica.Telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Celular);

                if (telefoneResidencial == null || (telefoneResidencial.Codigo == 0 && !string.IsNullOrEmpty(telefoneResidencial.TelefoneNum)))
                {
                    telefoneResidencial = new Telefone
                    {
                        DataRegistro = DateTime.Now,
                        Ativo = true,
                        TelefoneNum = (telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Celular) != null) ? telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Celular).TelefoneNum : "",
                        TipoTelefone = TipoTelefone.Celular,
                        Pessoa = pessoaFisica
                    };

                    repTelefone.Salvar(telefoneResidencial);
                    pessoaFisica.Telefones.Add(telefoneResidencial);
                }
                else
                {
                    telefoneResidencial.TelefoneNum = (telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Celular) != null) ? telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Celular).TelefoneNum : "";
                    repTelefone.Atualizar(telefoneResidencial);
                }
            }

            return pessoaFisica;
        }

        /// <summary>
        /// Pesquisar.
        /// </summary>
        ///
        /// <param name="parameters">
        /// Options for controlling the operation.
        /// </param>
        /// <param name="status">
        /// O(a) status.
        /// </param>
        /// <param name="cpf">
        /// O(a) cpf.
        /// </param>
        /// <param name="empresa">
        /// O(a) empresa.
        /// </param>
        /// <param name="nome">
        /// O(a) nome.
        /// </param>
        /// <param name="cargo">
        /// O(a) cargo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>        
        public ActionResult Pesquisar(StoreRequestParameters parameters = null, StatusPapelLog? status = null, string cpf = null, string empresa = null, string nome = null, int cargo = 0, string id = null, string passaporte = null, MaoDeObra? maodeobra = null, int fornecedor = 0, string crachaativo = null)
        {
            IQueryable<PrestadorServico> colaboradores = FiltrarPrestador(status, cpf, empresa, nome, cargo, id, passaporte, maodeobra, fornecedor, crachaativo);            
            int total = colaboradores.Count();
            List<PrestadorServicoModelView> data = montaListaObj(colaboradores, parameters);
            return new StoreResult(data, total);
        }

        /// <summary>
        /// Filtrar prestador.
        /// </summary>
        ///
        /// <param name="status">
        /// O(a) status.
        /// </param>
        /// <param name="cpf">
        /// O(a) cpf.
        /// </param>
        /// <param name="empresa">
        /// O(a) empresa.
        /// </param>
        /// <param name="nome">
        /// O(a) nome.
        /// </param>
        /// <param name="cargo">
        /// O(a) cargo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) IQueryable&lt;PrestadorServico&gt;
        /// </returns>        
        private IQueryable<PrestadorServico> FiltrarPrestador(StatusPapelLog? status = null, string cpf = null, string empresa = null, string nome = null, int cargo = 0, string id = null, string passaporte = null, MaoDeObra? maodeobra = null, int fornecedor = 0, string crachaativo = null)
        {
            IRepositorioPrestadorServico rep = Fabrica.Instancia.ObterRepositorio<IRepositorioPrestadorServico>(UnidadeTrabalho);
            IQueryable<PrestadorServico> prestador = rep.ObterTodos();

            if (!string.IsNullOrEmpty(id))
                prestador = prestador.Where(c => c.PessoaFisica.Codigo == id);

            if (status.HasValue)
                prestador = prestador.Where(p => p.PapelLogs.OrderByDescending(x => x.Codigo).Select(m => m.Status).FirstOrDefault() == status);
            if (!string.IsNullOrEmpty(nome))
                prestador = prestador.Where(p => p.PessoaFisica.Nome.Contains(nome));
            if (!string.IsNullOrEmpty(cpf))
            {
                cpf = cpf.Replace(".", "").Replace("-", "");
                prestador = prestador.Where(p => p.PessoaFisica.CPF == cpf);
            }
            if (!string.IsNullOrEmpty(empresa))
                prestador = prestador.Where(p => p.Empresa.Codigo == empresa);
            if (cargo > 0)
                prestador = prestador.Where(p => p.Cargo.Codigo == cargo);
            if (!string.IsNullOrEmpty(passaporte))
                prestador = prestador.Where(p => p.PessoaFisica.Passaporte == passaporte);

            if (maodeobra != null)
                prestador = prestador.Where(p => p.MaoDeObra == maodeobra);

            if (fornecedor > 0)
                    prestador = prestador.Where(p => p.Fornecedor.Codigo == fornecedor);            

            if (!string.IsNullOrEmpty(crachaativo))
            {
                IRepositorioCracha repCracha = Fabrica.Instancia.ObterRepositorio<IRepositorioCracha>(UnidadeTrabalho);

                var cracha = repCracha.ObterTodos().Where(x => x.Numero == crachaativo && x.Ativo == true).FirstOrDefault(); 

                if (cracha != null)
                    prestador = prestador.Where(p => p.Codigo == cracha.Papel.Codigo);
                else
                    prestador = prestador.Where(p => p.Codigo == 0);
            }

            return prestador.OrderBy(p => p.PessoaFisica.Nome);
        }

        /// <summary>
        /// Monta lista object.
        /// </summary>
        ///
        /// <param name="regs">
        /// O(a) regs.
        /// </param>
        /// <param name="parameters">
        /// Options for controlling the operation.
        /// </param>
        /// <param name="ignoreParameters">
        /// true to ignore parameters.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;PrestadorServicoModelView&gt;
        /// </returns>
        private List<PrestadorServicoModelView> montaListaObj(IQueryable<PrestadorServico> regs, StoreRequestParameters parameters = null, bool ignoreParameters = false)
        {
            if (!ignoreParameters)
            {
                if (parameters == null)
                    parameters = new StoreRequestParameters();
                regs = regs.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 25);
            }

            return regs.ToList().Select(s => new PrestadorServicoModelView()
            {
                Codigo = s.Codigo,
                CodigoPessoa = s.PessoaFisica.Codigo,
                Nome = s.PessoaFisica.Nome,
                CPF = s.PessoaFisica.CPFFormatado(),
                Passaporte = s.PessoaFisica.Passaporte,
                //Empresa = s.Empresa.Nome,
                EmpresaFornecedor = s.TipoPrestador == TipoPrestador.Temporario ?
                    (string.IsNullOrEmpty(s.Empresa.Apelido) ? s.Empresa.Nome : s.Empresa.Apelido) :
                    (string.IsNullOrEmpty(s.Fornecedor.NomeFantasia) ? s.Fornecedor.RazaoSocial : s.Fornecedor.NomeFantasia),
                Cargo = s.Cargo.Codigo,
                CargoDesc = s.Cargo.Nome,
                CodigoCracha = obterCodigoCracha(s.PapelLogs[0]),
                StatusPapel = obterDesc(s.ObterLogPapel().Status),
                MaoDeObra = (Int32)s.MaoDeObra,
                IdUsuario = s.ObterLogPapel().Papel.IdUsuario
            }).ToList();
        }


        private string obterCodigoCracha(PapelLog cracha)
        {
            IRepositorioCracha repCracha = Fabrica.Instancia.ObterRepositorio<IRepositorioCracha>(UnidadeTrabalho);
            var listCracha = repCracha.ObterTodos().Where(x => x.Papel.Codigo == cracha.Papel.Codigo);
            if (listCracha.Count() > 0)
                return listCracha.OrderByDescending(x => x.DataRegistro).First().Numero.ToString();
            else return null;
        }

        /// <summary>
        /// Cria um objeto contendo informações relacionadas ao conteudo gerenciado por
        /// PrestadorServicoController.
        /// </summary>
        ///
        /// <param name="prestador">
        /// O(a) prestador.
        /// </param>
        /// <param name="imageUrl">
        /// URL of the image.
        /// </param>
        ///
        /// <returns>
        /// Um(a) object.
        /// </returns>
        private object montaObj(PrestadorServico prestador, string imageUrl = null, IRepositorioPrestadorServico repositorio = null)
        {
            Endereco endereco = prestador.PessoaFisica.Endereco;

            Telefone telefoneRes = prestador.PessoaFisica.Telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Residencial);
            Telefone telefoneEmer = prestador.PessoaFisica.Telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Emergencial);
            Telefone telefoneCel = prestador.PessoaFisica.Telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Celular) != null ? prestador.PessoaFisica.Telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Celular) : null;

            var prestadorModelView = new PrestadorServicoModelView();

            prestadorModelView.CodigoPrestador = prestador.Codigo;
            prestadorModelView.Codigo = prestador.Codigo;

            prestadorModelView.CodigoPessoa = prestador.PessoaFisica.Codigo;
            prestadorModelView.CPF = prestador.PessoaFisica.CPF != null ? prestador.PessoaFisica.CPFFormatado() : null;
            prestadorModelView.Passaporte = prestador.PessoaFisica.Passaporte;
            prestadorModelView.Nome = prestador.PessoaFisica.Nome;
            prestadorModelView.Apelido = prestador.PessoaFisica.Apelido;
            prestadorModelView.Email = prestador.PessoaFisica.Email;
            prestadorModelView.DataNascimento = prestador.PessoaFisica.DataNascimento;
            prestadorModelView.DataVisto = prestador.PessoaFisica.DataVisto;
            prestadorModelView.DataValidadeVisto = prestador.PessoaFisica.DataValidadeVisto;
            prestadorModelView.Sexo = prestador.PessoaFisica.Sexo.ObterDescricaoEnum();
            prestadorModelView.NomeMae = prestador.PessoaFisica.NomeMae;
            prestadorModelView.NomePai = prestador.PessoaFisica.NomePai;
            prestadorModelView.RNE = prestador.PessoaFisica.RNE;
            prestadorModelView.DataExpedicaoRNE = prestador.PessoaFisica.DataExpedicaoRNE;
            prestadorModelView.EstadoCivil = prestador.PessoaFisica.EstadoCivil;
            prestadorModelView.Escolaridade = prestador.PessoaFisica.Escolaridade;
            prestadorModelView.TipoSanguineo = prestador.PessoaFisica.TipoSanguineo;

            if (prestador.PessoaFisica.Foto != null)
                prestadorModelView.Img = Convert.ToBase64String(prestador.PessoaFisica.Foto);

            prestadorModelView.Nacionalidade = prestador.PessoaFisica.Nacionalidade;
            prestadorModelView.NaturalidadeUF = prestador.PessoaFisica.Naturalidade != null && prestador.PessoaFisica.Naturalidade.Estado != null ? prestador.PessoaFisica.Naturalidade.Estado.Codigo : (int?)null;
            prestadorModelView.Naturalidade = prestador.PessoaFisica.Naturalidade != null ? prestador.PessoaFisica.Naturalidade.Codigo : (int?)null;

            //RG
            prestadorModelView.RG = prestador.PessoaFisica.RG != null ? prestador.PessoaFisica.RG : null;
            prestadorModelView.RGOrgaoEmissor = prestador.PessoaFisica.RGOrgaoEmissor != null ? prestador.PessoaFisica.RGOrgaoEmissor : null;
            prestadorModelView.RGOrgaoEmissorUF = prestador.PessoaFisica.RGOrgaoEmissorUF != null ? prestador.PessoaFisica.RGOrgaoEmissorUF.Codigo : (int?)null;
            prestadorModelView.RGDataEmissao = prestador.PessoaFisica.RGDataEmissao != null ? prestador.PessoaFisica.RGDataEmissao : (DateTime?)null;

            //Titulo de eleitor
            prestadorModelView.TituloEleitor = prestador.PessoaFisica.TituloEleitor != null ? prestador.PessoaFisica.TituloEleitor : (int?)null;
            prestadorModelView.TituloEleitorSecao = prestador.PessoaFisica.TituloEleitorSecao != null ? prestador.PessoaFisica.TituloEleitorSecao : (int?)null;
            prestadorModelView.TituloEleitorZona = prestador.PessoaFisica.TituloEleitorZona != null ? prestador.PessoaFisica.TituloEleitorZona : (int?)null;
            prestadorModelView.TituloEleitorCidade = prestador.PessoaFisica.TituloEleitorCidade != null ? prestador.PessoaFisica.TituloEleitorCidade.Codigo : (int?)null;
            prestadorModelView.TituloEleitorEstado = (prestador.PessoaFisica.TituloEleitorCidade != null && prestador.PessoaFisica.TituloEleitorCidade.Estado != null) ? prestador.PessoaFisica.TituloEleitorCidade.Estado.Codigo : (int?)null;


            ////CTPS                
            prestadorModelView.CTPS = prestador.PessoaFisica.CTPS != null ? prestador.PessoaFisica.CTPS : (int?)null;
            prestadorModelView.CTPSData = prestador.PessoaFisica.CTPSData != null ? prestador.PessoaFisica.CTPSData : (DateTime?)null;
            prestadorModelView.CTPSSerie = prestador.PessoaFisica.CTPSSerie != null ? prestador.PessoaFisica.CTPSSerie : null;
            prestadorModelView.CTPSEstado = prestador.PessoaFisica.CTPSEstado != null ? prestador.PessoaFisica.CTPSEstado.Codigo : (int?)null;

            ////CNH
            prestadorModelView.CNH = prestador.PessoaFisica.CNH != null ? prestador.PessoaFisica.CNH : null;
            prestadorModelView.CNHCategoria = prestador.PessoaFisica.CNHCategoria != null ? prestador.PessoaFisica.CNHCategoria : null;
            prestadorModelView.CNHDataValidade = prestador.PessoaFisica.CNHDataValidade != null ? prestador.PessoaFisica.CNHDataValidade : (DateTime?)null;

            ////Certificado de Reservista
            prestadorModelView.CertificadoReservista = prestador.PessoaFisica.CertificadoReservista != null ? prestador.PessoaFisica.CertificadoReservista : null;
            prestadorModelView.CertificadoReservistaCat = prestador.PessoaFisica.CertificadoReservistaCat != null ? prestador.PessoaFisica.CertificadoReservistaCat : null;

            ////Telefones
            prestadorModelView.TelefoneNumResidencial = telefoneRes != null ? telefoneRes.TelefoneNum : null;
            prestadorModelView.TelefoneNumEmergencial = telefoneEmer != null ? telefoneEmer.TelefoneNum : null;
            prestadorModelView.TelefoneNumCelular = telefoneCel != null ? telefoneCel.TelefoneNum : null;


            if (endereco != null)
            {
                if (!string.IsNullOrEmpty(endereco.Complemento) && endereco.Logradouro == null)
                {
                    prestadorModelView.Endereco = new EnderecoMovelView()
                    {
                        CodigoEndereco = endereco.Codigo,
                        Complemento = endereco.Complemento
                    };
                }
                else
                {
                    prestadorModelView.Endereco = new EnderecoMovelView()
                    {
                        CodigoEndereco = endereco.Codigo,
                        CodigoLog = endereco.Logradouro.Codigo,
                        Pais = endereco.Logradouro.Bairro.Cidade.Estado.Pais.Nome,
                        Estado = endereco.Logradouro.Bairro.Cidade.Estado.Nome,
                        Cidade = endereco.Logradouro.Bairro.Cidade.Nome,
                        Bairro = endereco.Logradouro.Bairro.Nome,
                        Logradouro = endereco.Logradouro.Nome,
                        Numero = endereco.Numero,
                        Complemento = endereco.Complemento,
                        CEP = endereco.Logradouro.CEP//,

                        //TipoLogradouro = endereco.Logradouro.TipoLogradouro.Nome
                    };
                }
            }
            //dados de prestadorServico
            prestadorModelView.TipoPrestador = obterDescTipoPrestador(prestador.TipoPrestador);

            if (prestador.Solicitante != null)
            {
                Colaborador solic = prestador.Solicitante;
                string solicitante = solic.PessoaFisica.Nome + " / " + solic.PessoaFisica.CPFFormatado() + " / " + solic.Empresa.Apelido;

                prestadorModelView.Solicitante = new ColaboradorModelView()
                {
                    Codigo = prestador.Solicitante.Codigo,
                    Nome = solicitante
                };
            }

            prestadorModelView.AntiPassBack = prestador.AntiPassBack;
            prestadorModelView.PessoaJuridica = prestador.PessoaJuridica;
            prestadorModelView.AcessoPorto = prestador.AcessoPorto;
            prestadorModelView.Observacao = prestador.Observacao;
            prestadorModelView.DataValidade = prestador.DataValidade;
            prestadorModelView.Empresa = prestador.Empresa != null ? new EmpresaModelView() { Codigo = prestador.Empresa.Codigo, Nome = prestador.Empresa.Nome + " (" + prestador.Empresa.CNPJ + ")" } : null;
            prestadorModelView.EmpresaDesc = prestador.Empresa != null ? prestador.Empresa.Nome : null;
            prestadorModelView.Cargo = prestador.Cargo != null ? prestador.Cargo.Codigo : 0;
            prestadorModelView.CargoDesc = prestador.Cargo != null ? prestador.Cargo.Nome : null;
            prestadorModelView.Fornecedor = prestador.Fornecedor != null ? prestador.Fornecedor.RazaoSocial : null;
            //prestadorModelView.EmpresaFornecedor = prestador.Empresa != null ? prestador.Empresa.Nome : prestador.Fornecedor.NomeFantasia;
            prestadorModelView.EmpresaFornecedor = prestador.TipoPrestador == TipoPrestador.Temporario ? prestador.Empresa.Nome : prestador.Fornecedor.NomeFantasia;
            prestadorModelView.DataAdmissao = prestador.DataAdmissao != null ? prestador.DataAdmissao : (DateTime?)null;
            prestadorModelView.StatusPapel = prestador.ObterLogPapel() != null ? prestador.ObterLogPapel().Status.ObterDescricaoEnum() : null;
            prestadorModelView.DataStatusPapel = prestador.ObterLogPapel().DataInicio;
            prestadorModelView.ModeloCracha = prestador.ModeloCracha != null ? prestador.ModeloCracha.Nome : null;
            prestadorModelView.MaoDeObra = (Int32)prestador.MaoDeObra;
            prestadorModelView.GrupoPessoa = (Int32)prestador.GrupoPessoa;

            prestadorModelView.PedidoCompra = prestador.PedidoCompra != null ? prestador.PedidoCompra.Codigo.ToString() : null;
            prestadorModelView.PedidoCompraNumeroPedido = prestador.PedidoCompra != null ? prestador.PedidoCompra.Numero : null;
            prestadorModelView.SetorCusto = prestador.SetorCusto != null ? prestador.SetorCusto.Codigo.ToString() : null;
            prestadorModelView.SetorCustoNome = prestador.SetorCusto != null ? prestador.SetorCusto.Nome : null;
            prestadorModelView.IdUsuario = prestador.IdUsuario != null ? prestador.IdUsuario.Trim() : null;

            //documentos
            prestadorModelView.Documentos = prestador.Documentos.Select(d => new DocumentoPapelModelView()
            {
                CodigoDocumento = d.Documento.Codigo,
                Codigo = d.Documento.TipoDocumento.Codigo,
                Sigla = d.Documento.TipoDocumento.Sigla,
                Descricao = d.Documento.Descricao,
                DataValidade = d.Documento.DataValidade,
                //DiasValidade = d.Documento.TipoDocumento.DiasValidade,
                Status = obterDesc(d.Documento.Status).ToString(),
                QtdMidias = d.Documento.Midias.Count(m => m.Ativo),
                Midias = d.Documento.Midias.Select(dm => new MidiaModelView()
                {
                    CodigoMidia = dm.Midia.Codigo,
                    Arquivo = string.Empty,
                    DataRegistro = dm.Midia.DataRegistro,
                    Extensao = dm.Midia.Extensao,
                    MIME = dm.Midia.MimeType,
                    NomeDisco = dm.Midia.NomeArquivo,
                    NomeOriginal = string.Empty,
                    URL = dm.Midia.Url
                }).ToList()
            }).ToList();

            //asos
            prestadorModelView.Asos = prestador.ASOs.Select(a => new ASO()
            {
                Codigo = a.Codigo,
                DataRealizacao = a.DataRealizacao,
                DataVencimento = a.DataVencimento
            }).ToList();

            //treinamentos
            prestadorModelView.Treinamentos = prestador.Treinamentos.Select(d => new TreinamentoPessoaModelView()
            {
                Codigo = d.Codigo,
                DataRealizacao = d.DataRealizacao,
                DataValidade = d.DataValidade,
                Nome = d.Treinamento.Nome,
                CodigoTreinamento = d.Treinamento.Codigo
            }).ToList();

            prestadorModelView.HistoricoEmpresa = repositorio == null ? ObterHistoricoEmpresa(prestador.PessoaFisica.CPF) :
                repositorio.ObterTodos().Where(p => p.PessoaFisica.CPF == prestador.PessoaFisica.CPF).Select(p => new Empresa()
                {
                    Codigo = p.Empresa.Codigo,
                    CNPJ = p.Empresa.CNPJFormatado(),
                    Nome = p.Empresa.Nome
                }).ToList();

            return prestadorModelView;
        }

        /// <summary>
        /// Selecionar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Selecionar(int codigo)
        {
            IRepositorioPrestadorServico rep = Fabrica.Instancia.ObterRepositorio<IRepositorioPrestadorServico>(UnidadeTrabalho);
            PrestadorServico prestador = rep.ObterPorId(codigo);
            return Json(montaObj(prestador));
        }

        /// <summary>
        /// Retorna todos os objetos do tipo PrestadorServico.
        /// </summary>
        ///
        /// <param name="apenasValidos">
        /// true to apenas validos.
        /// </param>
        ///
        /// <returns>
        /// Detalhes do tipo de objeto ligado ao controller no formato JSON.
        /// </returns>
        public ActionResult ObterTodos(bool apenasValidos = false)
        {
            IRepositorioPrestadorServico rep = Fabrica.Instancia.ObterRepositorio<IRepositorioPrestadorServico>(UnidadeTrabalho);

            IQueryable<PrestadorServico> prestadorServidor = rep.ObterTodos();

            return Json(prestadorServidor.Select(u => new
            {
                u.Codigo,
                CodigoPessoa = u.PessoaFisica.Codigo,
                CPF = u.PessoaFisica.CPF != null ? u.PessoaFisica.CPFFormatado() : null,
                u.PessoaFisica.Passaporte,
                u.PessoaFisica.Nome,
                EmpresaFornecedor = u.TipoPrestador == TipoPrestador.Temporario ? u.Empresa.Nome : u.Fornecedor.NomeFantasia,
                StatusPapel = u.ObterLogPapel() != null ? obterDesc(u.ObterLogPapel().Status) : obterDesc(StatusPapelLog.Ativo)
            }).ToArray().OrderBy(p => p.Nome));
        }

        /// <summary>
        /// Obter description.
        /// </summary>
        ///
        /// <param name="statusLog">
        /// O(a) status log.
        /// </param>
        ///
        /// <returns>
        /// Um(a) object.
        /// </returns>
        private string obterDesc(StatusPapelLog statusLog)
        {
            return EnumExtensoes.ObterDescricaoEnum(statusLog);
        }

        /// <summary>
        /// Obter description.
        /// </summary>
        ///
        /// <param name="maodeobra">
        /// O(a) status log.
        /// </param>
        ///
        /// <returns>
        /// Um(a) object.
        /// </returns>
        private string obterDesc(MaoDeObra maodeobra)
        {
            return EnumExtensoes.ObterDescricaoEnum(maodeobra);
        }

        /// <summary>
        /// Lista tipo prestador.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ListaTipoPrestador()
        {
            Dictionary<int, string> dic = new Dictionary<int, string>();

            foreach (int item in Enum.GetValues(typeof(TipoPrestador)))
            {
                dic.Add(item, ((TipoPrestador)item).ObterDescricaoEnum());
            }
            return Json(dic.Select(d => new
            {
                Codigo = d.Key,
                Descricao = d.Value
            }).ToList());

        }

        /// <summary>
        /// Obter description.
        /// </summary>
        ///
        /// <param name="statusDocumento">
        /// O(a) status documento.
        /// </param>
        ///
        /// <returns>
        /// Um(a) object.
        /// </returns>
        private object obterDesc(StatusDocumento statusDocumento)
        {
            return EnumExtensoes.ObterDescricaoEnum(statusDocumento);
        }

        /// <summary>
        /// Obter description tipo prestador.
        /// </summary>
        ///
        /// <param name="tipoPrestador">
        /// O(a) tipo prestador.
        /// </param>
        ///
        /// <returns>
        /// Um(a) string.
        /// </returns>
        private string obterDescTipoPrestador(TipoPrestador tipoPrestador)
        {
            return EnumExtensoes.ObterDescricaoEnum(tipoPrestador);
        }

        /// <summary>
        /// Obter historico empresa.
        /// </summary>
        ///
        /// <param name="cpf">
        /// O(a) cpf.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;Empresa&gt;
        /// </returns>
        private List<Empresa> ObterHistoricoEmpresa(string cpf)
        {
            IRepositorioPrestadorServico rep = Fabrica.Instancia.ObterRepositorio<IRepositorioPrestadorServico>(UnidadeTrabalho);
            return rep.ObterTodos().Where(p => p.PessoaFisica.CPF == cpf).Select(p => new Empresa()
            {
                Codigo = p.Empresa.Codigo,
                CNPJ = p.Empresa.CNPJFormatado(),
                Nome = p.Empresa.Nome
            }).ToList();
        }

        #region Webcam / Jcrop

        /// <summary>
        /// Edit.
        /// </summary>
        ///
        /// <param name="x">
        /// The x coordinate.
        /// </param>
        /// <param name="y">
        /// The y coordinate.
        /// </param>
        /// <param name="w">
        /// The width.
        /// </param>
        /// <param name="h">
        /// The height.
        /// </param>
        /// <param name="imageUrl">
        /// URL of the image.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [HttpPost]
        public ActionResult Edit(int x, int y, int w, int h, string imageUrl)
        {
            var image = new WebImage("~" + imageUrl);
            var height = image.Height;
            var width = image.Width;

            image.Crop((int)x, (int)y, (int)(height - w), (int)(width - h));

            int indiceDoPonto = imageUrl.IndexOf(".");
            string antesPonto = imageUrl.Substring(0, indiceDoPonto);
            string depoisPonto = imageUrl.Substring(indiceDoPonto, imageUrl.Length - indiceDoPonto);
            string nome = antesPonto + "_crop" + depoisPonto;

            image.Save(Server.MapPath("~" + nome));

            return Json(nome);

        }

        /// <summary>
        /// Upload.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Upload()
        {
            if (Request.ContentLength > 1048576)
                throw new CoreException("Não é possível anexar, o tamanho do arquivo excede 4MB.");

            HttpPostedFileBase postedFile = Request.Files[0];
            string unique = Guid.NewGuid().ToString("N");
            string nomeArquivo = unique + Path.GetExtension(postedFile.FileName);

            const int maxW = 800;
            const int maxH = 600;
            WebImage image = new WebImage(postedFile.InputStream);
            int w = image.Width;
            int h = image.Height;
            if ((w < 128) || (h < 128))
                throw new CoreException("Tamanho da imagem inferior a 128px.");

            if ((w > maxW) || (h > maxH))
            {
                if (w > h)
                {
                    w = maxW;
                    h = image.Height * w / image.Width;
                }
                else
                {
                    h = maxH;
                    w = image.Width * h / image.Height;
                }
            }
            image.Resize(w, h);
            image.Save(Server.MapPath("~/Content/img/webcam/" + nomeArquivo));

            return Json(nomeArquivo);
        }

        /// <summary>
        /// Carregar imagem.
        /// </summary>
        ///
        /// <param name="imagem">
        /// O(a) imagem.
        /// </param>
        ///
        /// <returns>
        /// Um(a) byte[].
        /// </returns>
        private byte[] CarregarImagem(string imagem)
        {
            byte[] imagemBytes = null;
            string caminhoCompletoImagem = imagem;

            int posicao = caminhoCompletoImagem.IndexOf("/Content");
            string caminho = caminhoCompletoImagem.Substring(posicao);
            imagemBytes = System.IO.File.ReadAllBytes(Server.MapPath("~" + caminho));
            return imagemBytes;
        }

        //private string GravarTemporario(byte[] foto)
        //{
        //    if (foto != null)
        //    {
        //        Random randNum = new Random();
        //        int number = randNum.Next(DateTime.Now.Millisecond);

        //        string nomeArquivo = number + ".jpeg";

        //        byte[] b = foto;

        //        using (FileStream fs = new FileStream(Server.MapPath("~/Content/img/webcam/") + nomeArquivo, FileMode.Create))
        //            fs.Write(b, 0, b.Length);

        //        return "/Content/img/webcam/" + nomeArquivo;
        //    }
        //    return "/Content/img/avatar.png";
        //}

        #endregion
    }
}
