﻿using Controlsys.Dominio.Parametros;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Parametros;
using Controlsys.Repositorios.Parametros;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class InfracaoController : Controller, IControllerBase
    {

        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        public InfracaoController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Inclui um novo infracao.
        /// </summary>
        ///
        /// <param name="infracao">
        /// O(a) infracao.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Incluir(Infracao infracao)
        {
            IRepositorioInfracao repInfracao = Fabrica.Instancia.ObterRepositorio<IRepositorioInfracao>(UnidadeTrabalho);
            infracao.DataRegistro = DateTime.Now;
            infracao.Ativar();
            repInfracao.Validar(infracao, EstadoObjeto.Novo);
            repInfracao.Salvar(infracao);
            return Json(new InfracaoModelView { Ativo = infracao.Ativo, Codigo = infracao.Codigo, Grau = infracao.Grau.ObterDescricaoEnum(), Nome = infracao.Nome, Sigla = infracao.Sigla });

        }

        /// <summary>
        /// Altera um registro de infracao.
        /// </summary>
        ///
        /// <param name="infracao">
        /// O(a) infracao.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Alterar(Infracao infracao)
        {
            IRepositorioInfracao repInfracao = Fabrica.Instancia.ObterRepositorio<IRepositorioInfracao>(UnidadeTrabalho);
            Infracao infracaoA = repInfracao.ObterPorId(infracao.Codigo);
            infracaoA.Grau = infracao.Grau;
            infracaoA.Nome = infracao.Nome;
            infracaoA.Sigla = infracao.Sigla;
            repInfracao.Validar(infracaoA, EstadoObjeto.Alterado);
            repInfracao.Atualizar(infracaoA);

            return Json(new InfracaoModelView { Ativo = infracaoA.Ativo, Codigo = infracaoA.Codigo, Grau = infracaoA.Grau.ObterDescricaoEnum(), Nome = infracaoA.Nome, Sigla = infracaoA.Sigla });
        }

        /// <summary>
        /// Ativar inativar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult AtivarInativar(int codigo)
        {
            IRepositorioInfracao repInfracao = Fabrica.Instancia.ObterRepositorio<IRepositorioInfracao>(UnidadeTrabalho);
            Infracao infracao = repInfracao.ObterPorId(codigo);
            if (infracao.Ativo)
            {
                repInfracao.Validar(infracao, EstadoObjeto.Inativado);
                infracao.Inativar();
            }
            else
            {
                repInfracao.Validar(infracao, EstadoObjeto.Ativado);
                infracao.Ativar();
            }
            repInfracao.Atualizar(infracao);
            return Json(new InfracaoModelView()
            {
                Codigo = infracao.Codigo,
                Nome = infracao.Nome,
                Sigla = infracao.Sigla,
                Grau = infracao.Grau.ObterDescricaoEnum(),
                Ativo = infracao.Ativo
            });
            //return Json(infracao);
        }

        /// <summary>
        /// Selecionar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Selecionar(int codigo)
        {
            IRepositorioInfracao repInfracao = Fabrica.Instancia.ObterRepositorio<IRepositorioInfracao>(UnidadeTrabalho);
            Infracao infracao = repInfracao.ObterPorId(codigo);
            return Json(infracao);
        }

        /// <summary>
        /// Retorna todos os objetos do tipo infracao.
        /// </summary>
        ///
        /// <param name="apenasAtivos">
        /// true to apenas ativos.
        /// </param>
        ///
        /// <returns>
        /// Detalhes do tipo de objeto ligado ao controller no formato JSON.
        /// </returns>
        public ActionResult ObterTodos(bool apenasAtivos = false)
        {
            IRepositorioInfracao repInfracao = Fabrica.Instancia.ObterRepositorio<IRepositorioInfracao>(UnidadeTrabalho);

            IQueryable<Infracao> infracoes = repInfracao.ObterTodos();

            if (apenasAtivos)
            {
                infracoes = infracoes.Where(t => t.Ativo == true);
            }

            return Json(infracoes.ToList().Select(i => new InfracaoModelView()
            {
                Codigo = i.Codigo,
                Nome = i.Nome,
                Sigla = i.Sigla,
                Grau = i.Grau.ObterDescricaoEnum(),
                Ativo = i.Ativo
            }).ToList().OrderBy(i => i.Nome));
        }

        public ActionResult ObterGrausInfracao()
        {
            Dictionary<int, string> dic = new Dictionary<int, string>();

            foreach (int item in Enum.GetValues(typeof(GrauInfracao)))
            {
                dic.Add(item, ((GrauInfracao)item).ObterDescricaoEnum());
            }

            return Json(dic.Select(d => new
            {
                Codigo = d.Key,
                Descricao = d.Value
            }).ToList());
        }


    }
}
