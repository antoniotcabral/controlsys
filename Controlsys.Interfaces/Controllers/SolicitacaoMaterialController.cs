﻿using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Controlsys.Dominio.Seguranca;
using Controlsys.Dominio.SolicitacoesMaterial;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Parametros;
using Controlsys.Repositorio.SolicitacoesMaterial;
using Controlsys.Repositorios.Empresas;
using Controlsys.Repositorios.Parametros;
using Controlsys.Repositorios.Pessoas;
using Controlsys.Repositorios.Seguranca;
using Ext.Net;
using Ext.Net.MVC;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web.Mvc;
using System.Web.UI;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class SolicitacaoMaterialController : Controller, IControllerBase
    {
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        public SolicitacaoMaterialController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        public ActionResult Index()
        {
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            Usuario u = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name);
            Papel p = u.PessoaFisica.ObterPapel(true);

            ViewBag.Nome = u.PessoaFisica.Nome;
            ViewBag.Cargo = p != null && p.Cargo != null ? p.Cargo.Nome : string.Empty;

            return View();
        }

        [Transaction]
        public ActionResult Aprovar(int codigoSolicitacao = 0)
        {
            IRepositorioSolicitacaoMaterial repSolicitacaoMaterial = Fabrica.Instancia.ObterRepositorio<IRepositorioSolicitacaoMaterial>(UnidadeTrabalho);
            IRepositorioStatusSolicitacaoMaterial repStatusSolicitacaoMaterial = Fabrica.Instancia.ObterRepositorio<IRepositorioStatusSolicitacaoMaterial>(UnidadeTrabalho);

            SolicitacaoMaterial solicitacao = repSolicitacaoMaterial.ObterPorId(codigoSolicitacao);
            if (solicitacao == null)
                throw new CoreException("Solicitação não encontrada");

            List<StatusSolicitacaoMaterial> listaStatusSolicitacaoMaterial = new List<StatusSolicitacaoMaterial>();
            SituacaoSolicitacao situacaoNova = SituacaoSolicitacao.Aprovada;

            IncluirStatusSolicitacaoMaterial(repStatusSolicitacaoMaterial, solicitacao, listaStatusSolicitacaoMaterial, situacaoNova);
            solicitacao.Status.Concat(listaStatusSolicitacaoMaterial);

            List<SolicitacaoMaterial> listaSolicitacoes = new List<SolicitacaoMaterial>();
            listaSolicitacoes.Add(solicitacao);

            EnviarEmail(solicitacao);

            return Json(MontaObjetos(listaSolicitacoes.AsQueryable(), null, true));
        }

        [Transaction]
        public ActionResult Imprimir(int codigoSolicitacao = 0)
        {
            IRepositorioSolicitacaoMaterial repSolicitacaoMaterial = Fabrica.Instancia.ObterRepositorio<IRepositorioSolicitacaoMaterial>(UnidadeTrabalho);
            IRepositorioStatusSolicitacaoMaterial repStatusSolicitacaoMaterial = Fabrica.Instancia.ObterRepositorio<IRepositorioStatusSolicitacaoMaterial>(UnidadeTrabalho);

            SolicitacaoMaterial solicitacao = repSolicitacaoMaterial.ObterPorId(codigoSolicitacao);
            if (solicitacao == null)
                throw new CoreException("Solicitação não encontrada");

            List<StatusSolicitacaoMaterial> listaStatusSolicitacaoMaterial = new List<StatusSolicitacaoMaterial>();
            SituacaoSolicitacao situacaoNova = SituacaoSolicitacao.Impressa;

            IncluirStatusSolicitacaoMaterial(repStatusSolicitacaoMaterial, solicitacao, listaStatusSolicitacaoMaterial, situacaoNova);
            solicitacao.Status.Concat(listaStatusSolicitacaoMaterial);

            List<SolicitacaoMaterial> listaSolicitacoes = new List<SolicitacaoMaterial>();
            listaSolicitacoes.Add(solicitacao);

            ReportViewer rv = new ReportViewer();
            LocalReport relatorio = rv.LocalReport;
            RenderingExtension[] exts = relatorio.ListRenderingExtensions();
            string _solicitante = solicitacao.Solicitante.PessoaFisica.CPFFormatado() + " - " + solicitacao.Solicitante.PessoaFisica.Nome;

            relatorio.ReportPath = Server.MapPath("~/Reports/TemplateSolicitaMaterial.rdlc");
            relatorio.DataSources.Add(new ReportDataSource("DTOSolicitacaoMaterial", solicitacao.Itens.Select(i => new { Descricao = i.Descricao, Quantidade = i.Quantidade, Patrimonio = i.NumeroPatrimonio, NotaServico = i.NotaServico, NFEntrada = i.NotaFiscalEntrada, NFSaida = i.NotaFiscalSaida }).ToList()));
            relatorio.SetParameters(new ReportParameter("Solicitante", _solicitante));
            relatorio.SetParameters(new ReportParameter("Status", solicitacao.ObterStatusAtual().Situacao.ObterDescricaoEnum()));
            relatorio.SetParameters(new ReportParameter("TipoSolicitacao", solicitacao.TipoSolicitacao.ObterDescricaoEnum()));
            relatorio.SetParameters(new ReportParameter("Modalidade", solicitacao.Modalidade.ObterDescricaoEnum<Modalidade>()));
            relatorio.SetParameters(new ReportParameter("Contratada", solicitacao.Contratada != null ? solicitacao.Contratada.Nome : string.Empty));
            relatorio.SetParameters(new ReportParameter("SetorCusto", solicitacao.SetorCusto.Nome));
            relatorio.SetParameters(new ReportParameter("Responsavel", solicitacao.ColaboradorResponsavel == null ? solicitacao.GestorResponsavel.Colaborador.PessoaFisica.Nome : solicitacao.ColaboradorResponsavel.PessoaFisica.Nome));
            relatorio.SetParameters(new ReportParameter("Destino", solicitacao.Destino));
            relatorio.SetParameters(new ReportParameter("Motivo", solicitacao.Motivo));
            relatorio.SetParameters(new ReportParameter("Cargo", solicitacao.Solicitante.PessoaFisica.ObterPapel().Cargo.Nome));
            relatorio.SetParameters(new ReportParameter("SolicitadoEm", solicitacao.DataAguardandoAprovacao.Value.ToString("dd/MM/yyyy")));
            relatorio.SetParameters(new ReportParameter("Data", solicitacao.DataEntradaSaida.ToString("dd/MM/yyyy")));
            relatorio.SetParameters(new ReportParameter("RetornoPrevisto", solicitacao.DataPrevisaoRetorno.HasValue ? solicitacao.DataPrevisaoRetorno.Value.ToString("dd/MM/yyyy") : string.Empty));
            relatorio.SetParameters(new ReportParameter("Subcontratada", solicitacao.SubContratada != null ? solicitacao.SubContratada.Nome : string.Empty));
            relatorio.SetParameters(new ReportParameter("NumeroPedido", solicitacao.PedidoCompra != null ? solicitacao.PedidoCompra.Numero : string.Empty));
            relatorio.SetParameters(new ReportParameter("Placa", solicitacao.PlacaVeiculo));
            relatorio.SetParameters(new ReportParameter("Portador", solicitacao.Portador));
            relatorio.SetParameters(new ReportParameter("Codigo", solicitacao.Codigo.ToString()));


            string mimeType = "application/pdf";
            string fileName = "download.pdf";
            string encoding;
            string fileNameExtension;
            Warning[] warnings;
            string[] streams;
            byte[] bytes;


            bytes = relatorio.Render(
            "PDF",
            null,
            out mimeType,
            out encoding,
            out fileNameExtension,
            out streams,
            out warnings);

            return File(bytes, mimeType, fileName);
        }

        public ActionResult ObterColaboradoresComPermissao(string cpf, string passaporte, string nome, bool verificaStatus = false, bool buscarCracha = false, string tipoPapel = null, bool isRecebidoPor = false)
        {
            IRepositorioPapel rep = Fabrica.Instancia.ObterRepositorio<IRepositorioPapel>(UnidadeTrabalho);
            IQueryable<Papel> papeis = rep.ObterTodos().Where(p => p.PapelLogs.OrderByDescending(pp => pp.Codigo).Select(x => x.Status).FirstOrDefault() != StatusPapelLog.BaixaDefinitiva);

            cpf = cpf.Replace(".", "").Replace("-", "");
            if (!string.IsNullOrEmpty(cpf))
                papeis = papeis.Where(p => p.PessoaFisica.CPF.Contains(cpf));

            if (!string.IsNullOrEmpty(passaporte))
                papeis = papeis.Where(p => p.PessoaFisica.Passaporte.Contains(passaporte));

            if (!string.IsNullOrEmpty(nome))
                papeis = papeis.Where(p => p.PessoaFisica.Nome.Contains(nome));

            if (!string.IsNullOrEmpty(tipoPapel))
                switch (tipoPapel)
                {
                    case "Colaborador":
                        papeis = papeis.Where(p => p is Colaborador);
                        break;
                    case "Prestador":
                        papeis = papeis.Where(p => p is PrestadorServico);
                        break;
                    case "Visitante":
                        papeis = papeis.Where(p => p is Visitante);
                        break;
                    default:
                        break;
                }

            if (!isRecebidoPor)
            {
                IRepositorioPermissao repPermissao = Fabrica.Instancia.ObterRepositorio<IRepositorioPermissao>(UnidadeTrabalho);

                List<GrupoUsuario> gruposUsuarioPermissaoAprovar = repPermissao.ObterTodos().Where(p => p.Acao.Url == "/SolicitacaoMaterial/Aprovar").SelectMany(p => p.Grupo.GruposUsuarios).ToList();
                List<PessoaFisica> pessoasFisicasPermissaoAprovar = gruposUsuarioPermissaoAprovar.Select(u => u.Usuario.PessoaFisica).ToList();
                List<Papel> papeisPermissaoAprovar = pessoasFisicasPermissaoAprovar.Select(pf => pf.ObterPapel(true)).ToList();

                List<GrupoUsuario> gruposUsuarioPermissaoReprovar = repPermissao.ObterTodos().Where(p => p.Acao.Url == "/SolicitacaoMaterial/Reprovar").SelectMany(p => p.Grupo.GruposUsuarios).ToList();
                List<PessoaFisica> pessoasFisicasPermissaoReprovar = gruposUsuarioPermissaoAprovar.Select(u => u.Usuario.PessoaFisica).ToList();
                List<Papel> papeisPermissaoReprovar = pessoasFisicasPermissaoAprovar.Select(pf => pf.ObterPapel(true)).ToList();

                List<int> codigoPapeisPermissaoAprovar = papeisPermissaoAprovar.Where(p => p != null).Select(p => p.Codigo).ToList();
                List<int> codigoPapeisPermissaoReprovar = papeisPermissaoReprovar.Where(p => p != null).Select(p => p.Codigo).ToList();
                List<int> codigoPapeisPermissaoAprovarEReprovar = codigoPapeisPermissaoAprovar.Intersect(codigoPapeisPermissaoReprovar).ToList();

                papeis = papeis.Where(p => codigoPapeisPermissaoAprovarEReprovar.Contains(p.Codigo));
            }

            return Json(papeis.ToList().Select(p =>
            {
                return new
                {
                    Codigo = p.PessoaFisica.Codigo,
                    CodigoPapel = p.Codigo,
                    TipoPapel = p != null ? (p is Colaborador) ? "Colaborador" : (p is PrestadorServico) ? "Prestador de Serviço" : "Visitante" : "",
                    Nome = p.PessoaFisica.Nome,
                    CPF = p.PessoaFisica.CPF != null ? p.PessoaFisica.CPFFormatado() : null,
                    Passaporte = p.PessoaFisica.Passaporte,
                    Empresa = ((p is Colaborador) ? (p as Colaborador).Empresa.Apelido :
                        ((p is PrestadorServico) ? (p as PrestadorServico).Empresa != null ?
                            (p as PrestadorServico).Empresa.Apelido : "" :
                            ((p is Visitante) ? (p as Visitante).Empresa : ""))),
                    Status = p.ObterLogPapel().Status.ObterDescricaoEnum()
                };
            })
            .ToList());
        }

        public ActionResult ObterGestores(int codigoPedido = 0)
        {
            if (codigoPedido > 0)
            {
                IList<ColaboradorPedido> responsaveis = Fabrica.Instancia.ObterRepositorio<IRepositorioPedidoCompra>(UnidadeTrabalho).ObterPorId(codigoPedido).GestoresResponsaveis;
                var xxx = responsaveis.Select(r => new { Codigo = r.Codigo, Nome = r.Colaborador.PessoaFisica.Nome });
                return Json(xxx);
            }
            else
            {
                return Json(null);
            }
        }

        public ActionResult Pesquisar(StoreRequestParameters parameters, int codigoSolicitacao = 0, string codigoEmpresa = null, string nomeGestor = null, int codigoSetor = 0, int codigoPedido = 0, DateTime? dataMinima = null, DateTime? dataMaxima = null)
        {
            IQueryable<SolicitacaoMaterial> solicitacoes = Filtrar(codigoSolicitacao, codigoEmpresa, nomeGestor, codigoSetor, codigoPedido, dataMinima, dataMaxima);
            int total = solicitacoes.Count();
            SolicitacaoMaterialModelView[] data = MontaObjetos(solicitacoes, parameters);

            return new StoreResult(data, total);
        }

        [Transaction]
        public ActionResult Receber(int codigoSolicitacao = 0, int codigoPapel = 0)
        {
            IRepositorioSolicitacaoMaterial repSolicitacaoMaterial = Fabrica.Instancia.ObterRepositorio<IRepositorioSolicitacaoMaterial>(UnidadeTrabalho);
            IRepositorioStatusSolicitacaoMaterial repStatusSolicitacaoMaterial = Fabrica.Instancia.ObterRepositorio<IRepositorioStatusSolicitacaoMaterial>(UnidadeTrabalho);
            IRepositorioPapel repPapel = Fabrica.Instancia.ObterRepositorio<IRepositorioPapel>(UnidadeTrabalho);

            SolicitacaoMaterial solicitacao = repSolicitacaoMaterial.ObterPorId(codigoSolicitacao);
            Papel papel = repPapel.ObterPorId(codigoPapel);

            if (solicitacao == null)
                throw new CoreException("Solicitação não encontrada");

            if (papel == null)
                throw new CoreException("Papel não encontrada");

            solicitacao.RecebidoPor = papel;

            List<StatusSolicitacaoMaterial> listaStatusSolicitacaoMaterial = new List<StatusSolicitacaoMaterial>();
            SituacaoSolicitacao situacaoNova = SituacaoSolicitacao.Recebida;

            IncluirStatusSolicitacaoMaterial(repStatusSolicitacaoMaterial, solicitacao, listaStatusSolicitacaoMaterial, situacaoNova);
            solicitacao.Status.Concat(listaStatusSolicitacaoMaterial);

            List<SolicitacaoMaterial> listaSolicitacoes = new List<SolicitacaoMaterial>();
            listaSolicitacoes.Add(solicitacao);

            EnviarEmail(solicitacao);

            return Json(MontaObjetos(listaSolicitacoes.AsQueryable(), null, true));
        }

        [Transaction]
        public ActionResult AlterarStatusNaoConforme(int codigoSolicitacao = 0, string motivo = null)
        {
            IRepositorioSolicitacaoMaterial repSolicitacaoMaterial = Fabrica.Instancia.ObterRepositorio<IRepositorioSolicitacaoMaterial>(UnidadeTrabalho);
            IRepositorioItemSolicitacaoMaterial repItemSolicitacaoMaterial = Fabrica.Instancia.ObterRepositorio<IRepositorioItemSolicitacaoMaterial>(UnidadeTrabalho);
            IRepositorioStatusSolicitacaoMaterial repStatusSolicitacaoMaterial = Fabrica.Instancia.ObterRepositorio<IRepositorioStatusSolicitacaoMaterial>(UnidadeTrabalho);
            IRepositorioSolicitacaoMaterialMidia repSolicitacaoMaterialMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioSolicitacaoMaterialMidia>(UnidadeTrabalho);

            SolicitacaoMaterial solicitacao = repSolicitacaoMaterial.ObterPorId(codigoSolicitacao);

            if (solicitacao == null)
                throw new CoreException("Solicitação não encontrada");

            if (string.IsNullOrWhiteSpace(motivo))
                throw new CoreException("Informe o motivo");

            foreach (ItemSolicitacaoMaterial item in solicitacao.Itens)
            {
                item.AutorizacaoVigilante = false;

                repItemSolicitacaoMaterial.Salvar(item);
            }

            List<StatusSolicitacaoMaterial> listaStatusSolicitacaoMaterial = new List<StatusSolicitacaoMaterial>();
            SituacaoSolicitacao situacaoNova = SituacaoSolicitacao.NaoConforme;

            IncluirStatusSolicitacaoMaterial(repStatusSolicitacaoMaterial, solicitacao, listaStatusSolicitacaoMaterial, situacaoNova, motivo);
            solicitacao.Status.Concat(listaStatusSolicitacaoMaterial);

            List<SolicitacaoMaterial> listaSolicitacoes = new List<SolicitacaoMaterial>();
            listaSolicitacoes.Add(solicitacao);

            EnviarEmail(solicitacao);

            return Json(MontaObjetos(listaSolicitacoes.AsQueryable(), null, true));
        }

        [Transaction]
        public ActionResult Registrar(int codigoSolicitacao = 0, MaterialModelView[] materiaisRecebidos = null)
        {
            IRepositorioSolicitacaoMaterial repSolicitacaoMaterial = Fabrica.Instancia.ObterRepositorio<IRepositorioSolicitacaoMaterial>(UnidadeTrabalho);
            IRepositorioItemSolicitacaoMaterial repItemSolicitacaoMaterial = Fabrica.Instancia.ObterRepositorio<IRepositorioItemSolicitacaoMaterial>(UnidadeTrabalho);
            IRepositorioStatusSolicitacaoMaterial repStatusSolicitacaoMaterial = Fabrica.Instancia.ObterRepositorio<IRepositorioStatusSolicitacaoMaterial>(UnidadeTrabalho);
            IRepositorioSolicitacaoMaterialMidia repSolicitacaoMaterialMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioSolicitacaoMaterialMidia>(UnidadeTrabalho);

            SolicitacaoMaterial solicitacao = repSolicitacaoMaterial.ObterPorId(codigoSolicitacao);

            if (solicitacao == null)
                throw new CoreException("Solicitação não encontrada");

            if (materiaisRecebidos == null)
                throw new CoreException("Materiais não encontrados");

            List<ItemSolicitacaoMaterial> listaItems = new List<ItemSolicitacaoMaterial>();

            AlterarItensSolicitacaoMaterial(materiaisRecebidos, codigoSolicitacao, listaItems, repItemSolicitacaoMaterial, true);

            List<StatusSolicitacaoMaterial> listaStatusSolicitacaoMaterial = new List<StatusSolicitacaoMaterial>();
            SituacaoSolicitacao situacaoNova = SituacaoSolicitacao.Registrada;

            IncluirStatusSolicitacaoMaterial(repStatusSolicitacaoMaterial, solicitacao, listaStatusSolicitacaoMaterial, situacaoNova);
            solicitacao.Status.Concat(listaStatusSolicitacaoMaterial);

            List<SolicitacaoMaterial> listaSolicitacoes = new List<SolicitacaoMaterial>();
            listaSolicitacoes.Add(solicitacao);

            EnviarEmail(solicitacao);

            return Json(MontaObjetos(listaSolicitacoes.AsQueryable(), null, true));
        }

        [Transaction]
        public ActionResult Reprovar(int codigoSolicitacao = 0)
        {
            IRepositorioSolicitacaoMaterial repSolicitacaoMaterial = Fabrica.Instancia.ObterRepositorio<IRepositorioSolicitacaoMaterial>(UnidadeTrabalho);
            IRepositorioStatusSolicitacaoMaterial repStatusSolicitacaoMaterial = Fabrica.Instancia.ObterRepositorio<IRepositorioStatusSolicitacaoMaterial>(UnidadeTrabalho);

            SolicitacaoMaterial solicitacao = repSolicitacaoMaterial.ObterPorId(codigoSolicitacao);
            if (solicitacao == null)
                throw new CoreException("Solicitação não encontrada");

            List<StatusSolicitacaoMaterial> listaStatusSolicitacaoMaterial = new List<StatusSolicitacaoMaterial>();
            SituacaoSolicitacao situacaoNova = SituacaoSolicitacao.Reprovada;

            IncluirStatusSolicitacaoMaterial(repStatusSolicitacaoMaterial, solicitacao, listaStatusSolicitacaoMaterial, situacaoNova);
            solicitacao.Status.Concat(listaStatusSolicitacaoMaterial);

            List<SolicitacaoMaterial> listaSolicitacoes = new List<SolicitacaoMaterial>();
            listaSolicitacoes.Add(solicitacao);

            EnviarEmail(solicitacao);

            return Json(MontaObjetos(listaSolicitacoes.AsQueryable(), null, true));
        }

        public ActionResult Selecionar(int codigoSolicitacao = 0)
        {
            if (codigoSolicitacao <= 0)
                throw new CoreException("Solicitação não encontrada");

            IQueryable<SolicitacaoMaterial> solicitacoes = Filtrar(codigoSolicitacao);
            SolicitacaoMaterialModelView[] data = MontaObjetos(solicitacoes, null, true);
            return Json(data);
        }

        [Transaction]
        public ActionResult Solicitar(TipoSolicitacao tipo, DateTime dataEntradaSaida, int codigoSolicitacaoMaterial = 0, Modalidade? modalidade = null, DateTime? retorno = null, int? setorCusto = null, string contratada = null, int pedidoCompra = 0, string subcontratada = null, string placa = null, string destino = null, string portador = null, string motivo = null, MaterialModelView[] materiais = null, MidiaModelView[] midias = null, int codigoColaborador = 0, int codigoGestor = 0)
        {

            IRepositorioItemSolicitacaoMaterial repItemSolicitacaoMaterial = Fabrica.Instancia.ObterRepositorio<IRepositorioItemSolicitacaoMaterial>(UnidadeTrabalho);
            IRepositorioStatusSolicitacaoMaterial repStatusSolicitacaoMaterial = Fabrica.Instancia.ObterRepositorio<IRepositorioStatusSolicitacaoMaterial>(UnidadeTrabalho);
            IRepositorioSolicitacaoMaterialMidia repSolicitacaoMaterialMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioSolicitacaoMaterialMidia>(UnidadeTrabalho);

            SolicitacaoMaterial novaSolicitacaoMaterial = IncluirSolicitacaoMaterial(tipo, dataEntradaSaida, modalidade, retorno, setorCusto, contratada, pedidoCompra, subcontratada, placa, destino, portador, motivo, materiais, codigoColaborador, codigoGestor, codigoSolicitacaoMaterial);

            List<ItemSolicitacaoMaterial> listaItems = new List<ItemSolicitacaoMaterial>();

            AlterarItensSolicitacaoMaterial(materiais, codigoSolicitacaoMaterial, listaItems, repItemSolicitacaoMaterial);
            IncluirItensSolicitacaoMaterial(materiais, novaSolicitacaoMaterial, listaItems, repItemSolicitacaoMaterial);

            List<SolicitacaoMaterialMidia> listaMidias = new List<SolicitacaoMaterialMidia>();

            AlterarSolicitacaoMaterialMidias(codigoSolicitacaoMaterial, midias, repSolicitacaoMaterialMidia, listaMidias);
            IncluirSolicitacaoMaterialMidias(midias, repSolicitacaoMaterialMidia, novaSolicitacaoMaterial, listaMidias);

            if (codigoSolicitacaoMaterial == 0)
            {
                List<StatusSolicitacaoMaterial> listaStatusSolicitacaoMaterial = new List<StatusSolicitacaoMaterial>();
                SituacaoSolicitacao situacaoNova = SituacaoSolicitacao.AguardandoAprovacao;
                IncluirStatusSolicitacaoMaterial(repStatusSolicitacaoMaterial, novaSolicitacaoMaterial, listaStatusSolicitacaoMaterial, situacaoNova);
                novaSolicitacaoMaterial.Status = listaStatusSolicitacaoMaterial;
            }

            novaSolicitacaoMaterial.Itens = listaItems;
            novaSolicitacaoMaterial.Midias = listaMidias;

            List<SolicitacaoMaterial> listaSolicitacoes = new List<SolicitacaoMaterial>();
            listaSolicitacoes.Add(novaSolicitacaoMaterial);

            EnviarEmail(novaSolicitacaoMaterial);

            return Json(MontaObjetos(listaSolicitacoes.AsQueryable(), null, true));
        }

        private IQueryable<SolicitacaoMaterial> Filtrar(int codigoSolicitacao = 0, string codigoEmpresa = null, string nomeGestor = null, int codigoSetor = 0, int codigoPedido = 0, DateTime? dataMinima = null, DateTime? dataMaxima = null)
        {
            IQueryable<SolicitacaoMaterial> solicitacoes = Fabrica.Instancia.ObterRepositorio<IRepositorioSolicitacaoMaterial>(UnidadeTrabalho).ObterTodos();

            if (codigoSolicitacao > 0)
            {
                solicitacoes = solicitacoes.Where(s => s.Codigo == codigoSolicitacao);
            }
            else
            {
                IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
                Usuario usuarioLogado = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name);

                if (!string.IsNullOrEmpty(codigoEmpresa))
                    solicitacoes = solicitacoes.Where(s => s.Contratada.Codigo.Equals(codigoEmpresa));

                if (!string.IsNullOrEmpty(nomeGestor))
                    solicitacoes = solicitacoes.Where(s => s.GestorResponsavel.Colaborador.PessoaFisica.Nome.Contains(nomeGestor) || s.ColaboradorResponsavel.PessoaFisica.Nome.Contains(nomeGestor));

                if (codigoSetor > 0)
                    solicitacoes = solicitacoes.Where(s => s.SetorCusto.Codigo == codigoSetor);

                if (codigoPedido > 0)
                    solicitacoes = solicitacoes.Where(s => s.PedidoCompra != null && s.PedidoCompra.Codigo == codigoPedido);

                if (dataMaxima.HasValue)
                    solicitacoes = solicitacoes.Where(s => s.DataEntradaSaida.Date <= dataMaxima.Value);

                if (dataMinima.HasValue)
                    solicitacoes = solicitacoes.Where(s => s.DataEntradaSaida.Date >= dataMinima.Value);

                if (!usuarioLogado.GruposUsuarios.Any(gu => gu.Grupo.IsAdmin()))
                {
                    var papel = usuarioLogado.PessoaFisica.ObterPapel(true);
                    int codigoPapel = papel != null ? papel.Codigo : 0;

                    IRepositorioPermissao repPermissao = Fabrica.Instancia.ObterRepositorio<IRepositorioPermissao>(UnidadeTrabalho);

                    var grupos = usuarioLogado.GruposUsuarios.Select(g => g.Grupo);
                    var acoes = grupos.SelectMany(g => g.Acoes);
                    bool podeRegistrar = (acoes.Where(a => a.Url == "/SolicitacaoMaterial/Registrar")).Any();

                    solicitacoes = solicitacoes.Where(sm => sm.ColaboradorResponsavel.Codigo == codigoPapel || sm.GestorResponsavel.Colaborador.Codigo == codigoPapel || sm.Solicitante.Codigo == usuarioLogado.Codigo || podeRegistrar);

                }
            }

            return solicitacoes;
        }

        private static void IncluirStatusSolicitacaoMaterial(IRepositorioStatusSolicitacaoMaterial repStatusSolicitacaoMaterial, SolicitacaoMaterial novaSolicitacaoMaterial, List<StatusSolicitacaoMaterial> listaStatusSolicitacaoMaterial, SituacaoSolicitacao situacaoNova, string observacao = null)
        {
            StatusSolicitacaoMaterial novoStatusSolicitacao = new StatusSolicitacaoMaterial();
            novoStatusSolicitacao.DataRegistro = DateTime.Now;
            novoStatusSolicitacao.Situacao = situacaoNova;
            novoStatusSolicitacao.SolicitacaoMaterial = novaSolicitacaoMaterial;
            novoStatusSolicitacao.Observacoes = observacao;
            novoStatusSolicitacao.UsuarioStatus = novaSolicitacaoMaterial.Solicitante;//Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho).ObterPorLogin(HttpContext.User.Identity.Name, somenteAtivos: true);

            repStatusSolicitacaoMaterial.Validar(novoStatusSolicitacao, EstadoObjeto.Novo);
            repStatusSolicitacaoMaterial.Salvar(novoStatusSolicitacao);
            listaStatusSolicitacaoMaterial.Add(novoStatusSolicitacao);
        }

        private void IncluirSolicitacaoMaterialMidias(MidiaModelView[] midias, IRepositorioSolicitacaoMaterialMidia repSolicitacaoMaterialMidia, SolicitacaoMaterial novaSolicitacaoMaterial, List<SolicitacaoMaterialMidia> listaMidias)
        {
            if (midias == null || midias.Length == 0)
                return;

            string path = ConfigurationManager.AppSettings.Get("pathUploads");

            if (!System.IO.Directory.Exists(path))
                throw new CoreException("O diretório onde a midia seria armazanada não está acessivel.");

            foreach (MidiaModelView midia in midias)
            {
                Midia novaMidia = new Midia();
                string nomeNovaMidia = "NF-" + novaSolicitacaoMaterial.Codigo.ToString() + "-" + listaMidias.Count();

                novaMidia.DataRegistro = DateTime.Now;
                novaMidia.Extensao = midia.Extensao;
                novaMidia.MimeType = midia.MIME;
                novaMidia.NomeArquivo = nomeNovaMidia;

                IRepositorioMidia repMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioMidia>(UnidadeTrabalho);
                repMidia.Salvar(novaMidia);

                SolicitacaoMaterialMidia novaSolicitacaoMidia = new SolicitacaoMaterialMidia();
                novaSolicitacaoMidia.Ativar();
                novaSolicitacaoMidia.DataRegistro = DateTime.Now;
                novaSolicitacaoMidia.Midia = novaMidia;
                novaSolicitacaoMidia.SolicitacaoMaterial = novaSolicitacaoMaterial;

                repSolicitacaoMaterialMidia.Validar(novaSolicitacaoMidia, EstadoObjeto.Novo);
                repSolicitacaoMaterialMidia.Salvar(novaSolicitacaoMidia);
            }
        }

        private static void AlterarSolicitacaoMaterialMidias(int codigoSolicitacaoMaterial, MidiaModelView[] midias, IRepositorioSolicitacaoMaterialMidia repSolicitacaoMaterialMidia, List<SolicitacaoMaterialMidia> listaMidias)
        {
            IQueryable<SolicitacaoMaterialMidia> midiasAntigos = repSolicitacaoMaterialMidia.ObterTodos().Where(ssm => ssm.SolicitacaoMaterial.Codigo == codigoSolicitacaoMaterial);

            foreach (SolicitacaoMaterialMidia midiaAntiga in midiasAntigos)
            {
                MidiaModelView midiaAlterada = midias.FirstOrDefault(m => m.CodigoMidia == midiaAntiga.Midia.Codigo);

                if (midiaAlterada == null)
                {
                    midiaAntiga.Inativar();
                }

                repSolicitacaoMaterialMidia.Validar(midiaAntiga, EstadoObjeto.Alterado);
                repSolicitacaoMaterialMidia.Salvar(midiaAntiga);

                listaMidias.Add(midiaAntiga);
            }
        }

        private static void IncluirItensSolicitacaoMaterial(MaterialModelView[] materiais, SolicitacaoMaterial novaSolicitacaoMaterial, List<ItemSolicitacaoMaterial> listaItems, IRepositorioItemSolicitacaoMaterial repItemSolicitacaoMaterial)
        {
            foreach (MaterialModelView item in materiais.Where(m => m.CodigoMaterial == 0))
            {
                ItemSolicitacaoMaterial novoItemSolicitacao = new ItemSolicitacaoMaterial();
                novoItemSolicitacao.Descricao = item.Descricao;
                novoItemSolicitacao.NotaFiscalEntrada = item.NotaFiscalEntrada;
                novoItemSolicitacao.NotaFiscalSaida = item.NotaFiscalSaida;
                novoItemSolicitacao.NotaServico = item.NotaServico;
                novoItemSolicitacao.NumeroPatrimonio = item.Patrimonio;
                novoItemSolicitacao.Quantidade = item.Quantidade;
                novoItemSolicitacao.SolicitacaoMaterial = novaSolicitacaoMaterial;

                repItemSolicitacaoMaterial.Validar(novoItemSolicitacao, EstadoObjeto.Novo);
                repItemSolicitacaoMaterial.Salvar(novoItemSolicitacao);

                listaItems.Add(novoItemSolicitacao);
            }
        }

        private static void AlterarItensSolicitacaoMaterial(MaterialModelView[] materiais, int codigoSolicitacaoMaterial, List<ItemSolicitacaoMaterial> listaItems, IRepositorioItemSolicitacaoMaterial repItemSolicitacaoMaterial, bool isMateriaisRegistrados = false)
        {
            //Obtem todos os materiais da solicitação
            IQueryable<ItemSolicitacaoMaterial> itensAntigos = repItemSolicitacaoMaterial.ObterTodos().Where(ism => ism.SolicitacaoMaterial.Codigo == codigoSolicitacaoMaterial);


            foreach (ItemSolicitacaoMaterial itemAntigo in itensAntigos)
            {
                MaterialModelView itemAlterado = materiais.FirstOrDefault(m => m.CodigoMaterial == itemAntigo.Codigo);
                // se encontrou o material na lista de materiais salvos é pq ele foi modificado entao deve salvar as alterações
                // a solicitação so pode ser alterada quando estiver com status de aguardando aprovação, e nesse caso os detalhes do material foram alterados
                // ou com o status de registrado, nesse caso o vigilante apenas autoriza a entrada/saida do cada material, e os outros detalhes nao sofrem alteração
                if (itemAlterado != null)
                {
                    itemAntigo.AutorizacaoVigilante = isMateriaisRegistrados ? true : false;
                    if (!isMateriaisRegistrados)
                    {
                        itemAntigo.Descricao = itemAlterado.Descricao;
                        itemAntigo.NotaFiscalEntrada = itemAlterado.NotaFiscalEntrada;
                        itemAntigo.NotaFiscalSaida = itemAlterado.NotaFiscalSaida;
                        itemAntigo.NotaServico = itemAlterado.NotaServico;
                        itemAntigo.NumeroPatrimonio = itemAlterado.Patrimonio;
                        itemAntigo.Quantidade = itemAlterado.Quantidade;
                    }
                    repItemSolicitacaoMaterial.Validar(itemAntigo, EstadoObjeto.Alterado);
                }
                else
                {
                    // se nao encontrou é pq o item nao foi incluido na lista de materias vindos da view, 
                    // assim o material ou foi removido quando o status for de aguardando solicitação ou 
                    // nao foi incluido na lista de materiais registrados quando o vigilante for registrar os materiais
                    if (isMateriaisRegistrados)
                    {
                        itemAntigo.AutorizacaoVigilante = false;
                        repItemSolicitacaoMaterial.Validar(itemAntigo, EstadoObjeto.Alterado);
                    }
                    else
                    {
                        itemAntigo.Inativar();
                        repItemSolicitacaoMaterial.Validar(itemAntigo, EstadoObjeto.Inativado);
                    }
                }

                repItemSolicitacaoMaterial.Salvar(itemAntigo);

                listaItems.Add(itemAntigo);
            }
        }

        private SolicitacaoMaterial IncluirSolicitacaoMaterial(TipoSolicitacao tipo, DateTime dataEntradaSaida, Modalidade? modalidade, DateTime? retorno, int? setorCusto, string contratada, int pedidoCompra, string subcontratada, string placa, string destino, string portador, string motivo, MaterialModelView[] materiais, int codigoColaborador, int codigoGestor, int codigoSolicitacao)
        {
            if (materiais == null || materiais.Length <= 0)
            {
                throw new CoreException("Deve ser incluido ao menos um material.");
            }

            SolicitacaoMaterial solicitacaoAnterior = Fabrica.Instancia.ObterRepositorio<IRepositorioSolicitacaoMaterial>(UnidadeTrabalho).ObterPorId(codigoSolicitacao);

            modalidade.ObterDescricaoEnum<Modalidade>();
            PedidoCompra po = Fabrica.Instancia.ObterRepositorio<IRepositorioPedidoCompra>(UnidadeTrabalho).ObterPorId(pedidoCompra);
            SetorCusto sc = Fabrica.Instancia.ObterRepositorio<IRepositorioSetorCusto>(UnidadeTrabalho).ObterPorId(setorCusto.HasValue ? setorCusto.Value : 0);
            Empresa ec = contratada == null ? null : Fabrica.Instancia.ObterRepositorio<IRepositorioEmpresa>(UnidadeTrabalho).ObterPorId(contratada);
            Empresa esc = subcontratada == null ? null : Fabrica.Instancia.ObterRepositorio<IRepositorioEmpresa>(UnidadeTrabalho).ObterPorId(subcontratada);
            Colaborador cr = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho).ObterPorId(codigoColaborador);

            SolicitacaoMaterial novaSolicitacaoMaterial = solicitacaoAnterior ?? new SolicitacaoMaterial();
            novaSolicitacaoMaterial.TipoSolicitacao = tipo;
            novaSolicitacaoMaterial.Contratada = ec;
            novaSolicitacaoMaterial.Destino = destino;
            novaSolicitacaoMaterial.DataEntradaSaida = dataEntradaSaida;
            novaSolicitacaoMaterial.DataPrevisaoRetorno = retorno;
            novaSolicitacaoMaterial.GestorResponsavel = po != null && po.GestoresResponsaveis != null && po.GestoresResponsaveis.Count > 0 && codigoGestor > 0 ? po.GestoresResponsaveis.FirstOrDefault(gr => gr.Codigo == codigoGestor) : null;
            novaSolicitacaoMaterial.ColaboradorResponsavel = cr;
            novaSolicitacaoMaterial.Modalidade = modalidade;
            novaSolicitacaoMaterial.Motivo = motivo;
            novaSolicitacaoMaterial.PedidoCompra = po;
            novaSolicitacaoMaterial.PlacaVeiculo = placa;
            novaSolicitacaoMaterial.Portador = portador;
            novaSolicitacaoMaterial.SetorCusto = sc;
            novaSolicitacaoMaterial.Solicitante = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho).ObterPorLogin(HttpContext.User.Identity.Name, somenteAtivos: true);
            novaSolicitacaoMaterial.SubContratada = esc;
            novaSolicitacaoMaterial.TipoSolicitacao = tipo;
            novaSolicitacaoMaterial.DataRegistro = DateTime.Now;

            IRepositorioSolicitacaoMaterial repSolicitacaoMaterial = Fabrica.Instancia.ObterRepositorio<IRepositorioSolicitacaoMaterial>(UnidadeTrabalho);

            if (solicitacaoAnterior == null)
            {
                novaSolicitacaoMaterial.Ativar();
                repSolicitacaoMaterial.Validar(novaSolicitacaoMaterial, EstadoObjeto.Novo);
                repSolicitacaoMaterial.Salvar(novaSolicitacaoMaterial);
            }
            else
            {
                repSolicitacaoMaterial.Validar(novaSolicitacaoMaterial, EstadoObjeto.Alterado);
                repSolicitacaoMaterial.Atualizar(novaSolicitacaoMaterial);
            }
            return novaSolicitacaoMaterial;
        }

        public void EnviarEmail(int codigoSituacao)
        {
            SolicitacaoMaterial solicitacao = Fabrica.Instancia.ObterRepositorio<IRepositorioSolicitacaoMaterial>(UnidadeTrabalho).ObterPorId(codigoSituacao);

            this.EnviarEmail(solicitacao);
        }


        public void EnviarEmail(SolicitacaoMaterial solicitacao)
        {
            string url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/";

            switch (solicitacao.ObterStatusAtual().Situacao)
            {
                case SituacaoSolicitacao.AguardandoAprovacao:
                    EnviarEmailSolicitar(solicitacao, url);
                    break;
                case SituacaoSolicitacao.Aprovada:
                    EnviarEmailAprovar(solicitacao, url);
                    break;
                case SituacaoSolicitacao.Reprovada:
                    EnviarEmailReprovar(solicitacao, url);
                    break;
                case SituacaoSolicitacao.Registrada:
                    EnviarEmailRegistrar(solicitacao);
                    break;
                case SituacaoSolicitacao.NaoConforme:
                    EnviarEmailNaoConforme(solicitacao);
                    break;
                default:
                    break;
            }
        }

        private static void EnviarEmailNaoConforme(SolicitacaoMaterial solicitacao)
        {
            string corpoEmail = string.Format(@"A solicitação de {0} de material com ID {1} foi registrada como não conforme as {2} do dia {3}.",
                solicitacao.TipoSolicitacao.ObterDescricaoEnum(),
                solicitacao.Codigo,
                solicitacao.DataNaoConforme.Value.ToString("HH:mm"),
                solicitacao.DataNaoConforme.Value.ToString("dd/MM/yyyy"));

            Globalsys.Util.Email.Enviar(string.Format("[CONTROLSYS] Solicitação de {0} de Material", solicitacao.TipoSolicitacao.ObterDescricaoEnum()),
                                   corpoEmail,
                                   solicitacao.Solicitante.PessoaFisica.Email);
        }

        private static void EnviarEmailRegistrar(SolicitacaoMaterial solicitacao)
        {
            string corpoEmail = string.Format(@"A solicitação de {0} de material com ID {1} foi registrada as {2} do dia {3}.",
                solicitacao.TipoSolicitacao.ObterDescricaoEnum(),
                solicitacao.Codigo,
                solicitacao.DataRegistrado.Value.ToString("HH:mm"),
                solicitacao.DataRegistrado.Value.ToString("dd/MM/yyyy"));

            Globalsys.Util.Email.Enviar(string.Format("[CONTROLSYS] Solicitação de {0} de Material", solicitacao.TipoSolicitacao.ObterDescricaoEnum()),
                                   corpoEmail,
                                   solicitacao.Solicitante.PessoaFisica.Email);
        }

        private static void EnviarEmailAprovar(SolicitacaoMaterial solicitacao, string url)
        {
            string corpoEmail = string.Format(@"Prezado Solicitante,
<br/><br/>
A sua solicitação de {2} de material com ID {3} foi aprovada pelo {0} {1}.
<br/><br/>
Para imprimi-la acesse o menu CONTROLE DE MATERIAL > CONTROLE > Solicitação material através do site <a href=""{4}"">controlsys.tkcsa.com.br</a>.
<br/><br/>
Atenciosamente,
<br/><br/>
Controlsys.", solicitacao.GestorResponsavel != null ? "Gestor Responsável" : "Colaborador Responsável",
                                                                                                 solicitacao.GestorResponsavel != null ? solicitacao.GestorResponsavel.Colaborador.PessoaFisica.Nome : solicitacao.ColaboradorResponsavel.PessoaFisica.Nome,
                                                                                                 solicitacao.TipoSolicitacao.ObterDescricaoEnum(),
                                                                                                 solicitacao.Codigo,
                                                                                                 url);

            Globalsys.Util.Email.Enviar(string.Format("[CONTROLSYS] Solicitação de {0} de Material", solicitacao.TipoSolicitacao.ObterDescricaoEnum()),
                                   corpoEmail,
                                   solicitacao.Solicitante.PessoaFisica.Email);
        }

        private static void EnviarEmailReprovar(SolicitacaoMaterial solicitacao, string url)
        {
            string corpoEmail = string.Format(@"Prezado Solicitante,
<br/><br/>
A sua solicitação de {2} de material com ID {3} foi reprovada pelo {0} {1}.
<br/><br/>
Para realização de uma nova solicitação acesse o menu CONTROLE DE MATERIAL > CONTROLE > Solicitação material através do site <a href=""{4}"">controlsys.tkcsa.com.br</a>.
<br/><br/>
Atenciosamente,
<br/><br/>
Controlsys.", solicitacao.GestorResponsavel != null ? "Gestor Responsável" : "Colaborador Responsável",
                                                                                                 solicitacao.GestorResponsavel != null ? solicitacao.GestorResponsavel.Colaborador.PessoaFisica.Nome : solicitacao.ColaboradorResponsavel.PessoaFisica.Nome,
                                                                                                 solicitacao.TipoSolicitacao.ObterDescricaoEnum(),
                                                                                                 solicitacao.Codigo,
                                                                                                 url);

            Globalsys.Util.Email.Enviar(string.Format("[CONTROLSYS] Solicitação de {0} de Material", solicitacao.TipoSolicitacao.ObterDescricaoEnum()),
                                   corpoEmail,
                                   solicitacao.Solicitante.PessoaFisica.Email);
        }

        private void EnviarEmailSolicitar(SolicitacaoMaterial solicitacao, string url)
        {
            ReportViewer rv = new ReportViewer();
            LocalReport relatorio = rv.LocalReport;

            string _solicitante = solicitacao.Solicitante.PessoaFisica.CPFFormatado() + " - " + solicitacao.Solicitante.PessoaFisica.Nome;
            string _cargo = solicitacao.Solicitante.PessoaFisica.ObterPapel() == null || solicitacao.Solicitante.PessoaFisica.ObterPapel().Cargo == null ? string.Empty : solicitacao.Solicitante.PessoaFisica.ObterPapel().Cargo.Nome;
            relatorio.ReportPath = Server.MapPath("~/Reports/TemplateSolicitaMaterial.rdlc");
            relatorio.DataSources.Add(new ReportDataSource("DTOSolicitacaoMaterial", solicitacao.Itens.Select(i => new { Descricao = i.Descricao, Quantidade = i.Quantidade, Patrimonio = i.NumeroPatrimonio, NotaServico = i.NotaServico, NFEntrada = i.NotaFiscalEntrada, NFSaida = i.NotaFiscalSaida }).ToList()));
            relatorio.SetParameters(new ReportParameter("Solicitante", _solicitante));
            relatorio.SetParameters(new ReportParameter("Status", solicitacao.ObterStatusAtual().Situacao.ObterDescricaoEnum()));
            relatorio.SetParameters(new ReportParameter("TipoSolicitacao", solicitacao.TipoSolicitacao.ObterDescricaoEnum()));
            relatorio.SetParameters(new ReportParameter("Modalidade", solicitacao.Modalidade.ObterDescricaoEnum<Modalidade>()));
            relatorio.SetParameters(new ReportParameter("Contratada", solicitacao.Contratada != null ? solicitacao.Contratada.Nome : string.Empty));
            relatorio.SetParameters(new ReportParameter("SetorCusto", solicitacao.SetorCusto.Nome));
            relatorio.SetParameters(new ReportParameter("Responsavel", solicitacao.ColaboradorResponsavel == null ? solicitacao.GestorResponsavel.Colaborador.PessoaFisica.Nome : solicitacao.ColaboradorResponsavel.PessoaFisica.Nome));
            relatorio.SetParameters(new ReportParameter("Destino", solicitacao.Destino));
            relatorio.SetParameters(new ReportParameter("Motivo", solicitacao.Motivo));
            relatorio.SetParameters(new ReportParameter("Cargo", _cargo));
            relatorio.SetParameters(new ReportParameter("SolicitadoEm", solicitacao.DataAguardandoAprovacao.Value.ToString("dd/MM/yyyy")));
            relatorio.SetParameters(new ReportParameter("Data", solicitacao.DataEntradaSaida.ToString("dd/MM/yyyy")));
            relatorio.SetParameters(new ReportParameter("RetornoPrevisto", solicitacao.DataPrevisaoRetorno.HasValue ? solicitacao.DataPrevisaoRetorno.Value.ToString("dd/MM/yyyy") : string.Empty));
            relatorio.SetParameters(new ReportParameter("Subcontratada", solicitacao.SubContratada != null ? solicitacao.SubContratada.Nome : string.Empty));
            relatorio.SetParameters(new ReportParameter("NumeroPedido", solicitacao.PedidoCompra != null ? solicitacao.PedidoCompra.Numero : string.Empty));
            relatorio.SetParameters(new ReportParameter("Placa", solicitacao.PlacaVeiculo));
            relatorio.SetParameters(new ReportParameter("Portador", solicitacao.Portador));
            relatorio.SetParameters(new ReportParameter("Codigo", solicitacao.Codigo.ToString()));

            string mimeType = "application/pdf";
            string fileName = "download.pdf";

            string encoding;
            string fileNameExtension;
            Warning[] warnings;
            string[] streams;
            byte[] bytes;

            bytes = relatorio.Render(
            "PDF",
            null,
            out mimeType,
            out encoding,
            out fileNameExtension,
            out streams,
            out warnings);

            string corpoEmail = string.Format(@"Prezado Gestor(a),
<br/><br/>
Uma solicitação de {2} de material com ID {3} foi realizada e requer sua aprovação. Detalhes no anexo. 
<br/><br/>
Para aprovar ou reprovar a solicitação acesse o menu CONTROLE DE MATERIAL > CONTROLE > Solicitação Material do site <a href=""{4}"">controlsys.tkcsa.com.br</a>.
<br/><br/>
Atenciosamente,
<br/><br/>
Controlsys.", solicitacao.GestorResponsavel != null ? "Gestor Responsável" : "Colaborador Responsável",
                                                                                                 solicitacao.GestorResponsavel != null ? solicitacao.GestorResponsavel.Colaborador.PessoaFisica.Nome : solicitacao.ColaboradorResponsavel.PessoaFisica.Nome,
                                                                                                 solicitacao.TipoSolicitacao.ObterDescricaoEnum(),
                                                                                                 solicitacao.Codigo,
                                                                                                 url);

            Globalsys.Util.Email.Enviar(string.Format("[CONTROLSYS] Solicitação de {0} de Material", solicitacao.TipoSolicitacao.ObterDescricaoEnum()),
                                          null,
                                          null,
                                          corpoEmail,
                                          new Attachment[] { new Attachment(new MemoryStream(bytes), string.Format("Solicitacao{0}Material.pdf", solicitacao.TipoSolicitacao.ObterDescricaoEnum())) },
                                          solicitacao.GestorResponsavel != null ? solicitacao.GestorResponsavel.Colaborador.PessoaFisica.Email : solicitacao.ColaboradorResponsavel.PessoaFisica.Email);
        }


        public SolicitacaoMaterialModelView[] MontaObjetos(IQueryable<SolicitacaoMaterial> solicitacoesMaterial, StoreRequestParameters parameters = null, bool ignoreParameters = false)
        {
            if (!ignoreParameters)
            {
                if (parameters == null)
                    parameters = new StoreRequestParameters();
                solicitacoesMaterial = solicitacoesMaterial.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 25);
            }

            if (solicitacoesMaterial == null || solicitacoesMaterial.Count() <= 0)
            {
                return null;
            }

            SolicitacaoMaterialModelView[] solicitacoesModelView = solicitacoesMaterial.ToList().Select(s => new SolicitacaoMaterialModelView()
            {
                CodigoSolicitacaoMaterial = s.Codigo,
                ColaboradorResponsavel = s.ColaboradorResponsavel != null ? s.ColaboradorResponsavel.PessoaFisica.Nome : string.Empty,
                Contratada = s.Contratada.Nome,
                DataAguardandoAprovacao = s.DataAguardandoAprovacao.HasValue ? s.DataAguardandoAprovacao.Value.ToString("dd/MM/yyyy") : string.Empty,
                DataAprovacaoReprovacao = s.DataAprovacaoReprovacao.HasValue ? s.DataAprovacaoReprovacao.Value.ToString("dd/MM/yyyy") : string.Empty,
                DataEntradaSaída = s.DataEntradaSaida.ToString("dd/MM/yyyy"),
                DataPrevisaoRetorno = s.DataPrevisaoRetorno.HasValue ? s.DataPrevisaoRetorno.Value.ToString("dd/MM/yyyy") : string.Empty,
                DataRegistrado = s.DataRegistrado.HasValue ? s.DataRegistrado.Value.ToString("dd/MM/yyyy HH:mm") : string.Empty,
                Destino = s.Destino,
                GestorResponsavel = s.GestorResponsavel != null ? s.GestorResponsavel.Colaborador.PessoaFisica.Nome : string.Empty,
                Materiais = s.Itens.Select(i => new MaterialModelView() { CodigoMaterial = i.Codigo, Descricao = i.Descricao, NotaFiscalEntrada = i.NotaFiscalEntrada, NotaFiscalSaida = i.NotaFiscalSaida, NotaServico = i.NotaServico, Patrimonio = i.NumeroPatrimonio, Quantidade = i.Quantidade, AutorizacaoVigilante = i.AutorizacaoVigilante }).ToArray(),
                Midias = s.Midias.Select(m => new MidiaModelView() { Arquivo = string.Empty, CodigoMidia = m.Codigo, DataRegistro = m.DataRegistro, Extensao = m.Midia.Extensao, MIME = m.Midia.MimeType, NomeDisco = m.Midia.NomeArquivo, NomeOriginal = string.Empty, URL = m.Midia.Url }).ToArray(),
                DataRegistro = s.DataRegistro.ToString("dd/MM/yyyy"),
                Modalidade = s.Modalidade.ObterDescricaoEnum<Modalidade>(),
                Motivo = s.Motivo,
                PedidoCompra = s.PedidoCompra != null ? s.PedidoCompra.Numero : string.Empty,
                PlacaVeiculo = s.PlacaVeiculo,
                Portador = s.Portador,
                Responsavel = s.GestorResponsavel == null ? s.ColaboradorResponsavel.PessoaFisica.Nome : s.GestorResponsavel.Colaborador.PessoaFisica.Nome,
                SetorCusto = s.SetorCusto.Nome,
                Solicitante = s.Solicitante.PessoaFisica.Nome,
                SolicitanteCargo = ((s.Solicitante.PessoaFisica.Papeis != null && s.Solicitante.PessoaFisica.Papeis.Count() > 0) ? s.Solicitante.PessoaFisica.ObterPapel().Cargo != null ? s.Solicitante.PessoaFisica.ObterPapel().Cargo.Nome : string.Empty : string.Empty),
                Status = s.Status.Select(st => new StatusSolicitacaoMaterialModelVIew() { CodigoStatusSolicitacaoMaterial = st.Codigo, DataRegistro = st.DataRegistro.ToString("dd/MM/yyyy"), Data = st.DataRegistro, SituacaoSolicitacao = st.Situacao.ObterDescricaoEnum(), Observacao = st.Observacoes }).ToArray(),
                StatusAtual = s.Status.OrderByDescending(st => st.Codigo).FirstOrDefault().Situacao.ObterDescricaoEnum(),
                RegistradoPor = s.RecebidoPor != null ? s.RecebidoPor.PessoaFisica.Nome : string.Empty,
                TipoSolicitacao = s.TipoSolicitacao.ObterDescricaoEnum(),
                CodigoTipoSolicitacao = (int)s.TipoSolicitacao,
                CodigoModalidade = (int?)s.Modalidade,
                CodigoContratada = s.Contratada.Codigo,
                CodigoSubcontratada = s.SubContratada == null ? string.Empty : s.SubContratada.Codigo,
                CodigoSetorCusto = s.SetorCusto.Codigo,
                CodigoPedidoCompra = s.PedidoCompra == null ? (int?)null : s.PedidoCompra.Codigo,
                CodigoGestorResponsavel = s.GestorResponsavel == null ? (int?)null : s.GestorResponsavel.Codigo,
                CodigoColaboradorResponsavel = s.ColaboradorResponsavel != null ? s.ColaboradorResponsavel.Codigo : (int?)null

            }).ToArray();

            return solicitacoesModelView;
        }

        public ActionResult PossuiPermissaoSolicitacao(int codigoSolicitacao = 0)
        {
            SolicitacaoMaterial sm = null;
            if (codigoSolicitacao > 0)
            {
                sm = Fabrica.Instancia.ObterRepositorio<IRepositorioSolicitacaoMaterial>(UnidadeTrabalho).ObterPorId(codigoSolicitacao);
            }

            StatusSolicitacaoMaterial status = sm == null ? null : sm.Status.OrderByDescending(x => x.Codigo).FirstOrDefault();

            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            Usuario usuarioLogado = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name);

            if (usuarioLogado == null)
                throw new CoreException("Usário não autenticado.");

            var grupos = usuarioLogado.GruposUsuarios.Select(g => g.Grupo);
            var acoes = grupos.SelectMany(g => g.Acoes);

            bool solicitar = acoes.Where(a => a.Url == "/SolicitacaoMaterial/Solicitar").Any() && (status == null || status.Situacao == SituacaoSolicitacao.AguardandoAprovacao);
            bool aprovar = acoes.Where(a => a.Url == "/SolicitacaoMaterial/Aprovar").Any() && status != null && status.Situacao == SituacaoSolicitacao.AguardandoAprovacao;
            bool reprovar = acoes.Where(a => a.Url == "/SolicitacaoMaterial/Reprovar").Any() && status != null && status.Situacao == SituacaoSolicitacao.AguardandoAprovacao;
            bool imprimir = acoes.Where(a => a.Url == "/SolicitacaoMaterial/Imprimir").Any() && status != null && status.Situacao == SituacaoSolicitacao.Aprovada;
            bool naoConforme = acoes.Where(a => a.Url == "/SolicitacaoMaterial/AlterarStatusNaoConforme").Any() && status != null && status.Situacao == SituacaoSolicitacao.Impressa;
            bool registrar = acoes.Where(a => a.Url == "/SolicitacaoMaterial/Registrar").Any() && status != null && status.Situacao == SituacaoSolicitacao.Impressa;
            bool receber = acoes.Where(a => a.Url == "/SolicitacaoMaterial/Receber").Any() && status != null && status.Situacao == SituacaoSolicitacao.Registrada;

            return Json(new { Solicitar = solicitar, Aprovar = aprovar, Reprovar = reprovar, Imprimir = imprimir, NaoConforme = naoConforme, Registrar = registrar, Receber = receber });

        }

        public ActionResult PermissoesModificarItensSolicitacao(int codigoSolicitacao = 0)
        {
            SolicitacaoMaterial sm = null;
            if (codigoSolicitacao > 0)
            {
                sm = Fabrica.Instancia.ObterRepositorio<IRepositorioSolicitacaoMaterial>(UnidadeTrabalho).ObterPorId(codigoSolicitacao);
            }

            StatusSolicitacaoMaterial status = sm == null ? null : sm.Status.OrderByDescending(x => x.Codigo).FirstOrDefault();

            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            Usuario usuarioLogado = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name);

            if (usuarioLogado == null)
                throw new CoreException("Usário não autenticado.");

            var grupos = usuarioLogado.GruposUsuarios.Select(g => g.Grupo);
            var acoes = grupos.SelectMany(g => g.Acoes);

            bool removerAlterar = acoes.Where(a => a.Url == "/SolicitacaoMaterial/Solicitar").Any() && status.Situacao == SituacaoSolicitacao.AguardandoAprovacao;
            bool aprovarItem = acoes.Where(a => a.Url == "/SolicitacaoMaterial/Registrar").Any() && status.Situacao == SituacaoSolicitacao.Impressa;

            return Json(new { RemoverAlterar = removerAlterar, AprovarItem = aprovarItem });

        }
    }
}