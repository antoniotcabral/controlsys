﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Controlsys.Dominio.Parametros;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorios.Parametros;
using Ext.Net;
using Ext.Net.MVC;
using Globalsys;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar escalas.
    /// </summary>
    [ExtendController]
    public class EscalaController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /Escala/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.EscalaController.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public EscalaController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        /// <summary>
        /// Retorna a página de escala.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Retorna todos os objetos do tipo Escala.
        /// </summary>
        ///
        /// <param name="parameters">
        /// Options for controlling the operation.
        /// </param>
        /// <param name="dataEscala">
        /// O(a) data escala.
        /// </param>
        /// <param name="codGrupoTrabalho">
        /// O(a) cod grupo trabalho.
        /// </param>
        ///
        /// <returns>
        /// Detalhes do tipo de objeto ligado ao controller no formato JSON.
        /// </returns>
        public ActionResult ObterTodos(StoreRequestParameters parameters = null, DateTime? dataEscala = null, int? codGrupoTrabalho = 0)
        {
            var escalas = FiltrarEscalas(dataEscala, codGrupoTrabalho);

            int total = escalas.Count();
            var data = montaObj(escalas, parameters);

            return new StoreResult(data, total);
        }

        /// <summary>
        /// Filtrar escalas.
        /// </summary>
        ///
        /// <param name="dataEscala">
        /// O(a) data escala.
        /// </param>
        /// <param name="codGrupoTrabalho">
        /// O(a) cod grupo trabalho.
        /// </param>
        ///
        /// <returns>
        /// Um(a) IQueryable&lt;EscalaTrabalho&gt;
        /// </returns>
        private IQueryable<EscalaTrabalho> FiltrarEscalas(DateTime? dataEscala, int? codGrupoTrabalho)
        {
            IRepositorioEscalaTrabalho repEscala = Fabrica.Instancia.ObterRepositorio<IRepositorioEscalaTrabalho>(UnidadeTrabalho);

            IQueryable<EscalaTrabalho> escalas = repEscala.ObterTodos();

            if (dataEscala.HasValue)
                escalas = escalas.Where(e => e.DataEscala >= dataEscala);

            if (codGrupoTrabalho.HasValue)
                escalas = escalas.Where(e => e.GrupoTrabalho.Codigo == codGrupoTrabalho);

            return escalas;
        }

        /// <summary>
        /// Cria um objeto contendo informações relacionadas ao conteudo gerenciado por
        /// EscalaController.
        /// </summary>
        ///
        /// <param name="escalas">
        /// O(a) escalas.
        /// </param>
        /// <param name="parameters">
        /// Options for controlling the operation.
        /// </param>
        /// <param name="ignoreParameters">
        /// true to ignore parameters.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;EscalaModelView&gt;
        /// </returns>
        private List<EscalaModelView> montaObj(IQueryable<EscalaTrabalho> escalas, StoreRequestParameters parameters = null, bool ignoreParameters = false)
        {
            if (!ignoreParameters)
            {
                if (parameters == null)
                    parameters = new StoreRequestParameters();
                escalas = escalas.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 30);
            }

            return escalas.Select(e => new EscalaModelView()
            {
                Codigo = e.Codigo,
                DataEscala = e.DataEscala,
                DataRegistro = e.DataRegistro,
                GrupoTrabalho = e.GrupoTrabalho.Nome,
                HoraTurno = string.Format("{0} - {1}", e.HoraTurno.HoraInicio.ToString(@"hh\:mm"), e.HoraTurno.HoraFim.ToString(@"hh\:mm"))
            }).ToList();
        }

        /// <summary>
        /// Executar carga.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ExecutarCarga()
        {

            IRepositorioEscalaTrabalho repEscala = Fabrica.Instancia.ObterRepositorio<IRepositorioEscalaTrabalho>(UnidadeTrabalho);
            UnidadeTrabalho.ExecuteProcedure("sp_carrega_escala");

            return null;
        }
    }
}
