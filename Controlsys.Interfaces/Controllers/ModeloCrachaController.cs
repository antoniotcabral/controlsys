﻿using System.IO;
using System.Web.Helpers;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Parametros;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar modelo crachas.
    /// </summary>
    [ExtendController]
    public class ModeloCrachaController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /ModeloCracha/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.ModeloCrachaController.
        /// </summary>
        ///
        /// <param name="unidade">
        /// O(a) unidade.
        /// </param>
        public ModeloCrachaController(IUnidadeTrabalho unidade)
        {
            UnidadeTrabalho = unidade;
        }

        /// <summary>
        /// Retorna a página de modeloCracha.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Retorna todos os objetos do tipo ModeloCracha.
        /// </summary>
        ///
        /// <param name="papel">
        /// O(a) papel.
        /// </param>
        /// <param name="apenasAtivos">
        /// true to apenas ativos.
        /// </param>
        ///
        /// <returns>
        /// Detalhes do tipo de objeto ligado ao controller no formato JSON.
        /// </returns>
        public ActionResult ObterTodos(TipoPapel? papel, bool apenasAtivos = false)
        {
            IRepositorioModeloCracha rep = Fabrica.Instancia.ObterRepositorio<IRepositorioModeloCracha>(UnidadeTrabalho);

            IQueryable<ModeloCracha> modelos = rep.ObterTodos();

            if (apenasAtivos)
            {
                modelos = modelos.Where(t => t.Ativo == true && t.TipoPapel == papel);
            }
            return Json(modelos.Select(s => new
                               {
                                   Codigo = s.Codigo,
                                   Nome = s.Nome,
                                   TipoPapel = obterDesc(s.TipoPapel),
                                   DataRegistro = s.DataRegistro,
                                   Ativo = s.Ativo
                               })
                                .ToList(), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Obter description.
        /// </summary>
        ///
        /// <param name="tipoPapel">
        /// O(a) tipo papel.
        /// </param>
        ///
        /// <returns>
        /// Um(a) string.
        /// </returns>
        private string obterDesc(TipoPapel tipoPapel)
        {
            return EnumExtensoes.ObterDescricaoEnum(tipoPapel);
        }

        /// <summary>
        /// Obter campos.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ObterCampos()
        {
            List<object> list = new List<object>
            {
                new { Codigo = "codigoPessoa", Nome = "CÓDIGO PESSOA" },
                new { Codigo = "nome", Nome = "NOME" },
                new { Codigo = "nomeGuerra", Nome = "PRIMEIROAPELIDO" },
                new { Codigo = "nomeLast", Nome = "SEGUNDOAPELIDO" },
                new { Codigo = "empresa", Nome = "EMPRESA" },
                new { Codigo = "contratada", Nome = "CONTRATADA" },
                new { Codigo = "empresacontratada", Nome = "CONTRATADA/EMPRESA" },
                new { Codigo = "rfid", Nome = "RFID" },
                new { Codigo = "tipoSanguineo", Nome = "TIPO SANGUÍNEO" },
                new { Codigo = "cargo", Nome = "CARGO" },
                new { Codigo = "8id", Nome = "8ID" },
                new { Codigo = "codigoPapel", Nome = "CÓD PAPEL" },
                ////new { Codigo = "RFID", Nome = "NÚMERO CRACHÁ" },
                new { Codigo = "nomeFantasiaFornecedor", Nome = "NOME FANTASIA FORNECEDOR" }
            };
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Obter fontes.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ObterFontes()
        {
            List<object> list = new List<object>
            {
                new { Codigo = 1, Nome = "Arial" },
                new { Codigo = 2, Nome = "Calibri" },
                new { Codigo = 3, Nome = "Tahoma" },
                new { Codigo = 4, Nome = "Times New Roman" },
                new { Codigo = 5, Nome = "TKTypeBold" },
                new { Codigo = 6, Nome = "Verdana" },
                new { Codigo = 7, Nome = "TKTypeMedium" },
                new { Codigo = 8, Nome = "TKTypeRegular" }
            };
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Inclui um novo modeloCracha.
        /// </summary>
        ///
        /// <param name="modeloCracha">
        /// O(a) modelo cracha.
        /// </param>
        /// <param name="fundo">
        /// O(a) fundo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Incluir(ModeloCracha modeloCracha, string fundo = null)
        {
            IRepositorioModeloCracha repModeloCracha = Fabrica.Instancia.ObterRepositorio<IRepositorioModeloCracha>(UnidadeTrabalho);

            modeloCracha.Html = modeloCracha.Html.Replace("campo edt", "campo");
            modeloCracha.Fundo = (string.IsNullOrEmpty(fundo)) ? null : Convert.FromBase64String(fundo);
            modeloCracha.DataRegistro = DateTime.Now;
            modeloCracha.Ativar();
            repModeloCracha.Validar(modeloCracha, EstadoObjeto.Novo);
            repModeloCracha.Salvar(modeloCracha);
            
            return Json(montaObj(modeloCracha));

        }

        /// <summary>
        /// Altera um registro de modeloCracha.
        /// </summary>
        ///
        /// <param name="modeloCracha">
        /// O(a) modelo cracha.
        /// </param>
        /// <param name="fundo">
        /// O(a) fundo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Alterar(ModeloCracha modeloCracha, string fundo = null)
        {
            IRepositorioModeloCracha repModeloCracha = Fabrica.Instancia.ObterRepositorio<IRepositorioModeloCracha>(UnidadeTrabalho);
            ModeloCracha m = repModeloCracha.ObterPorId(modeloCracha.Codigo);

            m.Html = modeloCracha.Html.Replace("campo edt", "campo");
            m.Fundo = (string.IsNullOrEmpty(fundo)) ? null : Convert.FromBase64String(fundo);
            m.Nome = modeloCracha.Nome;
            m.Horizontal = modeloCracha.Horizontal;
            repModeloCracha.Validar(m, EstadoObjeto.Alterado);
            repModeloCracha.Atualizar(m);

            return Json(montaObj(m));

        }

        /// <summary>
        /// Lista tipo papel.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ListaTipoPapel()
        {
            Dictionary<int, string> dic = new Dictionary<int, string>();

            foreach (int item in Enum.GetValues(typeof(TipoPapel)))
            {
                dic.Add(item, ((TipoPapel)item).ObterDescricaoEnum());
            }
            return Json(dic.Select(d => new
            {
                Codigo = d.Key,
                Descricao = d.Value
            }).ToList().OrderBy(x => x.Descricao));

        }

        /// <summary>
        /// Selecionar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Selecionar(int codigo)
        {
            IRepositorioModeloCracha rep = Fabrica.Instancia.ObterRepositorio<IRepositorioModeloCracha>(UnidadeTrabalho);
            ModeloCracha modeloCracha = rep.ObterPorId(codigo);
            return Json(montaObj(modeloCracha));
        }

        /// <summary>
        /// Cria um objeto contendo informações relacionadas ao conteudo gerenciado por
        /// ModeloCrachaController.
        /// </summary>
        ///
        /// <param name="modeloCracha">
        /// O(a) modelo cracha.
        /// </param>
        ///
        /// <returns>
        /// Um(a) object.
        /// </returns>
        private object montaObj(ModeloCracha modeloCracha)
        {
            return new ModeloCrachaModelView
            {
                Codigo = modeloCracha.Codigo,
                Nome = modeloCracha.Nome,
                Html = !string.IsNullOrEmpty(modeloCracha.Html) ?  modeloCracha.Html.Replace("campo","campo edt") : null,
                Fundo = modeloCracha.Fundo == null ? null : Convert.ToBase64String(modeloCracha.Fundo),
                TipoPapel = obterDesc(modeloCracha.TipoPapel),
                Ativo = modeloCracha.Ativo,
                DataRegistro = modeloCracha.DataRegistro,
                Horizontal = modeloCracha.Horizontal
            };
        }

        /// <summary>
        /// Ativar inativar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult AtivarInativar(int codigo)
        {
            IRepositorioModeloCracha rep = Fabrica.Instancia.ObterRepositorio<IRepositorioModeloCracha>(UnidadeTrabalho);
            ModeloCracha modeloCracha = rep.ObterPorId(codigo);
            if (modeloCracha.Ativo)
            {
                modeloCracha.Inativar();
            }
            else
            {
                rep.Validar(modeloCracha, EstadoObjeto.Ativado);
                modeloCracha.Ativar();

            }

            return Json(montaObj(modeloCracha));

        }

    }
}
