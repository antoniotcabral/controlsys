﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Controlsys.Dominio.Audit;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorios.Audit;
using Ext.Net;
using Globalsys;
using Globalsys.Relatorios;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar relatorio auditorias.
    /// </summary>
    [ExtendController]
    public class RelatorioAuditoriaController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /RelatorioAuditoria/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.RelatorioAuditoriaController.
        /// </summary>
        ///
        /// <param name="unidade">
        /// O(a) unidade.
        /// </param>
        public RelatorioAuditoriaController(IUnidadeTrabalho unidade) { UnidadeTrabalho = unidade; }

        /// <summary>
        /// Retorna a página de relatorioAuditoria.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Exportar.
        /// </summary>
        ///
        /// <param name="tipoRelatorio">
        /// O(a) tipo relatorio.
        /// </param>
        /// <param name="selectedFields">
        /// The selected fields.
        /// </param>
        /// <param name="codUsuario">
        /// O(a) cod usuario.
        /// </param>
        /// <param name="codAcao">
        /// O(a) cod acao.
        /// </param>
        /// <param name="codPagina">
        /// O(a) cod pagina.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Exportar(string tipoRelatorio, List<string> selectedFields, string codUsuario, int? codAcao, int? codPagina, DateTime? dtInicio, DateTime? dtFinal)
        {
            IRepositorioAuditoria repositorio = Fabrica.Instancia.ObterRepositorio<IRepositorioAuditoria>(UnidadeTrabalho);
            IQueryable<Auditoria> regs = repositorio.ObterTodos();

            if (!string.IsNullOrWhiteSpace(codUsuario))
                regs = regs.Where(e => e.Usuario.Codigo == codUsuario);

            if (codAcao != null)
                regs = regs.Where(e => e.Acao.Codigo == codAcao);

            if (codPagina != null)
                regs = regs.Where(e => e.Acao.Pagina.Codigo == codPagina);

            if (dtInicio!= null)
                regs = regs.Where(e => e.DataRegistro >= dtInicio);

            if (dtFinal != null)
                regs = regs.Where(e => e.DataRegistro <= dtFinal);

            List<AuditoriaModelView> controls = regs.Select(auditoria => new AuditoriaModelView()
            {
                Codigo = auditoria.Codigo,
                Nome = auditoria.Usuario.PessoaFisica.Nome,
                Documento = (auditoria.Usuario.PessoaFisica.CPF ?? auditoria.Usuario.PessoaFisica.Passaporte),
                Pagina = (auditoria.Acao != null ? auditoria.Acao.Pagina.Nome : null),
                Acao = (auditoria.Acao != null ? auditoria.Acao.Nome : null),
                Descricao = auditoria.Descricao,
                DataHora = auditoria.DataRegistro
            }).ToList();

            string fileName = string.Empty;

            switch (tipoRelatorio)
            {
                case "PDF":
                    fileName = "download.pdf";
                    break;
                case "EXCELOPENXML":
                    fileName = "download.xlsx";
                    break;
                case "WORDOPENXML":
                    fileName = "download.docx";
                    break;
            }

            ReportConfig reportConfig = ReportHelper.RenderReport<AuditoriaModelView>(selectedFields, tipoRelatorio, controls);
            return File(reportConfig.Bytes, reportConfig.MimeType, fileName);
        }


        private IQueryable<Auditoria> FiltrarAuditorias(IQueryable<Auditoria> audits, string codUsuario, int? codPagina, int? codAcao)
        {
            if (!string.IsNullOrEmpty(codUsuario))
                audits = audits.Where(a => a.Usuario.Codigo == codUsuario);

            if (codPagina.HasValue)
                audits = audits.Where(a => a.Acao.Pagina.Codigo == codPagina);

            if (codAcao.HasValue)
                audits = audits.Where(a => a.Acao.Codigo == codAcao);

            audits = audits.OrderByDescending(a => a.DataRegistro);

            return audits;
        }

        private List<AuditarSistemaModelView> montaObj(IQueryable<Auditoria> audits, StoreRequestParameters parameters = null)
        {
            if (parameters == null)
                parameters = new StoreRequestParameters();
            audits = audits.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 30);

            return audits.Select(a => new AuditarSistemaModelView()
            {
                Codigo = a.Codigo,
                Usuario = a.Usuario.PessoaFisica.Nome,
                CPFPassaporte = a.Usuario.PessoaFisica.CPFFormatado() ?? a.Usuario.PessoaFisica.Passaporte,
                Acao = a.Acao != null ? a.Acao.Nome : null,
                Url = a.Acao != null ? a.Acao.Url : null,
                Descricao = a.Descricao,
                Interface = a.Acao != null ? a.Acao.Pagina.Nome : null,
                DataHora = a.DataRegistro,
                CodigoUsuario = a.Usuario.Codigo
            }).ToList();
        }

        public ActionResult Pesquisar(string codUsuario, int? codPagina, int? codAcao, DateTime? dtInicio, DateTime? dtFim)
        {
            AuditarSistemaController controller = new AuditarSistemaController(UnidadeTrabalho);
            return controller.Pesquisar(codUsuario, codPagina, codAcao, dtInicio, dtFim);

            //IRepositorioAuditoria repAudit = Fabrica.Instancia.ObterRepositorio<IRepositorioAuditoria>(UnidadeTrabalho);
            //
            //IQueryable<Auditoria> audits = repAudit.ObterTodos();
            //
            //audits = FiltrarAuditorias(audits, codUsuario, codPagina, codAcao);
            //int total = audits.Count();
            //var data = montaObj(audits);
            //
            //return Json(new
            //{
            //    Total = total,
            //    Data = data
            //}, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Paginacao(StoreRequestParameters parameters, string codUsuario, int? codPagina, int? codAcao, DateTime? dtInicio, DateTime? dtFim)
        {
            AuditarSistemaController controller = new AuditarSistemaController(UnidadeTrabalho);
            return controller.Paginacao(parameters, codUsuario, codPagina, codAcao, dtInicio, dtFim);

            //IRepositorioAuditoria repAudit = Fabrica.Instancia.ObterRepositorio<IRepositorioAuditoria>(UnidadeTrabalho);
            //IQueryable<Auditoria> audits = repAudit.ObterTodos();
            //
            //audits = FiltrarAuditorias(audits, codUsuario, codPagina, codAcao);
            //int total = audits.Count();
            //List<AuditarSistemaModelView> data = montaObj(audits, parameters);
            //
            //return new StoreResult()
            //{
            //    Total = total,
            //    Data = data
            //};
        }

    }
}
