﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Mvc;
using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Pessoas;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorios.Empresas;
using Ext.Net;
using Controlsys.Dominio.Acesso;
using Controlsys.Repositorio.Acesso;
using Controlsys.Infra;
using Globalsys;
using Ext.Net.MVC;
using Globalsys.Relatorios;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class RelatorioPermanenciaDiariaController : Controller, IControllerBase
    {
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        public RelatorioPermanenciaDiariaController(IUnidadeTrabalho unidade) { UnidadeTrabalho = unidade; }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Pesquisar(StoreRequestParameters parameters, DateTime dataInicio, DateTime dataFim, string nome = null, string cpf = null, string passaporte = null, string empresa = null, string cnpj = null, TipoPapel? tipoPapel = null)
        {
            dataFim = dataFim.AddDays(1);
            int total = Total(nome, cpf, passaporte, empresa, cnpj, tipoPapel, dataInicio, dataFim);
            List<RelatorioPermanenciaDiariaModelView> data = FiltrarAcessos(dataInicio, dataFim, nome, cpf, passaporte, empresa, cnpj, tipoPapel, parameters);

            return new StoreResult(data, total);
        }

        private int Total(string nome = null, string cpf = null, string passaporte = null, string empresa = null, string cnpj = null, TipoPapel? tipoPapel = null, DateTime? dataInicio = null, DateTime? dataFim = null)
        {
            IRepositorioControleAcesso rep = Fabrica.Instancia.ObterRepositorio<IRepositorioControleAcesso>(UnidadeTrabalho);
            IQueryable<ControleAcesso> listControleAc = rep.ObterListaAcesso(nome, cpf, passaporte, empresa, cnpj, tipoPapel, dataInicio, dataFim);

            //int qtd = listControleAc.GroupBy(x => x.Papel.Codigo).Select(g => g.First()).Count();
            //int qtd = listControleAc.Select(ca => ca.Papel.Codigo).Distinct().Count();

            int qtd = 0;
            //var listaCods = listControleAc.OrderBy(x => x.Papel.PessoaFisica.Nome).Select(ca => ca.Papel.Codigo).Distinct().ToList();
            var listaCods = listControleAc.Select(ca => ca.Papel.Codigo).Distinct().ToList();
            //listaCods = listaCods.OrderBy(l => l).ToList();

            //foreach (int codPess in listaCods)    
            //{
            //    int pess = codPess;
            //    bool success = false;
            //    var dirEsperada = DirecaoLeitora.Entrada;
            //    var ss = listControleAc.Where(c => c.Papel.Codigo == pess).OrderBy(x => x.Codigo);
            //
            //    //var xx = from lca in listControleAc
            //    //    where lca.Papel.Codigo == pess
            //    //    orderby lca.Codigo
            //    //    select lca;
            //    //if(xx.Count() < 2)
            //    //  continue;
            //
            //    //if (!(ss.Count() > 1))
            //    //    continue;
            //
            //    foreach (var s in ss)
            //    {
            //        if (success)
            //            break;
            //
            //        if (dirEsperada != s.Direcao)
            //            continue;
            //
            //        if (dirEsperada == DirecaoLeitora.Entrada)
            //            dirEsperada = DirecaoLeitora.Saida;
            //        else
            //        {
            //            dirEsperada = DirecaoLeitora.Entrada;
            //            success = true;
            //        }
            //    }
            //    if (success)
            //        qtd++;
            //}

            //return qtd;
            return listaCods.Count;
        }

        private List<RelatorioPermanenciaDiariaModelView> FiltrarAcessos(DateTime dataInicio, DateTime dataFim, string nome = null, string cpf = null, string passaporte = null, string empresa = null, string cnpj = null, TipoPapel? tipoPapel = null, StoreRequestParameters parameters = null, bool ignoreParameters = false)
        {
            IRepositorioControleAcesso rep = Fabrica.Instancia.ObterRepositorio<IRepositorioControleAcesso>(UnidadeTrabalho);
            IQueryable<ControleAcesso> listControleAc = rep.ObterListaAcesso(nome, cpf, passaporte, empresa, cnpj, tipoPapel, dataInicio, dataFim);

            var list = new List<RelatorioPermanenciaDiariaModelView>();
            IEnumerable<int> listaCods = listControleAc.Select(ca => ca.Papel.Codigo).Distinct().ToList();
            listaCods = listaCods.OrderBy(l => l).ToList();

            if (!ignoreParameters)
            {
                if (parameters == null)
                    parameters = new StoreRequestParameters();
                listaCods = listaCods.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 25);
            }

            var dias = dataFim.AddDays(-1).Subtract(dataInicio).Add(new TimeSpan(1, 0, 0, 0)).TotalDays;

            foreach (int codPess in listaCods)
            {
                int pess = codPess;
                int cont = 0;
                var tempo = new TimeSpan();
                var tempoFimdoDia = new TimeSpan();                
                var dtAcessoEntrada = new DateTime();
                var dirEsperada = DirecaoLeitora.Entrada;
                var rel = new RelatorioPermanenciaDiariaModelView();
                foreach (var s in listControleAc.Where(c => c.Papel.Codigo == pess).OrderBy(c => c.DataAcesso).ToList())
                {
                    if (cont == 0)
                    {
                        rel.Cpf = s.Papel.PessoaFisica.CPFFormatado();
                        rel.Passaporte = s.Papel.PessoaFisica.Passaporte;
                        rel.Nome = s.Papel.PessoaFisica.Nome;

                        if (s.Papel is Colaborador)
                        {
                            rel.TipoPapel = "Colaborador";
                            if ((s.Papel as Colaborador).Empresa != null)
                            {
                                rel.Empresa = (s.Papel as Colaborador).Empresa.Nome;
                                rel.Cnpj = (s.Papel as Colaborador).Empresa.CNPJFormatado();
                            }
                        }
                        else if (s.Papel is PrestadorServico)
                        {
                            rel.TipoPapel = "Prestador de serviço";
                            if ((s.Papel as PrestadorServico).Empresa != null)
                            {
                                rel.Empresa = (s.Papel as PrestadorServico).Empresa.Nome;
                                rel.Cnpj = (s.Papel as PrestadorServico).Empresa.CNPJFormatado();
                            }
                        }
                        else if (s.Papel is Visitante)
                        {
                            rel.TipoPapel = "Visitante";
                            rel.Empresa = (s.Papel as Visitante).Empresa;
                        }
                        cont++;
                    }
                    if (dirEsperada != s.Direcao)
                        continue;

                    var diaMesAnoCorrente = new DateTime(s.DataAcesso.Year, s.DataAcesso.Month, s.DataAcesso.Day);

                    if (dirEsperada == DirecaoLeitora.Entrada)
                    {
                        dtAcessoEntrada = s.DataAcesso;
                        dirEsperada = DirecaoLeitora.Saida;

                        if (listControleAc.Count() == 1)
                        {
                            //Tempo até o fim do dia.
                            tempo = new DateTime(dtAcessoEntrada.Year, dtAcessoEntrada.Month, dtAcessoEntrada.Day).AddDays(1).AddSeconds(-1) - s.DataAcesso;
                        }                   
                    }
                    else
                    {
                        // Verifica se ele virou a noite dentro da empresa;
                        if (new TimeSpan(s.DataAcesso.Hour, s.DataAcesso.Minute, s.DataAcesso.Second) <
                            new TimeSpan(dtAcessoEntrada.Hour, dtAcessoEntrada.Minute, dtAcessoEntrada.Second))
                        {
                            //Entrada
                            tempo += new DateTime(dtAcessoEntrada.Year, dtAcessoEntrada.Month, dtAcessoEntrada.Day).AddDays(1).AddSeconds(-1) - dtAcessoEntrada;
                            //Saída
                            tempo += s.DataAcesso - diaMesAnoCorrente;
                        }
                        else
                            tempo += s.DataAcesso - dtAcessoEntrada;

                        dirEsperada = DirecaoLeitora.Entrada;
                    }

                }

                if (string.IsNullOrEmpty(rel.Nome)) continue;

                double secs = 0;
                if (tempo.TotalSeconds > 0)
                    secs = tempo.TotalSeconds / dias;
                rel.Tempo = new DateTime().AddSeconds(secs);

                list.Add(rel);
            }

            return list;
        }

        private List<RelatorioPermanenciaDiariaModelView> FiltrarAcessos2(string nome = null, string cpf = null, string passaporte = null, string empresa = null, string cnpj = null, TipoPapel? tipoPapel = null, DateTime? dataInicio = null, DateTime? dataFim = null, StoreRequestParameters parameters = null, bool ignoreParameters = false)
        {
            IRepositorioControleAcesso rep = Fabrica.Instancia.ObterRepositorio<IRepositorioControleAcesso>(UnidadeTrabalho);
            IQueryable<ControleAcesso> listControleAc = rep.ObterListaAcesso(nome, cpf, passaporte, empresa, cnpj, tipoPapel, dataInicio, dataFim);

            var list = new List<RelatorioPermanenciaDiariaModelView>();
            IEnumerable<int> listaCods = listControleAc.Select(ca => ca.Papel.Codigo).Distinct().ToList();
            listaCods = listaCods.OrderBy(l => l).ToList();

            if (!ignoreParameters)
            {
                if (parameters == null)
                    parameters = new StoreRequestParameters();
                listaCods = listaCods.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 25);
            }

            foreach (int codPess in listaCods)
            {
                int pess = codPess;
                int cont = 0;
                int dias = 0;
                var tempo = new TimeSpan();
                var dtUltimaData = new DateTime();
                var dtAcessoEntrada = new DateTime();
                var dirEsperada = DirecaoLeitora.Entrada;
                var rel = new RelatorioPermanenciaDiariaModelView();
                foreach (var s in listControleAc.Where(c => c.Papel.Codigo == pess).OrderBy(c => c.DataAcesso).ToList())
                {
                    if (dirEsperada != s.Direcao)
                        continue;

                    if (cont == 0)
                    {
                        rel.Cpf = s.Papel.PessoaFisica.CPFFormatado();
                        rel.Passaporte = s.Papel.PessoaFisica.Passaporte;
                        rel.Nome = s.Papel.PessoaFisica.Nome;

                        if (s.Papel is Colaborador)
                        {
                            rel.TipoPapel = "Colaborador";
                            if ((s.Papel as Colaborador).Empresa != null)
                            {
                                rel.Empresa = (s.Papel as Colaborador).Empresa.Nome;
                                rel.Cnpj = (s.Papel as Colaborador).Empresa.CNPJFormatado();
                            }
                        }
                        else if (s.Papel is PrestadorServico)
                        {
                            rel.TipoPapel = "Prestador de serviço";
                            if ((s.Papel as PrestadorServico).Empresa != null)
                            {
                                rel.Empresa = (s.Papel as PrestadorServico).Empresa.Nome;
                                rel.Cnpj = (s.Papel as PrestadorServico).Empresa.CNPJFormatado();
                            }
                        }
                        else if (s.Papel is Visitante)
                        {
                            rel.TipoPapel = "Visitante";
                            rel.Empresa = (s.Papel as Visitante).Empresa;
                        }
                        cont++;
                    }

                    var diaMesAnoCorrente = new DateTime(s.DataAcesso.Year, s.DataAcesso.Month, s.DataAcesso.Day);
                    if (diaMesAnoCorrente != dtUltimaData)
                        dias++;
                    dtUltimaData = diaMesAnoCorrente;

                    if (dirEsperada == DirecaoLeitora.Entrada)
                    {
                        dtAcessoEntrada = s.DataAcesso;
                        dirEsperada = DirecaoLeitora.Saida;
                    }
                    else
                    {
                        // Verifica se ele virou a noite dentro da empresa;
                        if (new TimeSpan(s.DataAcesso.Hour, s.DataAcesso.Minute, s.DataAcesso.Second) <
                            new TimeSpan(dtAcessoEntrada.Hour, dtAcessoEntrada.Minute, dtAcessoEntrada.Second))
                        {
                            //Entrada
                            tempo += dtAcessoEntrada - new DateTime(dtAcessoEntrada.Year, dtAcessoEntrada.Month, dtAcessoEntrada.Day).AddDays(1).AddSeconds(-1);
                            //Saída
                            tempo += s.DataAcesso - diaMesAnoCorrente;
                        }
                        else
                            tempo += s.DataAcesso - dtAcessoEntrada;

                        dirEsperada = DirecaoLeitora.Entrada;
                    }

                }

                if (string.IsNullOrEmpty(rel.Nome)) continue;

                var secs = tempo.TotalSeconds / dias;
                rel.Tempo = new DateTime().AddSeconds(secs);

                list.Add(rel);
            }

            return list;
        }

        public ActionResult Exportar(List<string> selectedFields, string tipoRelatorio, DateTime dataInicio, DateTime dataFim, string nome = null, string cpf = null, string passaporte = null, string empresa = null, string cnpj = null, TipoPapel? tipoPapel = null, StoreRequestParameters parameters = null)
        {
            dataFim = dataFim.AddDays(1);
            var lista = FiltrarAcessos(dataInicio, dataFim, nome, cpf, passaporte, empresa, cnpj, tipoPapel, parameters,true);

            lista.ForEach(x => x.TempoAux = x.Tempo.TimeOfDay);

            ReportConfig reportConfig = ReportHelper.RenderReport<RelatorioPermanenciaDiariaModelView>(selectedFields, tipoRelatorio, lista);

            string fileName = string.Empty;
            switch (tipoRelatorio)
            {
                case "PDF":
                    fileName = "download.pdf";
                    break;
                case "EXCELOPENXML":
                    fileName = "download.xlsx";
                    break;
                case "WORDOPENXML":
                    fileName = "download.docx";
                    break;
            }

            return File(reportConfig.Bytes, reportConfig.MimeType, fileName);
        }

    }
}
