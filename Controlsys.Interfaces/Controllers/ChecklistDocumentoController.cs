﻿using Controlsys.Dominio.Parametros;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorios.Parametros;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar checklist documentoes.
    /// </summary>
    [ExtendController]
    public class ChecklistDocumentoController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /ChecklistDocumento/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.ChecklistDocumentoController.
        /// </summary>
        ///
        /// <param name="unidade">
        /// O(a) unidade.
        /// </param>
        public ChecklistDocumentoController(IUnidadeTrabalho unidade) { UnidadeTrabalho = unidade; }

        /// <summary>
        /// Retorna a página de checklistDocumento.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Inclui um novo checklistDocumento.
        /// </summary>
        ///
        /// <param name="checkDoc">
        /// O(a) check document.
        /// </param>
        /// <param name="tiposDoc">
        /// O(a) tipos document.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Incluir(ChecklistDocumento checkDoc, List<TipoDocumento> tiposDoc)
        {
            IRepositorioChecklistDocumento repCheckDoc = Fabrica.Instancia.ObterRepositorio<IRepositorioChecklistDocumento>(UnidadeTrabalho);
            checkDoc.DataRegistro = DateTime.Now;
            checkDoc.Ativar();
            repCheckDoc.Validar(checkDoc, EstadoObjeto.Novo);
            repCheckDoc.Salvar(checkDoc);

            if (tiposDoc != null)
            {
                foreach (var item in tiposDoc)
                {
                    ChecklistTipoDocumento checkTipo = new ChecklistTipoDocumento();
                    checkTipo.ChecklistDocumento = checkDoc;
                    checkTipo.TipoDocumento = item;
                    checkTipo.DataRegistro = DateTime.Now;
                    checkTipo.Ativar();
                    repCheckDoc.SalvarCheckListTipoDoc(checkTipo);
                }
            }
            return Json(montaObj(checkDoc));
        }

        /// <summary>
        /// Altera um registro de checklistDocumento.
        /// </summary>
        ///
        /// <param name="checkDoc">
        /// O(a) check document.
        /// </param>
        /// <param name="tiposDocDel">
        /// O(a) tipos document delete.
        /// </param>
        /// <param name="tiposDocNew">
        /// O(a) tipos document new.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Alterar(ChecklistDocumento checkDoc, int[] tiposDocDel, int[] tiposDocNew)
        {
            IRepositorioChecklistDocumento repCheckDoc = Fabrica.Instancia.ObterRepositorio<IRepositorioChecklistDocumento>(UnidadeTrabalho);
            ChecklistDocumento checklistDoc = repCheckDoc.ObterPorId(checkDoc.Codigo);
            checklistDoc.Descricao = checkDoc.Descricao;
            checklistDoc.Nome = checkDoc.Nome;
            checklistDoc.TipoCadastro = checkDoc.TipoCadastro;
            repCheckDoc.Validar(checklistDoc, EstadoObjeto.Alterado);
            if (tiposDocDel != null)
            {
                foreach (var item in checklistDoc.TiposDocumento.Where(t => tiposDocDel.Any(tdd => tdd == t.TipoDocumento.Codigo)))
                {
                    ChecklistTipoDocumento checkTipo = item;
                    checkTipo.Inativar();
                }
            }
            if (tiposDocNew != null)
            {
                foreach (var item in tiposDocNew)
                {
                    ChecklistTipoDocumento checkTipo = new ChecklistTipoDocumento();
                    checkTipo.ChecklistDocumento = checkDoc;
                    checkTipo.TipoDocumento =  new TipoDocumento {Codigo = item};
                    checkTipo.DataRegistro = DateTime.Now;
                    checkTipo.Ativar();
                    repCheckDoc.SalvarCheckListTipoDoc(checkTipo);
                }
            }
            return Json(montaObj(checklistDoc));
        }

        /// <summary>
        /// Cria um objeto contendo informações relacionadas ao conteudo gerenciado por
        /// ChecklistDocumentoController.
        /// </summary>
        ///
        /// <param name="checkDoc">
        /// O(a) check document.
        /// </param>
        ///
        /// <returns>
        /// Um(a) object.
        /// </returns>
        private object montaObj(ChecklistDocumento checkDoc)
        {
            return new ChecklistDocumentoModelView
            {
                Codigo = checkDoc.Codigo,
                Nome = checkDoc.Nome,
                Descricao = checkDoc.Descricao,
                TipoCadastro = obterDesc(checkDoc.TipoCadastro),
                Ativo = checkDoc.Ativo,
                TiposDocumento = checkDoc.TiposDocumento
                    .Select(td => new TipoDocumentoModelView
                    {
                        Codigo = td.TipoDocumento.Codigo,
                        Sigla = td.TipoDocumento.Sigla
                    }).ToList()
            };
        }

        /// <summary>
        /// Ativar inativar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult AtivarInativar(int codigo)
        {
            IRepositorioChecklistDocumento repCheckDoc = Fabrica.Instancia.ObterRepositorio<IRepositorioChecklistDocumento>(UnidadeTrabalho);
            ChecklistDocumento checklistDoc = repCheckDoc.ObterPorId(codigo);
            if (checklistDoc.Ativo)
            {
                checklistDoc.Inativar();
            }
            else
            {
                repCheckDoc.Validar(checklistDoc, EstadoObjeto.Ativado);
                checklistDoc.Ativar();
            }
            
            return Json(montaObj(checklistDoc));
        }

        /// <summary>
        /// Selecionar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Selecionar(int codigo)
        {
            IRepositorioChecklistDocumento repCheckDoc = Fabrica.Instancia.ObterRepositorio<IRepositorioChecklistDocumento>(UnidadeTrabalho);
            ChecklistDocumento checkDoc = repCheckDoc.ObterPorId(codigo);

            return Json(new 
            {
                Codigo = checkDoc.Codigo,
                Nome = checkDoc.Nome,
                Descricao = checkDoc.Descricao,
                TipoCadastro = checkDoc.TipoCadastro,
                Ativo = checkDoc.Ativo,
                TiposDocumento = checkDoc.TiposDocumento
                    .Select(td => new 
                    {
                        Codigo = td.TipoDocumento.Codigo,
                        Sigla = td.TipoDocumento.Sigla
                    }).ToList()
            });
        }

        /// <summary>
        /// Retorna todos os objetos do tipo ChecklistDocumento.
        /// </summary>
        ///
        /// <returns>
        /// Detalhes do tipo de objeto ligado ao controller no formato JSON.
        /// </returns>
        public ActionResult ObterTodos()
        {
            IRepositorioChecklistDocumento repCheck = Fabrica.Instancia.ObterRepositorio<IRepositorioChecklistDocumento>(UnidadeTrabalho);
            return Json(repCheck.ObterTodos()
                               .Select(s => new
                               {
                                   Codigo = s.Codigo,
                                   Nome = s.Nome,
                                   Descricao = s.Descricao,
                                   TipoCadastro = obterDesc(s.TipoCadastro),
                                   Ativo = s.Ativo
                               })
                                .ToList(), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Obter description.
        /// </summary>
        ///
        /// <param name="tipoCadastro">
        /// O(a) tipo cadastro.
        /// </param>
        ///
        /// <returns>
        /// Um(a) string.
        /// </returns>
        private string obterDesc(TipoCadastro tipoCadastro)
        {
            return EnumExtensoes.ObterDescricaoEnum(tipoCadastro);
        }

        /// <summary>
        /// Lista tipo cadastro.
        /// </summary>
        ///
        /// <param name="query">
        /// O(a) query.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ListaTipoCadastro(string query)
        {
            Dictionary<int, string> dic = new Dictionary<int, string>();

            query = query.ToUpper();
            foreach (int item in Enum.GetValues(typeof(TipoCadastro)))
            {
                var reg = ((TipoCadastro)item).ObterDescricaoEnum();
                if (string.IsNullOrEmpty(reg)) continue;
                reg = reg.ToUpper();
                if (reg.Contains(query))
                    dic.Add(item, ((TipoCadastro)item).ObterDescricaoEnum());
            }
            return Json(dic.Select(d => new
            {
                Codigo = d.Key,
                Descricao = d.Value
            }).ToList());

        }

        /// <summary>
        /// Obter todos por tipo cadastro.
        /// </summary>
        ///
        /// <param name="parametroTipoCadastro">
        /// O(a) parametro tipo cadastro.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ObterTodosPorTipoCadastro(TipoCadastro parametroTipoCadastro)
        {
            
                IRepositorioChecklistDocumento repCheck = Fabrica.Instancia.ObterRepositorio<IRepositorioChecklistDocumento>(UnidadeTrabalho);
                ChecklistDocumento ch = repCheck.ObterTodosPorTipoCadastro(parametroTipoCadastro);

                if (ch != null)
                {
                    return Json(ch.TiposDocumento.Select(td => new
                    {
                        Codigo = td.TipoDocumento.Codigo,
                        Sigla = td.TipoDocumento.Sigla
                        //,
                        //DiasValidade = td.TipoDocumento.DiasValidade
                    }).ToList(), JsonRequestBehavior.AllowGet);
                }
                return null;

        }   

    }
}
