﻿using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Pessoas;
using Controlsys.Infra;
using Controlsys.Repositorio.Acesso;
using Controlsys.Repositorios.Acesso;
using Controlsys.Repositorios.Pessoas;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar Grupo Acesso Temporario.
    /// </summary>
    [ExtendController]
    public class AcessoTemporarioController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /GrupoAcesso/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.GrupoAcessoController.
        /// </summary>
        ///
        /// <param name="unidade">
        /// O(a) unidade.
        /// </param>
        public AcessoTemporarioController(IUnidadeTrabalho unidade)
        {
            UnidadeTrabalho = unidade;
        }

        public ActionResult Index()
        {
            return View();
        }

        [Transaction]
        public ActionResult Incluir(int codigoPapel, int codigoGrupoLeitora, DateTime dtValidadeInicio, DateTime hrValidadeInicio, DateTime dtValidadeFim, DateTime hrValidadeFim)
        {

            IRepositorioAcessoTemporario repAcessoTemp = Fabrica.Instancia.ObterRepositorio<IRepositorioAcessoTemporario>(UnidadeTrabalho);
            IRepositorioGrupoLeitora repGrupoLeitora = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoLeitora>(UnidadeTrabalho);
            IRepositorioPapel repPapel = Fabrica.Instancia.ObterRepositorio<IRepositorioPapel>(UnidadeTrabalho);

            Papel papel = repPapel.ObterPorId(codigoPapel);
            GrupoLeitora grupoLeitora = repGrupoLeitora.ObterPorId(codigoGrupoLeitora);

            AcessoTemporario _acessoTemp = new AcessoTemporario();

            _acessoTemp.DataHoraValidadeFim = new DateTime(dtValidadeFim.Year, dtValidadeFim.Month, dtValidadeFim.Day, hrValidadeFim.Hour, hrValidadeFim.Minute, hrValidadeFim.Second);
            _acessoTemp.DataHoraValidadeInicio = new DateTime(dtValidadeInicio.Year, dtValidadeInicio.Month, dtValidadeInicio.Day, hrValidadeInicio.Hour, hrValidadeInicio.Minute, hrValidadeInicio.Second);
            _acessoTemp.DataRegistro = DateTime.Now;
            _acessoTemp.GrupoLeitora = grupoLeitora;
            _acessoTemp.Papel = papel;
            _acessoTemp.Ativar();

            repAcessoTemp.Validar(_acessoTemp, EstadoObjeto.Novo);
            repAcessoTemp.Salvar(_acessoTemp);

            IRepositorioSincronizaPessoaAcesso repsinc = Fabrica.Instancia.ObterRepositorio<IRepositorioSincronizaPessoaAcesso>(UnidadeTrabalho);
            IRepositorioGrupoAcesso repGrupoAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoAcesso>(UnidadeTrabalho);
            repsinc.Sincronizar(papel, TipoSincronizacaoPessoa.Conceder, null, repGrupoAcesso.ObterControladoras(new List<GrupoLeitora>() { grupoLeitora }.AsEnumerable()));

            return null;
        }
        public ActionResult Pesquisar(StoreRequestParameters parameters, string nome, int? codigoGrupoLeitora, bool? status)
        {
            var acessos = FiltrarPesquisa(nome, codigoGrupoLeitora, status);

            int total = acessos.Count();
            var data = montaListaObj(acessos, parameters);
            return new StoreResult(data, total);
        }
        private IQueryable<AcessoTemporario> FiltrarPesquisa(string nome, int? codigoGrupoLeitora, bool? status)
        {
            IRepositorioAcessoTemporario repAcessoTemp = Fabrica.Instancia.ObterRepositorio<IRepositorioAcessoTemporario>(UnidadeTrabalho);
            IQueryable<AcessoTemporario> listaAcessosTemp = repAcessoTemp.ObterTodos().OrderBy(p => p.Papel.PessoaFisica.Nome);

            if (!string.IsNullOrEmpty(nome))
                listaAcessosTemp = listaAcessosTemp.Where(at => at.Papel.PessoaFisica.Nome.Contains(nome));

            if (codigoGrupoLeitora.HasValue)
                listaAcessosTemp = listaAcessosTemp.Where(p => p.GrupoLeitora.Codigo == codigoGrupoLeitora.Value);

            if (status.HasValue)
                listaAcessosTemp = listaAcessosTemp.Where(p => p.Ativo == status.Value);

            return listaAcessosTemp;
        }
        private List<AcessoTemporario> montaListaObj(IQueryable<AcessoTemporario> acessos, StoreRequestParameters parameters = null, bool ignoreParameters = false)
        {
            try
            {
                if (!ignoreParameters)
                {
                    if (parameters == null)
                        parameters = new StoreRequestParameters();
                    int total = acessos.Count();
                    acessos = acessos.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 25);
                }
                return acessos.Select(p => montaObj(p)).ToList();
            }
            catch (Exception)
            { throw; }
        }
        public AcessoTemporario montaObj(AcessoTemporario lista)
        {
            var _listaAcessosTemp = new AcessoTemporario();
            _listaAcessosTemp.Codigo = lista.Codigo;
            _listaAcessosTemp.Nome = lista.Papel.PessoaFisica.Nome;
            _listaAcessosTemp.GrupoLeitoraNome = lista.GrupoLeitora.Nome;
            _listaAcessosTemp.DtHrValidadeInicio = lista.DataHoraValidadeInicio.ToString("dd/MM/yyyy HH:mm");
            _listaAcessosTemp.DtHrValidadeFim = lista.DataHoraValidadeFim.ToString("dd/MM/yyyy HH:mm");
            _listaAcessosTemp.AtivoTemp = lista.Ativo;
            return _listaAcessosTemp;
        }
        /// <summary>
        /// Ativar inativar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult AtivarInativar(int codigo)
        {
            IRepositorioAcessoTemporario repAcessoTemp = Fabrica.Instancia.ObterRepositorio<IRepositorioAcessoTemporario>(UnidadeTrabalho);
            AcessoTemporario acessoTemp = repAcessoTemp.ObterPorId(codigo);

            if (acessoTemp.Ativo)
                acessoTemp.Inativar();
            else
            {
                repAcessoTemp.Validar(acessoTemp, EstadoObjeto.Ativado);
                acessoTemp.Ativar();
            }

            repAcessoTemp.Atualizar(acessoTemp);

            IRepositorioSincronizaPessoaAcesso repsinc = Fabrica.Instancia.ObterRepositorio<IRepositorioSincronizaPessoaAcesso>(UnidadeTrabalho);
            IRepositorioGrupoAcesso repGrupoAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoAcesso>(UnidadeTrabalho);

            if (acessoTemp.Ativo)
                repsinc.Sincronizar(acessoTemp.Papel, TipoSincronizacaoPessoa.Conceder, null, repGrupoAcesso.ObterControladoras(new List<GrupoLeitora>() { acessoTemp.GrupoLeitora }.AsEnumerable()));
            else
                repsinc.Sincronizar(acessoTemp.Papel, TipoSincronizacaoPessoa.Suspender, null, repGrupoAcesso.ObterControladoras(new List<GrupoLeitora>() { acessoTemp.GrupoLeitora }.AsEnumerable()));
            
            var jsonAcessoTemp = new
            {
                Codigo = acessoTemp.Codigo,
                Nome = acessoTemp.Papel.PessoaFisica.Nome,
                GrupoLeitoraNome = acessoTemp.GrupoLeitora.Nome,
                DtHrValidadeInicio = acessoTemp.DataHoraValidadeInicio.ToString("dd/MM/yyyy HH:mm"),
                DtHrValidadeFim = acessoTemp.DataHoraValidadeFim.ToString("dd/MM/yyyy HH:mm"),
                AtivoTemp = acessoTemp.Ativo
            };
            return Json(jsonAcessoTemp);

        }

    }
}
