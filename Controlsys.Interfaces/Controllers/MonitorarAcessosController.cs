﻿using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Controlsys.Dominio.Seguranca;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Acesso;
using Controlsys.Repositorios.Parametros;
using Controlsys.Repositorios.Seguranca;
using Globalsys;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mail;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar monitorar acessos.
    /// </summary>
    [ExtendController]
    public class MonitorarAcessosController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /MonitorarAcessos/.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Sob analise information.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult SobAnaliseInfo()
        {
            return PartialView();
        }

        /// <summary>
        /// Gets/Sets valor para UnidadeTrabalho.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.MonitorarAcessosController.
        /// </summary>
        ///
        /// <param name="unidade">
        /// O(a) unidade.
        /// </param>
        public MonitorarAcessosController(IUnidadeTrabalho unidade)
        {
            UnidadeTrabalho = unidade;
        }

        /// <summary>
        /// Retorna todos os objetos do tipo MonitorarAcessos.
        /// </summary>
        ///
        /// <param name="datahora">
        /// O(a) datahora.
        /// </param>
        /// <param name="ComImagem">
        /// true to com imagem.
        /// </param>
        ///
        /// <returns>
        /// Detalhes do tipo de objeto ligado ao controller no formato JSON.
        /// </returns>
        public ActionResult ObterTodos(string datahora = null, bool ComImagem = true)
        {
            IRepositorioControleAcesso rep = Fabrica.Instancia.ObterRepositorio<IRepositorioControleAcesso>(UnidadeTrabalho);
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            IRepositorioPagina repPagina = Fabrica.Instancia.ObterRepositorio<IRepositorioPagina>(UnidadeTrabalho);

            Pagina paginaColaborador = repPagina.ObterTodos().FirstOrDefault(p => p.Url == "/Colaborador/Index");
            Pagina paginaVisitante = repPagina.ObterTodos().FirstOrDefault(p => p.Url == "/Visitante/Index");
            Pagina paginaPrestador = repPagina.ObterTodos().FirstOrDefault(p => p.Url == "/PrestadorServico/Index");

            List<ControleAcesso> listControleAc;
            List<MonitorarAcessosModelView> listaRetorno = new List<MonitorarAcessosModelView>();

            if (!string.IsNullOrEmpty(datahora))
            {

                DateTime dataIini = Convert.ToDateTime(datahora).AddSeconds(-10);
                DateTime dataFim = Convert.ToDateTime(datahora).AddSeconds(10);
                listControleAc = rep.ObterTodos().Where(p => p.DataAcesso > dataIini && p.DataAcesso < dataFim).ToList();
            }
            else
                listControleAc = rep.ObterTodos().OrderByDescending(p => p.DataAcesso).Take(30).ToList();

            string userLogado = HttpContext.User.Identity.Name;
            Usuario usuario = repUsuario.ObterPorLogin(userLogado);

            //grupo de leitoras
            int[] leitoras = usuario.GruposUsuarios
                                    .SelectMany(gu => gu.Grupo.GruposLeitoras.Select(gr => gr.GrupoLeitora))
                                    .SelectMany(gl => gl.Leitoras.Select(lgl => lgl.Leitora.Codigo))
                                    .Distinct()
                                    .ToArray();

            listControleAc = listControleAc.Where(ca => leitoras.Contains(ca.Leitora.Codigo)).ToList();

            //grupo de usuários - verificar se pode ver sobanalise
            IRepositorioGrupoUsuario repGrupoUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoUsuario>(UnidadeTrabalho);
            IQueryable<GrupoUsuario> gruposUsuarios = repGrupoUsuario.ObterTodos().Where(x => x.Usuario.Login == usuario.Login && x.Ativo == true);

            IRepositorioParametro repParam = Fabrica.Instancia.ObterRepositorio<IRepositorioParametro>(UnidadeTrabalho);
            Parametro param = repParam.RecuperarParametro(ParametroSistema.GrupoSobanalise);

            bool hasValueGroupUser = false;
            hasValueGroupUser = gruposUsuarios.Any(g => g.Grupo.Codigo.ToString() == param.Valor);

            foreach (var item in listControleAc)
            {
                MonitorarAcessosModelView monitorarAcessosModelView = new MonitorarAcessosModelView();

                monitorarAcessosModelView.Codigo = item.Codigo;

                monitorarAcessosModelView.NumCracha = item.Cracha != null ? item.Cracha.Numero : item.NumeroSerie;

                if (item.Papel != null)
                {
                    monitorarAcessosModelView.CodigoPapel = item.Papel.Codigo;
                    monitorarAcessosModelView.Nome = item.Papel.PessoaFisica.Nome;
                    monitorarAcessosModelView.Apelido = item.Papel.PessoaFisica.Apelido;

                    if (item.Papel.Cargo != null)
                        monitorarAcessosModelView.Cargo = item.Papel.Cargo.Nome;

                    if (item.Papel is Colaborador)
                    {
                        monitorarAcessosModelView.Papel = "Colaborador";
                        monitorarAcessosModelView.Empresa = ((Colaborador)item.Papel).Empresa.Apelido;
                        monitorarAcessosModelView.CodigoPagina = paginaColaborador.Codigo;
                    }
                    if (item.Papel is PrestadorServico)
                    {
                        monitorarAcessosModelView.Papel = "Prestador de Serviço";
                        if (((PrestadorServico)item.Papel).TipoPrestador == TipoPrestador.Temporario)
                        {
                            if (((PrestadorServico)item.Papel).Empresa != null)
                                monitorarAcessosModelView.Empresa = ((PrestadorServico)item.Papel).Empresa.Nome;
                        }
                        else
                        {
                            if (((PrestadorServico)item.Papel).Fornecedor != null)
                                monitorarAcessosModelView.Empresa = ((PrestadorServico)item.Papel).Fornecedor.RazaoSocial;
                        }

                        monitorarAcessosModelView.CodigoPagina = paginaPrestador.Codigo;
                    }
                    if (item.Papel is Visitante)
                    {
                        monitorarAcessosModelView.Papel = "Visitante";
                        monitorarAcessosModelView.Empresa = ((Visitante)item.Papel).Empresa;
                        monitorarAcessosModelView.Cargo = ((Visitante)item.Papel).Funcao;
                        monitorarAcessosModelView.CodigoPagina = paginaVisitante.Codigo;
                    }

                    monitorarAcessosModelView.CPF = item.Papel.PessoaFisica.CPFFormatado();
                    monitorarAcessosModelView.Passaporte = item.Papel.PessoaFisica.Passaporte;

                    if (item.Papel.SobAnalise)
                        monitorarAcessosModelView.Sobanalise = "Sim";
                    else
                        monitorarAcessosModelView.Sobanalise = "Não";

                    if (item.Papel.PessoaFisica.Foto != null)
                        monitorarAcessosModelView.Img = Convert.ToBase64String(item.Papel.PessoaFisica.Foto);

                }

                monitorarAcessosModelView.Leitora = item.Leitora.Descricao;
                monitorarAcessosModelView.DataHora = item.DataAcesso.ToString("dd/MM/yyyy HH:mm:ss");
                monitorarAcessosModelView.StatusAcesso = item.StatusAcesso.ObterDescricaoEnum();
                monitorarAcessosModelView.Local = item.Local;
                monitorarAcessosModelView.Direcao = item.Direcao.ObterDescricaoEnum();

                monitorarAcessosModelView.PermitidoSobAnalise = hasValueGroupUser;
                listaRetorno.Add(monitorarAcessosModelView);
            }

            return Json(listaRetorno.ToList());
        }

        //private string GravarTemporario(byte[] foto)
        //{
        //    if (foto != null)
        //    {
        //        Random randNum = new Random();
        //        int number = randNum.Next(DateTime.Now.Millisecond);

        //        string nomeArquivo = number + ".jpeg";

        //        byte[] b = foto;

        //        using (FileStream fs = new FileStream(Server.MapPath("~/Content/img/webcam/") + nomeArquivo, FileMode.Create))
        //            fs.Write(b, 0, b.Length);

        //        return "/Content/img/webcam/" + nomeArquivo;
        //    }
        //    return "/Content/img/avatar.png";
        //}

        /// <summary>
        /// Selecionar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        /// <param name="ComImagem">
        /// true to com imagem.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Selecionar(int codigo, bool ComImagem = true)
        {
            IRepositorioControleAcesso rep = Fabrica.Instancia.ObterRepositorio<IRepositorioControleAcesso>(UnidadeTrabalho);

            return Json(montaObj(rep.ObterPorId(codigo), ComImagem), JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// Cria um objeto contendo informações relacionadas ao conteudo gerenciado por
        /// MonitorarAcessosController.
        /// </summary>
        ///
        /// <param name="controleAcesso">
        /// O(a) controle acesso.
        /// </param>
        /// <param name="ComImagem">
        /// true to com imagem.
        /// </param>
        ///
        /// <returns>
        /// Um(a) object.
        /// </returns>
        private object montaObj(ControleAcesso controleAcesso, bool ComImagem)
        {
            var monitorarAcessosView = new MonitorarAcessosModelView();

            monitorarAcessosView.Codigo = controleAcesso.Codigo;

            monitorarAcessosView.DataHora = controleAcesso.DataAcesso.ToString("dd/MM/yyyy HH:mm");
            monitorarAcessosView.Direcao = controleAcesso.Direcao.ObterDescricaoEnum();
            monitorarAcessosView.Local = controleAcesso.Local;
            monitorarAcessosView.Leitora = controleAcesso.Leitora.Descricao;
            if (controleAcesso.Papel != null)
            {
                monitorarAcessosView.Apelido = controleAcesso.Papel.PessoaFisica.Apelido;

                if (controleAcesso.Papel.Cargo != null)
                    monitorarAcessosView.Cargo = controleAcesso.Papel.Cargo.Nome;
                else if (controleAcesso.Papel is Visitante)
                    monitorarAcessosView.Cargo = (controleAcesso.Papel as Visitante).Funcao;

                monitorarAcessosView.CPF = controleAcesso.Papel.PessoaFisica.CPFFormatado();
                monitorarAcessosView.Email = controleAcesso.Papel.PessoaFisica.Email;
                monitorarAcessosView.Empresa = controleAcesso.Papel is Colaborador ?
                                                 ((Colaborador)controleAcesso.Papel).Empresa.Apelido : "";

                monitorarAcessosView.Nome = controleAcesso.Papel.PessoaFisica.Nome;
                monitorarAcessosView.Passaporte = controleAcesso.Papel.PessoaFisica.Passaporte;

                if (controleAcesso.Papel.SobAnalise)
                    monitorarAcessosView.Sobanalise = "Sim";
                else
                    monitorarAcessosView.Sobanalise = "Não";

                if (controleAcesso.Papel.Supervisionado)
                    monitorarAcessosView.Supervisionado = "Sim";
                else
                    monitorarAcessosView.Supervisionado = "Não";

                if (controleAcesso.Papel.AntiPassBack)
                    monitorarAcessosView.Antipassback = "Sim";
                else
                    monitorarAcessosView.Antipassback = "Não";

                if (controleAcesso.Papel.PessoaFisica.Telefones.Any())
                    monitorarAcessosView.Telefone = controleAcesso.Papel.PessoaFisica.Telefones[0].TelefoneNum;

                if (controleAcesso.Papel.PessoaFisica.Foto != null)
                    monitorarAcessosView.Img = Convert.ToBase64String(controleAcesso.Papel.PessoaFisica.Foto);//ComImagem ? GravarTemporario(controleAcesso.Papel.PessoaFisica.Foto) : "/Content/img/avatar.png";
            }

            monitorarAcessosView.StatusAcesso = controleAcesso.StatusAcesso.ObterDescricaoEnum();

            return monitorarAcessosView;
        }

        [AllowAnonymous]
        /// <summary>
        /// Enviar email.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult EnviarEmail(int codigo)
        {
            IRepositorioControleAcesso rep = Fabrica.Instancia.ObterRepositorio<IRepositorioControleAcesso>(UnidadeTrabalho);
            IRepositorioParametro repParam = Fabrica.Instancia.ObterRepositorio<IRepositorioParametro>(UnidadeTrabalho);
            IRepositorioGrupo repGrupo = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupo>(UnidadeTrabalho);
            ControleAcesso ca = rep.ObterPorId(codigo);

            if (!ca.Papel.SobAnalise)
                throw new Exception("O acesso '{ca.Codigo}' do papel '{ca.Papel.Codigo}' não está sob análise.");

            Dictionary<string, string> valoresSubs = new Dictionary<string, string>();

            valoresSubs.Add("$$CPF$$", ca.Papel.PessoaFisica.CPFFormatado());
            valoresSubs.Add("$$Passporte$$", ca.Papel.PessoaFisica.Passaporte);
            valoresSubs.Add("$$Nome$$", ca.Papel.PessoaFisica.Nome);
            valoresSubs.Add("$$DataHora$$", ca.DataAcesso.ToString("dd/MM/yyyy HH:mm"));
            valoresSubs.Add("$$Local$$", ca.Local);
            valoresSubs.Add("$$Direcao$$", ca.Direcao.ObterDescricaoEnum());

            Grupo g = repGrupo.ObterPorId(int.Parse(repParam.RecuperarParametro(ParametroSistema.GrupoSobanalise).Valor));

            foreach (var item in g.GruposUsuarios.Where(p => p.Ativo))
            {
                Globalsys.Util.Email.Enviar("[CONTROLSYS] Pessoa Sob Análise",
                                              Server.MapPath("/Content/templates/pessoaSobanalise.htm"),
                                              valoresSubs,
                                              string.Empty,
                                              null,
                                              item.Usuario.PessoaFisica.Email);
            }


            return Json(null);
        }
    }
}

