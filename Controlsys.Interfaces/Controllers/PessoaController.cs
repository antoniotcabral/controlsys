﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Controlsys.Dominio.Empresas;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Parametros;
using Controlsys.Repositorios.Empresas;
using Globalsys;
using Controlsys.Dominio.Pessoas;
using Controlsys.Repositorios.Pessoas;
using Controlsys.Infra;
using Controlsys.Dominio.Enderecos;
using Controlsys.Repositorios.Seguranca;
using Controlsys.Dominio.Seguranca;
using Globalsys.Exceptions;
using System.IO;
using Globalsys.Relatorios;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar pessoas.
    /// </summary>
    [ExtendController]
    public class PessoaController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /Pessoa/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.PessoaController.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public PessoaController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        /// <summary>
        /// Obtém todas as pessoas com papel ativo no sistema.
        /// </summary>
        ///
        /// <param name="verificaUsuarioAtivo">
        /// Verifica apenas pessoas que não são usuário ativo.
        /// </param>
        ///
        /// <returns>
        /// Detalhes do tipo de objeto ligado ao controller no formato JSON.
        /// </returns>
        public ActionResult ObterTodos(bool verificaUsuarioAtivo = false)
        {
            IRepositorioPessoaFisica repPessoaFisica = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaFisica>(UnidadeTrabalho);
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);

            IQueryable<PessoaFisica> pessoasFisicas = repPessoaFisica.ObterTodos().Where(pf => pf.Papeis.Any(p => p.PapelLogs.OrderByDescending(pl => pl.Codigo).Select(pl => pl.Status).FirstOrDefault() == StatusPapelLog.Ativo));

            if (verificaUsuarioAtivo)
            {
                IQueryable<Usuario> usuarios = repUsuario.ObterTodos();
                pessoasFisicas = pessoasFisicas.Where(pf => !usuarios.Any(u => u.PessoaFisica.Codigo == pf.Codigo));
            }

            return Json(pessoasFisicas.Select(pf => new PessoaModelView
            {
                Codigo = pf.Codigo,
                Nome = pf.Nome,
                CPF = pf.CPF,
                Passaporte = pf.Passaporte
            }).ToArray().OrderBy(x => x.Nome));
        }

        /// <summary>
        /// Obter por cpf passaporte.
        /// </summary>
        ///
        /// <param name="verificarPapelAtivo">
        /// true to verificar papel ativo.
        /// </param>
        /// <param name="cpf">
        /// O(a) cpf.
        /// </param>
        /// <param name="passaporte">
        /// O(a) passaporte.
        /// </param>
        /// <param name="usuario">
        /// true to usuario.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ObterPorCpfPassaporte(bool verificarPapelAtivo = false, string cpf = null, string passaporte = null, bool usuario = false)
        {
            IRepositorioPessoaFisica repPessoaFisica = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaFisica>(UnidadeTrabalho);
            if (cpf != null)
                cpf = cpf.Replace("-", "").Replace(".", "");
            return Json(montaObj(repPessoaFisica.ObterPorCpfPassaporte(verificarPapelAtivo, cpf, passaporte, usuario)));
        }

        /// <summary>
        /// Selecionar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Selecionar(string codigo)
        {
            IRepositorioPessoaFisica repPessoaFisica = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaFisica>(UnidadeTrabalho);

            PessoaFisica pessoaFisica = repPessoaFisica.ObterPorId(codigo);

            return Json(montaObj(pessoaFisica));
        }

        /// <summary>
        /// Obter estados civis.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ObterEstadosCivis()
        {
            Dictionary<int, string> dic = new Dictionary<int, string>();

            foreach (EstadoCivil estadoCivil in Enum.GetValues(typeof(EstadoCivil)))
            {
                dic.Add((int)estadoCivil, estadoCivil.ObterDescricaoEnum());
            }

            return Json(dic.Select(d => new
                                        {
                                            Codigo = d.Key,
                                            Nome = d.Value
                                        })
                           .ToArray());
        }

        /// <summary>
        /// Obter escolaridades.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ObterEscolaridades()
        {
            Dictionary<int, string> dic = new Dictionary<int, string>();

            foreach (Escolaridade escolaridade in Enum.GetValues(typeof(Escolaridade)))
            {
                dic.Add(Convert.ToInt32(escolaridade), escolaridade.ObterDescricaoEnum());
            }

            return Json(dic.Select(d => new
                                        {
                                            Codigo = d.Key,
                                            Nome = d.Value
                                        }).ToArray());
        }

        /// <summary>
        /// Obter tipos sanguineos.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ObterTiposSanguineos()
        {
            Dictionary<int, string> dic = new Dictionary<int, string>();

            foreach (TipoSanguineo tipoSanguineo in Enum.GetValues(typeof(TipoSanguineo)))
            {
                dic.Add(Convert.ToInt32(tipoSanguineo), tipoSanguineo.ObterDescricaoEnum());
            }

            return Json(dic.Select(d => new
                                        {
                                            Codigo = d.Key,
                                            Nome = d.Value
                                        }).ToArray());
        }

        //private string GravarTemporario(byte[] foto)
        //{
        //    if (foto != null)
        //    {
        //        Random randNum = new Random();
        //        int number = randNum.Next(DateTime.Now.Millisecond);

        //        string nomeArquivo = number + ".jpeg";

        //        byte[] b = foto;

        //        using (FileStream fs = new FileStream(Server.MapPath("~/Content/img/webcam/") + nomeArquivo, FileMode.Create))
        //            fs.Write(b, 0, b.Length);

        //        return "/Content/img/webcam/" + nomeArquivo;
        //    }
        //    return "/Content/img/avatar.png";
        //}

        /// <summary>
        /// Cria um objeto contendo informações relacionadas ao conteudo gerenciado por
        /// PessoaController.
        /// </summary>
        ///
        /// <param name="pf">
        /// O(a) pf.
        /// </param>
        ///
        /// <returns>
        /// Um(a) object.
        /// </returns>
        private object montaObj(PessoaFisica pf)
        {
            if (pf == null)
                return null;

            Endereco endereco = pf.Endereco;

            Telefone telefoneRes = pf.Telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Residencial);
            Telefone telefoneEmer = pf.Telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Emergencial);
            Telefone telefoneCel = pf.Telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Celular);

            var pfModelView = new PessoaFisicaModelView();
            pfModelView.Codigo = pf.Codigo;

            pfModelView.CodigoPessoa = pf.Codigo;
            pfModelView.CPF = pf.CPFFormatado();
            pfModelView.Passaporte = pf.Passaporte;
            pfModelView.Nome = pf.Nome;
            pfModelView.Apelido = pf.Apelido;
            pfModelView.Email = pf.Email;
            pfModelView.DataNascimento = pf.DataNascimento;
            pfModelView.DataVisto = pf.DataVisto;
            pfModelView.DataValidadeVisto = pf.DataValidadeVisto;
            pfModelView.Sexo = pf.Sexo;
            pfModelView.NomeMae = pf.NomeMae;
            pfModelView.NomePai = pf.NomePai;
            pfModelView.RNE = pf.RNE;
            pfModelView.DataExpedicaoRNE = pf.DataExpedicaoRNE;
            pfModelView.EstadoCivil = pf.EstadoCivil;
            pfModelView.Escolaridade = pf.Escolaridade;
            pfModelView.TipoSanguineo = pf.TipoSanguineo;

            pfModelView.Nacionalidade = pf.Nacionalidade;
            pfModelView.NaturalidadeUF = pf.Naturalidade == null ? (int?)null : pf.Naturalidade.Estado.Codigo;
            pfModelView.Naturalidade = pf.Naturalidade == null ? (int?)null : pf.Naturalidade.Codigo;

            //RG
            pfModelView.RG = pf.RG;
            pfModelView.RGOrgaoEmissor = pf.RGOrgaoEmissor;
            pfModelView.RGOrgaoEmissorUF = pf.RGOrgaoEmissorUF == null ? (int?)null : pf.RGOrgaoEmissorUF.Codigo;
            pfModelView.RGDataEmissao = pf.RGDataEmissao;

            //Titulo de eleitor
            pfModelView.TituloEleitor = pf.TituloEleitor;
            pfModelView.TituloEleitorSecao = pf.TituloEleitorSecao;
            pfModelView.TituloEleitorZona = pf.TituloEleitorZona;
            pfModelView.TituloEleitorEstado = pf.TituloEleitorCidade == null ? (int?)null : pf.TituloEleitorCidade.Estado.Codigo;
            pfModelView.TituloEleitorCidade = pf.TituloEleitorCidade == null ? (int?)null : pf.TituloEleitorCidade.Codigo;

            //CTPS                
            pfModelView.CTPS = pf.CTPS;
            pfModelView.CTPSData = pf.CTPSData;
            pfModelView.CTPSSerie = pf.CTPSSerie;
            pfModelView.CTPSEstado = pf.CTPSEstado != null ? pf.CTPSEstado.Codigo : (int?)null;

            //CNH
            pfModelView.CNH = pf.CNH;
            pfModelView.CNHCategoria = pf.CNHCategoria;
            pfModelView.CNHDataValidade = pf.CNHDataValidade ?? null;

            //Certificado de Reservista
            pfModelView.CertificadoReservista = pf.CertificadoReservista;
            pfModelView.CertificadoReservistaCat = pf.CertificadoReservistaCat;

            if (pf.Foto != null)
                pfModelView.Img = Convert.ToBase64String(pf.Foto);

            //Telefones
            pfModelView.TelefoneNumResidencial = telefoneRes != null ? telefoneRes.TelefoneNum : null;
            pfModelView.TelefoneNumEmergencial = telefoneEmer != null ? telefoneEmer.TelefoneNum : null;
            pfModelView.TelefoneNumCelular = telefoneCel != null ? telefoneCel.TelefoneNum : null;

            if (endereco != null)
            {
                pfModelView.Endereco = new EnderecoMovelView()
                    {
                        CodigoEndereco = endereco.Codigo,
                        Numero = endereco.Numero,
                        CodigoLog = endereco.Logradouro != null ? endereco.Logradouro.Codigo : 0,
                        Pais = endereco.Logradouro != null ? endereco.Logradouro.Bairro.Cidade.Estado.Pais.Nome : "",
                        Estado = endereco.Logradouro != null ? endereco.Logradouro.Bairro.Cidade.Estado.Nome : "",
                        Cidade = endereco.Logradouro != null ? endereco.Logradouro.Bairro.Cidade.Nome : "",
                        Bairro = endereco.Logradouro != null ? endereco.Logradouro.Bairro.Nome : "",
                        Logradouro = endereco.Logradouro != null ? endereco.Logradouro.Nome : "",
                        CEP = endereco.Logradouro != null ? endereco.Logradouro.CEP : ""
                    };
            }
            Papel papel = pf.ObterPapel();
            if (papel != null)
            {
                pfModelView.CodigoPapel = papel.Codigo;
                pfModelView.PapelModelView = new PapelModelView
                {
                    Supervisor = papel.Supervisor,
                    Supervisionado = papel.Supervisionado,
                    SobAnalise = papel.SobAnalise,
                    Cargo = (papel is Colaborador) ? (papel.Cargo == null ? null : papel.Cargo.Nome) :
                            ((papel is PrestadorServico) ? (papel.Cargo == null ? null : papel.Cargo.Nome) :
                            ((papel is Visitante) ? (papel as Visitante).Funcao : null)),
                    EmpresaNome = (papel is Colaborador) ? (papel as Colaborador).Empresa.Nome :
                            ((papel is PrestadorServico) ? (papel as PrestadorServico).Empresa.Nome :
                            ((papel is Visitante) ? (papel as Visitante).Empresa : null)),
                    CodigoPapel = papel.Codigo
                };
                
                PapelLog papelLog = papel.ObterLogPapel();
                if (papelLog != null)
                {
                    papelLog.Papel = null;
                    pfModelView.PapelLog = papelLog;
                }
            }

            if (pf.Usuario != null)
	        {
                pfModelView.Usuario = new UsuarioModelView()
                {
                    Codigo = pf.Usuario.Codigo
                };
	        }
            ////histórico de treinamento
            for (int c = 0; c < pf.Papeis.Count(); c++)
            {
                for (int i = 0; i < pf.Papeis[c].Treinamentos.Count(); i++)
                {
                    var TreinamentosPessoa = new TreinamentoPessoaModelView()
                    {
                        Codigo = pf.Papeis[c].Codigo,
                        DataRealizacao = pf.Papeis[c].Treinamentos[i].DataRealizacao,
                        DataValidade = pf.Papeis[c].Treinamentos[i].DataValidade,
                        Nome = pf.Papeis[c].Treinamentos[i].Treinamento.Descricao,
                        CodigoTreinamento = pf.Papeis[c].Treinamentos[i].Codigo
                    };
                    pfModelView.TreinamentosPessoa.Add(TreinamentosPessoa);
                }
            }
            return pfModelView;
        }

        /// <summary>
        /// Obter lista pessoa.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="cpf">
        /// O(a) cpf.
        /// </param>
        /// <param name="passaporte">
        /// O(a) passaporte.
        /// </param>
        /// <param name="nome">
        /// O(a) nome.
        /// </param>
        /// <param name="verificaStatus">
        /// true to verifica status.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ObterListaPessoa(string cpf, string passaporte, string nome, bool verificaStatus = false)
        {
            IRepositorioPessoaFisica rep = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaFisica>(UnidadeTrabalho);


            cpf = cpf.Replace(".", "").Replace("-", "");
            if (string.IsNullOrEmpty(nome)) { nome = null; } else { nome.ToUpper(); }
            PessoaFisica pessoaFisica = new PessoaFisica { CPF = cpf, Passaporte = passaporte, Nome = nome };

            List<PessoaFisica> pessoas = rep.ObterListaPessoa(pessoaFisica, verificaStatus);

            if (!pessoas.Any()) { throw new CoreException("Não existe pessoa com os dados informados."); }

            var listaPessoasFisica = pessoas.Select(p => new
            {
                PessoaFisica = p,
                Papel = p.Papeis.Any() ? p.Papeis.OrderByDescending(pp => pp.Codigo).FirstOrDefault().Codigo : 0
            }).ToList();

            return Json(listaPessoasFisica.Select(p => new PessoaFisicaModelView
            {
                Codigo = p.PessoaFisica.Codigo,
                CodigoPapel = p.Papel,
                Nome = p.PessoaFisica.Nome,
                CPF = p.PessoaFisica.CPF != null ? p.PessoaFisica.CPFFormatado() : null,
                Passaporte = p.PessoaFisica.Passaporte
            })
            .ToList());
        }

        /// <summary>
        /// Lista status.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ListaStatus()
        {
            Dictionary<int, string> dic = new Dictionary<int, string>();

            foreach (int item in Enum.GetValues(typeof(StatusPapelLog)))
            {
                dic.Add(item, ((StatusPapelLog)item).ObterDescricaoEnum());
            }
            return Json(dic.Select(d => new
            {
                Codigo = d.Key,
                Descricao = d.Value
            }).ToList());

        }

        /// <summary>
        /// Obter description.
        /// </summary>
        ///
        /// <param name="status">
        /// O(a) status.
        /// </param>
        ///
        /// <returns>
        /// Um(a) string.
        /// </returns>
        private string obterDesc(StatusPapelLog status)
        {
            return EnumExtensoes.ObterDescricaoEnum(status);
        }

        /// <summary>
        /// Obter lista papel pessoa.
        /// </summary>
        ///
        /// <param name="cpf">
        /// O(a) cpf.
        /// </param>
        /// <param name="passaporte">
        /// O(a) passaporte.
        /// </param>
        /// <param name="nome">
        /// O(a) nome.
        /// </param>
        /// <param name="verificaStatus">
        /// true to verifica status.
        /// </param>
        /// <param name="buscarCracha">
        /// true to buscar cracha.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ObterListaPapelPessoa(string cpf, string passaporte, string nome, bool verificaStatus = false, bool buscarCracha = false, bool verificarCNH = false, string tipoPapel = null)
        {
            IRepositorioPapel rep = Fabrica.Instancia.ObterRepositorio<IRepositorioPapel>(UnidadeTrabalho);
            IQueryable<Papel> papeis = rep.ObterTodos().Where(p => p.PapelLogs.OrderByDescending(pp => pp.Codigo).Select(x => x.Status).FirstOrDefault() != StatusPapelLog.BaixaDefinitiva);

            cpf = cpf.Replace(".", "").Replace("-", "");
            if (!string.IsNullOrEmpty(cpf))
                papeis = papeis.Where(p => p.PessoaFisica.CPF.Contains(cpf));

            if (!string.IsNullOrEmpty(passaporte))
                papeis = papeis.Where(p => p.PessoaFisica.Passaporte.Contains(passaporte));

            if (!string.IsNullOrEmpty(nome))
                papeis = papeis.Where(p => p.PessoaFisica.Nome.Contains(nome));
            if (!string.IsNullOrEmpty(tipoPapel))
                switch (tipoPapel)
                {
                    case "Colaborador":
                        papeis = papeis.Where(p => p is Colaborador);
                        break;
                    case "Prestador":
                        papeis = papeis.Where(p => p is PrestadorServico);
                        break;
                    case "Visitante":
                        papeis = papeis.Where(p => p is Visitante);
                        break;
                    default:
                        break;
                }

            if (verificarCNH)
                papeis = papeis.Where(p => p.PessoaFisica.CNH != null && p.PessoaFisica.CNHDataValidade.HasValue && p.PessoaFisica.CNHDataValidade.Value >= DateTime.Now);

            Cracha cracha = null;
            IRepositorioCracha repCracha = Fabrica.Instancia.ObterRepositorio<IRepositorioCracha>(UnidadeTrabalho);
            return Json(papeis.ToList().Select(p =>
            {
                if (buscarCracha)
                    cracha = repCracha.ObterTodos().OrderByDescending(c => c.DataRegistro).FirstOrDefault(x => x.Papel.Codigo == p.Codigo && x.Ativo == true);
                return new
                {
                    Codigo = p.PessoaFisica.Codigo,
                    CodigoPapel = p.Codigo,
                    TipoPapel = p != null ? (p is Colaborador) ? "Colaborador" : (p is PrestadorServico) ? "Prestador de Serviço" : "Visitante" : "",
                    Nome = p.PessoaFisica.Nome,
                    Apelido = p.PessoaFisica.Apelido,
                    TipoSanguineo = (p.PessoaFisica.TipoSanguineo != null && p.PessoaFisica.TipoSanguineo != 0) ? obterDescTipoSang(p.PessoaFisica.TipoSanguineo) : "",
                    CPF = !string.IsNullOrEmpty(p.PessoaFisica.CPF) ? p.PessoaFisica.CPFFormatado() : null,
                    Passaporte = p.PessoaFisica.Passaporte,
                    Cargo = p.Cargo != null ? p.Cargo.Nome : null,
                    Empresa = ((p is Colaborador) ? (p as Colaborador).Empresa.Apelido :
                        ((p is PrestadorServico) ? (p as PrestadorServico).Empresa != null ?
                            (p as PrestadorServico).Empresa.Apelido : "" :
                            ((p is Visitante) ? (p as Visitante).Empresa : ""))),
                    Contratada = obterContratada(p.Codigo),
                    Status = p.ObterLogPapel() != null ? p.ObterLogPapel().Status.ObterDescricaoEnum() : StatusPapelLog.Inativacao.ObterDescricaoEnum(),
                    RFID = (buscarCracha && (cracha != null)) ? cracha.Numero : null,
                    Telefone = (((p.PessoaFisica.Telefones != null && p.PessoaFisica.Telefones.Count > 0) && p.PessoaFisica.Telefones.Any(t => t.TipoTelefone == TipoTelefone.Residencial && t.Ativo)) ? p.PessoaFisica.Telefones.Where(t => t.TipoTelefone == TipoTelefone.Residencial && t.Ativo).FirstOrDefault().TelefoneNum : string.Empty),
                    Celular = (p.PessoaFisica.Telefones != null && p.PessoaFisica.Telefones.Count > 0 && p.PessoaFisica.Telefones.Any(t => t.TipoTelefone == TipoTelefone.Celular && t.Ativo) ? p.PessoaFisica.Telefones.Where(t => t.TipoTelefone == TipoTelefone.Celular && t.Ativo).FirstOrDefault().TelefoneNum : string.Empty),
                    NumeroCracha = (buscarCracha && (cracha != null)) ? cracha.Numero : null,
                    CEP = p.PessoaFisica?.Endereco?.Logradouro?.CEP
                };
            })
            .ToList());
        }

        /// <summary>
        /// Obter contratada.
        /// </summary>
        ///
        /// <param name="codPessoa">
        /// O(a) cod pessoa.
        /// </param>
        ///
        /// <returns>
        /// Um(a) string.
        /// </returns>
        public string obterContratada(int codPessoa)
        {
            IRepositorioAlocacaoColaborador repAlocColab = Fabrica.Instancia.ObterRepositorio<IRepositorioAlocacaoColaborador>(UnidadeTrabalho);
            var alocao = repAlocColab.ObterAlocacao(codPessoa);

            if (alocao != null)
                return alocao.ObterContratada().Empresa.Apelido;
            else
                return null;
        }

        /// <summary>
        /// Obter description tipo sang.
        /// </summary>
        ///
        /// <param name="tipoSanguineo">
        /// O(a) tipo sanguineo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) object.
        /// </returns>
        private object obterDescTipoSang(TipoSanguineo? tipoSanguineo)
        {
            return tipoSanguineo.HasValue ? EnumExtensoes.ObterDescricaoEnum(tipoSanguineo) : null;
        }

        /// <summary>
        /// Get papel.
        /// </summary>
        ///
        /// <param name="papel">
        /// O(a) papel.
        /// </param>
        ///
        /// <returns>
        /// Um(a) object.
        /// </returns>
        private object getPapel(Papel papel)
        {
            if (papel is Colaborador)
            {
                Colaborador colab = (Colaborador)papel;
                return new
                {
                    CodigoPapel = papel.Codigo,
                    Cargo = papel.Cargo,
                    Empresa = colab.Empresa.Apelido,
                    ModeloCracha = colab.ModeloCracha

                };
            }

            if (papel is Visitante)
            {
                Visitante visitante = (Visitante)papel;
                return new
                {
                    CodigoPapel = papel.Codigo,
                    Cargo = papel.Cargo,
                    Empresa = visitante.Empresa,
                    ModeloCracha = visitante.ModeloCracha
                };
            }

            if (papel is PrestadorServico)
            {
                PrestadorServico prest = (PrestadorServico)papel;
                return new
                {
                    CodigoPapel = papel.Codigo,
                    Cargo = papel.Cargo,
                    Empresa = prest.Empresa.Apelido,
                    ModeloCracha = prest.ModeloCracha
                };
            }

            return null;
        }

        /// <summary>
        /// Obter lista excecao.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ObterListaExcecao()
        {
            Dictionary<int, string> dic = new Dictionary<int, string>();

            foreach (CriterioBloqueio criterioBloqueio in Enum.GetValues(typeof(CriterioBloqueio)))
            {
                dic.Add((int)criterioBloqueio, criterioBloqueio.ObterDescricaoEnum());
            }

            return Json(dic.Select(d => new
            {
                CodigoCriterio = d.Key,
                Nome = d.Value
            })
                           .ToArray());
        }

        /// <summary>
        /// Seleciona pessoa papel.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult SelecionaPessoaPapel(int codigo)
        {
            IRepositorioPapel repPapel = Fabrica.Instancia.ObterRepositorio<IRepositorioPapel>(UnidadeTrabalho);
            PapelModelView papelModelView = null;
            PessoaFisicaModelView pfModelView = new PessoaFisicaModelView();

            Papel papel = repPapel.ObterPorId(codigo);

            pfModelView.Nome = papel.PessoaFisica.Nome;

            if (papel != null)
            {
                PapelLog papelLog = papel.ObterLogPapel();

                papelModelView = new PapelModelView
                {
                    Supervisor = papel.Supervisor,
                    Supervisionado = papel.Supervisionado,
                    SobAnalise = papel.SobAnalise,
                    CodigoPapel = papel.Codigo
                };

                if (papelLog != null)
                {
                    papelLog.Papel = null;
                    pfModelView.PapelLog = papelLog;
                }
            }

            var aux = new
            {
                PapelModelView = papelModelView,
                PFModelView = pfModelView,
                Logs = papel.PapelLogs.OrderByDescending(pl => pl.Codigo).Select(pl => new
                {
                    DataRegistro = pl.DataRegistro,
                    Status = pl.Status.ObterDescricaoEnum(),
                    DataInicio = pl.DataInicio,
                    DataFim = pl.DataFim,
                    Motivo = pl.Motivo,
                    Ativo = pl.Codigo == papel.ObterLogPapel().Codigo ? true : false
                })
            };

            return Json(aux);
        }

        /// <summary>
        /// Obter lista categorias cnh.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ObterListaCategoriasCNH()
        {
            Dictionary<int, string> dic = new Dictionary<int, string>();

            foreach (CategoriaCNH categoriasCNH in Enum.GetValues(typeof(CategoriaCNH)))
            {
                dic.Add(Convert.ToInt32(categoriasCNH), categoriasCNH.ObterDescricaoEnum());
            }

            return Json(dic.Select(d => new
            {
                Codigo = d.Key,
                Nome = d.Value
            }).ToArray());
        }
    }
}
