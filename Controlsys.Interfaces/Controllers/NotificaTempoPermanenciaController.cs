﻿using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Controlsys.Dominio.Seguranca;
using Controlsys.Infra;
using Controlsys.Repositorio.Acesso;
using Controlsys.Repositorios.Acesso;
using Controlsys.Repositorios.Empresas;
using Controlsys.Repositorios.Parametros;
using Controlsys.Repositorios.Pessoas;
using Controlsys.Repositorios.Seguranca;
using Globalsys;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class NotificaTempoPermanenciaController : Controller, IControllerBase
    {
        /// <summary>
        /// Gets/Sets valor para UnidadeTrabalho.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.NotificaTempoPermanenciaController.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public NotificaTempoPermanenciaController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        //private class UltimoAcessoPapeis
        //{
        //    public int idAcesso { get; set; }
        //    public int codigoPapel { get; set; }
        //    public string nomePapel { get; set; }
        //    public string emailPapel { get; set; }            
        //    public DateTime dataAcessoEntrada { get; set; }
        //}

        [AllowAnonymous]
        [HttpPost]
        public string Index()
        {
            if (Request.HttpMethod != "POST")
            {
                Response.StatusCode = 403;
                Response.StatusDescription = "Requisição inválida.";
                throw new InvalidOperationException("Requisição inválida.");
            }

            IRepositorioColaborador repColab = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);
            IRepositorioAlocacaoColaborador repAlocacao = Fabrica.Instancia.ObterRepositorio<IRepositorioAlocacaoColaborador>(UnidadeTrabalho);
            IRepositorioControleAcesso repControleAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioControleAcesso>(UnidadeTrabalho);

            var tempo = new TimeSpan();

            DateTime dataFim = DateTime.Now;
            DateTime dataInicio = new DateTime(dataFim.Year, dataFim.Month, dataFim.Day).AddDays(-3);

            DateTime dtInicio = DateTime.Now;
            string caminho = "/Content/img/png44x73/semaforoVemelho44x73.png";

            Dictionary<string, string> listaErrosEnviarEmail = new Dictionary<string, string>();

            IQueryable<ControleAcesso> listControleAc = repControleAcesso.ObterListaAcessoAlertaTempoPermanencia(TipoPapel.Colaborador,dataInicio,dataFim);

            //var listControleAcAgrupado = listControleAc.GroupBy(c => new { c.Papel.Codigo, c.Papel.PessoaFisica.Nome, c.Papel.PessoaFisica.Email }).Select(g => new UltimoAcessoPapeis
            //{
            //    codigoPapel = g.Key.Codigo,
            //    nomePapel = g.Key.Nome,
            //    emailPapel = g.Key.Email,                
            //    dataAcessoEntrada = g.Max(c => c.DataAcesso),
            //    idAcesso = g.Max(c => c.Codigo)
            //}).ToList();

            //listControleAc = listControleAc.Where(x => codIdAcessoLista.Contains(x.Codigo) && x.Direcao != DirecaoLeitora.Saida && x.DataAcesso < dataFim.AddHours(-12).AddSeconds(1));

            List<int> codIdAcessoLista = listControleAc.GroupBy(c => new { c.Papel.Codigo }).Select(g => g.Max(c => c.Codigo)).ToList();
            int qtdcolab = codIdAcessoLista.Count;


            List<ControleAcesso> listaControleAcesso = new List<ControleAcesso>();

            for (int i = 0; i < qtdcolab; i += 1800)
            {
                var page = codIdAcessoLista.Skip(i).Take(1800).ToArray();
                listaControleAcesso.AddRange(listControleAc.Where(x => page.Contains(x.Codigo) && x.Direcao != DirecaoLeitora.Saida && x.DataAcesso < dataFim.AddHours(-13).AddSeconds(1)));
            }

            //List<int> codPapelLista = listControleAcAgrupado.Where(x => x.direcaoAcesso == DirecaoLeitora.Saida ).Select(w => w.codigoPapel).ToList();
            //listControleAcAgrupado = listControleAcAgrupado.Where(x => x.dataAcessoEntrada < dataFim.AddHours(-12).AddSeconds(1) && (!codPapelLista.Contains(x.codigoPapel)) ).ToList();

            foreach (var ultimoAcessoPapel in listaControleAcesso)
            {
                try
                {
                    Colaborador colaborador = repColab.ObterPorId(ultimoAcessoPapel.Papel.Codigo);

                    Dictionary<string, string> valoresSubs = new Dictionary<string, string>();

                    #region [Informações do Colaborador]

                    DateTime dataAcessoEntrada = new DateTime(ultimoAcessoPapel.DataAcesso.Year, ultimoAcessoPapel.DataAcesso.Month, ultimoAcessoPapel.DataAcesso.Day, ultimoAcessoPapel.DataAcesso.Hour, ultimoAcessoPapel.DataAcesso.Minute, ultimoAcessoPapel.DataAcesso.Second);
                    DateTime dataAtual = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                    tempo = dataAtual - dataAcessoEntrada;

                    var u = colaborador.PessoaFisica;
                    valoresSubs.Add("$$Nome2$$", u.Nome);
                    valoresSubs.Add("$$Horas$$", tempo.Days.ToString() + " dia(s) " + tempo.ToString(@"hh\:mm\:ss"));
                    valoresSubs.Add("$$Empresa2$$", colaborador.Empresa.Nome);

                    if ((dataAcessoEntrada >= dataAtual.AddHours(-14)) && (dataAcessoEntrada < dataAtual.AddHours(-13)))
                        caminho = "/Content/img/png44x73/semaforoAzul44x73.png";
                    if ((dataAcessoEntrada >= dataAtual.AddHours(-16)) && (dataAcessoEntrada < dataAtual.AddHours(-14)))
                        caminho = "/Content/img/png44x73/semaforoAmarelo44x73.png";

                    valoresSubs.Add("$$Imagem$$", "data:image/png;base64," + Convert.ToBase64String(System.IO.File.ReadAllBytes(Server.MapPath("~" + caminho))));

                    #endregion

                    #region [Enviar email]

                    string AssuntoEmail = "CONTROLSYS - Alerta de Tempo de Permanência.";
                    string Path_htmlEmail = "/Content/templates/alertaTempoPermanencia.htm";
                    string email = "";

                    if (colaborador.Empresa.CNPJ == "07005330000119")
                    {
                        Colaborador colaboradorSuperiorImediato = repColab.ObterTodos().Where(x => x.EmpregadoSAP.Codigo == Convert.ToInt32(colaborador.EmpregadoSAP.SuperiorImediato.Codigo)).FirstOrDefault();
                        email = (colaboradorSuperiorImediato != null && colaboradorSuperiorImediato.PessoaFisica != null) ? colaboradorSuperiorImediato.PessoaFisica.Email.ToString() : null;
                    }
                    else
                    {
                        AlocacaoColaborador alocacao = repAlocacao.ObterAlocacao(colaborador.Codigo, true);
                        Colaborador gestorPedido = alocacao.PedidoCompra.GestoresResponsaveis.FirstOrDefault().Colaborador;
                        email = (gestorPedido != null && gestorPedido.PessoaFisica != null) ? gestorPedido.PessoaFisica.Email.ToString() : null;
                    }

                    if (email != null)
                    {
                        Globalsys.Util.Email.Enviar(AssuntoEmail,
                        Server.MapPath(Path_htmlEmail),
                        valoresSubs,
                        string.Empty,
                        null,
                        email);
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    listaErrosEnviarEmail.Add("Nome: " + ultimoAcessoPapel.Papel.PessoaFisica.Nome + " email: " + ultimoAcessoPapel.Papel.PessoaFisica.Email, ex.Message);
                }
            }

            DateTime dtfim = DateTime.Now;
            TimeSpan tempoDecorrido = dtfim - dtInicio;
            var json = new
            {
                Success = listaErrosEnviarEmail.Count == 0,
                Data = listaErrosEnviarEmail.Select(e => new { Empresa = e.Key, Erro = e.Value }),
                Message = "Tempo decorrido: " + tempoDecorrido.ToString(@"hh\:mm\:ss")
            };

            return JsonConvert.SerializeObject(json, Newtonsoft.Json.Formatting.Indented);
        }

    }
}
