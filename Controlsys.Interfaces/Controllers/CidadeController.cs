﻿using Controlsys.Dominio.Enderecos;
using Controlsys.Infra;
using Controlsys.Repositorio.Parametros;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Models
{
    /// <summary>
    /// Um controlador para tratar cidades.
    /// </summary>
    [ExtendController]
    public class CidadeController : Controller, IControllerBase
    {
        /// <summary>
        /// Gets/Sets valor para UnidadeTrabalho.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Models.CidadeController.
        /// </summary>
        ///
        /// <param name="unidade">
        /// O(a) unidade.
        /// </param>
        public CidadeController(IUnidadeTrabalho unidade)
        {
            UnidadeTrabalho = unidade;
        }

        /// <summary>
        /// GET: /Cidade/.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Inclui um novo cidade.
        /// </summary>
        ///
        /// <param name="cidade">
        /// O(a) cidade.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Incluir(Cidade cidade)
        {
            IRepositorioCidade repCidade = Fabrica.Instancia.ObterRepositorio<IRepositorioCidade>(UnidadeTrabalho);
            cidade.DataRegistro = DateTime.Now;
            cidade.Ativar();
            repCidade.Validar(cidade, EstadoObjeto.Novo);
            repCidade.Salvar(cidade);
            return Json(new
            {
                cidade.Codigo,
                Estado = cidade.Estado.Nome,
                cidade.Nome,
                cidade.Ativo                
            });

        }

        /// <summary>
        /// Altera um registro de cidade.
        /// </summary>
        ///
        /// <param name="cidade">
        /// O(a) cidade.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Alterar(Cidade cidade)
        {
            IRepositorioCidade repArea = Fabrica.Instancia.ObterRepositorio<IRepositorioCidade>(UnidadeTrabalho);
            Cidade cidadeA = repArea.ObterPorId(cidade.Codigo);
            cidadeA.Nome = cidade.Nome;
            cidadeA.Estado = new Estado()
            {
                Codigo = cidade.Estado.Codigo
            };
            repArea.Validar(cidadeA, EstadoObjeto.Alterado);
            repArea.Atualizar(cidadeA);
            return Json(new
            {
                Codigo = cidadeA.Codigo,
                Nome = cidadeA.Nome,
                Estado = cidadeA.Estado.Nome,
                cidade.Ativo
            });
        }

        /// <summary>
        /// Ativar inativar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult AtivarInativar(int codigo)
        {
            IRepositorioCidade rep = Fabrica.Instancia.ObterRepositorio<IRepositorioCidade>(UnidadeTrabalho);
            Cidade cidade = rep.ObterPorId(codigo);
            if (cidade.Ativo)
            {
                cidade.Inativar();
            }
            else
            {
                rep.Validar(cidade, EstadoObjeto.Ativado);
                cidade.Ativar();
            }
            rep.Atualizar(cidade);
            return Json(new
            {
                Codigo = cidade.Codigo,
                Nome = cidade.Nome,
                Estado = cidade.Estado.Nome,
                cidade.Ativo
            });
        }

        /// <summary>
        /// Selecionar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Selecionar(int codigo)
        {
            IRepositorioCidade repp = Fabrica.Instancia.ObterRepositorio<IRepositorioCidade>(UnidadeTrabalho);
            Cidade cidade = repp.ObterPorId(codigo);
            return Json(new
            {
                Codigo = cidade.Codigo,
                Nome = cidade.Nome,
                CodigoEstado = cidade.Estado.Codigo,
                Estado = cidade.Estado.Nome
            });
        }

    }
}
