﻿using System.Configuration;
using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.AgendamentosPessoa;
using Controlsys.Dominio.Enderecos;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Controlsys.Dominio.Seguranca;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Acesso;
using Controlsys.Repositorio.Agendamentos;
using Controlsys.Repositorio.Parametros;
using Controlsys.Repositorios.Acesso;
using Controlsys.Repositorios.Agendamentos;
using Controlsys.Repositorios.Enderecos;
using Controlsys.Repositorios.Parametros;
using Controlsys.Repositorios.Pessoas;
using Controlsys.Repositorios.Seguranca;
using Ext.Net;
using Ext.Net.MVC;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar visitantes.
    /// </summary>
    [ExtendController]
    public class VisitanteController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /Visitante/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.VisitanteController.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public VisitanteController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        /// <summary>
        /// Retorna a página de visitante.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index(int? codigo, int? codigoAg)
        {
            if (codigo.HasValue)
                X.GetCmp<Hidden>("recordId").SetValue(codigo);

            if (codigoAg.HasValue)
                return View("Index", model: codigoAg.ToString());

            return View();
        }

        /// <summary>
        /// Inclui um novo visitante.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="visitante">
        /// O(a) visitante.
        /// </param>
        /// <param name="propModificadasPessoa">
        /// O(a) property modificadas pessoa.
        /// </param>
        /// <param name="treinamentos">
        /// O(a) treinamentos.
        /// </param>
        /// <param name="documentos">
        /// O(a) documentos.
        /// </param>
        /// <param name="imagem">
        /// O(a) imagem.
        /// </param>
        /// <param name="gravacoes">
        /// Gravações de audio
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Incluir(Visitante visitante, string[] propModificadasPessoa, List<TreinamentoPessoa> treinamentos, List<DocumentoMidiaModelView> documentos, string imagem, string[] gravacoes, int? codAgVisitante)
        {
            IRepositorioGrupo repGrupo = null;
            IRepositorioParametro repParam = null;

            try
            {
                UnidadeTrabalho.BeginTransaction();

                //repositorios
                #region [REPOSITORIOS]
                IRepositorioVisitante repVisit = Fabrica.Instancia.ObterRepositorio<IRepositorioVisitante>(UnidadeTrabalho);
                IRepositorioPessoaFisica repPessoaFisica = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaFisica>(UnidadeTrabalho);
                IRepositorioEndereco repEndereco = Fabrica.Instancia.ObterRepositorio<IRepositorioEndereco>(UnidadeTrabalho);
                IRepositorioDocumento repDoc = Fabrica.Instancia.ObterRepositorio<IRepositorioDocumento>(UnidadeTrabalho);
                IRepositorioTreinamento repTreinamento = Fabrica.Instancia.ObterRepositorio<IRepositorioTreinamento>(UnidadeTrabalho);
                IRepositorioColaborador repColab = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);
                IRepositorioPapel repPapel = Fabrica.Instancia.ObterRepositorio<IRepositorioPapel>(UnidadeTrabalho);
                repGrupo = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupo>(UnidadeTrabalho);
                repParam = Fabrica.Instancia.ObterRepositorio<IRepositorioParametro>(UnidadeTrabalho);
                #endregion

                #region [FOTO]
                PessoaFisica pessoaFisica = visitante.PessoaFisica;
                Endereco end = visitante.PessoaFisica.Endereco;

                if (!string.IsNullOrEmpty(imagem))
                    pessoaFisica.Foto = Convert.FromBase64String(imagem);

                if (visitante.PessoaFisica.CPF != null)
                    visitante.PessoaFisica.CPF = visitante.PessoaFisica.CPF.Replace("-", "").Replace(".", "");

                var pAux = repPessoaFisica.ObterPorCpfPassaporte(false, pessoaFisica.CPF, pessoaFisica.Passaporte);
                if (pAux != null)
                    pessoaFisica.Codigo = pAux.Codigo;

                if (!string.IsNullOrEmpty(pessoaFisica.Codigo))
                {
                    pessoaFisica = repPessoaFisica.ObterPorId(pessoaFisica, propModificadasPessoa);
                    if (!string.IsNullOrEmpty(imagem))
                        pessoaFisica.Foto = Convert.FromBase64String(imagem);
                    repPessoaFisica.Atualizar(pessoaFisica);
                }

                
                #endregion

                #region [ENDEREÇO]

                if (end != null)
                {
                    pessoaFisica.Endereco = repEndereco.VerificarEndereco(end);
                    pessoaFisica.Endereco.DataRegistro = DateTime.Now;
                    pessoaFisica.Endereco.Ativar();
                    repEndereco.Salvar(pessoaFisica.Endereco);
                }

                #endregion

                #region [VALIDAÇÕES REFERENTES A PESSOA]
                if (string.IsNullOrEmpty(pessoaFisica.Codigo))
                {
                    repPessoaFisica.Salvar(pessoaFisica, visitante);
                }

                if (propModificadasPessoa != null)
                {
                    pessoaFisica = insereTelefone(propModificadasPessoa, pessoaFisica, visitante);
                }
                #endregion

                #region [VISITANTE]
                int codSolicitante = visitante.Solicitante.Codigo;
                visitante.Solicitante = repColab.ObterPorId(codSolicitante);
                int codResponsavel = visitante.ResponsavelVisita.Codigo;
                visitante.ResponsavelVisita = repColab.ObterPorId(codResponsavel);

                #endregion

                repVisit.Validar(EstadoObjeto.Novo, visitante);

                #region [PAPEL]
                Papel papel = repPapel.SalvarPapelLog(visitante.InserirPapelLog(DateTime.Now, null, StatusPapelLog.Ativo));
                
                repVisit.Salvar(visitante);
                #endregion

                #region [TREINAMENTOS]
                if (treinamentos != null)
                {
                    foreach (var item in treinamentos)
                        repTreinamento.SalvarTreinamentoPessoa(visitante, item);
                }
                #endregion

                #region [DOCUMENTOS]
                foreach (var item in documentos)
                {
                    // Se Documentos com status completo não tiverem midia gera erro.
                    if (item.Documento.Status == StatusDocumento.Completo && (item.Midias == null || !item.Midias.Any(m => m != null)))
                    {
                        throw new CoreException("Não é possível salvar documento com status 'Completo' sem mídia anexada.");
                    }

                    DocumentoPapel docPapel = repDoc.SalvarDocPapel(papel, item.Documento);

                    string path = ConfigurationManager.AppSettings.Get("pathUploads");

                    if (!System.IO.Directory.Exists(path))
                        throw new CoreException("O diretório onde a midia seria armazanada não está acessivel.");

                    IRepositorioDocumentoMidia repDocumentoMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioDocumentoMidia>(UnidadeTrabalho);
                    IRepositorioMidia repMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioMidia>(UnidadeTrabalho);

                    int cont = 1;

                    foreach (MidiaModelView midia in (IEnumerable<MidiaModelView>)item.Midias ?? Enumerable.Empty<MidiaModelView>())
                    {
                        if (midia == null)
                            continue;
                        string nomeDisco = string.Format("PF-{0}-{1}-{2}-{3}.{4}", (string.IsNullOrEmpty(visitante.PessoaFisica.CPF) ? visitante.PessoaFisica.Passaporte : visitante.PessoaFisica.CPF),
                            visitante.Codigo, docPapel.Documento.TipoDocumento.Sigla, cont, midia.Extensao);
                        Midia m = new Midia();
                        m.DataRegistro = midia.DataRegistro;
                        m.Extensao = midia.Extensao;
                        m.MimeType = midia.MIME;
                        m.NomeArquivo = nomeDisco;

                        repMidia.Salvar(m);

                        repDocumentoMidia.SalvarDocMidia(docPapel.Documento, m);

                        System.IO.File.WriteAllBytes(m.Url, Convert.FromBase64String(midia.Arquivo));

                        cont++;
                    }
                }
                #endregion

                #region [AGENDAMENTO]
                if (codAgVisitante.HasValue)
                {
                    IRepositorioAgendamentoVisitante repAgVisit = Fabrica.Instancia.ObterRepositorio<IRepositorioAgendamentoVisitante>(UnidadeTrabalho);
                    IRepositorioPapelAgendamento repPapelAg = Fabrica.Instancia.ObterRepositorio<IRepositorioPapelAgendamento>(UnidadeTrabalho);

                    VisitanteAgendamento agVisitante = repAgVisit.ObterPorId(codAgVisitante);
                    if (agVisitante.ObterStatusAtual().StatusAgendamento == StatusAgendamento.AguardandoAprovacao)
                        repPapelAg.SalvarStatusPapelAg(agVisitante.InserirPapelAg(StatusAgendamento.CadastroAprovado, null));
                        agVisitante.Papel = papel;
                        repAgVisit.Atualizar(agVisitante);
                }
                #endregion

                #region [ATENDIMENTO RECEPTIVO]
                if (gravacoes != null)
                {
                    IRepositorioMidia repMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioMidia>(UnidadeTrabalho);
                    IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
                    IRepositorioAtendimentoReceptivo repAtendimentoReceptivo = Fabrica.Instancia.ObterRepositorio<IRepositorioAtendimentoReceptivo>(UnidadeTrabalho);

                    string path = ConfigurationManager.AppSettings.Get("pathUploads");
                    if (!Directory.Exists(path))
                        throw new Exception("O diretório onde a midia seria armazanada não está acessivel.");

                    Usuario usuario = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name);
                    if (usuario == null)
                        throw new Exception("Não foi possível identificar o usuário logado.");

                    var qtd = repAtendimentoReceptivo.ObterTodos().Where(a => a.Papel.Codigo.Equals(visitante.Codigo)).Count();

                    foreach (var gravacao in gravacoes)
                    {
                        var instante = DateTime.Now;

                        qtd++;
                        string nomeDisco = string.Format("PF-AR-{0}-{1}-{2}.wav", instante.ToString("yyyyMMddHHmmss"), visitante.Codigo, qtd);

                        Midia m = new Midia();
                        m.DataRegistro = instante;
                        m.Extensao = "wav";
                        m.MimeType = "audio/wav";
                        m.NomeArquivo = nomeDisco;
                        repMidia.Validar(m, EstadoObjeto.Novo);
                        repMidia.Salvar(m);

                        AtendimentoReceptivo aR = new AtendimentoReceptivo();
                        aR.DataRegistro = instante;
                        aR.Midia = m;
                        aR.Papel = visitante;
                        aR.Usuario = usuario;
                        aR.Ativar();
                        repAtendimentoReceptivo.Validar(aR, EstadoObjeto.Novo);
                        repAtendimentoReceptivo.Salvar(aR);

                        var edt = gravacao.Split(',')[1];
                        System.IO.File.WriteAllBytes(m.Url, Convert.FromBase64String(edt));
                    }
                }
                #endregion

                UnidadeTrabalho.Commit();
                return Json(montaObj(visitante));
            }
            catch (CoreException msg)
            {
                #region [EXCEPTION]
                string msgCadastroPessoa = repParam.RecuperarParametro(ParametroSistema.MensagemCadastroPessoaFisica).Valor;
                if (msg.Message == msgCadastroPessoa)
                {
                    Grupo g = repGrupo.ObterPorId(int.Parse(repParam.RecuperarParametro(ParametroSistema.GrupoUsuarioResponsavelPessoaCritica).Valor));
                    string msgEmailGrupoUsuario = repParam.RecuperarParametro(ParametroSistema.MensagemEmailNotificaGrupoCadastroPessoaCritica).Valor;

                    Dictionary<string, string> valoresSubs = new Dictionary<string, string>();

                    IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
                    string userLogado = HttpContext.User.Identity.Name;
                    Usuario u = repUsuario.ObterPorLogin(userLogado);

                    IRepositorioPessoaCritica repPC = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaCritica>(UnidadeTrabalho);
                    PessoaCritica pc = repPC.ObterPorCPF(visitante.PessoaFisica.CPF, true);

                    //Informações da Pessoa que está cadastrando
                    valoresSubs.Add("$$Nome$$", u.PessoaFisica.Nome);
                    valoresSubs.Add("$$Empresa$$", (u.PessoaFisica.ObterPapel(true) is Colaborador ? (u.PessoaFisica.ObterPapel() as Colaborador).Empresa.Nome : ""));
                    valoresSubs.Add("$$Funcao$$", (u.PessoaFisica.ObterPapel(true) is Colaborador ? (u.PessoaFisica.ObterPapel() as Colaborador).Cargo.Nome : ""));

                    //Informações da Pessoa que está cadastrando
                    valoresSubs.Add("$$Nome2$$", visitante.PessoaFisica.Nome);
                    valoresSubs.Add("$$CPF$$", visitante.PessoaFisica.CPFFormatado());

                    //Informações da inclusão da Pessoa 
                    valoresSubs.Add("$$Motivo$$", (!string.IsNullOrEmpty(pc.MotivoBloqueio.Nome)) ? pc.MotivoBloqueio.Nome : "");
                    valoresSubs.Add("$$Observacao$$", (!string.IsNullOrEmpty(pc.Observacao)) ? pc.Observacao : "");
                    valoresSubs.Add("$$DataInclusao$$", pc.DataBloqueio.ToString("dd/MM/yyyy"));
                    valoresSubs.Add("$$Nome3$$", pc.Responsavel.PessoaFisica.Nome);
                    valoresSubs.Add("$$Funcao2$$", (pc.Responsavel.PessoaFisica.ObterPapel(true) is Colaborador ? (u.PessoaFisica.ObterPapel() as Colaborador).Cargo.Nome : ""));

                    valoresSubs.Add("$$Msg$$", msgEmailGrupoUsuario);


                    foreach (var item in g.GruposUsuarios.Where(t => t.Ativo))
                    {
                        Globalsys.Util.Email.Enviar("[CONTROLSYS] Pessoa Crítica",
                                                      Server.MapPath("/Content/templates/pessoaCritica.htm"),
                                                      valoresSubs,
                                                      string.Empty,
                                                      null,
                                                      item.Usuario.PessoaFisica.Email);
                    }
                }
                UnidadeTrabalho.Rollback();

                throw msg;
                #endregion
            }
            finally
            {
                UnidadeTrabalho.Dispose();
            }
        }

        /// <summary>
        /// Altera um registro de visitante.
        /// </summary>
        ///
        /// <param name="visitante">
        /// O(a) visitante.
        /// </param>
        /// <param name="propModificadasVisit">
        /// O(a) property modificadas visit.
        /// </param>
        /// <param name="propModificadasDadosVisit">
        /// O(a) property modificadas dados visit.
        /// </param>
        /// <param name="documentos">
        /// O(a) documentos.
        /// </param>
        /// <param name="treinamentoDel">
        /// O(a) treinamento delete.
        /// </param>
        /// <param name="treinamentoNew">
        /// O(a) treinamento new.
        /// </param>
        /// <param name="imagem">
        /// O(a) imagem.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Alterar(Visitante visitante, string[] propModificadasVisit, string[] propModificadasDadosVisit, List<DocumentoMidiaModelView> documentosMidias, TreinamentoPessoa[] treinamentoDel, TreinamentoPessoa[] treinamentoNew, string imagem, string[] atendRecepNew = null, int[] atendRecepDel = null)
        {
            IRepositorioVisitante repVisit = Fabrica.Instancia.ObterRepositorio<IRepositorioVisitante>(UnidadeTrabalho);
            IRepositorioPessoaFisica repPessoaFisica = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaFisica>(UnidadeTrabalho);
            IRepositorioDocumento repDoc = Fabrica.Instancia.ObterRepositorio<IRepositorioDocumento>(UnidadeTrabalho);
            IRepositorioTreinamento repTrein = Fabrica.Instancia.ObterRepositorio<IRepositorioTreinamento>(UnidadeTrabalho);
            IRepositorioEndereco repEndereco = Fabrica.Instancia.ObterRepositorio<IRepositorioEndereco>(UnidadeTrabalho);
            IRepositorioSincronizaPessoaAcesso repSincronizaPessoaAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioSincronizaPessoaAcesso>(UnidadeTrabalho);
            IRepositorioDocumentoMidia repDocumentoMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioDocumentoMidia>(UnidadeTrabalho);
            IRepositorioMidia repMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioMidia>(UnidadeTrabalho);
            IRepositorioAtendimentoReceptivo repAtendimentoReceptivo = Fabrica.Instancia.ObterRepositorio<IRepositorioAtendimentoReceptivo>(UnidadeTrabalho);


            List<Telefone> telefones = new List<Telefone>(visitante.PessoaFisica.Telefones);

            #region [PESSOA FISICA]
            PessoaFisica pessoaFisica = visitante.PessoaFisica;
            Endereco enderecoNovo = pessoaFisica.Endereco;

            pessoaFisica = repPessoaFisica.ObterPorId(pessoaFisica, propModificadasVisit);
            #endregion

            #region [ENDEREÇO]
            if (enderecoNovo != null && !string.IsNullOrEmpty(enderecoNovo.Logradouro.CEP))
            {
                if (pessoaFisica.Endereco != null)
                    repEndereco.Remover(pessoaFisica.Endereco.Codigo);

                //endereço
                pessoaFisica.Endereco = repEndereco.VerificarEndereco(enderecoNovo);
                pessoaFisica.Endereco.DataRegistro = DateTime.Now;
                pessoaFisica.Endereco.Ativar();
                repEndereco.Salvar(pessoaFisica.Endereco);

            }
            #endregion

            #region [FOTO]
            if (!string.IsNullOrEmpty(imagem))
                pessoaFisica.Foto = Convert.FromBase64String(imagem);

            repPessoaFisica.Validar(EstadoObjeto.Alterado, pessoaFisica, visitante);
            repPessoaFisica.Atualizar(pessoaFisica);

            if (propModificadasVisit != null)
            {
                pessoaFisica = insereTelefone(propModificadasVisit, pessoaFisica, visitante);
            }
            #endregion

            #region [VISITANTE]
            var Solicitante = visitante.Solicitante;
            var ResponsavelVis = visitante.ResponsavelVisita;

            visitante = repVisit.ObterPorId(visitante, propModificadasDadosVisit);

            if (propModificadasDadosVisit.Contains("DataFim") || (propModificadasDadosVisit.Contains("DataInicio")))
            {
                DateTime? dataInicio = null;

                if (visitante.DataInicio > DateTime.Today)
                    dataInicio = visitante.DataInicio;

                if (visitante.DataInicio <= DateTime.Today && visitante.DataFim >= DateTime.Today)
                    repSincronizaPessoaAcesso.Sincronizar(visitante, TipoSincronizacaoPessoa.Conceder, null);
                else if (visitante.DataInicio < DateTime.Today && visitante.DataFim < DateTime.Today)
                    repSincronizaPessoaAcesso.Sincronizar(visitante, TipoSincronizacaoPessoa.Suspender, null);
                else if (visitante.DataInicio > DateTime.Today && visitante.DataFim > DateTime.Today)
                    repSincronizaPessoaAcesso.Sincronizar(visitante, TipoSincronizacaoPessoa.Conceder, dataInicio);
            }

            if (visitante.Solicitante.Codigo != Solicitante.Codigo || visitante.ResponsavelVisita.Codigo != ResponsavelVis.Codigo)
            {

                visitante.Solicitante = new Colaborador()
                {
                    Codigo = Solicitante.Codigo
                };
                visitante.ResponsavelVisita = new Colaborador()
                {
                    Codigo = ResponsavelVis.Codigo
                };
            }

            if (propModificadasDadosVisit == null)
                propModificadasDadosVisit = new string[1];
            #endregion

            #region [DOCUMENTOS]
            string path = ConfigurationManager.AppSettings.Get("pathUploads");
            if (documentosMidias != null)
            {
                List<DocumentoMidiaModelView> docsPapel = new List<DocumentoMidiaModelView>();
                docsPapel = documentosMidias.Where(dm => visitante.Documentos.Any(cd => dm.Documento.Codigo == cd.Documento.Codigo)).ToList();

                foreach (var item in documentosMidias.Where(dm => visitante.Documentos.Any(cd => dm.Documento.Codigo == cd.Documento.Codigo)))
                {
                    // Se Documentos com status completo não tiverem midia gera erro.
                    if (item.Documento.Status == StatusDocumento.Completo && (item.Midias == null || !item.Midias.Any(m => m != null)))
                    {
                        throw new CoreException("Não é possível salvar documento com status 'Completo' sem mídia anexada.");
                    }

                    Documento doc = repDoc.ObterPorId(item.Documento.Codigo);
                    doc.Status = item.Documento.Status;
                    doc.Descricao = item.Documento.Descricao;
                    doc.DataValidade = item.Documento.DataValidade;
                    repDoc.Atualizar(doc);

                    if (item.Midias == null)
                    {
                        DocumentoMidiaModelView item1 = item;
                        var docMidias =
                            repDocumentoMidia.ObterTodos().Where(dm => dm.Documento.Codigo == item1.Documento.Codigo && dm.Ativo);
                        foreach (var documentoMidia in docMidias)
                        {
                            documentoMidia.Inativar();
                            repDocumentoMidia.Atualizar(documentoMidia);
                        }
                        continue;
                    }
                    else
                    {
                        DocumentoMidiaModelView item1 = item;
                        List<int> codigosMidia = item.Midias.Select(i => i.CodigoMidia).ToList();
                        IQueryable<DocumentoMidia> listaDocMidia = repDocumentoMidia.ObterTodos().Where(dm =>
                            dm.Documento.Codigo == item1.Documento.Codigo &&
                            dm.Ativo && !codigosMidia.Contains(dm.Midia.Codigo));

                        foreach (DocumentoMidia docMidia in listaDocMidia)
                        {
                            docMidia.Inativar();
                            repDocumentoMidia.Salvar(docMidia);
                        }
                    }

                    int cont = repDocumentoMidia.ObterTodos().Where(dm => dm.Documento.Codigo == item.Documento.Codigo).Count() + 1;

                    foreach (MidiaModelView midia in item.Midias.Where(m => m.CodigoMidia == 0))
                    {
                        if (!System.IO.Directory.Exists(path))
                            throw new CoreException("O diretório onde a midia seria armazanada não está acessivel.");

                        string nomeDisco = string.Format("PF-{0}-{1}-{2}-{3}.{4}", 
                            (string.IsNullOrEmpty(visitante.PessoaFisica.CPF) ? visitante.PessoaFisica.Passaporte : visitante.PessoaFisica.CPF),
                            visitante.Codigo, doc.TipoDocumento.Sigla, cont, midia.Extensao);

                        Midia m = new Midia();
                        m.DataRegistro = midia.DataRegistro;
                        m.Extensao = midia.Extensao;
                        m.MimeType = midia.MIME;
                        m.NomeArquivo = nomeDisco;

                        repMidia.Salvar(m);

                        repDocumentoMidia.SalvarDocMidia(doc, m);
                        System.IO.File.WriteAllBytes(m.Url, Convert.FromBase64String(midia.Arquivo));

                        cont++;

                    }
                }

                foreach (var item in documentosMidias.Where(dm => !visitante.Documentos.Any(cd => dm.Documento.Codigo == cd.Documento.Codigo)))
                {
                    // Se Documentos com status completo não tiverem midia gera erro.
                    if (item.Documento.Status == StatusDocumento.Completo && (item.Midias == null || !item.Midias.Any(m => m != null)))
                    {
                        throw new CoreException("Não é possível salvar documento com status 'Completo' sem mídia anexada.");
                    }

                    DocumentoPapel docPapel = repDoc.SalvarDocPapel(visitante, item.Documento);

                    int cont = repDocumentoMidia.ObterTodos().Where(dm => dm.Documento.Codigo == item.Documento.Codigo).Count() + 1;

                    foreach (MidiaModelView midia in item.Midias ?? Enumerable.Empty<MidiaModelView>())
                    {
                        if (midia == null)
                            continue;

                        if (!System.IO.Directory.Exists(path))
                            throw new CoreException("O diretório onde a midia seria armazanada não está acessivel.");

                        string nomeDisco = string.Format("PF-{0}-{1}-{2}-{3}.{4}", 
                            (string.IsNullOrEmpty(visitante.PessoaFisica.CPF) ? visitante.PessoaFisica.Passaporte : visitante.PessoaFisica.CPF),
                            visitante.Codigo, docPapel.Documento.TipoDocumento.Sigla, cont, midia.Extensao);
                        Midia m = new Midia();
                        m.DataRegistro = midia.DataRegistro;
                        m.Extensao = midia.Extensao;
                        m.MimeType = midia.MIME;
                        m.NomeArquivo = nomeDisco;

                        repMidia.Salvar(m);

                        repDocumentoMidia.SalvarDocMidia(docPapel.Documento, m);

                        System.IO.File.WriteAllBytes(m.Url, Convert.FromBase64String(midia.Arquivo));

                        cont++;
                    }
                }
            }

            #endregion

            #region [TREINAMENTO]
            if (treinamentoDel != null)
            {
                foreach (var item in visitante.Treinamentos.Where(t => treinamentoDel.Any(tdd => tdd.Treinamento.Codigo == t.Treinamento.Codigo)))
                {
                    TreinamentoPessoa treinamento = item;
                    treinamento.Inativar();
                }
            }
            if (treinamentoNew != null)
            {
                foreach (var item in treinamentoNew)
                {
                    TreinamentoPessoa tp = new TreinamentoPessoa();
                    tp.Papel = visitante;
                    tp.DataRealizacao = item.DataRealizacao;
                    tp.DataValidade = item.DataValidade;
                    repTrein.SalvarTreinamentoPessoa(visitante, item);
                }
            }
            #endregion

            #region [ATENDIMENTO RECEPTIVO]
            if (atendRecepDel != null)
            {
                if (!System.IO.Directory.Exists(path))
                    throw new CoreException("O diretório onde a midia seria armazanada não está acessivel.");

                var regs = repAtendimentoReceptivo.ObterTodos().Where(ar => atendRecepDel.Contains(ar.Codigo));
                foreach (var atendR in regs)
                //foreach (var atendR in repAtendimentoReceptivo.ObterTodos().Where(ar => atendRecepDel.Any(arr => ar.Codigo == arr)))
                {
                    //if (!System.IO.File.Exists(atendR.Midia.Url))
                    //    throw new CoreException("O arquivo '" + atendR.Midia.NomeArquivo + "' não foi encontrado.");

                    atendR.Inativar();
                    repAtendimentoReceptivo.Salvar(atendR);

                    if (System.IO.File.Exists(atendR.Midia.Url))
                        System.IO.File.Delete(atendR.Midia.Url);
                }
            }
            if (atendRecepNew != null)
            {
                if (!Directory.Exists(path))
                    throw new Exception("O diretório onde a midia seria armazanada não está acessivel.");

                IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
                Usuario usuario = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name);
                if (usuario == null)
                    throw new Exception("Não foi possível identificar o usuário logado.");

                int cont = repAtendimentoReceptivo.ObterTodos().Where(a => a.Papel.Codigo == visitante.Codigo).Count() + 1;
                IRepositorioPapel repPapel = Fabrica.Instancia.ObterRepositorio<IRepositorioPapel>(UnidadeTrabalho);
                foreach (var gravacao in atendRecepNew)
                {
                    var instante = DateTime.Now;

                    string nomeDisco = string.Format("PF-{0}-{1}-AR-{2}-{3}.wav", 
                        (string.IsNullOrEmpty(visitante.PessoaFisica.CPF) ? visitante.PessoaFisica.Passaporte : visitante.PessoaFisica.CPF),
                        visitante.Codigo, instante.ToString("yyyyMMddHHmm"), cont);

                    Midia m = new Midia();
                    m.DataRegistro = instante;
                    m.Extensao = "wav";
                    m.MimeType = "audio/wav";
                    m.NomeArquivo = nomeDisco;

                    repMidia.Validar(m, EstadoObjeto.Novo);
                    repMidia.Salvar(m);

                    AtendimentoReceptivo aR = new AtendimentoReceptivo();
                    aR.DataRegistro = instante;
                    aR.Midia = m;
                    //aR.Papel = repPapel.ObterPorId(visitante.Codigo);
                    aR.Papel = visitante;
                    aR.Usuario = usuario;
                    aR.Ativar();
                    repAtendimentoReceptivo.Validar(aR, EstadoObjeto.Novo);
                    repAtendimentoReceptivo.Salvar(aR);

                    var edt = gravacao.Split(',')[1];
                    System.IO.File.WriteAllBytes(m.Url, Convert.FromBase64String(edt));

                    cont++;
                }
            }
            #endregion

            repVisit.Validar(EstadoObjeto.Alterado, visitante);

            return Json(montaObj(visitante));
        }

        /// <summary>
        /// Pesquisar.
        /// </summary>
        ///
        /// <param name="parameters">
        /// Options for controlling the operation.
        /// </param>
        /// <param name="status">
        /// O(a) status.
        /// </param>
        /// <param name="cpf">
        /// O(a) cpf.
        /// </param>
        /// <param name="empresa">
        /// O(a) empresa.
        /// </param>
        /// <param name="nome">
        /// O(a) nome.
        /// </param>
        /// <param name="cargo">
        /// O(a) cargo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Pesquisar(StoreRequestParameters parameters = null, StatusPapelLog? status = null, string cpf = null, string empresa = null, string nome = null, int cargo = 0, string id = null, string passaporte = null, string crachaativo = null)
        {
            IQueryable<Visitante> regs = FiltrarVisitante(status, cpf, empresa, nome, cargo, id, passaporte, crachaativo);
            int total = regs.Count();
            List<VisitanteModelView> data = montaObjts(regs, parameters);

            return new StoreResult(data, total);
        }

        /// <summary>
        /// Filtrar visitante.
        /// </summary>
        ///
        /// <param name="status">
        /// O(a) status.
        /// </param>
        /// <param name="cpf">
        /// O(a) cpf.
        /// </param>
        /// <param name="empresa">
        /// O(a) empresa.
        /// </param>
        /// <param name="nome">
        /// O(a) nome.
        /// </param>
        /// <param name="cargo">
        /// O(a) cargo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) IQueryable&lt;Visitante&gt;
        /// </returns>
        private IQueryable<Visitante> FiltrarVisitante(StatusPapelLog? status = null, string cpf = null, string empresa = null, string nome = null, int cargo = 0, string id = null, string passaporte = null, string crachaativo = null)
        {
            IRepositorioVisitante rep = Fabrica.Instancia.ObterRepositorio<IRepositorioVisitante>(UnidadeTrabalho);
            cpf = cpf.Replace(".", "").Replace("-", "");

            IQueryable<Visitante> visitante = rep.ObterTodos().OrderBy(p => p.Codigo);

            if (!string.IsNullOrEmpty(id))
                visitante = visitante.Where(c => c.PessoaFisica.Codigo == id);

            if (status.HasValue)
                visitante = visitante.Where(p => p.PapelLogs.OrderByDescending(x => x.Codigo).Select(m => m.Status).FirstOrDefault() == status);
            if (!string.IsNullOrEmpty(nome))
                visitante = visitante.Where(p => p.PessoaFisica.Nome.Contains(nome));
            if (!string.IsNullOrEmpty(cpf))
                visitante = visitante.Where(p => p.PessoaFisica.CPF == cpf);
            if (!string.IsNullOrEmpty(passaporte))
                visitante = visitante.Where(p => p.PessoaFisica.Passaporte == passaporte);
            if (!string.IsNullOrEmpty(empresa))
                visitante = visitante.Where(p => p.Empresa.Contains(empresa));
            if (cargo > 0)
                visitante = visitante.Where(p => p.Cargo.Codigo == cargo);

            if (!string.IsNullOrEmpty(crachaativo))
            {
                IRepositorioCracha repCracha = Fabrica.Instancia.ObterRepositorio<IRepositorioCracha>(UnidadeTrabalho);

                var cracha = repCracha.ObterTodos().Where(x => x.Numero == crachaativo && x.Ativo == true).FirstOrDefault();

                if (cracha != null)
                    visitante = visitante.Where(p => p.Codigo == cracha.Papel.Codigo);
                else
                    visitante = visitante.Where(p => p.Codigo == 0);
            }            

            return visitante.OrderBy(c => c.PessoaFisica.Nome);
        }

        /// <summary>
        /// Monta objts.
        /// </summary>
        ///
        /// <param name="regs">
        /// O(a) regs.
        /// </param>
        /// <param name="parameters">
        /// Options for controlling the operation.
        /// </param>
        /// <param name="ignoreParameters">
        /// true to ignore parameters.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;VisitanteModelView&gt;
        /// </returns>
        private List<VisitanteModelView> montaObjts(IQueryable<Visitante> regs, StoreRequestParameters parameters = null, bool ignoreParameters = false)
        {
            if (!ignoreParameters)
            {
                if (parameters == null)
                    parameters = new StoreRequestParameters();
                regs = regs.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 25);
            }

            return regs.ToList().Select(s => new VisitanteModelView
            {
                Codigo = s.Codigo,
                CodigoPessoa = s.PessoaFisica.Codigo,
                Nome = s.PessoaFisica.Nome,
                CPF = s.PessoaFisica.CPFFormatado(),
                Passaporte = s.PessoaFisica.Passaporte,
                Empresa = s.Empresa,
                Funcao = s.Funcao,
                CodigoCracha = obterCodigoCracha(s.PapelLogs[0]),
                StatusPapel = obterDesc(s.ObterLogPapel().Status),
                IdUsuario = s.ObterLogPapel().Papel.IdUsuario
            }).ToList();
        }


        private string obterCodigoCracha(PapelLog cracha)
        {
            IRepositorioCracha repCracha = Fabrica.Instancia.ObterRepositorio<IRepositorioCracha>(UnidadeTrabalho);
            var listCracha = repCracha.ObterTodos().Where(x => x.Papel.Codigo == cracha.Papel.Codigo);
            if (listCracha.Count() > 0)
                return listCracha.OrderByDescending(x => x.DataRegistro).First().Numero.ToString();
            else return null;
        }


        /// <summary>
        /// Insere telefone.
        /// </summary>
        ///
        /// <param name="propModificadas">
        /// O(a) property modificadas.
        /// </param>
        /// <param name="pessoaFisica">
        /// O(a) pessoa fisica.
        /// </param>
        /// <param name="visitante">
        /// O(a) visitante.
        /// </param>
        ///
        /// <returns>
        /// Um(a) PessoaFisica.
        /// </returns>
        private PessoaFisica insereTelefone(string[] propModificadas, PessoaFisica pessoaFisica, Visitante visitante)
        {
            IRepositorioTelefone repTelefone = Fabrica.Instancia.ObterRepositorio<IRepositorioTelefone>(UnidadeTrabalho);
            List<Telefone> telefones = new List<Telefone>(visitante.PessoaFisica.Telefones);

            if (propModificadas.Any(a => a == "TelefoneNumResidencial"))
            {
                Telefone telefoneResidencial = pessoaFisica.Telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Residencial);

                if (telefoneResidencial == null || (telefoneResidencial.Codigo == 0 && !string.IsNullOrEmpty(telefoneResidencial.TelefoneNum)))
                {
                    telefoneResidencial = new Telefone
                    {
                        DataRegistro = DateTime.Now,
                        Ativo = true,
                        TelefoneNum = (telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Residencial) != null) ? telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Residencial).TelefoneNum : "",
                        TipoTelefone = TipoTelefone.Residencial,
                        Pessoa = pessoaFisica
                    };

                    pessoaFisica.Telefones.Add(telefoneResidencial);
                    repTelefone.Salvar(telefoneResidencial);
                }
                else
                {
                    telefoneResidencial.TelefoneNum = (telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Residencial) != null) ? telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Residencial).TelefoneNum : "";
                    repTelefone.Atualizar(telefoneResidencial);
                }
            }

            if (propModificadas.Any(a => a == "TelefoneNumEmergencial"))
            {
                Telefone telefoneResidencial = pessoaFisica.Telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Emergencial);

                if (telefoneResidencial == null || (telefoneResidencial.Codigo == 0 && !string.IsNullOrEmpty(telefoneResidencial.TelefoneNum)))
                {
                    telefoneResidencial = new Telefone
                    {
                        DataRegistro = DateTime.Now,
                        Ativo = true,
                        TelefoneNum = (telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Emergencial) != null) ? telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Emergencial).TelefoneNum : "",
                        TipoTelefone = TipoTelefone.Emergencial,
                        Pessoa = pessoaFisica
                    };

                    repTelefone.Salvar(telefoneResidencial);
                    pessoaFisica.Telefones.Add(telefoneResidencial);
                }
                else
                {
                    telefoneResidencial.TelefoneNum = (telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Emergencial) != null) ? telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Emergencial).TelefoneNum : "";
                    repTelefone.Atualizar(telefoneResidencial);
                }
            }

            if (propModificadas.Any(a => a == "TelefoneNumCelular"))
            {
                Telefone telefoneResidencial = pessoaFisica.Telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Celular);

                if (telefoneResidencial == null || (telefoneResidencial.Codigo == 0 && !string.IsNullOrEmpty(telefoneResidencial.TelefoneNum)))
                {
                    telefoneResidencial = new Telefone
                    {
                        DataRegistro = DateTime.Now,
                        Ativo = true,
                        TelefoneNum = (telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Celular) != null) ? telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Celular).TelefoneNum : "",
                        TipoTelefone = TipoTelefone.Celular,
                        Pessoa = pessoaFisica
                    };

                    repTelefone.Salvar(telefoneResidencial);
                    pessoaFisica.Telefones.Add(telefoneResidencial);
                }
                else
                {
                    telefoneResidencial.TelefoneNum = (telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Celular) != null) ? telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Celular).TelefoneNum : "";
                    repTelefone.Atualizar(telefoneResidencial);
                }
            }

            return pessoaFisica;
        }

        /// <summary>
        /// Selecionar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Selecionar(int codigo)
        {
            IRepositorioVisitante rep = Fabrica.Instancia.ObterRepositorio<IRepositorioVisitante>(UnidadeTrabalho);
            Visitante visitante = rep.ObterPorId(codigo);
            return Json(montaObj(visitante));
        }

        /// <summary>
        /// Retorna todos os objetos do tipo Visitante.
        /// </summary>
        ///
        /// <returns>
        /// Detalhes do tipo de objeto ligado ao controller no formato JSON.
        /// </returns>
        public ActionResult ObterTodos()
        {
            IRepositorioVisitante repVisitante = Fabrica.Instancia.ObterRepositorio<IRepositorioVisitante>(UnidadeTrabalho);

            IQueryable<Visitante> visitante = repVisitante.ObterTodos();

            return Json(visitante.Select(v => new
            {
                v.Codigo,
                CodigoPessoa = v.PessoaFisica.Codigo,
                CPF = v.PessoaFisica.CPF != null ? v.PessoaFisica.CPFFormatado() : null,
                v.PessoaFisica.Passaporte,
                v.PessoaFisica.Nome,
                v.Empresa,
                v.Funcao,
                StatusPapel = v.ObterLogPapel() != null ? obterDesc(v.ObterLogPapel().Status) : obterDesc(StatusPapelLog.Ativo)

            }).ToArray().OrderBy(p => p.Nome));
        }

        /// <summary>
        /// Obter description.
        /// </summary>
        ///
        /// <param name="statusLog">
        /// O(a) status log.
        /// </param>
        ///
        /// <returns>
        /// Um(a) object.
        /// </returns>
        private string obterDesc(StatusPapelLog statusLog)
        {
            return EnumExtensoes.ObterDescricaoEnum(statusLog);
        }

        /// <summary>
        /// Obter description.
        /// </summary>
        ///
        /// <param name="statusDocumento">
        /// O(a) status documento.
        /// </param>
        ///
        /// <returns>
        /// Um(a) object.
        /// </returns>
        private object obterDesc(StatusDocumento statusDocumento)
        {
            return EnumExtensoes.ObterDescricaoEnum(statusDocumento);
        }

        /// <summary>
        /// Obter description tipo visitante.
        /// </summary>
        ///
        /// <param name="tipoVisitante">
        /// O(a) tipo visitante.
        /// </param>
        ///
        /// <returns>
        /// Um(a) string.
        /// </returns>
        private string obterDescTipoVisitante(TipoVisitante tipoVisitante)
        {
            return EnumExtensoes.ObterDescricaoEnum(tipoVisitante);
        }

        /// <summary>
        /// Lista tipo visitante.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ListaTipoVisitante()
        {
            Dictionary<int, string> dic = new Dictionary<int, string>();

            foreach (int item in Enum.GetValues(typeof(TipoVisitante)))
            {
                dic.Add(item, ((TipoVisitante)item).ObterDescricaoEnum());
            }
            return Json(dic.Select(d => new
            {
                Codigo = d.Key,
                Descricao = d.Value
            }).ToList());

        }

        /// <summary>
        /// Cria um objeto contendo informações relacionadas ao conteudo gerenciado por
        /// VisitanteController.
        /// </summary>
        ///
        /// <param name="visitante">
        /// O(a) visitante.
        /// </param>
        /// <param name="imageUrl">
        /// URL of the image.
        /// </param>
        ///
        /// <returns>
        /// Um(a) object.
        /// </returns>
        private object montaObj(Visitante visitante, string imageUrl = null)
        {
            Endereco endereco = visitante.PessoaFisica.Endereco;

            Telefone telefoneRes = visitante.PessoaFisica.Telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Residencial);
            Telefone telefoneEmer = visitante.PessoaFisica.Telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Emergencial);
            Telefone telefoneCel = visitante.PessoaFisica.Telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Celular);

            var visitanteAux = new VisitanteModelView();

            visitanteAux.CodigoVisitante = visitante.Codigo;
            visitanteAux.Codigo = visitante.Codigo;

            visitanteAux.CodigoPessoa = visitante.PessoaFisica.Codigo;
            visitanteAux.CPF = visitante.PessoaFisica.CPF != null ? visitante.PessoaFisica.CPFFormatado() : null;
            visitanteAux.Passaporte = visitante.PessoaFisica.Passaporte;
            visitanteAux.Nome = visitante.PessoaFisica.Nome;
            visitanteAux.Apelido = visitante.PessoaFisica.Apelido;
            visitanteAux.Email = visitante.PessoaFisica.Email;
            visitanteAux.DataNascimento = visitante.PessoaFisica.DataNascimento;
            visitanteAux.DataVisto = visitante.PessoaFisica.DataVisto;
            visitanteAux.DataValidadeVisto = visitante.PessoaFisica.DataValidadeVisto;
            visitanteAux.Sexo = visitante.PessoaFisica.Sexo.ObterDescricaoEnum();
            visitanteAux.NomeMae = visitante.PessoaFisica.NomeMae;
            visitanteAux.NomePai = visitante.PessoaFisica.NomePai;
            visitanteAux.RNE = visitante.PessoaFisica.RNE;
            visitanteAux.DataExpedicaoRNE = visitante.PessoaFisica.DataExpedicaoRNE;
            visitanteAux.EstadoCivil = visitante.PessoaFisica.EstadoCivil;
            visitanteAux.Escolaridade = visitante.PessoaFisica.Escolaridade;
            visitanteAux.TipoSanguineo = visitante.PessoaFisica.TipoSanguineo;

            if (visitante.PessoaFisica.Foto != null)
                visitanteAux.Img = Convert.ToBase64String(visitante.PessoaFisica.Foto); ;

            visitanteAux.Nacionalidade = visitante.PessoaFisica.Nacionalidade;
            visitanteAux.NaturalidadeUF = visitante.PessoaFisica.Naturalidade != null && visitante.PessoaFisica.Naturalidade.Estado != null ? visitante.PessoaFisica.Naturalidade.Estado.Codigo : (int?)null;
            visitanteAux.Naturalidade = visitante.PessoaFisica.Naturalidade != null ? visitante.PessoaFisica.Naturalidade.Codigo : (int?)null;

            //RG
            visitanteAux.RG = visitante.PessoaFisica.RG != null ? visitante.PessoaFisica.RG : null;
            visitanteAux.RGOrgaoEmissor = visitante.PessoaFisica.RGOrgaoEmissor != null ? visitante.PessoaFisica.RGOrgaoEmissor : null;
            visitanteAux.RGOrgaoEmissorUF = visitante.PessoaFisica.RGOrgaoEmissorUF != null ? visitante.PessoaFisica.RGOrgaoEmissorUF.Codigo : (int?)null;
            visitanteAux.RGDataEmissao = visitante.PessoaFisica.RGDataEmissao != null ? visitante.PessoaFisica.RGDataEmissao : (DateTime?)null;

            //Titulo de eleitor
            visitanteAux.TituloEleitor = visitante.PessoaFisica.TituloEleitor != null ? visitante.PessoaFisica.TituloEleitor : (int?)null;
            visitanteAux.TituloEleitorSecao = visitante.PessoaFisica.TituloEleitorSecao != null ? visitante.PessoaFisica.TituloEleitorSecao : (int?)null;
            visitanteAux.TituloEleitorZona = visitante.PessoaFisica.TituloEleitorZona != null ? visitante.PessoaFisica.TituloEleitorZona : (int?)null;
            visitanteAux.TituloEleitorCidade = visitante.PessoaFisica.TituloEleitorCidade != null ? visitante.PessoaFisica.TituloEleitorCidade.Codigo : (int?)null;
            visitanteAux.TituloEleitorEstado = (visitante.PessoaFisica.TituloEleitorCidade != null && visitante.PessoaFisica.TituloEleitorCidade.Estado != null) ? visitante.PessoaFisica.TituloEleitorCidade.Estado.Codigo : (int?)null;


            ////CTPS                
            visitanteAux.CTPS = visitante.PessoaFisica.CTPS != null ? visitante.PessoaFisica.CTPS : (int?)null;
            visitanteAux.CTPSData = visitante.PessoaFisica.CTPSData != null ? visitante.PessoaFisica.CTPSData : (DateTime?)null;
            visitanteAux.CTPSSerie = visitante.PessoaFisica.CTPSSerie != null ? visitante.PessoaFisica.CTPSSerie : null;
            visitanteAux.CTPSEstado = visitante.PessoaFisica.CTPSEstado != null ? visitante.PessoaFisica.CTPSEstado.Codigo : (int?)null;

            ////CNH
            visitanteAux.CNH = visitante.PessoaFisica.CNH != null ? visitante.PessoaFisica.CNH : null;
            visitanteAux.CNHCategoria = visitante.PessoaFisica.CNHCategoria != null ? visitante.PessoaFisica.CNHCategoria : null;
            visitanteAux.CNHDataValidade = visitante.PessoaFisica.CNHDataValidade != null ? visitante.PessoaFisica.CNHDataValidade : (DateTime?)null;

            ////Certificado de Reservista
            visitanteAux.CertificadoReservista = visitante.PessoaFisica.CertificadoReservista != null ? visitante.PessoaFisica.CertificadoReservista : null;
            visitanteAux.CertificadoReservistaCat = visitante.PessoaFisica.CertificadoReservistaCat != null ? visitante.PessoaFisica.CertificadoReservistaCat : null;

            ////Telefones
            visitanteAux.TelefoneNumResidencial = telefoneRes != null ? telefoneRes.TelefoneNum : null;
            visitanteAux.TelefoneNumEmergencial = telefoneEmer != null ? telefoneEmer.TelefoneNum : null;
            visitanteAux.TelefoneNumCelular = telefoneCel != null ? telefoneCel.TelefoneNum : null;

            if (endereco != null)
            {
                if (!string.IsNullOrEmpty(endereco.Complemento) && endereco.Logradouro == null)
                {
                    visitanteAux.Endereco = new EnderecoMovelView()
                    {
                        CodigoEndereco = endereco.Codigo,
                        Complemento = endereco.Complemento
                    };
                }
                else
                {
                    visitanteAux.Endereco = new EnderecoMovelView()
                    {
                        CodigoEndereco = endereco.Codigo,
                        CodigoLog = endereco.Logradouro.Codigo,
                        Pais = endereco.Logradouro.Bairro.Cidade.Estado.Pais.Nome,
                        Estado = endereco.Logradouro.Bairro.Cidade.Estado.Nome,
                        Cidade = endereco.Logradouro.Bairro.Cidade.Nome,
                        Bairro = endereco.Logradouro.Bairro.Nome,
                        Logradouro = endereco.Logradouro.Nome,
                        Numero = endereco.Numero,
                        Complemento = endereco.Complemento,
                        CEP = endereco.Logradouro.CEP//,
                        //TipoLogradouro = endereco.Logradouro.TipoLogradouro.Nome
                    };
                }
            }
            //dados de visitante
            visitanteAux.AntiPassBack = visitante.AntiPassBack;
            visitanteAux.AcessoPorto = visitante.AcessoPorto;
            visitanteAux.TipoVisitante = obterDescTipoVisitante(visitante.TipoVisitante);
            visitanteAux.Funcao = visitante.Funcao;
            visitanteAux.Motivo = visitante.Motivo;
            visitanteAux.StatusPapel = visitante.ObterLogPapel() != null ? visitante.ObterLogPapel().Status.ObterDescricaoEnum() : null;
            visitanteAux.DataStatusPapel = visitante.ObterLogPapel() != null ? visitante.ObterLogPapel().DataInicio : (DateTime?)null;
            visitanteAux.Empresa = visitante.Empresa;
            visitanteAux.DataInicio = visitante.DataInicio;
            visitanteAux.DataFim = visitante.DataFim;
            visitanteAux.ModeloCracha = visitante.ModeloCracha != null ? visitante.ModeloCracha.Nome : null;
            visitanteAux.IdUsuario = visitante.IdUsuario != null ? visitante.IdUsuario.Trim() : null;

            IRepositorioColaborador repColab = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);

            Colaborador solic = repColab.ObterPorId(visitante.Solicitante.Codigo);
            Colaborador respon = repColab.ObterPorId(visitante.ResponsavelVisita.Codigo);
            visitanteAux.Solicitante = new ColaboradorModelView()
            {
                Codigo = visitante.Solicitante.Codigo,

                Nome = solic.PessoaFisica.Nome + " / " + solic.PessoaFisica.CPFFormatado() + " / " + solic.Empresa.Apelido
            };
            visitanteAux.ResponsavelVisita = new ColaboradorModelView()
            {
                Codigo = visitante.ResponsavelVisita.Codigo,
                Nome = respon.PessoaFisica.Nome + " / " + respon.PessoaFisica.CPFFormatado() + " / " + respon.Empresa.Apelido
            };

            foreach (DocumentoPapel item in visitante.Documentos)
            {
                var xxx = item;
            }
            //documentos
            visitanteAux.Documentos =
                visitante.Documentos.Select(d => new DocumentoPapelModelView()
            {
                CodigoDocumento = d.Documento.Codigo,
                Codigo = d.Documento.TipoDocumento.Codigo,
                Sigla = d.Documento.TipoDocumento.Sigla,
                DataValidade = d.Documento.DataValidade,
                Descricao = d.Documento.Descricao,
                //DiasValidade = d.Documento.TipoDocumento.DiasValidade,
                Status = obterDesc(d.Documento.Status).ToString(),
                QtdMidias = d.Documento.Midias.Count(m => m.Ativo),
                Midias = d.Documento.Midias.Select(dm => new MidiaModelView()
                {
                    CodigoMidia = dm.Midia.Codigo,
                    Arquivo = string.Empty,
                    DataRegistro = dm.Midia.DataRegistro,
                    Extensao = dm.Midia.Extensao,
                    MIME = dm.Midia.MimeType,
                    NomeDisco = dm.Midia.NomeArquivo,
                    NomeOriginal = string.Empty,
                    URL = dm.Midia.Url
                }).ToList()

            }).ToList();

            //treinamentos
            visitanteAux.Treinamentos = visitante.Treinamentos.Select(d => new TreinamentoPessoaModelView()
            {
                Codigo = d.Codigo,
                DataRealizacao = d.DataRealizacao,
                DataValidade = d.DataValidade,
                Nome = d.Treinamento.Nome,
                CodigoTreinamento = d.Treinamento.Codigo
            }).ToList();
            ////histórico de treinamento
            for (int c = 0; c < visitante.PessoaFisica.Papeis.Count(); c++)
            {
                for (int i = 0; i < visitante.PessoaFisica.Papeis[c].Treinamentos.Count(); i++)
                {
                    var TreinamentosPessoa = new TreinamentoPessoaModelView()
                    {
                        Codigo = visitante.PessoaFisica.Papeis[c].Codigo,
                        DataRealizacao = visitante.PessoaFisica.Papeis[c].Treinamentos[i].DataRealizacao,
                        DataValidade = visitante.PessoaFisica.Papeis[c].Treinamentos[i].DataValidade,
                        Nome = visitante.PessoaFisica.Papeis[c].Treinamentos[i].Treinamento.Descricao,
                        CodigoTreinamento = visitante.PessoaFisica.Papeis[c].Treinamentos[i].Codigo
                    };
                    visitanteAux.TreinamentosPessoa.Add(TreinamentosPessoa);
                }
            }

            visitanteAux.HistoricoEmpresa = ObterHistoricoEmpresa(visitante.PessoaFisica.CPF);

            visitanteAux.AtendimentosReceptivo = visitante.AtendimentosReceptivo.Select(a => new AtendimentoReceptivoModelView()
            {
                Codigo = a.Codigo,
                CodigoMidia = a.Midia.Codigo,
                DataRegistro = a.DataRegistro,
                NomeArquivo = a.Midia.NomeArquivo
            }).ToList();
            return visitanteAux;

        }

        /// <summary>
        /// Obter historico empresa.
        /// </summary>
        ///
        /// <param name="cpf">
        /// O(a) cpf.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;Empresa&gt;
        /// </returns>
        private List<Visitante> ObterHistoricoEmpresa(string cpf)
        {
            IRepositorioVisitante rep = Fabrica.Instancia.ObterRepositorio<IRepositorioVisitante>(UnidadeTrabalho);
            List<Visitante> visitantes = rep.ObterTodos().Where(v => v.PessoaFisica.CPF == cpf).Select(p => new Visitante()
            {
                Codigo = p.Codigo,
                Empresa = p.Empresa,
                Funcao = p.Funcao,
                DataInicio = p.DataInicio,
                DataFim = p.DataFim
            }).OrderByDescending(v => v.DataInicio).ToList();
            return visitantes;
        }
        #region Webcam / Jcrop

        /// <summary>
        /// Edit.
        /// </summary>
        ///
        /// <param name="x">
        /// The x coordinate.
        /// </param>
        /// <param name="y">
        /// The y coordinate.
        /// </param>
        /// <param name="w">
        /// The width.
        /// </param>
        /// <param name="h">
        /// The height.
        /// </param>
        /// <param name="imageUrl">
        /// URL of the image.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [HttpPost]
        public ActionResult Edit(int x, int y, int w, int h, string imageUrl)
        {
            var image = new WebImage("~" + imageUrl);
            var height = image.Height;
            var width = image.Width;

            image.Crop((int)x, (int)y, (int)(height - w), (int)(width - h));

            int indiceDoPonto = imageUrl.IndexOf(".");
            string antesPonto = imageUrl.Substring(0, indiceDoPonto);
            string depoisPonto = imageUrl.Substring(indiceDoPonto, imageUrl.Length - indiceDoPonto);
            string nome = antesPonto + "_crop" + depoisPonto;

            image.Save(Server.MapPath("~" + nome));

            return Json(nome);

        }

        /// <summary>
        /// Upload.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Upload()
        {
            if (Request.ContentLength > 1048576)
                throw new CoreException("Não é possível anexar, o tamanho do arquivo excede 4MB.");

            HttpPostedFileBase postedFile = Request.Files[0];
            string unique = Guid.NewGuid().ToString("N");
            string nomeArquivo = unique + Path.GetExtension(postedFile.FileName);

            const int maxW = 800;
            const int maxH = 600;
            WebImage image = new WebImage(postedFile.InputStream);
            int w = image.Width;
            int h = image.Height;
            if ((w < 128) || (h < 128))
                throw new CoreException("Tamanho da imagem inferior a 128px.");

            if ((w > maxW) || (h > maxH))
            {
                if (w > h)
                {
                    w = maxW;
                    h = image.Height * w / image.Width;
                }
                else
                {
                    h = maxH;
                    w = image.Width * h / image.Height;
                }
            }
            image.Resize(w, h);
            image.Save(Server.MapPath("~/Content/img/webcam/" + nomeArquivo));

            return Json(nomeArquivo);
        }

        /// <summary>
        /// Carregar imagem.
        /// </summary>
        ///
        /// <param name="imagem">
        /// O(a) imagem.
        /// </param>
        ///
        /// <returns>
        /// Um(a) byte[].
        /// </returns>
        private byte[] CarregarImagem(string imagem)
        {
            byte[] imagemBytes = null;
            string caminhoCompletoImagem = imagem;

            int posicao = caminhoCompletoImagem.IndexOf("/Content");
            string caminho = caminhoCompletoImagem.Substring(posicao);
            imagemBytes = System.IO.File.ReadAllBytes(Server.MapPath("~" + caminho));
            return imagemBytes;
        }

        //private string GravarTemporario(byte[] foto)
        //{
        //    if (foto != null)
        //    {
        //        Random randNum = new Random();
        //        int number = randNum.Next(DateTime.Now.Millisecond);

        //        string nomeArquivo = number + ".jpeg";

        //        byte[] b = foto;

        //        using (FileStream fs = new FileStream(Server.MapPath("~/Content/img/webcam/") + nomeArquivo, FileMode.Create))
        //            fs.Write(b, 0, b.Length);

        //        return "/Content/img/webcam/" + nomeArquivo;
        //    }
        //    return "/Content/img/avatar.png";
        //}

        #endregion


    }
}
