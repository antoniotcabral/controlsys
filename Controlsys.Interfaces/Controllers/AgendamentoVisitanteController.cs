﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using Controlsys.Infra;
//using Controlsys.Repositorio.Audit;
using Globalsys;
using Controlsys.Repositorios.Pessoas;
using Controlsys.Infra;
using Controlsys.Dominio.Pessoas;
using Controlsys.Dominio.AgendamentosPessoa;
using Controlsys.Repositorios.Seguranca;
using Controlsys.Repositorios.Agendamentos;
using Controlsys.Repositorio.Agendamentos;
using Globalsys.Exceptions;
using Controlsys.Interfaces.Models;
using Globalsys.Validacao;
using Ext.Net;
using Ext.Net.MVC;
using Controlsys.Dominio.Seguranca;
using System.Text;
using Globalsys.Relatorios;
using System.IO;
using System.Globalization;
using NPOI.XSSF.UserModel; //para arquivos XLSX
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using ICSharpCode.SharpZipLib;
using Controlsys.Repositorios.Enderecos;
using Controlsys.Dominio.Enderecos;
using Controlsys.Repositorios.Parametros;
using Controlsys.Dominio.Parametros;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class AgendamentoVisitanteController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /AgendamentoVisitante/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.AgendamentoVisitanteController.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public AgendamentoVisitanteController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        public ActionResult Index()
        {
            IRepositorioPermissao repPermissao = Fabrica.Instancia.ObterRepositorio<IRepositorioPermissao>(UnidadeTrabalho);
            List<GrupoUsuario> gruposUsuarioPermissaoImportar = repPermissao.ObterTodos().Where(p => p.Acao.Url == "/AgendamentoVisitante/ImportarModelo").SelectMany(p => p.Grupo.GruposUsuarios).ToList();
            List<string> pessoasFisicasPermissaoImportar = gruposUsuarioPermissaoImportar.Select(u => u.Usuario.PessoaFisica.Codigo).ToList();

            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            Usuario usuarioLogado = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name);

            if (pessoasFisicasPermissaoImportar.Contains(usuarioLogado.Codigo))
                return View("Index", model: true);

            return View("Index", model: false);
        }

        public ActionResult ObterTodos(StoreRequestParameters parameters = null, string cpf = null,string passaporte = null, string empresa = null, string nome = null, string funcao = null, StatusAgendamento? status = null, DateTime? dtInicioAgendamento = null, DateTime? dtFimAgendamento = null)
        {
            IRepositorioAgendamentoVisitante repVisitante = Fabrica.Instancia.ObterRepositorio<IRepositorioAgendamentoVisitante>(UnidadeTrabalho);

            IQueryable<VisitanteAgendamento> visitante = repVisitante.ObterTodos();

            if (status.HasValue)
                visitante = visitante.Where(p => p.Status.OrderByDescending(x => x.Codigo).Select(m => m.StatusAgendamento).FirstOrDefault() == status);
            if (!string.IsNullOrEmpty(cpf))
            {
                cpf = cpf.Replace(".", "").Replace("-", "");
                visitante = visitante.Where(t => t.PessoaAg.CPF == cpf);
            }
            if (!string.IsNullOrEmpty(nome))
                visitante = visitante.Where(p => p.PessoaAg.Nome.Contains(nome));
            if (!string.IsNullOrEmpty(empresa))
                visitante = visitante.Where(p => p.Empresa.Contains(empresa));
            if (!string.IsNullOrEmpty(funcao))
                visitante = visitante.Where(p => p.Funcao.Contains(funcao));
            if (!string.IsNullOrEmpty(passaporte))
                visitante = visitante.Where(p => p.PessoaAg.Passaporte == passaporte);
            if (dtInicioAgendamento != null)
                visitante = visitante.Where(p => p.DataInicio >= dtInicioAgendamento);
            if (dtFimAgendamento != null)
                visitante = visitante.Where(p => p.DataFim <= dtFimAgendamento);
            if (parameters != null)
            {
                int total = visitante.Count();
                List<VisitanteAgendamentoModelView> data = montaObjts(visitante, parameters);

                return new StoreResult(data, total);
            }

            return Json(visitante.Select(v => new
            {
                v.Codigo,
                CPF = v.PessoaAg.CPF != null ? v.PessoaAg.CPFFormatado() : null,
                v.PessoaAg.Passaporte,
                v.PessoaAg.Nome,
                v.Empresa,
                v.Funcao,
                StatusAgendamento = v.ObterStatusAtual() != null ? obterDesc(v.ObterStatusAtual().StatusAgendamento) : obterDesc(StatusAgendamento.AguardandoAprovacao),
                dtInicioAgendamento = v.DataInicio != null ? v.DataInicio.ToShortDateString() : "",
                dtFimAgendamento = v.DataFim != null ? v.DataFim.ToShortDateString() : ""

            }).ToArray().OrderBy(p => p.Nome));
        }

        private List<VisitanteAgendamentoModelView> montaObjts(IQueryable<VisitanteAgendamento> regs, StoreRequestParameters parameters = null, bool ignoreParameters = false)
        {
            if (!ignoreParameters)
            {
                if (parameters == null)
                    parameters = new StoreRequestParameters();
                regs = regs.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 25);
            }

            regs.OrderBy(t => t.PessoaAg.Nome);
            return regs.Select(t => montaObj(t)).ToList();
        }

        private string obterDesc(StatusAgendamento statusAg)
        {
            return EnumExtensoes.ObterDescricaoEnum(statusAg);
        }

        [Transaction]
        public ActionResult Alterar(VisitanteAgendamento visitante, string[] propModificadasPessoa)
        {
            IRepositorioPessoaAgendamento repPessoaAg = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaAgendamento>(UnidadeTrabalho);
            IRepositorioAgendamentoVisitante repVisit = Fabrica.Instancia.ObterRepositorio<IRepositorioAgendamentoVisitante>(UnidadeTrabalho);

            if (visitante.PessoaAg.CPF != null)
                visitante.PessoaAg.CPF = visitante.PessoaAg.CPF.Replace("-", "").Replace(".", "");

            PessoaAgendamento pessoaAg = visitante.PessoaAg;
            pessoaAg = repPessoaAg.ObterPorId(visitante.PessoaAg, propModificadasPessoa);
            repPessoaAg.Validar(EstadoObjeto.Alterado, pessoaAg, true);
            repPessoaAg.Atualizar(pessoaAg);


            var ResponsavelVis = visitante.ResponsavelVisita;
            visitante = repVisit.ObterPorId(visitante, propModificadasPessoa);

            if (visitante.ResponsavelVisita.Codigo != ResponsavelVis.Codigo)
            {
                visitante.ResponsavelVisita = new Colaborador()
                {
                    Codigo = ResponsavelVis.Codigo
                };
            }

            if (propModificadasPessoa == null)
                propModificadasPessoa = new string[1];

            repVisit.Validar(EstadoObjeto.Alterado, visitante);
            repVisit.Atualizar(visitante);

            return Json(montaObj(visitante));
        }

        public ActionResult DataInicialVisita()
        {
            IRepositorioParametro repParam = Fabrica.Instancia.ObterRepositorio<IRepositorioParametro>(UnidadeTrabalho);
            return Json(new { Data = Convert.ToDateTime(DateTime.Now.AddDays(Convert.ToInt32(repParam.RecuperarParametro(ParametroSistema.NumeroDiaInicioVisita) != null ? repParam.RecuperarParametro(ParametroSistema.NumeroDiaInicioVisita).Valor : "0")).ToShortDateString()) });
        }
        [Transaction]
        public ActionResult Incluir(VisitanteAgendamento visitante, string[] propModificadasPessoa)
        {

            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            IRepositorioAgendamentoVisitante repVisit = Fabrica.Instancia.ObterRepositorio<IRepositorioAgendamentoVisitante>(UnidadeTrabalho);
            IRepositorioColaborador repColab = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);
            IRepositorioPessoaAgendamento repPessoaAg = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaAgendamento>(UnidadeTrabalho);
            IRepositorioPapelAgendamento repPapelAg = Fabrica.Instancia.ObterRepositorio<IRepositorioPapelAgendamento>(UnidadeTrabalho);


            if (visitante.PessoaAg.CPF != null)
                visitante.PessoaAg.CPF = visitante.PessoaAg.CPF.Replace("-", "").Replace(".", "");

            if (visitante.PessoaAg.Codigo != 0)
            {
                // Pessoa já cadastrada
                visitante.PessoaAg = repPessoaAg.ObterPorId(visitante.PessoaAg, propModificadasPessoa);
                repPessoaAg.Validar(EstadoObjeto.Alterado, visitante.PessoaAg, true);
                repPessoaAg.Atualizar(visitante.PessoaAg);
            }
            else
            {
                // Novo registro e Pessoa
                visitante.PessoaAg.DataRegistro = DateTime.Now;
                repPessoaAg.Validar(EstadoObjeto.Novo, visitante.PessoaAg, true);
                repPessoaAg.Salvar(visitante.PessoaAg);
            }

            visitante.Solicitante = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name);
            visitante.ResponsavelVisita = repColab.ObterPorId(visitante.ResponsavelVisita.Codigo);

            //cadastrar papel
            visitante.DataRegistro = DateTime.Now;
            repVisit.Validar(EstadoObjeto.Novo, visitante);
            repVisit.Salvar(visitante);

            // cadastro do status papel agendamento
            repPapelAg.SalvarStatusPapelAg(visitante.InserirPapelAg(StatusAgendamento.AguardandoAprovacao, null));

            return Json(montaObj(visitante));
        }

        public ActionResult Selecionar(int codigo)
        {
            IRepositorioAgendamentoVisitante rep = Fabrica.Instancia.ObterRepositorio<IRepositorioAgendamentoVisitante>(UnidadeTrabalho);
            VisitanteAgendamento visitante = rep.ObterPorId(codigo);
            return Json(montaObj(visitante));
        }

        private VisitanteAgendamentoModelView montaObj(VisitanteAgendamento visitante)
        {
            VisitanteAgendamentoModelView agVis = new VisitanteAgendamentoModelView();
            agVis.Codigo = visitante.Codigo;
            agVis.CodigoVisitante = visitante.Codigo;
            agVis.CodigoPessoa = visitante.PessoaAg.Codigo;
            agVis.CPF = visitante.PessoaAg.CPF != null ? visitante.PessoaAg.CPFFormatado() : null;
            agVis.Empresa = visitante.Empresa;
            agVis.Funcao = visitante.Funcao;
            agVis.Nome = visitante.PessoaAg.Nome;
            agVis.Passaporte = visitante.PessoaAg.Passaporte != null ? visitante.PessoaAg.Passaporte : null;
            agVis.StatusAgendamento = visitante.ObterStatusAtual() != null ? visitante.ObterStatusAtual().StatusAgendamento.ObterDescricaoEnum() : null;
            agVis.Apelido = visitante.PessoaAg.Apelido;
            agVis.Email = visitante.PessoaAg.Email;
            agVis.Sexo = visitante.PessoaAg.Sexo.ToString();
            agVis.TelefoneNumEmergencial = visitante.PessoaAg.TelefoneNumEmergencial;
            agVis.TelefoneNumCelular = visitante.PessoaAg.TelefoneNumCelular;
            agVis.AcessoPorto = visitante.AcessoPorto;
            agVis.Motivo = visitante.Motivo;
            agVis.DataInicio = visitante.DataInicio;
            agVis.DataFim = visitante.DataFim;
            agVis.RG = visitante.PessoaAg.RG;
            agVis.RGDataEmissao = visitante.PessoaAg.RGDataEmissao;
            agVis.RGOrgaoEmissor = visitante.PessoaAg.RGOrgaoEmissor;
            agVis.RGOrgaoEmissorUF = visitante.PessoaAg.RGOrgaoEmissorUF != null ? visitante.PessoaAg.RGOrgaoEmissorUF.Codigo : (int?)0;

            IRepositorioColaborador repColab = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);
            IRepositorioUsuario repUs = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);

            Usuario solic = repUs.ObterPorId(visitante.Solicitante.Codigo);
            Colaborador respon = repColab.ObterPorId(visitante.ResponsavelVisita.Codigo);

            var solicPapel = solic.PessoaFisica.ObterPapel(buscarPrest: false, buscarVisit: false);
            if (solicPapel == null)
                throw new CoreException("Para fazer o agendamento, o solicitante tem que ser um colaborador.");
            else
            {
                agVis.SolicitanteResp = new ColaboradorModelView()
                {
                    Codigo = solicPapel.Codigo,
                    Nome = string.Format("{0} / {1}", solic.PessoaFisica.Nome,
                    solicPapel is Colaborador ? (solicPapel as Colaborador).Empresa.Apelido :
                    (solicPapel is PrestadorServico ? (solicPapel as PrestadorServico).Empresa.Apelido :
                    (solicPapel is Visitante ? (solicPapel as Visitante).Empresa : "")))
                };
            }
            agVis.Solicitante = string.Format("{0} / {1}", solic.PessoaFisica.Nome, string.IsNullOrEmpty(solic.PessoaFisica.CPF) ? solic.PessoaFisica.Passaporte : solic.PessoaFisica.CPFFormatado());
            agVis.Responsavel = new ColaboradorModelView()
            {
                Codigo = visitante.ResponsavelVisita.Codigo,
                Nome = respon.PessoaFisica.Nome + " / " + respon.PessoaFisica.CPFFormatado() + " / " + respon.Empresa.Apelido
            };

            agVis.HistoricoStatus = visitante.Status.Select(h => new StatusPapelAgendamentoModelView()
            {
                Codigo = h.Codigo,
                Data = h.DataRegistro,
                Status = h.StatusAgendamento.ObterDescricaoEnum(),
                Observacao = h.Observacao
            }).ToList();

            return agVis;
        }

        [Transaction]
        public ActionResult Reprovar(int codigo, string observacao)
        {
            IRepositorioAgendamentoVisitante repVisit = Fabrica.Instancia.ObterRepositorio<IRepositorioAgendamentoVisitante>(UnidadeTrabalho);
            IRepositorioPapelAgendamento repPapelAg = Fabrica.Instancia.ObterRepositorio<IRepositorioPapelAgendamento>(UnidadeTrabalho);

            VisitanteAgendamento visitante = repVisit.ObterPorId(codigo);
            if (visitante.ObterStatusAtual().StatusAgendamento == StatusAgendamento.AguardandoAprovacao)
                repPapelAg.SalvarStatusPapelAg(visitante.InserirPapelAg(StatusAgendamento.CadastroNaoAprovado, observacao));

            return Json(montaObj(visitante));
        }

        private void IncluirExcel(VisitanteAgendamento visitante)
        {
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            IRepositorioAgendamentoVisitante repVisit = Fabrica.Instancia.ObterRepositorio<IRepositorioAgendamentoVisitante>(UnidadeTrabalho);
            IRepositorioColaborador repColab = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);
            IRepositorioPessoaAgendamento repPessoaAg = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaAgendamento>(UnidadeTrabalho);
            IRepositorioPapelAgendamento repPapelAg = Fabrica.Instancia.ObterRepositorio<IRepositorioPapelAgendamento>(UnidadeTrabalho);

            visitante.PessoaAg.DataRegistro = DateTime.Now;
            repPessoaAg.Salvar(visitante.PessoaAg);

            //cadastrar papel
            repPapelAg.SalvarStatusPapelAg(visitante.InserirPapelAg(StatusAgendamento.AguardandoAprovacao, null));
            visitante.DataRegistro = DateTime.Now;
            repVisit.Salvar(visitante);
        }


        public ActionResult ImportarModelo()
        {
            var fileUpload = this.GetCmp<FileUploadField>("baixarArq");
            var extension = Path.GetExtension(fileUpload.FileName);
            try
            {

                if (!fileUpload.HasFile) throw new CoreException("Arquivo nulo inválido.");
                if (!(extension == ".xlsx" || extension == ".xls")) throw new CoreException("Formato do arquivo inválido.");

                IRepositorioPessoaFisica repPessoaFisica = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaFisica>(UnidadeTrabalho);
                bool linhaVazia = true;
                bool importouAlgum = false;
                string messagemResult = string.Empty;

                XSSFWorkbook hssfwb = new XSSFWorkbook(fileUpload.PostedFile.InputStream);
                ISheet sheet = hssfwb.GetSheet("Plan1");

                int totalRows = 0;
                for (int row = 1; row <= sheet.LastRowNum; row++)
                {
                    if (sheet.GetRow(row) == null) continue;
                    totalRows++;
                }

                string maskNum = "";
                if (totalRows < 10) maskNum = "0";
                else if (totalRows < 100) maskNum = "00";
                else if (totalRows < 1000) maskNum = "000";
                //else if (totalRows < 10000) maskNum = "0000";
                else throw new CoreException("Não é possível importar mais de 999 registros simultaneamente.");


                List<string> listaErro = new List<string>();
                for (int row = 1; row <= sheet.LastRowNum; row++)
                {
                    if (sheet.GetRow(row) == null) continue;

                    int numAux = 0;
                    string textAux = "";
                    DateTime dateAux = new DateTime();
                    List<string> errosNaLinha = new List<string>();

                    var pessoaAg = new PessoaAgendamento();
                    pessoaAg.Codigo = 0;
                    pessoaAg.CPF = sheet.GetRow(row).GetCell(0) != null ? sheet.GetRow(row).GetCell(0).StringCellValue.Trim() : null; //CPF
                    if (string.IsNullOrEmpty(pessoaAg.CPF))
                        pessoaAg.Passaporte = sheet.GetRow(row).GetCell(1) != null ? sheet.GetRow(row).GetCell(1).StringCellValue.Trim() : null; //Passaporte

                    pessoaAg.Nome = sheet.GetRow(row).GetCell(2) != null && sheet.GetRow(row).GetCell(2).StringCellValue.Trim().ToString() != "" && sheet.GetRow(row).GetCell(2).StringCellValue.Trim().ToString() != string.Empty ? sheet.GetRow(row).GetCell(2).StringCellValue.Trim() : null; //Nome
                    pessoaAg.Apelido = sheet.GetRow(row).GetCell(3) != null && sheet.GetRow(row).GetCell(3).StringCellValue.Trim().ToString() != "" && sheet.GetRow(row).GetCell(3).StringCellValue.Trim().ToString() != string.Empty ? sheet.GetRow(row).GetCell(3).StringCellValue.Trim() : null; //Nome de Guerra
                    if (string.IsNullOrEmpty(pessoaAg.Apelido) && pessoaAg.Nome != null)
                    {
                        var textInfo = new CultureInfo("pt-BR", false).TextInfo;
                        var splitNome = pessoaAg.Nome.Split(' ');
                        pessoaAg.Apelido = pessoaAg.Nome != null ? textInfo.ToTitleCase($"{splitNome[0]} {splitNome[splitNome.Length - 1]}") : null;
                    }
                    pessoaAg.Email = sheet.GetRow(row).GetCell(4) != null && sheet.GetRow(row).GetCell(4).StringCellValue.Trim().ToString() != "" && sheet.GetRow(row).GetCell(4).StringCellValue.Trim().ToString() != string.Empty ? sheet.GetRow(row).GetCell(4).StringCellValue.Trim() : null; //Email
                    textAux = sheet.GetRow(row).GetCell(5) != null && sheet.GetRow(row).GetCell(5).StringCellValue.Trim().ToString() != "" && sheet.GetRow(row).GetCell(5).StringCellValue.Trim().ToString() != string.Empty ? sheet.GetRow(row).GetCell(5).StringCellValue.Trim().ToUpper() : null;
                    if (string.IsNullOrEmpty(textAux)) errosNaLinha.Add("Sexo não informado.");
                    else if (!(textAux == "MASCULINO" || textAux == "FEMININO")) errosNaLinha.Add("Sexo informado inválido.");
                    else pessoaAg.Sexo = textAux.ToUpper() == "MASCULINO" ? Sexo.Masculino : Sexo.Feminino; //Sexo (Masculino ou Feminino)

                    string telEmergencial = sheet.GetRow(row).GetCell(6) != null && sheet.GetRow(row).GetCell(6).StringCellValue.Trim().ToString() != "" && sheet.GetRow(row).GetCell(6).StringCellValue.Trim().ToString() != string.Empty ? sheet.GetRow(row).GetCell(6).StringCellValue.Trim() : null;
                    if (string.IsNullOrWhiteSpace(telEmergencial)) errosNaLinha.Add("Tel. Emergencial não informado.");
                    pessoaAg.TelefoneNumEmergencial = telEmergencial; //Tel. De Emergencia
                    pessoaAg.TelefoneNumCelular = sheet.GetRow(row).GetCell(7) != null && sheet.GetRow(row).GetCell(7).StringCellValue.Trim().ToString() != "" && sheet.GetRow(row).GetCell(7).StringCellValue.Trim().ToString() != string.Empty ? sheet.GetRow(row).GetCell(7).StringCellValue.Trim() : null; //Celular
                                                                                                                                                                                                                                                                                                               //textAux = sheet.GetRow(row).GetCell(15) != null ? sheet.GetRow(row).GetCell(15).StringCellValue.Trim() : null;
                                                                                                                                                                                                                                                                                                               //if (!string.IsNullOrEmpty(textAux))
                                                                                                                                                                                                                                                                                                               //{
                                                                                                                                                                                                                                                                                                               //    numAux = 0;
                                                                                                                                                                                                                                                                                                               //    if (!int.TryParse(textAux, out numAux)) errosNaLinha.Add("RG deve conter somente número.");
                                                                                                                                                                                                                                                                                                               //    else pessoaAg.RG = sheet.GetRow(row).GetCell(15) != null ? sheet.GetRow(row).GetCell(15).StringCellValue.Trim() : null; //RG Numero
                                                                                                                                                                                                                                                                                                               //}
                    pessoaAg.RG = sheet.GetRow(row).GetCell(15) != null && sheet.GetRow(row).GetCell(15).StringCellValue.Trim().ToString() != "" && sheet.GetRow(row).GetCell(15).StringCellValue.Trim().ToString() != string.Empty ? sheet.GetRow(row).GetCell(15).StringCellValue.Trim() : null; //RG Numero
                    pessoaAg.RGOrgaoEmissor = sheet.GetRow(row).GetCell(16) != null && sheet.GetRow(row).GetCell(16).StringCellValue.Trim().ToString() != "" && sheet.GetRow(row).GetCell(16).StringCellValue.Trim().ToString() != string.Empty ? sheet.GetRow(row).GetCell(16).StringCellValue.Trim() : null; //RG Orgao Emissor
                                                                                                                                                                                                                                                                                                               //pessoaAg.RGOrgaoEmissorUF = new Estado() { Sigla = (sheet.GetRow(row).GetCell(17) != null ? sheet.GetRow(row).GetCell(17).StringCellValue.Trim() : null) }; //RG UF (Ex.: ES)
                    textAux = sheet.GetRow(row).GetCell(17) != null ? sheet.GetRow(row).GetCell(17).StringCellValue.Trim() : null;
                    if (!string.IsNullOrEmpty(textAux))
                    {
                        IRepositorioEndereco repEndereco = Fabrica.Instancia.ObterRepositorio<IRepositorioEndereco>(UnidadeTrabalho);
                        Estado estado = repEndereco.ObterEstadosSigla(textAux);
                        if (estado == null) errosNaLinha.Add("Não foi possível identifcar a UF do RG informado.");
                        pessoaAg.RGOrgaoEmissorUF = estado; //RG UF (Ex.: ES)
                    }
                    textAux = sheet.GetRow(row).GetCell(18) != null && sheet.GetRow(row).GetCell(18).StringCellValue.Trim().ToString() != "" && sheet.GetRow(row).GetCell(18).StringCellValue.Trim().ToString() != string.Empty ? sheet.GetRow(row).GetCell(18).StringCellValue.Trim() : null;
                    if (!string.IsNullOrEmpty(textAux))
                    {
                        dateAux = DateTime.MinValue;
                        DateTime.TryParseExact(sheet.GetRow(row).GetCell(18).StringCellValue.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateAux);
                        if (dateAux == DateTime.MinValue) errosNaLinha.Add("Formato da Data de Emissão do RG inválido, requerido: dd/MM/yyyy.");
                        else pessoaAg.RGDataEmissao = dateAux; //RG Data Emissao (dd/mm/aaaa)
                    }

                    var vAgendamento = new VisitanteAgendamento();
                    vAgendamento.PessoaAg = pessoaAg;
                    textAux = sheet.GetRow(row).GetCell(8) != null && sheet.GetRow(row).GetCell(8).StringCellValue.Trim().ToString() != "" && sheet.GetRow(row).GetCell(8).StringCellValue.Trim().ToString() != string.Empty ? sheet.GetRow(row).GetCell(8).StringCellValue.Trim().ToUpper() : null;
                    if (string.IsNullOrEmpty(textAux) || textAux == "NAO" || textAux == "NÃO") vAgendamento.AcessoPorto = false;
                    else if (textAux == "SIM") vAgendamento.AcessoPorto = true;
                    else errosNaLinha.Add($"'{textAux}' não é um registro válido para Acesso ao Porto, informe SIM ou NÃO."); //Acesso ao Porto (Sim ou Não)

                    // CPF DO RESPONSAVEL
                    textAux = sheet.GetRow(row).GetCell(9) != null ? sheet.GetRow(row).GetCell(9).StringCellValue.Trim() : null;
                    string cpfResponsalvel = textAux;
                    if (string.IsNullOrEmpty(cpfResponsalvel))
                        errosNaLinha.Add("CPF do Responsável não informado.");
                    else
                    {
                        cpfResponsalvel = cpfResponsalvel.Replace("-", "").Replace(".", "");
                        Papel papelResponsavel = null;
                        var pf = repPessoaFisica.ObterPorCpfPassaporte(false, cpfResponsalvel);
                        if (pf == null) errosNaLinha.Add("Não foi possível identificar o CPF do Responsável informado.");
                        else
                        {
                            papelResponsavel = pf.ObterPapel(true);
                            if (papelResponsavel == null) errosNaLinha.Add("Responsável informado não possui papel Ativo.");
                            else if (!(papelResponsavel is Colaborador)) errosNaLinha.Add("Responsável informado não é um Colaborador.");
                            else
                            {
                                Colaborador colabResponsavel = (Colaborador)papelResponsavel;
                                vAgendamento.ResponsavelVisita = new Colaborador()
                                {
                                    Codigo = colabResponsavel.Codigo,
                                    PessoaFisica = new PessoaFisica()
                                    {
                                        Codigo = colabResponsavel.PessoaFisica.Codigo,
                                        CPF = cpfResponsalvel
                                    }
                                }; //CPF do Responsavel
                            }
                        }

                    }
                    vAgendamento.Empresa = sheet.GetRow(row).GetCell(10) != null && sheet.GetRow(row).GetCell(10).StringCellValue.Trim().ToString() != "" && sheet.GetRow(row).GetCell(10).StringCellValue.Trim().ToString() != string.Empty ? sheet.GetRow(row).GetCell(10).StringCellValue.Trim() : null; //Empresa
                    vAgendamento.Motivo = sheet.GetRow(row).GetCell(11) != null && sheet.GetRow(row).GetCell(11).StringCellValue.Trim().ToString() != "" && sheet.GetRow(row).GetCell(11).StringCellValue.Trim().ToString() != string.Empty ? sheet.GetRow(row).GetCell(11).StringCellValue.Trim() : null; //Motivo
                    vAgendamento.Funcao = sheet.GetRow(row).GetCell(12) != null && sheet.GetRow(row).GetCell(12).StringCellValue.Trim().ToString() != "" && sheet.GetRow(row).GetCell(12).StringCellValue.Trim().ToString() != string.Empty ? sheet.GetRow(row).GetCell(12).StringCellValue.Trim() : null; //Funcao
                    textAux = sheet.GetRow(row).GetCell(13) != null && sheet.GetRow(row).GetCell(13).StringCellValue.Trim().ToString() != "" && sheet.GetRow(row).GetCell(13).StringCellValue.Trim().ToString() != string.Empty ? sheet.GetRow(row).GetCell(13).StringCellValue.Trim() : null;
                    if (string.IsNullOrEmpty(textAux)) errosNaLinha.Add("Data de Início nula.");
                    else
                    {
                        if (!DateTime.TryParseExact(textAux, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateAux))
                            errosNaLinha.Add("Formato da Data de Início inválido, requerido: dd/MM/yyyy.");
                        else vAgendamento.DataInicio = dateAux; //Data Início (dd/mm/aaaa)
                    }
                    textAux = sheet.GetRow(row).GetCell(14) != null && sheet.GetRow(row).GetCell(14).StringCellValue.Trim().ToString() != "" && sheet.GetRow(row).GetCell(14).StringCellValue.Trim().ToString() != string.Empty ? sheet.GetRow(row).GetCell(14).StringCellValue.Trim() : null;
                    if (string.IsNullOrEmpty(textAux)) errosNaLinha.Add("Data Final nula.");
                    else
                    {
                        if (!DateTime.TryParseExact(textAux, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateAux))
                            errosNaLinha.Add("Formato da Data Fim inválido, requerido: dd/MM/yyyy.");
                        else vAgendamento.DataFim = dateAux; //Data Fim (dd/mm/aaaa)
                    }
                    //vAgendamento.Solicitante = new Usuario() { PessoaFisica = new PessoaFisica() { CPF = Session["cpfLogado"].ToString() } };

                    try
                    {
                        if (errosNaLinha.Any()) throw new Exception(string.Join(" ", errosNaLinha.ToArray()));
                        Incluir(vAgendamento, new string[] { });
                        importouAlgum = true;
                    }
                    catch (Exception e)
                    {
                        for (int i = 0; i <= 18; i++)
                        {
                            if (!linhaVazia)
                                break;
                            try
                            {
                                string hasValue = sheet.GetRow(row).GetCell(8).StringCellValue.Trim();
                                linhaVazia = false;
                            }
                            catch { }
                        }
                        if (!linhaVazia) listaErro.Add($"Linha {row.ToString(maskNum)}: {e.Message}");
                    }
                }

                this.GetCmp<Panel>("panelImportacao").Hidden = true;
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.INFO,
                    Title = "Sucesso",
                    Message = "Ação executada com sucesso."
                });

                if (listaErro.Any() || !importouAlgum)
                {
                    if (!importouAlgum)
                        messagemResult = $"Nenhum registro foi importado{(listaErro.Any() ? ":" : ".")}";
                    else
                        messagemResult = listaErro.Count > 1 ?
                            $"Atenção: {listaErro.Count} registros não foram importados:" :
                            "O seguinte registro não foi importado:";

                    messagemResult += "<BR/>" + string.Join("<BR/>", listaErro.ToArray());
                    this.GetCmp<Panel>("panelImportacao").Html = messagemResult;
                    this.GetCmp<Panel>("panelImportacao").Show();
                }
            }
            catch (CoreException cEx)
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.ERROR,
                    Title = "Erro",
                    Message = cEx.Message
                });
            }
            catch (Exception ex)
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.ERROR,
                    Title = "Erro",
                    Message = "Não foi possível realizar importação." + ex.Message
                });
            }
            finally
            {
                X.Mask.Hide();
            }


            DirectResult result = new DirectResult();
            result.IsUpload = true;
            return result;
        }

    }
}
