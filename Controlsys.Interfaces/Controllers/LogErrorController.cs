﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Controlsys.Dominio.Audit;
using Controlsys.Infra;
using Controlsys.Repositorio.Audit;
using Ext.Net;
using Globalsys;
using Ext.Net.MVC;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar log errors.
    /// </summary>
    [AllowAnonymous]
    [ExtendController]
    public class LogErrorController : Controller, IControllerBase
    {
        /// <summary>
        /// Gets/Sets valor para UnidadeTrabalho.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// GET: /LogError/.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public LogErrorController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        /// <summary>
        /// Retorna a página de logError.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Retorna todos os objetos do tipo LogError.
        /// </summary>
        ///
        /// <returns>
        /// Detalhes do tipo de objeto ligado ao controller no formato JSON.
        /// </returns>
        public ActionResult ObterTodos(StoreRequestParameters parameters)
        {
            var regs = Fabrica.Instancia.ObterRepositorio<IRepositorioLogError>(UnidadeTrabalho).ObterTodos();

            int total = regs.Count();
            regs = regs.OrderByDescending(l => l.DataRegistro);
            var data = montaObj(regs, parameters);

            return new StoreResult(data, total);
        }

        private List<LogError> montaObj(IQueryable<LogError> regs, StoreRequestParameters parameters = null, bool ignoreParameters = false)
        {
            if (!ignoreParameters)
            {
                if (parameters == null)
                    parameters = new StoreRequestParameters();
                regs = regs.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 25);
            }

            return regs.ToList();
        }
    }
}
