﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Mvc;
using Controlsys.Dominio.Acesso;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Acesso;
using Ext.Net;
using Ext.Net.Utilities;
using Globalsys;
using Microsoft.Reporting.WebForms;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class GraficoAcessosDiaController : Controller, IControllerBase
    {
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        public GraficoAcessosDiaController(IUnidadeTrabalho unidade) { UnidadeTrabalho = unidade; }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Exportar(string tipoRelatorio, DateTime dtInicio, DateTime dtFim)
        {
            LocalReport relatorio = new LocalReport();
            relatorio.ReportPath = Server.MapPath("~/Reports/GraficoAcessosDia.rdlc");
            var acessos = FiltrarAcessos(dtInicio, dtFim);
            relatorio.DataSources.Add(new ReportDataSource("DTOGraficoAcessosDia", acessos));
            relatorio.SetParameters(new ReportParameter("DataInicio", dtInicio.ToString()));
            relatorio.SetParameters(new ReportParameter("DataFinal", dtFim.ToString()));

            string mimeType;
            string fileName = string.Empty;
            switch (tipoRelatorio)
            {
                case "PDF":
                    mimeType = "application/pdf";
                    fileName = "download.pdf";
                    break;
                case "Excel":
                    mimeType = "application/vnd.ms-excel";
                    fileName = "download.xls";
                    break;
                case "Word":
                    mimeType = "application/msword";
                    fileName = "download.doc";
                    break;
            }

            string encoding;
            string fileNameExtension;

            Warning[] warnings;
            string[] streams;
            byte[] bytes;

            bytes = relatorio.Render(
            tipoRelatorio,
            null,
            out mimeType,
            out encoding,
            out fileNameExtension,
            out streams,
            out warnings);

            return File(bytes, mimeType, fileName);
        }

        private List<GraficoAcessosDiaModelView> FiltrarAcessos(DateTime dtInicio, DateTime dtFim)
        {
            IRepositorioControleAcesso rep = Fabrica.Instancia.ObterRepositorio<IRepositorioControleAcesso>(UnidadeTrabalho);

            dtFim = dtFim.AddDays(1);
            var listaCA = rep.ObterListaAcesso(null, null, null, null, null, null, dtInicio, dtFim);
            //listaCA = listaCA.Where(ca => ca.StatusAcesso == StatusAcesso.OCO_UR_ENTRADA_COMPLETA ||
            //                          ca.StatusAcesso == StatusAcesso.OCO_UR_SAIDA_COMPLETA);

            var dias = listaCA.Select(ca => ca.DataAcesso.Date);
            var listaDias = dias.Distinct().ToList();
            //dias = dias.OrderBy(d => d);

            //var qtdDias = from ca in listaCA
            //    where ca.DataAcesso >= dia && ca.DataAcesso < dia.AddDays(1)
            //    select ca.Codigo;

            var lista = new List<GraficoAcessosDiaModelView>();
            foreach (var dia in listaDias)
            {
                DateTime dtAux = dia.AddDays(1);
                var listaCAaux = listaCA.Where(ca => ca.DataAcesso >= dia && ca.DataAcesso < dtAux);
                var entradas = listaCAaux.Count(ca => ca.Direcao == DirecaoLeitora.Entrada);
                var saidas = listaCAaux.Count(ca => ca.Direcao == DirecaoLeitora.Saida);
                lista.Add(new GraficoAcessosDiaModelView()
                {
                    Data = dia,
                    Entradas = entradas,
                    Saidas = saidas
                });
            }

            return new List<GraficoAcessosDiaModelView>(lista.OrderBy(l=> l.Data));
        }

    }
}
