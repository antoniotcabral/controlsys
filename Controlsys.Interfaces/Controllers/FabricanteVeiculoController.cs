﻿using System.Diagnostics.CodeAnalysis;
using System.Security.Cryptography;
using Controlsys.Dominio.Parametros;
using Controlsys.Infra;
using Controlsys.Repositorio.Parametros;
using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Globalsys.Validacao;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class FabricanteVeiculoController : Controller, IControllerBase
    {

        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        public FabricanteVeiculoController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        [ExcludeFromCodeCoverage]
        public ActionResult Index()
        {
            return View();
        }

        [Transaction]
        public ActionResult Incluir(FabricanteVeiculo fabricanteVeiculo)
        {
            IRepositorioFabricanteVeiculo repFabricanteVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioFabricanteVeiculo>(UnidadeTrabalho);
            fabricanteVeiculo.DataRegistro = DateTime.Now;
            fabricanteVeiculo.Ativar();
            repFabricanteVeiculo.Validar(fabricanteVeiculo, EstadoObjeto.Novo);
            repFabricanteVeiculo.Salvar(fabricanteVeiculo);
            return Json(fabricanteVeiculo);
        }

        [Transaction]
        public ActionResult Alterar(FabricanteVeiculo fabricanteVeiculo)
        {
            IRepositorioFabricanteVeiculo repFabricanteVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioFabricanteVeiculo>(UnidadeTrabalho);
            FabricanteVeiculo fabricanteVeiculoA = repFabricanteVeiculo.ObterPorId(fabricanteVeiculo.Codigo);
            fabricanteVeiculoA.Nome = fabricanteVeiculo.Nome;
            repFabricanteVeiculo.Validar(fabricanteVeiculoA, EstadoObjeto.Alterado);
            repFabricanteVeiculo.Atualizar(fabricanteVeiculoA);
            return Json(fabricanteVeiculoA);
        }

        [Transaction]
        public ActionResult AtivarInativar(int codigo)
        {
            IRepositorioFabricanteVeiculo repFabricanteVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioFabricanteVeiculo>(UnidadeTrabalho);
            FabricanteVeiculo fabricanteVeiculo = repFabricanteVeiculo.ObterPorId(codigo);
            if (fabricanteVeiculo.Ativo)
            {
                repFabricanteVeiculo.Validar(fabricanteVeiculo, EstadoObjeto.Inativado);
                fabricanteVeiculo.Inativar();
            }
            else
            {
                repFabricanteVeiculo.Validar(fabricanteVeiculo, EstadoObjeto.Ativado);
                fabricanteVeiculo.Ativar();
            }
            repFabricanteVeiculo.Atualizar(fabricanteVeiculo);
            return Json(fabricanteVeiculo);
        }

        public ActionResult Selecionar(int codigo)
        {
            IRepositorioFabricanteVeiculo repFabricanteVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioFabricanteVeiculo>(UnidadeTrabalho);
            FabricanteVeiculo fabricanteVeiculo = repFabricanteVeiculo.ObterPorId(codigo);
            return Json(fabricanteVeiculo);
        }

        public ActionResult ObterTodos(bool apenasAtivos = false, string descricao = null)
        {
            IRepositorioFabricanteVeiculo repFabricanteVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioFabricanteVeiculo>(UnidadeTrabalho);

            IQueryable<FabricanteVeiculo> lista = repFabricanteVeiculo.ObterTodos();

            if (apenasAtivos)
                lista = lista.Where(t => t.Ativo == true);

            if (!string.IsNullOrEmpty(descricao))
                lista = lista.Where(x => x.Nome.ToUpper().Contains(descricao));

            return Json(lista.ToList().OrderBy(a => a.Nome));
        }

    }
}
