﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Controlsys.Dominio.Pessoas;
using Controlsys.Dominio.Seguranca;
using Controlsys.Infra;
using Controlsys.Repositorios.Seguranca;
using Globalsys;
using Globalsys.Exceptions;
using iTextSharp.tool.xml.html;
using System.Security.Cryptography.X509Certificates;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar logins.
    /// </summary>
    [AllowAnonymous]
    [ExtendController]
    public class LoginController : Controller, IControllerBase
    {
        /// <summary>
        /// Gets/Sets valor para UnidadeTrabalho.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// GET: /Login/.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public LoginController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        /// <summary>
        /// Retorna a página de login.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index()
        {
            ////string chavePublica = Guid.NewGuid().ToString();
            //System.Security.Cryptography.TripleDESCryptoServiceProvider TDES = new System.Security.Cryptography.TripleDESCryptoServiceProvider();
            //TDES.GenerateIV();
            //TDES.GenerateKey();
            //
            //// Generate a public/private key pair.
            //var RSA = new System.Security.Cryptography.RSACryptoServiceProvider();
            ////Save the public key information to an RSAParameters structure.  
            //System.Security.Cryptography.RSAParameters RSAKeyInfo = RSA.ExportParameters(false);
            //
            ////RSA.Encrypt
            //
            //var x = System.Text.Encoding.UTF8.GetBytes("aasdas");


            //var base64 = Convert.FromBase64String("SGVsbG8gV29ybGQh");
            //var result1 = System.Text.ASCIIEncoding.ASCII.GetString(base64);
            //var result2 = System.Text.ASCIIEncoding.UTF8.GetString(base64);
            //
            //int unicode = 65;
            //char character = (char)unicode;
            //string text = character.ToString();

            string chavePublica = Guid.NewGuid().ToString("N");
            Session["pKey"] = chavePublica;
            return View("Index", model: chavePublica);
        }

        /// <summary>
        /// Login.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="login">
        /// O(a) login.
        /// </param>
        /// <param name="senha">
        /// O(a) senha.
        /// </param>
        /// <param name="ad">
        /// true to ad.
        /// </param>
        /// <param name="resultado">
        /// O(a) resultado.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Login(string login, string senha, bool ad, int resultado)
        {
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);

            if (Session["Captcha"] == null)
                throw new CoreException("O tempo para o login expirou. Recarregue a página e tente novamente.");

            if (resultado != Convert.ToInt32(Session["Captcha"]))
                throw new CoreException("Não é possível fazer login. O resultado da soma não confere com o digitado.");

            Usuario usuario;
            try
            {

                string chavePublica = Session["pKey"] as string;
                senha = Globalsys.Util.Tools.decryptStringWithXORFromHex(senha, chavePublica);

                if (ad)
                {
                    Globalsys.Util.Tools.ValidarLogin(login, senha);
                    senha = null;
                }
                else
                    senha = Globalsys.Util.Tools.CriptografarMD5(senha);

                //usuario = repUsuario.ObterPorLogin(login, senha, true);
                usuario = repUsuario.ObterPorLogin(login, senha);

                if (usuario == null)
                    throw new CoreException("Login ou senha inválido.");
                else
                    if (!usuario.Ativo)
                    throw new CoreException("Usuário não está ativo.");

                Session["cpfLogado"] = usuario.PessoaFisica.CPF;
                Session["login"] = null;
                Session["tentativa"] = null;

            }
            catch (CoreException ex)
            {
                if (ex.Source == StatusPapelLog.Inativacao.ObterDescricaoEnum()) throw new CoreException(ex.Message);
                usuario = repUsuario.ObterPorLogin(login);
                var loginAnterior = (string)Session["login"];
                if (usuario == null || (usuario.Login != loginAnterior))
                {
                    Session["login"] = login;
                    Session["tentativa"] = 1;
                }
                else
                {
                    int tentativa = (int)Session["tentativa"];
                    tentativa++;
                    Session["tentativa"] = tentativa;
                    if (tentativa >= 5)
                    {
                        UnidadeTrabalho.BeginTransaction();
                        usuario.Inativar();
                        repUsuario.Salvar(usuario);
                        UnidadeTrabalho.Commit();
                        throw new CoreException("Limite de 5 tentativas Excedido - Usuário bloqueado.");
                    }
                }
                throw;
            }

            FormsAuthentication.SetAuthCookie(login, false);

            return Json(null);
        }

        /// <summary>
        /// Logout.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();

            return Json(null);
        }
    }
}
