﻿using Controlsys.Dominio.Parametros;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorios.Parametros;
using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Controlsys.Repositorios.Acesso;
using Controlsys.Dominio.Acesso;
using Globalsys.Exceptions; 

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar parametroes.
    /// </summary>
    [ExtendController]
    public class ParametroController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /Parametro/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.ParametroController.
        /// </summary>
        ///
        /// <param name="unidade">
        /// O(a) unidade.
        /// </param>
        public ParametroController(IUnidadeTrabalho unidade) { UnidadeTrabalho = unidade; }

        /// <summary>
        /// Retorna a página de parametro.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Altera um registro de parametro.
        /// </summary>
        ///
        /// <param name="parametro">
        /// O(a) parametro.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Alterar(Parametro parametro)
        {
            IRepositorioParametro repParametro = Fabrica.Instancia.ObterRepositorio<IRepositorioParametro>(UnidadeTrabalho);

            Parametro parametroA = repParametro.ObterPorId(parametro.Codigo);
            parametroA.Valor = parametro.Valor;
            repParametro.Validar(parametroA);

            if (parametroA.Nome == ParametroSistema.DataExecuçãoRelatorioFTE)
            {
                this.AplicarRegraParametroDataExecuçãoRelatorioFTE(parametroA);
                return Json(montaObj(parametroA));
            }
            else if (parametroA.Nome == ParametroSistema.DataAgendamentoRelatorioHHT)
            {
                this.AplicarRegraParametroDataAgendamentoHTT(parametroA);
                return Json(montaObj(parametroA));
            }

            if (parametroA.Nome != ParametroSistema.CatracasAtualizadas || parametroA.Valor != "Não")
                return Json(montaObj(parametroA));

            // Busca todas as controladoras ativas e online
            IRepositorioControladora repControladora = Fabrica.Instancia.ObterRepositorio<IRepositorioControladora>(UnidadeTrabalho);
            IRepositorioSincronizaPessoaAcesso repSinc = Fabrica.Instancia.ObterRepositorio<IRepositorioSincronizaPessoaAcesso>(UnidadeTrabalho);
            List<Controladora> controladoras = repControladora.ObterTodos().Where(c => c.Ativo).ToList();
            repSinc.Sincronizar(null, TipoSincronizacaoPessoa.Conceder, null, controladoras);

            return Json(montaObj(parametroA));
        }

        private void AplicarRegraParametroDataExecuçãoRelatorioFTE(Parametro parametroA)
        {
            if (DateTime.Parse(parametroA.Valor) >= (new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 00, 00, 00)))
                throw new CoreException("A data deve ser menor que a data atual.");

            RelatorioFullTimeEquivalenteController relatorio = new RelatorioFullTimeEquivalenteController(UnidadeTrabalho);
            relatorio.ExecutarCarga(Convert.ToDateTime(parametroA.Valor));
        }

        private void AplicarRegraParametroDataAgendamentoHTT(Parametro parametroA)
        {
            var spParams = new Dictionary<string, object>();
            spParams.Add("DATA_REF", DateTime.Parse(parametroA.Valor).AddMonths(1)); // É adicionado um mês, pois dentro da procedure é feito o tratamento de M menos 1, isso por conta do job que roda mensalmente.
            UnidadeTrabalho.ExecuteProcedure("SP_EXPORT_REL_VISUAL_FLASH :DATA_REF", spParams);
        }

        /// <summary>
        /// Cria um objeto contendo informações relacionadas ao conteudo gerenciado por
        /// ParametroController.
        /// </summary>
        ///
        /// <param name="parametro">
        /// O(a) parametro.
        /// </param>
        ///
        /// <returns>
        /// Um(a) object.
        /// </returns>
        private object montaObj(Parametro parametro)
        {
            return new ParametroModelView()
            {
                Codigo = parametro.Codigo,
                Nome = obterDesc(parametro.Nome),
                Valor = parametro.Valor,
                Tipo = obterDesc(parametro.TipoParametro),
                Url = parametro.Url
            };
        }

        /// <summary>
        /// Retorna todos os objetos do tipo Parametro.
        /// </summary>
        ///
        /// <returns>
        /// Detalhes do tipo de objeto ligado ao controller no formato JSON.
        /// </returns>
        public ActionResult ObterTodos()
        {
            IRepositorioParametro repParametro = Fabrica.Instancia.ObterRepositorio<IRepositorioParametro>(UnidadeTrabalho);
            var lista = repParametro.ObterTodos()
                                    .Where(x => x.ExecutaProcedimento == false)
                                    .Select(s => new
                                    {
                                        Codigo = s.Codigo,
                                        Nome = obterDesc(s.Nome),
                                        Valor = s.Valor,
                                        Tipo = obterDesc(s.TipoParametro),
                                        Url = s.Url
                                    })
                                    .ToList();

            return Json(lista.OrderBy(r => r.Nome).ToList());
        }

        /// <summary>
        /// Obter description.
        /// </summary>
        ///
        /// <param name="parametro">
        /// O(a) parametro.
        /// </param>
        ///
        /// <returns>
        /// Um(a) string.
        /// </returns>
        private string obterDesc(ParametroSistema parametro)
        {
            return EnumExtensoes.ObterDescricaoEnum(parametro);
        }

        /// <summary>
        /// Obter description.
        /// </summary>
        ///
        /// <param name="tipoParametro">
        /// O(a) tipo parametro.
        /// </param>
        ///
        /// <returns>
        /// Um(a) string.
        /// </returns>
        private string obterDesc(TipoParametro tipoParametro)
        {
            return EnumExtensoes.ObterDescricaoEnum(tipoParametro);
        }

    }
}
