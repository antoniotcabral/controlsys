﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Globalsys;
using Controlsys.Dominio.Parametros;
using Controlsys.Repositorio.Empresas;
using Controlsys.Infra;
using NCalc;
using Globalsys.Exceptions;
using System.Text.RegularExpressions;
using Controlsys.Repositorios.Parametros;
using Ext.Net;
using NCalc.Domain;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class ClassificacaoJuridicaController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /Parametro/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        public ClassificacaoJuridicaController(IUnidadeTrabalho unidade) { UnidadeTrabalho = unidade; }

        public ActionResult Index()
        {
            IRepositorioParametro repParam = Fabrica.Instancia.ObterRepositorio<IRepositorioParametro>(UnidadeTrabalho);
            List<Parametro> listaParam = repParam.ObterTodos().ToList();
            if (listaParam.Any())
            {
                Ext.Net.X.GetCmp<TextField>("txtRuimMin").SetValue(
                    listaParam.Where(p => p.Nome == ParametroSistema.ClassificacaoJuridicaRuimMinimo).Select(p2 => p2.Valor).FirstOrDefault());
                Ext.Net.X.GetCmp<TextField>("txtRuimMax").SetValue(
                    listaParam.Where(p => p.Nome == ParametroSistema.ClassificacaoJuridicaRuimMaximo).Select(p2 => p2.Valor).FirstOrDefault());

                Ext.Net.X.GetCmp<TextField>("txtMedioMin").SetValue(
                    listaParam.Where(p => p.Nome == ParametroSistema.ClassificacaoJuridicaMedioMinimo).Select(p2 => p2.Valor).FirstOrDefault());
                Ext.Net.X.GetCmp<TextField>("txtMedioMax").SetValue(
                    listaParam.Where(p => p.Nome == ParametroSistema.ClassificacaoJuridicaMedioMaximo).Select(p2 => p2.Valor).FirstOrDefault());

                Ext.Net.X.GetCmp<TextField>("txtBomMin").SetValue(
                    listaParam.Where(p => p.Nome == ParametroSistema.ClassificacaoJuridicaBomMinimo).Select(p2 => p2.Valor).FirstOrDefault());
                Ext.Net.X.GetCmp<TextField>("txtBomMax").SetValue(
                    listaParam.Where(p => p.Nome == ParametroSistema.ClassificacaoJuridicaBomMaximo).Select(p2 => p2.Valor).FirstOrDefault());
            }

            return View();
        }

        [Transaction]
        public ActionResult Alterar(ClassificacaoJuridica classJuridicaA)
        {
            IRepositorioClassificacaoJuridica repClassificacaoJuridica = Fabrica.Instancia.ObterRepositorio<IRepositorioClassificacaoJuridica>(UnidadeTrabalho);

            if (!string.IsNullOrEmpty(classJuridicaA.Formula))
                classJuridicaA.Formula = classJuridicaA.Formula.ToUpper().Replace("IF", "if");

            ClassificacaoJuridica classJuridicaB = repClassificacaoJuridica.ObterPorId(classJuridicaA.Codigo);
            classJuridicaB.Letra = classJuridicaA.Letra.ToUpper();
            classJuridicaB.Formula = classJuridicaA.Formula;
            classJuridicaB.Peso = classJuridicaA.Peso;
            classJuridicaB.Pontua = classJuridicaA.Pontua;

            var letrasFormula = validarFormula(classJuridicaB.Formula);
            repClassificacaoJuridica.Validar(classJuridicaB, letrasFormula);

            return Json(montaObj(classJuridicaB));
        }

        [Transaction]
        public ActionResult AtivarInativar(int codigo)
        {
            IRepositorioClassificacaoJuridica repClassJurid = Fabrica.Instancia.ObterRepositorio<IRepositorioClassificacaoJuridica>(UnidadeTrabalho);

            ClassificacaoJuridica entidade = repClassJurid.ObterPorId(codigo);
            if (entidade.Ativo)
            {
                entidade.Inativar();
                var letrasFormula = validarFormula(entidade.Formula);
                repClassJurid.Validar(entidade, letrasFormula);
            }
            else
            {
                entidade.Ativar();
                var letrasFormula = validarFormula(entidade.Formula);
                repClassJurid.Validar(entidade, letrasFormula);
            }

            return Json(montaObj(entidade));
        }

        public ActionResult ObterTodos()
        {
            IRepositorioClassificacaoJuridica repClassificacaoJuridica = Fabrica.Instancia.ObterRepositorio<IRepositorioClassificacaoJuridica>(UnidadeTrabalho);
            return Json(repClassificacaoJuridica.ObterTodos().Select(c => montaObj(c)).ToList());
        }

        private object montaObj(ClassificacaoJuridica entidade)
        {
            return new
            {
                Codigo = entidade.Codigo,
                ItemClassificacao = obterDesc(entidade.ItemClassificacao),
                Letra = entidade.Letra,
                Formula = entidade.Formula,
                Peso = entidade.Peso,
                Pontua = entidade.Pontua,
                Ativo = entidade.Ativo
            };
        }

        private string[] validarFormula(string formula)
        {
            if (string.IsNullOrEmpty(formula))
                return new string[] { };

            formula = formula.ToUpper();
            Expression expression = new Expression(formula);
            if (expression.HasErrors())
                throw new CoreException("A expressão da fórmula está inválida.");

            var expressionAux = NCalc.Expression.Compile(formula, false);

            ParameterExtractionVisitor visitor = new ParameterExtractionVisitor();
            expressionAux.Accept(visitor);

            var extractedParameters = visitor.Parameters;
            if (!extractedParameters.Any())
                return new string[] { };

            List<string> letras = new List<string>();
            foreach (var param in extractedParameters)
                letras.Add(param);

            return letras.ToArray();
        }

        private string obterDesc(ItemClassificacaoJuridica item)
        {
            return EnumExtensoes.ObterDescricaoEnum(item);
        }
    }

}
