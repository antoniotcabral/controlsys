﻿using Controlsys.Dominio.Parametros;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorios.Parametros;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar hora turnoes.
    /// </summary>
    [ExtendController]
    public class HoraTurnoController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /HoraTurno/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.HoraTurnoController.
        /// </summary>
        ///
        /// <param name="unidade">
        /// O(a) unidade.
        /// </param>
        public HoraTurnoController(IUnidadeTrabalho unidade) { UnidadeTrabalho = unidade; }


        /// <summary>
        /// Ativar inativar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult AtivarInativar(int codigo)
        {
            IRepositorioHoraTurno repHoraTurno = Fabrica.Instancia.ObterRepositorio<IRepositorioHoraTurno>(UnidadeTrabalho);
            HoraTurno horaTurno = repHoraTurno.ObterPorId(codigo);

            if (horaTurno.Ativo)
            {
                horaTurno.Inativar();
            }
            else
            {
                horaTurno.Ativar();
            }

            return Json(montaObj(horaTurno));
        }

        /// <summary>
        /// Cria um objeto contendo informações relacionadas ao conteudo gerenciado por
        /// HoraTurnoController.
        /// </summary>
        ///
        /// <param name="horaTurno">
        /// O(a) hora turno.
        /// </param>
        ///
        /// <returns>
        /// Um(a) object.
        /// </returns>
        private object montaObj(HoraTurno horaTurno)
        {
            return new HoraTurnoModelView()
            {
                    Codigo = horaTurno.Codigo,
                    //Turno = horaTurno.Turno,
                    HoraInicio = horaTurno.HoraInicio,
                    HoraFim = horaTurno.HoraFim,
                    Ordem = horaTurno.Ordem,
                    DiaSemana = horaTurno.DiaSemana.HasValue ? obterDesc(horaTurno.DiaSemana.Value) : null,
                    Trabalha = horaTurno.Trabalha,
                    Maior24h = horaTurno.Maior24h,
                    Ativo = horaTurno.Ativo
            };
        }

        /// <summary>
        /// Obter description.
        /// </summary>
        ///
        /// <param name="diaSemana">
        /// O(a) dia semana.
        /// </param>
        ///
        /// <returns>
        /// Um(a) string.
        /// </returns>
        private string obterDesc(DiaSemana diaSemana)
        {
            return EnumExtensoes.ObterDescricaoEnum(diaSemana);
        }

    }
}