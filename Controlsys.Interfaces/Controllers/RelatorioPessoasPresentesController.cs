﻿using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Pessoas;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Acesso;
using Controlsys.Repositorios.Acesso;
using Ext.Net;
using Ext.Net.MVC;
using Globalsys;
using Globalsys.Relatorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class RelatorioPessoasPresentesController : Controller, IControllerBase
    {
        //
        // GET: /RelatorioPessoasPresentes/

         public IUnidadeTrabalho UnidadeTrabalho { get; set; }

         public RelatorioPessoasPresentesController(IUnidadeTrabalho unidade) { UnidadeTrabalho = unidade; }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Pesquisar(StoreRequestParameters parameters, DateTime data, TimeSpan horaInicial, int? grupoLeitora = 0, string[] localLeitora = null, string empresa = null, string cpf = null, string nome = null, string tipoPapel = null)
        {
            IQueryable<ControleAcesso> regs = FiltrarRegistros(data, horaInicial, grupoLeitora, localLeitora, empresa, cpf, nome, tipoPapel);

            int total = regs.Count();
            List<RelatorioPessoasPresentesModelView> datas = montaObj(regs, parameters);

            return new StoreResult(datas, total);
        }


        public ActionResult Exportar(List<string> selectedFields, string tipoRelatorio, DateTime data, TimeSpan horaInicial, int? grupoLeitora = 0, string[] localLeitora = null, string empresa = null, string cpf = null, string nome = null, string tipoPapel = null)
        {
            var list = FiltrarRegistros(data, horaInicial, grupoLeitora, localLeitora, empresa, cpf, nome, tipoPapel);
            var visitantes = montaObj(list, null, true);

            ReportConfig reportConfig = ReportHelper.RenderReport<RelatorioPessoasPresentesModelView>(selectedFields, tipoRelatorio, visitantes);

            string fileName = string.Empty;

            switch (tipoRelatorio)
            {
                case "PDF":
                    fileName = "download.pdf";
                    break;
                case "EXCELOPENXML":
                    fileName = "download.xlsx";
                    break;
                case "WORDOPENXML":
                    fileName = "download.docx";
                    break;
            }

            return File(reportConfig.Bytes, reportConfig.MimeType, fileName);
        }


        private IQueryable<ControleAcesso> FiltrarRegistros(DateTime data, TimeSpan horaInicial, int? grupoLeitora = 0, string[] localLeitora = null, string empresa = null, string cpf = null, string nome = null, string tipoPapel = null)
        {
            IRepositorioControleAcesso repControleAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioControleAcesso>(UnidadeTrabalho);
            IRepositorioGrupoLeitora repGrupoLeitora = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoLeitora>(UnidadeTrabalho);

            DateTime dataFinal = data.Add(horaInicial);
            DateTime dataInicial = dataFinal.AddHours(-13);

            IQueryable<ControleAcesso> registros = repControleAcesso.ObterTodos();

            if (!string.IsNullOrEmpty(tipoPapel))
            {
                if (tipoPapel == "Colaborador")
                   registros = registros.Where(g => g.Papel is Colaborador);

                if (tipoPapel == "Prestador Serviço")
                   registros = registros.Where(g => g.Papel is PrestadorServico);

                if (tipoPapel == "Visitante")
                    registros = registros.Where(g => g.Papel is Visitante);
            }

            if (!string.IsNullOrEmpty(cpf))
            {
                cpf = cpf.Replace(".", "").Replace("-", "");
                registros = registros.Where(g => g.Papel.PessoaFisica.CPF.ToUpper() == cpf);
            }

            if (!string.IsNullOrEmpty(nome))
            {
                nome = nome.ToUpper();
                registros = registros.Where(g => g.Papel.PessoaFisica.Nome.ToUpper().Contains(nome));
            }

            List<int> ultAcessoPapel = registros.Where(ca => ca.DataAcesso >= dataInicial
                                                       && ca.DataAcesso <= dataFinal
                                                       && ca.Papel != null)
                                                   .GroupBy(ca => ca.Papel.Codigo)
                                                   .Select(gca => gca.Select(a => a.Codigo).Max()).ToList();

            int qtdcolab = ultAcessoPapel.Count;

            List<ControleAcesso> listaControleAcesso = new List<ControleAcesso>();

            for (int i = 0; i < qtdcolab; i += 2000)
            {
                var page = ultAcessoPapel.Skip(i).Take(2000).ToArray();
                listaControleAcesso.AddRange(registros.Where(c => page.Contains(c.Codigo)));
            }

            IQueryable<ControleAcesso> auxListControleAcesso = listaControleAcesso.AsQueryable<ControleAcesso>();

            auxListControleAcesso = auxListControleAcesso.Where(a => a.StatusAcesso == StatusAcesso.OCO_UR_ACESSO_LIBERADO || a.StatusAcesso == StatusAcesso.OCO_UR_ENTRADA_COMPLETA);

            if (grupoLeitora == 1)// sim
            {
                var cont = repGrupoLeitora.ObterTodos().Where(t => t.Externa);
                auxListControleAcesso = auxListControleAcesso.Where(r => cont.Any(g => g.Leitoras.Select(gl => gl.Codigo).Contains(r.Leitora.Codigo)));
            }

            if (grupoLeitora == 2)
            {
                var cont = repGrupoLeitora.ObterTodos().Where(t => !t.Externa);
                auxListControleAcesso = auxListControleAcesso.Where(r => cont.Any(g => g.Leitoras.Select(gl => gl.Codigo).Contains(r.Leitora.Codigo)));
            }

            if (localLeitora != null)
            {
                auxListControleAcesso = auxListControleAcesso.Where(t => localLeitora.Contains(t.Leitora.Local));
            }

            var papeis = auxListControleAcesso.Select(t => t.Papel);

            if (!string.IsNullOrEmpty(empresa))
            {
                var auxCodPapeis = new List<int>();
                empresa = empresa.ToUpper();
                foreach (var item in papeis)
                {

                    if (item is Colaborador)
                    {
                        if ((item as Colaborador).Empresa.Apelido.ToUpper().Contains(empresa))
                        {
                            auxCodPapeis.Add(item.Codigo);
                        }
                    }
                    else if (item is PrestadorServico)
                    {
                        if ((item as PrestadorServico).TipoPrestador == TipoPrestador.Temporario && (item as PrestadorServico).Empresa.Apelido.ToUpper().Contains(empresa))
                        {
                            auxCodPapeis.Add(item.Codigo);
                        }

                        if ((item as PrestadorServico).TipoPrestador == TipoPrestador.Emergencial && (item as PrestadorServico).Fornecedor.NomeFantasia.ToUpper().Contains(empresa))
                        {
                            auxCodPapeis.Add(item.Codigo);
                        }
                    }
                    else if (item is Visitante)
                    {
                        if ((item as Visitante).Empresa.ToUpper().Contains(empresa))
                        {
                            auxCodPapeis.Add(item.Codigo);
                        }
                    }
                }

                auxListControleAcesso = auxListControleAcesso.Where(t => auxCodPapeis.Contains(t.Papel.Codigo));
            }


            return auxListControleAcesso;
        }


        private List<RelatorioPessoasPresentesModelView> montaObj(IQueryable<ControleAcesso> regs, StoreRequestParameters parameters = null, bool ignoreParameters = false)
        {
            if (!ignoreParameters)
            {
                if (parameters == null)
                    parameters = new StoreRequestParameters();
                regs = regs.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 25);
            }

            List<RelatorioPessoasPresentesModelView> listViPresente = new List<RelatorioPessoasPresentesModelView>();
            IRepositorioGrupoLeitora repGrupoLeitora = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoLeitora>(UnidadeTrabalho);
            var grLeitora = repGrupoLeitora.ObterTodos();

            foreach (var item in regs.ToList())
            {
                RelatorioPessoasPresentesModelView relatorio = new RelatorioPessoasPresentesModelView();

                
                relatorio.Codigo = item.Papel.Codigo;
                relatorio.Nome = item.Papel.PessoaFisica.Nome;
                relatorio.CPF = item.Papel.PessoaFisica.CPF != null ? item.Papel.PessoaFisica.CPFFormatado() : null;
                relatorio.Passaporte = item.Papel.PessoaFisica.Passaporte;
                relatorio.TipoPapel = (item.Papel is Colaborador) ? "Colaborador" : (item.Papel is PrestadorServico) ? "Prestador Serviço" : "Visitante";
                relatorio.Empresa = (item.Papel is Colaborador) ? (((item.Papel as Colaborador).Empresa) != null ? (item.Papel as Colaborador).Empresa.Apelido : "") :
                                    (item.Papel is PrestadorServico) ? ((item.Papel as PrestadorServico).TipoPrestador == TipoPrestador.Temporario ? (item.Papel as PrestadorServico).Empresa.Apelido : (item.Papel as PrestadorServico).Fornecedor.NomeFantasia)
                                    : (item.Papel as Visitante).Empresa;
                
                relatorio.Local = item.Leitora.Local;

                var x = grLeitora.Where(gl => gl.Leitoras.Any(l => l.Leitora.Codigo == item.Leitora.Codigo)).ToList();
                var s = x.Select(j => j.Externa).FirstOrDefault();
                relatorio.GrupoLeitoraExterna = grLeitora.Where(gl => gl.Leitoras.Any(l => l.Leitora.Codigo == item.Leitora.Codigo)) != null ? (grLeitora.Where(gl => gl.Leitoras.Any(l => l.Leitora.Codigo == item.Leitora.Codigo && gl.Externa == true)).Select(j => j.Externa).FirstOrDefault() == true ? "Sim" : "Não") : "S/ Grupo Leitora";

                //relatorio.GrupoLeitoraExterna = grLeitora.Any(g => g.Leitoras.Select(gl => gl.Codigo).Contains(item.Leitora.Codigo)) ? (grLeitora.Any(g => g.Leitoras.Select(gl => gl.Codigo).Contains(item.Leitora.Codigo) && g.Externa) ? "Sim" : "Não") : "S/ Grupo Leitora";
                relatorio.DataAcesso = item.DataAcesso;
                relatorio.HoraAcesso = item.DataAcesso;

                listViPresente.Add(relatorio);
            };

            return listViPresente;
        }
    }
}


