﻿using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.CredenciamentoVeiculos;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Acesso;
using Controlsys.Repositorio.Parametros;
using Controlsys.Repositorios.Acesso;
using Ext.Net;
using Ext.Net.MVC;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar Grupo Acesso Temporario.
    /// </summary>
    [ExtendController]
    public class AcessoTemporarioVeiculoController : Controller, IControllerBase
    {

        #region Properiedades
        /// <summary>
        /// GET: /GrupoAcesso/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        #endregion

        #region Construtor
        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.GrupoAcessoController.
        /// </summary>
        ///
        /// <param name="unidade">
        /// O(a) unidade.
        /// </param>
        public AcessoTemporarioVeiculoController(IUnidadeTrabalho unidade)
        {
            UnidadeTrabalho = unidade;
        }

        #endregion

        #region Métodos Públicos
        public ActionResult Index()
        {
            return View();
        }

        [Transaction]
        public ActionResult Incluir(int codigoVeiculoCredencialVeiculo, int codigoGrupoLeitora, DateTime dtValidadeInicio, DateTime hrValidadeInicio, DateTime dtValidadeFim, DateTime hrValidadeFim)
        {

            IRepositorioAcessoTemporarioVeiculo repAcessoTemp = Fabrica.Instancia.ObterRepositorio<IRepositorioAcessoTemporarioVeiculo>(UnidadeTrabalho);
            IRepositorioGrupoLeitora repGrupoLeitora = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoLeitora>(UnidadeTrabalho);
            IRepositorioVeiculoCredencialVeiculo repositorioVeiculoCredencialVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioVeiculoCredencialVeiculo>(UnidadeTrabalho);

            VeiculoCredencialVeiculo veiculoCredencialVeiculo = repositorioVeiculoCredencialVeiculo.ObterPorId(codigoVeiculoCredencialVeiculo);

            GrupoLeitora grupoLeitora = repGrupoLeitora.ObterPorId(codigoGrupoLeitora);

            AcessoTemporarioVeiculo _acessoTemp = new AcessoTemporarioVeiculo();

            _acessoTemp.DataHoraValidadeFim = new DateTime(dtValidadeFim.Year, dtValidadeFim.Month, dtValidadeFim.Day, hrValidadeFim.Hour, hrValidadeFim.Minute, hrValidadeFim.Second);
            _acessoTemp.DataHoraValidadeInicio = new DateTime(dtValidadeInicio.Year, dtValidadeInicio.Month, dtValidadeInicio.Day, hrValidadeInicio.Hour, hrValidadeInicio.Minute, hrValidadeInicio.Second);
            _acessoTemp.DataRegistro = DateTime.Now;
            _acessoTemp.GrupoLeitora = grupoLeitora;
            _acessoTemp.VeiculoCredencialVeiculo = veiculoCredencialVeiculo;
            _acessoTemp.Ativar();

            repAcessoTemp.Validar(_acessoTemp, EstadoObjeto.Novo);
            repAcessoTemp.Salvar(_acessoTemp);

            IRepositorioSincronizaPessoaAcesso repsinc = Fabrica.Instancia.ObterRepositorio<IRepositorioSincronizaPessoaAcesso>(UnidadeTrabalho);
            IRepositorioGrupoAcesso repGrupoAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoAcesso>(UnidadeTrabalho);


            return null;
        }

        public ActionResult Pesquisar(StoreRequestParameters parameters, string placa, int? codigoGrupoLeitora, bool? status)
        {
            var acessos = FiltrarPesquisa(placa, codigoGrupoLeitora, status);

            int total = acessos.Count();
            var data = MapModelView(acessos, Map, parameters);
            return new StoreResult(data, total);
        }

        public ActionResult ObterListaVeiculosCredenciais(StoreRequestParameters parameters, string placa)
        {
            var credenciaisVeiculos = ObterListaVeiculosCredenciasImpressas(placa);

            int total = credenciaisVeiculos.Count();
            var data = MapModelView(credenciaisVeiculos, Map, parameters);
            return new StoreResult(data, total);
        }

        public ActionResult SelecionaVeiculoCredencialVeiculo(StoreRequestParameters parameters, int codigo)
        {
            IRepositorioVeiculoCredencialVeiculo repositorioVeiculoCredencialVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioVeiculoCredencialVeiculo>(UnidadeTrabalho);

            VeiculoCredencialVeiculo veiculoCredencialVeiculo = repositorioVeiculoCredencialVeiculo.ObterPorId(codigo);

            var data = Map(veiculoCredencialVeiculo);

            return new StoreResult(data);
        }

        /// <summary>
        /// Ativar inativar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult AtivarInativar(int codigo)
        {
            IRepositorioAcessoTemporarioVeiculo repAcessoTemp = Fabrica.Instancia.ObterRepositorio<IRepositorioAcessoTemporarioVeiculo>(UnidadeTrabalho);
            AcessoTemporarioVeiculo acessoTempVeiculo = repAcessoTemp.ObterPorId(codigo);

            if (acessoTempVeiculo.Ativo)
                acessoTempVeiculo.Inativar();
            else
            {
                repAcessoTemp.Validar(acessoTempVeiculo, EstadoObjeto.Ativado);
                acessoTempVeiculo.Ativar();
            }

            repAcessoTemp.Atualizar(acessoTempVeiculo);


            var jsonAcessoTemp = new
            {
                Codigo = acessoTempVeiculo.Codigo,
                //map to view model
                GrupoLeitoraNome = acessoTempVeiculo.GrupoLeitora.Nome,
                DtHrValidadeInicio = acessoTempVeiculo.DataHoraValidadeInicio.ToString("dd/MM/yyyy HH:mm"),
                DtHrValidadeFim = acessoTempVeiculo.DataHoraValidadeFim.ToString("dd/MM/yyyy HH:mm"),
                AtivoTemp = acessoTempVeiculo.Ativo
            };
            return Json(jsonAcessoTemp);
        }
        #endregion

        #region Métodos Privados
        [NonAction]
        private IQueryable<AcessoTemporarioVeiculo> FiltrarPesquisa(string placa, int? codigoGrupoLeitora, bool? status)
        {
            IRepositorioAcessoTemporarioVeiculo repAcessoTemp = Fabrica.Instancia.ObterRepositorio<IRepositorioAcessoTemporarioVeiculo>(UnidadeTrabalho);
            IQueryable<AcessoTemporarioVeiculo> listaAcessosTempVeiculo = repAcessoTemp.ObterTodos().OrderBy(p => p.VeiculoCredencialVeiculo.Veiculo.Placa);

            if (!string.IsNullOrEmpty(placa))
            {
                listaAcessosTempVeiculo = listaAcessosTempVeiculo.Where(atv => atv.VeiculoCredencialVeiculo.Veiculo.Placa.ToUpper().Contains(placa.ToUpper()));
            }


            if (codigoGrupoLeitora.HasValue)
            {
                listaAcessosTempVeiculo = listaAcessosTempVeiculo.Where(atv => atv.GrupoLeitora.Codigo == codigoGrupoLeitora.Value);
            }

            if (status.HasValue)
            {
                listaAcessosTempVeiculo = listaAcessosTempVeiculo.Where(atv => atv.Ativo == status.Value);
            }

            return listaAcessosTempVeiculo;
        }

        [NonAction]
        private IQueryable<VeiculoCredencialVeiculo> ObterListaVeiculosCredenciasImpressas(string placa)
        {
            IRepositorioVeiculoCredencialVeiculo repositorioVeiculoCredencialVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioVeiculoCredencialVeiculo>(UnidadeTrabalho);

            IQueryable<VeiculoCredencialVeiculo> veiculosCredenciados = repositorioVeiculoCredencialVeiculo.ObterTodos()
                .Where(c => c.CredencialVeiculo.Situacoes.Any(s => s.Status == StatusCredencial.Impressa)
                        && c.CredencialVeiculo.Situacoes.Any(s => s.Status == StatusCredencial.Devolvida) == false
                        && (c.CredencialVeiculo.DataVencimento == null || c.CredencialVeiculo.DataVencimento > DateTime.Now));

            if (!string.IsNullOrEmpty(placa))
            {
                veiculosCredenciados = veiculosCredenciados.Where(atv => atv.Veiculo.Placa.ToUpper().Contains(placa.ToUpper()));
            }
            
            return veiculosCredenciados;
        }

        [NonAction]
        static IList<TTypeModelView> MapModelView<TType, TTypeModelView>(IQueryable<TType> sources, Func<TType, TTypeModelView> Map, StoreRequestParameters parameters = null, bool ignoreParameters = false) where TType : class, new() where TTypeModelView : class, new()
        {
            try
            {
                if (!ignoreParameters)
                {
                    if (parameters == null)
                        parameters = new StoreRequestParameters();
                    int total = sources.Count();
                    sources = sources.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 25);
                }
                return sources.Select(p => Map(p)).ToList();
            }
            catch (Exception)
            { throw; }
        }

        [NonAction]
        private AcessoTemporarioVeiculoModelView Map(AcessoTemporarioVeiculo acessoTempVeiculo)
        {
            var acessoTemVeiculoViewModel = new AcessoTemporarioVeiculoModelView();
            acessoTemVeiculoViewModel.Codigo = acessoTempVeiculo.Codigo;
            acessoTemVeiculoViewModel.CodigoVeiculoCredencialVeiculo = acessoTempVeiculo.VeiculoCredencialVeiculo.Codigo;
            acessoTemVeiculoViewModel.Placa = acessoTempVeiculo.VeiculoCredencialVeiculo.Veiculo.Placa;
            acessoTemVeiculoViewModel.Fabricante = acessoTempVeiculo.VeiculoCredencialVeiculo.Veiculo.Modelo.Fabricante.Nome;
            acessoTemVeiculoViewModel.Empresa = acessoTempVeiculo.VeiculoCredencialVeiculo.CredencialVeiculo.Empresa.Nome;
            acessoTemVeiculoViewModel.Modelo = acessoTempVeiculo.VeiculoCredencialVeiculo.Veiculo.Modelo.Nome;
            acessoTemVeiculoViewModel.Cor = acessoTempVeiculo.VeiculoCredencialVeiculo.Veiculo.Cor;
            acessoTemVeiculoViewModel.GrupoLeitoraNome = acessoTempVeiculo.GrupoLeitora.Nome;
            acessoTemVeiculoViewModel.DataHoraValidadeInicio = acessoTempVeiculo.DataHoraValidadeInicio.ToString("dd/MM/yyyy HH:mm");
            acessoTemVeiculoViewModel.DataHoraValidadeFim = acessoTempVeiculo.DataHoraValidadeFim.ToString("dd/MM/yyyy HH:mm");
            acessoTemVeiculoViewModel.AtivoTemp = acessoTempVeiculo.Ativo;
            return acessoTemVeiculoViewModel;
        }

        [NonAction]
        private AcessoTemporarioVeiculoModelView Map(VeiculoCredencialVeiculo veiculoCredencialVeiculo)
        {
            return new AcessoTemporarioVeiculoModelView
            {
                CodigoVeiculoCredencialVeiculo = veiculoCredencialVeiculo.Codigo,
                Placa = veiculoCredencialVeiculo.Veiculo.Placa,
                Fabricante = veiculoCredencialVeiculo.Veiculo.Modelo.Fabricante.Nome,
                Empresa = veiculoCredencialVeiculo.CredencialVeiculo.Empresa.Nome,
                Modelo = veiculoCredencialVeiculo.Veiculo.Modelo.Nome,
                Cor = veiculoCredencialVeiculo.Veiculo.Cor,
            };
        }
        #endregion
    }
}
