﻿using Ext.Net;
using Globalsys;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar crops.
    /// </summary>
    [ExtendController]
    public class CropController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /Crop/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.CropController.
        /// </summary>
        ///
        /// <param name="unidade">
        /// O(a) unidade.
        /// </param>
        public CropController(IUnidadeTrabalho unidade)
        {
            UnidadeTrabalho = unidade;
        }

        /// <summary>
        /// Crop.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Crop()
        {
            return View();

        }

        //[DirectMethod(Namespace = "Metodo")]
        

    }
}
