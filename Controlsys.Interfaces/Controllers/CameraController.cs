﻿using Controlsys.Dominio.Acesso;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Acesso;
using Controlsys.Repositorios.Acesso;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar as câmeras
    /// </summary>
    [ExtendController]
    public class CameraController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /Camera/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.CameraController.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade.
        /// </param>        
        public CameraController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        /// <summary>
        /// Retorna a página de camera.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [ExcludeFromCodeCoverage]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Inclui um novo camera.
        /// </summary>
        ///
        /// <param name="camera">
        /// O camera.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Incluir(Camera camera)
        {
            IRepositorioCamera repCamera = Fabrica.Instancia.ObterRepositorio<IRepositorioCamera>(UnidadeTrabalho);
            camera.Nome = camera.Nome;
            camera.Modelo = camera.Modelo;
            camera.Descricao = camera.Descricao;
            camera.DataRegistro = DateTime.Now;
            camera.Precisao = camera.Precisao;
            camera.Ativar();
            repCamera.Validar(camera, EstadoObjeto.Novo);
            repCamera.Salvar(camera);
            //return Json(camera);
            return Json(formatObject(camera));
        }

        /// <summary>
        /// Altera um registro de camera.
        /// </summary>
        ///
        /// <param name="camera">
        /// O(a) area.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Alterar(Camera camera)
        {
            IRepositorioCamera repCamera = Fabrica.Instancia.ObterRepositorio<IRepositorioCamera>(UnidadeTrabalho);
            Camera cameraA = repCamera.ObterPorId(camera.Codigo);
            cameraA.Nome = camera.Nome;
            cameraA.Modelo = camera.Modelo;
            cameraA.Descricao = camera.Descricao;
            cameraA.DataRegistro = camera.DataRegistro;
            cameraA.Precisao = camera.Precisao;

            repCamera.Validar(cameraA, EstadoObjeto.Alterado);
            repCamera.Atualizar(cameraA);
            //return Json(cameraA);
            return Json(formatObject(cameraA));
        }

        /// <summary>
        /// Ativar inativar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult AtivarInativar(int codigo)
        {
            IRepositorioCamera repCamera = Fabrica.Instancia.ObterRepositorio<IRepositorioCamera>(UnidadeTrabalho);
            Camera camera = repCamera.ObterPorId(codigo);
            if (camera.Ativo)
            {
                repCamera.Validar(camera, EstadoObjeto.Inativado);
                camera.Inativar();
            }
            else
            {
                repCamera.Validar(camera, EstadoObjeto.Ativado);
                camera.Ativar();
            }
            repCamera.Atualizar(camera);            
            return Json(formatObject(camera));
        }

        /// <summary>
        /// Selecionar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Selecionar(int codigo)
        {
            IRepositorioCamera repCamera = Fabrica.Instancia.ObterRepositorio<IRepositorioCamera>(UnidadeTrabalho);
            Camera camera = repCamera.ObterPorId(codigo);
            //return Json(camera);
            return Json(formatObject(camera));
        }

        /// <summary>
        /// Retorna todos os objetos do tipo Camera.
        /// </summary>
        ///
        /// <param name="apenasAtivos">
        /// true to apenas ativos.
        /// </param>
        ///
        /// <returns>
        /// Detalhes do tipo de objeto ligado ao controller no formato JSON.
        /// </returns>
        public ActionResult ObterTodos(bool apenasAtivos = false, int[] controladora = null)
        {
            IRepositorioCamera repCamera = Fabrica.Instancia.ObterRepositorio<IRepositorioCamera>(UnidadeTrabalho);

            IQueryable<Camera> cameras = repCamera.ObterTodos();

            if (apenasAtivos)
            {
                cameras = cameras.Where(t => t.Ativo == true);
            }

            if (controladora != null)
            {
                cameras = cameras.Where(t => t.Controladora == null || controladora.Contains(t.Controladora.Codigo));
            }

            return Json(cameras.ToList().Select(c => formatObject(c)).ToArray());
        }


        /// <summary>
        /// Format object.
        /// </summary>
        ///
        /// <param name="Camera">
        /// O(a) controladora.
        /// </param>        
        ///
        /// <returns>
        /// Um(a) object.
        /// </returns>
        /// 
        private CameraModelView formatObject(Camera camera)
        {
            IRepositorioControladora repControladora = Fabrica.Instancia.ObterRepositorio<IRepositorioControladora>(UnidadeTrabalho);

            return new CameraModelView
            {
                Codigo = camera.Codigo,                
                Nome = camera.Nome,
                Modelo = camera.Modelo,
                Descricao = camera.Descricao,
                DataRegistro = camera.DataRegistro,
                Ativo = camera.Ativo,
                DataStatus = camera.DataStatus,                
                Precisao = camera.Precisao
            };
        }
    }
}
