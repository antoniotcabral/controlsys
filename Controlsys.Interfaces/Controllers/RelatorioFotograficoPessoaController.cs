﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Pessoas;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Parametros;
using Controlsys.Repositorios.Empresas;
using Controlsys.Repositorios.Pessoas;
using Ext.Net;
using Ext.Net.MVC;
using Globalsys;
using Globalsys.Exceptions;
using Microsoft.Reporting.WebForms;
using Controlsys.Infra;
using Globalsys.Relatorios;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class RelatorioFotograficoPessoaController : Controller
    {
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        public RelatorioFotograficoPessoaController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Exportar(List<string> selectedFields, string tipoRelatorio, StatusPapelLog? status, TipoPapel? papel, string nome = null, string cpf = null, string empresa = null, string cargo = null)
        {
            empresa = Empresa.RetornaApenasNomeEmpresa(empresa);

            LocalReport relatorio = new LocalReport();
            relatorio.ReportPath = Server.MapPath("~/Reports/RelatorioFotograficoPessoa.rdlc");
            var regs = FiltrarPapeis(status, papel, nome, cpf, empresa, cargo);
            var lista = montaListaObj(regs, null, true);

            relatorio.DataSources.Add(new ReportDataSource("DTORelatorioFotograficoPessoa", lista));
            relatorio.SetParameters(new ReportParameter("bo_colCodigoPapel", selectedFields.Contains("Codigo") ? "false" : "true"));
            relatorio.SetParameters(new ReportParameter("bo_colCPF", selectedFields.Contains("CPF") ? "false" : "true"));
            relatorio.SetParameters(new ReportParameter("bo_colPassaporte", selectedFields.Contains("Passaporte") ? "false" : "true"));
            relatorio.SetParameters(new ReportParameter("bo_colNome", selectedFields.Contains("Nome") ? "false" : "true"));
            relatorio.SetParameters(new ReportParameter("bo_colApelido", selectedFields.Contains("Apelido") ? "false" : "true"));
            relatorio.SetParameters(new ReportParameter("bo_colSexo", selectedFields.Contains("Sexo") ? "false" : "true"));
            relatorio.SetParameters(new ReportParameter("bo_colPais", selectedFields.Contains("Pais") ? "false" : "true"));
            relatorio.SetParameters(new ReportParameter("bo_colEstado", selectedFields.Contains("Estado") ? "false" : "true"));
            relatorio.SetParameters(new ReportParameter("bo_colCidade", selectedFields.Contains("Cidade") ? "false" : "true"));
            relatorio.SetParameters(new ReportParameter("bo_colEmpresa", selectedFields.Contains("Empresa") ? "false" : "true"));
            relatorio.SetParameters(new ReportParameter("bo_colCargo", selectedFields.Contains("Cargo") ? "false" : "true"));
            relatorio.SetParameters(new ReportParameter("bo_colDataStatus", selectedFields.Contains("DataStatus") ? "false" : "true"));
            relatorio.SetParameters(new ReportParameter("bo_colStatus", selectedFields.Contains("Status") ? "false" : "true"));
            relatorio.SetParameters(new ReportParameter("bo_colPapel", selectedFields.Contains("Papel") ? "false" : "true"));
            relatorio.SetParameters(new ReportParameter("bo_colContratada", selectedFields.Contains("Contratada") ? "false" : "true"));
            relatorio.SetParameters(new ReportParameter("bo_colRFID", selectedFields.Contains("RFID") ? "false" : "true"));
            relatorio.SetParameters(new ReportParameter("bo_colFoto", "false"));

            string fileName = string.Empty;
            string mimeType;
            switch (tipoRelatorio)
            {
                case "PDF":
                    mimeType = "application/pdf";
                    fileName = "download.pdf";
                    break;
                case "EXCELOPENXML":
                    mimeType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    fileName = "download.xlsx";
                    break;
                case "WORDOPENXML":
                    mimeType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                    fileName = "download.docx";
                    break;
            }

            string encoding;
            string fileNameExtension;

            Warning[] warnings;
            string[] streams;
            byte[] bytes;

            bytes = relatorio.Render(
            tipoRelatorio,
            null,
            out mimeType,
            out encoding,
            out fileNameExtension,
            out streams,
            out warnings);

            return File(bytes, mimeType, fileName);
        }

        /// <summary>
        /// Pesquisar papeis.
        /// </summary>
        ///
        /// <param name="parameters">
        /// Options for controlling the operation.
        /// </param>
        /// <param name="status">
        /// O(a) status.
        /// </param>
        /// <param name="nome">
        /// O(a) nome.
        /// </param>
        /// <param name="cpf">
        /// O(a) cpf.
        /// </param>
        /// <param name="empresa">
        /// O(a) empresa.
        /// </param>
        /// <param name="cargo">
        /// O(a) cargo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Pesquisar(StoreRequestParameters parameters, StatusPapelLog? status, TipoPapel? papel, string nome = null, string cpf = null, string empresa = null, string cargo = null)
        {
            empresa = Empresa.RetornaApenasNomeEmpresa(empresa);

            var papeis = FiltrarPapeis(status, papel, nome, cpf, empresa, cargo);

            int total = papeis.Count();
            var data = montaListaObj(papeis, parameters);

            return new StoreResult(data, total);
        }

        /// <summary>
        /// Filtrar papeis.
        /// </summary>
        ///
        /// <param name="status">
        /// O(a) status.
        /// </param>
        /// <param name="nome">
        /// O(a) nome.
        /// </param>
        /// <param name="cpf">
        /// O(a) cpf.
        /// </param>
        /// <param name="empresa">
        /// O(a) empresa.
        /// </param>
        /// <param name="cargo">
        /// O(a) cargo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) IQueryable&lt;Papel&gt;
        /// </returns>
        private IQueryable<Papel> FiltrarPapeis(StatusPapelLog? status, TipoPapel? papel, string nome = null, string cpf = null, string empresa = null, string cargo = null)
        {
            bool allIsNull = true;

            string query = @"
            SELECT      PA.* 
                        ,PF.[CD_PESSOA_FISICA]
                        ,PF.[TX_CPF]
                        ,PF.[CD_NATURAL] 
                        ,PF.[TX_PASSAPORTE]
                        ,PF.[TX_RG]
                        ,PF.[TX_RG_ORGAO]
                        ,PF.[CD_RG_UF]
                        ,PF.[DT_RG_EMISSAO]
                        ,PF.[DT_NASCIMENTO]
                        ,PF.[TX_MAE]
                        ,PF.[TX_PAI]
                        ,PF.[TX_SANGE]
                        ,PF.[TX_SEXO]
                        ,PF.[ME_FOTO]
                        ,PF.[DT_VISTO]
                        ,PF.[DT_VALIDADE_VISTO]
                        ,PF.[TX_RNE]
                        ,PF.[DT_EXPEDICAO_RNE]
                        ,PF.[TX_ESTADOCIVIL]
                        ,PF.[TX_ESCOLARIDADE]
                        ,PF.[NU_CR_NR]
                        ,PF.[NU_CR_TIPO]
                        ,PF.[TX_CNH]
                        ,PF.[TX_CNH_TP]
                        ,PF.[DT_CNH_VALIDADE]
                        ,PF.[NU_TE]
                        ,PF.[NU_TE_ZONA]
                        ,PF.[NU_TE_SECAO]
                        ,PF.[NU_CT]
                        ,PF.[TX_CT_SERIE]
                        ,PF.[CD_CT_ID_UF]
                        ,PF.[DT_CT_DT]
                        ,PF.[CD_MUNICIPIO]
                        ,PF.[TX_NACIONALIDADE]
                        , CO.*, PR.*, V.*, EC.*, EP.*, PEPF.*, PEEC.*, PEEP.*, CA.*
                        , CASE 
                            WHEN CO.CD_PAPEL is not null then 1
                            WHEN PR.CD_PAPEL is not null then 2
                            WHEN V.CD_PAPEL is not null then 3
                            WHEN EC.CD_PESSOA is not null then 4
                            WHEN EP.CD_PESSOA is not null then 5
                            WHEN PEPF.CD_PESSOA is not null then 6
                            WHEN PEEC.CD_PESSOA is not null then 7
                            WHEN PEEP.CD_PESSOA is not null then 8
                            WHEN CA.CD_CARGO is not null then 9
            ELSE 0
                        END as clazz_
            FROM  PAPEL PA
               INNER JOIN PESSOA_FISICA PF ON PA.CD_PESSOA_FISICA = PF.CD_PESSOA_FISICA
               LEFT JOIN COLABORADOR CO ON PA.CD_PAPEL = CO.CD_PAPEL
               LEFT JOIN PRESTADOR PR ON PA.CD_PAPEL = PR.CD_PAPEL
               LEFT JOIN VISITANTE V ON PA.CD_PAPEL = V.CD_PAPEL
               LEFT JOIN EMPRESA EC ON CO.CD_EMPRESA = EC.CD_PESSOA
               LEFT JOIN EMPRESA EP ON PR.CD_EMPRESA = EP.CD_PESSOA
               LEFT JOIN PESSOA PEPF ON PEPF.CD_PESSOA = PF.CD_PESSOA_FISICA
               LEFT JOIN PESSOA PEEC ON EC.CD_PESSOA = PEEC.CD_PESSOA
               LEFT JOIN PESSOA PEEP ON EP.CD_PESSOA = PEEP.CD_PESSOA
               LEFT JOIN CARGO CA ON PA.CD_CARGO = CA.CD_CARGO
            WHERE  1 = 1
            --AND PF.[ME_FOTO] IS NOT NULL 
            ";

            string queryFiltroAtivo = @"AND			EXISTS (SELECT		PL.CD_PAPEL 
					FROM		PAPEL_LOG PL
								INNER JOIN (select		pl.CD_PAPEL, max(pl.CD_PAPEL_LOG) CD_PAPEL_LOG 
											from		papel_log pl 
											group by	pl.CD_PAPEL) MAX_PL ON PL.CD_PAPEL_LOG = MAX_PL.CD_PAPEL_LOG
					WHERE		UPPER(PL.TX_STATUS) = :status
					and			PA.CD_PAPEL = PL.CD_PAPEL)";

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            List<string> filtros = new List<string>();

            if (!string.IsNullOrEmpty(nome))
            {
                parameters.Add("nome", nome);
                filtros.Add("AND			PEPF.TX_NOME LIKE CONCAT('%', :nome, '%')");
            }

            if (!string.IsNullOrEmpty(cpf))
            {
                cpf = cpf.Replace(".", "").Replace("-", "");
                parameters.Add("cpf", cpf);
                filtros.Add("AND			PF.TX_CPF LIKE CONCAT('%', :cpf, '%')");
            }

            if (!string.IsNullOrEmpty(cargo))
            {
                parameters.Add("cargo", cargo);
                filtros.Add("AND			(CA.TX_NOME = :cargo OR V.TX_FUNCAO LIKE CONCAT('%', :cargo, '%'))");
            }

            if (!string.IsNullOrEmpty(empresa))
            {
                parameters.Add("empresa", empresa);
                filtros.Add("AND			(PEEC.TX_NOME = :empresa OR PEEP.TX_NOME = :empresa OR V.TX_EMPRESA LIKE CONCAT('%', :empresa, '%'))");
            }

            if (status.HasValue)
            {
                parameters.Add("status", status.Value.ToString());
                filtros.Add(queryFiltroAtivo);
            }

            if (filtros.Any())
                query += string.Join(Environment.NewLine, filtros);

            query += string.Join(Environment.NewLine, "ORDER BY ISNULL(PEPF.TX_NOME, ISNULL(PEEC.TX_NOME, ISNULL(PEEP.TX_NOME, '')))");

            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            IQueryable<Papel> resultado = unidTrabalho.ExecuteSql<Papel>(query, parameters, new List<Type>() { typeof(Papel) }).AsQueryable();
            if (papel.HasValue)
            {
                switch (papel.Value)
                {
                    case TipoPapel.Visitante:
                        resultado = resultado.OfType<Visitante>();
                        break;
                    case TipoPapel.Colaborador:
                        resultado = resultado.OfType<Colaborador>();
                        break;
                    case TipoPapel.PrestadorServico:
                        resultado = resultado.OfType<PrestadorServico>();
                        break;
                    default:
                        break;
                }
            }

            return resultado;
        }

        /// <summary>
        /// Cria um objeto contendo informações relacionadas ao conteudo gerenciado por
        /// RelatorioPessoasController.
        /// </summary>
        ///
        /// <param name="papeis">
        /// O(a) papeis.
        /// </param>
        /// <param name="parameters">
        /// Options for controlling the operation.
        /// </param>
        /// <param name="ignoreParameters">
        /// true to ignore parameters.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;RelatorioPessoaModelView&gt;
        /// </returns>
        private List<RelatorioFotograficoPessoaModelView> montaListaObj(IQueryable<Papel> papeis, StoreRequestParameters parameters = null, bool ignoreParameters = false)
        {
            if (!ignoreParameters)
            {
                if (parameters == null)
                    parameters = new StoreRequestParameters();
                papeis = papeis.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 25);
            }

            return papeis.Select(p => montaObj(p)).ToList();
        }

        private RelatorioFotograficoPessoaModelView montaObj(Papel papel)
        {
            IRepositorioPrestadorServico repPrestador = Fabrica.Instancia.ObterRepositorio<IRepositorioPrestadorServico>(UnidadeTrabalho);
            IRepositorioAlocacaoColaborador repAlocacao = Fabrica.Instancia.ObterRepositorio<IRepositorioAlocacaoColaborador>(UnidadeTrabalho);
            IRepositorioCracha repCracha = Fabrica.Instancia.ObterRepositorio<IRepositorioCracha>(UnidadeTrabalho);
            IQueryable<AlocacaoColaborador> aloc = repAlocacao.ObterTodos();

            var relPessoa = new RelatorioFotograficoPessoaModelView();

            relPessoa.CodigoPapel = papel.Codigo;
            relPessoa.Foto = papel.PessoaFisica.Foto != null ? Convert.ToBase64String(papel.PessoaFisica.Foto) : null;

            string apelidoX = "", cargoX = "";

            AlocacaoColaborador alocacao = aloc.Where(a => a.Papel.Codigo == papel.Codigo).OrderByDescending(a => a.DataRegistro).FirstOrDefault();
            PedidoCompra alocada = alocacao != null ? alocacao.ObterContratada() : null;

            Cracha cracha = repCracha.ObterCracha(papel.Codigo);
            if (cracha != null)
                relPessoa.RFID = cracha.Numero;

            if (papel is Colaborador)
            {
                var colabX = (papel as Colaborador);

                apelidoX = ((colabX.Empresa) != null ? colabX.Empresa.Nome : "");
                cargoX = (colabX.Cargo != null) ? colabX.Cargo.Nome : "";
            }
            else if (papel is PrestadorServico)
            {
                var prestX = (papel as PrestadorServico);

                var prest = repPrestador.ObterTodos().FirstOrDefault(p => p.Codigo == prestX.Codigo);
                apelidoX = prest.TipoPrestador == TipoPrestador.Temporario ? prest.Empresa.Nome : prest.Fornecedor.NomeFantasia;

                cargoX = (prest.Cargo != null) ? prest.Cargo.Nome : "";
            }
            else if (papel is Visitante)
            {
                var visitX = (papel as Visitante);
                apelidoX = visitX.Empresa;
                cargoX = visitX.Funcao;
            }

            relPessoa.CPF = papel.PessoaFisica.CPFFormatado();
            relPessoa.Passaporte = papel.PessoaFisica.Passaporte;
            relPessoa.Nome = papel.PessoaFisica.Nome;
            relPessoa.Apelido = papel.PessoaFisica.Apelido;
            relPessoa.Sexo = papel.PessoaFisica.Sexo != 0 ? papel.PessoaFisica.Sexo.ObterDescricaoEnum() : "";

            if (papel.PessoaFisica.Endereco != null)
            {
                if (papel.PessoaFisica.Endereco.Logradouro != null)
                {
                    relPessoa.Cidade = papel.PessoaFisica.Endereco.Logradouro.Bairro.Cidade.Nome;
                    relPessoa.Estado = papel.PessoaFisica.Endereco.Logradouro.Bairro.Cidade.Estado.Nome;
                    relPessoa.Pais = papel.PessoaFisica.Endereco.Logradouro.Bairro.Cidade.Estado.Pais.Nome;
                }
            }

            relPessoa.Empresa = apelidoX;
            relPessoa.Cargo = cargoX;

            PapelLog _papelLog = papel.ObterLogPapel();

            if (_papelLog != null)
            {
                relPessoa.Status = _papelLog.Status.ObterDescricaoEnum();
                relPessoa.DataStatus = _papelLog.DataInicio;
            }

            relPessoa.Papel = papel != null ? papel is Colaborador ? "Colaborador" : papel is PrestadorServico ? "Prestador de Serviço" : "Visitante" : "";


            if (alocada != null)
            {
                if (alocada.Numero == alocacao.PedidoCompra.Numero)
                {
                    relPessoa.Contratada = ""; // alocacao.PedidoCompra.Empresa.Nome;
                    //itemLista.Subcontratada = "";
                }
                else
                {
                    relPessoa.Contratada = alocada.Empresa.Nome;
                    //itemLista.Subcontratada = alocacao.PedidoCompra.Empresa.Nome;
                }
            }

            return relPessoa;

        }

    }
}
