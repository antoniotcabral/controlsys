﻿using Controlsys.Dominio.Parametros;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorios.Parametros;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar turnoes.
    /// </summary>
    [ExtendController]
    public class TurnoController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /Turno/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.TurnoController.
        /// </summary>
        ///
        /// <param name="unidade">
        /// O(a) unidade.
        /// </param>
        public TurnoController(IUnidadeTrabalho unidade) { UnidadeTrabalho = unidade; }

        /// <summary>
        /// Retorna a página de turno.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Inclui um novo turno.
        /// </summary>
        ///
        /// <param name="turno">
        /// O(a) turno.
        /// </param>
        /// <param name="horasTurno">
        /// O(a) horas turno.
        /// </param>
        /// <param name="feriados">
        /// O(a) feriados.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Incluir(Turno turno, List<HoraTurno> horasTurno, List<Feriado> feriados)
        {
            IRepositorioTurno repTurno = Fabrica.Instancia.ObterRepositorio<IRepositorioTurno>(UnidadeTrabalho);
            turno.DataRegistro = DateTime.Now;
            turno.Ativar();
            repTurno.Validar(turno, EstadoObjeto.Novo);
            repTurno.Salvar(turno);

            if (horasTurno != null)
                foreach (var horaTurno in horasTurno)
                {
                    horaTurno.Turno = turno;
                    horaTurno.DataRegistro = DateTime.Now;
                    horaTurno.Ativar();
                    repTurno.SalvarHoraTurno(horaTurno);
                }

            if (feriados != null)
                foreach (var item in feriados)
                {
                    FeriadoTurno feriadoTurno = new FeriadoTurno();
                    feriadoTurno.Feriado = item;
                    feriadoTurno.Turno = turno;
                    feriadoTurno.DataRegistro = DateTime.Now;
                    feriadoTurno.Ativar();
                    repTurno.SalvarFeriadoTurno(feriadoTurno);
                }

            return Json(montaObj(turno));
        }

        /// <summary>
        /// Altera um registro de turno.
        /// </summary>
        ///
        /// <param name="turno">
        /// O(a) turno.
        /// </param>
        /// <param name="horasTurnoDel">
        /// O(a) horas turno delete.
        /// </param>
        /// <param name="horasTurnoNew">
        /// O(a) horas turno new.
        /// </param>
        /// <param name="feriadosDel">
        /// O(a) feriados delete.
        /// </param>
        /// <param name="feriadosNew">
        /// O(a) feriados new.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Alterar(Turno turno, int[] horasTurnoDel, List<HoraTurno> horasTurnoNew, int[] feriadosDel, int[] feriadosNew)
        {
            IRepositorioTurno repTurno = Fabrica.Instancia.ObterRepositorio<IRepositorioTurno>(UnidadeTrabalho);
            Turno turnoObj = repTurno.ObterPorId(turno.Codigo);
            turnoObj.Nome = turno.Nome;
            turnoObj.ObedeceEscala = turno.ObedeceEscala;
            turnoObj.Tolerancia = turno.Tolerancia;
            repTurno.Validar(turnoObj, EstadoObjeto.Alterado);

            #region [Horas Turno]
            if (horasTurnoDel != null)
            {
                foreach (var item in turnoObj.HorasTurno.Where(t => horasTurnoDel.Any(tdd => tdd == t.Codigo)))
                {
                    HoraTurno horaTurno = item;
                    horaTurno.Inativar();
                }
            }
            if (horasTurnoNew != null)
            {
                foreach (var horaTurno in horasTurnoNew)
                {
                    horaTurno.Turno = turnoObj;
                    horaTurno.DataRegistro = DateTime.Now;
                    horaTurno.Ativar();
                    repTurno.SalvarHoraTurno(horaTurno);
                }
            }
            #endregion

            #region [Feriados Turno]
            if (feriadosDel != null)
            {
                foreach (var item in turnoObj.FeriadosTurno.Where(t => feriadosDel.Any(tdd => tdd == t.Feriado.Codigo)))
                {
                    FeriadoTurno feriadoTurno = item;
                    feriadoTurno.Inativar();
                }
            }
            if (feriadosNew != null)
            {
                foreach (var item in feriadosNew)
                {
                    FeriadoTurno feriadoTurno = new FeriadoTurno();
                    feriadoTurno.Turno = turnoObj;
                    feriadoTurno.Feriado = new Feriado { Codigo = item };
                    feriadoTurno.DataRegistro = DateTime.Now;
                    feriadoTurno.Ativar();
                    repTurno.SalvarFeriadoTurno(feriadoTurno);
                }
            }
            #endregion

            #region [Com lista - desabilitado]
            //if (horasTurnoDel != null)
            //{
            //    foreach (var item in turnoObj.HorasTurno.Where(t => horasTurnoDel.Any(tdd => tdd.Codigo == t.Codigo)))
            //    {
            //        HoraTurno horaTurno = item;
            //        horaTurno.Inativar();
            //        //repTurno.SalvarHoraTurno(horaTurno);
            //    }
            //}
            //if (horasTurnoNew != null)
            //{
            //    foreach (var horaTurno in horasTurnoNew)
            //    {
            //        horaTurno.Turno = turno;
            //        horaTurno.DataRegistro = DateTime.Now;
            //        horaTurno.Ativar();
            //        repTurno.SalvarHoraTurno(horaTurno);
            //    }
            //}
            //
            //if (feriadosDel != null)
            //{
            //    foreach (var item in turnoObj.FeriadosTurno.Where(t => feriadosDel.Any(tdd => tdd.Codigo == t.Feriado.Codigo)))
            //    {
            //        FeriadoTurno feriadoTurno = item;
            //        feriadoTurno.Inativar();
            //    }
            //}
            //if (feriadosNew != null)
            //{
            //    foreach (var item in feriadosNew)
            //    {
            //        FeriadoTurno feriadoTurno = new FeriadoTurno();
            //        feriadoTurno.Turno = turno;
            //        feriadoTurno.Feriado = new Feriado { Codigo = item.Codigo };
            //        feriadoTurno.DataRegistro = DateTime.Now;
            //        feriadoTurno.Ativar();
            //        repTurno.SalvarFeriadoTurno(feriadoTurno);
            //    }
            //}
            #endregion
            return Json(montaObj(turnoObj));
        }

        /// <summary>
        /// Ativar inativar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult AtivarInativar(int codigo)
        {
            IRepositorioTurno repTurno = Fabrica.Instancia.ObterRepositorio<IRepositorioTurno>(UnidadeTrabalho);
            Turno turno = repTurno.ObterPorId(codigo);

            if (turno.Ativo)
            {
                repTurno.Validar(turno, EstadoObjeto.Inativado);
                turno.Inativar();
            }
            else
            {
                repTurno.Validar(turno, EstadoObjeto.Ativado);
                turno.Ativar();
            }

            return Json(montaObj(turno));
        }

        /// <summary>
        /// Selecionar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Selecionar(int codigo)
        {
            IRepositorioTurno repTurno = Fabrica.Instancia.ObterRepositorio<IRepositorioTurno>(UnidadeTrabalho);
            Turno turno = repTurno.ObterPorId(codigo);
            return Json(montaObj(turno));
        }

        /// <summary>
        /// Retorna todos os objetos do tipo Turno.
        /// </summary>
        ///
        /// <param name="apenasAtivos">
        /// true to apenas ativos.
        /// </param>
        ///
        /// <returns>
        /// Detalhes do tipo de objeto ligado ao controller no formato JSON.
        /// </returns>
        public ActionResult ObterTodos(bool apenasAtivos = false)
        {
            IRepositorioTurno repTurno = Fabrica.Instancia.ObterRepositorio<IRepositorioTurno>(UnidadeTrabalho);

            IQueryable<Turno> turno = repTurno.ObterTodos();

            if (apenasAtivos)
            {
                turno = turno.Where(t => t.Ativo == true);
            }

            return Json(turno.Select(a => new
                               {
                                   Codigo = a.Codigo,
                                   Nome = a.Nome,
                                   ObedeceEscala = a.ObedeceEscala,
                                   Ativo = a.Ativo,
                                   Tolerancia = a.Tolerancia
                               })
                               .ToList());
        }

        /// <summary>
        /// Cria um objeto contendo informações relacionadas ao conteudo gerenciado por
        /// TurnoController.
        /// </summary>
        ///
        /// <param name="turno">
        /// O(a) turno.
        /// </param>
        ///
        /// <returns>
        /// Um(a) object.
        /// </returns>
        private object montaObj(Turno turno)
        {
            return new TurnoModelView()
            {
                Codigo = turno.Codigo,
                Nome = turno.Nome,
                ObedeceEscala = turno.ObedeceEscala,
                Ativo = turno.Ativo,
                Tolerancia = turno.Tolerancia.HasValue ? turno.Tolerancia.Value.ToString() : "",
                HorasTurno = turno.HorasTurno.Select(x => new HoraTurnoModelView()
                {
                    Codigo = x.Codigo,
                    //Turno = x.Turno,
                    HoraInicio = x.HoraInicio,
                    HoraFim = x.HoraFim,
                    Ordem = x.Ordem,
                    DiaSemana = x.DiaSemana.HasValue ? obterDesc(x.DiaSemana.Value) : null,
                    Trabalha = x.Trabalha,
                    Maior24h = x.Maior24h
                }).ToList(),
                FeriadosTurno = turno.FeriadosTurno.Select(x => new FeriadoTurnoModelView()
                {
                    Codigo = x.Feriado.Codigo,
                    DataFeriado = x.Feriado.DataFeriado,
                    Nome = x.Feriado.Nome,
                    Descricao = x.Feriado.Descricao,
                    Ativo = x.Feriado.Ativo
                }).ToList()
            };
        }

        /// <summary>
        /// Obter description.
        /// </summary>
        ///
        /// <param name="diaSemana">
        /// O(a) dia semana.
        /// </param>
        ///
        /// <returns>
        /// Um(a) string.
        /// </returns>
        private string obterDesc(DiaSemana diaSemana)
        {
            return EnumExtensoes.ObterDescricaoEnum(diaSemana);
        }

    }
}
