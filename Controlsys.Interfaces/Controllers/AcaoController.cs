﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Controlsys.Dominio.Seguranca;
using Controlsys.Infra;
using Controlsys.Repositorios.Seguranca;
using Globalsys;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar acaoes.
    /// </summary>
    [ExtendController]
    public class AcaoController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /Acao/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.AcaoController.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public AcaoController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        /// <summary>
        /// Retorna todos os objetos do tipo Acao.
        /// </summary>
        ///
        /// <param name="codPagina">
        /// O(a) cod pagina.
        /// </param>
        ///
        /// <returns>
        /// Detalhes do tipo de objeto ligado ao controller no formato JSON.
        /// </returns>
        public ActionResult ObterTodos(int? codPagina)
        {
            IRepositorioAcao repAcao = Fabrica.Instancia.ObterRepositorio<IRepositorioAcao>(UnidadeTrabalho);

            IQueryable<Acao> acoes = repAcao.ObterTodos();

            if (codPagina.HasValue)            
                acoes = acoes.Where(a => a.Pagina.Codigo == codPagina);            

            return Json(acoes.Select(a => new
                                         {
                                             a.Codigo,
                                             a.Nome,
                                             Pagina = a.Pagina.Nome,
                                             Auditavel = a.Auditavel
                                         })
                               .ToArray());
        }
    }
}
