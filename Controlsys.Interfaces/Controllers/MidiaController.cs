﻿using Controlsys.Dominio.Parametros;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Parametros;
using Globalsys;
using Globalsys.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar Midias.
    /// </summary>
    [ExtendController]
    public class MidiaController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /Colaborador/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.MidiaController.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public MidiaController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        public FileResult ObterMidia(string url)
        {
            if (!HttpContext.User.Identity.IsAuthenticated || string.IsNullOrEmpty(url))
                return null;

            if (!System.IO.File.Exists(url))
                throw new CoreException("Arquivo não encontrado.");

            byte[] fileBytes = System.IO.File.ReadAllBytes(url);
            string fileName = System.IO.Path.GetFileName(url);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        public FileResult ObterMidiaPessoaCritica(string url)
        {
            return this.ObterMidia(url);
        }

        public JsonResult ObterTodos(int codDocumento = 0)
        {
            IRepositorioDocumentoMidia repDocMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioDocumentoMidia>(UnidadeTrabalho);
            IQueryable<DocumentoMidia> midias = repDocMidia.ObterTodos().Where(m => m.Documento.Ativo);
            if (codDocumento != 0)
            {
                midias = midias.Where(dm => dm.Documento.Codigo == codDocumento && dm.Ativo);
            }

            DocumentoMidia[] resultados = midias.ToArray();

            var _json = resultados.Select(dm => new 
            {
                CodigoMidia = dm.Codigo,
                NomeOriginal = "",
                nomeDisco = dm.Midia.NomeArquivo,
                DataRegistro = dm.Midia.DataRegistro,
                Arquivo = "",
                MIME = dm.Midia.MimeType,
                Extensao = dm.Midia.Extensao,
                URL = dm.Midia.Url
            }).ToList();

            return Json(_json, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ObterMidiaVeiculo(int codVeiculo = 0, bool somenteAtivos = true)
        {
            IRepositorioVeiculoMidia rep = Fabrica.Instancia.ObterRepositorio<IRepositorioVeiculoMidia>(UnidadeTrabalho);
            IQueryable<VeiculoMidia> midias = rep.ObterTodos();
            if (somenteAtivos)
                midias = midias.Where(m => m.Ativo);

            if (codVeiculo != 0)
                midias = midias.Where(m => m.Veiculo.Codigo == codVeiculo);

            VeiculoMidia[] results = midias.ToArray();

            return Json(results.Select(dm => new
            {
                Codigo = dm.Codigo,
                CodigoMidia = dm.Midia.Codigo,
                NomeOriginal = "",
                nomeDisco = dm.Midia.NomeArquivo,
                DataRegistro = dm.Midia.DataRegistro,
                Arquivo = "",
                MIME = dm.Midia.MimeType,
                Extensao = dm.Midia.Extensao,
                URL = dm.Midia.Url
            }).ToList(), JsonRequestBehavior.AllowGet);
        }

    }
}
