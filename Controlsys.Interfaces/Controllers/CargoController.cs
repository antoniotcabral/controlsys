﻿
using Controlsys.Dominio.Parametros;
using Controlsys.Infra;
using Controlsys.Repositorio.Parametros;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar cargoes.
    /// </summary>
    [ExtendController]
    public class CargoController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /Cargo/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.CargoController.
        /// </summary>
        ///
        /// <param name="unidade">
        /// O(a) unidade.
        /// </param>
        public CargoController(IUnidadeTrabalho unidade)
        {
            UnidadeTrabalho = unidade;
        }

        /// <summary>
        /// Retorna a página de cargo.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Inclui um novo cargo.
        /// </summary>
        ///
        /// <param name="cargo">
        /// O(a) cargo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Incluir(Cargo cargo)
        {
            IRepositorioCargo repCargo = Fabrica.Instancia.ObterRepositorio<IRepositorioCargo>(UnidadeTrabalho);
            cargo.DataRegistro = DateTime.Now;
            cargo.Ativar();
            repCargo.Validar(cargo, EstadoObjeto.Novo);
            repCargo.Salvar(cargo);
            return Json(cargo);

        }

        /// <summary>
        /// Altera um registro de cargo.
        /// </summary>
        ///
        /// <param name="cargo">
        /// O(a) cargo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Alterar(Cargo cargo)
        {
            IRepositorioCargo repCargo = Fabrica.Instancia.ObterRepositorio<IRepositorioCargo>(UnidadeTrabalho);
            Cargo cargoA = repCargo.ObterPorId(cargo.Codigo);
            cargoA.Descricao = cargo.Descricao;
            cargoA.Nome = cargo.Nome;
            repCargo.Validar(cargoA, EstadoObjeto.Alterado);
            repCargo.Atualizar(cargoA);
            return Json(cargoA);
        }

        /// <summary>
        /// Ativar inativar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult AtivarInativar(int codigo)
        {
            IRepositorioCargo repCargo = Fabrica.Instancia.ObterRepositorio<IRepositorioCargo>(UnidadeTrabalho);
            Cargo cargo = repCargo.ObterPorId(codigo);
            if (cargo.Ativo)
            {
                cargo.Inativar();
            }
            else
            {
                repCargo.Validar(cargo, EstadoObjeto.Ativado);
                cargo.Ativar();
            }
            repCargo.Atualizar(cargo);
            return Json(cargo);
        }

        /// <summary>
        /// Selecionar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Selecionar(int codigo)
        {
            IRepositorioCargo repCargo = Fabrica.Instancia.ObterRepositorio<IRepositorioCargo>(UnidadeTrabalho);
            Cargo cargo = repCargo.ObterPorId(codigo);
            return Json(cargo);
        }

        /// <summary>
        /// Retorna todos os objetos do tipo Cargo.
        /// </summary>
        ///
        /// <param name="apenasAtivos">
        /// true to apenas ativos.
        /// </param>
        /// <param name="descricao">
        /// O(a) descricao.
        /// </param>
        ///
        /// <returns>
        /// Detalhes do tipo de objeto ligado ao controller no formato JSON.
        /// </returns>
        public ActionResult ObterTodos(bool apenasAtivos = false, string descricao = null)
        {
            IRepositorioCargo repCargo = Fabrica.Instancia.ObterRepositorio<IRepositorioCargo>(UnidadeTrabalho);

            IQueryable<Cargo> cargos = repCargo.ObterTodos().OrderBy(p => p.Nome);

            if (apenasAtivos)
                cargos = cargos.Where(t => t.Ativo);

            if (!string.IsNullOrEmpty(descricao))
                cargos = cargos.Where(c => c.Nome.Contains(descricao));

            return Json(cargos.Select(a => new
            {
                a.Codigo,
                a.Nome,
                a.Descricao,
                a.Ativo
            }).ToList());
        }

    }
}
