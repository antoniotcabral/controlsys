﻿using Controlsys.Dominio.Parametros;
using Controlsys.Infra;
using Controlsys.Repositorio.Parametros;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [ExtendController]
    public class TipoOcorrenciaController : Controller, IControllerBase
    {

        /// <summary>
        /// GET: /TipoOcorrencia/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.TipoOcorrenciaController.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade.
        /// </param>
        public TipoOcorrenciaController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        /// <summary>
        /// Retorna a página de tipo de ocorrência.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [ExcludeFromCodeCoverage]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Inclui um novo tipo de ocorrência.
        /// </summary>
        ///
        /// <param name="tipoOcorrencia">
        /// O(a) tipo de ocorrência.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Incluir(TipoOcorrencia tipoOcorrencia)
        {
            IRepositorioTipoOcorrencia repTipoOcorrencia = Fabrica.Instancia.ObterRepositorio<IRepositorioTipoOcorrencia>(UnidadeTrabalho);
            tipoOcorrencia.DataRegistro = DateTime.Now;
            tipoOcorrencia.Ativar();
            repTipoOcorrencia.Validar(tipoOcorrencia, EstadoObjeto.Novo);
            repTipoOcorrencia.Salvar(tipoOcorrencia);
            return Json(tipoOcorrencia);

        }

        /// <summary>
        /// Altera um registro de tipo de ocorrência.
        /// </summary>
        ///
        /// <param name="tipoOcorrencia">
        /// O(a) tipo de ocorrência.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Alterar(TipoOcorrencia tipoOcorrencia)
        {
            IRepositorioTipoOcorrencia repTipoOcorrencia = Fabrica.Instancia.ObterRepositorio<IRepositorioTipoOcorrencia>(UnidadeTrabalho);
            TipoOcorrencia tipoOcorrenciaA = repTipoOcorrencia.ObterPorId(tipoOcorrencia.Codigo);
            tipoOcorrenciaA.Descricao = tipoOcorrencia.Descricao;
            repTipoOcorrencia.Validar(tipoOcorrenciaA, EstadoObjeto.Alterado);
            repTipoOcorrencia.Atualizar(tipoOcorrenciaA);
            return Json(tipoOcorrenciaA);
        }

        /// <summary>
        /// Ativar inativar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult AtivarInativar(int codigo)
        {
            IRepositorioTipoOcorrencia repTipoOcorencia = Fabrica.Instancia.ObterRepositorio<IRepositorioTipoOcorrencia>(UnidadeTrabalho);
            TipoOcorrencia tipoOcorrencia = repTipoOcorencia.ObterPorId(codigo);
            if (tipoOcorrencia.Ativo)
            {
                repTipoOcorencia.Validar(tipoOcorrencia, EstadoObjeto.Inativado);
                tipoOcorrencia.Inativar();
            }
            else
            {
                repTipoOcorencia.Validar(tipoOcorrencia, EstadoObjeto.Ativado);
                tipoOcorrencia.Ativar();
            }
            repTipoOcorencia.Atualizar(tipoOcorrencia);
            return Json(tipoOcorrencia);
        }

        /// <summary>
        /// Selecionar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Selecionar(int codigo)
        {
            IRepositorioTipoOcorrencia repTipoOcorrencia = Fabrica.Instancia.ObterRepositorio<IRepositorioTipoOcorrencia>(UnidadeTrabalho);
            TipoOcorrencia tipoOcorrencia = repTipoOcorrencia.ObterPorId(codigo);
            return Json(tipoOcorrencia);
        }

        /// <summary>
        /// Retorna todos os objetos do tipo: tipo ocorrência.
        /// </summary>
        ///
        /// <param name="apenasAtivos">
        /// true to apenas ativos.
        /// </param>
        ///
        /// <returns>
        /// Detalhes do tipo de objeto ligado ao controller no formato JSON.
        /// </returns>
        public ActionResult ObterTodos(bool apenasAtivos = false)
        {
            IRepositorioTipoOcorrencia repTipoOcorrencia = Fabrica.Instancia.ObterRepositorio<IRepositorioTipoOcorrencia>(UnidadeTrabalho);

            IQueryable<TipoOcorrencia> tipoOcorrencia = repTipoOcorrencia.ObterTodos();

            if (apenasAtivos)
            {
                tipoOcorrencia = tipoOcorrencia.Where(t => t.Ativo == true);
            }

            return Json(tipoOcorrencia.Select(tpO => new
                                        {
                                            tpO.Codigo,
                                            tpO.Descricao,
                                            tpO.Ativo
                                        })
                                .ToList());
        }

    }
}
