﻿using Controlsys.Dominio.Seguranca;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorios.Seguranca;
using Ext.Net;
using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Globalsys.Validacao;
using Controlsys.Dominio.Acesso;
using Controlsys.Repositorios.Acesso;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar grupoes.
    /// </summary>
    [ExtendController]
    public class GrupoController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /Grupo/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.GrupoController.
        /// </summary>
        ///
        /// <param name="unidade">
        /// O(a) unidade.
        /// </param>
        public GrupoController(IUnidadeTrabalho unidade) { UnidadeTrabalho = unidade; }

        /// <summary>
        /// Retorna a página de grupo.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Retorna todos os objetos do tipo Grupo.
        /// </summary>
        ///
        /// <param name="apenasAtivos">
        /// true to apenas ativos.
        /// </param>
        ///
        /// <returns>
        /// Detalhes do tipo de objeto ligado ao controller no formato JSON.
        /// </returns>
        public ActionResult ObterTodos(bool apenasAtivos = false)
        {
            IRepositorioGrupo repGrupo = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupo>(UnidadeTrabalho);

            IQueryable<Grupo> grupos = repGrupo.ObterTodos();

            if (apenasAtivos)
            {
                grupos = grupos.Where(t => t.Ativo);
            }

            return Json(grupos.Select(t => new
            {
                t.Codigo,
                t.Nome,
                t.Ativo

            }).ToList(), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Inclui um novo grupo.
        /// </summary>
        ///
        /// <param name="grupo">
        /// O(a) grupo.
        /// </param>
        /// <param name="acoes">
        /// O(a) acoes.
        /// </param>
        /// <param name="usuarios">
        /// O(a) usuarios.
        /// </param>
        /// <param name="gruposLeitoras">
        /// O(a) grupos leitoras.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Incluir(Grupo grupo, int[] acoes, string[] usuarios, int[] gruposLeitoras)
        {
            IRepositorioGrupo repGrupo = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupo>(UnidadeTrabalho);
            IRepositorioAcao repAcao = Fabrica.Instancia.ObterRepositorio<IRepositorioAcao>(UnidadeTrabalho);
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            IRepositorioPermissao repPermissao = Fabrica.Instancia.ObterRepositorio<IRepositorioPermissao>(UnidadeTrabalho);
            IRepositorioGrupoLeitora repGrupoLeitora = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoLeitora>(UnidadeTrabalho);
            IRepositorioGrupoLeitoraGrupo repGrupoLeitoraGrupo = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoLeitoraGrupo>(UnidadeTrabalho);
            IRepositorioGrupoUsuario repGrupoUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoUsuario>(UnidadeTrabalho);

            grupo.DataRegistro = DateTime.Now;
            grupo.Ativar();

            repGrupo.Validar(grupo, EstadoObjeto.Novo);
            repGrupo.Salvar(grupo);

            incluirPermissoes(grupo, acoes, repAcao, repPermissao);

            incluirGruposUsuarios(grupo, usuarios, repUsuario, repGrupoUsuario);

            if (gruposLeitoras != null)
                incluirGruposLeitoras(grupo, gruposLeitoras, repGrupoLeitora, repGrupoLeitoraGrupo);

            return Json(montaObj(grupo));
        }

        /// <summary>
        /// Incluir grupos leitoras.
        /// </summary>
        ///
        /// <param name="grupo">
        /// O(a) grupo.
        /// </param>
        /// <param name="gruposLeitoras">
        /// O(a) grupos leitoras.
        /// </param>
        /// <param name="repGrupoLeitora">
        /// O(a) rep grupo leitora.
        /// </param>
        /// <param name="repGrupoLeitoraGrupo">
        /// O(a) rep grupo leitora grupo.
        /// </param>
        private void incluirGruposLeitoras(Grupo grupo, int[] gruposLeitoras, IRepositorioGrupoLeitora repGrupoLeitora, IRepositorioGrupoLeitoraGrupo repGrupoLeitoraGrupo)
        {
            foreach (GrupoLeitora grupoLeitora in repGrupoLeitora
                                                    .ObterTodos()
                                                    .Where(gl => gruposLeitoras.Contains(gl.Codigo)))
            {
                GrupoLeitoraGrupo grupoLeitoraGrupo = new GrupoLeitoraGrupo();

                grupoLeitoraGrupo.Grupo = grupo;
                grupoLeitoraGrupo.GrupoLeitora = grupoLeitora;
                grupoLeitoraGrupo.DataRegistro = DateTime.Now;
                grupoLeitoraGrupo.Ativar();

                repGrupoLeitoraGrupo.Salvar(grupoLeitoraGrupo);
            }
        }

        /// <summary>
        /// Incluir grupos usuarios.
        /// </summary>
        ///
        /// <param name="grupo">
        /// O(a) grupo.
        /// </param>
        /// <param name="usuarios">
        /// O(a) usuarios.
        /// </param>
        /// <param name="repUsuario">
        /// O(a) rep usuario.
        /// </param>
        /// <param name="repGrupoUsuario">
        /// O(a) rep grupo usuario.
        /// </param>
        private void incluirGruposUsuarios(Grupo grupo, string[] usuarios, IRepositorioUsuario repUsuario, IRepositorioGrupoUsuario repGrupoUsuario)
        {
            foreach (Usuario usuario in repUsuario.ObterTodos().Where(u => usuarios.Contains(u.Codigo)))
            {
                GrupoUsuario grupoUsuario = new GrupoUsuario();
                grupoUsuario.Grupo = grupo;
                grupoUsuario.Usuario = usuario;
                grupoUsuario.DataRegistro = DateTime.Now;
                grupoUsuario.Ativar();

                grupo.GruposUsuarios.Add(grupoUsuario);

                repGrupoUsuario.Salvar(grupoUsuario);
            }
        }

        /// <summary>
        /// Incluir permissoes.
        /// </summary>
        ///
        /// <param name="grupo">
        /// O(a) grupo.
        /// </param>
        /// <param name="acoes">
        /// O(a) acoes.
        /// </param>
        /// <param name="repAcao">
        /// O(a) rep acao.
        /// </param>
        /// <param name="repPermissao">
        /// O(a) rep permissao.
        /// </param>
        private void incluirPermissoes(Grupo grupo, int[] acoes, IRepositorioAcao repAcao, IRepositorioPermissao repPermissao)
        {
            foreach (Acao acao in repAcao.ObterTodos().Where(a => acoes.Contains(a.Codigo)))
            {
                Permissao permissao = new Permissao();
                permissao.Acao = acao;
                permissao.Grupo = grupo;
                permissao.DataRegistro = DateTime.Now;
                permissao.Ativar();                

                repPermissao.Salvar(permissao);                
            }
        }

        /// <summary>
        /// Selecionar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Selecionar(int codigo)
        {
            IUnidadeTrabalho unidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            IRepositorioGrupo repGrupo = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupo>(UnidadeTrabalho);

            Grupo grupo = repGrupo.ObterPorId(codigo);

            return Json(montaObj(grupo));
        }

        /// <summary>
        /// Altera um registro de grupo.
        /// </summary>
        ///
        /// <param name="grupo">
        /// O(a) grupo.
        /// </param>
        /// <param name="propModificadas">
        /// O(a) property modificadas.
        /// </param>
        /// <param name="acoesNew">
        /// O(a) acoes new.
        /// </param>
        /// <param name="acoesDel">
        /// O(a) acoes delete.
        /// </param>
        /// <param name="usuariosNew">
        /// O(a) usuarios new.
        /// </param>
        /// <param name="usuariosDel">
        /// O(a) usuarios delete.
        /// </param>
        /// <param name="gruposLeitorasNew">
        /// O(a) grupos leitoras new.
        /// </param>
        /// <param name="gruposLeitorasDel">
        /// O(a) grupos leitoras delete.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Alterar(Grupo grupo, string[] propModificadas, int[] acoesNew, int[] acoesDel, string[] usuariosNew, string[] usuariosDel, int[] gruposLeitorasNew, int[] gruposLeitorasDel)
        {
            IUnidadeTrabalho unidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            IRepositorioGrupo repGrupo = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupo>(UnidadeTrabalho);
            IRepositorioAcao repAcao = Fabrica.Instancia.ObterRepositorio<IRepositorioAcao>(UnidadeTrabalho);
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            IRepositorioPermissao repPermissao = Fabrica.Instancia.ObterRepositorio<IRepositorioPermissao>(UnidadeTrabalho);
            IRepositorioGrupoUsuario repGrupoUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoUsuario>(UnidadeTrabalho);
            IRepositorioGrupoLeitora repGrupoLeitora = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoLeitora>(UnidadeTrabalho);
            IRepositorioGrupoLeitoraGrupo repGrupoLeitoraGrupo = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoLeitoraGrupo>(UnidadeTrabalho);

            grupo = repGrupo.ObterPorId(grupo, propModificadas);
            

            if (acoesNew != null)
                incluirPermissoes(grupo, acoesNew, repAcao, repPermissao);

            if (acoesDel != null){
                List<Permissao> permissoes = repPermissao.ObterTodos().Where(t => t.Grupo.Codigo == grupo.Codigo && acoesDel.Contains(t.Acao.Codigo) && t.Ativo).ToList();
                foreach (Permissao permissao in permissoes)
                {
                    permissao.Inativar();
                    repPermissao.Atualizar(permissao);
                }
            }
            if (usuariosNew != null)
                incluirGruposUsuarios(grupo, usuariosNew, repUsuario, repGrupoUsuario);

            if (usuariosDel != null)
                foreach (GrupoUsuario grupoUsuario in grupo.GruposUsuarios.Where(gu => usuariosDel.Contains(gu.Usuario.Codigo)))
                {
                    grupoUsuario.Inativar();
                    repGrupoUsuario.Atualizar(grupoUsuario);
                }

            if (gruposLeitorasNew != null)
                incluirGruposLeitoras(grupo, gruposLeitorasNew, repGrupoLeitora, repGrupoLeitoraGrupo);

            if (gruposLeitorasDel != null)
                foreach (GrupoLeitoraGrupo grupoLeitoraGrupo in grupo.GruposLeitoras.Where(ggl => gruposLeitorasDel.Contains(ggl.GrupoLeitora.Codigo)))
                {
                    grupoLeitoraGrupo.Inativar();
                    repGrupoLeitoraGrupo.Atualizar(grupoLeitoraGrupo);
                }

            repGrupo.Validar(grupo, EstadoObjeto.Alterado);
            repGrupo.Atualizar(grupo);

            return Json(montaObj(grupo));
        }

        /// <summary>
        /// Ativar inativar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult AtivarInativar(int codigo)
        {
            IUnidadeTrabalho unidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            IRepositorioGrupo repGrupo = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupo>(UnidadeTrabalho);

            Grupo grupo = repGrupo.ObterPorId(codigo);

            if (grupo.Ativo)
                grupo.Inativar();
            else
            {
                repGrupo.Validar(grupo, EstadoObjeto.Ativado);
                grupo.Ativar();
            }

            return Json(montaObj(grupo));
        }

        /// <summary>
        /// Representa um(a) Escala.
        /// </summary>
        internal class Escala
        {
           /// <summary>
           /// Gets/Sets valor para HoraTurno.
           /// </summary>
           ///
           /// <value>
           /// Retorna o(a) HoraTurno.
           /// </value>
           public Controlsys.Dominio.Parametros.HoraTurno HoraTurno { get; set; }

           /// <summary>
           /// Gets/Sets valor para GrupoTrabalho.
           /// </summary>
           ///
           /// <value>
           /// Retorna o(a) GrupoTrabalho.
           /// </value>
           public Controlsys.Dominio.Parametros.GrupoTrabalho GrupoTrabalho { get; set; }
        }

        /// <summary>
        /// Teste.
        /// </summary>
        void teste()
        {
            List<Controlsys.Dominio.Parametros.GrupoTrabalho> grupos = new List<Dominio.Parametros.GrupoTrabalho>(); //obtém todos os grupos de trabalho
            DateTime dataUltimaAtualizacao = DateTime.Now; //obtém da parametrização

            List<Escala> escalas = null;

            foreach (Controlsys.Dominio.Parametros.GrupoTrabalho grupo in grupos)
            {
                Controlsys.Dominio.Parametros.GrupoTrabalhoTurno turno = grupo.ObterTurnoTrabalho();

                IList<Controlsys.Dominio.Parametros.HoraTurno> horasTurno = turno.Turno.HorasTurno;

                Escala escala = escalas.Where(e => grupo.Codigo == grupo.Codigo).OrderBy(e => 1).First();

                //escala.HoraTurno - ordeno a partir dessa hora turno.

                //descobrir quais são os intervalos de folga para gerar o próximo dia
                foreach (Controlsys.Dominio.Parametros.HoraTurno item in horasTurno)
                {
                    if (item.Trabalha)
                    {
                        //gera escala
                    }

                    //dataEscala++
                }
            }
        }

        /// <summary>
        /// Cria um objeto contendo informações relacionadas ao conteudo gerenciado por
        /// GrupoController.
        /// </summary>
        ///
        /// <param name="grupo">
        /// O(a) grupo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) object.
        /// </returns>
        private object montaObj(Grupo grupo)
        {
            return new GrupoModelView()
            {
                Codigo = grupo.Codigo,
                Nome = grupo.Nome,
                Descricao = grupo.Descricao,
                DataDesativacao = grupo.DataDesativacao,
                Ativo = grupo.Ativo,

                GruposLeitoras = grupo.GruposLeitoras.Select(gl => gl.GrupoLeitora).ToList()
                                      .Select(gl => new GrupoLeitoraModelView()
                                      {
                                          Codigo = gl.Codigo,
                                          Nome = gl.Nome
                                      }).ToArray(),

                Usuarios = grupo.GruposUsuarios
                                .Select(gu => new GrupoUsuarioModelView()
                                {
                                    Codigo = gu.Usuario.Codigo,
                                    Nome = gu.Usuario.PessoaFisica.Nome,
                                    Login = gu.Usuario.Login
                                }).ToArray(),

                //Paginas = grupo.Permissoes.Select(pe => pe.Acao).ToList()
                Paginas = grupo.Acoes
                               .Select(pe => new GrupoPermissoesModelView()
                               {
                                   Nome = pe.Nome,
                                   Codigo = pe.Codigo,
                                   Pagina = pe.Pagina.Nome
                               }).ToArray()
            };

            #region [retornando tipo anonimo - desabilitado]
            //return new
            //{
            //    grupo.Codigo,
            //    grupo.Nome,
            //    grupo.Descricao,
            //    grupo.DataDesativacao,
            //    grupo.Ativo,

            //    GruposLeitoras = grupo.GruposLeitoras
            //                          .Select(gl => new
            //                          {
            //                              gl.GrupoLeitora.Codigo,
            //                              gl.GrupoLeitora.Nome
            //                          }).ToArray(),

            //    Usuarios = grupo.GruposUsuarios
            //                    .Select(gu => new
            //                    {
            //                        gu.Usuario.Codigo,
            //                        gu.Usuario.PessoaFisica.Nome,
            //                        gu.Usuario.Login
            //                    }).ToArray(),

            //    Paginas = grupo.Permissoes
            //                   .Select(pe => new
            //                   {
            //                       pe.Acao.Nome,
            //                       pe.Acao.Codigo,
            //                       Pagina = pe.Acao.Pagina.Nome
            //                   }).ToArray()
            //}; 
            #endregion
        }
    }
}
