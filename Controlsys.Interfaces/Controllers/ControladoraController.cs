﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Globalsys;
using Controlsys.Dominio.Acesso;
using Controlsys.Infra;
using Controlsys.Repositorios.Acesso;
using Globalsys.Validacao;
using Controlsys.Dominio.Seguranca;
using Controlsys.Repositorios.Seguranca;
using Controlsys.Interfaces.Models;
using Globalsys.Exceptions;
using Controlsys.Repositorio.Acesso;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar controladoras.
    /// </summary>
    [ExtendController]
    public class ControladoraController : Controller, IControllerBase
    {
        /// <summary>
        /// Gets/Sets valor para UnidadeTrabalho.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.ControladoraController.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public ControladoraController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        /// <summary>
        /// GET: /Controladora/.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Lista tipos controladora.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ListaTiposControladora()
        {
            Dictionary<int, string> dic = new Dictionary<int, string>();

            foreach (TipoControladora tpControladora in Enum.GetValues(typeof(TipoControladora)))
            {
                dic.Add((int)tpControladora, tpControladora.ObterDescricaoEnum());
            }

            return Json(dic.Select(d => new
                                        {
                                            Codigo = d.Key,
                                            Descricao = d.Value
                                        })
                           .ToArray());
        }

        /// <summary>
        /// Inclui um novo controladora.
        /// </summary>
        ///
        /// <param name="controladora">
        /// O(a) controladora.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Incluir(Controladora controladora, int[] camera = null)
        {
            IRepositorioControladora repControladora = Fabrica.Instancia.ObterRepositorio<IRepositorioControladora>(UnidadeTrabalho);
            IRepositorioLeitora repLeitora = Fabrica.Instancia.ObterRepositorio<IRepositorioLeitora>(UnidadeTrabalho);

            controladora.DataRegistro = DateTime.Now;
            controladora.AlterarStatus(StatusControladora.Ativo);

            repControladora.Salvar(controladora);            

            foreach (Leitora leitora in controladora.Leitoras)
            {
                leitora.Ativar();
                leitora.Controladora = controladora;
                leitora.DataRegistro = DateTime.Now;                
                repLeitora.Salvar(leitora);                
            }

            repControladora.Validar(controladora, EstadoObjeto.Novo);

            AlterarCameraControladora(camera, controladora);

            return Json(formatObject(controladora));
        }

        /// <summary>
        /// Altera um registro de controladora.
        /// </summary>
        ///
        /// <param name="controladora">
        /// O(a) controladora.
        /// </param>
        /// <param name="modifProperties">
        /// O(a) modif properties.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Alterar(Controladora controladora, string[] modifProperties, int[] camera = null)
        {
            IRepositorioControladora repControladora = Fabrica.Instancia.ObterRepositorio<IRepositorioControladora>(UnidadeTrabalho);
            IRepositorioLeitora repLeitora = Fabrica.Instancia.ObterRepositorio<IRepositorioLeitora>(UnidadeTrabalho);

            var controladoraDB = repControladora.ObterPorId(controladora, modifProperties);
            controladoraDB.ValidaOCR = controladora.ValidaOCR;
            repControladora.Validar(controladoraDB, EstadoObjeto.Alterado);
            repControladora.Atualizar(controladoraDB);
            if (controladora.Ativo)
            {
                controladoraDB.AlterarStatus(StatusControladora.Ativo);
            }
            else
            {
                controladoraDB.AlterarStatus(StatusControladora.Inativo);
            }

            // if (modifProperties.Any(a => a == "Camera"))
            //{
            AlterarCameraControladora(camera, controladora);
            //}

            return Json(formatObject(controladora));
        }
        
        public void AlterarCameraControladora(int[] pcamera, Controladora controladora)
        {
            IRepositorioCamera repCamera = Fabrica.Instancia.ObterRepositorio<IRepositorioCamera>(UnidadeTrabalho);

            List<int> CamerasControladora = new List<int>();

            if (pcamera != null)
            {
                for (int i = 0; i < pcamera.Count(); i++)
                {
                    CamerasControladora.Add(pcamera[i]);
                }
            }

            if (CamerasControladora != null && CamerasControladora.Count > 0)
            {
                if (CamerasControladora.Count() > 2)
                    throw new CoreException("Não é permitido selecionar mais de duas câmeras. ");

                var Cameras = repCamera.ObterTodos().Where(c => c.Controladora.Codigo == controladora.Codigo).ToList();

                for (int x = 0; x < Cameras.Count; x++)
                {
                    for (int i = 0; i < CamerasControladora.Count; i++)
                    {
                        if (CamerasControladora[i] == Cameras[x].Codigo)
                        {
                            Cameras.RemoveAt(x);
                            CamerasControladora.RemoveAt(i);
                        }
                    }
                }

                for (int i = 0; i < Cameras.Count; i++)
                {
                    Cameras[i].Controladora = null;
                    repCamera.Atualizar(Cameras[i]);
                }


                for (int i = 0; i < CamerasControladora.Count; i++)
                {
                    var camera = repCamera.ObterTodos().Where(c => c.Codigo == CamerasControladora[i]).SingleOrDefault();
                    camera.Controladora = controladora;
                    repCamera.Atualizar(camera);
                }
            }
            else
            {
                var Cameras = repCamera.ObterTodos().Where(c => c.Controladora.Codigo == controladora.Codigo).ToList();
                for (int i = 0; i < Cameras.Count; i++)
                {
                    Cameras[i].Controladora = null;
                    repCamera.Atualizar(Cameras[i]);
                }
            }
        }

        /// <summary>
        /// Selecionar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Selecionar(int codigo)
        {
            IRepositorioControladora repControladora = Fabrica.Instancia.ObterRepositorio<IRepositorioControladora>(UnidadeTrabalho);
            IRepositorioSincronizaPessoaAcesso repSincAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioSincronizaPessoaAcesso>(UnidadeTrabalho);

            Controladora controladora = repControladora.ObterPorId(codigo);

            SincronizaPessoaAcesso sincAcesso = repSincAcesso.ObterUltimaSincronizacao(controladora.Codigo);

            return Json(formatObject(controladora, sincAcesso, true));
        }

        /// <summary>
        /// Ativar inativar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult AtivarInativar(int codigo)
        {
            IRepositorioControladora repControladora = Fabrica.Instancia.ObterRepositorio<IRepositorioControladora>(UnidadeTrabalho);
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);

            Controladora controladora = repControladora.ObterPorId(codigo);

            StatusControladora status =StatusControladora.Ativo;

            if (controladora.Ativo)
                status = StatusControladora.Inativo;

            Usuario usuario = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name);

            repControladora.AlterarStatus(controladora, status, usuario);

            return Json(formatObject(controladora));
        }
       /// <summary>
       /// 
       /// </summary>
       /// <param name="codigo"></param>
       /// <param name="ativarInativar"></param>
       /// <returns></returns>
        [Transaction]
        public ActionResult AtivarInativarIntegradorAvigilon(int codigo, bool ativarInativar)
        {
            IRepositorioControladora repControladora = Fabrica.Instancia.ObterRepositorio<IRepositorioControladora>(UnidadeTrabalho);
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);

            Controladora controladora = repControladora.ObterPorId(codigo);
            controladora.DataDesativacao = DateTime.Now;
            StatusControladora status = StatusControladora.Ativo;

            if (ativarInativar)
            {
                controladora.Ativo = true;
                status = StatusControladora.Ativo;
                //Usuario usuario = repUsuario.ObterPorLogin("adm.desenv");

                repControladora.AlterarStatusControladoraIntegrador(controladora, status, null);
                UnidadeTrabalho.BeginTransaction();
                UnidadeTrabalho.Commit();
                UnidadeTrabalho.Dispose();
            }
            else
            {
                //if (controladora.DataDesativacao == null || controladora.DataDesativacao < DateTime.Now.AddMinutes(-1))
                //{
                    controladora.DataDesativacao = DateTime.Now;
                    controladora.Ativo = false;
                    status = StatusControladora.Inativo;

                    //Usuario usuario = repUsuario.ObterPorLogin("adm.desenv");

                    repControladora.AlterarStatusControladoraIntegrador(controladora, status, null);

                    UnidadeTrabalho.BeginTransaction();
                    UnidadeTrabalho.Commit();
                    UnidadeTrabalho.Dispose();

                    //}
            }

            return Json("");
        }

        /// <summary>
        /// Retorna todos os objetos do tipo Controladora.
        /// </summary>
        ///
        /// <param name="apenasAtivos">
        /// true to apenas ativos.
        /// </param>
        ///
        /// <returns>
        /// Detalhes do tipo de objeto ligado ao controller no formato JSON.
        /// </returns>
        public ActionResult ObterTodos(bool apenasAtivos = false)
        {
            IRepositorioControladora repControladora = Fabrica.Instancia.ObterRepositorio<IRepositorioControladora>(UnidadeTrabalho);

            IQueryable<Controladora> controladoras = repControladora.ObterTodos();

            if (apenasAtivos)
                controladoras = controladoras.Where(c => c.Ativo);

            return Json(controladoras.ToList().Select(c => formatObject(c, null)).ToArray());
        }

        /// <summary>
        /// Format object.
        /// </summary>
        ///
        /// <param name="controladora">
        /// O(a) controladora.
        /// </param>
        /// <param name="carregarLeitoras">
        /// true to carregar leitoras.
        /// </param>
        ///
        /// <returns>
        /// Um(a) object.
        /// </returns>
        private ControladoraModelView formatObject(Controladora controladora, SincronizaPessoaAcesso sincAcesso = null, bool carregarLeitoras = false)
        {
            IRepositorioCamera repCamera = Fabrica.Instancia.ObterRepositorio<IRepositorioCamera>(UnidadeTrabalho);            

            return new ControladoraModelView
            {
                Codigo = controladora.Codigo,
                Ativo = controladora.Ativo,
                DataRegistro = controladora.DataRegistro,
                Modelo = controladora.Modelo,
                IP = controladora.IP,
                TipoControladora = controladora.TipoControladora,
                TipoControladoraDesc = controladora.TipoControladora.ObterDescricaoEnum(),
                HoraSincronizacao = controladora.HoraSincronizacao.HasValue ? controladora.HoraSincronizacao.Value.ToString("hh\\:mm") : null,
                Tensao = controladora.Tensao,
                Temperatura = controladora.Temperatura,
                Corrente = controladora.Corrente,
                Online = controladora.Online ? "SIM" : "NÃO",
                Status = controladora.Status.HasValue ? controladora.Status.ObterDescricaoEnum() : null,
                QtdMaxListaDesordenada = controladora.QtdMaxListaDesordenada,
                QtdMaxListaOrdenada = controladora.QtdMaxListaOrdenada,
                QtdListaDesordenada = controladora.QtdListaDesordenada,
                QtdListaOrdenada = controladora.QtdListaOrdenada,
                ValidaOCR = controladora.ValidaOCR,

                UltimaSincronizacao = sincAcesso != null ? string.Format("Última sincronização de lista concluída em {0}", sincAcesso.DataSincronizacao.Value.ToString("dd/MM/yyyy HH:mm")) : null,

                //cameras = repCamera.ObterTodos().Where(c => c.Controladora.Codigo == controladora.Codigo).ToList(),
                cameras = repCamera.ObterTodos().Where(c => c.Controladora.Codigo == controladora.Codigo).Select(c => new CameraModelView
                                                    {
                                                        Codigo = c.Codigo,
                                                        Nome = c.Nome,
                                                        Modelo= c.Modelo,
                                                        Descricao = c.Descricao,
                                                        DataRegistro = c.DataRegistro,
                                                        Ativo = c.Ativo,
                                                        DataStatus = c.DataStatus,
                                                        Precisao = c.Precisao
                                                    }).ToArray(),

                Leitoras = carregarLeitoras ? controladora.Leitoras
                                                          .Select(l => new LeitoraModelView
                                                            {
                                                                Codigo = l.Codigo,
                                                                Direcao = l.Direcao,
                                                                Descricao = l.Descricao,
                                                                DirecaoDesc = l.Direcao.ObterDescricaoEnum(),
                                                                //l.TipoLeitora,
                                                                //TipoLeitoraDesc = l.TipoLeitora.ObterDescricaoEnum(),
                                                                Fabricante = l.Fabricante,
                                                                FabricanteDesc = l.Fabricante.ObterDescricaoEnum(),
                                                                Senha = l.Senha,
                                                                Ordem = l.Ordem,
                                                                Local = l.Local,
                                                                IDLeitora = l.IDLeitora,
                                                                EmiteBaixaCracha = l.EmiteBaixaCracha
                                                            }).ToArray()
                                             : null
            };
        }
    }
}
