﻿using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Enderecos;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Empresas;
using Controlsys.Repositorio.Parametros;
using Controlsys.Repositorios.Empresas;
using Controlsys.Repositorios.Enderecos;
using Controlsys.Repositorios.Parametros;
using Controlsys.Repositorios.Pessoas;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar empresas.
    /// </summary>
    [ExtendController]
    public class EmpresaController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /Empresa/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.EmpresaController.
        /// </summary>
        ///
        /// <param name="unidade">
        /// O(a) unidade.
        /// </param>
        public EmpresaController(IUnidadeTrabalho unidade)
        {
            UnidadeTrabalho = unidade;
        }

        /// <summary>
        /// Retorna a página de empresa.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Obter todos cnae.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ObterTodosCNAE()
        {
            IRepositorioEmpresa rep = Fabrica.Instancia.ObterRepositorio<IRepositorioEmpresa>(UnidadeTrabalho);
            return Json(rep.ObterTodosCNAE()
                               .Select(s => new
                                {
                                    Codigo = s.Codigo,
                                    Nome = string.Format("{0} - {1}", s.Codigo, s.Nome)
                                })
                                .ToList(), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Inclui um novo empresa.
        /// </summary>
        ///
        /// <param name="empresa">
        /// O(a) empresa.
        /// </param>
        /// <param name="cnaes">
        /// O(a) cnaes.
        /// </param>
        /// <param name="documentosMidias">
        /// O(a) documentos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Incluir(Empresa empresa, List<CNAE> cnaes, List<DocumentoMidiaModelView> documentosMidias, List<Sindicato> sindicatos)
        {
            UnidadeTrabalho.BeginTransaction();

            IRepositorioEmpresa rep = Fabrica.Instancia.ObterRepositorio<IRepositorioEmpresa>(UnidadeTrabalho);
            IRepositorioDocumento repDoc = Fabrica.Instancia.ObterRepositorio<IRepositorioDocumento>(UnidadeTrabalho);
            empresa.Ativar();
            empresa.DataRegistro = DateTime.Now;
            empresa.GerarCodigoPessoa(rep.ObterTodos());
            empresa.CNPJ = empresa.CNPJ.Replace(".", "").Replace("-", "").Replace("/", "");
            empresa.EmpresaSAP = empresa.EmpresaSAP;
            rep.Validar(empresa, EstadoObjeto.Novo);

            //endereço
            InserirEndereco(empresa.Endereco);

            if (documentosMidias != null)
            {
                int documentoCompleto = 0;
                foreach (var item in documentosMidias)
                {
                    if (item.Documento.Status == 0 || item.Documento.Status == StatusDocumento.NaoEntregue || item.Documento.Status == StatusDocumento.Incompleto)
                        documentoCompleto++;
                }
                if (documentoCompleto > 0)
                    empresa.Inativar();
            }

            rep.Salvar(empresa);

            //cnae
            foreach (var item in cnaes)
            {
                EmpresaCNAE empresaCnae = new EmpresaCNAE();
                empresaCnae.Empresa = empresa;
                empresaCnae.CNAE = item;
                empresaCnae.DataRegistro = DateTime.Now;
                empresaCnae.Ativar();
                rep.SalvarEmpresaCnae(empresaCnae);
            }

            //documento

            empresa.Documentos = new List<DocumentoEmpresa>();
            if (documentosMidias != null)
            {
                foreach (var item in documentosMidias)
                {
                    // Se Documentos com status completo não tiverem midia gera erro.
                    if (item.Documento.Status == StatusDocumento.Completo && (item.Midias == null || !item.Midias.Any(m => m != null)))
                    {
                        throw new CoreException("Não é possível salvar documento com status 'Completo' sem mídia anexada.");
                    }

                    DocumentoEmpresa docEmpresa = repDoc.SalvarDocEmpresa(empresa, item.Documento);

                    string path = ConfigurationManager.AppSettings.Get("pathUploads");

                    if (!System.IO.Directory.Exists(path))
                        throw new CoreException("O diretório onde a midia seria armazanada não está acessivel.");

                    IRepositorioDocumentoMidia repDocumentoMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioDocumentoMidia>(UnidadeTrabalho);
                    IRepositorioMidia repMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioMidia>(UnidadeTrabalho);

                    int cont = 1;

                    foreach (MidiaModelView midia in item.Midias ?? Enumerable.Empty<MidiaModelView>())
                    {
                        if (midia == null)
                            continue;
                        string nomeDisco = "PJ-" + empresa.CNPJ + "-" + docEmpresa.Documento.TipoDocumento.Sigla + "-" + cont + "." + midia.Extensao;
                        Midia m = new Midia();
                        m.DataRegistro = midia.DataRegistro;
                        m.Extensao = midia.Extensao;
                        m.MimeType = midia.MIME;
                        m.NomeArquivo = nomeDisco;
                        repMidia.Salvar(m);

                        repDocumentoMidia.SalvarDocMidia(docEmpresa.Documento, m);

                        System.IO.File.WriteAllBytes(m.Url, Convert.FromBase64String(midia.Arquivo));

                        cont++;
                    }

                    empresa.Documentos.Add(docEmpresa);
                }
            }
            //telefones
            List<Telefone> telefones = empresa.Telefones.ToList();
            if (telefones != null && telefones.Count > 0)
            {
                empresa.Telefones = new List<Telefone>();
                foreach (Telefone telefone in telefones)
                {
                    telefone.Pessoa = empresa;
                    telefone.TelefoneNum = telefone.TelefoneNum.ToString().Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "");
                    telefone.TipoTelefone = telefone.TipoTelefone;
                    telefone.DataRegistro = DateTime.Now;

                    InserirTelefone(telefone);
                    empresa.Telefones.Add(telefone);
                }
            }

            if (sindicatos != null && sindicatos.Count > 0)
            {
                IRepositorioEmpresaSindicato repEmpresaSindicato = Fabrica.Instancia.ObterRepositorio<IRepositorioEmpresaSindicato>(UnidadeTrabalho);

                foreach (Sindicato sindicato in sindicatos)
                {
                    EmpresaSindicato esNovo = new EmpresaSindicato();
                    esNovo.Ativar();
                    esNovo.DataRegistro = DateTime.Now;
                    esNovo.Empresa = empresa;
                    esNovo.Sindicato = sindicato;

                    repEmpresaSindicato.Validar(esNovo, EstadoObjeto.Novo);
                    repEmpresaSindicato.Salvar(esNovo);
                }
            }
            return Json(montaObj(empresa));
        }

        /// <summary>
        /// Inserir telefone.
        /// </summary>
        ///
        /// <param name="telefone">
        /// O(a) telefone.
        /// </param>
        private void InserirTelefone(Telefone telefone)
        {
            IRepositorioTelefone repTel = Fabrica.Instancia.ObterRepositorio<IRepositorioTelefone>(UnidadeTrabalho);

            repTel.Salvar(telefone);
        }

        /// <summary>
        /// Inserir endereco.
        /// </summary>
        ///
        /// <param name="endereco">
        /// O(a) endereco.
        /// </param>
        private void InserirEndereco(Endereco endereco)
        {
            IRepositorioEndereco repEnd = Fabrica.Instancia.ObterRepositorio<IRepositorioEndereco>(UnidadeTrabalho);
            endereco.DataRegistro = DateTime.Now;
            endereco.Ativar();
            endereco.Logradouro.CEP = endereco.Logradouro.CEP.Replace(".", "").Replace("-", "");
            endereco = repEnd.VerificarEndereco(endereco);
            repEnd.Salvar(endereco);
        }

        /// <summary>
        /// Cria um objeto contendo informações relacionadas ao conteudo gerenciado por
        /// EmpresaController.
        /// </summary>
        ///
        /// <param name="empresa">
        /// O(a) empresa.
        /// </param>
        ///
        /// <returns>
        /// Um(a) object.
        /// </returns>
        private object montaObj(Empresa empresa)
        {
            Telefone telResidencial = empresa.Telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Residencial);
            Telefone telEmergencial = empresa.Telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Emergencial);
            Telefone telCelular = empresa.Telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Celular);

            var empModelView = new EmpresaModelView();
            empModelView.Codigo = empresa.Codigo;
            empModelView.Nome = empresa.Nome;
            empModelView.Apelido = empresa.Apelido;
            empModelView.CNPJ = empresa.CNPJFormatado();
            empModelView.InscEstadual = empresa.InscEstadual;
            empModelView.InscMunicipal = empresa.InscMunicipal;
            empModelView.Email = empresa.Email;
            empModelView.Ativo = empresa.Ativo;
            empModelView.CalculoFTE = empresa.CalculoFte;
            empModelView.ExibirRelatorioFTE = empresa.ExibirRelatorioFTE;
            empModelView.BloqColabSemPedido = empresa.BloqColabSemPedido;
            empModelView.BloqColabSemPedido = empresa.BloqColabSemPedido;
            empModelView.EmpresaSAP = empresa.EmpresaSAP;

            empModelView.CNAEs = empresa.CNAEs.Select(td => new CNAEModelView()
            {
                Codigo = td.CNAE.Codigo,
                Nome = td.CNAE.Nome
            }).ToList();
            empModelView.Documentos = empresa.Documentos.Select(d => new DocumentoModelView()
            {
                CodigoDocumento = d.Documento.Codigo,
                Codigo = d.Documento.TipoDocumento.Codigo,
                Sigla = d.Documento.TipoDocumento.Sigla,
                Descricao = d.Documento.Descricao,
                DataEmissao = (d.Documento.DataEmissao.HasValue ? d.Documento.DataEmissao : null),
                DataValidade = (d.Documento.DataValidade.HasValue ? d.Documento.DataValidade : null),
                Status = d.Documento.Status != null && d.Documento.Status > 0 ? obterDesc(d.Documento.Status).ToString() : string.Empty,
                QtdMidias = d.Documento.Midias.Count(m => m.Ativo),
                Midias = d.Documento.Midias.Select(dm => new MidiaModelView()
                {
                    CodigoMidia = dm.Midia.Codigo,
                    Arquivo = string.Empty,
                    DataRegistro = dm.Midia.DataRegistro,
                    Extensao = dm.Midia.Extensao,
                    MIME = dm.Midia.MimeType,
                    NomeDisco = dm.Midia.NomeArquivo,
                    NomeOriginal = string.Empty,
                    URL = dm.Midia.Url
                }).ToList()
            }).ToList();

            //telefone
            empModelView.TelefoneResidencial = (telResidencial != null ? string.Format("{0:(##) ####-####}", telResidencial.TelefoneNum) : null);
            empModelView.TelefoneEmergencial = (telEmergencial != null ? string.Format("{0:(##) ####-####}", telEmergencial.TelefoneNum) : null);
            empModelView.TelefoneCelular = (telCelular != null ? string.Format("{0:(##) ####-####}", telCelular.TelefoneNum) : null);

            // endereço
            if (empresa.Endereco != null)
            {
                if (!string.IsNullOrEmpty(empresa.Endereco.Complemento) && empresa.Endereco.Logradouro == null)
                {
                    empModelView.Endereco = new EnderecoMovelView()
                      {
                          CodigoEndereco = empresa.Endereco.Codigo,
                          Complemento = empresa.Endereco.Complemento
                      };
                }
                else
                {
                    empModelView.Endereco = new EnderecoMovelView()
                    {
                        CodigoEndereco = empresa.Endereco.Codigo,
                        CodigoLog = empresa.Endereco.Logradouro.Codigo,
                        Pais = empresa.Endereco.Logradouro.Bairro.Cidade.Estado.Pais.Nome,
                        Estado = empresa.Endereco.Logradouro.Bairro.Cidade.Estado.Nome,
                        Cidade = empresa.Endereco.Logradouro.Bairro.Cidade.Nome,
                        Bairro = empresa.Endereco.Logradouro.Bairro.Nome,
                        Logradouro = empresa.Endereco.Logradouro.Nome,
                        Numero = empresa.Endereco.Numero,
                        CEP = empresa.Endereco.Logradouro.CEP,
                        Complemento = empresa.Endereco.Complemento
                    };
                }
            }

            if (empresa.Sindicatos != null)
            {
                empModelView.Sindicatos = empresa.Sindicatos.Select(s => new SindicatoModelView
                {
                    Apelido = s.Sindicato.Nome,
                    Ativo = s.Sindicato.Ativo,
                    Celular = TelefoneFormatado(s.Sindicato.Celular),
                    Codigo = s.Sindicato.Codigo,
                    CNPJ = s.Sindicato.CNPJFormatado(),
                    DataDesativacao = s.Sindicato.DataDesativacao.HasValue ? s.Sindicato.DataDesativacao.Value.ToString("dd/MM/yyyy") : null,
                    DataRegistro = s.Sindicato.DataRegistro.ToString("dd/MM/yyyy"),
                    Email = s.Sindicato.Email,
                    Nome = s.Sindicato.Razao,
                    Telefone = TelefoneFormatado(s.Sindicato.Telefone),
                    TelEmergencia = TelefoneFormatado(s.Sindicato.TelEmergencia)
                }).ToList();
            }

            return empModelView;

            #region [retorna tipo anonimo - desabilitado]

            //return new
            //{
            //    Codigo = empresa.Codigo,
            //    Nome = empresa.Nome,
            //    Apelido = empresa.Apelido,
            //    CNPJ = empresa.CNPJFormatado(),
            //    InscEstadual = empresa.InscEstadual,
            //    InscMunicipal = empresa.InscMunicipal,
            //    Email = empresa.Email,
            //    NaturezaJuridica = empresa.NaturezaJuridica,
            //    Ativo = empresa.Ativo,
            //    CNAEs = empresa.CNAEs.Select(td => new
            //                             {
            //                                 Codigo = td.CNAE.Codigo,
            //                                 Nome = td.CNAE.Nome
            //                             }).ToList(),
            //    Documentos = empresa.Documentos.Select(d => new
            //                            {
            //                                Codigo = d.Documento.Codigo,
            //                                Sigla = d.Documento.TipoDocumento.Sigla,
            //                                Descricao = d.Documento.Descricao,
            //                                DiasValidade = d.Documento.TipoDocumento.DiasValidade,
            //                                Status = obterDesc(d.Documento.Status)
            //                            }).ToList(),
            //    //telefone
            //    TelefoneResidencial = (telResidencial != null ? string.Format("{0:(##) ####-####}", telResidencial.TelefoneNum) : null),
            //    TelefoneEmergencial = (telEmergencial != null ? string.Format("{0:(##) ####-####}", telEmergencial.TelefoneNum) : null),
            //    TelefoneCelular = (telCelular != null ? string.Format("{0:(##) ####-####}", telCelular.TelefoneNum) : null),

            //    // endereço
            //    Endereco = new
            //    {
            //        Codigo = empresa.Endereco.Codigo,
            //        CodigoLog = empresa.Endereco.Logradouro.Codigo,
            //        Pais = empresa.Endereco.Logradouro.Bairro.Cidade.Estado.Pais.Nome,
            //        Estado = empresa.Endereco.Logradouro.Bairro.Cidade.Estado.Nome,
            //        Cidade = empresa.Endereco.Logradouro.Bairro.Cidade.Nome,
            //        Bairro = empresa.Endereco.Logradouro.Bairro.Nome,
            //        Logradouro = empresa.Endereco.Logradouro.Nome,
            //        Numero = empresa.Endereco.Numero,
            //        CEP = empresa.Endereco.Logradouro.CEP,
            //        Complemento = empresa.Endereco.Complemento
            //    }
            //}; 

            #endregion
        }

        /// <summary>
        /// Obter description.
        /// </summary>
        ///
        /// <param name="statusDocumento">
        /// O(a) status documento.
        /// </param>
        ///
        /// <returns>
        /// Um(a) object.
        /// </returns>
        private object obterDesc(StatusDocumento statusDocumento)
        {
            return EnumExtensoes.ObterDescricaoEnum(statusDocumento);
        }

        /// <summary>
        /// Lista status.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ListaStatus(bool incluirStatusPreCadastro = false)
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();

            foreach (StatusDocumento item in Enum.GetValues(typeof(StatusDocumento)))
            {
                if (!incluirStatusPreCadastro && item != StatusDocumento.PreCadastro)
                    dic.Add(item.ToString(), item.ObterDescricaoEnum());
            }
            return Json(dic.Select(d => new
            {
                Codigo = d.Key,
                Descricao = d.Value
            }).ToList());

        }

        /// <summary>
        /// Retorna todos os objetos do tipo Empresa.
        /// </summary>
        ///
        /// <param name="descricao">
        /// O(a) descricao.
        /// </param>
        /// <param name="apenasAtivos">
        /// true to apenas ativos.
        /// </param>
        /// <param name="verificarDocumentacao">
        /// true to verificar documentacao.
        /// </param>
        ///
        /// <returns>
        /// Detalhes do tipo de objeto ligado ao controller no formato JSON.
        /// </returns>
        public ActionResult ObterTodos(string descricao = null, bool apenasAtivos = false, bool verificarDocumentacao = false, int? codigoPO = null, bool apenasCodigoENome = false, bool apenasCodigoENomeCNPJ = false)
        {
            IRepositorioEmpresa rep = Fabrica.Instancia.ObterRepositorio<IRepositorioEmpresa>(UnidadeTrabalho);
            IQueryable<Empresa> empresas = null;
            if (codigoPO.HasValue)
            {
                IRepositorioPedidoCompra repPO = Fabrica.Instancia.ObterRepositorio<IRepositorioPedidoCompra>(UnidadeTrabalho);
                empresas = repPO.ObterEmpresasRelacionadasPO(codigoPO.Value).OrderBy(e => e.Nome).AsQueryable();
            }
            else
                empresas = rep.ObterTodos().OrderBy(e => e.Nome);

            if (apenasAtivos)
                empresas = empresas.Where(e => e.Ativo);

            if (verificarDocumentacao)
                empresas = empresas.Where(e => !e.Documentos.Any(d => d.Documento.Status != StatusDocumento.Completo));

            if (!string.IsNullOrEmpty(descricao))
                empresas = empresas.Where(e => e.Nome.Contains(descricao));

            if (apenasCodigoENome)
            {

                return Json(empresas.Select(s => new
                {
                    Codigo = s.Codigo,                    
                    Nome = s.Nome
                }).ToList());

            }

            if (apenasCodigoENomeCNPJ)
            {

                return Json(empresas.Select(s => new
                {
                    Codigo = s.Codigo,
                    Nome = s.Nome + " (" + s.CNPJ + ")"                    
                }).ToList());

            }

            return Json(empresas.Select(s => new
            {
                Codigo = s.Codigo,
                CNPJ = s.CNPJFormatado(),
                Nome = s.Nome,
                NomeCNPJ = s.Nome + " (" + s.CNPJ + ")",
                Apelido = s.Apelido,
                ApelidoCNPJ = s.Apelido + " (" + s.CNPJ + ")",
                Ativo = s.Ativo
            }).ToList());
        }       

        /// <summary>
        /// 
        /// </summary>
        /// <param name="descricao"></param>
        /// <param name="apenasAtivos"></param>
        /// <param name="verificarDocumentacao"></param>
        /// <param name="codigoPO"></param>
        /// <param name="apenasCodigoENome"></param>
        /// <returns></returns>
        public ActionResult ObterTodosFte(string descricao = null, bool apenasAtivos = false, bool verificarDocumentacao = false, int? codigoPO = null, bool apenasCodigoENome = false)
        {
            IRepositorioEmpresa rep = Fabrica.Instancia.ObterRepositorio<IRepositorioEmpresa>(UnidadeTrabalho);
            IQueryable<Empresa> empresas = rep.ObterTodos();
            if (codigoPO.HasValue)
            {
                IRepositorioPedidoCompra repPO = Fabrica.Instancia.ObterRepositorio<IRepositorioPedidoCompra>(UnidadeTrabalho);
                empresas = repPO.ObterEmpresasRelacionadasPO(codigoPO.Value)
                    //.Where(f => f.CalculoFte)
                    .OrderBy(e => e.Nome).AsQueryable();
            }
            //else
            //    empresas = rep.ObterTodos().Where(f => f.CalculoFte).OrderBy(e => e.Nome);

            empresas = empresas.Where(x => x.ExibirRelatorioFTE);

            if (apenasAtivos)
                empresas = empresas.Where(e => e.Ativo);

            if (verificarDocumentacao)
                empresas = empresas.Where(e => !e.Documentos.Any(d => d.Documento.Status != StatusDocumento.Completo));

            if (!string.IsNullOrEmpty(descricao))
                empresas = empresas.Where(e => e.Nome.Contains(descricao));            

            if (apenasCodigoENome)
            {

                return Json(empresas.Select(s => new
                {
                    Codigo = s.Codigo,
                    Nome = s.Nome
                }).ToList());

            }

            return Json(empresas.Select(s => new
            {
                Codigo = s.Codigo,
                CNPJ = s.CNPJFormatado(),
                Nome = s.Nome,
                NomeCNPJ = s.Nome + " (" + s.CNPJ + ")",
                Apelido = s.Apelido,
                Ativo = s.Ativo
            }).ToList());
        }
        /// <summary>
        /// Selecionar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Selecionar(string codigo)
        {
            IRepositorioEmpresa rep = Fabrica.Instancia.ObterRepositorio<IRepositorioEmpresa>(UnidadeTrabalho);
            Empresa emp = rep.ObterPorId(codigo);
            return Json(montaObj(emp));
        }

        /// <summary>
        /// Altera um registro de empresa.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="empresaNew">
        /// O(a) empresa new.
        /// </param>
        /// <param name="cnaesDel">
        /// O(a) cnaes delete.
        /// </param>
        /// <param name="cnaesNew">
        /// O(a) cnaes new.
        /// </param>
        /// <param name="documentos">
        /// O(a) documentos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Alterar(Empresa empresaNew, CNAE[] cnaesDel, CNAE[] cnaesNew, List<DocumentoMidiaModelView> documentosMidias, List<Sindicato> sindicatos)
        {
            IRepositorioEmpresa repEmpresa = Fabrica.Instancia.ObterRepositorio<IRepositorioEmpresa>(UnidadeTrabalho);
            IRepositorioDocumento repDoc = Fabrica.Instancia.ObterRepositorio<IRepositorioDocumento>(UnidadeTrabalho);
            IRepositorioEndereco repEnd = Fabrica.Instancia.ObterRepositorio<IRepositorioEndereco>(UnidadeTrabalho);
            IRepositorioTelefone repTel = Fabrica.Instancia.ObterRepositorio<IRepositorioTelefone>(UnidadeTrabalho);
            IRepositorioPedidoCompra repPedidoCompra = Fabrica.Instancia.ObterRepositorio<IRepositorioPedidoCompra>(UnidadeTrabalho);
            IRepositorioDocumentoMidia repDocumentoMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioDocumentoMidia>(UnidadeTrabalho);
            IRepositorioMidia repMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioMidia>(UnidadeTrabalho);

            Empresa empresa = repEmpresa.ObterPorId(empresaNew.Codigo);
            empresa.Nome = empresaNew.Nome;
            empresa.Apelido = empresaNew.Apelido;
            empresa.InscEstadual = empresaNew.InscEstadual;
            empresa.InscMunicipal = empresaNew.InscMunicipal;
            empresa.Email = empresaNew.Email;
            empresa.CalculoFte = empresaNew.CalculoFte;
            empresa.ExibirRelatorioFTE = empresaNew.ExibirRelatorioFTE;
            empresa.BloqColabSemPedido = empresaNew.BloqColabSemPedido;
            empresa.EmpresaSAP = empresaNew.EmpresaSAP;
            //empresa.NaturezaJuridica = empresaNew.NaturezaJuridica;

            Telefone telResidencialNew = empresaNew.Telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Residencial);
            Telefone telEmergencialNew = empresaNew.Telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Emergencial);
            Telefone telCelularNew = empresaNew.Telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Celular);

            Telefone telResidencial = empresa.Telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Residencial);
            Telefone telEmergencial = empresa.Telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Emergencial);
            Telefone telCelular = empresa.Telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Celular);

            if (telResidencialNew != null)
            {
                if (telResidencial != null)
                {
                    telResidencial.TelefoneNum = telResidencialNew.TelefoneNum;
                    repTel.Atualizar(telResidencial);
                }
                else
                {
                    telResidencial = new Telefone
                    {
                        TelefoneNum = telResidencialNew.TelefoneNum,
                        TipoTelefone = TipoTelefone.Residencial,
                        DataRegistro = DateTime.Now,
                        Pessoa = empresa,
                        Ativo = true
                    };
                    repTel.Salvar(telResidencial);
                }
            }

            if (telEmergencialNew != null)
            {
                if (telEmergencial != null)
                {
                    telEmergencial.TelefoneNum = telEmergencialNew.TelefoneNum;
                    repTel.Atualizar(telEmergencial);
                }
                else
                {
                    telEmergencial = new Telefone
                    {
                        TelefoneNum = telEmergencialNew.TelefoneNum,
                        TipoTelefone = TipoTelefone.Emergencial,
                        DataRegistro = DateTime.Now,
                        Pessoa = empresa,
                        Ativo = true
                    };
                    repTel.Salvar(telEmergencial);
                }
            }

            if (telCelularNew != null)
            {
                if (telCelular != null)
                {
                    telCelular.TelefoneNum = telCelularNew.TelefoneNum;
                    repTel.Atualizar(telCelular);
                }
                else
                {
                    Telefone telefone = new Telefone
                    {
                        Pessoa = empresa,
                        TelefoneNum = telCelularNew.TelefoneNum,
                        TipoTelefone = telCelularNew.TipoTelefone,
                        DataRegistro = DateTime.Now
                    };

                    InserirTelefone(telefone);
                    empresa.Telefones.Add(telefone);
                }
            }

            empresaNew.Endereco.Logradouro.CEP = empresaNew.Endereco.Logradouro.CEP.Replace(".", "").Replace("-", "");
            empresaNew.Endereco.DataRegistro = DateTime.Now;
            empresa.Endereco = repEnd.VerificarEndereco(empresaNew.Endereco);
            empresa.Endereco.Ativar();
            if (empresaNew.Endereco.Codigo <= 0)
                repEnd.Salvar(empresa.Endereco);
            else
                repEnd.Atualizar(empresa.Endereco);

            if (cnaesDel != null)
            {
                foreach (var item in empresa.CNAEs.Where(t => cnaesDel.Any(tdd => tdd.Codigo == t.CNAE.Codigo)))
                {
                    EmpresaCNAE empCnae = item;
                    empCnae.Inativar();
                }
            }
            if (cnaesNew != null)
            {
                foreach (var item in cnaesNew)
                {
                    EmpresaCNAE ec = new EmpresaCNAE();
                    ec.Empresa = empresa;
                    ec.CNAE = new CNAE { Codigo = item.Codigo };
                    ec.DataRegistro = DateTime.Now;
                    ec.Ativar();
                    repEmpresa.SalvarEmpresaCnae(ec);
                }
            }
            //DOCUMENTOS
            string path = ConfigurationManager.AppSettings.Get("pathUploads");

            if (documentosMidias != null)
            {
                foreach (var item in documentosMidias.Where(dm => empresa.Documentos.Any(cd => dm.Documento.Codigo == cd.Documento.Codigo)))
                {
                    // Se Documentos com status completo não tiverem midia gera erro.
                    if (item.Documento.Status == StatusDocumento.Completo && (item.Midias == null || !item.Midias.Any(m => m != null)))
                    {
                        throw new CoreException("Não é possível salvar documento com status 'Completo' sem mídia anexada.");
                    }

                    Documento doc = repDoc.ObterPorId(item.Documento.Codigo);
                    doc.Status = item.Documento.Status;
                    doc.Descricao = item.Documento.Descricao;
                    doc.DataValidade = item.Documento.DataValidade;
                    repDoc.Atualizar(doc);

                    if (item.Midias == null)
                    {
                        DocumentoMidiaModelView item1 = item;
                        var docMidias = repDocumentoMidia.ObterTodos().Where(dm => dm.Documento.Codigo == item1.Documento.Codigo && dm.Ativo);
                        foreach (var documentoMidia in docMidias)
                        {
                            documentoMidia.Inativar();
                            repDocumentoMidia.Atualizar(documentoMidia);
                        }
                        continue;
                    }
                    else
                    {
                        DocumentoMidiaModelView item1 = item;
                        List<int> codigosMidia = item.Midias.Select(i => i.CodigoMidia).ToList();
                        IQueryable<DocumentoMidia> listaDocMidia = repDocumentoMidia.ObterTodos().Where(dm =>
                            dm.Documento.Codigo == item1.Documento.Codigo &&
                            dm.Ativo && !codigosMidia.Contains(dm.Midia.Codigo));

                        foreach (DocumentoMidia docMidia in listaDocMidia)
                        {
                            docMidia.Inativar();
                            repDocumentoMidia.Salvar(docMidia);
                        }
                    }

                    int cont = repDocumentoMidia.ObterTodos().Where(dm => dm.Documento.Codigo == item.Documento.Codigo).Count() + 1;

                    foreach (MidiaModelView midia in item.Midias.Where(m => m.CodigoMidia == 0))
                    {
                        if (!System.IO.Directory.Exists(path))
                            throw new CoreException("O diretório onde a midia seria armazanada não está acessivel.");

                        string nomeDisco = "PJ-" + empresa.CNPJ + "-" + doc.TipoDocumento.Sigla + "-" + cont + "." + midia.Extensao;

                        Midia m = new Midia();
                        m.DataRegistro = midia.DataRegistro;
                        m.Extensao = midia.Extensao;
                        m.MimeType = midia.MIME;
                        m.NomeArquivo = nomeDisco;

                        repMidia.Salvar(m);

                        repDocumentoMidia.SalvarDocMidia(doc, m);
                        System.IO.File.WriteAllBytes(m.Url, Convert.FromBase64String(midia.Arquivo));

                        cont++;

                    }
                }

                foreach (var item in documentosMidias.Where(dm => !empresa.Documentos.Any(cd => dm.Documento.Codigo == cd.Documento.Codigo)))
                {
                    // Se Documentos com status completo não tiverem midia gera erro.
                    if (item.Documento.Status == StatusDocumento.Completo && (item.Midias == null || !item.Midias.Any(m => m != null)))
                    {
                        throw new CoreException("Não é possível salvar documento com status 'Completo' sem mídia anexada.");
                    }

                    DocumentoEmpresa docEmpresa = repDoc.SalvarDocEmpresa(empresa, item.Documento);

                    int cont = repDocumentoMidia.ObterTodos().Where(dm => dm.Documento.Codigo == item.Documento.Codigo).Count() + 1;

                    foreach (MidiaModelView midia in item.Midias ?? Enumerable.Empty<MidiaModelView>())
                    {
                        if (midia == null)
                            continue;

                        if (!System.IO.Directory.Exists(path))
                            throw new CoreException("O diretório onde a midia seria armazanada não está acessivel.");

                        string nomeDisco = "PJ-" + empresa.CNPJ + "-" + docEmpresa.Documento.TipoDocumento.Sigla + "-" + cont + "." + midia.Extensao;
                        Midia m = new Midia();
                        m.DataRegistro = midia.DataRegistro;
                        m.Extensao = midia.Extensao;
                        m.MimeType = midia.MIME;
                        m.NomeArquivo = nomeDisco;

                        repMidia.Salvar(m);

                        repDocumentoMidia.SalvarDocMidia(docEmpresa.Documento, m);

                        System.IO.File.WriteAllBytes(m.Url, Convert.FromBase64String(midia.Arquivo));

                        cont++;
                    }
                }
            }

            if (repEmpresa.ObterTodos().SelectMany(e => e.Documentos).Any(d => d.Documento.Status != StatusDocumento.Completo && d.Empresa.Codigo == empresa.Codigo))
            {
                PedidoCompra pc = repPedidoCompra.ObterTodos().Where(p => p.Empresa.Codigo == empresa.Codigo && p.Status == StatusPedido.Ativo).FirstOrDefault();
                if (pc == null)
                    empresa.Inativar();
                else
                    throw new CoreException("Não é possível alterar documentação para Incompleta/Não entregue, a empresa tem Pedido de Compra (PO) ativo.");
            }
            else
                empresa.Ativar();

            if (sindicatos != null && sindicatos.Count > 0)
            {
                IRepositorioSindicato repSindicato = Fabrica.Instancia.ObterRepositorio<IRepositorioSindicato>(UnidadeTrabalho);
                IRepositorioEmpresaSindicato repEmpresaSindicato = Fabrica.Instancia.ObterRepositorio<IRepositorioEmpresaSindicato>(UnidadeTrabalho);

                int[] codigosSindicatos = sindicatos.Select(s => s.Codigo).ToArray();
                int[] codigosSindicatosAtivosEmpresa = repEmpresaSindicato.ObterTodos().Where(es => es.Empresa.Codigo == empresa.Codigo && es.Ativo).Select(es => es.Sindicato.Codigo).ToArray();

                int[] codigosSindicatosRemovidos = codigosSindicatosAtivosEmpresa.Except(codigosSindicatos).ToArray();
                int[] codigosSindicatosNovos = codigosSindicatos.Except(codigosSindicatosAtivosEmpresa).ToArray();

                List<Sindicato> sindicatosNovos = sindicatos.Where(s => codigosSindicatosNovos.Contains(s.Codigo)).ToList();
                List<EmpresaSindicato> esRemovidos = repEmpresaSindicato.ObterTodos().Where(es => codigosSindicatosRemovidos.Contains(es.Sindicato.Codigo) && es.Empresa.Codigo == empresa.Codigo).ToList();

                if (sindicatosNovos != null && sindicatosNovos.Count > 0)
                {
                    foreach (Sindicato sindicato in sindicatosNovos)
                    {
                        EmpresaSindicato esNovo = new EmpresaSindicato();
                        esNovo.Ativar();
                        esNovo.DataRegistro = DateTime.Now;
                        esNovo.Empresa = empresa;
                        esNovo.Sindicato = sindicato;

                        repEmpresaSindicato.Validar(esNovo, EstadoObjeto.Novo);
                        repEmpresaSindicato.Salvar(esNovo);
                    }
                }

                if (esRemovidos != null && esRemovidos.Count > 0)
                {
                    foreach (EmpresaSindicato empresaSindicato in esRemovidos)
                    {
                        empresaSindicato.Inativar();

                        repEmpresaSindicato.Validar(empresaSindicato, EstadoObjeto.Removido);
                        repEmpresaSindicato.Salvar(empresaSindicato);
                    }
                }
            }

            repEmpresa.Validar(empresa, EstadoObjeto.Alterado);
            repEmpresa.Atualizar(empresa);

            return Json(montaObj(empresa));
        }

        /// <summary>
        /// Ativar inativar.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult AtivarInativar(string codigo)
        {
            IRepositorioEmpresa repEmpresa = Fabrica.Instancia.ObterRepositorio<IRepositorioEmpresa>(UnidadeTrabalho);
            IRepositorioPedidoCompra repPedidoCompra = Fabrica.Instancia.ObterRepositorio<IRepositorioPedidoCompra>(UnidadeTrabalho);

            Empresa empresa = repEmpresa.ObterPorId(codigo);
            if (empresa.Ativo)
            {
                PedidoCompra pc = repPedidoCompra.ObterTodos().Where(p => p.Empresa.Codigo == codigo && p.Status == StatusPedido.Ativo).FirstOrDefault();
                if (pc == null)
                    empresa.Inativar();
                else
                    throw new CoreException("Não é possível inativar, a empresa possui Pedido de Compra (PO) ativo.");
            }
            else
            {
                repEmpresa.Validar(empresa, EstadoObjeto.Ativado);
                empresa.Ativar();
            }

            return Json(montaObj(empresa));
        }

        /// <summary>
        /// Obter por cnpj.
        /// </summary>
        ///
        /// <param name="cnpj">
        /// O(a) cnpj.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ObterPorCnpj(string cnpj = null)
        {
            IRepositorioEmpresa repEmpresa = Fabrica.Instancia.ObterRepositorio<IRepositorioEmpresa>(UnidadeTrabalho);

            if (cnpj != null)
                cnpj = cnpj.Replace("-", "").Replace(".", "").Replace("/", "");

            Empresa empresa = repEmpresa.ObterPorCnpj(cnpj);

            if (empresa != null)
                return Json(montaObj(empresa));

            return Json(null);
        }

        public string TelefoneFormatado(string numero)
        {
            if (string.IsNullOrEmpty(numero))
                return null;

            Regex apenasDigitos = new Regex(@"[^\d]");
            string numeroFormatado = apenasDigitos.Replace(numero, "");
            numeroFormatado = string.Format(@"{0:(00) 0000-0000}{1}", Convert.ToInt64(numeroFormatado.Substring(0, 10)), numeroFormatado.Length > 10 ? Convert.ToInt32(numeroFormatado.Substring(10)).ToString() : string.Empty);
            return numeroFormatado;
        }
    }
}
