﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using Globalsys.Relatorios;
using Microsoft.Reporting.WebForms;
using Controlsys.Repositorios.Pessoas;
using Controlsys.Infra;
using Globalsys;
using Controlsys.Dominio.Pessoas;
using Controlsys.Interfaces.Models;
using Controlsys.Dominio.Empresas;
using System.Collections;
using Controlsys.Repositorios.Empresas;
using Controlsys.Repositorio.Parametros;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar relatorio pessoas.
    /// </summary>
    [ExtendController]
    public class RelatorioPessoasController : Controller
    {
        /// <summary>
        /// GET: /RelatorioPessoas/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.RelatorioPessoasController.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RelatorioPessoasController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        /// <summary>
        /// Retorna a página de relatorioPessoas.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Exportar.
        /// </summary>
        ///
        /// <param name="status">
        /// O(a) status.
        /// </param>
        /// <param name="selectedFields">
        /// The selected fields.
        /// </param>
        /// <param name="tipoRelatorio">
        /// O(a) tipo relatorio.
        /// </param>
        /// <param name="nome">
        /// O(a) nome.
        /// </param>
        /// <param name="cpf">
        /// O(a) cpf.
        /// </param>
        /// <param name="empresa">
        /// O(a) empresa.
        /// </param>
        /// <param name="cargo">
        /// O(a) cargo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Exportar(StatusPapelLog? status, List<string> selectedFields, TipoPapel? papel, string tipoRelatorio, string nome = null, string cpf = null, string empresa = null, string cargo = null, string area = null, string maodeobra = null, string fornecedor = null)
        {
            empresa = Empresa.RetornaApenasNomeEmpresa(empresa);
            IRepositorioPapel repPal = Fabrica.Instancia.ObterRepositorio<IRepositorioPapel>(UnidadeTrabalho);
            IQueryable<Papel> papeis = repPal.ObterTodos();

            string fileName = string.Empty;

            switch (tipoRelatorio)
            {
                case "PDF":
                    fileName = "download.pdf";
                    break;
                case "EXCELOPENXML":
                    fileName = "download.xlsx";
                    break;
                case "WORDOPENXML":
                    fileName = "download.doc";
                    break;
            }

            papeis = FiltrarPapeis(status, papel, nome, cpf, empresa, cargo, area, maodeobra, fornecedor);
            ReportConfig reportConfig = ReportHelper.RenderReport<RelatorioPessoaModelView>(selectedFields, tipoRelatorio, papeis.Select(p => montaObj(p)));
            return File(reportConfig.Bytes, reportConfig.MimeType, fileName);
        }

        /// <summary>
        /// Pesquisar papeis.
        /// </summary>
        ///
        /// <param name="parameters">
        /// Options for controlling the operation.
        /// </param>
        /// <param name="status">
        /// O(a) status.
        /// </param>
        /// <param name="nome">
        /// O(a) nome.
        /// </param>
        /// <param name="cpf">
        /// O(a) cpf.
        /// </param>
        /// <param name="empresa">
        /// O(a) empresa.
        /// </param>
        /// <param name="cargo">
        /// O(a) cargo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult PesquisarPapeis(StoreRequestParameters parameters, StatusPapelLog? status, TipoPapel? papel, string nome = null, string cpf = null, string empresa = null, string cargo = null, string area = null, string maodeobra = null, string fornecedor = null)
        {
            empresa = Empresa.RetornaApenasNomeEmpresa(empresa);
            var papeis = FiltrarPapeis(status, papel, nome, cpf, empresa, cargo, area, maodeobra, fornecedor);

            int total = papeis.Count();
            var data = montaListaObj(papeis, parameters);
            return new StoreResult(data, total);
        }

        /// <summary>
        /// Filtrar papeis.
        /// </summary>
        ///
        /// <param name="status">
        /// O(a) status.
        /// </param>
        /// <param name="nome">
        /// O(a) nome.
        /// </param>
        /// <param name="cpf">
        /// O(a) cpf.
        /// </param>
        /// <param name="empresa">
        /// O(a) empresa.
        /// </param>
        /// <param name="cargo">
        /// O(a) cargo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) IQueryable&lt;Papel&gt;
        /// </returns>
        private IQueryable<Papel> FiltrarPapeis(StatusPapelLog? status, TipoPapel? papel, string nome = null, string cpf = null, string empresa = null, string cargo = null, string area = null, string maodeobra = null, string fornecedor = null)
        {
            string query = @"
            SELECT      PA.* 
                        ,PF.[CD_PESSOA_FISICA]
                        ,PF.[TX_CPF]
                        ,PF.[CD_NATURAL] 
                        ,PF.[TX_PASSAPORTE]
                        ,PF.[TX_RG]
                        ,PF.[TX_RG_ORGAO]
                        ,PF.[CD_RG_UF]
                        ,PF.[DT_RG_EMISSAO]
                        ,PF.[DT_NASCIMENTO]
                        ,PF.[TX_MAE]
                        ,PF.[TX_PAI]
                        ,PF.[TX_SANGE]
                        ,PF.[TX_SEXO]
                        --,PF.[ME_FOTO]
                        ,PF.[DT_VISTO]
                        ,PF.[DT_VALIDADE_VISTO]
                        ,PF.[TX_RNE]
                        ,PF.[DT_EXPEDICAO_RNE]
                        ,PF.[TX_ESTADOCIVIL]
                        ,PF.[TX_ESCOLARIDADE]
                        ,PF.[NU_CR_NR]
                        ,PF.[NU_CR_TIPO]
                        ,PF.[TX_CNH]
                        ,PF.[TX_CNH_TP]
                        ,PF.[DT_CNH_VALIDADE]
                        ,PF.[NU_TE]
                        ,PF.[NU_TE_ZONA]
                        ,PF.[NU_TE_SECAO]
                        ,PF.[NU_CT]
                        ,PF.[TX_CT_SERIE]
                        ,PF.[CD_CT_ID_UF]
                        ,PF.[DT_CT_DT]
                        ,PF.[CD_MUNICIPIO]
                        ,PF.[TX_NACIONALIDADE]
                        ,(CASE WHEN CO.[TXT_MAODEOBRA] IS NOT NULL THEN CO.[TXT_MAODEOBRA] 
						       WHEN PR.[TXT_MAODEOBRA] IS NOT NULL THEN PR.[TXT_MAODEOBRA] 
							   ELSE Null END) [TXT_MAODEOBRA] 
                        ,(CASE 
							WHEN CO.CD_PAPEL IS NOT NULL THEN CO.DT_ADMISSAO
							WHEN PR.CD_PAPEL IS NOT NULL THEN PR.DT_ADMISSAO
							ELSE NULL
							END ) DT_ADMISSAO
                        ,(CASE 
							WHEN CO.CD_PAPEL IS NOT NULL THEN CO.DT_VALIDADE
							WHEN PR.CD_PAPEL IS NOT NULL THEN PR.DT_VALIDADE
							ELSE NULL
							END ) AS DT_VALIDADE
                        , CO.*, PR.*, V.*, EC.*, EP.*, PEPF.*, PEEC.*, PEEP.*, CA.*
                        , CASE 
                            WHEN CO.CD_PAPEL is not null then 1
                            WHEN PR.CD_PAPEL is not null then 2
                            WHEN V.CD_PAPEL is not null then 3
                            WHEN EC.CD_PESSOA is not null then 4
                            WHEN EP.CD_PESSOA is not null then 5
                            WHEN PEPF.CD_PESSOA is not null then 6
                            WHEN PEEC.CD_PESSOA is not null then 7
                            WHEN PEEP.CD_PESSOA is not null then 8
                            WHEN CA.CD_CARGO is not null then 9
            ELSE 0
                        END as clazz_
            FROM  PAPEL PA WITH(NOLOCK)
               INNER JOIN PESSOA_FISICA PF WITH(NOLOCK) ON PA.CD_PESSOA_FISICA = PF.CD_PESSOA_FISICA ";

            if (papel == null)
                query += @"LEFT JOIN COLABORADOR CO WITH(NOLOCK) ON PA.CD_PAPEL = CO.CD_PAPEL  
                                LEFT JOIN PRESTADOR PR WITH(NOLOCK) ON PA.CD_PAPEL = PR.CD_PAPEL 
                                LEFT JOIN VISITANTE V WITH(NOLOCK) ON PA.CD_PAPEL = V.CD_PAPEL ";
            else
            {
                if (papel.Value.ToString() == "Colaborador")
                    query += @"INNER JOIN COLABORADOR CO WITH(NOLOCK) ON PA.CD_PAPEL = CO.CD_PAPEL  
                                    LEFT JOIN PRESTADOR PR WITH(NOLOCK) ON PA.CD_PAPEL = PR.CD_PAPEL 
                                    LEFT JOIN VISITANTE V WITH(NOLOCK) ON PA.CD_PAPEL = V.CD_PAPEL ";
                if (papel.Value.ToString() == "PrestadorServico")
                    query += @"INNER JOIN PRESTADOR PR WITH(NOLOCK) ON PA.CD_PAPEL = PR.CD_PAPEL 
                                    LEFT JOIN COLABORADOR CO WITH(NOLOCK) ON PA.CD_PAPEL = CO.CD_PAPEL 
                                    LEFT JOIN VISITANTE V WITH(NOLOCK) ON PA.CD_PAPEL = V.CD_PAPEL ";
                if (papel.Value.ToString() == "Visitante")
                    query += @"INNER JOIN VISITANTE V WITH(NOLOCK) ON PA.CD_PAPEL = V.CD_PAPEL 
                                    LEFT JOIN PRESTADOR PR WITH(NOLOCK) ON PA.CD_PAPEL = PR.CD_PAPEL 
                                    LEFT JOIN COLABORADOR CO WITH(NOLOCK) ON PA.CD_PAPEL = CO.CD_PAPEL ";
            }        
            query += @"LEFT JOIN EMPRESA EC WITH(NOLOCK) ON CO.CD_EMPRESA = EC.CD_PESSOA 
            LEFT JOIN EMPRESA EP WITH(NOLOCK) ON PR.CD_EMPRESA = EP.CD_PESSOA 
            LEFT JOIN PESSOA PEPF WITH(NOLOCK) ON PEPF.CD_PESSOA = PF.CD_PESSOA_FISICA 
            LEFT JOIN PESSOA PEEC WITH(NOLOCK) ON EC.CD_PESSOA = PEEC.CD_PESSOA 
            LEFT JOIN PESSOA PEEP WITH(NOLOCK) ON EP.CD_PESSOA = PEEP.CD_PESSOA 
            LEFT JOIN CARGO CA WITH(NOLOCK) ON PA.CD_CARGO = CA.CD_CARGO 
            WHERE  1 = 1
            ";

            string queryFiltroAtivo = @"AND			EXISTS (SELECT		PL.CD_PAPEL 
					FROM		PAPEL_LOG PL
								INNER JOIN (select		pl.CD_PAPEL, max(pl.CD_PAPEL_LOG) CD_PAPEL_LOG 
											from		papel_log pl WITH(NOLOCK) 
											group by	pl.CD_PAPEL) MAX_PL ON PL.CD_PAPEL_LOG = MAX_PL.CD_PAPEL_LOG
					WHERE		UPPER(PL.TX_STATUS) = :status
					and			PA.CD_PAPEL = PL.CD_PAPEL)";

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            List<string> filtros = new List<string>();

            if (!string.IsNullOrEmpty(nome))
            {
                parameters.Add("nome", nome);
                filtros.Add("AND			PEPF.TX_NOME LIKE CONCAT('%', :nome, '%')");
            }

            if (!string.IsNullOrEmpty(cpf))
            {
                cpf = cpf.Replace(".", "").Replace("-", "");
                parameters.Add("cpf", cpf);
                filtros.Add("AND			PF.TX_CPF LIKE CONCAT('%', :cpf, '%')");
            }

            if (!string.IsNullOrEmpty(cargo))
            {
                parameters.Add("cargo", cargo);
                filtros.Add("AND			(CA.TX_NOME = :cargo OR V.TX_FUNCAO LIKE CONCAT('%', :cargo, '%'))");
            }

            if (!string.IsNullOrEmpty(empresa))
            {
                parameters.Add("empresa", empresa);
                filtros.Add("AND			(PEEC.TX_NOME = :empresa OR PEEP.TX_NOME = :empresa OR V.TX_EMPRESA LIKE CONCAT('%', :empresa, '%'))");
            }

            if (!string.IsNullOrEmpty(area))
            {
                parameters.Add("area", area);
                filtros.Add("AND			(PA.CD_AREA = :area)");
            }

            if (!string.IsNullOrEmpty(fornecedor))
            {
                parameters.Add("fornecedor", fornecedor);
                filtros.Add("AND			(PR.CD_FORNECEDOR = :fornecedor)");
            }

            if (status.HasValue)
            {
                parameters.Add("status", status.Value.ToString());
                filtros.Add(queryFiltroAtivo);
            }

            if (!string.IsNullOrEmpty(maodeobra.ToString()))            
            {
                parameters.Add("maodeobra", maodeobra.ToString());
                filtros.Add("AND			((CO.TXT_MAODEOBRA = :maodeobra) Or (PR.TXT_MAODEOBRA = :maodeobra))");
            }

            if (filtros.Any())
                query += string.Join(Environment.NewLine, filtros);

            query += string.Join(Environment.NewLine, "ORDER BY ISNULL(PEPF.TX_NOME, ISNULL(PEEC.TX_NOME, ISNULL(PEEP.TX_NOME, '')))");

            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            IQueryable<Papel> resultado = unidTrabalho.ExecuteSql<Papel>(query, parameters, new List<Type>() { typeof(Papel) }).AsQueryable();
            return resultado;
        }

        /// <summary>
        /// Cria um objeto contendo informações relacionadas ao conteudo gerenciado por
        /// RelatorioPessoasController.
        /// </summary>
        ///
        /// <param name="papeis">
        /// O(a) papeis.
        /// </param>
        /// <param name="parameters">
        /// Options for controlling the operation.
        /// </param>
        /// <param name="ignoreParameters">
        /// true to ignore parameters.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;RelatorioPessoaModelView&gt;
        /// </returns>
        private List<RelatorioPessoaModelView> montaListaObj(IQueryable<Papel> papeis, StoreRequestParameters parameters = null, bool ignoreParameters = false)
        {
            try
            {
                if (!ignoreParameters)
                {
                    if (parameters == null)
                        parameters = new StoreRequestParameters();
                    int total = papeis.Count();
                    papeis = papeis.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 25);
                }
                return papeis.Select(p => montaObj(p)).ToList();
            }
            catch (Exception)
            { throw; }
        }

        private RelatorioPessoaModelView montaObj(Papel papel)
        {
            IRepositorioPrestadorServico repPrestador = Fabrica.Instancia.ObterRepositorio<IRepositorioPrestadorServico>(UnidadeTrabalho);
            IRepositorioColaborador repColaborador = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);
            IRepositorioVisitante repVisitante = Fabrica.Instancia.ObterRepositorio<IRepositorioVisitante>(UnidadeTrabalho);
            IRepositorioAlocacaoColaborador repAlocacao = Fabrica.Instancia.ObterRepositorio<IRepositorioAlocacaoColaborador>(UnidadeTrabalho);
            IRepositorioCracha repCracha = Fabrica.Instancia.ObterRepositorio<IRepositorioCracha>(UnidadeTrabalho);
            IQueryable<AlocacaoColaborador> aloc = repAlocacao.ObterTodos();

            var relPessoa = new RelatorioPessoaModelView();

            string apelidoX = "", cargoX = "";

            AlocacaoColaborador alocacao = aloc.Where(a => a.Papel.Codigo == papel.Codigo).OrderByDescending(a => a.DataRegistro).FirstOrDefault();
            PedidoCompra alocada = alocacao != null ? alocacao.ObterContratada() : null;

            Cracha cracha = repCracha.ObterCracha(papel.Codigo);

            if (cracha != null)
            {
                relPessoa.RFID = cracha.Numero;
                relPessoa.DataRegistroCracha = cracha.DataRegistro.ToString("dd/MM/yyyy");
            }
            relPessoa.DataRegistroPapel = papel.DataRegistro.ToString("dd/MM/yyyy");
            relPessoa.Codigo = papel.Codigo.ToString();
            relPessoa.DataValidadeASO = papel.ASOs == null || papel.ASOs.Count == 0 ? string.Empty : papel.ASOs.Select(aso => aso.DataVencimento).Max().ToString("dd/MM/yyyy");

            if (papel is Colaborador)
            {                
                var colabX = (papel as Colaborador);

                apelidoX = (((papel as Colaborador).Empresa) != null ? (papel as Colaborador).Empresa.Nome : "");
                cargoX = ((papel as Colaborador).Cargo != null) ? (papel as Colaborador).Cargo.Nome : "";

                relPessoa.ConselhoProfissional = (papel as Colaborador).ConselhoProfissional;
                relPessoa.RegistroProfissional = (papel as Colaborador).RegistroProfissional;
                relPessoa.PessoaJuridica = ((papel as Colaborador).PessoaJuridica) ? "Sim" : "Não";
                relPessoa.DataAdmissao = (papel as Colaborador).DataAdmissao.ToString();
                relPessoa.Salario = (papel as Colaborador).Salario.ToString();
                relPessoa.SetorCusto = (alocacao != null) ? (alocacao.SetorCusto != null ? string.Format("{0} - {1}", alocacao.SetorCusto.Numero, alocacao.SetorCusto.Nome) : "") : "";
                relPessoa.Projeto = (alocacao != null) ? alocacao.PedidoCompra.Projeto.Nome : "";
                relPessoa.PedidoCompra = (alocacao != null) ? string.Format("{0} - {1}", alocacao.PedidoCompra.Numero, alocacao.PedidoCompra.Nome) : "";
                relPessoa.Observacao = (papel as Colaborador).Observacao;
                relPessoa.DataValidadeColaborador = colabX.DataValidade.HasValue ? colabX.DataValidade.Value.ToShortDateString() : string.Empty;

                string delimitador = " - ";
                if (colabX.GruposTrabalho != null && colabX.GruposTrabalho.Count > 0)
                {
                    relPessoa.NumerosPHT = colabX.GruposTrabalho.OrderByDescending(gt => gt.Codigo).FirstOrDefault().GrupoTrabalho.CodigoPHT;
                    //relPessoa.NumerosPHT = colabX.GruposTrabalho.Select(x => x.GrupoTrabalho.CodigoPHT).Aggregate((i, j) => i + delimitador + j);
                }                
                relPessoa.MaoDeObra = (papel as Colaborador).MaoDeObra.ObterDescricaoEnum();
            }
            else if (papel is PrestadorServico)
            {
                var prest = (papel as PrestadorServico);

                if (prest.Empresa == null && prest.Fornecedor == null)
                    prest = repPrestador.ObterTodos().FirstOrDefault(p => p.Codigo == (papel as PrestadorServico).Codigo);

                apelidoX = prest.TipoPrestador == TipoPrestador.Temporario ? prest.Empresa.Nome : prest.Fornecedor.NomeFantasia;

                cargoX = (prest.Cargo != null) ? prest.Cargo.Nome : "";
                relPessoa.DataAdmissao = prest.DataAdmissao.HasValue ? prest.DataAdmissao.Value.ToString() : "";

                relPessoa.DataValidadePrestador = prest.DataValidade.ToShortDateString();
                relPessoa.MaoDeObra = (papel as PrestadorServico).MaoDeObra.ObterDescricaoEnum();
            }
            else if (papel is Visitante)
            {
                apelidoX = (papel as Visitante).Empresa;
                cargoX = (papel as Visitante).Funcao;

                relPessoa.DataInicioVisitante = (papel as Visitante).DataInicio.ToShortDateString();
                relPessoa.DataFimVisitante = (papel as Visitante).DataFim.ToShortDateString();                
            }

            relPessoa.CPF = papel.PessoaFisica.CPFFormatado();
            relPessoa.Passaporte = papel.PessoaFisica.Passaporte;
            relPessoa.Nome = papel.PessoaFisica.Nome;
            relPessoa.Apelido = papel.PessoaFisica.Apelido;
            relPessoa.Email = papel.PessoaFisica.Email;
            relPessoa.DataNascimento = papel.PessoaFisica.DataNascimento.ToString();
            relPessoa.DataVisto = papel.PessoaFisica.DataVisto.ToString();
            relPessoa.DataValidadeVisto = papel.PessoaFisica.DataValidadeVisto.ToString();
            relPessoa.Sexo = papel.PessoaFisica.Sexo != 0 ? papel.PessoaFisica.Sexo.ObterDescricaoEnum() : "";
            relPessoa.NomeMae = papel.PessoaFisica.NomeMae;
            relPessoa.NomePai = papel.PessoaFisica.NomePai;
            relPessoa.RNE = papel.PessoaFisica.RNE;
            relPessoa.DataExpedicaoRNE = papel.PessoaFisica.DataExpedicaoRNE.ToString();
            relPessoa.EstadoCivil = (papel.PessoaFisica.EstadoCivil != 0 && papel.PessoaFisica.EstadoCivil != null) ? papel.PessoaFisica.EstadoCivil.ObterDescricaoEnum() : "";
            relPessoa.Escolaridade = (papel.PessoaFisica.Escolaridade != 0 && papel.PessoaFisica.Escolaridade != null) ? papel.PessoaFisica.Escolaridade.ObterDescricaoEnum() : "";
            relPessoa.TipoSanguineo = (papel.PessoaFisica.TipoSanguineo != 0 && papel.PessoaFisica.TipoSanguineo != null) ? papel.PessoaFisica.TipoSanguineo.ObterDescricaoEnum() : "";
            relPessoa.Nacionalidade = papel.PessoaFisica.Nacionalidade;

            relPessoa.NaturalidadeUF = (papel.PessoaFisica.Naturalidade != null) ? papel.PessoaFisica.Naturalidade.Estado.Nome : "";
            relPessoa.Naturalidade = (papel.PessoaFisica.Naturalidade != null) ? papel.PessoaFisica.Naturalidade.Nome : "";

            relPessoa.RG = papel.PessoaFisica.RG;
            relPessoa.RGOrgaoEmissor = papel.PessoaFisica.RGOrgaoEmissor;
            relPessoa.RGOrgaoEmissorUF = (papel.PessoaFisica.RGOrgaoEmissorUF != null) ? papel.PessoaFisica.RGOrgaoEmissorUF.Nome : "";

            relPessoa.TituloEleitor = papel.PessoaFisica.TituloEleitor.ToString();
            relPessoa.TituloEleitorSecao = papel.PessoaFisica.TituloEleitorSecao.ToString();
            relPessoa.TituloEleitorZona = papel.PessoaFisica.TituloEleitorZona.ToString();
            relPessoa.TituloEleitorCidade = (papel.PessoaFisica.TituloEleitorCidade != null) ? papel.PessoaFisica.TituloEleitorCidade.Nome : "";
            relPessoa.TituloEleitorEstado = (papel.PessoaFisica.TituloEleitorCidade != null) ? papel.PessoaFisica.TituloEleitorCidade.Estado.Nome : "";

            relPessoa.CTPS = papel.PessoaFisica.CTPS.ToString();
            relPessoa.CTPSData = papel.PessoaFisica.CTPSData.ToString();
            relPessoa.CTPSSerie = papel.PessoaFisica.CTPSSerie;
            relPessoa.CTPSEstado = (papel.PessoaFisica.CTPSEstado != null) ? papel.PessoaFisica.CTPSEstado.Nome : "";
            relPessoa.CNH = papel.PessoaFisica.CNH;
            relPessoa.CNHCategoria = (papel.PessoaFisica.CNHCategoria != 0 && papel.PessoaFisica.CNHCategoria != null) ? papel.PessoaFisica.CNHCategoria.ObterDescricaoEnum() : "";
            relPessoa.CNHDataValidade = papel.PessoaFisica.CNHDataValidade.ToString();

            relPessoa.CertificadoReservista = papel.PessoaFisica.CertificadoReservista;
            relPessoa.CertificadoReservistaCat = papel.PessoaFisica.CertificadoReservistaCat;

            relPessoa.TelefoneCelular = papel.PessoaFisica.Telefones.Where(p => p.TipoTelefone == Dominio.Pessoas.TipoTelefone.Celular).Select(x => x.TelefoneNum).FirstOrDefault();
            relPessoa.TelefoneEmergencial = papel.PessoaFisica.Telefones.Where(p => p.TipoTelefone == Dominio.Pessoas.TipoTelefone.Celular).Select(x => x.TelefoneNum).FirstOrDefault();
            relPessoa.TelefoneResidencial = papel.PessoaFisica.Telefones.Where(p => p.TipoTelefone == Dominio.Pessoas.TipoTelefone.Residencial).Select(x => x.TelefoneNum).FirstOrDefault();

            if (papel.PessoaFisica.Endereco != null)
            {
                relPessoa.Complemento = papel.PessoaFisica.Endereco.Complemento;
                relPessoa.Numero = papel.PessoaFisica.Endereco.Numero;
                if (papel.PessoaFisica.Endereco.Logradouro != null)
                {
                    relPessoa.CEP = papel.PessoaFisica.Endereco.Logradouro.CEP;
                    relPessoa.Logradouro = papel.PessoaFisica.Endereco.Logradouro.Nome;
                    relPessoa.Bairro = papel.PessoaFisica.Endereco.Logradouro.Bairro.Nome;
                    relPessoa.Cidade = papel.PessoaFisica.Endereco.Logradouro.Bairro.Cidade.Nome;
                    relPessoa.Estado = papel.PessoaFisica.Endereco.Logradouro.Bairro.Cidade.Estado.Nome;
                    relPessoa.Pais = papel.PessoaFisica.Endereco.Logradouro.Bairro.Cidade.Estado.Pais.Nome;
                }                
            }

            relPessoa.Empresa = apelidoX;
            relPessoa.Cargo = cargoX;

            relPessoa.ModeloCracha = (papel.ModeloCracha != null) ? papel.ModeloCracha.Nome : "";

            PapelLog _papelLog = papel.ObterLogPapel();

            if (_papelLog != null)
            {
                relPessoa.StatusPapel = _papelLog.Status.ObterDescricaoEnum();
                relPessoa.DataStatusPapel = _papelLog.DataInicio.ToString();
                relPessoa.Status = _papelLog.Status.ObterDescricaoEnum();
            }

            relPessoa.Papel = papel != null ? papel is Colaborador ? "Colaborador" : papel is PrestadorServico ? "Prestador de Serviço" : "Visitante" : "";


            if (alocada != null)
            {
                if (alocada.Numero == alocacao.PedidoCompra.Numero)
                {
                    relPessoa.Contratada = alocada.Empresa.Nome; // alocacao.PedidoCompra.Empresa.Nome;
                    //itemLista.Subcontratada = "";
                }
                else
                {
                    relPessoa.Contratada = alocada.Empresa.Nome;
                    //itemLista.Subcontratada = alocacao.PedidoCompra.Empresa.Nome;
                }
            }

            if (papel.EmpregadoSAP != null)
            {
                relPessoa.EmpregadoSAP = papel.EmpregadoSAP.Codigo.ToString();

                if (papel.EmpregadoSAP.GestorPonto != null)
                {
                    relPessoa.GestorPonto = papel.EmpregadoSAP.GestorPonto.Codigo.ToString();
                }

                if (papel.EmpregadoSAP.SuperiorImediato != null)
                {
                    relPessoa.SuperiorImediato = papel.EmpregadoSAP.SuperiorImediato.Codigo.ToString();
                }


            }

            if (papel is Colaborador && (papel as Colaborador).Area != null)
            {
                relPessoa.Area = (papel as Colaborador).Area.Nome;
            }

            return relPessoa;

        }

        /// <summary>
        /// Obter campos.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ObterCampos()
        {
            List<object> lista = new List<object>();

            lista.Add(new { Nome = "CPF" });
            lista.Add(new { Nome = "Nome" });
            lista.Add(new { Nome = "Empresa" });
            lista.Add(new { Nome = "Cargo" });
            lista.Add(new { Nome = "Status" });
            lista.Add(new { Nome = "Passaporte" });
            lista.Add(new { Nome = "Papel" });
            lista.Add(new { Nome = "Contratada" });
            lista.Add(new { Nome = "Subcontratada" });
            return Json(lista);

        }

    }

    /// <summary>
    /// Representa um(a) Dado.
    /// </summary>
    public class Dado
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Descricao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Descricao.
        /// </value>
        public string Descricao { get; set; }

        /// <summary>
        /// Gets/Sets valor para Data.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Data.
        /// </value>
        public DateTime Data { get; set; }
    }
}
