﻿using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Pessoas;
using Controlsys.Dominio.CredenciamentoVeiculos;
using Controlsys.Infra;
using Controlsys.Repositorio.Acesso;
using Globalsys;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Controlsys.Repositorio.Parametros;
using System.IO;
using System.Diagnostics;
using Controlsys.Repositorios.Acesso;
using Controlsys.Repositorios.Parametros;
using AvigilonDotNet;
using System.Net;
using System.Threading;
using System.Text;
using Controlsys.Persistencia;
using System.Globalization;
using System.Web.Hosting;
using Controlsys.Dominio.Parametros;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar Grupo Acesso Temporario.
    /// </summary>
    [ExtendController]
    //[Route("/[controller]")]    
    //[Route("api/Integrador")]
    //[Route("api/[controller]")]
    public class IntegradorController : ApiController
    {
        IUnidadeTrabalho unidade;
        private Bitmap m_bitmap;
        //public static bool SuccessTentativa { get; set; }
        //public static int InfoControladora { get; set; }
        static readonly object GravaTentativa = new object();
        static readonly object LockInfoControladora = new object();
        List<ImagemPlaca> imagemPlacas = new List<ImagemPlaca>();
        DateTime dataRegistro = new DateTime();
        string paramentroUrlCaminhoImg = "";
        List<Camera> cameras = null;
        Bitmap imagem = null;
        string placa = "";
        string placaMaiorPrecisao = "";
        AcessoVeiculo acesso = null;
        IEventDeviceLprPlateFound lpr = null;
        CameraImagem cameraImagem = new CameraImagem();
        List<CameraImagem> LPR = new List<CameraImagem>();
        List<CameraBitmap> cameraBitmaps = new List<CameraBitmap>();
        List<bool> statusCameras = null;
        List<string> ImagensGravadas = new List<string>();
        float precisao = 0f;
        float maiorPrecisao = 0f;
        int parametroExpirar = 3;
        List<Bitmap> bitmaps = new List<Bitmap>();
        IAvigilonControlCenter controlCenter = null;
        RequestParams m_params = new RequestParams();
        INvr nvr = null;
        string nomeDevice = null;
        ManualResetEvent oSignalEvent = new ManualResetEvent(false);
        int MaxPlateFound = int.MaxValue;
        int MaxImageFound = int.MinValue;
        int TotalDevices = int.MinValue;
        float parametroPrecisao = 80;
        Camera camera = null;
        string urlBase = null;
        string erro;
        /// <summary>
        /// GET: /GrupoAcesso/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Contrutor padrão do Integrador
        /// </summary>
        public IntegradorController()
        {
            UnidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            urlBase = HttpContext.Current.Server.MapPath("~");
        }

        /// <summary>
        /// Retorna o tempo de espera especificado nos paramentros do sistema
        /// </summary>
        /// <param name="tempoEspera"></param>
        /// <returns></returns>
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public string ObterTempoEspera(int tempoEspera)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(1);
        }

        /// <summary>
        /// Verificação da obtenção de acesso do veículo
        /// </summary>
        /// <param name="IdLeitora"></param>
        /// <param name="IpControladora"></param>
        /// <param name="idAcesso"></param>
        /// <returns></returns>
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Mvc.ActionName("ObterPermissaoVeiculo")]
        public async Task<string> ObterPermissaoVeiculo(int IdLeitora, int IpControladora, int idAcesso)
        {
            statusCameras = new List<bool>();
            string json = null;
            //Buscar no banco de dados o caminho onde a imagem será gravada no servidor de arquivos
            var path = "";
            //paramentroUrlCaminhoImg = 
            try
            {
                var parametroAtivarValidacao = false;
                if (!parametroAtivarValidacao)
                {

                }

                //Inteligência para desativar e ativar a validação de veículos
                //Desativação de verificação por parâmetro geral
                IRepositorioParametro repParametro = Fabrica.Instancia.ObterRepositorio<IRepositorioParametro>(UnidadeTrabalho);

                //paramentroUrlCaminhoImg = repParametro.ObterTodos().Where(p => p.Nome == Dominio.Parametros.ParametroSistema.CaminhoBaseImagem).SingleOrDefault().Url;
                //var parametro = repParametro.ObterTodos().Where(p => p.Nome == Dominio.Parametros.ParametroSistema.AtivacaoValidarVeiculo).SingleOrDefault();

                ////Validação ativa == "1"
                ////Validação inativa == "0"
                //if (parametro.Valor != "1")
                //{
                //    //e valor for diferente de 1
                //    json = Newtonsoft.Json.JsonConvert.SerializeObject(true);
                //    return json;
                //}

                //urlBase = repParametro.ObterTodos().Where(p => p.Nome == Dominio.Parametros.ParametroSistema.CaminhoBaseImagem).SingleOrDefault().Url;

                //Tempo de expiração
                //Stopwatch expiracao = new Stopwatch();
                //expiracao.Start();

                IRepositorioAcessoVeiculo repAcessoVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioAcessoVeiculo>(UnidadeTrabalho);

                IRepositorioControleAcesso repAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioControleAcesso>(UnidadeTrabalho);
                var acessoPapel = repAcesso.ObterPorId(idAcesso);

                Controladora controladora = await Task.Run(() =>
                {

                    IRepositorioControladora repControladora = Fabrica.Instancia.ObterRepositorio<IRepositorioControladora>(UnidadeTrabalho);

                    return repControladora.ObterTodos().Where(x => x.Codigo == IpControladora).SingleOrDefault();

                });

                if (controladora.TipoControladora != TipoControladora.Cancela)
                {
                    json = Newtonsoft.Json.JsonConvert.SerializeObject(true);
                    return json;
                }

                Leitora leitora = await Task.Run(() =>
                {
                    IRepositorioLeitora repLeitora = Fabrica.Instancia.ObterRepositorio<IRepositorioLeitora>(UnidadeTrabalho);
                    return repLeitora.ObterTodos().Where(x => x.Codigo == IdLeitora).SingleOrDefault();
                });

                List<Camera> cameras = await Task.Run(() =>
                {
                    IRepositorioCamera repCamera = Fabrica.Instancia.ObterRepositorio<IRepositorioCamera>(UnidadeTrabalho);
                    var cams = repCamera.ObterTodos().Where(c => c.Controladora.Codigo == IpControladora).ToList();
                    return cams;
                });

                AcessoVeiculo acesso = new AcessoVeiculo();
                acesso.Controladora = controladora;

                //Não há câmeras vinculadas a controladora
                if (cameras == null || cameras.Count == 0)
                {
                    if (acessoPapel.StatusAcesso == StatusAcesso.OCO_UR_ACESSO_LIBERADO)
                    {
                        json = Newtonsoft.Json.JsonConvert.SerializeObject(true);
                        VerificarCamerasNaoVinculadas(acesso, acessoPapel);
                        //Não registra imagens
                        repAcessoVeiculo.Salvar(acesso);
                        return json;
                    }
                    else
                    {
                        json = Newtonsoft.Json.JsonConvert.SerializeObject(false);
                        VerificarCamerasNaoVinculadas(acesso, acessoPapel);
                        //Não registra imagens
                        repAcessoVeiculo.Salvar(acesso);
                        return json;
                    }
                }

                //Tem câmera(s) vinculada(s) a controladora porém ela/elas NÃO está/estão ativa(s) no sistema
                if (cameras.Count(c => c.Ativo == false) == cameras.Count())
                {
                    if (acessoPapel.StatusAcesso == StatusAcesso.OCO_UR_ACESSO_LIBERADO)
                    {
                        json = Newtonsoft.Json.JsonConvert.SerializeObject(true);
                    }
                    else
                    {
                        json = Newtonsoft.Json.JsonConvert.SerializeObject(false);
                    }
                    VerificarPermissaoSemLPR(ref acesso, controladora, cameras, true, acessoPapel);
                    return json;
                }
                acesso.AcessoPapel = acessoPapel;
                acesso.Leitora = leitora;
                acesso.StatusAcesso = StatusAcesso.OCO_UR_ACESSO_LIBERADO;
                acesso.CamerasImagens = new List<CameraImagem>();

                //A partir daqui existem 1 ou mais câmeras vinculadas à controladora
                acesso = await ObterImagensAvigilonACC(acesso, cameras);
                acesso.DataRegistro = DateTime.Now;

                //Quando não há câmeras online ou o sistema da Avigilon estejam offline
                if (TotalDevices == 0)
                {
                    var autorizacaoVeiculo = LogCamerasOffline(ref acesso);
                    json = Newtonsoft.Json.JsonConvert.SerializeObject(autorizacaoVeiculo);
                    return json;
                }
                //Não identificou placas
                if (LPR.Count == 0)
                {
                    json = Newtonsoft.Json.JsonConvert.SerializeObject(false);
                    return json;
                }
                //Caso obteve uma imagem ou mais
                if (acesso != null)
                {
                    //Verificar com o LPR se tem a permissão
                    if (acesso.Controladora.ValidaLPR)
                    {
                        var autorizacaoVeiculoGrupoLeitora = false;

                        IRepositorioVeiculo repVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioVeiculo>(UnidadeTrabalho);
                        var veiculo = repVeiculo.ObterTodos().Where(v => v.Placa.Replace("-", "") == placa).FirstOrDefault();

                        VeiculoCredencialVeiculo veiculoCredencialVeiculo = null;
                        //Veículo não cadastrado
                        if (veiculo == null)
                        {
                            acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_ENCONTRADO_BASE_DADOS;
                            LogAcessoVeiculo(acesso, acesso.Controladora, false);
                            json = Newtonsoft.Json.JsonConvert.SerializeObject(false);
                            return json;
                        }
                        else
                        {
                            //Veículo não encontrado no banco de dados
                            IRepositorioVeiculoCredencialVeiculo repVeiculoCredencialVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioVeiculoCredencialVeiculo>(UnidadeTrabalho);
                            veiculoCredencialVeiculo = repVeiculoCredencialVeiculo.ObterTodos().Where(vcv => vcv.Veiculo.Codigo == veiculo.Codigo).SingleOrDefault();

                            //Condutor não possui permissão na credencial
                            if (veiculoCredencialVeiculo != null)
                            {
                                acesso.VeiculoCredencialVeiculo = veiculoCredencialVeiculo;
                            }

                            if (veiculoCredencialVeiculo == null)
                            {
                                //Veículo sem credencial
                                acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_CREDENCIAL_INEXISTENTE;
                                LogAcessoVeiculo(acesso, acesso.Controladora, false);
                                json = Newtonsoft.Json.JsonConvert.SerializeObject(false);
                                return json;
                            }
                            else if (veiculoCredencialVeiculo.CredencialVeiculo.Situacoes.LastOrDefault().Status != StatusCredencial.Impressa)
                            {
                                //Credencial não impressa
                                acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_CREDENCIAL_NAO_IMPRESSA;
                                LogAcessoVeiculo(acesso, acesso.Controladora, false);
                                json = Newtonsoft.Json.JsonConvert.SerializeObject(false);
                                return json;
                            }
                            else if (veiculoCredencialVeiculo.CredencialVeiculo.DataVencimento < DateTime.Now)
                            {
                                //Credencial Expirada
                                acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_DATA_ACESSO_TEMPORARIO_EXPIRADO;
                                LogAcessoVeiculo(acesso, acesso.Controladora, false);
                                json = Newtonsoft.Json.JsonConvert.SerializeObject(false);
                                return json;
                            }

                            IRepositorioAcessoTemporarioVeiculo repAcessotemporarioVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioAcessoTemporarioVeiculo>(UnidadeTrabalho);
                            IRepositorioLeitora repLeitora = Fabrica.Instancia.ObterRepositorio<IRepositorioLeitora>(UnidadeTrabalho);


                            //var acessoTemporarioVeiculo = repAcessotemporarioVeiculo.ObterTodos().Where(atv => atv.VeiculoCredencialVeiculo.Codigo == veiculoCredencialVeiculo.Codigo).SingleOrDefault();

                            ////Tratamento permissão grupo leitora
                            //var Leitora = repLeitora.ObterTodos().Where(l => l.Codigo == IdLeitora).SingleOrDefault();
                            ////acessoTemporarioVeiculo.GrupoLeitora
                            //var PermissaoLeitora = false;
                            //for (int i = 0; i < acessoTemporarioVeiculo.GrupoLeitora.Leitoras.Count; i++)
                            //{
                            //    if (acessoTemporarioVeiculo.GrupoLeitora.Leitoras[i].Codigo == Leitora.Codigo)
                            //    {
                            //        PermissaoLeitora = true;
                            //    }
                            //}
                            //if (!PermissaoLeitora)
                            //{
                            //    //Se não houver permissão de acesso a leitora
                            //    //Log de não acesso a Leitora
                            //    acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_GRUPO_LEITORA;
                            //    LogAcessoVeiculo(acesso, acesso.Controladora, false);
                            //    json = Newtonsoft.Json.JsonConvert.SerializeObject(false);
                            //    return json;
                            //}
                        }

                        //if (veiculoCredencialVeiculo.CredencialVeiculo.Condutor.PessoaFisica.Codigo != acesso.AcessoPapel.Papel.PessoaFisica.Codigo)
                        //{
                        //    //Log de Pessoa não condiz com a credencial utilizada
                        //    //Não colocado no LOG
                        //    acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_GRUPO_LEITORA;
                        //    json = Newtonsoft.Json.JsonConvert.SerializeObject(false);
                        //    return json;
                        //}

                        acesso.AcessoPapel.StatusAcesso = StatusAcesso.OCO_UR_ACESSO_LIBERADO;

                        //Papel não tem acesso, veículo no banco de dados
                        if (acesso.AcessoPapel.StatusAcesso != StatusAcesso.OCO_UR_ACESSO_LIBERADO)
                        {
                            //Log de acesso não liberado OCR Ativo
                            //Verificar acesso veiculo ao grupo de leitora
                            if (autorizacaoVeiculoGrupoLeitora)
                            {
                                acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_PAPEL_NAO_AUTORIZADO_CANCELA;
                            }
                            else
                            {
                                acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_PAPEL_NAO_AUTORIZADO_VEICULO_NAO_AUTORIZADO;
                            }

                            LogAcessoVeiculo(acesso, acesso.Controladora, false);
                            json = Newtonsoft.Json.JsonConvert.SerializeObject(false);
                            return json;
                        }

                        //Verificar se a placa está acima do parâmetro do LPR
                        parametroPrecisao = 0;
                        List<CameraImagem> acimaLPR = new List<CameraImagem>();
                        List<CameraImagem> abaixoLPR = new List<CameraImagem>();

                        //Se não houver câmeras que valida LPR - Inteligência para desativar e ativar a validação de veículos
                        //if(cameras.Count  em que o valor de estiverem com o parametro.Valor != "1" for igual ao cameras.Count){
                        //acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_ATIVA_LPR_INATIVO_PARAMETRO;
                        //LogAcessoVeiculo(acesso, acesso.Controladora, true);
                        //json = Newtonsoft.Json.JsonConvert.SerializeObject(true);
                        //return true;
                        //}

                        for (int i = 0; i < LPR.Count; i++)
                        {
                            var precisao = float.Parse(acesso.CamerasImagens.Where(c => c.Camera.Nome == LPR[i].Camera.Nome).Single().Camera.Precisao, CultureInfo.InvariantCulture);
                            if (LPR[i].PrecisaoImage > precisao)
                            {
                                acimaLPR.Add(LPR[i]);
                            }
                            else
                            {
                                abaixoLPR.Add(LPR[i]);
                            }
                        }
                        //Calculo da maior precisão aferida entre as duas câmeras do sistema para colocar na mensagem do log
                        //Se pelo menos uma placa aferida estiver acima do parametro
                        if (acimaLPR.Count > 0)
                        {
                            var precisaoAnterior = 0f;
                            for (int i = 0; i < acimaLPR.Count; i++)
                            {
                                if (acimaLPR[i].PrecisaoImage > precisaoAnterior)
                                {
                                    precisaoAnterior = acimaLPR[i].PrecisaoImage;
                                    maiorPrecisao = acimaLPR[i].PrecisaoImage;
                                    placaMaiorPrecisao = acimaLPR[i].PlacaVeiculo;
                                    parametroPrecisao = float.Parse(acesso.CamerasImagens.Where(c => c.Camera.Nome == acimaLPR[i].Camera.Nome).Single().Camera.Precisao, CultureInfo.InvariantCulture);
                                }
                            }

                            acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_AUTORIZADO_LPR_ATIVO_ACIMA;
                            LogAcessoVeiculo(acesso, acesso.Controladora, true);
                            json = Newtonsoft.Json.JsonConvert.SerializeObject(true);
                            return json;
                        }
                        else
                        {
                            //Verificar a maior precisão entre as câmeras 
                            var precisaoAnterior = 0f;
                            for (int ix = 0; ix < abaixoLPR.Count; ix++)
                            {
                                if (abaixoLPR[ix].PrecisaoImage > precisaoAnterior)
                                {
                                    precisaoAnterior = abaixoLPR[ix].PrecisaoImage;
                                    maiorPrecisao = abaixoLPR[ix].PrecisaoImage;
                                    placaMaiorPrecisao = abaixoLPR[ix].PlacaVeiculo;
                                    parametroPrecisao = float.Parse(acesso.CamerasImagens.Where(c => c.Camera.Nome == abaixoLPR[ix].Camera.Nome).Single().Camera.Precisao, CultureInfo.InvariantCulture);
                                }
                            }
                            //Pegar a maior precisao e setar no Precisao e Precisao
                            acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_LPR_ATIVO_ABAIXO;
                            LogAcessoVeiculo(acesso, acesso.Controladora, true);
                            json = Newtonsoft.Json.JsonConvert.SerializeObject(false);
                            return json;
                        }
                    }
                    else
                    {
                        var permissaoGrupoLeitoras = true;
                        //Validar o acesso baseado no grupo de leitoras
                        if (!permissaoGrupoLeitoras)
                        {
                            acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_GRUPO_LEITORA;
                            await Task.Run(() =>
                            {
                                LogAcessoVeiculo(acesso, acesso.Controladora, false);
                            }).ConfigureAwait(false);
                            json = Newtonsoft.Json.JsonConvert.SerializeObject(false);
                            return json;
                        }
                        //Se tiver acesso ao grupo de leitoras
                        var liberacao = VerificarPermissaoSemLPR(ref acesso, controladora, cameras, false);
                        //repAcessoVeiculo.Salvar(acesso);
                        json = Newtonsoft.Json.JsonConvert.SerializeObject(liberacao);
                        return json;
                    }
                }
                else
                {
                    return await Task.Run(() =>
                    {
                        json = Newtonsoft.Json.JsonConvert.SerializeObject(false);
                        return json;
                    });
                }
            }
            catch (Exception e)
            {
                //Log de erro
                erro = e.Message;

                Controladora controladora = await Task.Run(() =>
                {
                    IRepositorioControladora repControladora = Fabrica.Instancia.ObterRepositorio<IRepositorioControladora>(UnidadeTrabalho);
                    return repControladora.ObterTodos().Where(x => x.Codigo == IpControladora).SingleOrDefault();
                });

                AcessoVeiculo acesso = new AcessoVeiculo();
                acesso.Controladora = controladora;

                IRepositorioControleAcesso repAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioControleAcesso>(UnidadeTrabalho);
                var acessoPapel = repAcesso.ObterPorId(idAcesso);
                acesso.AcessoPapel = acessoPapel;

                acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_ERRO;

                LogAcessoVeiculo(acesso, acesso.Controladora, false);
                
                json = Newtonsoft.Json.JsonConvert.SerializeObject(false);
                return json;
            }
        }
        /// <summary>
        /// Solicita informações de Cameras, placas e imagens da controladora
        /// </summary>
        /// <param name="acessoVeiculo"></param>
        /// <param name="cameras"></param>
        /// <returns></returns>
        public async Task<AcessoVeiculo> ObterImagensAvigilonACC(AcessoVeiculo acessoVeiculo, List<Camera> cameras)
        {
            try
            {

                for (int i = 0; i < cameras.Count; i++)
                {
                    camera = cameras[i];

                    if (cameras[i].Ativo)
                    {
                        CameraImagem PlacaPrecisao = null;
                        var acessoImage = await Task.Run(() =>
                         {

                             //acessoVeiculo = 
                             return ObterImagemACC(cameras[i], acessoVeiculo);
                         });



                        if (acessoImage.Controladora.ValidaLPR)
                        {
                            await Task.Run(() =>
                                 {
                                     CameraImagem PlacaEPrecisao = new CameraImagem(); ;
                                     var acessoVeiculoLPR = ObterLprAcc(cameras[i], ref acessoVeiculo, ref cameraImagem, "");
                                     if (acessoVeiculoLPR != null && acessoImage != null)
                                     {
                                         float precisaoAnterior = 0f;

                                         for (int ix = 0; ix < LPR.Count; ix++)
                                         {
                                             if (LPR[ix].PrecisaoImage > precisaoAnterior)
                                             {
                                                 precisaoAnterior = LPR[ix].PrecisaoImage;
                                                 precisao = LPR[ix].PrecisaoImage;
                                                 placa = LPR[ix].PlacaVeiculo;

                                             }
                                            //Fazer a comparação de placas registradas para colocar no LOG
                                            //Pegar a placa de maior precisão para o LOG

                                        }
                                     }
                                    //return PlacaEPrecisao;
                                }).ConfigureAwait(false);
                        }

                        //if(acessoImage != null && acessoImage.CamerasImagens.Count > 0)
                        //{
                        //    acessoImage.CamerasImagens[i].PrecisaoImage = precisao;
                        //    acessoImage.CamerasImagens[i].PlacaVeiculo = placa;

                        //}

                        acessoVeiculo = acessoImage;

                        //Verificar as placas obtidas na lista LPR

                    }
                }

                if (acessoVeiculo.CamerasImagens.Count > 0 || TotalDevices == 0)
                {
                    return acessoVeiculo;

                }
                else
                {
                    return null;
                }
            }

            catch (Exception exAvigilon)
            {
                Console.WriteLine(exAvigilon.Message);
                return null;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="controladora"></param>
        /// <param name="papel"></param>
        public void ObterLprAvigilonACC(Controladora controladora, Papel papel, ref CameraImagem cameraImagem, Camera camera)
        {
            //      ObterLprAcc(camera, ref acesso, controladora, "", cameraImagem);

        }

        /// <summary>
        /// Seta a  acesso baseado no grupo de leitoras
        /// </summary>
        /// <param name="acessoVeiculo"></param>
        /// <returns></returns>
        public bool SetAcessoVeiculo(AcessoVeiculo acessoVeiculo)
        {

            //Log de acesso


            //Valida acesso
            return true;
        }

        /// <summary>
        /// //Verifica a permissão baseado no LPR e faz o cadastro da tentativa de acesso
        /// </summary>
        /// <param name="acessoVeiculo"></param>
        /// /// <param name="indexCameraImagem"></param>
        /// <param name="controladora"></param>
        /// <returns></returns>
        public bool LogPermissaoLPR(
            AcessoVeiculo acessoVeiculo, int indexCameraImagem,
            Controladora controladora)
        {
            var parametroPrecisao = 100;

            //Verifica se o Veículo tem acesso ao GrupoLeitora que está tentando acessar
            //-------------------------------------------------------------------------// 

            var autorizacaoGrupoLeitora = ValidaPlaca(acessoVeiculo);

            if (!autorizacaoGrupoLeitora)
            {
                acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_GRUPO_LEITORA;
                acessoVeiculo.StatusAutorizacao = false;
            }
            else if (acessoVeiculo.CamerasImagens[indexCameraImagem].PrecisaoImage >= parametroPrecisao)
            {
                if (autorizacaoGrupoLeitora)
                {
                    //Permitido o acesso ao local, LPR ativo e acima do parametro do sistema
                    acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_AUTORIZADO_LPR_ATIVO_ACIMA;
                    acessoVeiculo.StatusAutorizacao = true;
                }
                else
                {
                    //Permitido o acesso ao local, LPR ativo e acima do parametro do sistema
                    acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_LPR_ATIVO_ACIMA;
                    acessoVeiculo.StatusAutorizacao = false;
                }
            }
            else
            {
                acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_LPR_ATIVO_ABAIXO;
                acessoVeiculo.StatusAutorizacao = false;
            }

            Task.Run(() =>
            {
                LogAcessoVeiculo(acessoVeiculo, controladora, autorizacaoGrupoLeitora);
            }).ConfigureAwait(false);


            return autorizacaoGrupoLeitora;
        }

        /// <summary>
        /// //Verifica a permissão baseado em câmera Inativas e faz o cadastro da tentativa de acesso
        /// </summary>
        /// <param name="acessoVeiculo"></param>
        /// <param name="controladora"></param>
        ///<param name="acessoPapel"></param>
        /// <returns></returns>
        public bool VerificarCamerasNaoVinculadas(AcessoVeiculo acessoVeiculo, ControleAcesso acessoPapel)
        {
            //Câmeras não vinculadas a controladora
            if (acessoPapel.StatusAcesso == StatusAcesso.OCO_UR_ACESSO_LIBERADO)
            {
                var permissaoAcesso = true;
                acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERAS_NAO_VINCULADAS;

                LogAcessoVeiculo(acessoVeiculo, acessoVeiculo.Controladora, permissaoAcesso);
                return true;
            }
            else
            {
                var permissaoAcesso = false;
                acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_CAMERAS_NAO_VINCULADAS;

                LogAcessoVeiculo(acessoVeiculo, acessoVeiculo.Controladora, permissaoAcesso);
                return false;
            }
        }

        /// <summary>
        /// //Verifica a permissão baseado em câmeras Vinculadas Ativas ou Inativas e faz o cadastro da tentativa de acesso
        /// </summary>
        /// <param name="acessoVeiculo"></param>
        /// <param name="controladora"></param>
        ///<param name="cameras"></param>
        ///<param name="lpr"></param>
        /// <returns></returns>
        public bool VerificarPermissaoSemLPR(
            ref AcessoVeiculo acessoVeiculo,
            Controladora controladora, List<Camera> cameras, bool lpr, ControleAcesso acessoPapel = null)
        {

            //Somente câmeras inativas
            if (cameras != null && cameras.Count > 0 && cameras.Count(c => c.Ativo == false) == cameras.Count)
            {
                var permissaoAcesso = false;
                if (acessoPapel.StatusAcesso == StatusAcesso.OCO_UR_ACESSO_LIBERADO)
                {
                    acessoVeiculo.StatusAutorizacao = true;
                    acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_INATIVA;
                    LogAcessoVeiculo(acessoVeiculo, controladora, permissaoAcesso);
                    return true;
                }
                else
                {
                    acessoVeiculo.StatusAutorizacao = false;
                    acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_CAMERA_INATIVA;
                    LogAcessoVeiculo(acessoVeiculo, controladora, permissaoAcesso);
                    return false;
                }
            }

            //Há uma ou mais câmeras ativa(s)
            if (cameras != null && cameras.Count > 0)
            {
                if (acessoVeiculo.Controladora.ValidaLPR)
                {
                    //LPR Ativo, cameras ativas
                    if (cameras != null && cameras.Count(c => c.Ativo) > 0)
                    {
                        acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_INATIVA;
                        acessoVeiculo.StatusAutorizacao = true;
                        LogAcessoVeiculo(acessoVeiculo, acessoVeiculo.Controladora, true);
                        return true;
                    }
                    //LPR Ativo, camneras Inativas
                    else
                    {
                        acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_CAMERA_INATIVA;
                        acessoVeiculo.StatusAutorizacao = false;
                        LogAcessoVeiculo(acessoVeiculo, acessoVeiculo.Controladora, false);
                        return false;
                    }

                }
                else
                {
                    //LPR Inativo, cameras Ativas
                    if (acessoPapel.StatusAcesso == StatusAcesso.OCO_UR_ACESSO_LIBERADO)
                    {
                        acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_ATIVA_LPR_INATIVO;
                        acessoVeiculo.StatusAutorizacao = true;
                        LogAcessoVeiculo(acessoVeiculo, controladora, true);
                        return true;
                    }
                    //LPR InAtivo, cameras Ativas
                    else
                    {
                        acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_CAMERA_ATIVA_LPR_INATIVO;
                        acessoVeiculo.StatusAutorizacao = false;
                        LogAcessoVeiculo(acessoVeiculo, controladora, false);
                        return false;

                    }
                }
            }
            else
            {
                //camerasOff
                if (acessoPapel.StatusAcesso == StatusAcesso.OCO_UR_ACESSO_LIBERADO)
                {
                    acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_OFFLINE;
                    acessoVeiculo.StatusAutorizacao = true;
                    LogAcessoVeiculo(acessoVeiculo, controladora, true);
                    return true;
                }
                else
                {
                    acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_CAMERA_OFFLINE;
                    acessoVeiculo.StatusAutorizacao = true;
                    LogAcessoVeiculo(acessoVeiculo, controladora, true);
                    return false;
                }
            }
        }

        /// <summary>
        /// Log de tentativa de acesso onde as câmeras estão offline
        /// </summary>
        /// <param name="acessoVeiculo"></param>
        /// <param name="controladora"></param>
        ///<param name="cameras"></param>
        ///<param name="lpr"></param>
        /// <returns></returns>
        public bool LogCamerasOffline(
            ref AcessoVeiculo acessoVeiculo)
        {
            var permissaoAcesso = false;
            if (acessoVeiculo.AcessoPapel.StatusAcesso == StatusAcesso.OCO_UR_ACESSO_LIBERADO)
            {
                acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_OFFLINE;

                permissaoAcesso = true;
                LogAcessoVeiculo(acessoVeiculo, acessoVeiculo.Controladora, permissaoAcesso);
                return true;
            }
            else
            {
                acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_CAMERA_OFFLINE;
                LogAcessoVeiculo(acessoVeiculo, acessoVeiculo.Controladora, permissaoAcesso);
                return false;
            }
        }




        /// <summary>
        /// 
        /// </summary>
        /// <param name="camera"></param>
        /// <param name="acessoVeiculo"></param>
        /// <param name="controladora"></param>
        /// <param name="channelIdParam"></param>
        /// <param name="imagem"></param>
        /// <returns></returns>
        public AcessoVeiculo ObterImagemACC(Camera camera, AcessoVeiculo acessoVeiculo)
        {
            try
            {
                String hostname = "10.10.2.37";
                int port = 38880; //38888
                String username = "administrator";
                String password = "123@Mudar";
                String channelId = "0a14312e3030313838353063376335642e63616d30301a10f355e06e8ced443d908e9fc462336a12221098b315b7044c438a917f8f83c5d19591380a";
                const int paramentroEspera = 1000;
                const int nvrConnectWait = paramentroEspera; // (seconds)
                nomeDevice = camera.Nome;
                IDataStore connectedDataStore = null;
                // var retorno = "";
                //-----Init Control Center

                if (!IPAddress.TryParse(hostname, out IPAddress addr))
                {
                    return null;
                    throw new Exception("IP Address is invalid.");

                }

                Debug.WriteLine("Init Control Center");

                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();
                AvigilonSdk sdk = new AvigilonSdk();

                SdkInitParams initParams = new SdkInitParams(
                    AvigilonSdk.MajorVersion,

                    AvigilonSdk.MinorVersion)
                {
                    //Era False
                    ServiceMode = true,
                    AutoDiscoverNvrs = true
                };
                initParams.SdkPath = urlBase + "packages\\AvigilonSdkDotNet.dll";
                controlCenter = sdk.CreateInstance(initParams);


                if (controlCenter == null)
                {
                    return null;

                }
                controlCenter.NvrListAdd += new AvigilonDotNet.UuidHandler﻿(m_sharedAcc_NvrListAdd﻿﻿﻿);

                //CameraIP
                IPEndPoint endpoint = new IPEndPoint(addr, port);
                DateTime waitEnd = DateTime.Now + new TimeSpan(0, 0, nvrConnectWait);


                //controlCenter.NvrListAdd += new AvigilonDotN﻿e﻿t.UuidHandler﻿﻿(m_sharedAcc_NvrListAd﻿d﻿﻿﻿﻿﻿﻿);
                AvigilonDotNet.AvgError error = controlCenter.AddNvr(endpoint);

                Stopwatch tempo = new Stopwatch();
                tempo.Start();
                while (nvr == null)
                {
                    nvr = controlCenter.GetNvr(endpoint.Address);

                    if (tempo.ElapsedMilliseconds > paramentroEspera * 1000) return null;
                }
                tempo.Stop();

                var result = nvr.Login(username, password);
                nvr.DeviceListAdd += new AvigilonDotNet.UuidHandler(NVR_DeviceLi﻿stAdd﻿);
                var tentativaDevices = 10;

                while (nvr.Devices.Count == 0 || tentativaDevices > 0)
                {

                    tentativaDevices--;

                    var camerasLigadas = 0;
                    if (nvr.Devices.Count > 0)
                    {
                        TotalDevices = nvr.Devices.Count;
                        camerasLigadas = nvr.Devices.Count(d => d.Connected);
                    }

                    if (camerasLigadas > 0)
                    {
                        var device = nvr.Devices.Where(d => d.DisplayName == camera.Nome).SingleOrDefault().Entities.Where(T => T.DisplayName == nomeDevice && T.Type == EntityType.Camera).SingleOrDefault();
                        if (device == null)
                        {
                            return null;
                        }
                        IRequestor m_requestor = controlCenter.CreateRequestor();
                        m_params.DeviceId = device.DeviceId;
                        m_params.NvrUuid = device.ReportingNvrUuid;
                        m_params.Timestamp = DateTime.Now.AddSeconds(-10);
                        m_params.DataStoreUuid = device.ReportingDataStoreUuid;
                        m_requestor.ResultFrameReceived += ImageRequestResultFrameHandler_;

                        //var eventTypes = new List<EventType>();
                        //eventTypes.Add(AvigilonDotNet.EventType.DeviceLprPlateFound);
                        Stopwatch stopImg0 = new Stopwatch();
                        stopImg0.Start();
                        m_requestor.Query(
                               1,
                               m_params);

                        // m_requestor.Query(1, m_params);
                        try

                        {
                            sdk.Shutdown();

                            Stopwatch stopImg = new Stopwatch();
                            stopImg.Start();
                            var tempoPassdo = stopImg.ElapsedMilliseconds;

                            while (MaxImageFound < 0)
                            {
                                if (stopImg.ElapsedMilliseconds > 15000) break;

                            }

                            MaxImageFound = int.MinValue;
                            stopImg0.Stop();
                            stopImg.Stop();
                            cameraImagem.AcessoVeiculo = acessoVeiculo;
                            cameraImagem.Camera = camera;
                            acessoVeiculo.CamerasImagens.Add(cameraImagem);
                            statusCameras.Add(true);
                            //if(acessoVeiculo.Controladora != null)
                            //{
                            //    if (acessoVeiculo.Controladora.ValidaLPR)
                            //    {
                            //        acessoVeiculo = ObterLprAcc(camera, ref acessoVeiculo, ref cameraImagem, "");
                            //        cameraImagem.PrecisaoImage = precisao;
                            //        cameraImagem.PlacaVeiculo = placa;
                            //    }
                            //}


                            acessoVeiculo.DataRegistro = DateTime.Now;

                            return acessoVeiculo;
                        }
                        catch
                        {
                            return null;
                        }
                    }

                }
                if (nvr.Devices == null || nvr.Devices.Count(c => c.Connected) == 0 || nvr.Devices.Count == 0)
                {
                    TotalDevices = 0;
                    return acessoVeiculo;
                }
                else
                {
                    return null;
                }
                //return acessoVeiculo;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="camera"></param>
        /// <param name="acessoVeiculo"></param>
        /// <param name="cameraImagem"></param>
        /// <param name="channelIdParam"></param>
        /// <returns></returns>
        public AcessoVeiculo ObterLprAcc(Camera camera, ref AcessoVeiculo acessoVeiculo,
        ref CameraImagem cameraImagem,
          string channelIdParam)
        {
            String hostname = "10.10.2.37";
            int port = 38880; //38888
            String username = "administrator";
            String password = "123@Mudar";
            String channelId = "0a14312e3030313838353063376335642e63616d30301a10f355e06e8ced443d908e9fc462336a12221098b315b7044c438a917f8f83c5d19591380a";
            const int paramentroEspera = 1000;
            const int nvrConnectWait = paramentroEspera; // (seconds)
            nomeDevice = camera.Nome;
            IDataStore connectedDataStore = null;
            // var retorno = "";
            //-----Init Control Center

            if (!IPAddress.TryParse(hostname, out IPAddress addr))
            {
                return null;
                throw new Exception("IP Address is invalid.");

            }

            Debug.WriteLine("Init Control Center");

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            AvigilonSdk sdk = new AvigilonSdk();

            SdkInitParams initParams = new SdkInitParams(
                AvigilonSdk.MajorVersion,

                AvigilonSdk.MinorVersion)
            {
                //Era False
                ServiceMode = true,
                AutoDiscoverNvrs = true
            };
            initParams.SdkPath = urlBase + "\\packages\\AvigilonSdkDotNet.dll";
            controlCenter = sdk.CreateInstance(initParams);


            if (controlCenter == null)
            {
                return null;
            }

            //CameraIP
            IPEndPoint endpoint = new IPEndPoint(addr, port);
            DateTime waitEnd = DateTime.Now + new TimeSpan(0, 0, nvrConnectWait);


            controlCenter.NvrListAdd += new AvigilonDotN﻿e﻿t.UuidHandler﻿﻿(m_sharedAcc_NvrListAd﻿d﻿﻿﻿﻿﻿﻿);
            AvigilonDotNet.AvgError error = controlCenter.AddNvr(endpoint);


            //nvr.DeviceListAdd += new AvigilonDotNet.UuidHandler(NVR_DeviceLi﻿stAdd﻿);
            Stopwatch tempo = new Stopwatch();
            tempo.Start();
            while (nvr == null)
            {
                nvr = controlCenter.GetNvr(endpoint.Address);
                if (tempo.ElapsedMilliseconds > paramentroEspera * 500) return null;
            }
            tempo.Stop();


            var result = nvr.Login(username, password);

            while (nvr.Devices != null)
            {
                if (nvr.Devices.Count(d => d.Connected == false) == nvr.Devices.Count)
                {
                    return null;
                }
                TotalDevices = nvr.Devices.Count(c => c.Connected);

                if (TotalDevices > 0)
                {
                    try

                    {
                        var device = nvr.Devices.Where(d => d.DisplayName == camera.Nome).SingleOrDefault().Entities.Where(T => T.DisplayName == camera.Nome).SingleOrDefault();
                        var devices = nvr.Devices.Where(d => d.DisplayName == camera.Nome).ToList();
                        List<string> ids = new List<string>();
                        for (int i = 0; i < devices.Count; i++)
                        {
                            ids.Add(devices[i].Entities.Where(e => e.Type == EntityType.Camera).SingleOrDefault().DeviceId);
                        }
                        var eventTypes = new List<EventType>();

                        eventTypes.Add(AvigilonDotNet.EventType.DeviceLprPlateFound);
                        eventTypes.Add(AvigilonDotNet.EventType.DeviceLprMatch);
                        eventTypes.Add(AvigilonDotNet.EventType.DeviceLprPlateLost);
                        //-------------GET PLR------------------------------ 
                        AvigilonDotNet.IQuery query;

                        query = nvr.QueryEventsDeviceId(
                              DateTime.Now.AddMinutes(-1),
                              DateTime.Now,
                              AvigilonDotNet.EventLinkMode.Closed | AvigilonDotNet.EventLinkMode.Opened |
                              AvigilonDotNet.EventLinkMode.Single | AvigilonDotNet.EventLinkMode.Undefined,
                              false, ids);

                        //-------------Pegar Placa ------------------------------
                        query.ResultReceived += ImageLPRResultFrameHandler_;

                        var vgError = query.Execute();

                        // m_requestor.Query(1, m_params);
                        while (MaxPlateFound > 0)
                        {

                        }

                        var indexLPR = 0;
                        if (LPR.Count > 0 && acessoVeiculo.CamerasImagens.Count > 0)
                        {
                            var precisaoAnterior = 0f;

                            for (int ix = 0; ix < LPR.Count; ix++)
                            {
                                if (LPR[ix].PrecisaoImage > precisaoAnterior)
                                {
                                    acessoVeiculo.CamerasImagens.Where(c => c.Camera.Nome == camera.Nome).Single().PlacaVeiculo = LPR[indexLPR].PlacaVeiculo;
                                    acessoVeiculo.CamerasImagens.Where(c => c.Camera.Nome == camera.Nome).Single().PrecisaoImage = LPR[indexLPR].PrecisaoImage;
                                }
                            }
                        }


                        sdk.Shutdown();

                        //cameraImagem.AcessoVeiculo = acessoVeiculo;
                        //cameraImagem.Camera = camera;
                        //cameraImagem.CaminhoImagem = paramentroUrlCaminhoImg + "_CAM_" + camera.Nome + "_" + DateTime.Now.ToString() + ".jpg";

                        //statusCameras.Add(true);
                        //if (acessoVeiculo.Controladora != null)
                        //{
                        //    if (acessoVeiculo.Controladora.ValidaLPR)
                        //    {
                        //        var placaResult = ObterLprAcc(camera, ref acesso, ref cameraImagem, "");
                        //       
                        //    }
                        //}

                        //acessoVeiculo.CamerasImagens.Add(cameraImagem);
                        return acessoVeiculo;
                    }
                    catch (Exception exLPR)
                    {
                        Console.WriteLine(exLPR.Message);
                        return null;
                    }
                }

            }
            stopwatch.Stop();

            if (stopwatch.ElapsedMilliseconds / 1000 > nvrConnectWait)
            {
                return null;
            }


            switch (result)
            {
                case LoginResult.Successful:
                    Debug.WriteLine("Login  - successfuly");
                    //Log Conexão con sucesso
                    connectedDataStore = nvr;

                    break;
                case LoginResult.ErrAlreadyLoggedIn:
                    Debug.WriteLine("Login - already logged in");
                    //Log já logado
                    connectedDataStore = nvr;
                    break;
                case LoginResult.ErrBadPassword:
                    Debug.WriteLine("The given password is not valid.");
                    //Log erro de senha
                    throw new Exception("The given password is not valid.");
                default:
                    //Log Erro inexperado ao logar no NVR
                    Debug.WriteLine("Unexpected error occurred while logging in to the NVR.");
                    throw new Exception("Unexpected error occurred while logging in to the NVR.");
            }

            return null;
        }

        private void ImageRequestResultFrameHandler_(uint requestId, AvgError error, IFrame frame)
        {
            var fram = frame;
            //Pegar todas as imagens
            if (frame != null)
            {
                //var imagemPath = paramentroUrlCaminhoImg + "\\Img_" + DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".jpg";
                var imagemPath = "C:\\Users\\denis.nogueira\\Downloads\\Img_" + DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".jpg";
                cameraImagem.CaminhoImagem = imagemPath;
                ImagensGravadas.Add(imagemPath);
                IFrameImage m_frameImage = frame as AvigilonDotNet.IFrameImage;

                AvigilonDotNet.IFrameImageRaw fir = m_frameImage as AvigilonDotNet.IFrameImageRaw;
                MaxImageFound = 1;

                Task.Run(() =>
                {
                    imagem = MakeBitmapFromImageRaw(fir);
                    imagem.Save(imagemPath);
                }).ConfigureAwait(false);

            }
            else
            {
                String errMsg = "Request " + requestId.ToString() + " has failed, err = " + error.ToString();

            }

        }



        private void ImageLPRResultFrameHandler_(IQueryResult result, QueryState state)
        {

            IQueryRe﻿sultEve﻿nts﻿﻿﻿﻿﻿﻿﻿﻿﻿ eventResult = (IQueryRe﻿sultEve﻿nts﻿﻿﻿﻿﻿﻿﻿﻿﻿)result;

            MaxPlateFound = eventResult.Results.Count;
            IRepositorioCameraImagem repCameraImagem = Fabrica.Instancia.ObterRepositorio<IRepositorioCameraImagem>(UnidadeTrabalho);
            
            //LPR.RemoveAll(p => p.Codigo >= 0);
            //Remover as placas que foram já gravadas no banco de dados com algum tratamento

            //DateTime.Now.AddMinutes(-1),
            for (int i = 0; i < eventResult.Results.Count; i++)
            {
                if (eventResult.Results[i].originalEvent != null)
                {
                    //  typeof(foo).IsAssignableFrom(iron.GetType())
                    var devicename = ((AvigilonDotNet.IEventCategoryDevice)eventResult.Results[i].originalEvent).DeviceName;
                    var tipo = eventResult.Results[i].originalEvent.GetType().ToString();
                    if (tipo.Contains("DeviceLprPlateFound"))
                    {

                        lpr = (IEventDeviceLprPlateFound)eventResult.Results[i].originalEvent;
                        CameraImagem cameraImagem = 
                        repCameraImagem.ObterTodos().Where(p => p.PlacaVeiculo.Replace("-", "") == lpr.LicensePlateString && 
                        p.AcessoVeiculo.DataRegistro >= DateTime.Now.AddMinutes(-1)).SingleOrDefault();
                        //Placa ainda não tratada, ou seja, não houve tentativa de acesso com o veículo desta placa
                        if (cameraImagem == null)
                        {
                            LPR.Add(new CameraImagem
                            {
                                PlacaVeiculo = lpr.LicensePlateString,
                                PrecisaoImage = lpr.Confidence * 100,
                                Camera = new Camera
                                {
                                    Nome = devicename
                                }
                            });
                            placa = lpr.LicensePlateString;
                            precisao = lpr.Confidence * 100;
                        }
                    }
                    else if (tipo.Contains("DeviceLprPlateMatch"))
                    {
                        var lpr = (IEventDeviceLprMatch)eventResult.Results[i].originalEvent;
                        placa = lpr.DetectedLicensePlate;
                       
                        CameraImagem cameraImagem =
                        repCameraImagem.ObterTodos().Where(p => p.PlacaVeiculo.Replace("-", "") == lpr.DetectedLicensePlate &&
                        p.AcessoVeiculo.DataRegistro >= DateTime.Now.AddMinutes(-1)).SingleOrDefault();
                        //Placa ainda não tratada, ou seja, não houve tentativa de acesso com o veículo desta placa
                        if (cameraImagem == null)
                        {
                            LPR.Add(new CameraImagem
                            {
                                PlacaVeiculo = lpr.DetectedLicensePlate,
                                PrecisaoImage = lpr.Confidence * 100,
                                Camera = new Camera
                                {
                                    Nome = devicename
                                }
                            });
                            placa = lpr.DetectedLicensePlate;
                            precisao = lpr.Confidence * 100;
                        }
                    }
                    else if (tipo.Contains("DeviceLprPlateLost"))
                    {
                        var lpr = (IEventDeviceLprPlateLost)eventResult.Results[i].originalEvent;
                        CameraImagem cameraImagem =
                        repCameraImagem.ObterTodos().Where(p => p.PlacaVeiculo.Replace("-", "") == lpr.LicensePlateString &&
                        p.AcessoVeiculo.DataRegistro >= DateTime.Now.AddMinutes(-1)).SingleOrDefault();
                        //Placa ainda não tratada, ou seja, não houve tentativa de acesso com o veículo desta placa
                        if (cameraImagem == null)
                        {
                            LPR.Add(new CameraImagem
                            {
                                PlacaVeiculo = lpr.LicensePlateString,
                                PrecisaoImage = lpr.Confidence * 100,
                                Camera = new Camera
                                {
                                    Nome = devicename
                                }
                            });
                            placa = lpr.LicensePlateString;
                            precisao = lpr.Confidence * 100;
                        }
                    }
                }
                MaxPlateFound--;
            }
        }


        private Bitmap MakeBitmapFromImageRaw(AvigilonDotNet.IFrameImageRaw rawFrame)
        {
            Rectangle imageRect = new Rectangle(0, 0, rawFrame.Size.Width, rawFrame.Size.Height);

            switch (rawFrame.DefaultPixelFormat)
            {
                case AvigilonDotNet.PixelFormat.Gray8:
                    {
                        Bitmap retVal = new Bitmap(
                            rawFrame.Size.Width,
                            rawFrame.Size.Height,
                            System.Drawing.Imaging.PixelFormat.Format8bppIndexed);

                        // Define palette for 8bit grayscale
                        System.Drawing.Imaging.ColorPalette pal = retVal.Palette;
                        for (int ix = 0; ix < pal.Entries.Length; ++ix)
                        {
                            pal.Entries[ix] = System.Drawing.Color.FromArgb(ix, ix, ix);
                        }
                        retVal.Palette = pal;

                        System.Drawing.Imaging.BitmapData bitmapData = retVal.LockBits(
                            imageRect,
                            System.Drawing.Imaging.ImageLockMode.WriteOnly,
                            System.Drawing.Imaging.PixelFormat.Format8bppIndexed);

                        byte[] byteArray = rawFrame.GetAsArray(
                            AvigilonDotNet.PixelFormat.Gray8,
                            (ushort)bitmapData.Stride);

                        System.Runtime.InteropServices.Marshal.Copy(
                            byteArray,
                            0,
                            bitmapData.Scan0,
                            byteArray.Length);
                        retVal.UnlockBits(bitmapData);

                        return retVal;
                    }

                case AvigilonDotNet.PixelFormat.RGB24:
                    {
                        Bitmap retVal = new Bitmap(
                            rawFrame.Size.Width,
                            rawFrame.Size.Height,
                            System.Drawing.Imaging.PixelFormat.Format24bppRgb);

                        System.Drawing.Imaging.BitmapData bitmapData = retVal.LockBits(
                            imageRect,
                            System.Drawing.Imaging.ImageLockMode.WriteOnly,
                            System.Drawing.Imaging.PixelFormat.Format24bppRgb);

                        byte[] byteArray = rawFrame.GetAsArray(
                            AvigilonDotNet.PixelFormat.RGB24,
                            (ushort)bitmapData.Stride);

                        System.Runtime.InteropServices.Marshal.Copy(
                            byteArray,
                            0,
                            bitmapData.Scan0,
                            byteArray.Length);
                        retVal.UnlockBits(bitmapData);

                        return retVal;
                    }

                case AvigilonDotNet.PixelFormat.RGB32:
                    {
                        Bitmap retVal = new Bitmap(
                            rawFrame.Size.Width,
                            rawFrame.Size.Height,
                            System.Drawing.Imaging.PixelFormat.Format32bppRgb);

                        System.Drawing.Imaging.BitmapData bitmapData = retVal.LockBits(
                            imageRect,
                            System.Drawing.Imaging.ImageLockMode.WriteOnly,
                            System.Drawing.Imaging.PixelFormat.Format32bppRgb);

                        byte[] byteArray = rawFrame.GetAsArray(
                            AvigilonDotNet.PixelFormat.RGB32,
                            (ushort)bitmapData.Stride);

                        System.Runtime.InteropServices.Marshal.Copy(
                            byteArray,
                            0,
                            bitmapData.Scan0,
                            byteArray.Length);
                        retVal.UnlockBits(bitmapData);

                        return retVal;
                    }

                default:
                    return null;
            }
        }

        /// <summary>
        /// Log de Acesso do Veículo
        /// </summary>
        /// <param name="acessoVeiculo"></param>
        /// <param name="controladora"></param>
        /// <param name="acesso"></param>
        public void LogAcessoVeiculo(
            AcessoVeiculo acessoVeiculo, Controladora controladora, bool acesso = false)
        {
            try
            {
                IRepositorioAcessoVeiculo repAcessoVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioAcessoVeiculo>(UnidadeTrabalho);
                IRepositorioCameraImagem repCameraImagem = Fabrica.Instancia.ObterRepositorio<IRepositorioCameraImagem>(UnidadeTrabalho);

                int parametro = 100;

                StringBuilder sb = new StringBuilder();
                sb.Append(acessoVeiculo.AcessoPapel.Papel.PessoaFisica.Nome + " teve acesso ");
                switch (acessoVeiculo.StatusAcesso)
                {
                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_AUTORIZADO_LPR_ATIVO_ACIMA:

                        sb.Append("liberado às " + acessoVeiculo.DataRegistro + " por possuir permissão de acesso ao local da leitora " + acessoVeiculo.Leitora.IDLeitora + " que estava com LPR ativo, ");

                        acessoVeiculo.StatusAutorizacao = true;
                        sb.Append("a placa aferida do veículo foi registrada como " + placa + " com a precisão aferida da placa de " + precisao + "%, ");
                        if (precisao > parametroPrecisao)
                        {
                            sb.Append("precisão registrada estava acima do nível de aceitável pelo sistema de Leitura de Placa que é de " + parametroPrecisao + "%.");

                        }
                        else if (precisao == parametroPrecisao)
                        {
                            sb.Append("precisão registrada estava igual ao nível de aceitável pelo sistema de Leitura de Placa que é de " + parametroPrecisao + "%.");
                        }

                        var observacaoLiberadoLPR = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoLiberadoLPR;
                        //UnidadeTrabalho.BeginTransaction();
                        Task.Run(() =>
                        {
                            repAcessoVeiculo.Salvar(acessoVeiculo);
                            //UnidadeTrabalho.Commit();
                            for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                            {
                                repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                            }

                        }).ConfigureAwait(false);

                        break;

                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_LPR_ATIVO_ACIMA:

                        sb.Append("negado às " + acessoVeiculo.DataRegistro + "por não possuir permissão de acesso ao local da leitora" + acessoVeiculo.Leitora.IDLeitora + " que estava com LPR ativo , ");
                        acessoVeiculo.StatusAutorizacao = true;
                        sb.Append(" a placa aferida do veículo foi registrada como " + placa + " com a precisão aferida da placa de " + precisao + "%, ");
                        if (precisao > parametroPrecisao)
                        {
                            sb.Append("precisão registrada estava acima do nível de aceitável pelo sistema de Leitura de Placa que é de " + parametroPrecisao + "%.");

                        }
                        else if (precisao == parametroPrecisao)
                        {
                            sb.Append("precisão registrada estava igual ao nível de aceitável pelo sistema de Leitura de Placa que é de " + parametroPrecisao + "%.");
                        }
                        var observacaoNegadoLPR = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoNegadoLPR;
                        Task.Run(() =>
                        {
                            repAcessoVeiculo.Salvar(acessoVeiculo);
                            //UnidadeTrabalho.Commit();
                            for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                            {
                                repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                            }

                        }).ConfigureAwait(false);
                        break;

                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_LPR_ATIVO_ABAIXO:
                        sb.Append("negado às " + acessoVeiculo.DataRegistro + "o que o veículo não foi identificado pelo sistema de Leitura de placas");
                        sb.Append(" a placa aferida do veículo foi registrada como " + placa + " com a precisão aferida da placa de " + precisao + "%, esta abaixo do parâmetro definido no sistema,");
                        sb.Append(" no local da leitora " + acessoVeiculo.Leitora.IDLeitora + " que estava com Sistema de leitura de placas ativo");

                        var observacaoVeiculoNaoIdentificado = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoVeiculoNaoIdentificado;
                        Task.Run(() =>
                        {
                            repAcessoVeiculo.Salvar(acessoVeiculo);
                            //UnidadeTrabalho.Commit();
                            for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                            {
                                repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                            }

                        }).ConfigureAwait(false);

                        break;


                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_GRUPO_LEITORA:
                        sb.Append("negado às " + acessoVeiculo.DataRegistro + "por não possuir permissão de acesso ao local da leitora" + acessoVeiculo.Leitora.IDLeitora + " que estava com Sistema de leitura de placas ativo, ");

                        sb.Append(" a placa aferida do veículo foi registrada como " + placa + " com a precisão aferida da placa de " + precisao + "%, ");

                        sb.Append("precisão registrada estava abaixo do nível de aceitável pelo sistema de Leitura de Placa que é de " + parametroPrecisao + "%.");
                        var observacaoNegadoGrupoLeitora = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoNegadoGrupoLeitora;
                        Task.Run(() =>
                        {
                            repAcessoVeiculo.Salvar(acessoVeiculo);
                            //UnidadeTrabalho.Commit();
                            for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                            {
                                repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                            }

                        }).ConfigureAwait(false);

                        break;


                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_INATIVA:


                        sb.Append("autorizado às " + acessoVeiculo.DataRegistro + " porque " + acessoVeiculo.AcessoPapel.Papel.PessoaFisica.Nome +
                             " possuia permissão de acesso ao local da leitora " + acessoVeiculo.Leitora.IDLeitora +
                             ", não há registro fotográfico, porque no momento da passagem do crachá, não havia câmera ativa no local");

                        var observacaoLiberadoPapelVeiculoCamOffLine = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoLiberadoPapelVeiculoCamOffLine;
                        Task.Run(() =>
                        {
                            repAcessoVeiculo.Salvar(acessoVeiculo);
                        }).ConfigureAwait(false);
                        break;

                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_CAMERA_INATIVA:


                        sb.Append("não autorizado às " + acessoVeiculo.DataRegistro + " porque " + acessoVeiculo.AcessoPapel.Papel.PessoaFisica.Nome +
                            "não possuia permissão de acesso ao local da leitora " + acessoVeiculo.Leitora.IDLeitora +
                            ", não há registro fotográfico do momento porque no momento da passagem do crachá, não havia câmera ativa no local");

                        var observacaoCamInativa = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoCamInativa;

                        Task.Run(() =>
                        {
                            repAcessoVeiculo.Salvar(acessoVeiculo);
                        }).ConfigureAwait(false);
                        break;


                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_ATIVA_LPR_INATIVO:
                        acessoVeiculo.StatusAutorizacao = true;

                        sb.Append("liberado às " + acessoVeiculo.DataRegistro + " no local da leitora " + acessoVeiculo.Leitora.IDLeitora +
                            ", sendo assim seu veículo não foi identificado, " +
                            "porém imagem foi tirada fotografica do carro no momento da passagem do crachá," +
                            " sistema de detecção de placas desligado");

                        var observacaoCamOnLineOCROff = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoCamOnLineOCROff;

                        Task.Run(() =>
                        {
                            repAcessoVeiculo.Salvar(acessoVeiculo);
                            //UnidadeTrabalho.Commit();
                            for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                            {
                                repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                            }

                        }).ConfigureAwait(false);
                        break;


                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_OFFLINE:
                        acessoVeiculo.StatusAutorizacao = true;
                        sb.Append("autorizado às " + acessoVeiculo.DataRegistro +
                             " possuia permissão de acesso ao local da leitora " + acessoVeiculo.Leitora.IDLeitora +
                            " , a autorização levou em conta apenas a permissão do acesso da pessoa  porque a câmera(s) está/estão offline, " +
                            "sendo assim seu veículo não foi identificado, porém autorizado");

                        var observacaoCameraOffline = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoCameraOffline;

                        Task.Run(() =>
                        {
                            repAcessoVeiculo.Salvar(acessoVeiculo);
                        }).ConfigureAwait(false);
                        break;

                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_CAMERA_OFFLINE:
                        sb.Append("não autorizado às " + acessoVeiculo.DataRegistro +
                            " não possuia permissão de acesso ao local da leitora " + acessoVeiculo.Leitora.IDLeitora +
                           " , a autorização levou em conta apenas a permissão do acesso da pessoa  porque a câmera(s) está/estão offline, " +
                            "sendo assim seu veículo não foi identificado e também não autorizado");


                        var observacaoNaoAutorizadoCameraOffline = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoNaoAutorizadoCameraOffline;
                        acessoVeiculo.StatusAutorizacao = false;
                        acessoVeiculo.CamerasImagens = null;

                        Task.Run(() =>
                        {
                            repAcessoVeiculo.Salvar(acessoVeiculo);
                        }).ConfigureAwait(false);
                        break;


                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERAS_NAO_VINCULADAS:
                        if (acesso)
                        {
                            acessoVeiculo.StatusAutorizacao = true;
                            sb.Append("autorizado ");
                        }
                        else
                        {
                            sb.Append("não autorizado ");
                        }

                        sb.Append("às " + acessoVeiculo.DataRegistro + ", pela validação de crachá," +
                            " não houve imagem fotografada no momento por não haver câmeras vinculadas a controladora "
                            + controladora.Modelo + ", sendo assim seu veículo não foi identificado");

                        var observacaoLiberadoPapelVeiculoBloqueado = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoLiberadoPapelVeiculoBloqueado;

                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        break;

                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_ACC_INATIVA_NEGADO:
                        sb.Append("negado às " + acessoVeiculo.DataRegistro + " porque o sistema Avigilon Control Center estava offline");
                        var observacaoAccOffline = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoAccOffline;

                        Task.Run(() =>
                        {
                            repAcessoVeiculo.Salvar(acessoVeiculo);
                            //UnidadeTrabalho.Commit();
                        }).ConfigureAwait(false);
                        break;

                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_DATA_ACESSO_TEMPORARIO_EXPIRADO:
                        sb.Append("negado às " + acessoVeiculo.DataRegistro + " porque a o veículo de placa " + placa +
                            " tentou acesso no local da leitora " + acessoVeiculo.Leitora.IDLeitora + " fora do seu período de permissão de acesso temporário, acesso expirado");

                        var observacaoAcessoExpirado = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoAcessoExpirado;

                        Task.Run(() =>
                        {
                            repAcessoVeiculo.Salvar(acessoVeiculo);
                            //UnidadeTrabalho.Commit();
                            for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                            {
                                repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                            }

                        }).ConfigureAwait(false);

                        break;
                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_PAPEL_NAO_AUTORIZADO_CANCELA:


                        //Verificar se o veículo tem acesso
                        sb.Append("negado às " + acessoVeiculo.DataRegistro + " com o veículo de placa " + acessoVeiculo.VeiculoCredencialVeiculo.Veiculo.Placa +
                            ", o/a condutor(a) tentou acesso no local da leitora " + acessoVeiculo.Leitora.IDLeitora + ", porém não possui acesso ao local");

                        var observacaoAcessoNegadoPapelVeiculo = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoAcessoNegadoPapelVeiculo;

                        Task.Run(() =>
                        {
                            repAcessoVeiculo.Salvar(acessoVeiculo);
                            //UnidadeTrabalho.Commit();
                            for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                            {
                                repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                            }

                        }).ConfigureAwait(false);

                        break;

                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_PAPEL_AUTORIZADO_VEICULO_NAO_AUTORIZADO:


                        //Verificar se o veículo tem acesso
                        sb.Append("negado às " + acessoVeiculo.DataRegistro + " porque o veículo de placa, " + acessoVeiculo.VeiculoCredencialVeiculo.Veiculo.Placa +
                            " tentou acesso no local da leitora " + acessoVeiculo.Leitora.IDLeitora + ", porém o veículo não possuia permissão de acesso ao local");

                        var observacaoAcessoAutorizadoPapelVeiculoNegado = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoAcessoAutorizadoPapelVeiculoNegado;

                        Task.Run(() =>
                        {
                            repAcessoVeiculo.Salvar(acessoVeiculo);
                            //UnidadeTrabalho.Commit();
                            for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                            {
                                repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                            }

                        }).ConfigureAwait(false);

                        break;

                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_PAPEL_NAO_AUTORIZADO_VEICULO_NAO_AUTORIZADO:


                        //Verificar se o veículo tem acesso
                        sb.Append("negado às " + acessoVeiculo.DataRegistro + " porque o veículo de placa " + acessoVeiculo.VeiculoCredencialVeiculo.Veiculo.Placa +
                            ", tentou acesso no local da leitora " + acessoVeiculo.Leitora.IDLeitora + ", porém não possui permissão ao local");

                        var observacaoAcessoNegadoPapelAcessoNegadoVeiculo = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoAcessoNegadoPapelAcessoNegadoVeiculo;

                        Task.Run(() =>
                        {
                            repAcessoVeiculo.Salvar(acessoVeiculo);
                            //UnidadeTrabalho.Commit();
                            for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                            {
                                repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                            }

                        }).ConfigureAwait(false);

                        break;

                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_CREDENCIAL_INEXISTENTE:

                        //Verificar se o veículo tem acesso
                        sb.Append("negado às " + acessoVeiculo.DataRegistro + " porque o veículo de placa " + placa +
                            " tentou acesso no local da leitora " + acessoVeiculo.Leitora.IDLeitora + ", porém o veículo não possuia credencial cadastrada, " +
                            " sendo assim o veículo teve seu acesso não autorizado");

                        var observacaoAcessoNegadoCredencialInexistente = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoAcessoNegadoCredencialInexistente;
                        Task.Run(() =>
                        {
                            repAcessoVeiculo.Salvar(acessoVeiculo);
                            //UnidadeTrabalho.Commit();
                            for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                            {
                                repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                            }

                        }).ConfigureAwait(false);

                        //repAcessoVeiculo.Salvar(acessoVeiculo);

                        break;
                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_ENCONTRADO_BASE_DADOS:

                        sb.Append("negado às " + acessoVeiculo.DataRegistro + " porque o veículo de placa " + placa +
                            " tentou acesso no local da leitora " + acessoVeiculo.Leitora.IDLeitora + ", porém o veículo não foi identificado na base de dados, " +
                            " sendo assim o veículo teve seu acesso não autorizado");

                        var observacaoAcessoNegadoVeiculoInexistente = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoAcessoNegadoVeiculoInexistente;
                        Task.Run(() =>
                        {
                            repAcessoVeiculo.Salvar(acessoVeiculo);
                            //UnidadeTrabalho.Commit();
                            for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                            {
                                repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                            }

                        }).ConfigureAwait(false);

                        //repAcessoVeiculo.Salvar(acessoVeiculo);

                        break;

                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_CREDENCIAL_NAO_IMPRESSA:

                        sb.Append("negado às " + acessoVeiculo.DataRegistro + " porque o veículo de placa " + placa +
                            " tentou acesso no local da leitora " + acessoVeiculo.Leitora.IDLeitora + ", porém o estado da credencial do veículo não era \"impressa\"" +
                            ", sendo assim o veículo teve seu acesso não autorizado");

                        var observacaoAcessoNegadoCredencialnaoImpressa = sb.ToString();

                        acessoVeiculo.Observacoes = observacaoAcessoNegadoCredencialnaoImpressa;
                        Task.Run(() =>
                        {
                            repAcessoVeiculo.Salvar(acessoVeiculo);
                            //UnidadeTrabalho.Commit();
                            for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                            {
                                repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                            }

                        }).ConfigureAwait(false);

                        //repAcessoVeiculo.Salvar(acessoVeiculo);

                        break;

                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_ERRO:

                        sb.Append("negado às " + acessoVeiculo.DataRegistro + " houve erro na aplicação \n Erro: " + erro);

                        var observacaoAcessoErro= sb.ToString();

                        acessoVeiculo.Observacoes = observacaoAcessoErro;
                        Task.Run(() =>
                        {
                            repAcessoVeiculo.Salvar(acessoVeiculo);
                            //UnidadeTrabalho.Commit();
                            for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                            {
                                repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                            }

                        }).ConfigureAwait(false);

                        //repAcessoVeiculo.Salvar(acessoVeiculo);

                        break;

                }

            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.Message);
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="json"></param>
        public ContentResult Json(string json)
        {
            return new ContentResult
            {
                Content = json,
                ContentType = "application/json"
            };

        }


        public bool UploadFTPSecure()
        {
            return true;

        }

        private void m_sharedAcc_NvrListAdd﻿(string uuid)
        {
            nvr = controlCenter.GetNvr(uuid);
            //nvr.DeviceListAdd += new AvigilonDotNet.UuidHandler(NVR_DeviceListAdd﻿);

            //String username = "administrator";
            //String password = "123@Mudar";

            //var result = nvr.Login(username, password);
            //if (result != AvigilonDotNet.LoginResult.Successful)
            //{
            //    return;
            //}

        }

        private DateTime LastDeviceAdded = DateTime.MinValue;
        public List<AvigilonDotNet.IEntity> dicTLCEntity = new List<AvigilonDotNet.IEntity>();
        private void NVR_DeviceListAdd(string deviceId)
        {
            //Log(LoggingLevel.INFO, "SDK NVR_DeviceListAdd START (Host {0})", Host);

            //LastDeviceAdded = DateTime.Now;

            AvigilonDotNet.IDevice device = nvr.GetDeviceById(deviceId);
            nvr.Devices.Add(device);
            if (device.DisplayName == nomeDevice)
            {
                for (int i = 0; i < device.Entities.Count; i++)
                {
                    if (device.Entities[i].Type == AvigilonDotNet.EntityType.Camera && device.Entities[i].DisplayName == nomeDevice)
                    {
                        dicTLCEntity.Add(device.Entities[i]);
                    }
                }
            }
        }


        /// <summary>
        /// Valida as placas existentes e compara com a placa do veículo Credenciado
        /// </summary>
        /// <param name="acessoVeiculo"></param>
        private bool ValidaPlaca(AcessoVeiculo acessoVeiculo)
        {
            float precisaoAnterior = 0f;
            float precisaoAtual = 0f;

            for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
            {
                //Verificar precisão das placas
                if (float.Parse(acessoVeiculo.CamerasImagens[i].Camera.Precisao) >= acessoVeiculo.CamerasImagens[i].PrecisaoImage)
                {
                    //Validação da 
                }
                else
                {

                }

                //Verificar se alguma placa passa do 
            }

            return true;
        }

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="acessoVeiculo"></param>
        ///// <param name="placa"></param>
        ///// <returns></returns>
        //private bool ValidaAcessoGrupoLeitora(AcessoVeiculo acessoVeiculo)
        //{

        //}
    }


}
