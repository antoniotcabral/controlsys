﻿using Controlsys.Dominio.Parametros;
using Controlsys.Infra;
using Controlsys.Repositorios.Parametros;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar treinamentoes.
    /// </summary>
    [ExtendController]
    public class TreinamentoController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /Treinamento/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.TreinamentoController.
        /// </summary>
        ///
        /// <param name="unidade">
        /// O(a) unidade.
        /// </param>
        public TreinamentoController(IUnidadeTrabalho unidade)
        {
            UnidadeTrabalho = unidade;
        }

        /// <summary>
        /// Retorna a página de treinamento.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Inclui um novo treinamento.
        /// </summary>
        ///
        /// <param name="treinamento">
        /// O(a) treinamento.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Incluir(Treinamento treinamento)
        {
            IRepositorioTreinamento repTreinamento = Fabrica.Instancia.ObterRepositorio<IRepositorioTreinamento>(UnidadeTrabalho);
            treinamento.DataRegistro = DateTime.Now;
            treinamento.Ativar();
            repTreinamento.Validar(treinamento, EstadoObjeto.Novo);
            repTreinamento.Salvar(treinamento);
            return Json(treinamento);

        }

        /// <summary>
        /// Altera um registro de treinamento.
        /// </summary>
        ///
        /// <param name="treinamento">
        /// O(a) treinamento.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Alterar(Treinamento treinamento)
        {
            IRepositorioTreinamento repTreinamento = Fabrica.Instancia.ObterRepositorio<IRepositorioTreinamento>(UnidadeTrabalho);
            Treinamento treinamentoA = repTreinamento.ObterPorId(treinamento.Codigo);
            treinamentoA.Descricao = treinamento.Descricao;
            treinamentoA.Nome = treinamento.Nome;
            treinamentoA.Numero = treinamento.Numero;
            treinamentoA.DiasValidade = treinamento.DiasValidade;
            repTreinamento.Validar(treinamentoA, EstadoObjeto.Alterado);
            repTreinamento.Atualizar(treinamentoA);
            return Json(treinamentoA);
        }

        /// <summary>
        /// Ativar inativar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult AtivarInativar(int codigo)
        {
            IRepositorioTreinamento repTreinamento = Fabrica.Instancia.ObterRepositorio<IRepositorioTreinamento>(UnidadeTrabalho);
            Treinamento treinamento = repTreinamento.ObterPorId(codigo);
            if (treinamento.Ativo)
            {
                treinamento.Inativar();
            }
            else
            {
                repTreinamento.Validar(treinamento, EstadoObjeto.Ativado);
                treinamento.Ativar();

            }
            
            repTreinamento.Atualizar(treinamento);
            return Json(treinamento);

        }

        /// <summary>
        /// Selecionar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Selecionar(int codigo)
        {
            IRepositorioTreinamento repTreinamento = Fabrica.Instancia.ObterRepositorio<IRepositorioTreinamento>(UnidadeTrabalho);

            Treinamento treinamento = repTreinamento.ObterPorId(codigo);
            return Json(treinamento);
        }

        /// <summary>
        /// Retorna todos os objetos do tipo Treinamento.
        /// </summary>
        ///
        /// <param name="apenasAtivos">
        /// true to apenas ativos.
        /// </param>
        ///
        /// <returns>
        /// Detalhes do tipo de objeto ligado ao controller no formato JSON.
        /// </returns>
        public ActionResult ObterTodos(bool apenasAtivos = false)
        {
            IRepositorioTreinamento repTreinamento = Fabrica.Instancia.ObterRepositorio<IRepositorioTreinamento>(UnidadeTrabalho);

            IQueryable<Treinamento> treinamentos = repTreinamento.ObterTodos();

            if (apenasAtivos)
            {
                treinamentos = treinamentos.Where(t => t.Ativo == true);
            }

            return Json(treinamentos.ToList());
        }
    }
}
