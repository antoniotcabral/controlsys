﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Controlsys.Infra;
using Controlsys.Repositorios.Seguranca;
using Globalsys;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar paginas.
    /// </summary>
    [ExtendController]
    public class PaginaController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /Pagina/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.PaginaController.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public PaginaController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        /// <summary>
        /// Retorna todos os objetos do tipo Pagina.
        /// </summary>
        ///
        /// <returns>
        /// Detalhes do tipo de objeto ligado ao controller no formato JSON.
        /// </returns>
        public ActionResult ObterTodos()
        {
            IRepositorioPagina repPagina = Fabrica.Instancia.ObterRepositorio<IRepositorioPagina>(UnidadeTrabalho);

            return Json(repPagina.ObterTodos().Where(p => !p.PaginasFilhas.Any())
                                              .Select(p => new
                                              {
                                                  p.Codigo,
                                                  p.Nome,
                                                  p.Icone,
                                                  p.Ordem,
                                                  p.TipoPagina
                                              })
                                              .ToArray());
        }
    }
}
