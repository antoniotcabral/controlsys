﻿using Controlsys.Dominio.Parametros;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Parametros;
using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using Ext.Net;
using System.Web.Script.Serialization;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class ModeloCredencialController : Controller, IControllerBase
    {
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        public ModeloCredencialController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult ObterCamposFrente()
        {
            List<object> list = new List<object>
            {
                new { Codigo = "placa1Frente", Nome = "PLACA 1" },
                new { Codigo = "placa2Frente", Nome = "PLACA 2" },
                new { Codigo = "validadeFrente", Nome = "VALIDADE" },
                new { Codigo = "numeroFrente", Nome = "NÚMERO REGISTRO" },
                new { Codigo = "dataInspecaoFrente", Nome = "DATA INSPEÇÃO" },
                new { Codigo = "indicadorCorporativoFrente", Nome = "CORPORATIVO (S/N)" },
                new { Codigo = "condutorNomeFrente", Nome = "CONDUTOR" },
                new { Codigo = "empresaNomeFrente", Nome = "EMPRESA" },
                new { Codigo = "areaFrente", Nome = "ÁREA" }
            };
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public ActionResult ObterCamposVerso()
        {
            List<object> list = new List<object>
            {
                new { Codigo = "placa1Verso", Nome = "PLACA 1" },
                new { Codigo = "placa2Verso", Nome = "PLACA 2" },
                new { Codigo = "validadeVerso", Nome = "VALIDADE" },
                new { Codigo = "numeroVerso", Nome = "NÚMERO REGISTRO" },
                new { Codigo = "dataInspecaoVerso", Nome = "DATA INSPEÇÃO" },
                new { Codigo = "indicadorCorporativoVerso", Nome = "CORPORATIVO (S/N)" },
                new { Codigo = "condutorNomeVerso", Nome = "CONDUTOR" },
                new { Codigo = "empresaNomeVerso", Nome = "EMPRESA" },
                new { Codigo = "areaVerso", Nome = "ÁREA" }
            };
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public ActionResult ObterFontes()
        {
            List<object> list = new List<object>
            {
                new { Codigo = 1, Nome = "Arial" },
                new { Codigo = 2, Nome = "Calibri" },
                new { Codigo = 3, Nome = "Tahoma" },
                new { Codigo = 4, Nome = "Times New Roman" },
                new { Codigo = 5, Nome = "TKTypeBold" },
                new { Codigo = 6, Nome = "Verdana" }
            };
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public ActionResult ListaTipoCredencial()
        {
            Dictionary<int, string> dic = new Dictionary<int, string>();

            foreach (int item in Enum.GetValues(typeof(TipoCredencial)))
            {
                dic.Add(item, ((TipoCredencial)item).ObterDescricaoEnum());
            }
            return Json(dic.Select(d => new
            {
                Codigo = d.Key,
                Descricao = d.Value
            }).ToList());

        }

        public ActionResult ObterTodos(TipoCredencial? tipoCredencial, bool apenasAtivos = false)
        {
            IRepositorioModeloCredencial rep = Fabrica.Instancia.ObterRepositorio<IRepositorioModeloCredencial>(UnidadeTrabalho);

            IQueryable<ModeloCredencial> modelos = rep.ObterTodos();

            if (apenasAtivos)
                modelos = modelos.Where(t => t.Ativo == true && t.TipoCredencial == tipoCredencial);

            modelos = modelos.OrderBy(t => t.Nome);

            return Json(modelos.Select(s => 
                montaObj(s)).ToList(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Selecionar(int codigo)
        {
            IRepositorioModeloCredencial rep = Fabrica.Instancia.ObterRepositorio<IRepositorioModeloCredencial>(UnidadeTrabalho);
            ModeloCredencial modeloCredencial = rep.ObterPorId(codigo);
            return Json(montaObj(modeloCredencial));
        }

        //public ActionResult ObterTodos()
        //{
        //    IRepositorioModeloCredencial rep = Fabrica.Instancia.ObterRepositorio<IRepositorioModeloCredencial>(UnidadeTrabalho);
        //    List<ModeloCredencial> modeloCredencial = rep.ObterTodos().ToList();
        //    return Json(montaObj(modeloCredencial));
        //}

        [Transaction]
        public ActionResult Incluir(ModeloCredencial modeloCredencial, string fundoFrente, string fundoVerso)
        {
            IRepositorioModeloCredencial repModeloCredencial = Fabrica.Instancia.ObterRepositorio<IRepositorioModeloCredencial>(UnidadeTrabalho);
            if(modeloCredencial.HtmlVerso != null)
            {
                modeloCredencial.HtmlVerso = modeloCredencial.HtmlVerso.Replace("campo edt", "campo");
                modeloCredencial.FundoVerso = (string.IsNullOrEmpty(fundoVerso)) ? null : Convert.FromBase64String(fundoVerso);
            }
            modeloCredencial.HtmlFrente = modeloCredencial.HtmlFrente.Replace("campo edt", "campo");
            modeloCredencial.FundoFrente = (string.IsNullOrEmpty(fundoFrente)) ? null : Convert.FromBase64String(fundoFrente);
            modeloCredencial.DataRegistro = DateTime.Now;
            modeloCredencial.Ativar();
            repModeloCredencial.Validar(modeloCredencial, EstadoObjeto.Novo);
            repModeloCredencial.Salvar(modeloCredencial);

            return Json(montaObj(modeloCredencial));
        }

        [Transaction]
        public ActionResult Alterar(ModeloCredencial modeloCredencial, string fundoFrente, string fundoVerso)
        {
            IRepositorioModeloCredencial repModeloCredencial = Fabrica.Instancia.ObterRepositorio<IRepositorioModeloCredencial>(UnidadeTrabalho);
            ModeloCredencial m = repModeloCredencial.ObterPorId(modeloCredencial.Codigo);

            m.Nome = modeloCredencial.Nome;
            m.TipoCredencial = modeloCredencial.TipoCredencial;
            m.HtmlFrente = modeloCredencial.HtmlFrente.Replace("campo edt", "campo");
            if (modeloCredencial.HtmlVerso != null) { 
                m.HtmlVerso = modeloCredencial.HtmlVerso.Replace("campo edt", "campo");
                m.FundoVerso = (string.IsNullOrEmpty(fundoVerso)) ? null : Convert.FromBase64String(fundoVerso);
            }
            m.FundoFrente = (string.IsNullOrEmpty(fundoFrente)) ? null : Convert.FromBase64String(fundoFrente);
            m.Altura = modeloCredencial.Altura;
            m.Largura = modeloCredencial.Largura;

            repModeloCredencial.Validar(m, EstadoObjeto.Alterado);
            repModeloCredencial.Atualizar(m);

            return Json(montaObj(m));

        }

        [Transaction]
        public ActionResult AtivarInativar(int codigo)
        {
            IRepositorioModeloCredencial rep = Fabrica.Instancia.ObterRepositorio<IRepositorioModeloCredencial>(UnidadeTrabalho);
            ModeloCredencial modeloCredencial = rep.ObterPorId(codigo);
            if (modeloCredencial.Ativo)
            {
                rep.Validar(modeloCredencial, EstadoObjeto.Inativado);
                modeloCredencial.Inativar();
            }
            else
            {
                rep.Validar(modeloCredencial, EstadoObjeto.Ativado);
                modeloCredencial.Ativar();
            }

            return Json(montaObj(modeloCredencial));

        }

        private object montaObj(ModeloCredencial modeloCredencial)
        {
            return new ModeloCredencialModelView
            {
                Codigo = modeloCredencial.Codigo,
                Nome = modeloCredencial.Nome,
                HtmlFrente = !string.IsNullOrEmpty(modeloCredencial.HtmlFrente) ? modeloCredencial.HtmlFrente.Replace("campo", "campo edt") : null,
                HtmlVerso = !string.IsNullOrEmpty(modeloCredencial.HtmlVerso) ? modeloCredencial.HtmlVerso.Replace("campo", "campo edt") : null,
                FundoFrente = modeloCredencial.FundoFrente == null ? null : Convert.ToBase64String(modeloCredencial.FundoFrente),
                FundoVerso = modeloCredencial.FundoVerso == null ? null : Convert.ToBase64String(modeloCredencial.FundoVerso),
                TipoCredencialNome = obterDesc(modeloCredencial.TipoCredencial),
                Altura = modeloCredencial.Altura,
                Largura = modeloCredencial.Largura,
                Ativo = modeloCredencial.Ativo,
                DataRegistro = modeloCredencial.DataRegistro
            };
        }

        private string obterDesc(TipoCredencial tipoCredencial)
        {
            return EnumExtensoes.ObterDescricaoEnum(tipoCredencial);
        }

    }
}
