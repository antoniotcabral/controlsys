﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Controlsys.Dominio.Seguranca;
using Controlsys.Infra;
using Controlsys.Repositorios.Seguranca;
using Ext.Net;
using Ext.Net.MVC;
using Globalsys;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar desktops.
    /// </summary>
    [DirectController]
    [ExtendController]
    public class DesktopController : Controller, IControllerBase
    {
        /// <summary>
        /// Gets/Sets valor para UnidadeTrabalho.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.DesktopController.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public DesktopController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        /// <summary>
        /// GET: /Desktop/.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Obter modulos.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [DirectMethod(Namespace = "Metodo", ShowMask = true, Msg = "Carregando...")]
        public ActionResult ObterModulos()
        {
            string usuarioLogado = HttpContext.User.Identity.Name;

            IRepositorioPagina repositorioPagina = Fabrica.Instancia.ObterRepositorio<IRepositorioPagina>(UnidadeTrabalho);
            IRepositorioPermissao repPermissao = Fabrica.Instancia.ObterRepositorio<IRepositorioPermissao>(UnidadeTrabalho);

            IQueryable<Acao> acoes = repPermissao.ObterAcoesUsuario(usuarioLogado);
            IQueryable<Pagina> modulos = repositorioPagina.ObterTodos().Where(p => p.TipoPagina == TipoPagina.Modulo);

            modulos = modulos.Where(m => acoes.Any(a => a.Pagina.PaginaPai.PaginaPai.Codigo == m.Codigo));

            foreach (Pagina pagina in modulos)
            {
                DesktopModule desktopModule = new DesktopModule
                {
                    ModuleID = pagina.Codigo.ToString(),
                    Shortcut = new DesktopShortcut
                    {
                        Name = pagina.Nome,
                        IconCls = pagina.Icone + "48"
                    },
                    Launcher = new Ext.Net.MenuItem
                    {
                        Text = pagina.Nome
                    },
                    Window = 
                    {
                        createWindow(pagina)
                    }
                };

                this.GetDesktop().AddModule(desktopModule);
            }

            return this.Direct();
        }

        /// <summary>
        /// Create window.
        /// </summary>
        ///
        /// <param name="pagina">
        /// O(a) pagina.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Window.
        /// </returns>
        private Window createWindow(Pagina pagina)
        {
            Window window = new Window
            {
                ID = pagina.Codigo.ToString(),
                Title = pagina.Nome,
                IconCls = pagina.Icone + "16",
                Loader = new ComponentLoader
                {
                    Url = string.Format("/Modulo/Index?codigo={0}", pagina.Codigo),
                    Mode = LoadMode.Frame,
                    LoadMask =
                    {
                        ShowMask = true,
                        Msg = "Carregando..."
                    }
                },
                Width = 900,
                Height = 600,
                ConstrainHeader = true,
                Maximizable = true,
                Minimizable = true,
                Resizable = true,
                Modal = false,
                AnimCollapse = true,
                MaintainFlex = true,
                Border = false,
                CloseAction = CloseAction.Destroy
            };

            window.Loader.CustomConfig.Add(new ConfigItem { Name = "CodigoModulo", Value = pagina.Codigo.ToString() });

            return window;
        }
    }
}
