﻿using Controlsys.Dominio.Seguranca;
using Controlsys.Infra;
using Controlsys.Repositorios.Seguranca;
using Ext.Net;
using Globalsys;
using Globalsys.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar esqueci senhas.
    /// </summary>
    [ExtendController]
    [AllowAnonymous]
    public class EsqueciSenhaController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /EsqueciSenha/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.EsqueciSenhaController.
        /// </summary>
        ///
        /// <param name="unidade">
        /// O(a) unidade.
        /// </param>
        public EsqueciSenhaController(IUnidadeTrabalho unidade)
        {
            UnidadeTrabalho = unidade;
        }

        /// <summary>
        /// Retorna a página de esqueciSenha.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Gera captcha.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult GeraCaptcha()
        {
            int valor1 = GeraNumeros(30);
            int valor2 = GeraNumeros(13);
            string captcha = valor1 + " + " + valor2;
            Session["Captcha"] = valor1 + valor2;

            return Json(captcha);
        }

        /// <summary>
        /// Gera numeros.
        /// </summary>
        ///
        /// <param name="valor">
        /// O(a) valor.
        /// </param>
        ///
        /// <returns>
        /// Um(a) int.
        /// </returns>
        private int GeraNumeros(int valor)
        {
            Random randNum = new Random();
            return randNum.Next(valor);
        }

        /// <summary>
        /// Recuperar acesso.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="email">
        /// O(a) email.
        /// </param>
        /// <param name="cpf">
        /// O(a) cpf.
        /// </param>
        /// <param name="passaporte">
        /// O(a) passaporte.
        /// </param>
        /// <param name="captcha">
        /// O(a) captcha.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult RecuperarAcesso(string email, string cpf, string passaporte, int captcha)
        {
            if (captcha.ToString() == Session["Captcha"].ToString())
            {
                IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
                IRepositorioUrlAlteracaoSenha repUrl = Fabrica.Instancia.ObterRepositorio<IRepositorioUrlAlteracaoSenha>(UnidadeTrabalho);
                cpf = cpf.Replace(".", "").Replace("-", "");
                Usuario user = repUsuario.ObterPorEmailDoc(email, cpf, passaporte);
                string url = geraUrlCrip(user.Login);

                UrlAlteracaoSenha urlAlteracao = new UrlAlteracaoSenha
                {
                    UrlAlteracao = url,
                    DataRegistro = DateTime.Now,
                    Usuario = user
                };
                urlAlteracao.Ativar();
                repUrl.Salvar(urlAlteracao);

                String[] urlCurrent = HttpContext.Request.Url.AbsoluteUri.Split('/');
                string urlEmail = urlCurrent[0] + "//" + urlCurrent[2] + "/" + urlCurrent[3] + "/AlterarSenha/?" + url;
                enviarEmailEsqueci(user, urlEmail);

            }
            else
            { throw new CoreException("A soma está incorreta. Tente novamente."); }

            return null;
        }

        /// <summary>
        /// Enviar email esqueci.
        /// </summary>
        ///
        /// <param name="usuario">
        /// O(a) usuario.
        /// </param>
        /// <param name="url">
        /// URL of the document.
        /// </param>
        private void enviarEmailEsqueci(Usuario usuario, string url)
        {
            Dictionary<string, string> valoresSubs = new Dictionary<string, string>();

            valoresSubs.Add("$$Url$$", url);

            Globalsys.Util.Email.Enviar("[CONTROLSYS] - Dados de acesso",
                                        Server.MapPath("/Content/templates/esqueciSenha.htm"),
                                        valoresSubs,
                                        string.Empty,
                                        null,
                                        usuario.PessoaFisica.Email);
        }

        /// <summary>
        /// Alterar senha.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult AlterarSenha()
        {
            string strLogin = "", strData = "";
            try
            {
                string urlCrip = Request.RawUrl;
                urlCrip = urlCrip.Substring(urlCrip.IndexOf('?') + 1);

                string urlDescrip = Globalsys.Util.Tools.DescriptografarQueryString(urlCrip, "r0b1nr0y");

                string[] arrMsgs = urlDescrip.Split('&');
                string[] arrIndMsg;

                arrIndMsg = arrMsgs[0].Split('=');
                strLogin = arrIndMsg[0].ToString().Trim();
                strData = arrIndMsg[1].ToString().Trim();

                IRepositorioUrlAlteracaoSenha repUrl = Fabrica.Instancia.ObterRepositorio<IRepositorioUrlAlteracaoSenha>(UnidadeTrabalho);
                UrlAlteracaoSenha url = repUrl.ObterPorLogin(strLogin, urlCrip, false);
                if (url == null)
                    throw new CoreException();
                if (!url.Ativo || (strData != DateTime.Today.ToString()))
                    throw new CoreException("Inativo");
            }
            catch (Exception ex)
            {
                return ex.Message == "Inativo" ? View("Inativado") : View("AcessoNegado");
            }

            X.GetCmp<Hidden>("hdnLogin").SetValue(strLogin);

            return View();
        }

        /// <summary>
        /// Alterar senha login.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="senhaNova">
        /// O(a) senha nova.
        /// </param>
        /// <param name="senhaNovaConf">
        /// O(a) senha nova conf.
        /// </param>
        /// <param name="login">
        /// O(a) login.
        /// </param>
        /// <param name="captcha">
        /// O(a) captcha.
        /// </param>
        [Transaction]
        public void AlterarSenhaLogin(string senhaNova, string senhaNovaConf, string login, int captcha)
        {
            if (captcha.ToString() != Session["Captcha"].ToString())
                throw new CoreException("A soma está incorreta. Tente novamente.");

            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            IRepositorioUrlAlteracaoSenha repUrl = Fabrica.Instancia.ObterRepositorio<IRepositorioUrlAlteracaoSenha>(UnidadeTrabalho);
            string urlCript = geraUrlCrip(login);

            UrlAlteracaoSenha url = repUrl.ObterPorLogin(login, urlCript);
            if (!url.Ativo || url == null)
                throw new CoreException("Url já utilizada/vencida. Por favor, solicitar novamente.");

            Usuario user = repUsuario.AlterarSenha(url.Usuario.Login, senhaNova, senhaNovaConf);
            url.Inativar();
            enviarEmailAlteracao(user, senhaNova);
        }

        /// <summary>
        /// Enviar email alteracao.
        /// </summary>
        ///
        /// <param name="usuario">
        /// O(a) usuario.
        /// </param>
        /// <param name="senha">
        /// O(a) senha.
        /// </param>
        private void enviarEmailAlteracao(Usuario usuario, string senha)
        {
            Dictionary<string, string> valoresSubs = new Dictionary<string, string>();

            valoresSubs.Add("$$Login$$", usuario.Login);
            valoresSubs.Add("$$Senha$$", senha);

            Globalsys.Util.Email.Enviar("[CONTROLSYS] - Dados de acesso",
                                        Server.MapPath("/Content/templates/alteracaoSenha.htm"),
                                        valoresSubs,
                                        string.Empty,
                                        null,
                                        usuario.PessoaFisica.Email);
        }

        /// <summary>
        /// Gera URL crip.
        /// </summary>
        ///
        /// <param name="login">
        /// O(a) login.
        /// </param>
        ///
        /// <returns>
        /// Um(a) string.
        /// </returns>
        private string geraUrlCrip(string login)
        {
            string url = login.ToString() + "=" + DateTime.Today.ToString();
            url = "" + Globalsys.Util.Tools.CriptografarQueryString(url, "r0b1nr0y");
            return url;
        }

    }
}
