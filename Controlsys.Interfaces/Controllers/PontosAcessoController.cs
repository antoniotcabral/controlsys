﻿using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Controlsys.Repositorios.Acesso;
using Controlsys.Infra;
using Controlsys.Dominio.Acesso;
using Controlsys.Repositorios.Seguranca;
using Controlsys.Interfaces.Models;

namespace Controlsys.Interfaces.Controllers
{    
    [ExtendController]
    public class PontosAcessoController : Controller, IControllerBase
    {

        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        public PontosAcessoController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ObterTodos()
        {
            IRepositorioControladora repControladora = Fabrica.Instancia.ObterRepositorio<IRepositorioControladora>(UnidadeTrabalho);

            IQueryable<Controladora> controladoras = repControladora.ObterTodos().Where(x => x.Ativo == true);

            return Json(controladoras
                            .ToList()
                            .Select(c => new
                             {
                                 c.Codigo,
                                 c.IP,
                                 TipoControladora = obterDescricaoEnum(c.TipoControladora),
                                 Status = obterDescricaoEnum(c.Status),
                                 c.Online,
                                 DataStatus = c.DataStatus,
                                 c.Leitoras.LastOrDefault().Descricao,
                                 c.Leitoras.LastOrDefault().Local
                             }));
        }

        public ActionResult ObterLogs(int codigoControladora)
        {
            IRepositorioLogControladora repLogControladora = Fabrica.Instancia.ObterRepositorio<IRepositorioLogControladora>(UnidadeTrabalho);

            return Json(repLogControladora.ObterPorControladora(codigoControladora)
                                          .ToList()
                                          .Select(lc => new
                                                  {
                                                      lc.Codigo,
                                                      Controladora = lc.Controladora.IP,
                                                      Status = lc.Status.ObterDescricaoEnum(),
                                                      DataHora = lc.DataHora.ToString("dd/MM/yyyy HH:mm"),
                                                      Usuario = lc.Usuario != null ? lc.Usuario.PessoaFisica.Nome : "Registrado pelo sistema.",
                                                      Observacoes = lc.Observacoes
                                                  })
                                          );
        }

        [Transaction]
        public ActionResult AlterarStatus(int codigoControladora, StatusControladora status)
        {
            IRepositorioControladora repControladora = Fabrica.Instancia.ObterRepositorio<IRepositorioControladora>(UnidadeTrabalho);
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);

            Controladora controladora = repControladora.ObterPorId(codigoControladora);

            repControladora.AlterarStatus(controladora, status, repUsuario.ObterPorLogin(HttpContext.User.Identity.Name));

            return Json(formatObject(controladora));
        }

        private object formatObject(Controladora controladora)
        {
            return new
            {
                controladora.Codigo,
                controladora.IP,
                TipoControladora = obterDescricaoEnum(controladora.TipoControladora),
                Status = obterDescricaoEnum(controladora.Status),
                controladora.Online,
                DataStatus = controladora.DataStatus
            };
        }

        private string obterDescricaoEnum(TipoControladora? enumValue)
        {
            return enumValue.HasValue ? enumValue.ObterDescricaoEnum() : null;
        }

        private string obterDescricaoEnum(StatusControladora? enumValue)
        {
            return enumValue.HasValue ? enumValue.ObterDescricaoEnum() : null;
        }

        public ActionResult Exportar()
        {
            return View();
        }

    }
}
