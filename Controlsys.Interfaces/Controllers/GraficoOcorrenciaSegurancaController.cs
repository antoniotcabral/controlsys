﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Controlsys.Dominio.Ocorrencias;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Ocorrencias;
using Globalsys;
using Globalsys.Exceptions;
using Microsoft.Reporting.WebForms;
using System.Globalization;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class GraficoOcorrenciaSegurancaController : Controller, IControllerBase
    {
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        public GraficoOcorrenciaSegurancaController(IUnidadeTrabalho unidade) { UnidadeTrabalho = unidade; }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Exportar(string tipoRelatorio, string classificacao, DateTime dtInicio, DateTime dtFim)
        {
            classificacao = classificacao.ToUpper();
            if (classificacao != "TIPO" && classificacao != "GRAVIDADE")
                throw new CoreException("Não foi possível identificar a classificação informada.");

            var ocorrencias = FiltrarOcorrencias(dtInicio, dtFim, classificacao);
            //if(!ocorrencias.Any())
            //    throw new CoreException("Não há registros para o filtro informado.");

            LocalReport relatorio = new LocalReport();
            relatorio.ReportPath = Server.MapPath("~/Reports/GraficoOcorrenciaSeguranca.rdlc");
            relatorio.DataSources.Add(new ReportDataSource("DTOGraficoOcorrenciaSeguranca", ocorrencias));
            relatorio.SetParameters(new ReportParameter("Classificacao", classificacao));
            relatorio.SetParameters(new ReportParameter("Total", ocorrencias.Count.ToString()));
            relatorio.SetParameters(new ReportParameter("DataInicio", dtInicio.ToString("dd/MM/yyyy")));
            relatorio.SetParameters(new ReportParameter("DataFinal", dtFim.ToString("dd/MM/yyyy")));

            string mimeType;
            string fileName = string.Empty;
            switch (tipoRelatorio)
            {
                case "PDF":
                    mimeType = "application/pdf";
                    fileName = "download.pdf";
                    break;
                case "Excel":
                    mimeType = "application/vnd.ms-excel";
                    fileName = "download.xls";
                    break;
                case "Word":
                    mimeType = "application/msword";
                    fileName = "download.doc";
                    break;
            }

            string encoding;
            string fileNameExtension;

            Warning[] warnings;
            string[] streams;
            byte[] bytes;

            bytes = relatorio.Render(
            tipoRelatorio,
            null,
            out mimeType,
            out encoding,
            out fileNameExtension,
            out streams,
            out warnings);

            return File(bytes, mimeType, fileName);
        }

        private List<GraficoOcorrenciaSegurancaModelView> FiltrarOcorrencias(DateTime dtInicio, DateTime dtFim, string classificacao)
        {
            if (classificacao != "TIPO" && classificacao != "GRAVIDADE")
                throw new CoreException("Não foi possível identificar a classificação informada.");

            IRepositorioOcorrenciaSeguranca rep = Fabrica.Instancia.ObterRepositorio<IRepositorioOcorrenciaSeguranca>(UnidadeTrabalho);

            dtFim = dtFim.AddDays(1);
            List<OcorrenciaSeguranca> lista = rep.ObterTodos().Where(x => x.DataOcorrencia >= dtInicio && x.DataOcorrencia < dtFim).ToList();

            if(!lista.Any())
                return new List<GraficoOcorrenciaSegurancaModelView>();

            return lista.Select(x => new GraficoOcorrenciaSegurancaModelView()
            {
                Codigo = x.Codigo,
                Descricao = classificacao == "TIPO" ? x.TipoOcorrencia.Descricao : x.Gravidade.ObterDescricaoEnum()
            }).ToList();

            //return listaAux.Where(x => !string.IsNullOrEmpty(x.Descricao)).ToList();
        }

    }
}
