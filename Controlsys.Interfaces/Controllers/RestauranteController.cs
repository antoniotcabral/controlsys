﻿using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Parametros;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Acesso;
using Controlsys.Repositorio.Parametros;
using Controlsys.Repositorios.Acesso;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class RestauranteController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /Acao/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.AcaoController.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RestauranteController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ObterTodos(bool apenasAtivos = false, string descricao = null)
        {
            IRepositorioRestaurante repRest = Fabrica.Instancia.ObterRepositorio<IRepositorioRestaurante>(UnidadeTrabalho);

            IQueryable<Restaurante> rests = repRest.ObterTodos();

            if (apenasAtivos)
                rests = rests.Where(t => t.Ativo == true);

            if (!string.IsNullOrEmpty(descricao))
                rests = rests.Where(r => r.Nome.ToLower().Contains(descricao.ToLower()));

            rests = rests.OrderBy(r => r.Nome);

            return Json(rests.Select(s => new RestauranteModelView()
                               {
                                   Codigo = s.Codigo,
                                   Nome = s.Nome,
                                   Ativo = s.Ativo,
                                   descGrupoLeitora = s.GrupoLeitora.Nome,
                                   TiposAlimentacao = new List<TipoAlimentacaoRestModelView>(
                                       s.TiposAlimentacao.Select(ta => new TipoAlimentacaoRestModelView()
                                       {
                                           Codigo = ta.TipoAlimentacao.Codigo,
                                           Nome = ta.TipoAlimentacao.Nome
                                       })
                                   )
                               })
                                .ToList());

        }

        [Transaction]
        public ActionResult Incluir(Restaurante restaurante, List<TipoAlimentacaoRestaurante> tiposAli, TipoAlimentacaoRestaurante[] tiposAliAlterado)
        {
            IRepositorioRestaurante repRest = Fabrica.Instancia.ObterRepositorio<IRepositorioRestaurante>(UnidadeTrabalho);
            IRepositorioTipoAlimentacaoRestaurante repTpAlRest = Fabrica.Instancia.ObterRepositorio<IRepositorioTipoAlimentacaoRestaurante>(UnidadeTrabalho);
            restaurante.DataRegistro = DateTime.Now;
            restaurante.Ativar();
            repRest.Validar(restaurante, EstadoObjeto.Novo);
            repRest.Salvar(restaurante);

            if (tiposAli != null)
            {
                foreach (var item in tiposAli)
                {
                    item.Restaurante = restaurante;
                    item.DataRegistro = DateTime.Now;
                    item.Ativar();
                    repTpAlRest.Validar(item, tiposAliAlterado, EstadoObjeto.Novo);
                    restaurante.TiposAlimentacao.Add(item);
                    repRest.SalvarTipoAlimentacaoRestaurante(item);
                }
            }
            return Json(montaObj(restaurante));
        }

        [Transaction]
        public ActionResult Alterar(Restaurante restaurante, TipoAlimentacaoRestaurante[] tiposAliDel, TipoAlimentacaoRestaurante[] tiposAliNew, TipoAlimentacaoRestaurante[] tiposAliAlterado) //, TipoAlimentacaoRestaurante[] tiposAli)
        {
            IRepositorioTipoAlimentacaoRestaurante repTpAlRest = Fabrica.Instancia.ObterRepositorio<IRepositorioTipoAlimentacaoRestaurante>(UnidadeTrabalho);
            IRepositorioRestaurante repRest = Fabrica.Instancia.ObterRepositorio<IRepositorioRestaurante>(UnidadeTrabalho);
            Restaurante restauranteA = repRest.ObterPorId(restaurante.Codigo);
            restauranteA.Nome = restaurante.Nome;
            restauranteA.GrupoLeitora = restaurante.GrupoLeitora;
            repRest.Validar(restauranteA, EstadoObjeto.Alterado);

            if (tiposAliDel != null)
            {
                IRepositorioGrupoLeitora repGrupoLeitora = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoLeitora>(UnidadeTrabalho);
                IRepositorioControladora repControladora = Fabrica.Instancia.ObterRepositorio<IRepositorioControladora>(UnidadeTrabalho);
                IRepositorioAcessoRestaurante repAccessoRest = Fabrica.Instancia.ObterRepositorio<IRepositorioAcessoRestaurante>(UnidadeTrabalho);
                IRepositorioSincronizaPessoaAcesso repSincronizaPessoaAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioSincronizaPessoaAcesso>(UnidadeTrabalho);

                List<GrupoLeitora> grupoLeitoras = repGrupoLeitora.ObterTodos().Where(gl => gl.Codigo == restauranteA.GrupoLeitora.Codigo).ToList();
                List<Controladora> controladoras = grupoLeitoras.SelectMany(l => l.Leitoras.Select(c => c.Leitora.Controladora)).Where(x => x.Ativo).ToList();

                repSincronizaPessoaAcesso.Sincronizar(null, TipoSincronizacaoPessoa.Suspender, null, controladoras);

                foreach (var item in restauranteA.TiposAlimentacao.Where(t => tiposAliDel.Any(tdd => tdd.TipoAlimentacao.Codigo == t.TipoAlimentacao.Codigo)))
                {
                    var acessos = repAccessoRest.ObterTodos();
                    acessos = acessos.Where(ar => ar.TiposAlimentacao.Any(arta => arta.Ativo && arta.TipoAlimentacao.Ativo && arta.TipoAlimentacao.Restaurante.TiposAlimentacao.Select(tpa => tpa.Codigo).Contains(item.Codigo)));

                    TipoAlimentacaoRestaurante tpAliRestA = repTpAlRest.ObterPorId(item.Codigo);
                    if (tiposAliNew != null && !tiposAliNew.Any(t => t.TipoAlimentacao.Codigo.Equals(tpAliRestA.TipoAlimentacao.Codigo)))
                        repTpAlRest.Validar(item, tiposAliAlterado, EstadoObjeto.Inativado);
                    tpAliRestA.Inativar();
                }
            }

            if (tiposAliNew != null)
            {
                IRepositorioAcessoRestaurante repAccessoRest = Fabrica.Instancia.ObterRepositorio<IRepositorioAcessoRestaurante>(UnidadeTrabalho);
                foreach (var item in tiposAliNew)
                {
                    item.Restaurante = restaurante;
                    item.DataRegistro = DateTime.Now;
                    item.Ativar();
                    repTpAlRest.Validar(item,tiposAliAlterado, EstadoObjeto.Novo);
                    repRest.SalvarTipoAlimentacaoRestaurante(item);

                    //Buscando todos, até os inativos, para atualizar a referência dos que foram inativado;
                    var acessos = repAccessoRest.ObterTodos();
                    acessos = acessos.Where(ar => ar.TiposAlimentacao.Any(arta => arta.TipoAlimentacao.Restaurante.TiposAlimentacao.Select(tpa => tpa.Codigo).Contains(item.Codigo)));
                }
            }

            if (tiposAliAlterado != null)
            {
                int cont = 0;
                foreach (var item in restauranteA.TiposAlimentacao.Where(t => tiposAliAlterado.Any(tdd => tdd.TipoAlimentacao.Codigo == t.TipoAlimentacao.Codigo)))
                {
                    TipoAlimentacaoRestaurante tpAliUp = repTpAlRest.ObterPorId(item.Codigo);
                    if (item.TipoAlimentacao.Codigo == tiposAliAlterado[cont].TipoAlimentacao.Codigo)
                    {
                        item.HoraInicio = tiposAliAlterado[cont].HoraInicio;
                        item.HoraFim = tiposAliAlterado[cont].HoraFim;
                        cont++;
                    }
                    repTpAlRest.Validar(item, tiposAliAlterado, EstadoObjeto.Alterado);
                    repTpAlRest.Atualizar(item);
                }
            }
            return Json(montaObj(restauranteA));
        }

        private RestauranteModelView montaObj(Restaurante restaurante)
        {
            var restauranteMV = new RestauranteModelView();
            restauranteMV.Codigo = restaurante.Codigo;
            restauranteMV.Nome = restaurante.Nome;
            restauranteMV.GrupoLeitora = restaurante.GrupoLeitora.Codigo;
            restauranteMV.descGrupoLeitora = restaurante.GrupoLeitora.Nome;
            restauranteMV.Ativo = restaurante.Ativo;
            restauranteMV.TiposAlimentacao = restaurante.TiposAlimentacao == null ? null :
                restaurante.TiposAlimentacao.Where(tpr => tpr.Ativo)
                .Select(td => new TipoAlimentacaoRestModelView
                {
                    Codigo = td.TipoAlimentacao.Codigo,
                    Nome = td.TipoAlimentacao.Nome,
                    HoraInicio = td.HoraInicio.ToString(@"hh\:mm"),
                    HoraFim = td.HoraFim.ToString(@"hh\:mm")
                }).ToList();

            return restauranteMV;
        }

        [Transaction]
        public ActionResult AtivarInativar(int codigo)
        {
            IRepositorioRestaurante repRest = Fabrica.Instancia.ObterRepositorio<IRepositorioRestaurante>(UnidadeTrabalho);
            Restaurante restaurante = repRest.ObterPorId(codigo);
            if (restaurante.Ativo)
            {
                repRest.Validar(restaurante, EstadoObjeto.Inativado);
                restaurante.Inativar();
            }
            else
            {
                repRest.Validar(restaurante, EstadoObjeto.Ativado);
                restaurante.Ativar();
            }

            return Json(montaObj(restaurante));
        }

        public ActionResult Selecionar(int codigo)
        {
            IRepositorioRestaurante repRest = Fabrica.Instancia.ObterRepositorio<IRepositorioRestaurante>(UnidadeTrabalho);
            Restaurante restaurante = repRest.ObterPorId(codigo);

            return Json(montaObj(restaurante));
        }

    }
}
