﻿using Controlsys.Dominio.Empresas;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorios.Parametros;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ninject;
using System.Collections;
using Controlsys.Dominio.Pessoas;
using Controlsys.Repositorios.Pessoas;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar setor custoes.
    /// </summary>
    [ExtendController]
    public class SetorCustoController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /Setor/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.SetorCustoController.
        /// </summary>
        ///
        /// <param name="unidade">
        /// O(a) unidade.
        /// </param>
        public SetorCustoController(IUnidadeTrabalho unidade)
        {
            UnidadeTrabalho = unidade;
        }

        /// <summary>
        /// Retorna a página de setorCusto.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Inclui um novo setorCusto.
        /// </summary>
        ///
        /// <param name="setor">
        /// O(a) setor.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Incluir(SetorCusto setor)
        {
            IRepositorioSetorCusto repSetor = Fabrica.Instancia.ObterRepositorio<IRepositorioSetorCusto>(UnidadeTrabalho);
            var repPapel = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);
            setor.DataRegistro = DateTime.Now;
            setor.Ativar();

            if (setor.Responsavel != null)
                setor.Responsavel = repPapel.ObterPorId(setor.Responsavel.Codigo);

            repSetor.Validar(setor, EstadoObjeto.Novo);
            repSetor.Salvar(setor);
            return Json(montaObj(setor));
        }

        /// <summary>
        /// Altera um registro de setorCusto.
        /// </summary>
        ///
        /// <param name="setor">
        /// O(a) setor.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Alterar(SetorCusto setor)
        {
            IRepositorioSetorCusto repSetor = Fabrica.Instancia.ObterRepositorio<IRepositorioSetorCusto>(UnidadeTrabalho);
            var repPapel = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);
            SetorCusto setorA = repSetor.ObterPorId(setor.Codigo);
            setorA.Numero = setor.Numero;
            setorA.TipoSetor = setor.TipoSetor;
            setorA.Descricao = setor.Descricao;
            setorA.Nome = setor.Nome;
            setorA.IDResponsavelSAP = setor.IDResponsavelSAP;
            setorA.Responsavel = setor.Responsavel != null ? setorA.Responsavel = repPapel.ObterPorId(setor.Responsavel.Codigo) : null;

            repSetor.Validar(setorA, EstadoObjeto.Alterado);
            repSetor.Atualizar(setorA);
            return Json(montaObj(setorA));
        }

        /// <summary>
        /// Cria um objeto contendo informações relacionadas ao conteudo gerenciado por
        /// SetorCustoController.
        /// </summary>
        ///
        /// <param name="setor">
        /// O(a) setor.
        /// </param>
        ///
        /// <returns>
        /// Um(a) object.
        /// </returns>
        private object montaObj(SetorCusto setor)
        {
            return new SetorCustoModelView()
            {
                Codigo = setor.Codigo,
                Nome = setor.Nome,
                Numero = setor.Numero,
                Descricao = setor.Descricao,
                TipoSetor = obterDesc(setor.TipoSetor),
                Ativo = setor.Ativo,
                IDResponsavelSAP = setor.IDResponsavelSAP
            };
        }

        /// <summary>
        /// Ativar inativar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult AtivarInativar(int codigo)
        {
            IRepositorioSetorCusto repSetor = Fabrica.Instancia.ObterRepositorio<IRepositorioSetorCusto>(UnidadeTrabalho);
            SetorCusto setor = repSetor.ObterPorId(codigo);
            if (setor.Ativo)
            {
                setor.Inativar();
            }
            else
            {
                repSetor.Validar(setor, EstadoObjeto.Ativado);
                setor.Ativar();

            }
            repSetor.Atualizar(setor);
            return Json(montaObj(setor));

        }

        /// <summary>
        /// Selecionar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Selecionar(int codigo)
        {
            IRepositorioSetorCusto repSetor = Fabrica.Instancia.ObterRepositorio<IRepositorioSetorCusto>(UnidadeTrabalho);
            SetorCusto setor = repSetor.ObterPorId(codigo);
            return Json(new {
                Codigo = setor.Codigo,
                Nome = setor.Nome,
                Descricao = setor.Descricao,
                TipoSetor = setor.TipoSetor,
                Numero = setor.Numero,
                CPFResponsavel = string.IsNullOrEmpty(setor.Responsavel?.PessoaFisica.CPF) ? "" : Convert.ToUInt64(setor.Responsavel?.PessoaFisica.CPF).ToString(@"000\.000\.000\-00"),
                CodigoResponsavel = setor.Responsavel?.Codigo,
                NomeResponsavel = setor.Responsavel?.PessoaFisica.Nome,
                IDResponsavelSAP = setor.IDResponsavelSAP
            });
        }

        /// <summary>
        /// Retorna todos os objetos do tipo SetorCusto.
        /// </summary>
        ///
        /// <param name="apenasAtivos">
        /// true to apenas ativos.
        /// </param>
        ///
        /// <returns>
        /// Detalhes do tipo de objeto ligado ao controller no formato JSON.
        /// </returns>
        public ActionResult ObterTodos(bool apenasAtivos = false, string descricao = null)
        {
            IRepositorioSetorCusto repSetor = Fabrica.Instancia.ObterRepositorio<IRepositorioSetorCusto>(UnidadeTrabalho);

            IQueryable<SetorCusto> setorCusto = repSetor.ObterTodos();

            if (apenasAtivos)
                setorCusto = setorCusto.Where(t => t.Ativo == true);

            if (!string.IsNullOrEmpty(descricao))
                setorCusto = setorCusto.Where(sc => sc.Nome.ToLower().Contains(descricao.ToLower()));

            return Json(setorCusto
                               .Select(s => new
                                {
                                    Codigo = s.Codigo,
                                    Nome = s.Numero + " - " + s.Nome + " - " + s.Descricao,
                                    NomeEResponsavel = s.Numero + " - " + s.Nome + " - " + s.Descricao,
                                    Numero = s.Numero,
                                    Descricao = s.Descricao,
                                    TipoSetor = obterDesc(s.TipoSetor),
                                    Ativo = s.Ativo
                                })
                                .ToList().OrderBy(l => l.Nome), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Obter description.
        /// </summary>
        ///
        /// <param name="tipoSetorCusto">
        /// O(a) tipo setor custo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) string.
        /// </returns>
        private string obterDesc(TipoSetorCusto tipoSetorCusto)
        {
            return EnumExtensoes.ObterDescricaoEnum(tipoSetorCusto);
        }

        /// <summary>
        /// Lista tipo setor.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ListaTipoSetor()
        {
            Dictionary<int, string> dic = new Dictionary<int, string>();

            foreach (int item in Enum.GetValues(typeof(TipoSetorCusto)))
            {
                dic.Add(item, ((TipoSetorCusto)item).ObterDescricaoEnum());
            }
            return Json(dic.Select(d => new
                        {
                            Codigo = d.Key,
                            Descricao = d.Value
                        }).ToList());

        }

        
    }
}


