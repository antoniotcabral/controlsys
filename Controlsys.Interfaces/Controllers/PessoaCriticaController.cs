﻿using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Controlsys.Dominio.Seguranca;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Acesso;
using Controlsys.Repositorio.Parametros;
using Controlsys.Repositorios.Pessoas;
using Controlsys.Repositorios.Seguranca;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar pessoa criticas.
    /// </summary>
    [ExtendController]
    public class PessoaCriticaController : Controller, IControllerBase
    {
        /// <summary>
        /// Gets/Sets valor para UnidadeTrabalho.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.PessoaCriticaController.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public PessoaCriticaController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        /// <summary>
        /// Obtém todas as pessoas com papel ativo no sistema.
        /// </summary>
        ///
        /// <param name="verificaUsuarioAtivo">
        /// Verifica apenas pessoas que não são usuário ativo.
        /// </param>
        ///
        /// <returns>
        /// Detalhes do tipo de objeto ligado ao controller no formato JSON.
        /// </returns>
        public ActionResult ObterTodos(bool verificaUsuarioAtivo = false)
        {
            IRepositorioPessoaCritica repPessoaCritica = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaCritica>(UnidadeTrabalho);

            var listPessoaCritica = repPessoaCritica.ObterTodos().ToList().Select(s => new PessoaCriticaModelView()
            {
                Codigo = s.Codigo,

                CPF = !string.IsNullOrEmpty(s.PessoaFisica.CPF) ? s.PessoaFisica.CPFFormatado() : null,
                Passaporte = s.PessoaFisica.Passaporte,
                Nome = s.PessoaFisica.Nome,

                MotivoBloqueio = s.MotivoBloqueio.Nome,
                Ativo = s.Ativo,
                Responsavel = s.Responsavel.PessoaFisica.Nome,
                //ResponsavelFuncao = (s.Responsavel.PessoaFisica != null) ? ((s.Responsavel.PessoaFisica.Papeis.OrderByDescending(p => p.Codigo).FirstOrDefault() != null) ? ((s.Responsavel.PessoaFisica.Papeis.OrderByDescending(p => p.Codigo).FirstOrDefault().Cargo != null) ? s.Responsavel.PessoaFisica.Papeis.OrderByDescending(p => p.Codigo).FirstOrDefault().Cargo.Nome : "") : "") : ""//,
                ResponsavelFuncao = s.Responsavel.PessoaFisica.Papeis.OrderByDescending(pp => pp.Codigo).Select(pp => pp.Cargo == null ? ((pp is Visitante) ? (pp as Visitante).Funcao : string.Empty) : pp.Cargo.Nome).FirstOrDefault(),
                ResponsavelEmpresa = (s.Responsavel.PessoaFisica.Papeis.OrderByDescending(pp => pp.Codigo).FirstOrDefault() is Colaborador) ?
                                                        (s.Responsavel.PessoaFisica.Papeis.OrderByDescending(pp => pp.Codigo).Select(pp => ((Colaborador)pp).Empresa.Apelido).FirstOrDefault()) : ""
                //ResponsavelEmpresa = papel != null ? (papel is Colaborador) ? (papel as Colaborador).Empresa.Apelido : (papel is PrestadorServico) ? (papel as PrestadorServico).TipoPrestador == TipoPrestador.Temporario ? (papel as PrestadorServico).Empresa.Apelido : (papel as PrestadorServico).Fornecedor.NomeFantasia : (papel as Visitante).Empresa : ""

            });

            return Json(listPessoaCritica.ToList());

        }

        /// <summary>
        /// Pesquisar.
        /// </summary>
        ///
        /// <param name="cpf">
        /// O(a) cpf.
        /// </param>
        /// <param name="usuario">
        /// O(a) usuario.
        /// </param>
        /// <param name="nome">
        /// O(a) nome.
        /// </param>
        /// <param name="motivo">
        /// O(a) motivo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Pesquisar(string cpf = null, string usuario = null, string nome = null, int motivo = 0)
        {
            IRepositorioPessoaCritica rep = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaCritica>(UnidadeTrabalho);
            cpf = cpf.Replace(".", "").Replace("-", "");

            IQueryable<PessoaCritica> pessoaC = rep.ObterTodos();

            if (!string.IsNullOrEmpty(nome))
                pessoaC = pessoaC.Where(p => p.PessoaFisica.Nome.Contains(nome));
            if (!string.IsNullOrEmpty(cpf))
                pessoaC = pessoaC.Where(p => p.PessoaFisica.CPF == cpf);
            if (!string.IsNullOrEmpty(usuario))
                pessoaC = pessoaC.Where(p => p.Responsavel.Codigo == usuario);
            if (motivo > 0)
                pessoaC = pessoaC.Where(p => p.MotivoBloqueio.Codigo == motivo);

            var listPessoaCritica = pessoaC.ToList().Select(pf => new PessoaCriticaModelView()
            {
                Codigo = pf.Codigo,

                Nome = pf.PessoaFisica.Nome,
                CPF = !string.IsNullOrEmpty(pf.PessoaFisica.CPF) ? pf.PessoaFisica.CPFFormatado() : null,
                Passaporte = pf.PessoaFisica.Passaporte,

                MotivoBloqueio = pf.MotivoBloqueio.Nome,
                Ativo = pf.Ativo,
                Responsavel = pf.Responsavel.PessoaFisica.Nome,
                //ResponsavelFuncao = (s.Responsavel.PessoaFisica != null) ? ((s.Responsavel.PessoaFisica.Papeis.OrderByDescending(p => p.Codigo).FirstOrDefault() != null) ? ((s.Responsavel.PessoaFisica.Papeis.OrderByDescending(p => p.Codigo).FirstOrDefault().Cargo != null) ? s.Responsavel.PessoaFisica.Papeis.OrderByDescending(p => p.Codigo).FirstOrDefault().Cargo.Nome : "") : "") : ""//,
                ResponsavelFuncao = pf.Responsavel.PessoaFisica.Papeis.OrderByDescending(pp => pp.Codigo).Select(pp => pp.Cargo == null ? (pp is Visitante) ? (pp as Visitante).Funcao : string.Empty : pp.Cargo.Nome).FirstOrDefault(),
                ResponsavelEmpresa = (pf.Responsavel.PessoaFisica.Papeis.OrderByDescending(pp => pp.Codigo).FirstOrDefault() is Colaborador) ?
                                     (pf.Responsavel.PessoaFisica.Papeis.OrderByDescending(pp => pp.Codigo).Select(pp => ((Colaborador)pp).Empresa.Apelido).FirstOrDefault()) : ""
                //ResponsavelEmpresa = papel != null ? (papel is Colaborador) ? (papel as Colaborador).Empresa.Apelido : (papel is PrestadorServico) ? (papel as PrestadorServico).TipoPrestador == TipoPrestador.Temporario ? (papel as PrestadorServico).Empresa.Apelido : (papel as PrestadorServico).Fornecedor.NomeFantasia : (papel as Visitante).Empresa : ""

            });

            return Json(listPessoaCritica.ToList());
        }

        /// <summary>
        /// Retorna a página de pessoaCritica.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index()
        {
            return View();

        }

        /// <summary>
        /// Inclui um novo pessoaCritica.
        /// </summary>
        ///
        /// <param name="pessoaCritica">
        /// O(a) pessoa critica.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Incluir(PessoaCritica pessoaCritica, List<MidiaModelView> midias)
        {
            IRepositorioPessoaCritica repPessoaCritica = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaCritica>(UnidadeTrabalho);
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            IRepositorioPessoaFisica repPessoaFisica = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaFisica>(UnidadeTrabalho);
            IRepositorioPessoa repPessoa = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoa>(UnidadeTrabalho);
            PessoaCritica pessoaCriticaA = repPessoaCritica.ObterPorId(pessoaCritica.Codigo);
            if (!string.IsNullOrWhiteSpace(pessoaCritica.PessoaFisica.Codigo))
                pessoaCritica.PessoaFisica = repPessoaFisica.ObterPorId(pessoaCritica.PessoaFisica.Codigo);
            else
            {
                var pFisica = new PessoaFisica()
                {
                    Apelido = Globalsys.Util.Tools.ObtemApelido(pessoaCritica.PessoaFisica.Nome),
                    Nome = pessoaCritica.PessoaFisica.Nome,
                    CPF = !string.IsNullOrWhiteSpace(pessoaCritica.PessoaFisica.CPF) ? pessoaCritica.PessoaFisica.CPF.Replace(".", "").Replace("-", "") : null,
                    Passaporte = !string.IsNullOrWhiteSpace(pessoaCritica.PessoaFisica.Passaporte) ? pessoaCritica.PessoaFisica.Passaporte : null,
                    DataRegistro = DateTime.Now
                };

                pFisica.GerarCodigoPessoa(repPessoaFisica.ObterTodos());
                repPessoaFisica.Salvar(pFisica);
                repPessoaFisica.Validar(EstadoObjeto.Novo, pFisica);
                pessoaCritica.PessoaFisica = pFisica;
            }

            string userLogado = HttpContext.User.Identity.Name;

            pessoaCritica.Responsavel = repUsuario.ObterPorLogin(userLogado);
            pessoaCritica.DataRegistro = DateTime.Now;
            pessoaCritica.Ativar();
            repPessoaCritica.Validar(pessoaCritica, EstadoObjeto.Novo);
            repPessoaCritica.Salvar(pessoaCritica);

            #region Midias

            if (midias != null && midias.Count > 0)
            {

                string path = ConfigurationManager.AppSettings.Get("pathUploads");

                if (!System.IO.Directory.Exists(path))
                    throw new CoreException("O diretório onde a midia seria armazanada não está acessivel.");

                IRepositorioPessoaCriticaMidia repPessoaCriticaMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaCriticaMidia>(UnidadeTrabalho);
                IRepositorioMidia repMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioMidia>(UnidadeTrabalho);

                int cont = 1;

                foreach (MidiaModelView midia in midias ?? Enumerable.Empty<MidiaModelView>())
                {
                    if (midia == null)
                        continue;
                    string _codigoPessoaCritica = pessoaCritica.Codigo.ToString();
                    string _cpf = pessoaCritica.PessoaFisica.CPF;
                    string _passaporte = pessoaCritica.PessoaFisica.Passaporte;
                    Papel _papel = pessoaCritica.PessoaFisica.ObterPapel();
                    string _codigoPapel = (_papel == null ? string.Empty : _papel.Codigo.ToString());

                    string formato = "PC-{0}-{1}-{2}-{3}-DocCritica-{4}.{5}";

                    string nomeDisco = string.Format(formato, _codigoPessoaCritica, _cpf, _passaporte, _codigoPapel, cont, midia.Extensao);

                    Midia m = new Midia();
                    m.DataRegistro = midia.DataRegistro;
                    m.Extensao = midia.Extensao;
                    m.MimeType = midia.MIME;
                    m.NomeArquivo = nomeDisco;
                    repMidia.Salvar(m);

                    pessoaCritica.Midias.Add(repPessoaCriticaMidia.SalvarPessoaCriticaMidia(pessoaCritica, m));

                    System.IO.File.WriteAllBytes(m.Url, Convert.FromBase64String(midia.Arquivo));

                    cont++;
                }
            }

            #endregion

            return Json(montaObj(pessoaCritica));

        }

        /// <summary>
        /// Cria um objeto contendo informações relacionadas ao conteudo gerenciado por
        /// PessoaCriticaController.
        /// </summary>
        ///
        /// <param name="pessoaCritica">
        /// O(a) pessoa critica.
        /// </param>
        ///
        /// <returns>
        /// Um(a) object.
        /// </returns>
        private object montaObj(PessoaCritica pessoaCritica)
        {
            var pessoaCriticaModel = new PessoaCriticaModelView();
            pessoaCriticaModel.Codigo = pessoaCritica.Codigo;
            pessoaCriticaModel.DataBloqueio = pessoaCritica.DataBloqueio;
            pessoaCriticaModel.MotivoBloqueio = pessoaCritica.MotivoBloqueio.Nome;
            pessoaCriticaModel.MotivoBloqueioCod = pessoaCritica.MotivoBloqueio.Codigo;
            pessoaCriticaModel.Nome = pessoaCritica.PessoaFisica.Nome;

            pessoaCriticaModel.PessoaFisica = new PessoaFisica()
            {
                Codigo = pessoaCritica.PessoaFisica.Codigo,
                Nome = pessoaCritica.PessoaFisica.Nome,
                Passaporte = pessoaCritica.PessoaFisica.Passaporte,
                CPF = !string.IsNullOrEmpty(pessoaCritica.PessoaFisica.CPF) ? pessoaCritica.PessoaFisica.CPFFormatado() : null
            };

            pessoaCriticaModel.Nome = pessoaCritica.PessoaFisica.Nome;
            pessoaCriticaModel.Passaporte = pessoaCritica.PessoaFisica.Passaporte;
            pessoaCriticaModel.CPF = !string.IsNullOrEmpty(pessoaCritica.PessoaFisica.CPF) ? pessoaCritica.PessoaFisica.CPFFormatado() : null;

            pessoaCriticaModel.Observacao = pessoaCritica.Observacao;

            IRepositorioPessoaFisica repPessoa = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaFisica>(UnidadeTrabalho);
            PessoaFisica pf = repPessoa.ObterPorId(pessoaCritica.Responsavel.Codigo);
            Papel papel = pf.ObterPapel();

            pessoaCriticaModel.Responsavel = pf.Nome;
            pessoaCriticaModel.ResponsavelEmpresa = papel != null && (papel is Colaborador) ? (papel as Colaborador).Empresa.Apelido : "";
            pessoaCriticaModel.ResponsavelFuncao = papel != null && (papel is Colaborador) ? (papel as Colaborador).Cargo != null ? papel.Cargo.Nome : "" : "";
            //pessoaCriticaModel.ResponsavelEmpresa = ((Colaborador)pf.ObterPapel() != null) ? ((Colaborador)pf.ObterPapel()).Empresa.Apelido : "";
            //pessoaCriticaModel.ResponsavelFuncao = (((Colaborador)pf.ObterPapel()) != null) ? ((((Colaborador)pf.ObterPapel()).Cargo != null) ? ((Colaborador)pf.ObterPapel()).Cargo.Nome : "") : "";
            pessoaCriticaModel.Ativo = pessoaCritica.Ativo;
            pessoaCriticaModel.Midias = pessoaCritica.Midias.Select(m => new MidiaModelView()
            {
                Arquivo = null,
                CodigoMidia = m.Midia.Codigo,
                DataRegistro = m.Midia.DataRegistro,
                Extensao = m.Midia.Extensao,
                MIME = m.Midia.MimeType,
                NomeDisco = m.Midia.NomeArquivo,
                NomeOriginal = string.Empty,
                URL = m.Midia.Url
            }).ToList();

            return pessoaCriticaModel;
        }

        /// <summary>
        /// Altera um registro de pessoaCritica.
        /// </summary>
        ///
        /// <param name="pessoaCritica">
        /// O(a) pessoa critica.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Alterar(PessoaCritica pessoaCritica, List<MidiaModelView> midias)
        {
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            IRepositorioPessoaFisica repPessoaFisica = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaFisica>(UnidadeTrabalho);
            IRepositorioPessoaCritica repPessoaCritica = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaCritica>(UnidadeTrabalho);
            PessoaCritica pessoaCriticaA = repPessoaCritica.ObterPorId(pessoaCritica.Codigo);
            pessoaCriticaA.MotivoBloqueio = new MotivoBloqueio()
            {
                Codigo = pessoaCritica.MotivoBloqueio.Codigo,
                Nome = pessoaCritica.MotivoBloqueio.Nome
            };
            pessoaCriticaA.Observacao = pessoaCritica.Observacao;
            string userLogado = HttpContext.User.Identity.Name;
            pessoaCriticaA.Responsavel = new Usuario()
            {
                Codigo = repUsuario.ObterPorLogin(userLogado).Codigo
            };
            
            if (!string.IsNullOrWhiteSpace(pessoaCritica.PessoaFisica.Codigo))
                pessoaCriticaA.PessoaFisica = repPessoaFisica.ObterPorId(pessoaCritica.PessoaFisica.Codigo);
            else
            {
                var pFisica = new PessoaFisica()
                {
                    Apelido = Globalsys.Util.Tools.ObtemApelido(pessoaCritica.PessoaFisica.Nome),
                    Nome = pessoaCritica.PessoaFisica.Nome,
                    CPF = !string.IsNullOrWhiteSpace(pessoaCritica.PessoaFisica.CPF) ? pessoaCritica.PessoaFisica.CPF.Replace(".", "").Replace("-", "") : null,
                    Passaporte = !string.IsNullOrWhiteSpace(pessoaCritica.PessoaFisica.Passaporte) ? pessoaCritica.PessoaFisica.Passaporte : null,
                    DataRegistro = DateTime.Now
                };

                pFisica.GerarCodigoPessoa(repPessoaFisica.ObterTodos());
                repPessoaFisica.Salvar(pFisica);
                repPessoaFisica.Validar(EstadoObjeto.Novo, pFisica);
                pessoaCriticaA.PessoaFisica = pFisica;
            }
            
            repPessoaCritica.Validar(pessoaCriticaA, EstadoObjeto.Alterado);
            repPessoaCritica.Atualizar(pessoaCriticaA);

            #region Midias

            if (midias != null && midias.Count > 0)
            {

                IRepositorioPessoaCriticaMidia repPessoaCriticaMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaCriticaMidia>(UnidadeTrabalho);

                int[] codigosMidias = midias.Select(m => m.CodigoMidia).ToArray();

                List<MidiaModelView> midiasNovas = midias.Where(m => m.CodigoMidia <= 0).ToList();
                List<PessoaCriticaMidia> midiasInativadas = repPessoaCriticaMidia.ObterTodos().Where(m => !codigosMidias.Contains(m.Midia.Codigo) && m.PessoaCritica.Codigo == pessoaCritica.Codigo).ToList();

                if (midiasNovas != null && midiasNovas.Count > 0)
                {
                    string path = ConfigurationManager.AppSettings.Get("pathUploads");

                    if (!System.IO.Directory.Exists(path))
                        throw new CoreException("O diretório onde a midia seria armazanada não está acessivel.");

                    IRepositorioMidia repMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioMidia>(UnidadeTrabalho);

                    int cont = repPessoaCriticaMidia.ObterTodos().Where(dm => dm.PessoaCritica.Codigo == pessoaCritica.Codigo).Count() + 1;

                    foreach (MidiaModelView midia in midiasNovas ?? Enumerable.Empty<MidiaModelView>())
                    {
                        if (midia == null)
                            continue;

                        PessoaFisica pesFisica = repPessoaFisica.ObterPorId(pessoaCritica.PessoaFisica.Codigo);
                        string _codigoPessoaCritica = pessoaCritica.Codigo.ToString();
                        string _cpf = pesFisica.CPF;
                        string _passaporte = pesFisica.Passaporte;
                        Papel _papel = pesFisica.ObterPapel();
                        string _codigoPapel = (_papel == null ? string.Empty : _papel.Codigo.ToString());

                        string formato = "PC-{0}-{1}-{2}-{3}-DocCritica-{4}.{5}";

                        string nomeDisco = string.Format(formato, _codigoPessoaCritica, _cpf, _passaporte, _codigoPapel, cont, midia.Extensao);

                        Midia m = new Midia();
                        m.DataRegistro = midia.DataRegistro;
                        m.Extensao = midia.Extensao;
                        m.MimeType = midia.MIME;
                        m.NomeArquivo = nomeDisco;

                        repMidia.Salvar(m);

                        pessoaCritica.Midias.Add(repPessoaCriticaMidia.SalvarPessoaCriticaMidia(pessoaCritica, m));

                        System.IO.File.WriteAllBytes(m.Url, Convert.FromBase64String(midia.Arquivo));

                        cont++;
                    }
                }

                InativarPessoasCriticasMidias(repPessoaCriticaMidia, midiasInativadas);
            }
            else
            {
                IRepositorioPessoaCriticaMidia repPessoaCriticaMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaCriticaMidia>(UnidadeTrabalho);

                List<PessoaCriticaMidia> midiasInativadas = repPessoaCriticaMidia.ObterTodos().Where(m => m.PessoaCritica.Codigo == pessoaCritica.Codigo).ToList();

                InativarPessoasCriticasMidias(repPessoaCriticaMidia, midiasInativadas);
            }


            #endregion
            return Json(montaObj(pessoaCriticaA));
        }

        private static void InativarPessoasCriticasMidias(IRepositorioPessoaCriticaMidia repPessoaCriticaMidia, List<PessoaCriticaMidia> midiasInativadas)
        {
            if (midiasInativadas != null && midiasInativadas.Count > 0)
            {
                foreach (var item in midiasInativadas)
                {
                    item.Inativar();
                    repPessoaCriticaMidia.Validar(item, EstadoObjeto.Removido);
                    repPessoaCriticaMidia.Salvar(item);

                }
            }
        }

        /// <summary>
        /// Ativar inativar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult AtivarInativar(int codigo)
        {
            IRepositorioPessoaCritica repPessoaCritica = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaCritica>(UnidadeTrabalho);
            PessoaCritica pessoaCritica = repPessoaCritica.ObterPorId(codigo);
            if (pessoaCritica.Ativo)
            {
                pessoaCritica.Inativar();
            }
            else
            {
                repPessoaCritica.Validar(pessoaCritica, EstadoObjeto.Ativado);
                pessoaCritica.Ativar();
            }
            repPessoaCritica.Atualizar(pessoaCritica);
            return Json(montaObj(pessoaCritica));
        }

        /// <summary>
        /// Selecionar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Selecionar(int codigo)
        {
            IRepositorioPessoaCritica repPessoaCritica = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaCritica>(UnidadeTrabalho);
            PessoaCritica pessoaCritica = repPessoaCritica.ObterPorId(codigo);
            return Json(montaObj(pessoaCritica));
        }


    }
}
