﻿using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Pessoas;
using Controlsys.Dominio.Seguranca;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorios.Acesso;
using Controlsys.Repositorios.Empresas;
using Controlsys.Repositorios.Parametros;
using Controlsys.Repositorios.Pessoas;
using Controlsys.Repositorios.Seguranca;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Tree;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar pedido compras.
    /// </summary>
    [ExtendController]
    public class PedidoCompraController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /PedidoCompra/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.PedidoCompraController.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public PedidoCompraController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        /// <summary>
        /// Retorna a página de pedidoCompra.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Obter prazo limite.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="codigoProjeto">
        /// O(a) codigo projeto.
        /// </param>
        /// <param name="codigoPedido">
        /// O(a) codigo pedido.
        /// </param>
        /// <param name="estado">
        /// O(a) estado.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ObterPrazoLimite(int? codigoProjeto, int? codigoPedido, int? prazoModificado, EstadoObjeto estado)
        {
            IRepositorioPedidoCompra repPedidoCompra = Fabrica.Instancia.ObterRepositorio<IRepositorioPedidoCompra>(UnidadeTrabalho);
            IRepositorioProjeto repProjeto = Fabrica.Instancia.ObterRepositorio<IRepositorioProjeto>(UnidadeTrabalho);
            IRepositorioPrazo repPrazo = Fabrica.Instancia.ObterRepositorio<IRepositorioPrazo>(UnidadeTrabalho);

            PedidoCompra pedidoCompra = null;
            Projeto projeto = null;

            DateTime? dataInicio = null;
            DateTime? dataFim = null;
            bool aditivo = estado == EstadoObjeto.Alterado;

            if (codigoPedido.HasValue)
            {
                pedidoCompra = repPedidoCompra.ObterPorId(codigoPedido.Value);

                Prazo prazo = pedidoCompra.Prazos.OrderByDescending(p => p.DataRegistro).FirstOrDefault();

                if (prazo != null)
                {
                    dataInicio = prazo.DataInicio;
                    dataFim = prazo.DataFim.AddHours(12);
                }
            }
            else if (codigoProjeto.HasValue)
            {
                projeto = repProjeto.ObterPorId(codigoProjeto);

                dataInicio = projeto.DataInicio;
                dataFim = projeto.DataFim.AddHours(12);
            }
            else
                throw new CoreException("O projeto não foi selecionado.");

            if (prazoModificado.HasValue && prazoModificado.Value != 0)
            {
                Prazo p = repPrazo.ObterPorId(prazoModificado.Value);
                return Json(new
                {
                    DataInicioLimite = dataInicio,
                    DataFimLimite = dataFim,
                    Aditivo = p.Aditivo,
                    DataInicio = p.DataInicio,
                    DataFim = p.DataFim.AddHours(12),
                    Observacao = p.Observacao
                });

            }

            return Json(new
            {
                DataInicioLimite = dataInicio,
                DataFimLimite = dataFim,
                Aditivo = aditivo,
                DataInicio = string.Empty,
                DataFim = string.Empty,
                Observacao = string.Empty
            });
        }

        /// <summary>
        /// Incluir prazo.
        /// </summary>
        ///
        /// <param name="prazo">
        /// O(a) prazo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        //[Transaction]
        public ActionResult IncluirPrazo(Prazo prazo)
        {
            IRepositorioPrazo repPrazo = Fabrica.Instancia.ObterRepositorio<IRepositorioPrazo>(UnidadeTrabalho);
            IRepositorioPedidoCompra repPedidoCompra = Fabrica.Instancia.ObterRepositorio<IRepositorioPedidoCompra>(UnidadeTrabalho);

            PedidoCompra pedidoCompra = repPedidoCompra.ObterPorId(prazo.PedidoCompra.Codigo);
            var ultimoPrazoAtivo = pedidoCompra.Prazos.Where(x => x.Ativo).OrderByDescending(x => x.Codigo).FirstOrDefault();

            if (prazo.Codigo == 0)
            {
                prazo.PedidoCompra = pedidoCompra;
                prazo.Aditivo = true;
                prazo.Ativar();
                prazo.DataRegistro = DateTime.Now;
            }
            else
            {
                Prazo prazoAtualizado = repPrazo.ObterPorId(prazo.Codigo);
                prazoAtualizado.DataFim = prazo.DataFim;
                prazoAtualizado.DataInicio = prazo.DataInicio;
                prazoAtualizado.Observacao = prazo.Observacao;

                prazo = prazoAtualizado;
            }

            UnidadeTrabalho.BeginTransaction();
            try
            {
                repPrazo.Validar(prazo, prazo.Codigo == 0 ? EstadoObjeto.Novo : EstadoObjeto.Alterado);
                repPrazo.Salvar(prazo);
                UnidadeTrabalho.Commit();
            }
            catch (Exception)
            {
                UnidadeTrabalho.Rollback();
            }

            var thread = new Thread(new ThreadStart(() => { ExecutarRegrasSincronizacaoAcesso(prazo.PedidoCompra.Codigo, ultimoPrazoAtivo?.DataFim); }));
            thread.Start();

            return Json(montaObj(prazo));
        }

        public void ExecutarRegrasSincronizacaoAcesso(int codigoPedido, DateTime? dataPrazoFimPredecessor)
        {
            Thread.Sleep(7000);

            IRepositorioSincronizaPessoaAcesso repSincronizaPessoaAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioSincronizaPessoaAcesso>(UnidadeTrabalho);
            IRepositorioPedidoCompra repPedidoCompra = Fabrica.Instancia.ObterRepositorio<IRepositorioPedidoCompra>(UnidadeTrabalho);
            IRepositorioAlocacaoColaborador repAlocacao = Fabrica.Instancia.ObterRepositorio<IRepositorioAlocacaoColaborador>(UnidadeTrabalho);
            IRepositorioPrazo repPrazo = Fabrica.Instancia.ObterRepositorio<IRepositorioPrazo>(UnidadeTrabalho);

            var pedidoCompra = repPedidoCompra.ObterPorId(codigoPedido);
            var ultimoPrazoAtivo = repPrazo.ObterTodos().Where(x => x.PedidoCompra.Codigo == codigoPedido && x.Ativo).OrderByDescending(x => x.Codigo).FirstOrDefault();
            var colaboradores = repAlocacao.ObterTodos().Where(x => x.PedidoCompra.Codigo == codigoPedido && x.Ativo);

            foreach (var colaborador in colaboradores)
            {
                try
                {
                    if (!dataPrazoFimPredecessor.HasValue || DateTime.Now.Date > dataPrazoFimPredecessor.Value.Date)
                    {
                        repSincronizaPessoaAcesso.Sincronizar(colaborador.Papel, TipoSincronizacaoPessoa.Conceder, null);
                    }


                    //if (ultimoPrazoAtivo != null && DateTime.Now.Date > ultimoPrazoAtivo.DataFim.Date)
                    //    repSincronizaPessoaAcesso.Sincronizar(colaborador.Papel, TipoSincronizacaoPessoa.Suspender, null);
                    //else
                    //{
                    //    if (!dataPrazoFimPredecessor.HasValue || DateTime.Now.Date > dataPrazoFimPredecessor.Value.Date)
                    //        repSincronizaPessoaAcesso.Sincronizar(colaborador.Papel, TipoSincronizacaoPessoa.Conceder, null);
                    //}
                }
                catch (Exception ex)
                {
                    throw new CoreException(ex.Message);
                }
            }
        }

        /// <summary>
        /// Obter por projeto.
        /// </summary>
        ///
        /// <param name="codProjeto">
        /// O(a) cod projeto.
        /// </param>
        /// <param name="codEmpresa">
        /// O(a) cod empresa.
        /// </param>
        /// <param name="numeroPO">
        /// O(a) numero po.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ObterPorProjeto(int codProjeto, string codEmpresa, string numeroPO)
        {
            IRepositorioPedidoCompra repPedido = Fabrica.Instancia.ObterRepositorio<IRepositorioPedidoCompra>(UnidadeTrabalho);

            PedidoCompra pedido = repPedido.ObterPorProjeto(codProjeto);

            if (pedido != null)
                return Json(criarNode(null, pedido, codEmpresa, numeroPO));
            else
                return Json(null);
        }

        /// <summary>
        /// Inclui um novo pedidoCompra.
        /// </summary>
        ///
        /// <param name="pedido">
        /// O(a) pedido.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Incluir(PedidoCompra pedido)
        {
            IRepositorioPedidoCompra repPedido = Fabrica.Instancia.ObterRepositorio<IRepositorioPedidoCompra>(UnidadeTrabalho);
            IRepositorioPrazo repPrazo = Fabrica.Instancia.ObterRepositorio<IRepositorioPrazo>(UnidadeTrabalho);
            IRepositorioSetorCustoPedido repSetorCustoPedido = Fabrica.Instancia.ObterRepositorio<IRepositorioSetorCustoPedido>(UnidadeTrabalho);
            IRepositorioSetorCusto repSetor = Fabrica.Instancia.ObterRepositorio<IRepositorioSetorCusto>(UnidadeTrabalho);
            IRepositorio<ColaboradorPedido> repColabPed = Fabrica.Instancia.ObterRepositorio<IRepositorio<ColaboradorPedido>>(UnidadeTrabalho);
            IRepositorioColaborador repColaborador = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);
            IRepositorioEmpresa repEmpresa = Fabrica.Instancia.ObterRepositorio<IRepositorioEmpresa>(UnidadeTrabalho);

            pedido.DataRegistro = DateTime.Now;
            pedido.Status = StatusPedido.Ativo;
            pedido.DataStatus = DateTime.Now;
            pedido.Empresa = repEmpresa.ObterPorId(pedido.Empresa.Codigo);

            if (pedido.PedidoCompraPai != null)
                pedido.PedidoCompraPai = repPedido.ObterPorId(pedido.PedidoCompraPai.Codigo);

            repPedido.Salvar(pedido);

            foreach (SetorCustoPedido setorCustoPedido in pedido.Setores)
            {
                setorCustoPedido.DataRegistro = DateTime.Now;
                setorCustoPedido.Ativar();
                setorCustoPedido.SetorCusto = repSetor.ObterPorId(setorCustoPedido.SetorCusto.Codigo);
                setorCustoPedido.Pedido = pedido;

                repSetorCustoPedido.Salvar(setorCustoPedido);
            }

            foreach (Prazo prazo in pedido.Prazos)
            {
                prazo.PedidoCompra = pedido;
                prazo.DataRegistro = DateTime.Now;
                prazo.Ativar();

                repPrazo.Salvar(prazo);
            }

            foreach (ColaboradorPedido colaboradorPedido in pedido.GestoresResponsaveis)
            {
                colaboradorPedido.Pedido = pedido;
                colaboradorPedido.DataRegistro = DateTime.Now;
                colaboradorPedido.Ativar();
                colaboradorPedido.Colaborador = repColaborador.ObterPorId(colaboradorPedido.Colaborador.Codigo);

                repColabPed.Salvar(colaboradorPedido);
            }

            repPedido.Validar(pedido, Globalsys.Validacao.EstadoObjeto.Novo);

            int? codigoPai = pedido.PedidoCompraPai != null ? pedido.PedidoCompraPai.Codigo : (int?)null;

            return Json(criarNode(codigoPai, pedido));
        }

        /// <summary>
        /// Altera um registro de pedidoCompra.
        /// </summary>
        ///
        /// <param name="pedido">
        /// O(a) pedido.
        /// </param>
        /// <param name="propModificadas">
        /// O(a) property modificadas.
        /// </param>
        /// <param name="setoresNew">
        /// O(a) setores new.
        /// </param>
        /// <param name="setoresDel">
        /// O(a) setores delete.
        /// </param>
        /// <param name="responsaveisNew">
        /// O(a) responsaveis new.
        /// </param>
        /// <param name="responsaveisDel">
        /// O(a) responsaveis delete.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        //[Transaction]
        public ActionResult Alterar(PedidoCompra pedido, string[] propModificadas, int[] setoresNew, int[] setoresDel, int[] responsaveisNew, int[] responsaveisDel, int[] prazosNew, int[] prazosDel)
        {
            IRepositorioPedidoCompra repPedidoCompra = Fabrica.Instancia.ObterRepositorio<IRepositorioPedidoCompra>(UnidadeTrabalho);
            IRepositorioPrazo repPrazo = Fabrica.Instancia.ObterRepositorio<IRepositorioPrazo>(UnidadeTrabalho);
            IRepositorioSetorCustoPedido repSetorCustoPedido = Fabrica.Instancia.ObterRepositorio<IRepositorioSetorCustoPedido>(UnidadeTrabalho);
            IRepositorioSetorCusto repSetorCusto = Fabrica.Instancia.ObterRepositorio<IRepositorioSetorCusto>(UnidadeTrabalho);
            IRepositorioColaborador repColaborador = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);
            IRepositorio<ColaboradorPedido> repColabPed = Fabrica.Instancia.ObterRepositorio<IRepositorio<ColaboradorPedido>>(UnidadeTrabalho);

            pedido = repPedidoCompra.ObterPorId(pedido, propModificadas);
            int codigoPedido = pedido.Codigo;

            if (setoresNew != null)
                foreach (int codSetorPedido in setoresNew)
                {
                    SetorCustoPedido setorCustoPedido = new SetorCustoPedido();
                    setorCustoPedido.SetorCusto = repSetorCusto.ObterPorId(codSetorPedido);
                    setorCustoPedido.Pedido = pedido;
                    setorCustoPedido.DataRegistro = DateTime.Now;
                    setorCustoPedido.Ativar();

                    repSetorCustoPedido.Salvar(setorCustoPedido);

                    pedido.Setores.Add(setorCustoPedido);
                }

            if (setoresDel != null)
                foreach (SetorCustoPedido setorCustoPedido in pedido.Setores.Where(scp => setoresDel.Contains(scp.SetorCusto.Codigo)))
                    setorCustoPedido.Inativar();

            if (responsaveisNew != null)
                foreach (int codColaborador in responsaveisNew)
                {
                    ColaboradorPedido colaboradorPedido = new ColaboradorPedido();
                    colaboradorPedido.Pedido = pedido;
                    colaboradorPedido.Colaborador = repColaborador.ObterPorId(codColaborador);
                    colaboradorPedido.DataRegistro = DateTime.Now;
                    colaboradorPedido.Ativar();
                    repColabPed.Salvar(colaboradorPedido);

                    pedido.GestoresResponsaveis.Add(colaboradorPedido);
                }

            if (responsaveisDel != null)
                foreach (ColaboradorPedido colaboradorPedido in pedido.GestoresResponsaveis.Where(cp => responsaveisDel.Contains(cp.Colaborador.Codigo)))
                    colaboradorPedido.Inativar();

            if (prazosNew != null)
                foreach (int codPrazo in prazosNew)
                {
                    Prazo prazo = new Prazo();
                    prazo.PedidoCompra = pedido;
                    prazo.Aditivo = true;
                    prazo.Ativar();
                    prazo.DataRegistro = DateTime.Now;
                    repPrazo.Salvar(prazo);

                    pedido.Prazos.Add(prazo);
                }

            if (prazosDel != null)
            {
                foreach (Prazo prazo in pedido.Prazos.Where(p => prazosDel.Contains(p.Codigo)))
                    prazo.Inativar();
            }

            UnidadeTrabalho.BeginTransaction();
            try
            {
                repPedidoCompra.Validar(pedido, Globalsys.Validacao.EstadoObjeto.Alterado);
                repPedidoCompra.Atualizar(pedido);
                UnidadeTrabalho.Commit();
            }
            catch (Exception)
            {
                UnidadeTrabalho.Rollback();
                throw;
            }

            if (prazosDel != null && prazosDel.Any())
            {
                var thread = new Thread(new ThreadStart(() => { ExecutarRegrasSincronizacaoAcesso(codigoPedido, null); }));
                thread.Start();
            }

            return Json(montaObj(pedido));
        }

        /// <summary>
        /// Selecionar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Selecionar(int? codigo = null)
        {
            if (!codigo.HasValue)
                return Json(null);
            IRepositorioPedidoCompra repPedidoCompra = Fabrica.Instancia.ObterRepositorio<IRepositorioPedidoCompra>(UnidadeTrabalho);

            PedidoCompra pedidoCompra = repPedidoCompra.ObterPorId(codigo);

            return Json(montaObj(pedidoCompra));
        }

        /// <summary>
        /// Ativar inativar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult AtivarInativar(int codigo)
        {
            IRepositorioPedidoCompra repPedido = Fabrica.Instancia.ObterRepositorio<IRepositorioPedidoCompra>(UnidadeTrabalho);
            PedidoCompra pedido = repPedido.ObterPorId(codigo);

            List<int> ids = null;

            if (pedido.Status == StatusPedido.Ativo)
                ids = repPedido.Inativar(pedido);
            else
            {
                var pedidos = repPedido.ObterTodos().Where(x => x.Codigo != pedido.Codigo && x.Numero == pedido.Numero && x.Empresa.Codigo == pedido.Empresa.Codigo && x.Status == StatusPedido.Ativo);
                if (pedidos.Any())
                    throw new CoreException("Não foi possível executar a operação, pois já existe um pedido ativo de mesmo número cadastrado para essa empresa.");

                repPedido.Ativar(pedido);
            }

            return Json(new PedidoCompraAtivarInativarModelView()
            {
                IdsInativar = ids
            });
        }

        /// <summary>
        /// Remover.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Remover(int codigo)
        {
            IRepositorioPedidoCompra repPedido = Fabrica.Instancia.ObterRepositorio<IRepositorioPedidoCompra>(UnidadeTrabalho);

            PedidoCompra pedido = repPedido.ObterPorId(codigo);

            repPedido.Remover(pedido, StatusPedido.Removido);

            return Json(null);
        }

        /// <summary>
        /// Obter colaboradores.
        /// </summary>
        ///
        /// <param name="codPedido">
        /// O(a) cod pedido.
        /// </param>
        /// <param name="somenteAlocados">
        /// true to somente alocados.
        /// </param>
        /// <param name="somenteDesalocados">
        /// true to somente desalocados.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ObterColaboradores(int codPedido, bool somenteAlocados = false, bool somenteDesalocados = false, Ext.Net.StoreRequestParameters parameters = null, bool ignoreParameters = false, StatusPapelLog? status = null, string cpf = null, string passaporte = null, string nome = null, string setorcusto = null)
        {
            IQueryable<AlocacaoColaborador> colabs = FiltrarAlocacaoColaborador(codPedido, somenteAlocados, somenteDesalocados, status, cpf, passaporte, nome, setorcusto);
            int total = colabs.Count();

            //List<AlocacaoColaborador> colabs = new List<AlocacaoColaborador>();
            //var qtd = colabsx.Count();

            //return Json(colabs.Select(ca => formatObject(ca)).ToArray());         

            List<AlocacaoColaboradorModelView> data = montaListaObj(colabs, parameters, ignoreParameters);
            return new Ext.Net.MVC.StoreResult(data, total);
        }

        private List<AlocacaoColaboradorModelView> montaListaObj(IQueryable<AlocacaoColaborador> regs, Ext.Net.StoreRequestParameters parameters = null, bool ignoreParameters = false)
        {
            if (!ignoreParameters)
            {
                if (parameters == null)
                    parameters = new Ext.Net.StoreRequestParameters();
                regs = regs.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 25);
            }

            return regs.ToList().Select(c => new AlocacaoColaboradorModelView
            {
                Codigo = c.Papel.Codigo,
                CPFPassaporte = c.Papel.PessoaFisica.CPFFormatado() ?? c.Papel.PessoaFisica.Passaporte,
                SetorCusto = c.SetorCusto != null ? c.SetorCusto.Nome : null,
                Nome = c.Papel.PessoaFisica.Nome,
                Status = c.Papel.ObterLogPapel().Status.ObterDescricaoEnum()
            }).ToList();
        }

        /// <summary>
        /// Format object.
        /// </summary>
        ///
        /// <param name="ca">
        /// O(a) ca.
        /// </param>
        ///
        /// <returns>
        /// Um(a) object.
        /// </returns>
        private object formatObject(AlocacaoColaborador ca)
        {
            try
            {
                Colaborador colaborador = ca.Papel as Colaborador;

                return new
                {
                    colaborador.Codigo,
                    CPFPassaporte = colaborador.PessoaFisica.CPFFormatado() ?? colaborador.PessoaFisica.Passaporte,
                    SetorCusto = ca.SetorCusto != null ? ca.SetorCusto.Nome : null,
                    colaborador.PessoaFisica.Nome,
                    //GrupoTrabalho = grupo.GrupoTrabalho.Nome,
                    Status = colaborador.ObterLogPapel().Status.ObterDescricaoEnum()
                };
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        private IQueryable<AlocacaoColaborador> FiltrarAlocacaoColaborador(int codPedido, bool somenteAlocados = false, bool somenteDesalocados = false, StatusPapelLog? status = null, string cpf = null, string passaporte = null, string nome = null, string setorcusto = null)
        {
            IRepositorioPedidoCompra repPedido = Fabrica.Instancia.ObterRepositorio<IRepositorioPedidoCompra>(UnidadeTrabalho);
            PedidoCompra pedido = repPedido.ObterPorId(codPedido);

            //IEnumerable<AlocacaoColaborador> colabs = pedido.ColaboradoresAlocados;
            IRepositorioAlocacaoColaborador repAlocacaoColab = Fabrica.Instancia.ObterRepositorio<IRepositorioAlocacaoColaborador>(UnidadeTrabalho);
            IQueryable<AlocacaoColaborador> listaColabAloc = repAlocacaoColab.ObterTodos();

            //colabs = colabs.GroupBy(ca => ca.Papel.PessoaFisica).Select(ca => ca.OrderByDescending(ac => ac.DataRegistro).FirstOrDefault());

            listaColabAloc = listaColabAloc.Where(x => x.PedidoCompra.Codigo == pedido.Codigo);

            if (!string.IsNullOrEmpty(cpf))
                listaColabAloc = listaColabAloc.Where(x => x.Papel.PessoaFisica.CPF == cpf);

            if (!string.IsNullOrEmpty(passaporte))
                listaColabAloc = listaColabAloc.Where(x => x.Papel.PessoaFisica.Passaporte == passaporte);

            if (!string.IsNullOrEmpty(nome))
                listaColabAloc = listaColabAloc.Where(x => x.Papel.PessoaFisica.Nome.Contains(nome));

            if (!string.IsNullOrEmpty(setorcusto))
                listaColabAloc = listaColabAloc.Where(x => x.SetorCusto.Nome.Contains(setorcusto));

            if (status != null)
                listaColabAloc = listaColabAloc.Where(x => x.Papel.PapelLogs.OrderByDescending(p => p.Codigo).Select(m => m.Status).FirstOrDefault() == status);

            if (somenteAlocados)
                listaColabAloc = listaColabAloc.Where(ac => ac.Ativo);

            if (somenteDesalocados)
                listaColabAloc = listaColabAloc.Where(ac => !ac.Ativo);

            return listaColabAloc;
        }

        /// <summary>
        /// Alocar colaborador.
        /// </summary>
        ///
        /// <param name="colabs">
        /// O(a) colabs.
        /// </param>
        /// <param name="codSetorCusto">
        /// O(a) cod setor custo.
        /// </param>
        /// <param name="codPedido">
        /// O(a) cod pedido.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult AlocarColaborador(int[] colabs, int codSetorCusto, int codPedido)
        {
            IRepositorioPedidoCompra repPedido = Fabrica.Instancia.ObterRepositorio<IRepositorioPedidoCompra>(UnidadeTrabalho);
            IRepositorioSetorCusto repSetorCusto = Fabrica.Instancia.ObterRepositorio<IRepositorioSetorCusto>(UnidadeTrabalho);
            IRepositorioColaborador repColaborador = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);
            IRepositorioAlocacaoColaborador repAlocacao = Fabrica.Instancia.ObterRepositorio<IRepositorioAlocacaoColaborador>(UnidadeTrabalho);
            //IRepositorioGrupoTrabalho repGrupoTrabalho = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoTrabalho>(UnidadeTrabalho);
            //IRepositorioGrupoTrabalhoColab repGrupoTrabalhoColab = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoTrabalhoColab>(UnidadeTrabalho);
            IRepositorioSincronizaPessoaAcesso repSincronizaPessoaAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioSincronizaPessoaAcesso>(UnidadeTrabalho);

            PedidoCompra pedidoCompra = repPedido.ObterPorId(codPedido);
            SetorCusto setorCusto = repSetorCusto.ObterPorId(codSetorCusto);

            var ultimoPrazoAtivo = pedidoCompra.Prazos.Where(x => x.Ativo).OrderByDescending(x => x.Codigo).FirstOrDefault();

            List<AlocacaoColaborador> lista = new List<AlocacaoColaborador>();
            foreach (int colab in colabs)
            {
                AlocacaoColaborador alocacaoColaborador = new AlocacaoColaborador();
                alocacaoColaborador.PedidoCompra = pedidoCompra;
                alocacaoColaborador.Papel = repColaborador.ObterPorId(colab);
                alocacaoColaborador.SetorCusto = setorCusto;
                alocacaoColaborador.Ativar();
                alocacaoColaborador.DataRegistro = DateTime.Now;
                repAlocacao.Validar(alocacaoColaborador, EstadoObjeto.Novo);
                repAlocacao.Salvar(alocacaoColaborador);

                //if (ultimoPrazoAtivo != null && DateTime.Now.Date > ultimoPrazoAtivo.DataFim.Date)
                //    repSincronizaPessoaAcesso.Sincronizar(alocacaoColaborador.Papel, TipoSincronizacaoPessoa.Suspender, null);
                //else
                repSincronizaPessoaAcesso.Sincronizar(alocacaoColaborador.Papel, TipoSincronizacaoPessoa.Conceder, null);

                lista.Add(alocacaoColaborador);
            }

            return Json(lista.Select(ca => formatObject(ca)).ToArray());

        }

        /// <summary>
        /// Desalocar colaborador.
        /// </summary>
        ///
        /// <param name="colabs">
        /// O(a) colabs.
        /// </param>
        /// <param name="codPedido">
        /// O(a) cod pedido.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult DesalocarColaborador(int[] colabs, int codPedido)
        {
            IRepositorioAlocacaoColaborador repAlocacaoColaborador = Fabrica.Instancia.ObterRepositorio<IRepositorioAlocacaoColaborador>(UnidadeTrabalho);
            IRepositorioPedidoCompra repPedido = Fabrica.Instancia.ObterRepositorio<IRepositorioPedidoCompra>(UnidadeTrabalho);
            IRepositorioSincronizaPessoaAcesso repSincronizaPessoaAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioSincronizaPessoaAcesso>(UnidadeTrabalho);

            PedidoCompra pedido = repPedido.ObterPorId(codPedido);

            //List<AlocacaoColaborador> alocacoes = pedido.ColaboradoresAlocados
            //                                            .Where(ca => colabs.Contains(ca.Papel.Codigo)
            //                                                      && ca.Ativo)
            //                                            .ToList();

            List<AlocacaoColaborador> alocacoes = repAlocacaoColaborador.ObterTodos().Where(ca => ca.PedidoCompra.Codigo == pedido.Codigo
                                                                                         && colabs.Contains(ca.Papel.Codigo)
                                                                                         && ca.Ativo).ToList();

            foreach (AlocacaoColaborador alocacao in alocacoes)
            {
                alocacao.Inativar();
                repAlocacaoColaborador.Atualizar(alocacao);
                repSincronizaPessoaAcesso.Sincronizar(alocacao.Papel, TipoSincronizacaoPessoa.Suspender, null);
            }

            return Json(alocacoes.Select(ac => formatObject(ac)).ToArray());
        }

        /// <summary>
        /// Criar node.
        /// </summary>
        ///
        /// <param name="nodePai">
        /// O(a) node pai.
        /// </param>
        /// <param name="pedido">
        /// O(a) pedido.
        /// </param>
        /// <param name="codEmpresa">
        /// O(a) cod empresa.
        /// </param>
        /// <param name="numeroPO">
        /// O(a) numero po.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Node.
        /// </returns>
        private Node criarNode(int? nodePai, PedidoCompra pedido, string codEmpresa = null, string numeroPO = null)
        {
            Node node = new Node
            {
                Codigo = pedido.Codigo,
                CodigoEmpresa = pedido.Empresa.Codigo,
                NodePai = nodePai,
                Descricao = string.Format("{0} - {1}", pedido.Numero, pedido.Empresa.Apelido),
                Ativo = pedido.Status == StatusPedido.Ativo
            };

            if (!nodePai.HasValue)
            {
                //pedido.SubPedidos = pedido.SubPedidos.Where(sp => (string.IsNullOrEmpty(codEmpresa) || codEmpresa == sp.Empresa.Codigo) &&
                //                                                  (string.IsNullOrEmpty(numeroPO) || numeroPO == sp.Numero))
                //                                     .ToList();

                IRepositorioPedidoCompra pedidoRepository = Fabrica.Instancia.ObterRepositorio<IRepositorioPedidoCompra>(UnidadeTrabalho);

                pedido.SubPedidos = pedidoRepository.ObterTodos().Where(sp =>
                                                                            (string.IsNullOrEmpty(codEmpresa) || codEmpresa == sp.Empresa.Codigo) &&
                                                                            (string.IsNullOrEmpty(numeroPO) || numeroPO == sp.Numero)
                                                                            && sp.PedidoCompraPai == pedido
                                                                            && sp.Projeto.Codigo == pedido.Projeto.Codigo
                                                                            && sp.Status != StatusPedido.Removido
                                                                            ).ToList();
            }

            if (pedido.SubPedidos.Any() && ((string.IsNullOrWhiteSpace(codEmpresa) && !string.IsNullOrWhiteSpace(numeroPO)) && (pedido.SubPedidos.Select(x => x.Empresa.Codigo).Distinct().Count() > 1)))
            {
                var empresas = string.Join(" ", pedido.SubPedidos.Select(x => "[Razão Social: " + x.Empresa.Nome + " - Nome Fantasia: " + x.Empresa.Apelido + "]").Distinct());
                throw new CoreException(string.Format("É necessário selecionar um item na opção de filtro por empresa, pois a PO informada está relacionada a mais de uma empresa, são elas: {0}", empresas));
            }

            foreach (PedidoCompra subPedido in pedido.SubPedidos)
            {
                //Node filho = AdicionarSubpedido(node.Codigo, subPedido);
                //node.NodesFilhos.Add(filho);
                node.NodesFilhos.Add(criarNode(node.Codigo, subPedido));
            }

            return node;
        }

        /// <summary>
        /// Cria um objeto contendo informações relacionadas ao conteudo gerenciado por
        /// PedidoCompraController.
        /// </summary>
        ///
        /// <param name="prazo">
        /// O(a) prazo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) object.
        /// </returns>
        private object montaObj(Prazo prazo)
        {
            return new Prazo()
            {
                Codigo = prazo.Codigo,
                DataInicio = prazo.DataInicio,
                DataFim = prazo.DataFim.AddHours(12),
                Aditivo = prazo.Aditivo
            };
        }

        /// <summary>
        /// Cria um objeto contendo informações relacionadas ao conteudo gerenciado por
        /// PedidoCompraController.
        /// </summary>
        ///
        /// <param name="pedido">
        /// O(a) pedido.
        /// </param>
        ///
        /// <returns>
        /// Um(a) object.
        /// </returns>
        private PedidoCompraModelView montaObj(PedidoCompra pedido)
        {
            PedidoCompraModelView pedidoCompraModelView = new PedidoCompraModelView();
            pedidoCompraModelView.NumeroNome = string.Format("{0,12} {1}", pedido.Numero, pedido.Nome);
            pedidoCompraModelView.Codigo = pedido.Codigo;
            pedidoCompraModelView.Numero = pedido.Numero;
            pedidoCompraModelView.Nome = pedido.Nome;
            pedidoCompraModelView.Observacao = pedido.Observacao;
            pedidoCompraModelView.Projeto = pedido.Projeto.Nome;
            pedidoCompraModelView.PedidoCompraPai = pedido.PedidoCompraPai != null ? pedido.PedidoCompraPai.Codigo : (int?)null;
            pedidoCompraModelView.PedidoCompraPaiNome = pedido.PedidoCompraPai != null ? pedido.PedidoCompraPai.Empresa.Nome : null;
            pedidoCompraModelView.CNAE = pedido.CNAE != null ? pedido.CNAE.Codigo : null;
            pedidoCompraModelView.CNAEDesc = pedido.CNAE != null ? pedido.CNAE.Nome : null;
            pedidoCompraModelView.Empresa = pedido.Empresa.Codigo;
            pedidoCompraModelView.EmpresaDesc = pedido.Empresa.Apelido;
            pedidoCompraModelView.Prazos = pedido.Prazos.Where(p => p.Ativo).Select(p => new Prazo()
            {
                Codigo = p.Codigo,
                DataInicio = p.DataInicio,
                DataFim = p.DataFim.AddHours(12),
                Aditivo = p.Aditivo,
                Ativo = p.Ativo
            }).ToArray();

            pedidoCompraModelView.Setores = pedido.Setores.Select(s => new SetorCustoPedidoModelView()
            {
                Codigo = s.SetorCusto.Codigo,
                Nome = s.SetorCusto.Nome,
                Numero = s.SetorCusto.Numero
            }).ToArray();

            pedidoCompraModelView.GestoresResponsaveis = pedido.GestoresResponsaveis.Select(cp => new ColaboradorPedidoModelView()
            {
                Codigo = cp.Colaborador.Codigo,
                CPF = cp.Colaborador.PessoaFisica.CPFFormatado(),
                Nome = cp.Colaborador.PessoaFisica.Nome
            }).ToArray();

            return pedidoCompraModelView;
        }

        /// <summary>
        /// Obter setor pedido.
        /// </summary>
        ///
        /// <param name="cod_pedido">
        /// O(a) cod pedido.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ObterSetorPedido(int? cod_pedido)
        {
            IRepositorioPedidoCompra repPedido = Fabrica.Instancia.ObterRepositorio<IRepositorioPedidoCompra>(UnidadeTrabalho);
            IRepositorioSetorCusto repSetor = Fabrica.Instancia.ObterRepositorio<IRepositorioSetorCusto>(UnidadeTrabalho);

            IEnumerable<SetorCusto> setoresPedido = null;

            if (cod_pedido != null && cod_pedido > 0) // chamado para alocação e add 2º ou + pedido.
            {
                PedidoCompra pedidoCompra = repPedido.ObterPorId(cod_pedido);
                setoresPedido = pedidoCompra.Setores.Select(t => t.SetorCusto);
            }
            else // Adicionar o 1º pedido
            {
                setoresPedido = repSetor.ObterTodos().Where(t => t.Ativo).ToList();
            }

            var lista = setoresPedido.Select(p => new
            {
                Codigo = p.Codigo,
                Nome = p.Numero + " - " + p.Nome
            }).ToList();

            return Json(lista);
        }

        public ActionResult ObterPedidosPorEmpresaUsuario(string codigoEmpresa = null, bool apenasCodigoENumero = false, bool apenasAtivos = false, bool isPreCadastroColaborador = false, bool isPreCadastroPrestadorServico = false)
        {
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            Usuario usuario = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name);

            IRepositorioPedidoCompra repPedidoCompra = Fabrica.Instancia.ObterRepositorio<IRepositorioPedidoCompra>(UnidadeTrabalho);
            IQueryable<PedidoCompra> lista = repPedidoCompra.ObterTodos();

            if (apenasAtivos)
                lista = repPedidoCompra.ObterTodos().Where(po => po.Status == StatusPedido.Ativo);

            var grupos = usuario.GruposUsuarios.Select(g => g.Grupo);
            var acoes = grupos.SelectMany(g => g.Acoes);

            var podeAprovarReprovarColaborador = (acoes.Where(a => a.Url == "/PreCadastroColaborador/Aprovar" || a.Url == "/PreCadastroColaborador/Reprovar")).Any();
            var podeAprovarReprovarPrestadorServico = (acoes.Where(a => a.Url == "/PreCadastroPrestadorServico/Aprovar" || a.Url == "/PreCadastroPrestadorServico/Reprovar")).Any();

            isPreCadastroColaborador = isPreCadastroColaborador && podeAprovarReprovarColaborador;
            isPreCadastroPrestadorServico = isPreCadastroPrestadorServico && podeAprovarReprovarPrestadorServico;

            bool isPreCadastro = isPreCadastroColaborador || isPreCadastroPrestadorServico;

            if (!usuario.GruposUsuarios.Any(gu => gu.Grupo.IsAdmin()) && !isPreCadastro)
            {
                if (!string.IsNullOrEmpty(codigoEmpresa))
                {
                    lista = lista.Where(l => l.Empresa.Codigo == codigoEmpresa);
                    return Json(lista.Select(l => montaObj(l)).ToArray());
                }

                var papel = usuario.PessoaFisica.ObterPapel(true);
                if (papel == null)
                    return Json(null);

                Empresa empresa = null;
                empresa = (papel is Colaborador) ? (papel as Colaborador).Empresa : ((papel is PrestadorServico) ? (papel as PrestadorServico).Empresa : null);
                if (empresa == null)
                    return Json(null);

                lista = lista.Where(p => p.Empresa.Codigo == empresa.Codigo || p.GestoresResponsaveis.Any(gr => gr.Colaborador.Codigo == papel.Codigo));
            }

            if (apenasCodigoENumero)
            {
                var pedidos = lista.Select(po => new { Codigo = po.Codigo, NumeroNome = po.Numero }).ToArray();
                return Json(pedidos);
            }
            else
            {
                PedidoCompraModelView[] pedidos = lista.ToArray().Select(l => montaObj(l)).ToArray();
                return Json(pedidos);
            }

        }

        public ActionResult ObterGestoresPedido(int codigoPedido)
        {
            IRepositorioPedidoCompra repPedidoCompra = Fabrica.Instancia.ObterRepositorio<IRepositorioPedidoCompra>(UnidadeTrabalho);
            PedidoCompra pedidoCompra = repPedidoCompra.ObterPorId(codigoPedido);

            if (pedidoCompra == null)
                return Json(null);

            return Json(pedidoCompra.GestoresResponsaveis.Select(cp => new ColaboradorPedidoModelView()
            {
                Codigo = cp.Colaborador.Codigo,
                CPF = cp.Colaborador.PessoaFisica.CPFFormatado(),
                Nome = cp.Colaborador.PessoaFisica.Nome
            }).ToArray());
        }


        public ActionResult ObterGestoresPorEmpresaUsuario(string cpf, string codigoEmpresa, string nome, bool isPreCadastroColaborador = false, bool isPreCadastroPrestadorServico = false)
        {
            if ((isPreCadastroColaborador && isPreCadastroPrestadorServico) || (!isPreCadastroColaborador && !isPreCadastroPrestadorServico))
                throw new CoreException("Parametro inválido de tela Prestador x Colaborador.");

            if (string.IsNullOrWhiteSpace(cpf) && string.IsNullOrWhiteSpace(codigoEmpresa) && string.IsNullOrWhiteSpace(nome))
                throw new CoreException("Informe ao menos um parâmetro: CPF, Empresa ou Nome.");

            // Busca todos os grupos ATIVOS do sistema
            IRepositorioGrupo _repositorioGrupo = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupo>(UnidadeTrabalho);
            var gruposAll = _repositorioGrupo.ObterTodos().Where(x => x.Ativo).ToList();

            // Filtra apenas os grupos que tem acesso a funcionalidade de autorizar pré-cadastro
            var gruposFiltered = new List<Grupo>();
            foreach (var item in gruposAll)
            {
                if (item.Acoes.Any(x => x.Ativo && (x.Url == "/PreCadastroPrestadorServico/Autorizar" || x.Url == "/PreCadastroPrestadorServico/NaoAutorizar")))
                {
                    gruposFiltered.Add(item);
                }
            }

            // Seleciona as listas de GRUPO x USUARIO
            var grupoUsuario = new List<GrupoUsuario>();
            gruposFiltered.Where(x => x.Ativo).Select(x => x.GruposUsuarios).ToList().ForEach(gru => grupoUsuario.AddRange(gru));

            // Seleciona os códigos dos usuários que tem permissão de acesso a funcionalidade de autorizar pré-cadastro
            var listaUsuarios = grupoUsuario.Where(x => x.Usuario.Ativo).Select(x => x.Usuario).ToList(); // Lista de usuários com permissão de autorizar ou não um pré cadastro

            IRepositorioColaborador colaboradorRepository = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);
            var listaGestoresColaborador = new List<Colaborador>();
            foreach (var usuario in listaUsuarios)
            {
                if (usuario.PessoaFisica != null)
                {
                    Colaborador colaborador = colaboradorRepository.ObterTodos().FirstOrDefault(x => x.PessoaFisica.Codigo == usuario.PessoaFisica.Codigo);

                    if (colaborador != null)
                        listaGestoresColaborador.Add(colaborador);
                }
            }

            // Aplica filtros da tela
            cpf = cpf.Replace(".", "").Replace("-", "");

            if (!string.IsNullOrEmpty(cpf))
                listaGestoresColaborador = listaGestoresColaborador.Where(g =>
                (!string.IsNullOrEmpty(cpf) && g.PessoaFisica.CPF == cpf)).ToList();

            if (!string.IsNullOrEmpty(codigoEmpresa))
                listaGestoresColaborador = listaGestoresColaborador.Where(g =>
                  (!string.IsNullOrEmpty(codigoEmpresa) && g.Empresa.Codigo == codigoEmpresa)).ToList();

            if (!string.IsNullOrEmpty(nome))
                listaGestoresColaborador = listaGestoresColaborador.Where(g =>
                 (!string.IsNullOrEmpty(nome) && g.PessoaFisica.Nome.ToUpper().Contains(nome.ToUpper()))).ToList();

            var listaModelView = listaGestoresColaborador.Select(cp => new ColaboradorPedidoModelView()
            {
                Codigo = cp.Codigo,
                CPF = cp.PessoaFisica.CPFFormatado(),
                Nome = cp.PessoaFisica.Nome,
                Empresa = cp.Empresa.Apelido
            }).ToArray();

            var listaFinal = listaModelView
                .GroupBy(p => new { p.Codigo, p.CPF, p.Nome, p.Empresa })
                .Select(g => g.First())
                .ToList();

            return Json(listaFinal);

        }


        public ActionResult ObterPedidosEmpresa(string codigoEmpresa = "", bool todosAtivos = false)
        {
            IRepositorioPedidoCompra repPedidoCompra = Fabrica.Instancia.ObterRepositorio<IRepositorioPedidoCompra>(UnidadeTrabalho);

            IQueryable<PedidoCompra> pedidosCompra = repPedidoCompra.ObterTodos().Where(po => po.Status == StatusPedido.Ativo);

            if (!todosAtivos)
                pedidosCompra = pedidosCompra.Where(po => po.Empresa.Codigo.Equals(codigoEmpresa));

            return Json(pedidosCompra.Select(pc => new
            {
                Codigo = pc.Codigo,
                NumeroPedido = pc.Numero
            }).OrderBy(x => x.NumeroPedido).ToList());
        }

        public ActionResult ObterSubContradada(string codigoContratada = "")
        {
            IRepositorioPedidoCompra repPedidoCompra = Fabrica.Instancia.ObterRepositorio<IRepositorioPedidoCompra>(UnidadeTrabalho);
            IQueryable<PedidoCompra> pedidosCompra = repPedidoCompra.ObterTodos().Where(pc => pc.Status == StatusPedido.Ativo && pc.PedidoCompraPai != null && pc.PedidoCompraPai.Empresa.Codigo == codigoContratada);

            return Json(pedidosCompra.Select(pc => new
            {
                Codigo = pc.Empresa.Codigo,
                Nome = pc.Empresa.Nome
            }).OrderBy(x => x.Nome).ToList().Distinct());
        }
    }
}
