﻿using Controlsys.Dominio.Relatorio;
using Controlsys.Infra;
using Controlsys.Repositorio.Relatorio;
using Globalsys;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class RelatorioRestauranteSinteticoController : Controller, IControllerBase
    {

        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        public RelatorioRestauranteSinteticoController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Exportar(string tipoRelatorio, int tipoAlimentacao = 0, string empresa = null, string po = null, DateTime? dataInicial = null, DateTime? dataFinal = null)
        {
                LocalReport relatorio = new LocalReport();
                
                relatorio.ReportPath = Server.MapPath("~/Reports/RelatorioRestauranteSintetico.rdlc");
                var acessos = FiltrarAcessos(tipoAlimentacao, empresa, po, dataInicial, dataFinal).ToList();
                relatorio.DataSources.Add(new ReportDataSource("DTORelatorioRestauranteSintetico", acessos));
                relatorio.SetParameters(new ReportParameter("DataInicio", dataInicial.ToString()));
                relatorio.SetParameters(new ReportParameter("DataFinal", dataFinal.ToString()));
               
            /*
                relatorio.ReportPath = Server.MapPath("~/Reports/relaSintetico.rdlc");
                var acessos = FiltrarAcessos(tipoAlimentacao, empresa, po, dataInicial, dataFinal).ToList();
                relatorio.DataSources.Add(new ReportDataSource("DataSet1", acessos));
            */
                string mimeType;
                string fileName = string.Empty;
                switch (tipoRelatorio)
                {
                    case "PDF":
                        mimeType = "application/pdf";
                        fileName = "download.pdf";
                        break;
                    case "Excel":
                        mimeType = "application/vnd.ms-excel";
                        fileName = "download.xls";
                        break;
                    case "Word":
                        mimeType = "application/msword";
                        fileName = "download.doc";
                        break;
                }

                string encoding;
                string fileNameExtension;

                Warning[] warnings;
                string[] streams;
                byte[] bytes;

                bytes = relatorio.Render(
                tipoRelatorio,
                null,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);

                return File(bytes, mimeType, fileName);
        }

        private IQueryable<RelatorioRestauranteCatraca> FiltrarAcessos(int tipoAlimentacao = 0, string empresa = null, string po = null, DateTime? dataInicial = null, DateTime? dataFinal = null)
        {
            IRepositorioRelatorioRestauranteCatraca repRelatorio = Fabrica.Instancia.ObterRepositorio<IRepositorioRelatorioRestauranteCatraca>(UnidadeTrabalho);
            var lista = repRelatorio.ObterTodos();

            lista = lista.Where(l => l.DataLeitora >= dataInicial && l.DataLeitora <= dataFinal);

            if (!string.IsNullOrEmpty(po))
                lista = lista.Where(l => l.NumeroPO.Contains(po));

            if (tipoAlimentacao > 0)
                lista = lista.Where(l => l.CodigoTipoAlimentacao == tipoAlimentacao);

            if (!string.IsNullOrEmpty(empresa))
                lista = lista.Where(l => l.CodigoEmpresa == empresa);
            
            return lista;
        }
    }
}
