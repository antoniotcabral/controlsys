﻿using Controlsys.Dominio.Empresas;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorios.Empresas;
using Ext.Net;
using Ext.Net.MVC;
using Globalsys;
using Globalsys.Exceptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class ArquivoEmpresaController : Controller, IControllerBase
    {

        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        public ArquivoEmpresaController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        public ActionResult Index()
        {
            return View();
        }

        public FileResult ExportarSelecao(string[] codigosEmpresas = null, int[] codigosTipoDocumento = null)
        {
            if (codigosEmpresas == null || codigosEmpresas.Length == 0)
                throw new CoreException("Selecione ao menos uma empresa.");

            return GerarZip(FiltrarEmpresas(null, null, null, 0, codigosEmpresas), codigosTipoDocumento);
        }

        public FileResult ExportarPesquisa(string nomeEmpresa = null, string cnpjEmpresa = null, int[] codigosTipoDocumento = null, int codigoSindicato = 0)
        {
            return GerarZip(FiltrarEmpresas(nomeEmpresa, cnpjEmpresa, codigosTipoDocumento, codigoSindicato), codigosTipoDocumento);
        }

        public ActionResult Pesquisar(StoreRequestParameters parameters, string nomeEmpresa = null, string cnpjEmpresa = null, int[] codigosTipoDocumento = null, int codigoSindicato = 0)
        {
            IQueryable<Empresa> resultadoFiltro = FiltrarEmpresas(nomeEmpresa, cnpjEmpresa, codigosTipoDocumento, codigoSindicato);

            int total = resultadoFiltro.Count();
            ArquivoEmpresaModelView[] retorno = this.MontaObjetos(resultadoFiltro, parameters);

            return new StoreResult(retorno, total);
        }

        public IQueryable<Empresa> FiltrarEmpresas(string nomeEmpresa = null, string cnpjEmpresa = null, int[] codigosTipoDocumento = null, int codigoSindicato = 0, string[] codigosEmpresas = null)
        {
            IQueryable<Empresa> empresas = Fabrica.Instancia.ObterRepositorio<IRepositorioEmpresa>(UnidadeTrabalho).ObterTodos().Where(p => p.Documentos.Any(d => d.Ativo && d.Documento.Midias.Any(dm => dm.Ativo)));

            if (codigosEmpresas != null && codigosEmpresas.Length > 0)
                empresas = empresas.Where(p => codigosEmpresas.Contains(p.Codigo));

            if (!string.IsNullOrEmpty(nomeEmpresa))
                empresas = empresas.Where(p => p.Nome.ToUpper().Contains(nomeEmpresa.ToUpper()));

            if (!string.IsNullOrEmpty(cnpjEmpresa))
            {
                string cnpjFormatado = cnpjEmpresa.Replace("-", string.Empty).Replace(".", string.Empty).Replace("/", string.Empty);
                empresas = empresas.Where(p => p.CNPJ.Contains(cnpjFormatado));
            }

            if (codigosTipoDocumento != null)
                empresas = empresas.Where(p => p.Documentos.Any(d => codigosTipoDocumento.Contains(d.Documento.TipoDocumento.Codigo) && d.Documento.Midias.Any(dm=> dm.Ativo)));

            if (codigoSindicato != 0)
                empresas = empresas.Where(c => c.Sindicatos.Any(s => s.Sindicato.Codigo == codigoSindicato));


            return empresas;
        }

        public ArquivoEmpresaModelView[] MontaObjetos(IQueryable<Empresa> empresas, StoreRequestParameters parameters = null, bool ignoreParameters = false)
        {

            if (!ignoreParameters)
            {
                if (parameters == null)
                    parameters = new StoreRequestParameters();

                empresas = empresas.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 30);
            }

            ArquivoEmpresaModelView[] empresasModeView = empresas.ToList().Select(e => new ArquivoEmpresaModelView
            {
                CodigoEmpresa = e.Codigo,
                Apelido = e.Apelido,
                CNPJ = e.CNPJ,
                Nome = e.Nome
            }).OrderBy(p => p.Nome).ToArray();

            return empresasModeView;
        }

        private FileContentResult GerarZip(IQueryable<Empresa> empresas, int[] codigosTipoDocumento = null)
        {
            using (var streamArquivoFinalCompactado = new MemoryStream())
            {
                using (var zipArchive = new ZipArchive(streamArquivoFinalCompactado, ZipArchiveMode.Update, false))
                {
                    foreach (Empresa empresa in empresas)
                    {
                        Regex rgx = new Regex(@"[^\p{L}0-9 -]");

                        string nomePasta = empresa.CNPJ + "-" + rgx.Replace(empresa.Nome, string.Empty) + @"\";

                        IEnumerable<string> urls = empresa.Documentos.Where(d => codigosTipoDocumento == null || codigosTipoDocumento.Contains(d.Documento.TipoDocumento.Codigo)).SelectMany(d => d.Documento.Midias).Select(dm => dm.Midia.Url);

                        if (urls.Count() > 0)
                        {
                            var zipPasta = zipArchive.CreateEntry(nomePasta, CompressionLevel.Optimal);

                            string arquivosNaoEncontrados = string.Empty;

                            foreach (string url in urls)
                            {
                                if (System.IO.File.Exists(url))
                                {
                                    string nomeArquivo = System.IO.Path.Combine(new string[] { nomePasta, System.IO.Path.GetFileName(url) });

                                    var zipArquivo = zipArchive.CreateEntryFromFile(url, nomeArquivo, CompressionLevel.Optimal);
                                }
                                else
                                {
                                    arquivosNaoEncontrados = url + Environment.NewLine;
                                }
                            }
                        }
                    }

                }

                return new FileContentResult(streamArquivoFinalCompactado.ToArray(), "application/zip") { FileDownloadName = "Download.zip" };
            }
        }

    }
}
