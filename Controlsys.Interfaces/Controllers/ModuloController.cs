﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Controlsys.Dominio.Seguranca;
using Controlsys.Infra;
using Controlsys.Repositorios.Seguranca;
using Ext.Net;
using Ext.Net.MVC;
using Globalsys;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar moduloes.
    /// </summary>
    [ExtendController]
    public class ModuloController : Controller, IControllerBase
    {
        /// <summary>
        /// Gets/Sets valor para UnidadeTrabalho.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.ModuloController.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public ModuloController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        /// <summary>
        /// GET: /Modulo/.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index(int codigo)
        {
            IRepositorioPagina repPagina = Fabrica.Instancia.ObterRepositorio<IRepositorioPagina>(UnidadeTrabalho);
            IRepositorioPermissao repPermissao = Fabrica.Instancia.ObterRepositorio<IRepositorioPermissao>(UnidadeTrabalho);

            Pagina pagina = repPagina.ObterPorId(codigo);

            List<Pagina> paginasLiberadas = repPermissao.ObterAcoesUsuario(HttpContext.User.Identity.Name, pagina.Codigo)                                                        
                                                        .Select(a => a.Pagina)
                                                        .Distinct()
                                                        .ToList();

            Node node = null;

            if (paginasLiberadas.Any())
                node = ObterPaginas(pagina, paginasLiberadas);

            X.GetCmp<Panel>("imgCentro").BodyCls = pagina.Icone + "128";
            X.GetCmp<Label>("lblModulo").Text = node.Text;

            return View(node);
        }

        /// <summary>
        /// Obter paginas.
        /// </summary>
        ///
        /// <param name="paginaRaiz">
        /// O(a) pagina raiz.
        /// </param>
        /// <param name="paginasLiberadas">
        /// O(a) paginas liberadas.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Node.
        /// </returns>
        public Node ObterPaginas(Pagina paginaRaiz, List<Pagina> paginasLiberadas)
        {
            Ext.Net.Node root = new Ext.Net.Node
            {
                Text = paginaRaiz.Nome
            };

            root.Expanded = true;

            foreach (Pagina paginaPai in paginaRaiz.PaginasFilhas.Where(p => p.PaginasFilhas.Any(pf => paginasLiberadas.Any(pl => pl.Codigo == pf.Codigo))))
            {
                Ext.Net.Node pai = new Ext.Net.Node()
                {
                    NodeID = paginaPai.Codigo.ToString(),
                    Text = paginaPai.Nome,
                    Icon = (Ext.Net.Icon)Enum.Parse(typeof(Ext.Net.Icon), paginaPai.Icone)
                };

                root.Children.Add(pai);

                foreach (Pagina paginaFilha in paginaPai.PaginasFilhas.Where(p => paginasLiberadas.Any(pl => pl.Codigo == p.Codigo)))
                {
                    Ext.Net.Node filha = new Ext.Net.Node
                    {
                        NodeID = paginaFilha.Codigo.ToString(),
                        Text = paginaFilha.Nome,
                        Href = paginaFilha.Url,
                        Icon = (Ext.Net.Icon)Enum.Parse(typeof(Ext.Net.Icon), paginaFilha.Icone),
                        Leaf = true
                    };

                    pai.Children.Add(filha);
                }
            }
            return root;
        }
    }
}
