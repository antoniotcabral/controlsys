﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Controlsys.Dominio.Audit;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorios.Audit;
using Ext.Net;
using Ext.Net.MVC;
using Globalsys;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar auditar sistemas.
    /// </summary>
    [ExtendController]
    public class AuditarSistemaController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /AuditarSistema/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.AuditarSistemaController.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public AuditarSistemaController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        /// <summary>
        /// Retorna a página de auditarSistema.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Retorna todos os objetos do tipo AuditarSistema.
        /// </summary>
        ///
        /// <param name="parameters">
        /// Options for controlling the operation.
        /// </param>
        /// <param name="codUsuario">
        /// O(a) cod usuario.
        /// </param>
        /// <param name="codPagina">
        /// O(a) cod pagina.
        /// </param>
        /// <param name="codAcao">
        /// O(a) cod acao.
        /// </param>
        ///
        /// <returns>
        /// Detalhes do tipo de objeto ligado ao controller no formato JSON.
        /// </returns>
        public ActionResult ObterTodos(StoreRequestParameters parameters, string codUsuario, int? codPagina, int? codAcao, DateTime? dtInicioAuditoriaSistema, DateTime? dtFimAuditoriaSistema)
        {
            IQueryable<Auditoria> regs = FiltrarAuditorias(codUsuario, codPagina, codAcao, dtInicioAuditoriaSistema, dtFimAuditoriaSistema);

            int total = regs.Count();
            List<AuditarSistemaModelView> data = montaObj(regs, parameters);

            return new StoreResult(data, total);
        }

        /// <summary>
        /// Filtrar auditorias.
        /// </summary>
        ///
        /// <param name="codUsuario">
        /// O(a) cod usuario.
        /// </param>
        /// <param name="codPagina">
        /// O(a) cod pagina.
        /// </param>
        /// <param name="codAcao">
        /// O(a) cod acao.
        /// </param>
        /// <param name="dtInicio">
        /// O(a) Data Inicio.
        /// </param>
        /// /// <param name="dtFim">
        /// O(a) cod acao.
        /// </param>
        ///
        /// <returns>
        /// Um(a) IQueryable&lt;Auditoria&gt;
        /// </returns>
        private IQueryable<Auditoria> FiltrarAuditorias(string codUsuario, int? codPagina, int? codAcao, DateTime? dtInicio, DateTime? dtFim)
        {
            IRepositorioAuditoria repAudit = Fabrica.Instancia.ObterRepositorio<IRepositorioAuditoria>(UnidadeTrabalho);

            IQueryable<Auditoria> audits = repAudit.ObterTodos();

            if (!string.IsNullOrEmpty(codUsuario))
                audits = audits.Where(a => a.Usuario.Codigo == codUsuario);

            if (codPagina.HasValue)
                audits = audits.Where(a => a.Acao.Pagina.Codigo == codPagina);

            if (codAcao.HasValue)
                audits = audits.Where(a => a.Acao.Codigo == codAcao);

            if (dtInicio.HasValue && dtFim.HasValue)
                audits = audits.Where(a => a.DataRegistro >= dtInicio && a.DataRegistro <= dtFim);

            return audits;
        }

        /// <summary>
        /// Cria um objeto contendo informações relacionadas ao conteudo gerenciado por
        /// AuditarSistemaController.
        /// </summary>
        ///
        /// <param name="auditorias">
        /// O(a) auditorias.
        /// </param>
        /// <param name="parameters">
        /// Options for controlling the operation.
        /// </param>
        /// <param name="ignoreParameters">
        /// true to ignore parameters.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;AuditarSistemaModelView&gt;
        /// </returns>
        private List<AuditarSistemaModelView> montaObj(IQueryable<Auditoria> auditorias, StoreRequestParameters parameters = null, bool ignoreParameters = false)
        {
            if (!ignoreParameters)
            {
                if (parameters == null)
                    parameters = new StoreRequestParameters();
                auditorias = auditorias.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 25);
            }

            return auditorias.Select(a => new AuditarSistemaModelView()
            {
                Codigo = a.Codigo,
                Usuario = a.Usuario.PessoaFisica.Nome,
                CPFPassaporte = a.Usuario.PessoaFisica.CPFFormatado() ?? a.Usuario.PessoaFisica.Passaporte,
                Acao = a.Acao != null ? a.Acao.Nome : null,
                Url = a.Acao != null ? a.Acao.Url : null,
                Descricao = a.Descricao,
                Interface = a.Acao != null ? a.Acao.Pagina.Nome : null,
                DataHora = a.DataRegistro,
                CodigoUsuario = a.Usuario.Codigo
            }).ToList();
        }


        private IQueryable<Auditoria> FiltrarAuditorias(IQueryable<Auditoria> audits, string codUsuario, int? codPagina, int? codAcao, DateTime? dtInicio, DateTime? dtFim)
        {
            if (!string.IsNullOrEmpty(codUsuario))
                audits = audits.Where(a => a.Usuario.Codigo == codUsuario);

            if (codPagina.HasValue)
                audits = audits.Where(a => a.Acao.Pagina.Codigo == codPagina);

            if (codAcao.HasValue)
                audits = audits.Where(a => a.Acao.Codigo == codAcao);

            if (dtInicio.HasValue && dtFim.HasValue)
                audits = audits.Where(a => a.DataRegistro >= dtInicio && a.DataRegistro <= dtFim);

            audits = audits.OrderByDescending(a => a.DataRegistro);

            return audits;
        }   

        public ActionResult Pesquisar(string codUsuario, int? codPagina, int? codAcao, DateTime? dtInicio, DateTime? dtFim)
        {
            IRepositorioAuditoria repAudit = Fabrica.Instancia.ObterRepositorio<IRepositorioAuditoria>(UnidadeTrabalho);

            IQueryable<Auditoria> audits = repAudit.ObterTodos();

            audits = FiltrarAuditorias(audits, codUsuario, codPagina, codAcao, dtInicio, dtFim);
            int total = audits.Count();
            var data = montaObj(audits);

            return Json(new
            {
                Total = total,
                Data = data
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Paginacao(StoreRequestParameters parameters, string codUsuario, int? codPagina, int? codAcao, DateTime? dtInicio, DateTime? dtFim)
        {
            IRepositorioAuditoria repAudit = Fabrica.Instancia.ObterRepositorio<IRepositorioAuditoria>(UnidadeTrabalho);
            IQueryable<Auditoria> audits = repAudit.ObterTodos();

            audits = FiltrarAuditorias(audits, codUsuario, codPagina, codAcao, dtInicio, dtFim);
            int total = audits.Count();
            List<AuditarSistemaModelView> data = montaObj(audits, parameters);

            return new StoreResult()
            {
                Total = total,
                Data = data
            };
        }

    }
}
