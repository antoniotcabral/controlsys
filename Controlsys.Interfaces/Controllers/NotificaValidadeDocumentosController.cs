﻿
using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorios.Empresas;
using Controlsys.Repositorios.Parametros;
using Controlsys.Repositorios.Pessoas;
using Globalsys;
using Globalsys.Exceptions;
using Microsoft.Reporting.WebForms;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web.Mvc;
using System.Xml;
namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class NotificaValidadeDocumentosController : Controller, IControllerBase
    {
        /// <summary>
        /// Gets/Sets valor para UnidadeTrabalho.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.NotificaValidadeDocumentosController.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public NotificaValidadeDocumentosController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        //
        // GET: /NotificaValidadeDocumentos/
        [AllowAnonymous]
        [HttpPost]
        public string Index(bool isTest = false)
        {
            if (Request.HttpMethod != "POST")
            {
                Response.StatusCode = 403;
                Response.StatusDescription = "Requisição inválida.";
                throw new InvalidOperationException("Requisição inválida.");
            }

            DateTime dtInicio = DateTime.Now;
            //TODO: Implementar validação para permitir apenas que requisições feitas pelo banco de dados sejam autorizadas
            #region ValidarIP
            //System.Web.HttpContext context = System.Web.HttpContext.Current;
            //string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            //if (!string.IsNullOrEmpty(ipAddress))
            //{
            //    string[] addresses = ipAddress.Split(',');
            //    if (addresses.Length != 0)
            //    {
            //        ipAddress = addresses[0];
            //    }
            //}

            //ipAddress = context.Request.ServerVariables["REMOTE_ADDR"];

            #endregion

            IRepositorioEmpresa repEmpresa = Fabrica.Instancia.ObterRepositorio<IRepositorioEmpresa>(UnidadeTrabalho);
            IRepositorioPapel repPapel = Fabrica.Instancia.ObterRepositorio<IRepositorioPapel>(UnidadeTrabalho);
            IRepositorioParametro repParam = Fabrica.Instancia.ObterRepositorio<IRepositorioParametro>(UnidadeTrabalho);
            IRepositorioPedidoCompra repPedidos = Fabrica.Instancia.ObterRepositorio<IRepositorioPedidoCompra>(UnidadeTrabalho);
            IRepositorioAlocacaoColaborador repAlocCol = Fabrica.Instancia.ObterRepositorio<IRepositorioAlocacaoColaborador>(UnidadeTrabalho);
            IRepositorioColaborador repCol = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);

            Parametro param = repParam.RecuperarParametro(ParametroSistema.NumeroDiasVerificarValidadeDocumentos);
            int numeroDiasVerificarValidadeDocumentos = 0;

            if (param != null)
                numeroDiasVerificarValidadeDocumentos = Convert.ToInt32(param.Valor);

            DateTime diaMin = DateTime.Now.Date;
            DateTime diaMax = diaMin.AddDays(numeroDiasVerificarValidadeDocumentos);

            string assunto = "Notificação de Validade de Documentos";
            string corpo = repParam.RecuperarParametro(ParametroSistema.MensagemEmailNotificaValidadeDocumentos).Valor;
            int cont = 0;

            IQueryable<Empresa> empresas = repEmpresa.ObterTodos().Where(e => e.Ativo /*&& !string.IsNullOrEmpty(e.Email)*/);

            Dictionary<string, string> listaErrosEnviarEmail = new Dictionary<string, string>();

            foreach (Empresa empresa in empresas)
            {

                IQueryable<Colaborador> colaboradoresEmpresa = repCol.ObterTodos().Where(c => c.Empresa.Codigo == empresa.Codigo && c.PapelLogs.OrderByDescending(pl => pl.Codigo)
                                                                         .Select(pl => pl.Status)
                                                                         .FirstOrDefault() != StatusPapelLog.BaixaDefinitiva);

                int count = colaboradoresEmpresa.Count();

                List<Attachment> anexos = new List<Attachment>();
                foreach (Colaborador colaborador in colaboradoresEmpresa)
                {
                    DateTime aso = DateTime.MinValue;
                    if (colaborador.ASOs != null && colaborador.ASOs.Count() != 0)
                        aso = colaborador.ASOs.Max(a => a.DataVencimento);

                    List<TreinamentoPessoa> treinamentos = colaborador.Treinamentos.Where(t => t.DataValidade.Date >= diaMin && t.DataValidade.Date <= diaMax).ToList();
                    List<DocumentoPapel> documentos = colaborador.Documentos.Where(d => d.Documento.DataValidade.HasValue && d.Documento.DataValidade.Value >= diaMin && d.Documento.DataValidade.Value <= diaMax).ToList();
                    if ((aso == DateTime.MinValue || aso.Date < diaMin || aso.Date > diaMax) && (treinamentos == null || treinamentos.Count == 0) && (documentos == null || documentos.Count == 0))
                        continue;

                    NotificaValidadeDocumentosModelView validadeASO = new NotificaValidadeDocumentosModelView
                    {
                        TipoDocumento = "ASO",
                        DataValidade = aso.Date
                    };

                    List<NotificaValidadeDocumentosModelView> validadeTreinamentos = treinamentos.Select(t => new NotificaValidadeDocumentosModelView { TipoDocumento = "Treinamento: " + t.Treinamento.Nome, DataValidade = t.DataValidade.Date }).ToList();

                    List<NotificaValidadeDocumentosModelView> validadeDocumentos = documentos.Select(d => new NotificaValidadeDocumentosModelView { TipoDocumento = d.Documento.TipoDocumento.Sigla, DataValidade = d.Documento.DataValidade.Value.Date }).ToList();

                    List<NotificaValidadeDocumentosModelView> todasValidades = validadeDocumentos.Union(validadeTreinamentos).ToList();

                    if (colaborador.ASOs != null)
                        todasValidades.Add(validadeASO);

                    AlocacaoColaborador alocCol = repAlocCol.ObterAlocacao(colaborador.Codigo);

                    string NomeCompleto = colaborador.PessoaFisica.Nome;
                    string Status = colaborador.ObterLogPapel().Status.ObterDescricaoEnum();
                    string CPF = colaborador.PessoaFisica.CPFFormatado();
                    string Passaporte = colaborador.PessoaFisica.Passaporte;
                    string NomeEmpresa = colaborador.Empresa.Nome;
                    string CNPJEmpresa = colaborador.Empresa.CNPJFormatado();
                    string NomeContratada = colaborador.Empresa.Nome;
                    string SetorCusto = string.Empty;
                    string Gestor = string.Empty;

                    if (alocCol != null && alocCol.SetorCusto != null)
                    {
                        SetorCusto = alocCol.SetorCusto.Nome;
                        Gestor = alocCol.SetorCusto.Descricao;
                    }

                    LocalReport relatorio = new LocalReport();
                    relatorio.ReportPath = Server.MapPath("~/Reports/NotificaValidadeDocumentos.rdlc");
                    relatorio.DataSources.Add(new ReportDataSource("DTONotificaValidadeDocumentos", todasValidades));
                    relatorio.SetParameters(new ReportParameter("NomeCompleto", string.IsNullOrEmpty(NomeCompleto) ? " - " : NomeCompleto));
                    relatorio.SetParameters(new ReportParameter("Status", string.IsNullOrEmpty(Status) ? " - " : Status));
                    relatorio.SetParameters(new ReportParameter("CPF", string.IsNullOrEmpty(CPF) ? " - " : CPF));
                    relatorio.SetParameters(new ReportParameter("Passaporte", string.IsNullOrEmpty(Passaporte) ? " - " : Passaporte));
                    relatorio.SetParameters(new ReportParameter("NomeEmpresa", string.IsNullOrEmpty(NomeEmpresa) ? " - " : NomeEmpresa));
                    relatorio.SetParameters(new ReportParameter("CNPJEmpresa", string.IsNullOrEmpty(CNPJEmpresa) ? " - " : CNPJEmpresa));
                    relatorio.SetParameters(new ReportParameter("NomeContratada", string.IsNullOrEmpty(NomeContratada) ? " - " : NomeContratada));
                    relatorio.SetParameters(new ReportParameter("SetorCusto", string.IsNullOrEmpty(SetorCusto) ? " - " : SetorCusto));
                    relatorio.SetParameters(new ReportParameter("Gestor", string.IsNullOrEmpty(Gestor) ? " - " : Gestor));

                    string mimeType = "application/vnd.ms-excel";
                    string fileName = "download.xls";

                    string encoding;
                    string fileNameExtension;

                    Warning[] warnings;
                    string[] streams;
                    byte[] bytes;

                    bytes = relatorio.Render(
                    "Excel",
                    null,
                    out mimeType,
                    out encoding,
                    out fileNameExtension,
                    out streams,
                    out warnings);

                    anexos.Add(new Attachment(new MemoryStream(bytes), colaborador.PessoaFisica.Nome + ".xls"));

                }

                string CNPJ = empresa.CNPJ;
                string RazaoSocial = empresa.Nome;
                string NomeFantasia = empresa.Apelido;

                IQueryable<PedidoCompra> pedidosEmpresa = repPedidos.ObterTodos().Where(pc => pc.Empresa.Codigo == empresa.Codigo && pc.Prazos.Where(pz => pz.Ativo).Max(pz => pz.DataFim).Date >= diaMin && pc.Prazos.Where(pz => pz.Ativo).Max(pz => pz.DataFim).Date <= diaMax);

                List<NotificaValidadeDocumentosModelView> validadePedidos = pedidosEmpresa.Select(pc => new NotificaValidadeDocumentosModelView { DataValidade = pc.Prazos.Where(pz => pz.Ativo).Max(pz => pz.DataFim).Date, TipoDocumento = "Pedido de Compra:" + pc.Nome }).ToList();
                List<NotificaValidadeDocumentosModelView> validadeDocumentosEmpresa = empresa.Documentos.Where(d => (d.Documento.Status == StatusDocumento.Completo && d.Documento.DataValidade.HasValue) && (d.Documento.DataValidade.HasValue && d.Documento.DataValidade.Value >= diaMin && d.Documento.DataValidade.Value <= diaMax)).Select(d => new NotificaValidadeDocumentosModelView { TipoDocumento = d.Documento.TipoDocumento.Sigla, DataValidade = d.Documento.DataValidade.Value }).ToList();

                validadeDocumentosEmpresa = validadeDocumentosEmpresa.Union(validadePedidos).ToList();

                if (validadeDocumentosEmpresa != null && validadeDocumentosEmpresa.Count > 0)
                {

                    LocalReport relatorio = new LocalReport();
                    relatorio.ReportPath = Server.MapPath("~/Reports/NotificaValidadeDocumentosEmpresa.rdlc");
                    relatorio.DataSources.Add(new ReportDataSource("DTONotificaValidadeDocumentos", validadeDocumentosEmpresa));
                    relatorio.SetParameters(new ReportParameter("CNPJ", CNPJ));
                    relatorio.SetParameters(new ReportParameter("RazaoSocial", RazaoSocial));
                    relatorio.SetParameters(new ReportParameter("NomeFantasia", NomeFantasia));

                    string mimeType = "application/vnd.ms-excel";
                    string fileName = "download.xls";

                    string encoding;
                    string fileNameExtension;

                    Warning[] warnings;
                    string[] streams;
                    byte[] bytes;

                    bytes = relatorio.Render(
                    "Excel",
                    null,
                    out mimeType,
                    out encoding,
                    out fileNameExtension,
                    out streams,
                    out warnings);

                    anexos.Add(new Attachment(new MemoryStream(bytes), empresa.Apelido + ".xls"));
                    cont++;
                }

                if (anexos.Count > 0 && !(isTest))
                {
                    try
                    {
                        Globalsys.Util.Email.Enviar(assunto, null, null, corpo, anexos.ToArray(), empresa.Email);
                    }
                    catch (Exception ex)
                    {
                        listaErrosEnviarEmail.Add(empresa.Nome, ex.Message);
                    }
                }

            }
            DateTime dtfim = DateTime.Now;
            TimeSpan tempoDecorrido = dtfim - dtInicio;
            var json = new
            {
                Success = listaErrosEnviarEmail.Count == 0,
                Data = listaErrosEnviarEmail.Select(e => new { Empresa = e.Key, Erro = e.Value }),
                Message = "Tempo decorrido: " + tempoDecorrido.ToString(@"hh\:mm\:ss")
            };

            return JsonConvert.SerializeObject(json, Newtonsoft.Json.Formatting.Indented);

        }
    }
}
