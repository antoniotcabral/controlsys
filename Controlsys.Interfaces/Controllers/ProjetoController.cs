﻿using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Parametros;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorios.Empresas;
using Ext.Net;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar projetoes.
    /// </summary>
    [ExtendController]
    public class ProjetoController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /Projeto/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.ProjetoController.
        /// </summary>
        ///
        /// <param name="unidade">
        /// O(a) unidade.
        /// </param>
        public ProjetoController(IUnidadeTrabalho unidade) { UnidadeTrabalho = unidade; }

        /// <summary>
        /// Retorna a página de projeto.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Inclui um novo projeto.
        /// </summary>
        ///
        /// <param name="projeto">
        /// O(a) projeto.
        /// </param>
        /// <param name="areas">
        /// O(a) areas.
        /// </param>
        /// <param name="gruposTrabalho">
        /// O(a) grupos trabalho.
        /// </param>
        /// <param name="treinamentos">
        /// O(a) treinamentos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Incluir(Projeto projeto, List<Area> areas, List<GrupoTrabalho> gruposTrabalho, List<TreinamentoProjeto> treinamentos)
        {
            IRepositorioProjeto repProjeto = Fabrica.Instancia.ObterRepositorio<IRepositorioProjeto>(UnidadeTrabalho);
            projeto.DataRegistro = DateTime.Now;
            projeto.Ativar();
            repProjeto.Validar(projeto, EstadoObjeto.Novo);
            repProjeto.Salvar(projeto);

            if (areas != null)
            {
                foreach (var itemArea in areas)
                {
                    AreaProjeto areaprojeto = new AreaProjeto();
                    areaprojeto.Projeto = projeto;
                    areaprojeto.Area = itemArea;
                    areaprojeto.DataRegistro = DateTime.Now;
                    areaprojeto.Ativar();
                    repProjeto.SalvarAreaProjeto(areaprojeto);
                }
            }
            if (gruposTrabalho != null)
            {
                foreach (var itemGrupoTrab in gruposTrabalho)
                {
                    GrupoTrabalhoProjeto grupoTrabProjeto = new GrupoTrabalhoProjeto();
                    grupoTrabProjeto.Projeto = projeto;
                    grupoTrabProjeto.GrupoTrabalho = itemGrupoTrab;
                    grupoTrabProjeto.DataRegistro = DateTime.Now;
                    grupoTrabProjeto.Ativar();
                    repProjeto.SalvarGrupoTrabalhoProjeto(grupoTrabProjeto);
                }
            }
            if (treinamentos != null)
            {
                foreach (var itemTreinamento in treinamentos)
                {
                    TreinamentoProjeto treinamentoProjeto = new TreinamentoProjeto();
                    treinamentoProjeto.Projeto = projeto;
                    treinamentoProjeto.Treinamento = itemTreinamento.Treinamento;
                    treinamentoProjeto.Obrigatorio = itemTreinamento.Obrigatorio;
                    treinamentoProjeto.ImpressoCracha = itemTreinamento.ImpressoCracha;
                    treinamentoProjeto.Observacao = itemTreinamento.Observacao;
                    treinamentoProjeto.DataRegistro = DateTime.Now;
                    treinamentoProjeto.Ativar();
                    repProjeto.SalvarTreinamentoProjeto(treinamentoProjeto);
                }
            }
            return Json(montaObj(projeto));
        }

        /// <summary>
        /// Altera um registro de projeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="projeto">
        /// O(a) projeto.
        /// </param>
        /// <param name="areaDel">
        /// O(a) area delete.
        /// </param>
        /// <param name="areaNew">
        /// O(a) area new.
        /// </param>
        /// <param name="grupoDel">
        /// O(a) grupo delete.
        /// </param>
        /// <param name="grupoNew">
        /// O(a) grupo new.
        /// </param>
        /// <param name="treinamentoDel">
        /// O(a) treinamento delete.
        /// </param>
        /// <param name="treinamentoNew">
        /// O(a) treinamento new.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Alterar(Projeto projeto, int[] areaDel, int[] areaNew, int[] grupoDel, int[] grupoNew, int[] treinamentoDel, TreinamentoProjeto[] treinamentoNew)
        {
            IRepositorioProjeto repProjeto = Fabrica.Instancia.ObterRepositorio<IRepositorioProjeto>(UnidadeTrabalho);
            Projeto projetoS = repProjeto.ObterPorId(projeto.Codigo);
            projetoS.Descricao = projeto.Descricao;
            projetoS.Nome = projeto.Nome;
            projetoS.DataInicio = projeto.DataInicio;

            IRepositorioPedidoCompra repPedidoCompra = Fabrica.Instancia.ObterRepositorio<IRepositorioPedidoCompra>(UnidadeTrabalho);
            PedidoCompra pc = repPedidoCompra.ObterTodos().Where(p => p.Projeto.Codigo == projetoS.Codigo && p.PedidoCompraPai == null).FirstOrDefault();
            if (pc.Prazos.Count > 0)
                if ((pc != null && projeto.DataFim >= pc.Prazos.LastOrDefault().DataFim) || pc == null)
                    projetoS.DataFim = projeto.DataFim;
                else
                    throw new CoreException("Não é possível alterar a Data Fim do Projeto. Existe um Pedido Compra (PO) com prazo maior que a data informada.");
            else
                projetoS.DataFim = projeto.DataFim;

            repProjeto.Validar(projetoS, EstadoObjeto.Alterado);

            if (areaDel != null)
            {
                foreach (var item in projetoS.Areas.Where(t => areaDel.Any(tdd => tdd == t.Area.Codigo)))
                {
                    AreaProjeto areaProj = item;
                    areaProj.Inativar();
                }
            }
            if (areaNew != null)
            {
                foreach (var item in areaNew)
                {
                    AreaProjeto areaprojeto = new AreaProjeto();
                    areaprojeto.Projeto = projeto;
                    areaprojeto.Area = new Area { Codigo = item };
                    areaprojeto.DataRegistro = DateTime.Now;
                    areaprojeto.Ativar();
                    repProjeto.SalvarAreaProjeto(areaprojeto);
                }
            }

            if (grupoDel != null)
            {
                foreach (var item in projetoS.GruposTrabalho.Where(t => grupoDel.Any(tdd => tdd == t.GrupoTrabalho.Codigo)))
                {
                    GrupoTrabalhoProjeto grupoTrabProj = item;
                    grupoTrabProj.Inativar();
                }
            }
            if (grupoNew != null)
            {
                foreach (var item in grupoNew)
                {
                    GrupoTrabalhoProjeto grupoTrabProj = new GrupoTrabalhoProjeto();
                    grupoTrabProj.Projeto = projeto;
                    grupoTrabProj.GrupoTrabalho = new GrupoTrabalho { Codigo = item };
                    grupoTrabProj.DataRegistro = DateTime.Now;
                    grupoTrabProj.Ativar();
                    repProjeto.SalvarGrupoTrabalhoProjeto(grupoTrabProj);
                }
            }

            if (treinamentoDel != null)
            {
                foreach (var item in projetoS.Treinamentos.Where(t => treinamentoDel.Any(tdd => tdd == t.Treinamento.Codigo)))
                {
                    TreinamentoProjeto treinamentoProj = item;
                    treinamentoProj.Inativar();
                }
            }
            if (treinamentoNew != null)
            {
                foreach (var item in treinamentoNew)
                {
                    item.Projeto = projeto;
                    item.DataRegistro = DateTime.Now;
                    item.Ativar();
                    repProjeto.SalvarTreinamentoProjeto(item);
                }
            }
            return Json(montaObj(projetoS));
        }

        /// <summary>
        /// Cria um objeto contendo informações relacionadas ao conteudo gerenciado por
        /// ProjetoController.
        /// </summary>
        ///
        /// <param name="projeto">
        /// O(a) projeto.
        /// </param>
        ///
        /// <returns>
        /// Um(a) object.
        /// </returns>
        private object montaObj(Projeto projeto)
        {
            return new ProjetoModelView()
            {
                Codigo = projeto.Codigo,
                Nome = projeto.Nome,
                Descricao = projeto.Descricao,
                Ativo = projeto.Ativo,
                DataInicio = projeto.DataInicio,
                DataFim = projeto.DataFim,
                Area = projeto.Areas
                                .Select(td => new Area()
                                {
                                    Codigo = td.Area.Codigo,
                                    Nome = td.Area.Nome
                                }).ToList(),
                Treinamento = projeto.Treinamentos
                                    .Select(td => new TreinamentoProjetoModelView()
                                    {
                                        Codigo = td.Treinamento.Codigo,
                                        Nome = td.Treinamento.Nome,
                                        Obrigatorio = td.Obrigatorio,
                                        ImpressoCracha = td.ImpressoCracha,
                                        Observacao = td.Observacao

                                    }).ToList(),
                GrupoTrabalho = projeto.GruposTrabalho
                                    .Select(td => new GrupoTrabalhoModelView()
                                    {
                                        Codigo = td.GrupoTrabalho.Codigo,
                                        Nome = td.GrupoTrabalho.Nome

                                    }).ToList(),
            };
        }

        /// <summary>
        /// Ativar inativar.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult AtivarInativar(int codigo)
        {
            IRepositorioProjeto repProjeto = Fabrica.Instancia.ObterRepositorio<IRepositorioProjeto>(UnidadeTrabalho);
            IRepositorioPedidoCompra repPedidoCompra = Fabrica.Instancia.ObterRepositorio<IRepositorioPedidoCompra>(UnidadeTrabalho);

            Projeto projeto = repProjeto.ObterPorId(codigo);
            if (projeto.Ativo)
            {
                PedidoCompra pc = repPedidoCompra.ObterTodos().Where(p => p.Projeto == projeto && p.Status == StatusPedido.Ativo && p.PedidoCompraPai == null).FirstOrDefault();
                //chamar metodo verificar pedido de compra PAI inativo
                if (pc == null)
                    projeto.Inativar();
                else
                    throw new CoreException("Não é possível inativar, o projeto tem Pedido de Compra (PO) ativo.");
            }
            else
            {
                repProjeto.Validar(projeto, EstadoObjeto.Ativado);
                projeto.Ativar();

            }

            return Json(montaObj(projeto));

        }

        /// <summary>
        /// Selecionar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Selecionar(int codigo)
        {
            IRepositorioProjeto repProjeto = Fabrica.Instancia.ObterRepositorio<IRepositorioProjeto>(UnidadeTrabalho);
            Projeto projeto = repProjeto.ObterPorId(codigo);
            return Json(montaObj(projeto));
        }

        /// <summary>
        /// Retorna todos os objetos do tipo Projeto.
        /// </summary>
        ///
        /// <param name="apenasAtivos">
        /// true to apenas ativos.
        /// </param>
        ///
        /// <returns>
        /// Detalhes do tipo de objeto ligado ao controller no formato JSON.
        /// </returns>
        public ActionResult ObterTodos(bool apenasAtivos = false)
        {
            IRepositorioProjeto repProjeto = Fabrica.Instancia.ObterRepositorio<IRepositorioProjeto>(UnidadeTrabalho);
            IRepositorioPedidoCompra repPedido = Fabrica.Instancia.ObterRepositorio<IRepositorioPedidoCompra>(UnidadeTrabalho);

            IQueryable<Projeto> projetos = repProjeto.ObterTodos();

            if (apenasAtivos)
            {
                projetos = projetos.Where(t => t.Ativo == true);
            }

            return Json(projetos.ToList().Select(s => new
                               {
                                   s.Codigo,
                                   s.Nome,
                                   s.Ativo,
                                   s.DataInicio,
                                   s.DataFim,
                                   PossuiContrato = repPedido.ObterPorProjeto(s.Codigo) != null
                               }));
        }

        /// <summary>
        /// Obter grupos trabalho.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ObterGruposTrabalho(int codigo)
        {
            IRepositorioProjeto repProjeto = Fabrica.Instancia.ObterRepositorio<IRepositorioProjeto>(UnidadeTrabalho);
            Projeto projeto = repProjeto.ObterPorId(codigo);
            List<GrupoTrabalho> gruposTrabalho = projeto.GruposTrabalho.Where(x => x.Ativo == true).Select(x => x.GrupoTrabalho).ToList();
            return Json(gruposTrabalho.Select(x => new
            {
                Codigo = x.Codigo,
                Nome = x.Nome,
                Ativo = x.Ativo
            }));
        }
    }
}
