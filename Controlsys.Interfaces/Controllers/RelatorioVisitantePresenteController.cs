﻿using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Pessoas;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Acesso;
using Controlsys.Repositorios.Acesso;
using Ext.Net;
using Ext.Net.MVC;
using Globalsys;
using Globalsys.Relatorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class RelatorioVisitantePresenteController : Controller, IControllerBase
    {
        //
        // GET: /RelatorioVisitantePresente/

        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        public RelatorioVisitantePresenteController(IUnidadeTrabalho unidade)
        {
            UnidadeTrabalho = unidade;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Pesquisar(StoreRequestParameters parameters, DateTime data, TimeSpan horaInicial, int? grupoLeitora = 0,string[] localLeitora = null, string empresa = null, string cpf = null, string nome = null)
        {
            IQueryable<ControleAcesso> regs = filtrarRegistros(data, horaInicial, grupoLeitora, localLeitora, empresa, cpf, nome);

            int total = regs.Count();
            List<RelatorioVisitantePresenteModelView> datas = montaObj(regs, parameters);

            return new StoreResult(datas, total);
        }


        public ActionResult Exportar(List<string> selectedFields, string tipoRelatorio, DateTime data, TimeSpan horaInicial, int? grupoLeitora = 0, string[] localLeitora = null, string empresa = null, string cpf = null, string nome = null)
        {
            var list = filtrarRegistros(data, horaInicial, grupoLeitora, localLeitora, empresa, cpf, nome);
            var visitantes = montaObj(list, null, true);

            ReportConfig reportConfig = ReportHelper.RenderReport<RelatorioVisitantePresenteModelView>(selectedFields, tipoRelatorio, visitantes);

            string fileName = string.Empty;

            switch (tipoRelatorio)
            {
                case "PDF":
                    fileName = "download.pdf";
                    break;
                case "Excel":
                    fileName = "download.xls";
                    break;
                case "Word":
                    fileName = "download.doc";
                    break;
            }

            return File(reportConfig.Bytes, reportConfig.MimeType, fileName);
        }


        private IQueryable<ControleAcesso> filtrarRegistros(DateTime data, TimeSpan horaInicial, int? grupoLeitora = 0, string[] localLeitora = null, string empresa = null, string cpf = null, string nome = null)
        {
            IRepositorioControleAcesso repControleAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioControleAcesso>(UnidadeTrabalho);
            IRepositorioGrupoLeitora repGrupoLeitora = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoLeitora>(UnidadeTrabalho);

            DateTime dataFinal = data.Add(horaInicial);
            DateTime dataInicial = dataFinal.AddHours(-13);

            IQueryable<ControleAcesso> acessos = repControleAcesso.ObterTodos();

            acessos = acessos.Where(t => t.Papel is Visitante);

            var papeis = acessos.Select(t => t.Papel);

            if (!string.IsNullOrEmpty(cpf))
            {
                var auxCodPapeis = new List<int>();
                cpf = cpf.Replace(".", "").Replace("-", "");
                foreach (var item in papeis)
                {
                    if ((item as Visitante).Solicitante.PessoaFisica.CPF == cpf)
                    {
                        auxCodPapeis.Add(item.Codigo);
                    }
                }

                acessos = acessos.Where(t => auxCodPapeis.Contains(t.Papel.Codigo));
            }

            if (!string.IsNullOrEmpty(nome))
            {
                var auxCodPapeis = new List<int>();
                nome = nome.ToUpper();
                foreach (var item in papeis)
                {
                    if ((item as Visitante).Solicitante.PessoaFisica.Nome.ToUpper().Contains(nome))
                    {
                        auxCodPapeis.Add(item.Codigo);
                    }
                }
                acessos = acessos.Where(t => auxCodPapeis.Contains(t.Papel.Codigo));
            }


            List<int> ultAcessoPapel = acessos.Where(ca => ca.DataAcesso >= dataInicial
                                                       && ca.DataAcesso <= dataFinal
                                                       && ca.Papel != null)
                                                   .GroupBy(ca => ca.Papel.Codigo)
                                                   .Select(gca => gca.Select(a => a.Codigo).Max()).ToList();

            int qtdcolab = ultAcessoPapel.Count;

            List<ControleAcesso> listaControleAcesso = new List<ControleAcesso>();

            for (int i = 0; i < qtdcolab; i += 2000)
            {
                var page = ultAcessoPapel.Skip(i).Take(2000).ToArray();
                listaControleAcesso.AddRange(acessos.Where(c => page.Contains(c.Codigo)));
            }

            IQueryable<ControleAcesso> auxListControleAcesso = listaControleAcesso.AsQueryable<ControleAcesso>();

            auxListControleAcesso = auxListControleAcesso.Where(a => a.StatusAcesso == StatusAcesso.OCO_UR_ACESSO_LIBERADO || a.StatusAcesso == StatusAcesso.OCO_UR_ENTRADA_COMPLETA);


            if (grupoLeitora == 1)// sim
            {
                var cont = repGrupoLeitora.ObterTodos().Where(t => t.Externa);
                auxListControleAcesso = auxListControleAcesso.Where(r => cont.Any(g => g.Leitoras.Select(gl => gl.Codigo).Contains(r.Leitora.Codigo)));
            }

            if (grupoLeitora == 2)
            {
                var cont = repGrupoLeitora.ObterTodos().Where(t => !t.Externa);
                auxListControleAcesso = auxListControleAcesso.Where(r => cont.Any(g => g.Leitoras.Select(gl => gl.Codigo).Contains(r.Leitora.Codigo)));
            }

            if (localLeitora != null)
            {
                auxListControleAcesso = auxListControleAcesso.Where(t => localLeitora.Contains(t.Leitora.Local));
            }

            if (!string.IsNullOrEmpty(empresa))
            {
                var auxCodPapeis = new List<int>();
                empresa = empresa.ToUpper();
                foreach (var item in papeis)
                {
                    if ((item as Visitante).Solicitante.Empresa.Apelido.ToUpper().Contains(empresa))
                    {
                        auxCodPapeis.Add(item.Codigo);
                    }
                }

                auxListControleAcesso = auxListControleAcesso.Where(t => auxCodPapeis.Contains(t.Papel.Codigo));
            }
            return auxListControleAcesso;
        }


        private List<RelatorioVisitantePresenteModelView> montaObj(IQueryable<ControleAcesso> regs, StoreRequestParameters parameters = null, bool ignoreParameters = false)
        {
            if (!ignoreParameters)
            {
                if (parameters == null)
                    parameters = new StoreRequestParameters();
                regs = regs.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 25);
            }

            List<RelatorioVisitantePresenteModelView> listViPresente = new List<RelatorioVisitantePresenteModelView>();

            IRepositorioGrupoLeitora repGrupoLeitora = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoLeitora>(UnidadeTrabalho);
            var grLeitora = repGrupoLeitora.ObterTodos();

            foreach (var item in regs.ToList())
            {
                RelatorioVisitantePresenteModelView relatorio = new RelatorioVisitantePresenteModelView();
                
                relatorio.Codigo = item.Papel.Codigo;
                relatorio.Nome = item.Papel.PessoaFisica.Nome;
                relatorio.CPF = item.Papel.PessoaFisica.CPF != null ? item.Papel.PessoaFisica.CPFFormatado() : null;
                relatorio.Passaporte = item.Papel.PessoaFisica.Passaporte;
                relatorio.NomeSolicitante = (item.Papel as Visitante).Solicitante.PessoaFisica.Nome;
                relatorio.CPFSolicitante = (item.Papel as Visitante).Solicitante.PessoaFisica.CPF != null ? (item.Papel as Visitante).Solicitante.PessoaFisica.CPFFormatado() : null;
                relatorio.PassaporteSolicitante = (item.Papel as Visitante).Solicitante.PessoaFisica.Passaporte;
                relatorio.EmpresaSolicitante = (item.Papel as Visitante).Solicitante.Empresa.Apelido;
                relatorio.Local = item.Leitora.Local;
                relatorio.GrupoLeitoraExterna = grLeitora.Any(g => g.Leitoras.Select(gl => gl.Codigo).Contains(item.Leitora.Codigo)) ? (grLeitora.Any(g => g.Leitoras.Select(gl => gl.Codigo).Contains(item.Leitora.Codigo) && g.Externa) ? "Sim": "Não") : "S/ Grupo Leitora";
                relatorio.DataAcesso = item.DataAcesso;
                relatorio.HoraAcesso = item.DataAcesso;

                listViPresente.Add(relatorio);
            };

            return listViPresente;
        }

    }
}
