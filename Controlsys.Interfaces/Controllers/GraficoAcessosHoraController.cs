﻿using Controlsys.Dominio.Acesso;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Acesso;
using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Reporting.WebForms;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class GraficoAcessosHoraController : Controller, IControllerBase
    {
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        public GraficoAcessosHoraController(IUnidadeTrabalho unidade) { UnidadeTrabalho = unidade; }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Exportar(string tipoRelatorio, DateTime data, int hrInicio, int hrFim)
        {
            data = data.Date;
            var dtInicio = data.AddHours(hrInicio);
            var dtFim = data.AddHours(hrFim).AddHours(1);

            LocalReport relatorio = new LocalReport();
            relatorio.ReportPath = Server.MapPath("~/Reports/GraficoAcessosHora.rdlc");
            var acessos = FiltrarAcessos(dtInicio, dtFim);
            relatorio.DataSources.Add(new ReportDataSource("DTOGraficoAcessosHora", acessos));
            relatorio.SetParameters(new ReportParameter("Data", data.ToString("dd/MM/yyyy")));
            relatorio.SetParameters(new ReportParameter("HoraInicio", hrInicio.ToString("00") + "H00"));
            relatorio.SetParameters(new ReportParameter("HoraFim", hrFim.ToString("00") + "H59"));

            string mimeType;
            string fileName = string.Empty;
            switch (tipoRelatorio)
            {
                case "PDF":
                    mimeType = "application/pdf";
                    fileName = "download.pdf";
                    break;
                case "Excel":
                    mimeType = "application/vnd.ms-excel";
                    fileName = "download.xls";
                    break;
                case "Word":
                    mimeType = "application/msword";
                    fileName = "download.doc";
                    break;
            }

            string encoding;
            string fileNameExtension;

            Warning[] warnings;
            string[] streams;
            byte[] bytes;

            bytes = relatorio.Render(
            tipoRelatorio,
            null,
            out mimeType,
            out encoding,
            out fileNameExtension,
            out streams,
            out warnings);

            return File(bytes, mimeType, fileName);
        }

        private List<GraficoAcessosDiaModelView> FiltrarAcessos(DateTime dtInicio, DateTime dtFim)
        {
            IRepositorioControleAcesso rep = Fabrica.Instancia.ObterRepositorio<IRepositorioControleAcesso>(UnidadeTrabalho);
            var listaCA = rep.ObterListaAcesso(null, null, null, null, null, null, dtInicio, dtFim);

            var dias = listaCA.Select(ca => new DateTime(ca.DataAcesso.Year, ca.DataAcesso.Month, ca.DataAcesso.Day, ca.DataAcesso.Hour, 0, 0)).ToList();
            var listaDiaHora = dias.Distinct().ToList();
            //var listaDiaHora = dias.ToList().Distinct().ToList();

            var lista = new List<GraficoAcessosDiaModelView>();
            foreach (var reg in listaDiaHora)
            {
                DateTime diaHora = reg;
                DateTime dtAux = reg.AddHours(1);
                var listaCAaux = listaCA.Where(ca => ca.DataAcesso >= diaHora && ca.DataAcesso < dtAux);
                var entradas = listaCAaux.Count(ca => ca.Direcao == DirecaoLeitora.Entrada);
                var saidas = listaCAaux.Count(ca => ca.Direcao == DirecaoLeitora.Saida);
                lista.Add(new GraficoAcessosDiaModelView()
                {
                    Data = diaHora,
                    Entradas = entradas,
                    Saidas = saidas
                });
            }

            return lista;
            //return new List<GraficoAcessosDiaModelView>(lista.OrderBy(l=> l.Data));
        }

    }
}
