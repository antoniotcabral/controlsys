﻿using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorios.Parametros;
using Controlsys.Repositorios.Pessoas;
using Ext.Net;
using Ext.Net.MVC;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar grupo trabalhoes.
    /// </summary>
    [ExtendController]
    public class GrupoTrabalhoController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /GrupoTrabalho/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.GrupoTrabalhoController.
        /// </summary>
        ///
        /// <param name="unidade">
        /// O(a) unidade.
        /// </param>
        public GrupoTrabalhoController(IUnidadeTrabalho unidade) { UnidadeTrabalho = unidade; }

        /// <summary>
        /// Retorna a página de grupoTrabalho.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Pesquisar.
        /// </summary>
        ///
        /// <param name="parameters">
        /// O(a) parameters.
        /// </param>
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        /// <param name="cpf">
        /// O(a) cpf.
        /// </param>
        /// <param name="passaporte">
        /// O(a) passaporte.
        /// </param>
        /// <param name="nome">
        /// O(a) nome.
        /// </param>
        /// <param name="matriculasap">
        /// O(a) matriculasap.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>        
        public ActionResult Pesquisar(StoreRequestParameters parameters = null, int codigo = 0, string cpf = null, string passaporte = null, string nome = null, int matriculasap = 0)
        {
            IRepositorioGrupoTrabalho repGrupoTrab = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoTrabalho>(UnidadeTrabalho);
            GrupoTrabalho grupoTrab = repGrupoTrab.ObterPorId(codigo);

            IQueryable<GrupoTrabalhoColab> grupoTrabalhoColab = FiltrarGrupoTrabalhoColab(grupoTrab, cpf, passaporte, nome, matriculasap);
            int total = grupoTrabalhoColab.Count();

            List<GrupoTrabalhoColab2ModelView> data = montaListaObj(grupoTrabalhoColab, parameters);
            return new StoreResult(data, total);
        }

        /// <summary>
        /// Filtrar colaborador.
        /// </summary>
        ///
        /// <param name="grupoTrabalho">
        /// O(a) grupoTrabalho.
        /// </param>
        /// <param name="cpf">
        /// O(a) cpf.
        /// </param>
        /// <param name="passaporte">
        /// O(a) passaporte.
        /// </param>
        /// <param name="nome">
        /// O(a) nome.
        /// </param>
        /// <param name="matriculasap">
        /// O(a) matriculasap.
        /// </param>
        ///
        /// <returns>
        /// Um(a) IQueryable&lt;Colaborador&gt;
        /// </returns>        
        private IQueryable<GrupoTrabalhoColab> FiltrarGrupoTrabalhoColab(GrupoTrabalho grupoTrabalho, string cpf = null, string passaporte = null, string nome = null, int matriculasap = 0)
        {
            IRepositorioGrupoTrabalhoColab rep = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoTrabalhoColab>(UnidadeTrabalho);
            IQueryable<GrupoTrabalhoColab> grupoTrabalhoColab = rep.ObterTodos();

            grupoTrabalhoColab = grupoTrabalhoColab.Where(c => c.GrupoTrabalho == grupoTrabalho && c.Ativo == true);

            if (!string.IsNullOrEmpty(cpf))
                grupoTrabalhoColab = grupoTrabalhoColab.Where(c => c.Papel.PessoaFisica.CPF.Contains(cpf));

            if (!string.IsNullOrEmpty(passaporte))
                grupoTrabalhoColab = grupoTrabalhoColab.Where(c => c.Papel.PessoaFisica.Passaporte.Contains(passaporte));

            if (!string.IsNullOrEmpty(nome))
                grupoTrabalhoColab = grupoTrabalhoColab.Where(c => c.Papel.PessoaFisica.Nome.Contains(nome));            

            if (matriculasap > 0)
                grupoTrabalhoColab = grupoTrabalhoColab.Where(c => c.Papel.EmpregadoSAP.Codigo == matriculasap);

            return grupoTrabalhoColab;
        }

        /// <summary>
        /// Inclui um novo grupoTrabalho.
        /// </summary>
        ///
        /// <param name="grupoTrabalho">
        /// O(a) grupo trabalho.
        /// </param>
        /// <param name="turno">
        /// O(a) turno.
        /// </param>
        /// <param name="dtInicio">
        /// The dt inicio Date/Time.
        /// </param>
        /// <param name="colaboradores">
        /// O(a) colaboradores.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Incluir(GrupoTrabalho grupoTrabalho, int turno, DateTime dtInicio, DateTime? dtFim, List<int> colaboradores)
        {
            IRepositorioGrupoTrabalho repGrupoTrabalho = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoTrabalho>(UnidadeTrabalho);
            IRepositorioTurno repTurno = Fabrica.Instancia.ObterRepositorio<IRepositorioTurno>(UnidadeTrabalho);
            IRepositorioPessoaFisica repPf = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaFisica>(UnidadeTrabalho);
            IRepositorioPapel repPapel = Fabrica.Instancia.ObterRepositorio<IRepositorioPapel>(UnidadeTrabalho);

            grupoTrabalho.DataRegistro = DateTime.Now;
            grupoTrabalho.Ativar();

            repGrupoTrabalho.Validar(grupoTrabalho, EstadoObjeto.Novo);
            repGrupoTrabalho.Salvar(grupoTrabalho);

            GrupoTrabalhoTurno grupoTrabTurno = new GrupoTrabalhoTurno();
            grupoTrabTurno.GrupoTrabalho = grupoTrabalho;
            grupoTrabTurno.Turno = repTurno.ObterPorId(turno);
            grupoTrabTurno.DataInicio = dtInicio;
            grupoTrabTurno.DataFim = dtFim;
            grupoTrabTurno.DataRegistro = DateTime.Now;
            grupoTrabTurno.Ativar();
            repGrupoTrabalho.SalvarGrupoTrabalhoTurno(grupoTrabTurno);
            grupoTrabalho.GrupoTrabalhoTurno.Add(grupoTrabTurno);


            if (colaboradores != null)
            {
                for (int i = 0; i < colaboradores.Count; i++)
                {
                    GrupoTrabalhoColab grupoTrabColab = new GrupoTrabalhoColab();
                    grupoTrabColab.GrupoTrabalho = grupoTrabalho;
                    grupoTrabColab.Papel = repPapel.ObterPorId(colaboradores[i]);
                    grupoTrabColab.DataRegistro = DateTime.Now;
                    grupoTrabColab.Ativar();
                    repGrupoTrabalho.SalvarGrupoTrabalhoColaborador(grupoTrabColab);
                    grupoTrabalho.GrupoTrabalhoColaboradores.Add(grupoTrabColab);
                }
            }
            return Json(montaObj(grupoTrabalho));
        }

        /// <summary>
        /// Altera um registro de grupoTrabalho.
        /// </summary>
        ///
        /// <param name="grupoTrabalho">
        /// O(a) grupo trabalho.
        /// </param>
        /// <param name="turno">
        /// O(a) turno.
        /// </param>
        /// <param name="dtInicio">
        /// The dt inicio Date/Time.
        /// </param>
        /// <param name="gruposTrabColabDel">
        /// O(a) grupos trab colab delete.
        /// </param>
        /// <param name="dtFim">
        /// Data de fim da escala de trabalho
        /// </param>
        /// <param name="colabsNew">
        /// O(a) colabs new.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Alterar(GrupoTrabalho grupoTrabalho, int turno, DateTime dtInicio, DateTime? dtFim, int[] gruposTrabColabDel, int[] colabsNew)
        {
            IRepositorioGrupoTrabalho repGrupoTrabalho = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoTrabalho>(UnidadeTrabalho);
            IRepositorioTurno repTurno = Fabrica.Instancia.ObterRepositorio<IRepositorioTurno>(UnidadeTrabalho);
            IRepositorioPessoaFisica repPf = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaFisica>(UnidadeTrabalho);
            IRepositorioPapel repPapel = Fabrica.Instancia.ObterRepositorio<IRepositorioPapel>(UnidadeTrabalho);

            GrupoTrabalho grupoTrab = repGrupoTrabalho.ObterPorId(grupoTrabalho.Codigo);
            grupoTrab.Nome = grupoTrabalho.Nome;
            grupoTrab.CodigoPHT = grupoTrabalho.CodigoPHT;
            repGrupoTrabalho.Validar(grupoTrab, EstadoObjeto.Alterado);
            repGrupoTrabalho.Atualizar(grupoTrab);

            GrupoTrabalhoTurno grupotrabTurno = grupoTrab.ObterTurnoTrabalho();

            if ((grupotrabTurno.Turno.Codigo != turno) || (grupotrabTurno.DataInicio != dtInicio) || (grupotrabTurno.DataFim != dtFim))
            {
                grupotrabTurno.Inativar();

                GrupoTrabalhoTurno grupoTrabTurno = new GrupoTrabalhoTurno();
                grupoTrabTurno.GrupoTrabalho = grupoTrab;
                grupoTrabTurno.Turno = repTurno.ObterPorId(turno);
                grupoTrabTurno.DataInicio = dtInicio;
                grupoTrabTurno.DataFim = dtFim;
                grupoTrabTurno.DataRegistro = DateTime.Now;
                grupoTrabTurno.Ativar();
                repGrupoTrabalho.SalvarGrupoTrabalhoTurno(grupoTrabTurno);
                grupoTrab.GrupoTrabalhoTurno.Add(grupoTrabTurno);
            }





            if (gruposTrabColabDel != null)
            {
                foreach (var item in grupoTrab.GrupoTrabalhoColaboradores.Where(t => gruposTrabColabDel.Any(tdd => tdd == t.Codigo)))
                {
                    GrupoTrabalhoColab grupoTrabalhoColab = item;
                    grupoTrabalhoColab.Inativar();
                }
            }
            if (colabsNew != null)
            {
                foreach (var item in colabsNew)
                {
                    GrupoTrabalhoColab grupoTrabColab = new GrupoTrabalhoColab();
                    grupoTrabColab.GrupoTrabalho = grupoTrabalho;
                    grupoTrabColab.Papel = repPapel.ObterPorId(item);
                    grupoTrabColab.DataRegistro = DateTime.Now;
                    grupoTrabColab.Ativar();
                    repGrupoTrabalho.SalvarGrupoTrabalhoColaborador(grupoTrabColab);
                    grupoTrabalho.GrupoTrabalhoColaboradores.Add(grupoTrabColab);
                }
            }

            return Json(montaObj(grupoTrab));
        }

        /// <summary>
        /// Ativar inativar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult AtivarInativar(int codigo)
        {
            IRepositorioGrupoTrabalho repGrupoTrab = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoTrabalho>(UnidadeTrabalho);
            IRepositorioGrupoTrabalhoColab repGrupoTrabColab = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoTrabalhoColab>(UnidadeTrabalho);
            GrupoTrabalho grupoTrab = repGrupoTrab.ObterPorId(codigo);

            if (grupoTrab.Ativo)
            {
                grupoTrab.Inativar();

                foreach (GrupoTrabalhoColab grupoTrabColab in grupoTrab.GrupoTrabalhoColaboradores)
                {
                    grupoTrabColab.Inativar();
                    repGrupoTrabColab.Atualizar(grupoTrabColab);
                }
            }
            else
            {
                repGrupoTrab.Validar(grupoTrab, EstadoObjeto.Ativado);
                grupoTrab.Ativar();
            }

            repGrupoTrab.Atualizar(grupoTrab);
            return Json(montaObj(grupoTrab));
        }

        /// <summary>
        /// Selecionar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Selecionar(int codigo)
        {
            IRepositorioGrupoTrabalho repGrupoTrab = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoTrabalho>(UnidadeTrabalho);
            GrupoTrabalho grupoTrab = repGrupoTrab.ObterPorId(codigo);
            return Json(montaObj(grupoTrab));
        }

        /// <summary>
        /// Exclui escala dos grupos de trabalho cadastrados. Não permite a exclusão caso existam colaboradores vinculados ao grupo de trabalho em questão.
        /// </summary>
        /// <param name="codsGrupoTrabalho">Códigos de grupo de trabalho selecionados.</param>
        /// <returns></returns>
        [Transaction]
        public ActionResult ExcluirEscala(int[] codsGrupoTrabalho)
        {
            IRepositorioEscalaTrabalho repEscala = Fabrica.Instancia.ObterRepositorio<IRepositorioEscalaTrabalho>(UnidadeTrabalho);
            IRepositorioGrupoTrabalho repGrupoTrabalho = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoTrabalho>(UnidadeTrabalho);

            foreach (int codGrupoTrabalho in codsGrupoTrabalho)
            {
                GrupoTrabalho grupoTrabalho = repGrupoTrabalho.ObterPorId(codGrupoTrabalho);

                if (grupoTrabalho.GrupoTrabalhoColaboradores.Any())
                    throw new CoreException(string.Format("O grupo de trabalho {0} possui colaboradores vinculados.", grupoTrabalho.Nome));

                if (!grupoTrabalho.ObterTurnoTrabalho().Turno.ObedeceEscala)
                    throw new CoreException(string.Format("O grupo de trabalho {0} não obedece escala.", grupoTrabalho.Nome));
                
                foreach (EscalaTrabalho escala in repEscala.ObterTodos().Where(es => es.GrupoTrabalho.Codigo == codGrupoTrabalho))
                    repEscala.Remover(escala);
            }

            return Json(null);
        }

        /// <summary>
        /// Gera escala dos grupos de trabalho cadastrados. Não permite gerar caso já existam registros de escala no período selecionado para o grupo de trabalho.
        /// </summary>
        /// <param name="codsGrupoTrabalho">Códigos de grupo de trabalho selecionados.</param>
        /// <returns></returns>
        [Transaction]
        public ActionResult GerarEscala(int[] codsGrupoTrabalho)
        {
            IRepositorioGrupoTrabalho repGrupoTrabalho = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoTrabalho>(UnidadeTrabalho);
            IRepositorioEscalaTrabalho repEscala = Fabrica.Instancia.ObterRepositorio<IRepositorioEscalaTrabalho>(UnidadeTrabalho);

            foreach (int codGrupoTrabalho in codsGrupoTrabalho)
            {
                GrupoTrabalho grupoTrabalho = repGrupoTrabalho.ObterPorId(codGrupoTrabalho);

                if (!grupoTrabalho.ObterTurnoTrabalho().Turno.ObedeceEscala)
                    throw new CoreException(string.Format("O grupo de trabalho {0} não obedece escala.", grupoTrabalho.Nome));

                if (repEscala.ObterTodos().Any(e => e.GrupoTrabalho.Codigo == codGrupoTrabalho))
                    throw new CoreException(string.Format("O grupo de trabalho {0} possui registros de escala vinculados ao mesmo."));

                repEscala.GerarEscala(grupoTrabalho);
            }

            return Json(null);
        }

        /// <summary>
        /// Cria um objeto contendo informações relacionadas ao conteudo gerenciado por
        /// GrupoTrabalhoController.
        /// </summary>
        ///
        /// <param name="grupoTrabalho">
        /// O(a) grupo trabalho.
        /// </param>
        ///
        /// <returns>
        /// Um(a) object.
        /// </returns>
        private object montaObj(GrupoTrabalho grupoTrabalho)
        {
            return new
            {
                Codigo = grupoTrabalho.Codigo,
                Nome = grupoTrabalho.Nome,
                Ativo = grupoTrabalho.Ativo,
                DataInicio = obterDescData(grupoTrabalho),
                DataFim = obterDescDataFim(grupoTrabalho),
                grupoTrabalho.CodigoPHT,
                Turno = obterDescTurno(grupoTrabalho),
                TurnoTrabalho = grupoTrabalho.ObterTurnoTrabalho().Turno.Codigo

                /*
                GrupoTrabColab = grupoTrabalho.GrupoTrabalhoColaboradores.Select(c => new
                {
                    Codigo = c.Codigo,
                    CodigoPessoa = c.Papel.PessoaFisica.Codigo,
                    CPF = c.Papel.PessoaFisica.CPF != null ? c.Papel.PessoaFisica.CPFFormatado() : null,
                    Passaporte = c.Papel.PessoaFisica.Passaporte,
                    Nome = c.Papel.PessoaFisica.Nome,
                    Empresa = ((Colaborador)c.Papel).Empresa == null ? "" : ((Colaborador)c.Papel).Empresa.Nome,
                    Status = c.Papel.ObterLogPapel().Status.ObterDescricaoEnum()
                }).ToList()                
                */
            };
        }

        private List<GrupoTrabalhoColab2ModelView> montaListaObj(IQueryable<GrupoTrabalhoColab> regs, StoreRequestParameters parameters = null, bool ignoreParameters = false)
        {
            if (!ignoreParameters)
            {
                if (parameters == null)
                    parameters = new StoreRequestParameters();
                regs = regs.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 25);
            }
            
            return regs.ToList().Select(c => new GrupoTrabalhoColab2ModelView
            {
                Codigo = c.Codigo,
                CodigoPessoa = c.Papel.PessoaFisica.Codigo,
                CPF = c.Papel.PessoaFisica.CPF != null ? c.Papel.PessoaFisica.CPFFormatado() : null,
                Passaporte = c.Papel.PessoaFisica.Passaporte,
                Nome = c.Papel.PessoaFisica.Nome,
                Empresa = ((Colaborador)c.Papel).Empresa == null ? "" : ((Colaborador)c.Papel).Empresa.Nome,
                Status = c.Papel.ObterLogPapel().Status.ObterDescricaoEnum()
            }).ToList();
        }

        /// <summary>
        /// Retorna todos os objetos do tipo GrupoTrabalho.
        /// </summary>
        ///
        /// <param name="apenasAtivos">
        /// true to apenas ativos.
        /// </param>
        ///
        /// <returns>
        /// Detalhes do tipo de objeto ligado ao controller no formato JSON.
        /// </returns>
        public ActionResult ObterTodos(bool apenasAtivos = false)
        {
            IRepositorioGrupoTrabalho repGrupoTrabalho = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoTrabalho>(UnidadeTrabalho);

            IQueryable<GrupoTrabalho> grupoTrab = repGrupoTrabalho.ObterTodos();

            if (apenasAtivos)
            {
                grupoTrab = grupoTrab.Where(t => t.Ativo == true);
            }

            return Json(grupoTrab.Select(a => new
                               {
                                   Codigo = a.Codigo,
                                   Nome = a.Nome,
                                   Ativo = a.Ativo,
                                   DataInicio = obterDescData(a),
                                   Turno = obterDescTurno(a)
                               })
                               .ToList());
        }

        /// <summary>
        /// Obter description turno.
        /// </summary>
        ///
        /// <param name="grupo">
        /// O(a) grupo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) string.
        /// </returns>
        private string obterDescTurno(GrupoTrabalho grupo)
        {
            return grupo.ObterTurnoTrabalho().Turno.Nome;
        }

        /// <summary>
        /// Obter description data.
        /// </summary>
        ///
        /// <param name="grupo">
        /// O(a) grupo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) DateTime.
        /// </returns>
        private DateTime obterDescData(GrupoTrabalho grupo)
        {
            return grupo.ObterTurnoTrabalho().DataInicio;
        }

        private DateTime? obterDescDataFim(GrupoTrabalho grupo)
        {
            return grupo.ObterTurnoTrabalho().DataFim;
        }
    }
}
