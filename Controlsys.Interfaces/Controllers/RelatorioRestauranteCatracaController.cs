﻿using Controlsys.Dominio.Relatorio;
using Controlsys.Infra;
using Controlsys.Repositorio.Parametros;
using Controlsys.Repositorio.Relatorio;
using Ext.Net;
using Ext.Net.MVC;
using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Reporting.WebForms;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class RelatorioRestauranteCatracaController : Controller, IControllerBase
    {
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        public RelatorioRestauranteCatracaController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Exportar(DateTime referencia, string tipoRelatorio, int setorCusto = 0, int restaurante = 0, int tipoAlimentacao = 0, string empresa = null)
        {
            LocalReport relatorio = new LocalReport();
            relatorio.ReportPath = Server.MapPath("~/Reports/RelatorioRestauranteCatraca.rdlc");
            var acessos = filtrarAcessos(referencia, setorCusto, restaurante, tipoAlimentacao, empresa).ToList();
            relatorio.DataSources.Add(new ReportDataSource("DTORelatorioRestauranteCatraca", acessos));

            string mimeType;
            string fileName = string.Empty;
            switch (tipoRelatorio)
            {
                case "PDF":
                    fileName = "download.pdf";
                    break;
                case "EXCELOPENXML":
                    fileName = "download.xlsx";
                    break;
                case "WORDOPENXML":
                    fileName = "download.docx";
                    break;
            }

            string encoding;
            string fileNameExtension;

            Warning[] warnings;
            string[] streams;
            byte[] bytes;

            bytes = relatorio.Render(
            tipoRelatorio,
            null,
            out mimeType,
            out encoding,
            out fileNameExtension,
            out streams,
            out warnings);

            return File(bytes, mimeType, fileName);
        }

        private IQueryable<RelatorioRestauranteCatraca> filtrarAcessos(DateTime referencia, int setorCusto = 0, int restaurante = 0, int tipoAlimentacao = 0, string empresa = null)
        {
            IRepositorioRelatorioRestauranteCatraca repRelatorio = Fabrica.Instancia.ObterRepositorio<IRepositorioRelatorioRestauranteCatraca>(UnidadeTrabalho);
            var lista = repRelatorio.ObterTodos();

            DateTime min = new DateTime(referencia.Year, referencia.Month, 1);
            DateTime max = new DateTime(referencia.Year, referencia.Month + 1, 1).AddDays(-1);
            lista = lista.Where(l => l.DataLeitora >= min && l.DataLeitora <= max);

            if (setorCusto > 0)
                lista = lista.Where(l => l.CodigoSetorCusto == setorCusto);

            if (restaurante > 0)
                lista = lista.Where(l => l.CodigoRestaurante == restaurante);

            if (tipoAlimentacao > 0)
                lista = lista.Where(l => l.CodigoTipoAlimentacao == tipoAlimentacao);

            if (!string.IsNullOrEmpty(empresa))
                lista = lista.Where(l => l.CodigoEmpresa == empresa);

            return lista;
        }



    }
}
