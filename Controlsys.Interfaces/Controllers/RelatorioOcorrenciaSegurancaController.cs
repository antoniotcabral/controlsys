﻿using Controlsys.Dominio.Ocorrencias;
using Controlsys.Dominio.Pessoas;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Ocorrencias;
using Controlsys.Repositorios.Pessoas;
using Ext.Net;
using Ext.Net.MVC;
using Globalsys;
using Globalsys.Relatorios;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Reporting.WebForms;
using Controlsys.Repositorios.Empresas;
using System.Text;
using System.IO;
using System.Net;
using Controlsys.Repositorios.Seguranca;
using Controlsys.Dominio.Seguranca;
using System.Configuration;
using Globalsys.Exceptions;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class RelatorioOcorrenciaSegurancaController : Controller, IControllerBase
    {

        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        public RelatorioOcorrenciaSegurancaController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        public ActionResult Index()
        {
            #region [Filtro de usuário]
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            Usuario usuarioLogado = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name, somenteAtivos: true);
            #endregion

            #region [Filtro Administrador]
            var grupos = usuarioLogado.GruposUsuarios.Select(g => g.Grupo);
            var acoes = grupos.SelectMany(g => g.Acoes);
            var permissaoExportar = (acoes.Where(a => a.Url == "/RelatorioOcorrenciaSeguranca/ExportarRelFoto")).Any();
            #endregion

            if (permissaoExportar)
            {
                ViewBag.permissaoExportar = 1;
            }
            else
            {
                ViewBag.permissaoExportar = 2;
            }

            return View();
        }

        public ActionResult Exportar(string tipoRelatorio, List<string> selectedFields, DateTime? dataInicial = null, DateTime? dataFinal = null, string supervisor = null, int tipoOcorrencia = 0, int local = 0, GraduacaoOcorrencia[] graduacao = null, GravidadeOcorrencia? gravidade = null,
            string agPassivoCPF = null, string agPassivoPassaporte = null, string agPassivoNome = null, string agPassivoEmpresa = null, string agAtivoCPF = null, string agAtivoPassaporte = null, string agAtivoNome = null, string agAtivoEmpresa = null)
        {
            IRepositorioOcorrenciaSeguranca repO = Fabrica.Instancia.ObterRepositorio<IRepositorioOcorrenciaSeguranca>(UnidadeTrabalho);
            IQueryable<OcorrenciaSeguranca> ocorrencias = repO.ObterTodos();

            string fileName = string.Empty;

            switch (tipoRelatorio)
            {
                case "PDF":
                    fileName = "download.pdf";
                    break;
                case "Excel":
                    fileName = "download.xls";
                    break;
                case "Word":
                    fileName = "download.doc";
                    break;
            }

            ocorrencias = FiltrarOcorrencias(dataInicial, dataFinal, supervisor, tipoOcorrencia, local, graduacao, gravidade,
                agPassivoCPF, agPassivoPassaporte, agPassivoNome, agPassivoEmpresa, agAtivoCPF, agAtivoPassaporte, agAtivoNome, agAtivoEmpresa);
            ReportConfig reportConfig = ReportHelper.RenderReport<RelatorioOcorrenciaSegurancaModelView>(selectedFields, tipoRelatorio, ocorrencias.Select(p => montaObj(p)));
            return File(reportConfig.Bytes, reportConfig.MimeType, fileName);
        }


        public ActionResult ExportarRelFoto(string tipoRelatorio, List<string> selectedFields, DateTime? dataInicial = null, DateTime? dataFinal = null, string supervisor = null, int tipoOcorrencia = 0, int local = 0, GraduacaoOcorrencia[] graduacao = null, GravidadeOcorrencia? gravidade = null,
            string agPassivoCPF = null, string agPassivoPassaporte = null, string agPassivoNome = null, string agPassivoEmpresa = null, string agAtivoCPF = null, string agAtivoPassaporte = null, string agAtivoNome = null, string agAtivoEmpresa = null, int codigo = 0)
        {

            IRepositorioOcorrenciaSeguranca repO = Fabrica.Instancia.ObterRepositorio<IRepositorioOcorrenciaSeguranca>(UnidadeTrabalho);
            IQueryable<OcorrenciaSeguranca> ocorrencias = repO.ObterTodos();

            string fileName = string.Empty;
            //List<string> str = new List<string> { "Descricao" };

            tipoRelatorio = "PDF";
            fileName = "download.pdf";

            ocorrencias = FiltrarOcorrencias(dataInicial, dataFinal, supervisor, tipoOcorrencia, local, graduacao, gravidade,
                          agPassivoCPF, agPassivoPassaporte, agPassivoNome, agPassivoEmpresa, agAtivoCPF, agAtivoPassaporte, 
                          agAtivoNome, agAtivoEmpresa, null, null, null, null, null, null, codigo);
            
            List<RelatorioOcorrenciaSegurancaModelView> dados = ocorrencias.Select(p => montaObj(p)).ToList();
            List<MidiaModelView> dadosMidia = ocorrencias.Select(p => montaObjImagem(p)).FirstOrDefault().ToList();
            List<OcorrenciaSegurancaAgenteModelView> dadosSegAgente = ocorrencias.Select(p => montaObjSegAgente(p)).FirstOrDefault().ToList();            

            LocalReport relatorio = new LocalReport();

            relatorio.ReportPath = Server.MapPath("~/Reports/RelatorioFotograficoOcorrenciaSeguranca.rdlc");            
            if (dados.Count > 0)
            {
                relatorio.DataSources.Add(new ReportDataSource("DTORelatorioFotograficoOcorrenciaSeguranca", dados));
                relatorio.DataSources.Add(new ReportDataSource("DTORelatorioFotograficoOcorrenciaSegurancaImagem", dadosMidia));
                relatorio.DataSources.Add(new ReportDataSource("DTORelatorioFotograficoOcorrenciaSegurancaAgente", dadosSegAgente));
                string mimeType;
                string encoding;
                string fileNameExtension;

                Warning[] warnings;
                string[] streams;
                byte[] bytes;

                mimeType = "application/pdf";
                fileName = "download.pdf";

                bytes = relatorio.Render(
                tipoRelatorio,
                null,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);
                return File(bytes, mimeType, fileName);
            }
            else return null;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="dataInicial"></param>
        /// <param name="dataFinal"></param>
        /// <param name="supervisor"></param>
        /// <param name="tipoOcorrencia"></param>
        /// <param name="local"></param>
        /// <param name="graduacao"></param>
        /// <param name="gravidade"></param>
        /// <param name="agPassivoCPF"></param>
        /// <param name="agPassivoPassaporte"></param>
        /// <param name="agPassivoNome"></param>
        /// <param name="agPassivoEmpresa"></param>
        /// <param name="agAtivoCPF"></param>
        /// <param name="agAtivoPassaporte"></param>
        /// <param name="agAtivoNome"></param>
        /// <param name="agAtivoEmpresa"></param>
        /// <returns></returns>
        public ActionResult Pesquisar(StoreRequestParameters parameters = null, DateTime? dataInicial = null, DateTime? dataFinal = null, string supervisor = null, int tipoOcorrencia = 0, int local = 0, GraduacaoOcorrencia[] graduacao = null, GravidadeOcorrencia? gravidade = null,
            string agPassivoCPF = null, string agPassivoPassaporte = null, string agPassivoNome = null, string agPassivoEmpresa = null, string agPassivoEmpresaCnpj = null, string agPassivoContratada = null, string agPassivoContratadaCnpj = null,
            string agAtivoCPF = null, string agAtivoPassaporte = null, string agAtivoNome = null, string agAtivoEmpresa = null, string agAtivoEmpresaCnpj = null, string agAtivoContratada = null, string agAtivoContratadaCnpj = null)
        {
            IQueryable<OcorrenciaSeguranca> ocorren = FiltrarOcorrencias(dataInicial, dataFinal, supervisor, tipoOcorrencia, local, graduacao, gravidade,
                agPassivoCPF, agPassivoPassaporte, agPassivoNome, agPassivoEmpresa, agPassivoEmpresaCnpj, agPassivoContratada, agPassivoContratadaCnpj,
                agAtivoCPF, agAtivoPassaporte, agAtivoNome, agAtivoEmpresa, agAtivoEmpresaCnpj, agAtivoContratada, agAtivoContratadaCnpj);

            int total = ocorren.Count();
            List<RelatorioOcorrenciaSegurancaModelView> data = montaObjs(ocorren, parameters);

            return new StoreResult(data, total);
        }

        private IQueryable<OcorrenciaSeguranca> FiltrarOcorrencias(DateTime? dataInicial = null, DateTime? dataFinal = null, string supervisor = null, int tipoOcorrencia = 0, int local = 0, GraduacaoOcorrencia[] graduacao = null, GravidadeOcorrencia? gravidade = null,
            string agPassivoCPF = null, string agPassivoPassaporte = null, string agPassivoNome = null, string agPassivoEmpresa = null, string agPassivoEmpresaCnpj = null, string agPassivoContratada = null, string agPassivoContratadaCnpj = null,
            string agAtivoCPF = null, string agAtivoPassaporte = null, string agAtivoNome = null, string agAtivoEmpresa = null, string agAtivoEmpresaCnpj = null, string agAtivoContratada = null, string agAtivoContratadaCnpj = null, int codigo = 0)
        {
            IRepositorioOcorrenciaSeguranca rep = Fabrica.Instancia.ObterRepositorio<IRepositorioOcorrenciaSeguranca>(UnidadeTrabalho);
            IRepositorioColaborador repColab = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);
            IRepositorioPrestadorServico repPrest = Fabrica.Instancia.ObterRepositorio<IRepositorioPrestadorServico>(UnidadeTrabalho);
            IRepositorioVisitante repVisit = Fabrica.Instancia.ObterRepositorio<IRepositorioVisitante>(UnidadeTrabalho);
            IRepositorioAlocacaoColaborador repAlocColab = Fabrica.Instancia.ObterRepositorio<IRepositorioAlocacaoColaborador>(UnidadeTrabalho);

            IQueryable<OcorrenciaSeguranca> ocorrencia = rep.ObterTodos();

            if (codigo > 0)
                ocorrencia = ocorrencia.Where(o => o.Codigo == codigo);

            if (dataInicial != null)
                ocorrencia = ocorrencia.Where(o => o.DataOcorrencia >= dataInicial);

            if (dataFinal != null)
                ocorrencia = ocorrencia.Where(o => o.DataOcorrencia <= dataFinal);

            if (!string.IsNullOrEmpty(supervisor))
            {
                ocorrencia = ocorrencia.Where(o => o.Supervisor.Login.Contains(supervisor) ||
                    o.Supervisor.PessoaFisica.Nome.Contains(supervisor));
            }

            if (tipoOcorrencia > 0)
                ocorrencia = ocorrencia.Where(o => o.TipoOcorrencia.Codigo == tipoOcorrencia);

            if (local > 0)
                ocorrencia = ocorrencia.Where(o => o.Local.Codigo == local);

            if (tipoOcorrencia > 0)
                ocorrencia = ocorrencia.Where(o => o.TipoOcorrencia.Codigo == tipoOcorrencia);

            if (graduacao != null && graduacao.Length > 0)
            {
                foreach (GraduacaoOcorrencia item in graduacao)
                {
                    switch (item)
                    {
                        case GraduacaoOcorrencia.Tentativa:
                            ocorrencia = ocorrencia.Where(o => o.Tentativa == true);
                            break;
                        case GraduacaoOcorrencia.Efetiva:
                            ocorrencia = ocorrencia.Where(o => o.Efetiva == true);
                            break;
                        case GraduacaoOcorrencia.Vulnerabilidade:
                            ocorrencia = ocorrencia.Where(o => o.Vulnerabilidade == true);
                            break;
                        default:
                            break;
                    }
                }
            }

            if (gravidade != null)
                ocorrencia = ocorrencia.Where(o => o.Gravidade == gravidade);

            agPassivoEmpresaCnpj = agPassivoEmpresaCnpj != null ? agPassivoEmpresaCnpj.Replace(".", "").Replace("/", "").Replace("-", "") : null;
            agPassivoContratadaCnpj = agPassivoContratadaCnpj != null ? agPassivoContratadaCnpj.Replace(".", "").Replace("/", "").Replace("-", "") : null;
            agAtivoEmpresaCnpj = agAtivoEmpresaCnpj != null ? agAtivoEmpresaCnpj.Replace(".", "").Replace("/", "").Replace("-", "") : null;
            agAtivoContratadaCnpj = agAtivoContratadaCnpj != null ? agAtivoContratadaCnpj.Replace(".", "").Replace("/", "").Replace("-", "") : null;

            #region [AGENTE PASSIVO]
            if (!string.IsNullOrWhiteSpace(agPassivoCPF))
                ocorrencia = ocorrencia.Where(o => o.AgentesOcorrencia.Any(ag =>
                    ag.TipoAgente == TipoAgenteOcorrencia.Passivo &&
                    ag.PessoaFisica.CPF.Equals(agPassivoCPF.Replace(".", "").Replace("-", ""))));

            if (!string.IsNullOrWhiteSpace(agPassivoPassaporte))
                ocorrencia = ocorrencia.Where(o => o.AgentesOcorrencia.Any(ag =>
                    ag.TipoAgente == TipoAgenteOcorrencia.Passivo &&
                    ag.PessoaFisica.Passaporte.Equals(agPassivoPassaporte.Replace(".", "").Replace("-", ""))));

            if (!string.IsNullOrWhiteSpace(agPassivoNome))
                ocorrencia = ocorrencia.Where(o => o.AgentesOcorrencia.Any(ag =>
                    ag.TipoAgente == TipoAgenteOcorrencia.Passivo &&
                    ag.PessoaFisica.Nome.Contains(agPassivoNome)));

            if ((!string.IsNullOrWhiteSpace(agPassivoEmpresa) || !string.IsNullOrWhiteSpace(agPassivoEmpresaCnpj) || !string.IsNullOrEmpty(agPassivoContratada) || !string.IsNullOrEmpty(agPassivoContratadaCnpj)) &&
                (string.IsNullOrEmpty(agAtivoEmpresa) && string.IsNullOrEmpty(agAtivoEmpresaCnpj) && string.IsNullOrEmpty(agAtivoContratada) && string.IsNullOrEmpty(agAtivoContratadaCnpj)))
            {
                if (!string.IsNullOrWhiteSpace(agPassivoEmpresa) || !string.IsNullOrWhiteSpace(agPassivoEmpresaCnpj))
                {
                    ocorrencia = ocorrencia.Where(o => o.AgentesOcorrencia.Any(ag =>
                        ag.Ativo &&
                        ag.TipoAgente == TipoAgenteOcorrencia.Passivo && (
                            (repColab.ObterTodos()
                                .Where(c => c.PessoaFisica.Codigo.Equals(ag.PessoaFisica.Codigo) && (c.PapelLogs.Any(pl =>
                                    (pl.DataInicio <= o.DataOcorrencia) &&
                                    (pl.DataFim == null || pl.DataFim > o.DataOcorrencia)
                                    ))).Any(c => (!string.IsNullOrEmpty(agPassivoEmpresa) && c.Empresa.Nome.Contains(agPassivoEmpresa)) ||
                                                 (!string.IsNullOrEmpty(agPassivoEmpresaCnpj) && c.Empresa.CNPJ == agPassivoEmpresaCnpj)) ||

                            (repPrest.ObterTodos()
                                .Where(p => p.PessoaFisica.Codigo.Equals(ag.PessoaFisica.Codigo) && (p.PapelLogs.Any(pl =>
                                    (pl.DataInicio <= o.DataOcorrencia) &&
                                    (pl.DataFim == null || pl.DataFim > o.DataOcorrencia)
                                    ))).Any(p => (!string.IsNullOrEmpty(agPassivoEmpresa) && p.Empresa.Nome.Contains(agPassivoEmpresa)) ||
                                                 (!string.IsNullOrEmpty(agPassivoEmpresaCnpj) && p.Empresa.CNPJ == agPassivoEmpresaCnpj)))) ||

                            (repVisit.ObterTodos()
                                .Where(v => v.PessoaFisica.Codigo.Equals(ag.PessoaFisica.Codigo) && (v.PapelLogs.Any(pl =>
                                    (pl.DataInicio <= o.DataOcorrencia) &&
                                    (pl.DataFim == null || pl.DataFim > o.DataOcorrencia)
                                    ))).Any(v => !string.IsNullOrEmpty(agPassivoEmpresa) && v.Empresa.Contains(agPassivoEmpresa)))
                            )
                        )
                    );
                }

                if (!string.IsNullOrEmpty(agPassivoContratada) || !string.IsNullOrEmpty(agPassivoContratadaCnpj))
                {
                    var osa = ocorrencia.Where(o => o.AgentesOcorrencia.Any(ag =>
                        ag.Ativo &&
                        ag.TipoAgente == TipoAgenteOcorrencia.Passivo))
                        .SelectMany(aux => aux.AgentesOcorrencia);

                    List<string> pessoasFisica = osa.Select(ocorrenciaSegurancaAgente => ocorrenciaSegurancaAgente.PessoaFisica.Codigo).ToList();
                    var codColabs = repColab.ObterTodos().Where(c => pessoasFisica.Contains(c.PessoaFisica.Codigo)).Select(c => c.Codigo);

                    List<int> codsAlocacao = new List<int>();
                    foreach (var cod in codColabs)
                    {
                        var alocao = repAlocColab.ObterAlocacao(cod);
                        if (alocao == null) continue;
                        var empresa = alocao.ObterContratada().Empresa;
                        if (empresa.Nome.Contains(agPassivoContratada) || empresa.Apelido.Contains(agPassivoContratada) || empresa.CNPJ == agPassivoContratadaCnpj)
                            codsAlocacao.Add(alocao.Codigo);
                    }

                    ocorrencia = ocorrencia.Where(o => o.AgentesOcorrencia.Any(ag =>
                        ag.Ativo &&
                        ag.TipoAgente == TipoAgenteOcorrencia.Passivo && (
                            (repAlocColab.ObterTodos()
                                .Where(a => a.Papel.PessoaFisica.Codigo.Equals(ag.PessoaFisica.Codigo))
                                .Any(aux => codsAlocacao.Contains(aux.Codigo)))
                            )
                        )
                    );
                }
            }
            #endregion

            #region [AGENTE ATIVO]
            if (!string.IsNullOrWhiteSpace(agAtivoCPF))
                ocorrencia = ocorrencia.Where(o => o.AgentesOcorrencia.Any(ag =>
                    (ag.TipoAgente == TipoAgenteOcorrencia.Ativo) &&
                    ag.PessoaFisica.CPF.Equals(agAtivoCPF.Replace(".", "").Replace("-", ""))
                ));

            if (!string.IsNullOrWhiteSpace(agAtivoPassaporte))
                ocorrencia = ocorrencia.Where(o => o.AgentesOcorrencia.Any(ag =>
                    (ag.TipoAgente == TipoAgenteOcorrencia.Ativo) && ag.PessoaFisica.Passaporte.Equals(agAtivoPassaporte)));

            if (!string.IsNullOrWhiteSpace(agAtivoNome))
                ocorrencia = ocorrencia.Where(o => o.AgentesOcorrencia.Any(ag =>
                    ag.TipoAgente == TipoAgenteOcorrencia.Ativo &&
                    ag.PessoaFisica.Nome.Contains(agAtivoNome)));

            if ((!string.IsNullOrWhiteSpace(agAtivoEmpresa) || !string.IsNullOrEmpty(agAtivoEmpresaCnpj) || !string.IsNullOrEmpty(agAtivoContratada) || !string.IsNullOrEmpty(agAtivoContratadaCnpj)) &&
                (string.IsNullOrEmpty(agPassivoEmpresa) && string.IsNullOrEmpty(agPassivoEmpresaCnpj) && string.IsNullOrEmpty(agPassivoContratada) && string.IsNullOrEmpty(agPassivoContratadaCnpj)))
            {
                if (!string.IsNullOrWhiteSpace(agAtivoEmpresa) || !string.IsNullOrEmpty(agAtivoEmpresaCnpj))
                {
                    ocorrencia = ocorrencia.Where(o => o.AgentesOcorrencia.Any(ag =>
                        ag.Ativo &&
                        ag.TipoAgente == TipoAgenteOcorrencia.Ativo && (
                            (repColab.ObterTodos()
                                .Where(c => c.PessoaFisica.Codigo.Equals(ag.PessoaFisica.Codigo) && (c.PapelLogs.Any(pl =>
                                    (pl.DataInicio <= o.DataOcorrencia) &&
                                    (pl.DataFim == null || pl.DataFim > o.DataOcorrencia)
                                    ))).Any(c => (!string.IsNullOrEmpty(agAtivoEmpresa) && c.Empresa.Nome.Contains(agAtivoEmpresa)) ||
                                                 (!string.IsNullOrEmpty(agAtivoEmpresaCnpj) && c.Empresa.CNPJ == agAtivoEmpresaCnpj)) ||

                            (repPrest.ObterTodos()
                                .Where(p => p.PessoaFisica.Codigo.Equals(ag.PessoaFisica.Codigo) && (p.PapelLogs.Any(pl =>
                                    (pl.DataInicio <= o.DataOcorrencia) &&
                                    (pl.DataFim == null || pl.DataFim > o.DataOcorrencia)
                                    ))).Any(p => (!string.IsNullOrEmpty(agAtivoEmpresa) && p.Empresa.Nome.Contains(agAtivoEmpresa)) ||
                                                 (!string.IsNullOrEmpty(agAtivoEmpresaCnpj) && p.Empresa.CNPJ == agAtivoEmpresaCnpj)))) ||

                            (repVisit.ObterTodos()
                                .Where(v => v.PessoaFisica.Codigo.Equals(ag.PessoaFisica.Codigo) && (v.PapelLogs.Any(pl =>
                                    (pl.DataInicio <= o.DataOcorrencia) &&
                                    (pl.DataFim == null || pl.DataFim > o.DataOcorrencia)
                                    ))).Any(v => !string.IsNullOrEmpty(agAtivoEmpresa) && v.Empresa.Contains(agAtivoEmpresa)))
                            )
                    ));
                }

                if (!string.IsNullOrEmpty(agAtivoContratada) || !string.IsNullOrEmpty(agAtivoContratadaCnpj))
                {
                    var osa = ocorrencia.Where(o => o.AgentesOcorrencia.Any(ag =>
                        ag.Ativo &&
                        ag.TipoAgente == TipoAgenteOcorrencia.Ativo))
                        .SelectMany(aux => aux.AgentesOcorrencia);

                    List<string> pessoasFisica = osa.Select(ocorrenciaSegurancaAgente => ocorrenciaSegurancaAgente.PessoaFisica.Codigo).ToList();
                    var codColabs = repColab.ObterTodos().Where(c => pessoasFisica.Contains(c.PessoaFisica.Codigo)).Select(c => c.Codigo);

                    List<int> codsAlocacao = new List<int>();
                    foreach (var cod in codColabs)
                    {
                        var alocao = repAlocColab.ObterAlocacao(cod);
                        if (alocao == null) continue;
                        var empresa = alocao.ObterContratada().Empresa;
                        if (empresa.Nome.Contains(agAtivoContratada) || empresa.Apelido.Contains(agAtivoContratada) || empresa.CNPJ == agAtivoContratadaCnpj)
                            codsAlocacao.Add(alocao.Codigo);
                    }

                    ocorrencia = ocorrencia.Where(o => o.AgentesOcorrencia.Any(ag =>
                        ag.Ativo &&
                        ag.TipoAgente == TipoAgenteOcorrencia.Ativo && (
                            (repAlocColab.ObterTodos()
                                .Where(a => a.Papel.PessoaFisica.Codigo.Equals(ag.PessoaFisica.Codigo))
                                .Any(aux => codsAlocacao.Contains(aux.Codigo)))
                            )
                        )
                    );
                }
            }
            #endregion

            #region [CONTRATADA DE AGENSTES PASSIVO E ATIVO]
            if ((!string.IsNullOrEmpty(agPassivoEmpresa) || !string.IsNullOrEmpty(agPassivoEmpresaCnpj) || !string.IsNullOrEmpty(agPassivoContratada) || !string.IsNullOrEmpty(agPassivoContratadaCnpj)) &&
                (!string.IsNullOrEmpty(agAtivoEmpresa) || !string.IsNullOrEmpty(agAtivoEmpresaCnpj) || !string.IsNullOrEmpty(agAtivoContratada) || !string.IsNullOrEmpty(agAtivoContratadaCnpj)))
            {
                //BUSCANDO EMPRESA
                if (!string.IsNullOrEmpty(agPassivoEmpresa) || !string.IsNullOrEmpty(agPassivoEmpresaCnpj) ||
                    !string.IsNullOrEmpty(agAtivoEmpresa) || !string.IsNullOrEmpty(agAtivoEmpresaCnpj))
                {
                    ocorrencia = ocorrencia.Where(o => o.AgentesOcorrencia.Any(ag =>
                        ag.Ativo && (
                            (repColab.ObterTodos()
                                .Where(c => c.PessoaFisica.Codigo.Equals(ag.PessoaFisica.Codigo) && (c.PapelLogs.Any(pl =>
                                    (pl.DataInicio <= o.DataOcorrencia) &&
                                    (pl.DataFim == null || pl.DataFim > o.DataOcorrencia)
                                    ))).Any(c => (!string.IsNullOrEmpty(agPassivoEmpresa) && c.Empresa.Nome.Contains(agPassivoEmpresa)) ||
                                                 (!string.IsNullOrEmpty(agPassivoEmpresaCnpj) && c.Empresa.CNPJ == agPassivoEmpresaCnpj) ||
                                                 (!string.IsNullOrEmpty(agAtivoEmpresa) && c.Empresa.Nome.Contains(agAtivoEmpresa)) ||
                                                 (!string.IsNullOrEmpty(agAtivoEmpresaCnpj) && c.Empresa.CNPJ == agAtivoEmpresaCnpj)) ||

                            (repPrest.ObterTodos()
                                .Where(p => p.PessoaFisica.Codigo.Equals(ag.PessoaFisica.Codigo) && (p.PapelLogs.Any(pl =>
                                    (pl.DataInicio <= o.DataOcorrencia) &&
                                    (pl.DataFim == null || pl.DataFim > o.DataOcorrencia)
                                    ))).Any(p => (!string.IsNullOrEmpty(agPassivoEmpresa) && p.Empresa.Nome.Contains(agPassivoEmpresa)) ||
                                                 (!string.IsNullOrEmpty(agPassivoEmpresaCnpj) && p.Empresa.CNPJ == agPassivoEmpresaCnpj) ||
                                                 (!string.IsNullOrEmpty(agAtivoEmpresa) && p.Empresa.Nome.Contains(agAtivoEmpresa)) ||
                                                 (!string.IsNullOrEmpty(agAtivoEmpresaCnpj) && p.Empresa.CNPJ == agAtivoEmpresaCnpj)))) ||

                            (repVisit.ObterTodos()
                                .Where(v => v.PessoaFisica.Codigo.Equals(ag.PessoaFisica.Codigo) && (v.PapelLogs.Any(pl =>
                                    (pl.DataInicio <= o.DataOcorrencia) &&
                                    (pl.DataFim == null || pl.DataFim > o.DataOcorrencia)
                                    ))).Any(v => (!string.IsNullOrEmpty(agPassivoEmpresa) && v.Empresa.Contains(agPassivoEmpresa) ||
                                                 (!string.IsNullOrEmpty(agAtivoEmpresa) && v.Empresa.Contains(agAtivoEmpresa)))))
                            )
                        )
                    );
                }

                if (!string.IsNullOrEmpty(agPassivoContratada) || !string.IsNullOrEmpty(agPassivoContratadaCnpj) ||
                    !string.IsNullOrEmpty(agAtivoContratada) || !string.IsNullOrEmpty(agAtivoContratadaCnpj))
                {
                    var osa = ocorrencia.Where(o => o.AgentesOcorrencia.Any(ag =>
                        ag.Ativo &&
                        ag.TipoAgente == TipoAgenteOcorrencia.Ativo))
                        .SelectMany(aux => aux.AgentesOcorrencia);

                    List<string> pessoasFisica = osa.Select(ocorrenciaSegurancaAgente => ocorrenciaSegurancaAgente.PessoaFisica.Codigo).ToList();
                    var codColabs = repColab.ObterTodos().Where(c => pessoasFisica.Contains(c.PessoaFisica.Codigo)).Select(c => c.Codigo);

                    List<int> codsAlocacao = new List<int>();
                    foreach (var cod in codColabs)
                    {
                        var alocao = repAlocColab.ObterAlocacao(cod);
                        if (alocao == null) continue;
                        var empresa = alocao.ObterContratada().Empresa;
                        if ((empresa.Nome.Contains(agPassivoContratada) || empresa.Apelido.Contains(agPassivoContratada) || empresa.CNPJ == agPassivoContratadaCnpj) &&
                            (empresa.Nome.Contains(agAtivoContratada) || empresa.Apelido.Contains(agAtivoContratada) || empresa.CNPJ == agAtivoContratadaCnpj))
                            codsAlocacao.Add(alocao.Codigo);
                    }

                    ocorrencia = ocorrencia.Where(o => o.AgentesOcorrencia.Any(ag =>
                        ag.Ativo && (
                            repAlocColab.ObterTodos()
                                .Where(a => a.Papel.PessoaFisica.Codigo.Equals(ag.PessoaFisica.Codigo))
                                .Any(aux => codsAlocacao.Contains(aux.Codigo))
                            )
                        )
                    );
                }

            }
            #endregion

            return ocorrencia.OrderBy(o => o.DataOcorrencia);
        }       

        private List<RelatorioOcorrenciaSegurancaModelView> montaObjs(IQueryable<OcorrenciaSeguranca> regs, StoreRequestParameters parameters = null, bool ignoreParameters = false)
        {
            if (!ignoreParameters)
            {
                if (parameters == null)
                    parameters = new StoreRequestParameters();
                regs = regs.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 25);
            }

            return regs.Select(oc => montaObj(oc)).ToList();
        }

        private RelatorioOcorrenciaSegurancaModelView montaObj(OcorrenciaSeguranca ocorrencia)
        {
            CultureInfo cultbr = new CultureInfo("pt-BR");
            var relOcorrencia = new RelatorioOcorrenciaSegurancaModelView();

            relOcorrencia.AcoesRecomendadas     = ocorrencia.AcoesRecomendadas;
            relOcorrencia.ID                = ocorrencia.Codigo.ToString();
            relOcorrencia.Codigo                = ocorrencia.Codigo;
            relOcorrencia.Descricao             = ocorrencia.Descricao;
            relOcorrencia.Procedimentos         = ocorrencia.Procedimentos;
            relOcorrencia.DataOcorrencia        = ocorrencia.DataOcorrencia.ToString("dd/MM/yyyy", cultbr);
            relOcorrencia.HoraOcorrencia        = ocorrencia.HoraOcorrencia.ToString("hh\\:mm", cultbr);
            relOcorrencia.TipoOcorrenciaDesc    = ocorrencia.TipoOcorrencia.Descricao;
            relOcorrencia.Vulnerabilidade       = (ocorrencia.Vulnerabilidade.ToString() == "True" ? "SIM" : "NÃO");
            relOcorrencia.Tentativa             = (ocorrencia.Tentativa.ToString() == "True" ? "SIM" : "NÃO");
            relOcorrencia.Efetiva               = (ocorrencia.Efetiva.ToString() == "True" ? "SIM" : "NÃO");
            relOcorrencia.SupervisorLogin       = ocorrencia.Supervisor.Login.ToString();
            relOcorrencia.Supervisor            = ocorrencia.Supervisor.Login.ToString();
            relOcorrencia.SupervisorNome        = ocorrencia.Supervisor.PessoaFisica.Nome.ToString();
            relOcorrencia.Local                 = ocorrencia.Local.Sigla + " - " + ocorrencia.Local.Nome;
            relOcorrencia.Gravidade             = ocorrencia.Gravidade;
            relOcorrencia.GravidadeDesc         = ocorrencia.Gravidade.ObterDescricaoEnum();

            relOcorrencia.DataRegistro = ocorrencia.DataRegistro.ToString("dd/MM/yyyy HH:mm", cultbr);
            relOcorrencia.Graduacoes = ocorrencia.Vulnerabilidade ? GraduacaoOcorrencia.Vulnerabilidade.ObterDescricaoEnum() : "";
            relOcorrencia.Graduacoes += ocorrencia.Efetiva ? (string.Format("{0}{1}", (string.IsNullOrEmpty(relOcorrencia.Graduacoes) ? "" : ", "), GraduacaoOcorrencia.Efetiva.ObterDescricaoEnum())) : "";
            relOcorrencia.Graduacoes += ocorrencia.Tentativa ? (string.Format("{0}{1}", (string.IsNullOrEmpty(relOcorrencia.Graduacoes) ? "" : ", "), GraduacaoOcorrencia.Tentativa.ObterDescricaoEnum())) : "";
            relOcorrencia.AgentesPassivo = string.Join(" / ", ocorrencia.AgentesOcorrencia.Where(ag => ag.Ativo && ag.TipoAgente.Equals(TipoAgenteOcorrencia.Passivo)).Select(ag => 
                string.Format("{0}-{1}-{2}", ag.PessoaFisica.Nome, (!string.IsNullOrEmpty(ag.PessoaFisica.CPF) ? ag.PessoaFisica.CPF : ag.PessoaFisica.Passaporte),
                (
                    (ag.PessoaFisica.ObterPapel() == null ) ? "" : 
                    ((ag.PessoaFisica.ObterPapel() is Colaborador) ? ((ag.PessoaFisica.ObterPapel() as Colaborador).Empresa.Nome) :
                    ((ag.PessoaFisica.ObterPapel() is PrestadorServico) ? ((ag.PessoaFisica.ObterPapel() as PrestadorServico).Empresa.Nome) :
                    ((ag.PessoaFisica.ObterPapel() is Visitante) ? ((ag.PessoaFisica.ObterPapel() as Visitante).Empresa) : "")))
                )
            )).ToArray());
            relOcorrencia.AgentesAtivo = string.Join(" / ", ocorrencia.AgentesOcorrencia.Where(ag => ag.Ativo && ag.TipoAgente.Equals(TipoAgenteOcorrencia.Ativo)).Select(ag => 
                string.Format("{0}-{1}-{2}", ag.PessoaFisica.Nome, (!string.IsNullOrEmpty(ag.PessoaFisica.CPF) ? ag.PessoaFisica.CPF : ag.PessoaFisica.Passaporte),
                (
                    (ag.PessoaFisica.ObterPapel() == null ) ? "" : 
                    ((ag.PessoaFisica.ObterPapel() is Colaborador) ? ((ag.PessoaFisica.ObterPapel() as Colaborador).Empresa.Nome) :
                    ((ag.PessoaFisica.ObterPapel() is PrestadorServico) ? ((ag.PessoaFisica.ObterPapel() as PrestadorServico).Empresa.Nome) :
                    ((ag.PessoaFisica.ObterPapel() is Visitante) ? ((ag.PessoaFisica.ObterPapel() as Visitante).Empresa) : "")))
                )

            )).ToArray());
            relOcorrencia.Veiculos = string.Join("// ", ocorrencia.Veiculos.Where(v => v.Ativo).Select(v=> string.Format("Placa: {0}, Marca: {1}, Modelo: {2}, Cor: {3}, Ano: {4}/{5}",
                v.Veiculo.Placa, v.Veiculo.Modelo.Fabricante.Nome, v.Veiculo.Modelo.Nome, v.Veiculo.Cor, v.Veiculo.AnoFabricacao, v.Veiculo.AnoModelo)).ToArray());
            OcorrenciaSegurancaAgente agentePassivo = ocorrencia.AgentesOcorrencia.Where(a => a.TipoAgente == TipoAgenteOcorrencia.Passivo).FirstOrDefault();
            if (agentePassivo != null)
            {
                relOcorrencia.CPFAgentesPassivo = agentePassivo.PessoaFisica.CPFFormatado();
                relOcorrencia.PassaporteAgentesPassivo = agentePassivo.PessoaFisica.Passaporte;
                Papel papel = agentePassivo.PessoaFisica.ObterPapel();
                if (papel != null)
                    relOcorrencia.EmpresaAgentesPassivo = papel is Colaborador ? (papel as Colaborador).Empresa.Nome : papel is PrestadorServico ? (papel as PrestadorServico).Empresa.Nome : (papel as Visitante).Empresa;
            }
            
            return relOcorrencia;
        }

        private List<MidiaModelView> montaObjImagem(OcorrenciaSeguranca ocorrencia)
        {
            try
            {
                string path = ConfigurationManager.AppSettings.Get("pathUploads");

                if (!System.IO.Directory.Exists(path))
                    throw new CoreException("O diretório onde a midia seria lida não está acessivel.");

                return ocorrencia.Midias.Select(m => new MidiaModelView()
                {
                    Arquivo = null,
                    CodigoMidia = m.Midia.Codigo,
                    DataRegistro = m.Midia.DataRegistro,
                    Extensao = m.Midia.Extensao,
                    MIME = m.Midia.MimeType,
                    NomeDisco = m.Midia.NomeArquivo,
                    NomeOriginal = string.Empty,
                    //URL = ConvertImageURLToBase64("http://localhost:46251/Docs/" + m.Midia.NomeArquivo)
                    URL = m.Midia.NomeArquivo != null && System.IO.File.Exists(@path + m.Midia.NomeArquivo) ? System.Convert.ToBase64String(System.IO.File.ReadAllBytes(@path+m.Midia.NomeArquivo)): ""
                }).ToList();

            }
            catch (Exception ex)
            {
                throw new CoreException(" " + ex.Message);                
            }
        }

        private List<OcorrenciaSegurancaAgenteModelView> montaObjSegAgente(OcorrenciaSeguranca ocorrencia)
        {
            return ocorrencia.AgentesOcorrencia.Select(m => new OcorrenciaSegurancaAgenteModelView()
            {
                Codigo = m.Codigo,
                Nome = m.PessoaFisica.Nome,
                Telefone = m.PessoaFisica.Telefones.Select(x => x.TelefoneNum).FirstOrDefault().ToString()
            }).ToList();             
        }

        public String ConvertImageURLToBase64(String url)
        {
            StringBuilder _sb = new StringBuilder();

            Byte[] _byte = this.GetImage(url);

            _sb.Append(Convert.ToBase64String(_byte, 0, _byte.Length));

            return _sb.ToString();
        }

        private byte[] GetImage(string url)
        {
            Stream stream = null;
            byte[] buf;

            try
            {
                WebProxy myProxy = new WebProxy();
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);

                HttpWebResponse response = (HttpWebResponse)req.GetResponse();
                stream = response.GetResponseStream();

                using (BinaryReader br = new BinaryReader(stream))
                {
                    int len = (int)(response.ContentLength);
                    buf = br.ReadBytes(len);
                    br.Close();
                }

                stream.Close();
                response.Close();
            }
            catch (Exception exp)
            {
                buf = null;
            }

            return (buf);
        }


    }
}
