﻿using System.Diagnostics.CodeAnalysis;
using Controlsys.Dominio.Seguranca;
using Controlsys.Infra;
using Controlsys.Repositorios.Seguranca;
using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar alterar senhas.
    /// </summary>
    [ExtendController]
    public class AlterarSenhaController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /AlterarSenha/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.AlterarSenhaController.
        /// </summary>
        ///
        /// <param name="unidade">
        /// O(a) unidade.
        /// </param>
        public AlterarSenhaController(IUnidadeTrabalho unidade) { UnidadeTrabalho = unidade; }

        /// <summary>
        /// Retorna a página de alterarSenha.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [ExcludeFromCodeCoverage]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Altera um registro de alterarSenha.
        /// </summary>
        ///
        /// <param name="senhaNova">
        /// O(a) senha nova.
        /// </param>
        /// <param name="senhaNovaConf">
        /// O(a) senha nova conf.
        /// </param>
        /// <param name="senhaAtual">
        /// O(a) senha atual.
        /// </param>
        [Transaction]
        public void Alterar(string senhaNova, string senhaNovaConf, string senhaAtual)
        {
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            string userLogado = HttpContext.User.Identity.Name;
            Usuario user = repUsuario.AlterarSenha(userLogado, senhaNova, senhaNovaConf, senhaAtual);
            enviarEmail(user, senhaNova);
        }

        /// <summary>
        /// Enviar email.
        /// </summary>
        ///
        /// <param name="usuario">
        /// O(a) usuario.
        /// </param>
        /// <param name="senha">
        /// O(a) senha.
        /// </param>
        private void enviarEmail(Usuario usuario, string senha)
        {
            Dictionary<string, string> valoresSubs = new Dictionary<string, string>();

            valoresSubs.Add("$$Login$$", usuario.Login);
            valoresSubs.Add("$$Senha$$", senha);

            Globalsys.Util.Email.Enviar("[CONTROLSYS] - Dados de acesso",
                                        Server.MapPath("/Content/templates/alteracaoSenha.htm"),
                                        valoresSubs,
                                        string.Empty,
                                        null,
                                        usuario.PessoaFisica.Email);
        }
    }
}
