﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Controlsys.Dominio.Relatorio;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Parametros;
using Controlsys.Repositorios.Pessoas;
using Ext.Net;
using Globalsys;
using Controlsys.Dominio.Pessoas;
using Ext.Net.MVC;
using Globalsys.Relatorios;
using Controlsys.Infra;
using Controlsys.Repositorio.Relatorio;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class RelatorioCrachaPeriodoController : Controller, IControllerBase
    {

        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        public RelatorioCrachaPeriodoController(IUnidadeTrabalho unidade) { UnidadeTrabalho = unidade; }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Exportar(List<string> selectedFields, string tipoRelatorio, DateTime dataInicial, DateTime dataFinal, string empresa = null, string cnpj = null, TipoPapel? tipoPapel = null)
        {
            var list = FiltrarRegistros(dataInicial, dataFinal, empresa, cnpj, tipoPapel);
            var crachas = montaObj(list, null, true);

            ReportConfig reportConfig = ReportHelper.RenderReport<RelatorioCrachaPeriodoModelView>(selectedFields, tipoRelatorio, crachas);

            string fileName = string.Empty;

            switch (tipoRelatorio)
            {
                case "PDF":
                    fileName = "download.pdf";
                    break;
                case "EXCELOPENXML":
                    fileName = "download.xlsx";
                    break;
                case "WORDOPENXML":
                    fileName = "download.docx";
                    break;
            }

            return File(reportConfig.Bytes, reportConfig.MimeType, fileName);
        }

        public ActionResult Pesquisar(StoreRequestParameters parameters, DateTime dataInicial, DateTime dataFinal, string empresa = null, string cnpj = null, TipoPapel? tipoPapel = null)
        {
            IQueryable<RelatorioCrachaPeriodo> regs = FiltrarRegistros(dataInicial, dataFinal, empresa, cnpj, tipoPapel);

            int total = regs.Count();
            List<RelatorioCrachaPeriodo> data = montaObj(regs, parameters);

            return new StoreResult(data, total);
        }

        private IQueryable<RelatorioCrachaPeriodo> FiltrarRegistros(DateTime dataInicial, DateTime dataFinal, string empresa = null, string cnpj = null, TipoPapel? tipoPapel = null)
        {
            IRepositorioRelatorioCrachaPeriodo repRelatorio = Fabrica.Instancia.ObterRepositorio<IRepositorioRelatorioCrachaPeriodo>(UnidadeTrabalho);
            var registros = repRelatorio.ObterDados();

            dataFinal = dataFinal.AddDays(1);
            registros = registros.Where(r => (r.DataRegistroCracha >= dataInicial) && (r.DataRegistroCracha < dataFinal));

            if (!string.IsNullOrEmpty(empresa))
                registros = registros.Where(r => r.NomeFantasia.Contains(empresa));

            if (!string.IsNullOrEmpty(cnpj))
                registros = registros.Where(r => r.CNPJ == cnpj);

            if (tipoPapel != null)
                registros = registros.Where(r => r.TipoPapel == tipoPapel.ObterDescricaoEnum());

            return registros;
        }

        private List<RelatorioCrachaPeriodo> montaObj(IQueryable<RelatorioCrachaPeriodo> regs, StoreRequestParameters parameters = null, bool ignoreParameters = false)
        {
            if (!ignoreParameters)
            {
                if (parameters == null)
                    parameters = new StoreRequestParameters();
                regs = regs.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 25);
            }

            return regs.ToList();

            //List<RelatorioCrachaPeriodoModelView> lista = new List<RelatorioCrachaPeriodoModelView>();
            //foreach (var rel in lista)
            //{
            //    
            //}
            //
            //List<RelatorioCrachaPeriodoModelView> lista = regs.Select(r => new RelatorioCrachaPeriodoModelView()
            //{
            //    CodigoPapel = r.CodigoPapel,
            //    CodigoPessoaFisica = r.CodigoPessoaFisica,
            //    CPF = r.CPF,
            //    Passaporte = r.Passaporte,
            //    RG = r.RG,
            //    Nome = r.Nome,
            //    Cargo = r.Cargo,
            //    TipoPapel = r.TipoPapel,
            //    DataRegistroPessoa = r.DataRegistroPessoa,
            //    DataRegistroPapel = r.DataRegistroPapel,
            //    StatusPapel = r.StatusPapel,
            //    DataRegistroStatusPapel = r.DataRegistroStatusPapel,
            //    DataValidadeASO = (DateTime) (r.DataValidadeASO ?? null),
            //    MotivoPapelLog = r.MotivoPapelLog,
            //    Cracha = r.Cracha,
            //    StatusCracha = r.StatusCracha ? "Ativo" : "Inativo",
            //    DataRegistroCracha = r.DataRegistroCracha,
            //    CNPJ = r.CNPJ,
            //    RazaoSocial = r.RazaoSocial,
            //    NomeFantasia = r.NomeFantasia
            //}).ToList();

            //return lista;
        }
        
    }
}
