﻿
using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorios.Parametros;
using Controlsys.Repositorios.Pessoas;
using Ext.Net;
using Ext.Net.MVC;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Util;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class ArquivoPapelController : Controller, IControllerBase
    {

        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        public ArquivoPapelController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        public ActionResult Index()
        {
            return View();
        }

        public FileResult ExportarSelecao(int[] codigosPapeis = null, int[] codigosTipoDocumento = null)
        {
            if (codigosPapeis == null || codigosPapeis.Length == 0)
                throw new CoreException("Selecione ao menos um papel.");

            return GerarZip(FiltrarPapeis(null, null, null, null, null, 0, null, codigosPapeis), codigosTipoDocumento);
        }

        public FileResult ExportarPesquisa(string nomeEmpresa = null, string cnpjEmpresa = null, string nomePessoa = null, string cpfPassaporte = null, int[] codigosTipoDocumento = null, int codigoSindicato = 0, string statusPapel = null, TipoPapel[] tiposPapel = null)
        {
            return GerarZip(FiltrarPapeis(nomeEmpresa, cnpjEmpresa, nomePessoa, cpfPassaporte, codigosTipoDocumento, codigoSindicato, statusPapel, null, tiposPapel), codigosTipoDocumento);
        }

        public ActionResult Pesquisar(StoreRequestParameters parameters, string nomeEmpresa = null, string cnpjEmpresa = null, string nomePessoa = null, string cpfPassaporte = null, int[] codigosTipoDocumento = null, int codigoSindicato = 0, string statusPapel = null, TipoPapel[] tiposPapel = null, string fornecedor = null)
        {
            nomeEmpresa = Empresa.RetornaApenasNomeEmpresa(nomeEmpresa);

            IQueryable<Papel> resultadoFiltro = FiltrarPapeis(nomeEmpresa, cnpjEmpresa, nomePessoa, cpfPassaporte, codigosTipoDocumento, codigoSindicato, statusPapel, null, tiposPapel, fornecedor);

            int total = resultadoFiltro.Count();
            ArquivoPapelModelView[] retorno = this.MontaObjetos(resultadoFiltro, parameters);

            return new StoreResult(retorno, total);
        }

        public IQueryable<Papel> FiltrarPapeis(string nomeEmpresa = null, string cnpjEmpresa = null, string nomePessoa = null, string cpfPassaporte = null, int[] codigosTipoDocumento = null, int codigoSindicato = 0, string statusPapel = null, int[] codigosPapeis = null, TipoPapel[] tiposPapel = null, string fornecedor = null)
        {
            if (!string.IsNullOrEmpty(cnpjEmpresa))
                cnpjEmpresa = Tools.Limpa_CampoCNPJ(cnpjEmpresa);

            IQueryable<Papel> papeis = Fabrica.Instancia.ObterRepositorio<IRepositorioPapel>(UnidadeTrabalho).ObterTodos().Where(p => p.Documentos.Any(d => d.Ativo && d.Documento.Midias.Any(dm => dm.Ativo)));

            if (codigosPapeis != null && codigosPapeis.Length > 0)
                papeis = papeis.Where(p => codigosPapeis.Contains(p.Codigo));

            if (!string.IsNullOrEmpty(nomeEmpresa) || !string.IsNullOrEmpty(cnpjEmpresa) || codigoSindicato != 0 || !string.IsNullOrEmpty(fornecedor))
            {
                IQueryable<Colaborador> colaboradores = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho).ObterTodos();
                IQueryable<PrestadorServico> prestadores = Fabrica.Instancia.ObterRepositorio<IRepositorioPrestadorServico>(UnidadeTrabalho).ObterTodos();
                IQueryable<Visitante> visitantes = Fabrica.Instancia.ObterRepositorio<IRepositorioVisitante>(UnidadeTrabalho).ObterTodos();

                if (!string.IsNullOrEmpty(nomeEmpresa))
                {
                    colaboradores = colaboradores.Where(c => c.Empresa.Nome == nomeEmpresa);
                    prestadores = prestadores.Where(c => c.Empresa.Nome == nomeEmpresa);
                    visitantes = visitantes.Where(c => c.Empresa == nomeEmpresa);
                }

                if (!string.IsNullOrEmpty(cnpjEmpresa))
                {
                    colaboradores = colaboradores.Where(c => c.Empresa.CNPJ == cnpjEmpresa);
                    prestadores = prestadores.Where(c => c.Empresa.CNPJ == cnpjEmpresa);
                    visitantes = Enumerable.Empty<Visitante>().AsQueryable();
                }

                if (codigoSindicato != 0)
                {
                    colaboradores = colaboradores.Where(c => c.Empresa.Sindicatos.Any(s => s.Sindicato.Codigo == codigoSindicato && s.Ativo));
                    prestadores = prestadores.Where(c => c.Empresa.Sindicatos.Any(s => s.Sindicato.Codigo == codigoSindicato && s.Ativo));
                    visitantes = Enumerable.Empty<Visitante>().AsQueryable();
                }

                if (!string.IsNullOrEmpty(fornecedor))               
                {
                    colaboradores = Enumerable.Empty<Colaborador>().AsQueryable();
                    prestadores = prestadores.Where(c => c.Fornecedor.Codigo == Convert.ToInt32(fornecedor));
                    visitantes = Enumerable.Empty<Visitante>().AsQueryable();
                }

                List<Papel> listaColaboradores = colaboradores.ToList().Select(c => c as Papel).ToList();
                List<Papel> listaPrestadores = prestadores.ToList().Select(p => p as Papel).ToList();
                List<Papel> listaVisitantes = visitantes.ToList().Select(v => v as Papel).ToList();

                papeis = listaColaboradores.Union(listaPrestadores).Union(listaVisitantes).AsQueryable();
                papeis = papeis.Where(p => p.Documentos.Any(d => d.Documento.Midias.Any()));
            }

            if (!string.IsNullOrEmpty(nomePessoa))
                papeis = papeis.Where(p => p.PessoaFisica.Nome.ToUpper().Contains(nomePessoa.ToUpper()));

            if (!string.IsNullOrEmpty(cpfPassaporte))
            {
                string cpfpassaporteformatado = cpfPassaporte.Replace("-", string.Empty).Replace(".", string.Empty).Replace("/", string.Empty);
                papeis = papeis.Where(p => (p.PessoaFisica.CPF != null && p.PessoaFisica.CPF.Contains(cpfpassaporteformatado)) || (p.PessoaFisica.Passaporte != null && p.PessoaFisica.Passaporte.Contains(cpfpassaporteformatado)));
            }

            if (codigosTipoDocumento != null)
                papeis = papeis.Where(p => p.Documentos.Any(d => d.Documento.Midias.Any() && codigosTipoDocumento.Contains(d.Documento.TipoDocumento.Codigo)));

            if (!string.IsNullOrEmpty(statusPapel))
            {
                StatusPapelLog? enumStatusPapel = null;
                foreach (StatusPapelLog tipoStatus in Enum.GetValues(typeof(StatusPapelLog)))
                {
                    if (tipoStatus.ObterDescricaoEnum() == statusPapel)
                    {
                        enumStatusPapel = tipoStatus;
                        break;
                    }

                }

                papeis = papeis.Where(p => p.PapelLogs.OrderByDescending(pl => pl.Codigo)
                                            .Select(pl => pl.Status)
                                            .FirstOrDefault() == enumStatusPapel.Value);
            }

            if (tiposPapel != null && tiposPapel.Length > 0)
            {
                bool isColaborador = tiposPapel.Any(tp => tp == TipoPapel.Colaborador);
                bool isPrestador = tiposPapel.Any(tp => tp == TipoPapel.PrestadorServico);
                bool isVisitante = tiposPapel.Any(tp => tp == TipoPapel.Visitante);

                papeis = papeis.Where(p => (p is Colaborador && isColaborador) || (p is PrestadorServico && isPrestador) || (p is Visitante && isVisitante));

            }

            return papeis;
        }

        public ArquivoPapelModelView[] MontaObjetos(IQueryable<Papel> papeis, StoreRequestParameters parameters = null, bool ignoreParameters = false)
        {

            if (!ignoreParameters)
            {
                if (parameters == null)
                    parameters = new StoreRequestParameters();

                papeis = papeis.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 30);
            }

            ArquivoPapelModelView[] papeisModeVIew = papeis.ToList().Select(p => new ArquivoPapelModelView
            {
                CodigoPapel = p.Codigo,
                Nome = p.PessoaFisica.Nome,
                CPF = p.PessoaFisica.CPFFormatado() ?? string.Empty,
                Passaporte = p.PessoaFisica.Passaporte ?? string.Empty,
                Empresa = ((p is Colaborador) && (p as Colaborador).Empresa != null ? (p as Colaborador).Empresa.Nome : (p is PrestadorServico) && (p as PrestadorServico).Empresa != null ? (p as PrestadorServico).Empresa.Nome : (p is Visitante) ? (p as Visitante).Empresa : string.Empty),
                Cargo = ((p is Colaborador) && (p as Colaborador).Cargo != null ? (p as Colaborador).Cargo.Nome : (p is PrestadorServico) && (p as PrestadorServico).Cargo != null ? (p as PrestadorServico).Cargo.Nome : (p is Visitante) ? (p as Visitante).Funcao : string.Empty),
                StatusPapel = p.ObterLogPapel().Status.ObterDescricaoEnum()
            }).OrderBy(p => p.Nome).ToArray();

            return papeisModeVIew;
        }

        private FileContentResult GerarZip(IQueryable<Papel> papeis, int[] codigosTipoDocumento = null)
        {
            //Cria Stream para ser usada pelo ZupArchive sendo que este a utilizará para gerar o arquivo que será enviado para download
            using (var streamArquivoFinalCompactado = new MemoryStream())
            {
                //Cria um ZipArchive que é responsavel por anexar as pastas e arquivos ao arquivo final a ser exportado
                using (var zipArchive = new ZipArchive(streamArquivoFinalCompactado, ZipArchiveMode.Update, false))
                {
                    foreach (Papel papel in papeis)
                    {
                        //regex para remover caracteres diferentes de alfanumericos para não gerar problemas na criação de pastas
                        Regex rgx = new Regex(@"[^\p{L}0-9 -]");

                        string nomePasta = papel.Codigo.ToString() + "-" + (!string.IsNullOrEmpty(papel.PessoaFisica.CPF) ? papel.PessoaFisica.CPF + "-" : papel.PessoaFisica.Passaporte + "-") + rgx.Replace(papel.PessoaFisica.Nome, string.Empty) + @"\";

                        IEnumerable<string> urls = papel.Documentos.Where(d => codigosTipoDocumento == null || codigosTipoDocumento.Contains(d.Documento.TipoDocumento.Codigo)).SelectMany(d => d.Documento.Midias).Select(dm => dm.Midia.Url);

                        if (urls.Count() > 0)
                        {
                            //Cria uma nova entrada no zip. caminhos terminados com '\' se tornam pastas. 
                            var zipPasta = zipArchive.CreateEntry(nomePasta, CompressionLevel.Optimal);

                            string arquivosNaoEncontrados = string.Empty;

                            foreach (string url in urls)
                            {
                                if (System.IO.File.Exists(url))
                                {
                                    string nomeArquivo = System.IO.Path.Combine(new string[] { nomePasta, System.IO.Path.GetFileName(url) });

                                    var zipArquivo = zipArchive.CreateEntryFromFile(url, nomeArquivo, CompressionLevel.Optimal);
                                }
                                else
                                {
                                    arquivosNaoEncontrados = url + Environment.NewLine;
                                }
                            }
                            ///TODO: Incluir um arquivo txt listando os arquivos nao encontrados
                        }
                    }

                }

                return new FileContentResult(streamArquivoFinalCompactado.ToArray(), "application/zip") { FileDownloadName = "Download.zip" };
            }
        }
    }
}
