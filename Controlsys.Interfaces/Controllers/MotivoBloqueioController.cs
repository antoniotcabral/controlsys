﻿using Controlsys.Dominio.Acesso;
using Controlsys.Infra;
using Controlsys.Repositorios.Parametros;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar motivo bloqueioes.
    /// </summary>
    [ExtendController]
    public class MotivoBloqueioController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /MotivoBloqueio/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.MotivoBloqueioController.
        /// </summary>
        ///
        /// <param name="unidade">
        /// O(a) unidade.
        /// </param>
        public MotivoBloqueioController(IUnidadeTrabalho unidade)
        {
            UnidadeTrabalho = unidade;
        }

        /// <summary>
        /// Retorna a página de motivoBloqueio.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Inclui um novo motivoBloqueio.
        /// </summary>
        ///
        /// <param name="motivo">
        /// O(a) motivo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Incluir(MotivoBloqueio motivo)
        {
            IRepositorioMotivo repMotivo = Fabrica.Instancia.ObterRepositorio<IRepositorioMotivo>(UnidadeTrabalho);
            motivo.DataCadastro= DateTime.Now;
            motivo.Ativar();
            repMotivo.Validar(motivo, EstadoObjeto.Novo);
            repMotivo.Salvar(motivo);
            return Json(motivo);
        }

        /// <summary>
        /// Altera um registro de motivoBloqueio.
        /// </summary>
        ///
        /// <param name="motivo">
        /// O(a) motivo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Alterar(MotivoBloqueio motivo)
        {
            IRepositorioMotivo repMotivo = Fabrica.Instancia.ObterRepositorio<IRepositorioMotivo>(UnidadeTrabalho);
            MotivoBloqueio motivoM = repMotivo.ObterPorId(motivo.Codigo);
            motivoM.Nome = motivo.Nome;
            motivoM.Descricao = motivo.Descricao;
            repMotivo.Validar(motivoM, EstadoObjeto.Alterado);
            repMotivo.Atualizar(motivoM);
            return Json(motivoM);
        }

        /// <summary>
        /// Ativar inativar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult AtivarInativar(int codigo)
        {
            IRepositorioMotivo repMotivo = Fabrica.Instancia.ObterRepositorio<IRepositorioMotivo>(UnidadeTrabalho);
            MotivoBloqueio motivo = repMotivo.ObterPorId(codigo);
            if (motivo.Ativo)
            {
                motivo.Inativar();
            }
            else
            {
                repMotivo.Validar(motivo, EstadoObjeto.Ativado);
                motivo.Ativar();
            }
            repMotivo.Atualizar(motivo);
            return Json(motivo);

        }

        /// <summary>
        /// Selecionar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Selecionar(int codigo)
        {
            IRepositorioMotivo repMotivo = Fabrica.Instancia.ObterRepositorio<IRepositorioMotivo>(UnidadeTrabalho);
            MotivoBloqueio motivo = repMotivo.ObterPorId(codigo);
            return Json(motivo);
        }

        /// <summary>
        /// Retorna todos os objetos do tipo MotivoBloqueio.
        /// </summary>
        ///
        /// <param name="apenasAtivos">
        /// true to apenas ativos.
        /// </param>
        ///
        /// <returns>
        /// Detalhes do tipo de objeto ligado ao controller no formato JSON.
        /// </returns>
        public ActionResult ObterTodos(bool apenasAtivos = false)
        {
            IRepositorioMotivo repSetor = Fabrica.Instancia.ObterRepositorio<IRepositorioMotivo>(UnidadeTrabalho);

            IQueryable<MotivoBloqueio> motivos = repSetor.ObterTodos();

            if (apenasAtivos)
            {
                motivos = motivos.Where(t => t.Ativo);
            }

            return Json(motivos.Select(m => new
                               {
                                   Codigo = m.Codigo,
                                   Nome = m.Nome,
                                   Descricao = m.Descricao,
                                   Ativo = m.Ativo
                               })
                               .OrderBy(m => m.Nome)
                               .ToList(), JsonRequestBehavior.AllowGet);
        }
    }
}
