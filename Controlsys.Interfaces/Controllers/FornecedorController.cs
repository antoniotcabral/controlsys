﻿using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Enderecos;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorios.Empresas;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar fornecedors.
    /// </summary>
    [ExtendController]
    public class FornecedorController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /Fornecedor/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.FornecedorController.
        /// </summary>
        ///
        /// <param name="unidade">
        /// O(a) unidade.
        /// </param>
        public FornecedorController(IUnidadeTrabalho unidade)
        {
            UnidadeTrabalho = unidade;
        }

        /// <summary>
        /// Retorna a página de fornecedor.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Inclui um novo fornecedor.
        /// </summary>
        ///
        /// <param name="fornecedor">
        /// O(a) fornecedor.
        /// </param>
        /// <param name="empresas">
        /// O(a) empresas.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Incluir(Fornecedor fornecedor, List<Empresa> empresas)
        {
            IRepositorioFornecedor repFornecedor = Fabrica.Instancia.ObterRepositorio<IRepositorioFornecedor>(UnidadeTrabalho);
            fornecedor.DataRegistro = DateTime.Now;
            fornecedor.TelefoneContato = fornecedor.TelefoneContato.ToString().Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "");
            fornecedor.CNPJ = fornecedor.CNPJ.Replace(".", "").Replace("-", "").Replace("/", "");
            fornecedor.Ativar();

            repFornecedor.Validar(fornecedor, EstadoObjeto.Novo);
            repFornecedor.Salvar(fornecedor);

            foreach (var item in empresas)
            {
                FornecedorEmpresa fe = new FornecedorEmpresa();
                fe.Empresa = item;
                fe.Fornecedor = fornecedor;
                fe.DataRegistro = DateTime.Now;
                fe.Ativar();
                repFornecedor.SalvarFornecedorEmpresa(fe);
            }

            return Json(montaObj(fornecedor));
        }

        /// <summary>
        /// Altera um registro de fornecedor.
        /// </summary>
        ///
        /// <param name="fornecedor">
        /// O(a) fornecedor.
        /// </param>
        /// <param name="empresasNew">
        /// O(a) empresas new.
        /// </param>
        /// <param name="empresasDel">
        /// O(a) empresas delete.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Alterar(Fornecedor fornecedor, Empresa[] empresasNew, Empresa[] empresasDel)
        {
            IRepositorioFornecedor repFornecedor = Fabrica.Instancia.ObterRepositorio<IRepositorioFornecedor>(UnidadeTrabalho);
            Fornecedor fornecedorA = repFornecedor.ObterPorId(fornecedor.Codigo);
            fornecedorA.NomeFantasia = fornecedor.NomeFantasia;
            fornecedorA.RazaoSocial = fornecedor.RazaoSocial;
            fornecedorA.TelefoneContato = fornecedor.TelefoneContato.ToString().Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "");
            fornecedorA.EmailContato = fornecedor.EmailContato;
            fornecedorA.NomeContato = fornecedor.NomeContato;
            repFornecedor.Validar(fornecedorA, EstadoObjeto.Alterado);

            if (empresasDel != null)
            {
                foreach (var item in fornecedorA.Empresas.Where(t => empresasDel.Any(tdd => tdd.Codigo == t.Empresa.Codigo)))
                {
                    FornecedorEmpresa forne = item;
                    forne.Inativar();
                }
            }
            if (empresasNew != null)
            {
                foreach (var item in empresasNew)
                {
                    FornecedorEmpresa fe = new FornecedorEmpresa();
                    fe.Fornecedor = fornecedor;
                    fe.Empresa = new Empresa { Codigo = item.Codigo };
                    fe.DataRegistro = DateTime.Now;
                    fe.Ativar();
                    repFornecedor.SalvarFornecedorEmpresa(fe);
                }
            }

            return Json(montaObj(fornecedorA));
        }

        /// <summary>
        /// Ativar inativar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult AtivarInativar(int codigo)
        {
            IRepositorioFornecedor repFornecedor = Fabrica.Instancia.ObterRepositorio<IRepositorioFornecedor>(UnidadeTrabalho);
            Fornecedor fornecedor = repFornecedor.ObterPorId(codigo);
            if (fornecedor.Ativo)
            {
                fornecedor.Inativar();
            }
            else
            {
                repFornecedor.Validar(fornecedor, EstadoObjeto.Ativado);
                fornecedor.Ativar();
            }
            return Json(montaObj(fornecedor));
        }

        /// <summary>
        /// Selecionar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Selecionar(int codigo)
        {
            IRepositorioFornecedor repFornecedor = Fabrica.Instancia.ObterRepositorio<IRepositorioFornecedor>(UnidadeTrabalho);
            Fornecedor fornecedor = repFornecedor.ObterPorId(codigo);
            return Json(montaObj(fornecedor));
        }

        /// <summary>
        /// Retorna todos os objetos do tipo Fornecedor.
        /// </summary>
        ///
        /// <param name="apenasAtivos">
        /// true to apenas ativos.
        /// </param>
        /// <param name="cod_empresa">
        /// O(a) cod empresa.
        /// </param>
        ///
        /// <returns>
        /// Detalhes do tipo de objeto ligado ao controller no formato JSON.
        /// </returns>
        public ActionResult ObterTodos(bool apenasAtivos = false, string cod_empresa = null)
        {
            IRepositorioFornecedor repFornecedor = Fabrica.Instancia.ObterRepositorio<IRepositorioFornecedor>(UnidadeTrabalho);

            IQueryable<Fornecedor> fornecedores = repFornecedor.ObterTodos();

            if (apenasAtivos)
            {
                fornecedores = fornecedores.Where(t => t.Ativo == true);
            }
            if (!string.IsNullOrEmpty(cod_empresa))
            {
                fornecedores = fornecedores.Where(t => t.Empresas.Any(p => p.Empresa.Codigo == cod_empresa));
            }

            return Json(fornecedores
                               .Select(a => new
                                {
                                    Codigo = a.Codigo,
                                    CNPJ = Convert.ToUInt64(a.CNPJ).ToString(@"00\.000\.000\/0000\-00"),
                                    NomeFantasia = a.NomeFantasia,
                                    RazaoSocial = a.RazaoSocial,
                                    RazaoSocialCNPJ = a.RazaoSocial + " (" + a.CNPJ + ")",                                   
                                    NomeContato = a.NomeContato,
                                    TelefoneContato = a.TelefoneContato,
                                    EmailContato = a.EmailContato,
                                    Ativo = a.Ativo
                                })
                                .ToList().OrderBy(p => p.NomeFantasia));
        }

        /// <summary>
        /// Cria um objeto contendo informações relacionadas ao conteudo gerenciado por
        /// FornecedorController.
        /// </summary>
        ///
        /// <param name="fornecedor">
        /// O(a) fornecedor.
        /// </param>
        ///
        /// <returns>
        /// Um(a) object.
        /// </returns>
        private object montaObj(Fornecedor fornecedor)
        {
            return new
            {
                Codigo = fornecedor.Codigo,
                CNPJ = Convert.ToUInt64(fornecedor.CNPJ).ToString(@"00\.000\.000\/0000\-00"),
                NomeFantasia = fornecedor.NomeFantasia,
                RazaoSocial = fornecedor.RazaoSocial,
                NomeContato = fornecedor.NomeContato,
                TelefoneContato = fornecedor.TelefoneContato,
                EmailContato = fornecedor.EmailContato,
                Ativo = fornecedor.Ativo,
                Empresas = fornecedor.Empresas
                                     .Select(f => new EmpresaModelView()
                                     {
                                         Codigo = f.Empresa.Codigo,
                                         CNPJ = f.Empresa.CNPJ,
                                         Apelido = f.Empresa.Apelido,
                                         Nome = f.Empresa.Nome
                                     }).ToList(),

            };
        }

        /// <summary>
        /// Pesquisar.
        /// </summary>
        ///
        /// <param name="cnpj">
        /// O(a) cnpj.
        /// </param>
        /// <param name="nome">
        /// O(a) nome.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Pesquisar(string cnpj = null, string nome = null)
        {
            IRepositorioFornecedor rep = Fabrica.Instancia.ObterRepositorio<IRepositorioFornecedor>(UnidadeTrabalho);
            cnpj = new string(cnpj.Where(c => char.IsDigit(c)).ToArray());

            IQueryable<Fornecedor> fornecedor = rep.ObterTodos();

            if (!string.IsNullOrEmpty(nome))
                fornecedor = fornecedor.Where(p => p.RazaoSocial.Contains(nome));
            if (!string.IsNullOrEmpty(cnpj))
                fornecedor = fornecedor.Where(p => p.CNPJ == cnpj);

            return Json(fornecedor.Select(a => new FornecedorModelView()
            {
                Codigo = a.Codigo,
                CNPJ = Convert.ToUInt64(a.CNPJ).ToString(@"00\.000\.000\/0000\-00"),
                NomeFantasia = a.NomeFantasia,
                RazaoSocial = a.RazaoSocial,
                NomeContato = a.NomeContato,
                TelefoneContato = a.TelefoneContato,
                EmailContato = a.EmailContato,
                Ativo = a.Ativo
            }).ToList().Take(100).OrderBy(p => p.NomeFantasia), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Obter por cnpj.
        /// </summary>
        ///
        /// <param name="cnpj">
        /// O(a) cnpj.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ObterPorCnpj(string cnpj = null)
        {
            IRepositorioFornecedor repFornecedor = Fabrica.Instancia.ObterRepositorio<IRepositorioFornecedor>(UnidadeTrabalho);

            if (cnpj != null)
                cnpj = cnpj.Replace("-", "").Replace(".", "").Replace("/", "");

            Fornecedor Fornecedor = repFornecedor.ObterPorCnpj(cnpj);

            if (Fornecedor != null)
                return Json(montaObj(Fornecedor));

            return Json(null);
        }
    }
}
