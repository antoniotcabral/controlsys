﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls.WebParts;
using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Pessoas;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Acesso;
using Controlsys.Repositorios.Empresas;
using Ext.Net;
using Globalsys;
using Globalsys.Relatorios;
using Microsoft.Reporting.WebForms;
using Microsoft.SqlServer.Server;
using Ext.Net.MVC;
using System.Data;
using Globalsys.Exceptions;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class RelatorioPessoaCriticaFotoController : Controller
    {

        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        public RelatorioPessoaCriticaFotoController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Exportar(List<string> selectedFields, string tipoRelatorio, DateTime dataInicial, DateTime dataFinal, string cpf = null, string passaporte = null, string nome = null)
        {
            LocalReport relatorio = new LocalReport();
            relatorio.ReportPath = Server.MapPath("~/Reports/RelatorioPessoaCriticaFoto.rdlc");
            var regs = Filtrar(dataInicial, dataFinal, cpf, passaporte, nome);
            List<RelatorioPessoaCriticaFotoModelView> lista = montaObj(regs);

            //relatorio.DataSources.Add(new ReportDataSource("DTORelatorioPessoaCriticaFoto", lista));
            //relatorio.DataSources.Add(new ReportDataSource(string.Format("{0}DTORelatorioPessoaCriticaFoto", path ?? string.Empty), lista));
            relatorio.DataSources.Add(new ReportDataSource("DTORelatorioPessoaCriticaFoto", lista));
            relatorio.SetParameters(new ReportParameter("DataInicial", dataInicial.ToString()));
            relatorio.SetParameters(new ReportParameter("DataFinal", dataFinal.ToString()));
            relatorio.SetParameters(new ReportParameter("bo_colCpf", selectedFields.Contains("Cpf") ? "false" : "true"));
            relatorio.SetParameters(new ReportParameter("bo_colPassaporte", selectedFields.Contains("Passaporte") ? "false" : "true"));
            relatorio.SetParameters(new ReportParameter("bo_colNome", selectedFields.Contains("Nome") ? "false" : "true"));
            relatorio.SetParameters(new ReportParameter("bo_colRg", selectedFields.Contains("RG") ? "false" : "true"));
            relatorio.SetParameters(new ReportParameter("bo_colPapel", selectedFields.Contains("Papel") ? "false" : "true"));
            relatorio.SetParameters(new ReportParameter("bo_colCodigoPapel", selectedFields.Contains("CodigoPapel") ? "false" : "true"));
            relatorio.SetParameters(new ReportParameter("bo_colNomeEmpresa", selectedFields.Contains("NomeEmpresa") ? "false" : "true"));
            relatorio.SetParameters(new ReportParameter("bo_colCnpjEmpresa", selectedFields.Contains("CnpjEmpresa") ? "false" : "true"));
            relatorio.SetParameters(new ReportParameter("bo_colNomeContratada", selectedFields.Contains("NomeContratada") ? "false" : "true"));
            relatorio.SetParameters(new ReportParameter("bo_colCnpjContratada", selectedFields.Contains("CnpjContratada") ? "false" : "true"));
            relatorio.SetParameters(new ReportParameter("bo_colMotivoBloqueio", selectedFields.Contains("MotivoBloqueio") ? "false" : "true"));
            relatorio.SetParameters(new ReportParameter("bo_colDataBloqueio", selectedFields.Contains("DataBloqueio") ? "false" : "true"));
            relatorio.SetParameters(new ReportParameter("bo_colObservacao", selectedFields.Contains("Observacao") ? "false" : "true"));

            string mimeType;
            string fileName = string.Empty;
            switch (tipoRelatorio)
            {
                case "PDF":
                    mimeType = "application/pdf";
                    fileName = "download.pdf";
                    break;
                case "Excel":
                    mimeType = "application/vnd.ms-excel";
                    fileName = "download.xls";
                    break;
                case "Word":
                    mimeType = "application/msword";
                    fileName = "download.doc";
                    break;
            }

            string encoding;
            string fileNameExtension;

            Warning[] warnings;
            string[] streams;
            byte[] bytes;

            bytes = relatorio.Render(
            tipoRelatorio,
            null,
            out mimeType,
            out encoding,
            out fileNameExtension,
            out streams,
            out warnings);

            return File(bytes, mimeType, fileName);
        }

        private IQueryable<PessoaCritica> Filtrar(DateTime? dataInicial = null, DateTime? dataFinal = null, string cpf = null, string passaporte = null, string nome = null)
        {
            var rep = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaCritica>(UnidadeTrabalho);

            IQueryable<PessoaCritica> lista = rep.ObterTodos();

            if (!dataInicial.HasValue || !dataFinal.HasValue)
                throw new CoreException("Filtro de data Obrigatório");
            else
                lista = lista.Where(pc => (pc.DataBloqueio.Date >= dataInicial.Value.Date) && (pc.DataBloqueio.Date <= dataFinal.Value.Date));

            if (!string.IsNullOrEmpty(cpf))
            {
                cpf = cpf.Replace(".", "").Replace("-", "");
                lista = lista.Where(pc => pc.PessoaFisica.CPF == cpf);
            }

            if (!string.IsNullOrEmpty(passaporte))
                lista = lista.Where(pc => pc.PessoaFisica.Passaporte == passaporte);

            if (!string.IsNullOrEmpty(nome))
                lista = lista.Where(pc => pc.PessoaFisica.Nome.Contains(nome));

            return lista;
        }

        private List<RelatorioPessoaCriticaFotoModelView> montaObj(IQueryable<PessoaCritica> lista, StoreRequestParameters parameters = null, bool ignoreParameters = false)
        {
            //if (!ignoreParameters)
            //{
            //    if (parameters == null)
            //        parameters = new StoreRequestParameters();
            //    lista = lista.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 30);
            //}

            IUnidadeTrabalho unidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repAloc = Fabrica.Instancia.ObterRepositorio<IRepositorioAlocacaoColaborador>(unidadeTrabalho);
            var listaAux = new List<RelatorioPessoaCriticaFotoModelView>();
            foreach (var pc in lista)
            {
                PessoaFisica pf = pc.PessoaFisica;
                Papel papel = pf.ObterPapel();
                Empresa empresa = null;
                AlocacaoColaborador aloc = null;
                Empresa contratada = null;

                if (papel != null)
                {
                    empresa = (papel is Colaborador) ? (papel as Colaborador).Empresa :
                                        (papel is PrestadorServico) ? (papel as PrestadorServico).Empresa : null;

                    aloc = repAloc.ObterAlocacao(papel.Codigo);
                    contratada = (papel is Colaborador) ? (aloc != null) ? aloc.ObterContratada().Empresa : null : null;
                }

                listaAux.Add(new RelatorioPessoaCriticaFotoModelView()
                {
                    Foto = pf.Foto != null ? Convert.ToBase64String(pf.Foto) : null,
                    Cpf = pf.CPFFormatado(),
                    Passaporte = pf.Passaporte,
                    Nome = pf.Nome,
                    RG = pf.RG,
                    Papel = (papel is Colaborador) ? "Colaborador" :
                            (papel is PrestadorServico) ? "Prestador de Serviço" :
                            (papel is Visitante) ? "Colaborador" : 
                            "Sem Papel",
                    CodigoPapel = papel != null ? papel.Codigo.ToString() : string.Empty,
                    NomeEmpresa = (empresa != null) ? empresa.Apelido :
                                    (papel is Visitante) ? (papel as Visitante).Empresa : "",
                    CnpjEmpresa = (empresa != null) ? empresa.CNPJFormatado() : "",
                    NomeContratada = (contratada != null) ? contratada.Apelido : "",
                    CnpjContratada = (contratada != null) ? contratada.CNPJFormatado() : "",
                    MotivoBloqueio = pc.MotivoBloqueio.Nome,
                    DataBloqueio = pc.DataBloqueio,
                    Observacao = pc.Observacao
                });
            }

            return listaAux;
        }

    }
}
