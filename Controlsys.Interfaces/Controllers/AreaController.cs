﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Controlsys.Dominio.Empresas;
using Controlsys.Infra;
using Controlsys.Repositorios.Parametros;
using Ext.Net;
using Ext.Net.MVC;
using Globalsys;
using Ninject;
using Globalsys.Exceptions;
using Globalsys.Validacao;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar areas.
    /// </summary>
    [ExtendController]
    public class AreaController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /Area/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.AreaController.
        /// </summary>
        ///
        /// <param name="unidade">
        /// O(a) unidade.
        /// </param>
        public AreaController(IUnidadeTrabalho unidade)
        {
            UnidadeTrabalho = unidade;
        }

        /// <summary>
        /// Retorna a página de area.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [ExcludeFromCodeCoverage]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Inclui um novo area.
        /// </summary>
        ///
        /// <param name="area">
        /// O(a) area.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Incluir(Area area)
        {
            IRepositorioArea repArea = Fabrica.Instancia.ObterRepositorio<IRepositorioArea>(UnidadeTrabalho);
            area.DataRegistro = DateTime.Now;
            area.Ativar();   
            repArea.Validar(area, EstadoObjeto.Novo);
            repArea.Salvar(area);
            return Json(area);

        }

        /// <summary>
        /// Altera um registro de area.
        /// </summary>
        ///
        /// <param name="area">
        /// O(a) area.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Alterar(Area area)
        {
            IRepositorioArea repArea = Fabrica.Instancia.ObterRepositorio<IRepositorioArea>(UnidadeTrabalho);
            Area areaA = repArea.ObterPorId(area.Codigo);
            areaA.Numero = area.Numero;
            areaA.Nome = area.Nome;
            repArea.Validar(areaA, EstadoObjeto.Alterado);
            repArea.Atualizar(areaA);
            return Json(areaA);
        }

        /// <summary>
        /// Ativar inativar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult AtivarInativar(int codigo)
        {
            IRepositorioArea repArea = Fabrica.Instancia.ObterRepositorio<IRepositorioArea>(UnidadeTrabalho);
            Area area = repArea.ObterPorId(codigo);       
            if (area.Ativo)
            {
                area.Inativar();
            }
            else
            {
                repArea.Validar(area, EstadoObjeto.Ativado);
                area.Ativar();               
            }
            repArea.Atualizar(area);
            return Json(area);
        }

        /// <summary>
        /// Selecionar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Selecionar(int codigo)
        {
            IRepositorioArea repArea = Fabrica.Instancia.ObterRepositorio<IRepositorioArea>(UnidadeTrabalho);
            Area area = repArea.ObterPorId(codigo);
            return Json(area);
        }

        /// <summary>
        /// Retorna todos os objetos do tipo Area.
        /// </summary>
        ///
        /// <param name="apenasAtivos">
        /// true to apenas ativos.
        /// </param>
        ///
        /// <returns>
        /// Detalhes do tipo de objeto ligado ao controller no formato JSON.
        /// </returns>
        public ActionResult ObterTodos(bool apenasAtivos = false, string nome = "")
        {
            IRepositorioArea repArea = Fabrica.Instancia.ObterRepositorio<IRepositorioArea>(UnidadeTrabalho);

            IQueryable<Area> areas = repArea.ObterTodos();

            if (apenasAtivos)
                areas = areas.Where(t => t.Ativo == true);

            if (!string.IsNullOrEmpty(nome))
                areas = areas.Where(a => a.Nome.Contains(nome));

            return Json(areas.Select(a => new
                                {
                                    a.Codigo,
                                    a.Nome,
                                    a.Numero,
                                    a.Ativo
                                }).OrderBy(a => a.Nome)
                                .ToList());
        }

    }
}
