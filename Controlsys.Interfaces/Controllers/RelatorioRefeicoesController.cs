﻿using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Parametros;
using Controlsys.Repositorios.Empresas;
using Controlsys.Repositorios.Parametros;
using Controlsys.Repositorios.Pessoas;
using Ext.Net;
using Ext.Net.MVC;
using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Globalsys.Relatorios;
using Controlsys.Dominio.Acesso;
using Controlsys.Repositorio.Acesso;
using Globalsys.Exceptions;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class RelatorioRefeicoesController : Controller, IControllerBase
    {
        const string KEY_RELATORIO_REFEICOES = "KEY_RELATORIO_REFEICOES";

        public ActionResult Index()
        {
            Session[KEY_RELATORIO_REFEICOES] = null;
            return View();
        }

        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        public RelatorioRefeicoesController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        public ActionResult Pesquisar(StoreRequestParameters parameters, string nome = null, string cracha = null, string cpfPassaporte = null, string empresa = null, int restaurante = 0, int tipoAlimentacao = 0, int setor = 0, DateTime? dataInicio = null, DateTime? dataFim = null, bool ignoreParameters = false)
        {
            empresa = Empresa.RetornaApenasNomeEmpresa(empresa);

            var aloc = FiltrarAlocacaoColaboradores(setor);
            IQueryable<ControleAcesso> listControleAc = FiltrarAcessos(aloc, nome, cracha, cpfPassaporte, empresa, restaurante, tipoAlimentacao, setor, dataInicio, dataFim);
            int total = listControleAc.ToList().Count;
            List<ControleAcessoRefeicaoModelView> data = montaObj(listControleAc, aloc, parameters, ignoreParameters);

            return new StoreResult(data, total);
        }

        private IQueryable<AlocacaoColaborador> FiltrarAlocacaoColaboradores(int setor = 0)
        {
            IRepositorioAlocacaoColaborador repAlocacao = Fabrica.Instancia.ObterRepositorio<IRepositorioAlocacaoColaborador>(UnidadeTrabalho);
            IQueryable<AlocacaoColaborador> aloc = repAlocacao.ObterTodos();

            if (setor > 0)
                aloc = aloc.Where(p => p.SetorCusto.Codigo == setor);

            return aloc;
        }

        private IQueryable<ControleAcesso> FiltrarAcessos(IQueryable<AlocacaoColaborador> aloc, string nome = null, string cracha = null, string cpfPassaporte = null, string empresa = null, int restaurante = 0, int tipoAlimentacao = 0, int setor = 0, DateTime? dataInicio = null, DateTime? dataFim = null)
        {
            IRepositorioControleAcesso rep = Fabrica.Instancia.ObterRepositorio<IRepositorioControleAcesso>(UnidadeTrabalho);

            cpfPassaporte = cpfPassaporte.Replace(".", "").Replace("-", "");
            string userLogado = HttpContext.User.Identity.Name;
            IQueryable<ControleAcesso> listControleAc = (setor > 0) ?
                rep.ObterListaAcessoRestaurantePorRefeicao(nome, cracha, cpfPassaporte, empresa, restaurante, tipoAlimentacao, setor, dataInicio, dataFim, aloc) :
                rep.ObterListaAcessoRestaurantePorRefeicao(nome, cracha, cpfPassaporte, empresa, restaurante, tipoAlimentacao, setor, dataInicio, dataFim);

            return listControleAc.OrderByDescending(c => c.DataAcesso);
        }

        private List<ControleAcessoRefeicaoModelView> montaObj(IQueryable<ControleAcesso> controleAcessos, IQueryable<AlocacaoColaborador> aloc, StoreRequestParameters parameters = null, bool ignoreParameters = false)
        {
            if (!ignoreParameters)
            {
                if (parameters == null)
                    parameters = new StoreRequestParameters();
                controleAcessos = controleAcessos.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 25);
            }

            List<ControleAcessoRefeicaoModelView> list = new List<ControleAcessoRefeicaoModelView>();

            foreach (var s in controleAcessos)
            {
                ControleAcessoRefeicaoModelView item = new ControleAcessoRefeicaoModelView();

                item.Codigo = s.Codigo;
                item.Cracha = (s.Cracha != null) ? s.Cracha.Numero : s.NumeroSerie;
                item.Leitora = s.Leitora.Descricao;
                item.Acesso = s.DataAcesso;
                item.DataHoraAcesso = s.DataAcesso.ToString("dd/MM/yyyy HH:mm:ss");;

                #region [Restaurante]
                string query = @"SELECT r.* FROM dbo.RESTAURANTE r
                    INNER JOIN dbo.GRUPO_LEITORA gl ON gl.CD_GRUPO_LEITORA = r.CD_GRUPO_LEITORA
                    INNER JOIN dbo.RESTAURANTE_TIPOALIMENTACAO rt ON rt.CD_RESTAURANTE = r.CD_RESTAURANTE
                    INNER JOIN dbo.LEITORA_GRUPO_LEITOR lgl ON lgl.CD_GRUPO = gl.CD_GRUPO_LEITORA
                    INNER JOIN dbo.LEITORA l ON l.CD_LEITORA = lgl.CD_LEITORA
                    INNER JOIN dbo.ACESSO a ON a.CD_LEITORA = l.CD_LEITORA
                    WHERE a.CD_ACESSO = :CD_ACESSO
                    GROUP BY r.CD_RESTAURANTE, r.CD_GRUPO_LEITORA, r.TX_NOME, r.BL_ATIVO, r.DT_REGISTRO, r.DT_DESATIVACAO";

                var parametersAux = new Dictionary<string, object>();
                parametersAux.Add("CD_ACESSO", s.Codigo);

                var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
                IQueryable<Restaurante> regsRestaurantes = unidTrabalho.ExecuteSql<Restaurante>(query, parametersAux, new List<Type>() { typeof(Restaurante) }).AsQueryable();

                Restaurante restaurante = regsRestaurantes.FirstOrDefault();
                if (restaurante != null)
                    item.Restaurante = restaurante.Nome; 
                #endregion

                #region [Tipo Alimentação]
               /* query = @"SELECT ta.* FROM dbo.TIPO_ALIMENTACAO ta
                    INNER JOIN dbo.RESTAURANTE_TIPOALIMENTACAO rt ON rt.CD_TIPOALIMENTACAO = ta.CD_TIPOALIMENTACAO
                    INNER JOIN dbo.ACESSOREST_RESTTIPOALIMENT ar ON ar.CD_RESTAURANTE_TIPOALIMENTACAO = rt.CD_RESTAURANTE_TIPOALIMENTACAO
                    INNER JOIN dbo.RESTAURANTE r ON r.CD_RESTAURANTE = rt.CD_RESTAURANTE
                    INNER JOIN dbo.GRUPO_LEITORA gl ON gl.CD_GRUPO_LEITORA = r.CD_GRUPO_LEITORA
                    INNER JOIN dbo.LEITORA_GRUPO_LEITOR lgl ON lgl.CD_GRUPO = gl.CD_GRUPO_LEITORA
                    INNER JOIN dbo.LEITORA l ON l.CD_LEITORA = lgl.CD_LEITORA
                    INNER JOIN dbo.ACESSO a ON a.CD_LEITORA = l.CD_LEITORA
                    WHERE a.CD_ACESSO = :CD_ACESSO
                    AND a.DT_LEITORA >= rt.DT_REGISTRO AND (a.DT_LEITORA <= rt.DT_DESATIVACAO OR rt.DT_DESATIVACAO IS NULL)
                    AND ((A.DT_LEITORA >= DATEADD(day, DATEDIFF(day, 0, A.DT_LEITORA), CONVERT(VARCHAR, rt.HR_INICIO, 120))
                        AND (rt.HR_FIM > rt.HR_INICIO AND A.DT_LEITORA < DATEADD(day, DATEDIFF(day, 0, A.DT_LEITORA), CONVERT(VARCHAR, rt.HR_FIM, 120))))
                        OR(rt.HR_FIM < rt.HR_INICIO AND A.DT_LEITORA >= DATEADD(day, DATEDIFF(day, 0, DATEADD(day,0,A.DT_LEITORA)), CONVERT(VARCHAR, rt.HR_INICIO, 120))
                            AND A.DT_LEITORA < DATEADD(day, DATEDIFF(day, 0, DATEADD(day,1,A.DT_LEITORA)), CONVERT(VARCHAR, rt.HR_FIM, 120)))
                        OR (rt.HR_FIM < rt.HR_INICIO AND A.DT_LEITORA >= DATEADD(day, DATEDIFF(day, 0, DATEADD(day,-1,A.DT_LEITORA)), CONVERT(VARCHAR, rt.HR_INICIO, 120))
                            AND A.DT_LEITORA < DATEADD(day, DATEDIFF(day, 0, DATEADD(day,0,A.DT_LEITORA)), CONVERT(VARCHAR, rt.HR_FIM, 120)))) 
                    GROUP BY ta.CD_TIPOALIMENTACAO, ta.TX_NOME, ta.BL_ATIVO, ta.DT_REGISTRO, ta.DT_DESATIVACAO, rt.DT_REGISTRO, rt.DT_DESATIVACAO;";
                */
                /*
                 query = @"SELECT ta.* FROM dbo.TIPO_ALIMENTACAO ta
                    INNER JOIN dbo.RESTAURANTE_TIPOALIMENTACAO rt ON rt.CD_TIPOALIMENTACAO = ta.CD_TIPOALIMENTACAO
                    INNER JOIN dbo.ACESSOREST_RESTTIPOALIMENT ar ON ar.CD_RESTAURANTE_TIPOALIMENTACAO = rt.CD_RESTAURANTE_TIPOALIMENTACAO
                    INNER JOIN dbo.RESTAURANTE r ON r.CD_RESTAURANTE = rt.CD_RESTAURANTE  
                    INNER JOIN dbo.GRUPO_LEITORA gl ON gl.CD_GRUPO_LEITORA = r.CD_GRUPO_LEITORA
                    INNER JOIN dbo.LEITORA_GRUPO_LEITOR lgl ON lgl.CD_GRUPO = gl.CD_GRUPO_LEITORA
                    INNER JOIN dbo.LEITORA l ON l.CD_LEITORA = lgl.CD_LEITORA
                    INNER JOIN dbo.ACESSO a ON a.CD_LEITORA = l.CD_LEITORA
                    WHERE a.CD_ACESSO = :CD_ACESSO 
                         AND (rt.DT_DESATIVACAO IS NULL AND A.DT_LEITORA >= rt.DT_REGISTRO OR
                         rt.DT_DESATIVACAO IS NOT NULL AND A.DT_LEITORA >= rt.DT_REGISTRO AND A.DT_LEITORA < rt.DT_DESATIVACAO) AND A.DT_LEITORA >= rt.DT_REGISTRO AND 
                         (A.DT_LEITORA <= rt.DT_DESATIVACAO OR
                         rt.DT_DESATIVACAO IS NULL) AND (A.DT_LEITORA >= DATEADD(day, DATEDIFF(day, 0, A.DT_LEITORA), CONVERT(VARCHAR, rt.HR_INICIO, 120)) AND rt.HR_FIM > rt.HR_INICIO AND 
                         A.DT_LEITORA < DATEADD(day, DATEDIFF(day, 0, A.DT_LEITORA), CONVERT(VARCHAR, rt.HR_FIM, 120)) OR
                         rt.HR_FIM < rt.HR_INICIO AND A.DT_LEITORA >= DATEADD(day, DATEDIFF(day, 0, DATEADD(day, 0, A.DT_LEITORA)), CONVERT(VARCHAR, rt.HR_INICIO, 120)) AND A.DT_LEITORA < DATEADD(day, 
                         DATEDIFF(day, 0, DATEADD(day, 1, A.DT_LEITORA)), CONVERT(VARCHAR, rt.HR_FIM, 120)) OR
                         rt.HR_FIM < rt.HR_INICIO AND A.DT_LEITORA >= DATEADD(day, DATEDIFF(day, 0, DATEADD(day, - 1, A.DT_LEITORA)), CONVERT(VARCHAR, rt.HR_INICIO, 120)) AND A.DT_LEITORA < DATEADD(day, 
                         DATEDIFF(day, 0, DATEADD(day, 0, A.DT_LEITORA)), CONVERT(VARCHAR, rt.HR_FIM, 120)))";
                */
                query = @"SELECT DISTINCT TA.* 
	                            FROM dbo.ACESSO A 
	                            INNER JOIN dbo.LEITORA l ON l.CD_LEITORA = A.CD_LEITORA  AND (L.DT_DESATIVACAO IS NULL AND A.DT_LEITORA >= L.DT_REGISTRO OR L.DT_DESATIVACAO IS NOT NULL AND A.DT_LEITORA >= L.DT_REGISTRO AND A.DT_LEITORA < L.DT_DESATIVACAO) 
	                            INNER JOIN dbo.LEITORA_GRUPO_LEITOR lgl ON lgl.CD_LEITORA = L.CD_LEITORA AND (LGL.DT_DESATIVACAO IS NULL AND A.DT_LEITORA >= LGL.DT_REGISTRO OR LGL.DT_DESATIVACAO IS NOT NULL AND A.DT_LEITORA >= LGL.DT_REGISTRO AND A.DT_LEITORA < LGL.DT_DESATIVACAO) 
	                            INNER JOIN dbo.GRUPO_LEITORA gl ON gl.CD_GRUPO_LEITORA = LGL.CD_GRUPO AND (GL.DT_DESATIVACAO IS NULL AND A.DT_LEITORA >= GL.DT_REGISTRO OR GL.DT_DESATIVACAO IS NOT NULL AND A.DT_LEITORA >= GL.DT_REGISTRO AND A.DT_LEITORA < GL.DT_DESATIVACAO) 
	                            INNER JOIN dbo.RESTAURANTE r ON r.CD_GRUPO_LEITORA = gl.CD_GRUPO_LEITORA AND R.DT_DESATIVACAO IS NULL AND (A.DT_LEITORA >= R.DT_REGISTRO OR R.DT_DESATIVACAO IS NOT NULL AND A.DT_LEITORA >= R.DT_REGISTRO AND A.DT_LEITORA < R.DT_DESATIVACAO) 
	                            INNER JOIN dbo.RESTAURANTE_TIPOALIMENTACAO rt ON r.CD_RESTAURANTE = rt.CD_RESTAURANTE 
		                            AND (RT.DT_DESATIVACAO IS NULL AND A.DT_LEITORA >= RT.DT_REGISTRO OR RT.DT_DESATIVACAO IS NOT NULL AND A.DT_LEITORA >= RT.DT_REGISTRO AND A.DT_LEITORA < RT.DT_DESATIVACAO) 
		                            AND A.DT_LEITORA >= RT.DT_REGISTRO AND (A.DT_LEITORA <= RT.DT_DESATIVACAO OR	RT.DT_DESATIVACAO IS NULL) 
		                            AND ( 
				                            A.DT_LEITORA >= DATEADD(day, DATEDIFF(day, 0, A.DT_LEITORA), CONVERT(VARCHAR, RT.HR_INICIO, 120)) AND RT.HR_FIM > RT.HR_INICIO AND 
				                            A.DT_LEITORA < DATEADD(day, DATEDIFF(day, 0, A.DT_LEITORA), CONVERT(VARCHAR, RT.HR_FIM, 120)) OR 
				                            RT.HR_FIM < RT.HR_INICIO AND A.DT_LEITORA >= DATEADD(day, DATEDIFF(day, 0, DATEADD(day, 0, A.DT_LEITORA)), CONVERT(VARCHAR, RT.HR_INICIO, 120)) AND 
				                            A.DT_LEITORA < DATEADD(day, DATEDIFF(day, 0, DATEADD(day, 1, A.DT_LEITORA)), CONVERT(VARCHAR, RT.HR_FIM, 120)) OR 
				                            RT.HR_FIM < RT.HR_INICIO AND A.DT_LEITORA >= DATEADD(day, DATEDIFF(day, 0, DATEADD(day, - 1, A.DT_LEITORA)), CONVERT(VARCHAR, RT.HR_INICIO, 120)) AND 
				                            A.DT_LEITORA < DATEADD(day, DATEDIFF(day, 0, DATEADD(day, 0, A.DT_LEITORA)), CONVERT(VARCHAR, RT.HR_FIM, 120)) 
			                            ) 
	                            INNER JOIN dbo.ACESSOREST_RESTTIPOALIMENT ar ON ar.CD_RESTAURANTE_TIPOALIMENTACAO = rt.CD_RESTAURANTE_TIPOALIMENTACAO 
	                            INNER JOIN dbo.TIPO_ALIMENTACAO ta ON TA.CD_TIPOALIMENTACAO = RT.CD_TIPOALIMENTACAO AND (RT.DT_DESATIVACAO IS NULL AND A.DT_LEITORA >= RT.DT_REGISTRO OR RT.DT_DESATIVACAO IS NOT NULL AND A.DT_LEITORA >= RT.DT_REGISTRO AND A.DT_LEITORA < RT.DT_DESATIVACAO) 
                                WHERE a.CD_ACESSO = :CD_ACESSO ";

                parametersAux = new Dictionary<string, object>();
                parametersAux.Add("CD_ACESSO", s.Codigo);

                unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
                IQueryable<TipoAlimentacao> regsTipoAlimentacao = unidTrabalho.ExecuteSql<TipoAlimentacao>(query, parametersAux, new List<Type>() { typeof(TipoAlimentacao) }).AsQueryable();
                TipoAlimentacao tipoAlimentacao = regsTipoAlimentacao.FirstOrDefault();
                if (tipoAlimentacao != null)
                    item.TipoAlimentacao = tipoAlimentacao.Nome; 
                #endregion

                item.Controladora = s.Leitora.Controladora.IP;
                item.Leitora = s.Leitora.Descricao;

                if (s.Papel != null)
                {
                    item.Nome = s.Papel.PessoaFisica.Nome;
                    item.CPFPassaporte = s.Papel.PessoaFisica.Passaporte ?? s.Papel.PessoaFisica.CPFFormatado();

                    if (s.Papel is Colaborador)
                    {
                        item.EmpresaCNPJ = ((Colaborador)s.Papel).Empresa.CNPJFormatado();
                        item.EmpresaNomeFantasia = ((Colaborador)s.Papel).Empresa.Apelido;
                    }

                    if (s.Papel is PrestadorServico)
                    {
                        if (((PrestadorServico)s.Papel).TipoPrestador == TipoPrestador.Temporario)
                        {
                            if (((PrestadorServico)s.Papel).Empresa != null)
                            {
                                item.EmpresaCNPJ = ((PrestadorServico)s.Papel).Empresa.CNPJFormatado();
                                item.EmpresaNomeFantasia = ((PrestadorServico)s.Papel).Empresa.Apelido;
                            }
                        }
                        else
                        {
                            if (((PrestadorServico)s.Papel).Fornecedor != null)
                            {
                                item.EmpresaCNPJ = ((PrestadorServico)s.Papel).Fornecedor.CNPJFormatado();
                                item.EmpresaNomeFantasia = ((PrestadorServico)s.Papel).Fornecedor.NomeFantasia;
                            }
                        }
                    }

                    if (s.Papel is Visitante)
                    {
                        item.EmpresaNomeFantasia = ((Visitante)s.Papel).Empresa;
                    }

                    AlocacaoColaborador alocacao = aloc.Where(a => a.Papel.Codigo == s.Papel.Codigo).OrderByDescending(a => a.DataRegistro).FirstOrDefault();

                    if (alocacao != null)
                    {
                        item.SetorCusto = alocacao.SetorCusto != null ? alocacao.SetorCusto.Nome : "";
                        item.NumeroPO = alocacao.PedidoCompra.Numero; 
                        item.ContratadaCNPJ = alocacao.PedidoCompra.PedidoCompraPai != null ? alocacao.PedidoCompra.PedidoCompraPai.Empresa.CNPJFormatado() :
                                          alocacao.PedidoCompra.PedidoCompraPai == null ? alocacao.PedidoCompra.Empresa.CNPJFormatado() : "";

                        item.ContratadaNomeFantasia = alocacao.PedidoCompra.PedidoCompraPai != null ? alocacao.PedidoCompra.PedidoCompraPai.Empresa.Apelido :
                                          alocacao.PedidoCompra.PedidoCompraPai == null ? alocacao.PedidoCompra.Empresa.Apelido : "";


                    }
                }

                list.Add(item);
            }

            return list;
        }

        public ActionResult Exportar(StatusPapelLog? status, List<string> selectedFields, string tipoRelatorio, string nome = null, string cracha = null, string cpfPassaporte = null, string empresa = null, int restaurante = 0, int tipoAlimentacao = 0, int setor = 0, DateTime? dataInicio = null, DateTime? dataFim = null)
        {
            empresa = Empresa.RetornaApenasNomeEmpresa(empresa);
            try
            {
                Session[KEY_RELATORIO_REFEICOES] = Guid.NewGuid().ToString();

                var aloc = FiltrarAlocacaoColaboradores(setor);
                var listControleAc = FiltrarAcessos(aloc, nome, cracha, cpfPassaporte, empresa, restaurante, tipoAlimentacao, setor, dataInicio, dataFim);

                List<ControleAcessoRefeicaoModelView> controleAcs = montaObj(listControleAc, aloc, null, true);

                //if (!controleAcs.Any())
                //    controleAcs.Add(new ControleAcessoRefeicaoModelView());

                ReportConfig reportConfig = ReportHelper.RenderReport<ControleAcessoRefeicaoModelView>(selectedFields, tipoRelatorio, controleAcs);

                string fileName = string.Empty;

                switch (tipoRelatorio)
                {
                    case "PDF":
                        fileName = "download.pdf";
                        break;
                    case "EXCELOPENXML":
                        fileName = "download.xlsx";
                        break;
                    case "WORDOPENXML":
                        fileName = "download.docx";
                        break;
                }

                Session[KEY_RELATORIO_REFEICOES] = null;

                return File(reportConfig.Bytes, reportConfig.MimeType, fileName);
            }
            catch (Exception ex)
            {
                Session[KEY_RELATORIO_REFEICOES] = null;

                throw new CoreException(ex.Message);
            }
            
        }

        [AllowAnonymous]
        public ActionResult ExisteRelatorioPendente()
        {
            var result = Session[KEY_RELATORIO_REFEICOES] != null;

            return Json(result);
        }
    }
}