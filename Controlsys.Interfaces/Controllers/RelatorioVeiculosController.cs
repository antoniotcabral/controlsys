﻿using Controlsys.Dominio.CredenciamentoVeiculos;
using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Pessoas;
using Controlsys.Dominio.Relatorio;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Parametros;
using Controlsys.Repositorio.Relatorio;
using Ext.Net;
using Ext.Net.MVC;
using Globalsys;
using Globalsys.Relatorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class RelatorioVeiculosController : Controller, IControllerBase
    {

        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        public RelatorioVeiculosController(IUnidadeTrabalho unidade) { UnidadeTrabalho = unidade; }

        public ActionResult Index()
        { return View(); }

        public ActionResult Exportar(List<string> selectedFields, string tipoRelatorio, string empresa = null, string condutor = null, string placa = null, int? modeloCredencial = null,
            DateTime? dataValidadeInicial = null, DateTime? dataValidadeFinal = null, DateTime? dataInspecaoInicial = null, DateTime? dataInspecaoFinal = null)
        {
            empresa = Empresa.RetornaApenasNomeEmpresa(empresa);

            var list = FiltrarRegistros(empresa, condutor, placa, modeloCredencial, dataValidadeInicial, dataValidadeFinal, dataInspecaoInicial, dataInspecaoFinal);
            var veiculos = montaObj(list, null, true);

            ReportConfig reportConfig = ReportHelper.RenderReport<RelatorioVeiculosModelView>(selectedFields, tipoRelatorio, veiculos);

            string fileName = string.Empty;

            switch (tipoRelatorio)
            {
                case "PDF":
                    fileName = "download.pdf";
                    break;
                case "Excel":
                    fileName = "download.xls";
                    break;
                case "Word":
                    fileName = "download.doc";
                    break;
            }

            return File(reportConfig.Bytes, reportConfig.MimeType, fileName);
        }

        public ActionResult Pesquisar(StoreRequestParameters parameters, string empresa = null, string condutor = null, string placa = null, int? modeloCredencial = null, 
            DateTime? dataValidadeInicial = null, DateTime? dataValidadeFinal = null, DateTime? dataInspecaoInicial = null, DateTime? dataInspecaoFinal = null)
        {
            empresa = Empresa.RetornaApenasNomeEmpresa(empresa);

            IQueryable<CredencialVeiculo> regs = FiltrarRegistros(empresa, condutor, placa, modeloCredencial, dataValidadeInicial, dataValidadeFinal, dataInspecaoInicial, dataInspecaoFinal);
            int total = regs.Count();
            List<RelatorioVeiculosModelView> data = montaObj(regs, parameters);

            return new StoreResult(data, total);
        }

        private IQueryable<CredencialVeiculo> FiltrarRegistros(string empresa = null, string condutor = null, string placa = null, int? modeloCredencial = null, 
            DateTime? dataValidadeInicial = null, DateTime? dataValidadeFinal = null, DateTime? dataInspecaoInicial = null, DateTime? dataInspecaoFinal = null)
        {
            IRepositorioCredencialVeiculo repRelatorio = Fabrica.Instancia.ObterRepositorio<IRepositorioCredencialVeiculo>(UnidadeTrabalho);
            var registros = repRelatorio.ObterTodos();

            if (dataValidadeInicial.HasValue)
                registros = registros.Where(r => r.DataVencimento.Value.Date >= dataValidadeInicial.Value.Date);
            if (dataValidadeFinal.HasValue)
                registros = registros.Where(r => r.DataVencimento.Value.Date <= dataValidadeFinal.Value.Date);

            if (dataInspecaoInicial.HasValue)
                registros = registros.Where(r => r.Situacoes.Where(s=> s.Status == StatusCredencial.Inspecionada).Select(scv => scv.DataRegistro).FirstOrDefault() >= dataInspecaoInicial);
            if (dataInspecaoFinal.HasValue)
                registros = registros.Where(r => r.Situacoes.Where(s => s.Status == StatusCredencial.Inspecionada).Select(scv => scv.DataRegistro).FirstOrDefault() <= dataInspecaoFinal);

            if (!string.IsNullOrEmpty(empresa))
                registros = registros.Where(r => r.Empresa.Nome.Contains(empresa));

            if (!string.IsNullOrEmpty(condutor))
                registros = registros.Where(r => r.Condutor.PessoaFisica.Nome.Contains(condutor));

            if (!string.IsNullOrEmpty(placa))
                registros = registros.Where(sc => sc.Veiculos.Any(v => v.Veiculo.Placa.Contains(placa)));

            if (modeloCredencial.HasValue)
                registros = registros.Where(r => r.ModeloCredencial.Codigo == modeloCredencial);

            return registros;
        }

        private List<RelatorioVeiculosModelView> montaObj(IQueryable<CredencialVeiculo> regs, StoreRequestParameters parameters = null, bool ignoreParameters = false)
        {
            if (!ignoreParameters)
            {
                if (parameters == null)
                    parameters = new StoreRequestParameters();
                regs = regs.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 25);
            }

            List <RelatorioVeiculosModelView> lista = new List<RelatorioVeiculosModelView>();
            foreach (var r in regs.ToList())
            {
                var regAux = new RelatorioVeiculosModelView();

                //Dados gerais
                regAux.Codigo = r.Codigo;
                regAux.DataRegistro = r.DataRegistro;
                regAux.UsuarioNome = r.Usuario.PessoaFisica.Nome;
                regAux.DataValidade = r.DataVencimento;

                //Empresa
                regAux.EmpresaNome = r.Empresa.Nome;
                regAux.PedidoCompraNumeroNome = r.PedidoCompra != null ? string.Format("{0} {1}", r.PedidoCompra.Numero, r.PedidoCompra.Nome) : null;
                regAux.AreaNome = r.Area != null ? r.Area.Nome : null;
                regAux.SubContratadaNome = r.SubContratada != null ? r.SubContratada.Nome : null;
                regAux.Fornecedor = r.Fornecedor;

                //Condutor
                regAux.CondutorNome = r.Condutor.PessoaFisica.Nome;
                regAux.CondutorCPF = r.Condutor.PessoaFisica.CPF;
                regAux.CondutorPassaporte = r.Condutor.PessoaFisica.Passaporte;
                regAux.CondutorCargoNome = (r.Condutor is Colaborador) ? (r.Condutor as Colaborador).Cargo.Nome
                    : ((r.Condutor is PrestadorServico) ? (r.Condutor as PrestadorServico).Cargo.Nome
                        : ((r.Condutor is Visitante) ? (r.Condutor as Visitante).Cargo.ToString() : null));
                regAux.CondutorEmpresaNome = (r.Condutor is Colaborador) ? (r.Condutor as Colaborador).Empresa.Nome
                    : ((r.Condutor is PrestadorServico) ? (r.Condutor as PrestadorServico).Empresa.Nome
                        : ((r.Condutor is Visitante) ? (r.Condutor as Visitante).Empresa.ToString() : null));
                regAux.CondutorCNH = r.Condutor.PessoaFisica.CNH;
                regAux.CondutorCNHVencimento = r.Condutor.PessoaFisica.CNHDataValidade.HasValue
                        ? r.Condutor.PessoaFisica.CNHDataValidade.Value : (DateTime?)null;

                //Modelo Credencial
                regAux.TipoCredencialNome = r.ModeloCredencial.TipoCredencial.ObterDescricaoEnum();
                regAux.ModeloCredencialNome = r.ModeloCredencial.Nome;
                regAux.TipoValidadeNome = r.TipoValidade.Nome;
                regAux.ValidadeDiasCredencial = r.ValidadeDiasCredencial.HasValue ? r.ValidadeDiasCredencial.Value : (int?)null;
                regAux.DataValidadeLit = r.DataValidadeLit.HasValue ? r.DataValidadeLit.Value : (DateTime?)null;
                regAux.DataVencimentoSeguro = r.DataVencimentoSeguro;
                regAux.Corporativo = r.Corporativo ? "Sim" : "Não";
                regAux.Observacao = r.Observacao;

                //Veículo 1
                regAux.Veiculo1Placa = r.Veiculos[0].Veiculo.Placa;
                regAux.Veiculo1Fabricante = r.Veiculos[0].Veiculo.Modelo.Fabricante.Nome;
                regAux.Veiculo1Modelo = r.Veiculos[0].Veiculo.Modelo.Nome;
                regAux.Veiculo1AnoFabricacao = r.Veiculos[0].Veiculo.AnoFabricacao;
                regAux.Veiculo1AnoModelo = r.Veiculos[0].Veiculo.AnoModelo;
                regAux.Veiculo1Cor = r.Veiculos[0].Veiculo.Cor;

                //Veículo 2
                regAux.Veiculo2Placa = r.Veiculos.Count > 1 ? r.Veiculos[1].Veiculo.Placa : null;
                regAux.Veiculo2Fabricante = r.Veiculos.Count > 1 ? r.Veiculos[1].Veiculo.Modelo.Fabricante.Nome : null;
                regAux.Veiculo2Modelo = r.Veiculos.Count > 1 ? r.Veiculos[1].Veiculo.Modelo.Nome : null;
                regAux.Veiculo2AnoFabricacao = r.Veiculos.Count > 1 ? r.Veiculos[1].Veiculo.AnoFabricacao : (int?)null;
                regAux.Veiculo2AnoModelo = r.Veiculos.Count > 1 ? r.Veiculos[1].Veiculo.AnoModelo : (int?)null;
                regAux.Veiculo2Cor = r.Veiculos.Count > 1 ? r.Veiculos[1].Veiculo.Cor : null;

                //Situação
                regAux.SituacaoStatus = r.ObterSituacao().Status.ObterDescricaoEnum();
                regAux.SituacaoData = r.ObterSituacao().DataRegistro;
                regAux.SituacaoObservacao = r.ObterSituacao().Observacao;

                SituacaoCredencialVeiculo scvRecebida = r.Situacoes.Where(s => s.Status == StatusCredencial.Recebida).OrderByDescending(c => c.Codigo).FirstOrDefault();
                SituacaoCredencialVeiculo scvInspecionada = r.Situacoes.Where(s => s.Status == StatusCredencial.Inspecionada).OrderByDescending(c => c.Codigo).FirstOrDefault();
                SituacaoCredencialVeiculo scvAprovada = r.Situacoes.Where(s => s.Status == StatusCredencial.Aprovada).OrderByDescending(c => c.Codigo).FirstOrDefault();
                SituacaoCredencialVeiculo scvImpressa = r.Situacoes.Where(s => s.Status == StatusCredencial.Impressa).OrderByDescending(c => c.Codigo).FirstOrDefault();
                SituacaoCredencialVeiculo scvDevolvida = r.Situacoes.Where(s => s.Status == StatusCredencial.Devolvida).OrderByDescending(c => c.Codigo).FirstOrDefault();


                if (scvRecebida != null) regAux.DataRecebida = scvRecebida.DataRegistro;
                if (scvInspecionada != null) regAux.DataInspecionada = scvInspecionada.DataRegistro;
                if (scvAprovada != null) regAux.DataAprovada = scvAprovada.DataRegistro;
                if (scvImpressa != null) regAux.DataImpressa = scvImpressa.DataRegistro;
                if (scvDevolvida != null) regAux.DataDevolvida = scvDevolvida.DataRegistro;

                lista.Add(regAux);
            }

            return lista;

            //return regs.Select(r=> new RelatorioVeiculosModelView()
            //{
            //    //Dados gerais
            //    Codigo = r.Codigo,
            //    DataRegistro = r.DataRegistro,
            //    UsuarioNome = r.Usuario.PessoaFisica.Nome,
            //    DataValidade = r.DataVencimento,
            //
            //    //Empresa
            //    EmpresaNome = r.Empresa.Nome,
            //    PedidoCompraNumeroNome = string.Format("{0} {1}", r.PedidoCompra.Numero, r.PedidoCompra.Nome),
            //    AreaNome = r.Area.Nome,
            //    SubContratadaNome = r.SubContratada.Nome,
            //    Fornecedor = r.Fornecedor,
            //
            //    //Condutor
            //    CondutorNome = r.Condutor.PessoaFisica.Nome,
            //    CondutorCPF = r.Condutor.PessoaFisica.CPF,
            //    CondutorPassaporte = r.Condutor.PessoaFisica.Passaporte,
            //    CondutorCargoNome = (r.Condutor is Colaborador) ? (r.Condutor as Colaborador).Cargo.Nome
            //        : ((r.Condutor is PrestadorServico) ? (r.Condutor as PrestadorServico).Cargo.Nome
            //            : ((r.Condutor is Visitante) ? (r.Condutor as Visitante).Cargo.ToString() : null)),
            //    CondutorEmpresaNome = (r.Condutor is Colaborador) ? (r.Condutor as Colaborador).Empresa.Nome
            //        : ((r.Condutor is PrestadorServico) ? (r.Condutor as PrestadorServico).Empresa.Nome
            //            : ((r.Condutor is Visitante) ? (r.Condutor as Visitante).Empresa.ToString() : null)),
            //    CondutorCNH = r.Condutor.PessoaFisica.CNH,
            //    CondutorCNHVencimento = r.Condutor.PessoaFisica.CNHDataValidade.HasValue
            //            ? r.Condutor.PessoaFisica.CNHDataValidade.Value : (DateTime?)null,
            //
            //    //Modelo Credencial
            //    TipoCredencialNome = r.ModeloCredencial.TipoCredencial.ObterDescricaoEnum(),
            //    ModeloCredencialNome = r.ModeloCredencial.Nome,
            //    TipoValidadeNome = r.TipoValidade.Nome,
            //    ValidadeDiasCredencial = r.ValidadeDiasCredencial,
            //    DataValidadeLit = r.DataValidadeLit,
            //    DataVencimentoSeguro = r.DataVencimentoSeguro,
            //    Corporativo = r.Corporativo ? "Sim" : "Não",
            //    Observacao = r.Observacao,
            //
            //    //Veículo 1
            //    Veiculo1Placa = r.Veiculos[0].Veiculo.Placa,
            //    Veiculo1Fabricante = r.Veiculos[0].Veiculo.Modelo.Fabricante.Nome,
            //    Veiculo1Modelo = r.Veiculos[0].Veiculo.Modelo.Nome,
            //    Veiculo1AnoFabricacao = r.Veiculos[0].Veiculo.AnoFabricacao,
            //    Veiculo1AnoModelo = r.Veiculos[0].Veiculo.AnoModelo,
            //    Veiculo1Cor = r.Veiculos[0].Veiculo.Cor,
            //
            //    //Veículo 2
            //    Veiculo2Placa = r.Veiculos.Count > 1 ? r.Veiculos[1].Veiculo.Placa : null,
            //    Veiculo2Fabricante = r.Veiculos.Count > 1 ? r.Veiculos[1].Veiculo.Modelo.Fabricante.Nome : null,
            //    Veiculo2Modelo = r.Veiculos.Count > 1 ? r.Veiculos[1].Veiculo.Modelo.Nome : null,
            //    Veiculo2AnoFabricacao = r.Veiculos.Count > 1 ? r.Veiculos[1].Veiculo.AnoFabricacao : (int?)null,
            //    Veiculo2AnoModelo = r.Veiculos.Count > 1 ? r.Veiculos[1].Veiculo.AnoModelo : (int?)null,
            //    Veiculo2Cor = r.Veiculos.Count > 1 ? r.Veiculos[1].Veiculo.Cor : null,
            //
            //    //Situação
            //    SituacaoStatus = r.ObterSituacao().Status.ObterDescricaoEnum(),
            //    SituacaoData = r.ObterSituacao().DataRegistro,
            //    SituacaoObservacao = r.ObterSituacao().Observacao
            //
            //}).ToList();
        }

    }
}