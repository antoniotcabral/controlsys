﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Empresas;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Acesso;
using Controlsys.Repositorios.Empresas;
using Ext.Net;
using Globalsys;
using Globalsys.Relatorios;
using Ext.Net.MVC;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar relatorio homem horas.
    /// </summary>
    [ExtendController]
    public class RelatorioFTEController : Controller, IControllerBase
    {
        /// <summary>
        /// Gets/Sets valor para UnidadeTrabalho.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.RelatorioHomemHoraController.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RelatorioFTEController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        /// <summary>
        /// GET: /RelatorioHomemHora/.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Exportar.
        /// </summary>
        ///
        /// <param name="selectedFields">
        /// The selected fields.
        /// </param>
        /// <param name="tipoRelatorio">
        /// O(a) tipo relatorio.
        /// </param>
        /// <param name="responsavel">
        /// O(a) responsavel.
        /// </param>
        /// <param name="empresa">
        /// O(a) empresa.
        /// </param>
        /// <param name="setor">
        /// O(a) setor.
        /// </param>
        /// <param name="dataInicio">
        /// O(a) data inicio.
        /// </param>
        /// <param name="dataFim">
        /// O(a) data fim.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public void ExecutarCarga(DateTime? dataInicio)
        {
            Dictionary<string, object> dictionary = new Dictionary<string, object>();
            dictionary.Add("dtInicio_carga", dataInicio);
            
            UnidadeTrabalho.ExecuteProcedure("SP_REL_FTE_CARGA :dtInicio_carga",  dictionary);
        }
    }
}
