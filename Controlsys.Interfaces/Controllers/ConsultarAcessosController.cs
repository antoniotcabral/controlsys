﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Controlsys.Repositorio.Acesso;
using Controlsys.Infra;
using Ext.Net.MVC;
using Globalsys;
using Ext.Net;
using System.Xml.Xsl;
using System.Xml;
using System.Text;
using System.IO;
using System.Xml.XPath;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorios.Pessoas;
using Controlsys.Repositorios.Seguranca;
using Controlsys.Dominio.Seguranca;
using Controlsys.Dominio.Empresas;
using Controlsys.Repositorios.Empresas;
using Globalsys.Exceptions;
using NHibernate.Criterion;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar consultar acessos.
    /// </summary>
    [ExtendController]
    public class ConsultarAcessosController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /ConsultarAcessos/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.ConsultarAcessosController.
        /// </summary>
        ///
        /// <param name="unidade">
        /// O(a) unidade.
        /// </param>
        public ConsultarAcessosController(IUnidadeTrabalho unidade)
        {
            UnidadeTrabalho = unidade;
        }

        /// <summary>
        /// Retorna a página de consultarAcessos.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index()
        {
            return View();
        }

        //public ActionResult ObterTodos()
        //{
        //    IRepositorioControleAcesso rep = Fabrica.Instancia.ObterRepositorio<IRepositorioControleAcesso>(UnidadeTrabalho);
        //    IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);

        //    List<ControleAcesso> listControleAc = rep.ObterTodos().Take(100).OrderBy(p => p.DataRegistro).ToList();
        //    return Json(listControleAc.Select(s => new ControleAcessoModelView()
        //    {
        //        Codigo = s.Codigo,
        //        NumCracha = (s.Cracha != null) ? s.Cracha.Numero : s.NumeroSerie,
        //        Nome = (s.Papel != null) ? s.Papel.PessoaFisica.Nome : "",
        //        Papel = (s.Papel != null) ? (((s.Papel is Colaborador) ? "Colaborador" : ((s.Papel is PrestadorServico) ? "Prestador de Serviço" : "Visitante"))) : "",
        //        CPF = (s.Papel != null) ? s.Papel.PessoaFisica.CPFFormatado() : "",
        //        Empresa = (s.Papel != null) ? ((s.Papel.PessoaFisica.Papeis.OrderByDescending(pp => pp.Codigo).FirstOrDefault() is Colaborador) ?
        //                                                (s.Papel.PessoaFisica.Papeis.OrderByDescending(pp => pp.Codigo).Select(pp => ((Colaborador)pp).Empresa.Apelido).FirstOrDefault()) : "") : "",
        //        Cargo = (s.Papel != null) ? s.Papel.PessoaFisica.Papeis.OrderByDescending(pp => pp.Codigo).Select(pp => pp.Cargo.Nome).FirstOrDefault() : "",
        //        Leitora = s.Leitora.Descricao,
        //        DataHora = s.DataRegistro,
        //        StatusAcesso = s.StatusAcesso.ObterDescricaoEnum(),
        //        Local = s.Local,
        //        Direcao = s.Direcao.ObterDescricaoEnum()
        //    }).ToList(), JsonRequestBehavior.AllowGet);
        //}

        /// <summary>
        /// private.
        /// </summary>
        ///
        /// <param name="parameters">
        /// Options for controlling the operation.
        /// </param>
        /// <param name="tipoSetor">
        /// O(a) tipo setor.
        /// </param>
        /// <param name="status">
        /// O(a) status.
        /// </param>
        /// <param name="cracha">
        /// O(a) cracha.
        /// </param>
        /// <param name="cpf">
        /// O(a) cpf.
        /// </param>
        /// <param name="empresa">
        /// O(a) empresa.
        /// </param>
        /// <param name="nome">
        /// O(a) nome.
        /// </param>
        /// <param name="cargo">
        /// O(a) cargo.
        /// </param>
        /// <param name="dataInicio">
        /// O(a) data inicio.
        /// </param>
        /// <param name="dataFim">
        /// O(a) data fim.
        /// </param>
        /// <param name="setor">
        /// O(a) setor.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Pesquisar(StoreRequestParameters parameters, TipoSetorCusto? tipoSetor, List<StatusAcesso> status, string cracha = null, string cpf = null, string empresa = null, string nome = null, int cargo = 0, DateTime? dataInicio = null, DateTime? dataFim = null, int setor = 0, List<int> leitoras = null, TipoPapel? papel = null, bool validaGestor = false, long? empregadoSap = null, long? gestorPonto = null, long? superiorImediato = null, string codigoPht = null, IList<string> localLeitora = null, string passaporte = null, string maodeobra = null, string fornecedor = null, string contratada = null)
        {
            empresa = Empresa.RetornaApenasNomeEmpresa(empresa); 
                        
            var aloc = FiltrarAlocacaoColaboradores(tipoSetor, setor, contratada);
            IQueryable<ControleAcesso> listControleAc = FiltrarAcessos(tipoSetor, status, aloc, nome, cpf, cracha, empresa, cargo, dataInicio, dataFim, setor, leitoras, 
                papel, validaGestor: validaGestor, empregadoSap: empregadoSap, gestorPonto: gestorPonto, superiorImediato: superiorImediato, codigoPht: codigoPht, localLeitora: localLeitora, passaporte:passaporte, maodeobra: maodeobra, fornecedor: fornecedor, contratada: contratada);
            int total = listControleAc.Count();
            List<ControleAcessoModelView> data = montaObj(listControleAc, aloc, parameters);

            return new StoreResult(data, total);
        }
       
        /// <summary>
        /// Filtrar alocacao colaboradores.
        /// </summary>
        ///
        /// <param name="tipoSetor">
        /// O(a) tipo setor.
        /// </param>
        /// <param name="setor">
        /// O(a) setor.
        /// </param>
        ///
        /// <returns>
        /// Um(a) IQueryable&lt;AlocacaoColaborador&gt;
        /// </returns>
        public IQueryable<AlocacaoColaborador> FiltrarAlocacaoColaboradores(TipoSetorCusto? tipoSetor, int setor = 0, string contratada = null)
        {
            IRepositorioAlocacaoColaborador repAlocacao = Fabrica.Instancia.ObterRepositorio<IRepositorioAlocacaoColaborador>(UnidadeTrabalho);
            IQueryable<AlocacaoColaborador> aloc = repAlocacao.ObterTodos();

            if (tipoSetor != null || setor != 0 || !string.IsNullOrEmpty(contratada))
            {
                if (tipoSetor != null)
                    aloc = aloc.Where(p => p.SetorCusto.TipoSetor == tipoSetor);

                if (setor != 0)
                    aloc = aloc.Where(p => p.SetorCusto.Codigo == setor);
                
                if (!string.IsNullOrEmpty(contratada))
                {
                    var query = this.UnidadeTrabalho.ExecuteSql<Models.PedidosContratadaModelView>("SELECT * FROM VW_PedidosContratada ", new Dictionary<string, object>()).AsQueryable();

                    List<int> listaAux = query.Where(x => x.CONTRATADA == contratada).Select(pc => pc.CD_PEDIDO).ToList();                    

                    aloc = aloc.Where(p => listaAux.Contains(p.PedidoCompra.Codigo));
                }                
            }
            return aloc;
        }

        /// <summary>
        /// Filtrar acessos.
        /// </summary>
        ///
        /// <param name="tipoSetor">
        /// O(a) tipo setor.
        /// </param>
        /// <param name="status">
        /// O(a) status.
        /// </param>
        /// <param name="nome">
        /// O(a) nome.
        /// </param>
        /// <param name="cpf">
        /// O(a) cpf.
        /// </param>
        /// <param name="cracha">
        /// O(a) cracha.
        /// </param>
        /// <param name="empresa">
        /// O(a) empresa.
        /// </param>
        /// <param name="cargo">
        /// O(a) cargo.
        /// </param>
        /// <param name="dtInicio">
        /// O(a) dt inicio.
        /// </param>
        /// <param name="dtFim">
        /// O(a) dt fim.
        /// </param>
        /// <param name="setor">
        /// O(a) setor.
        /// </param>
        ///
        /// <returns>
        /// Um(a) IQueryable&lt;ControleAcesso&gt;
        /// </returns>
        public IQueryable<ControleAcesso> FiltrarAcessos(TipoSetorCusto? tipoSetor, List<StatusAcesso> status, IQueryable<AlocacaoColaborador> aloc, string nome = null, string cpf = null, string cracha = null, string empresa = null, int cargo = 0, DateTime? dtInicio = null, DateTime? dtFim = null, int setor = 0, List<int> leitoras = null, TipoPapel? papel = null, string userLogado = null, bool validaGestor = false, long? empregadoSap = null, long? gestorPonto = null, long? superiorImediato = null, string codigoPht = null, IList<string> localLeitora = null, string passaporte = null, string maodeobra = null, string fornecedor = null, string contratada = null)
        {
            IRepositorioControleAcesso rep = Fabrica.Instancia.ObterRepositorio<IRepositorioControleAcesso>(UnidadeTrabalho);

            if (HttpContext != null && HttpContext.User != null && HttpContext.User.Identity != null)
                userLogado = userLogado ?? HttpContext.User.Identity.Name;

            cpf = cpf.Replace(".", "").Replace("-", "");
            IQueryable<ControleAcesso> listControleAc = (tipoSetor != null || setor != 0 || (!string.IsNullOrEmpty(contratada)) ) ?
                rep.ObterListaAcesso(status, cracha, cpf, empresa, nome, cargo, dtInicio, dtFim, userLogado, leitoras, aloc, validaGestor, empregadoSap, gestorPonto, superiorImediato, codigoPht, localLeitora: localLeitora, passaporte: passaporte, maodeobra: maodeobra, fornecedor: fornecedor, contratada: contratada) :
                rep.ObterListaAcesso(status, cracha, cpf, empresa, nome, cargo, dtInicio, dtFim, userLogado, leitoras, validaGestor: validaGestor, empregadoSap: empregadoSap, gestorPonto: gestorPonto, superiorImediato: superiorImediato, codigoPht: codigoPht, localLeitora: localLeitora, passaporte: passaporte, maodeobra: maodeobra, fornecedor: fornecedor, contratada: contratada);

            if (papel.HasValue)
            {
                switch (papel.Value)
                {
                    case TipoPapel.Visitante:
                        listControleAc = listControleAc.Where(x => x.Papel is Visitante);
                        break;
                    case TipoPapel.Colaborador:
                        listControleAc = listControleAc.Where(x => x.Papel is Colaborador);
                        break;
                    case TipoPapel.PrestadorServico:
                        listControleAc = listControleAc.Where(x => x.Papel is PrestadorServico);
                        break;
                    default:
                        break;
                }
            }
                        
            return listControleAc.OrderByDescending(c => c.DataAcesso);
        }

        /// <summary>
        /// Cria um objeto contendo informações relacionadas ao conteudo gerenciado por
        /// ConsultarAcessosController.
        /// </summary>
        ///
        /// <param name="controleAcessos">
        /// O(a) controle acessos.
        /// </param>
        /// <param name="aloc">
        /// O(a) aloc.
        /// </param>
        /// <param name="parameters">
        /// Options for controlling the operation.
        /// </param>
        /// <param name="ignoreParameters">
        /// true to ignore parameters.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;ControleAcessoModelView&gt;
        /// </returns>
        public List<ControleAcessoModelView> montaObj(IQueryable<ControleAcesso> controleAcessos, IQueryable<AlocacaoColaborador> aloc, StoreRequestParameters parameters = null, bool ignoreParameters = false)
        {
            if (!ignoreParameters)
            {
                if (parameters == null)
                    parameters = new StoreRequestParameters();
                controleAcessos = controleAcessos.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 25);
            }

            List<ControleAcessoModelView> list = new List<ControleAcessoModelView>();

            foreach (var s in controleAcessos)
            {
                ControleAcessoModelView item = new ControleAcessoModelView();
                item.CodPapel = (s.Papel!= null ? s.Papel.Codigo.ToString() : "");
                item.Codigo = s.Codigo;
                item.NumCracha = (s.Cracha != null) ? s.Cracha.Numero : s.NumeroSerie;
                item.Leitora = s.Leitora.Descricao;
                item.DataHora = s.DataAcesso;
                item.StatusAcesso = s.StatusAcesso.ObterDescricaoEnum();
                item.Local = s.Local;
                item.Direcao = s.Direcao.ObterDescricaoEnum();

                if (s.Papel != null)
                {
                    item.Nome = s.Papel.PessoaFisica.Nome;
                    item.Papel = s.Papel is Colaborador ? "Colaborador" : (s.Papel is PrestadorServico ? "Prestador de Serviço" : (s.Papel is Visitante ? "Visitante" : ""));
                    item.CPF = s.Papel.PessoaFisica.CPFFormatado();
                    var papelClasse = s.Papel.GetType().ToString();
                    var papelClasse2 = s.Papel.GetType().Name;
                    if (s.Papel is Colaborador)
                    {
                        item.Papel = "Colaborador";
                        item.Empresa = ((Colaborador)s.Papel).Empresa.Apelido;
                        item.EmpresaCNPJ = ((Colaborador)s.Papel).Empresa.CNPJ;

                        IRepositorioColaborador repColaborador = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);
                        item.MaoDeObra = repColaborador.ObterPorId(s.Papel.Codigo).MaoDeObra.ToString();

                        if (s.Papel.EmpregadoSAP != null)
                        {
                            item.MatriculaEmpregadoSAP = string.Format("{0}", s.Papel.EmpregadoSAP.Codigo.ToString());
                            item.MatriculaGestorPonto = s.Papel.EmpregadoSAP.GestorPonto != null ? s.Papel.EmpregadoSAP.GestorPonto.Codigo.ToString() : string.Empty;
                            item.MatriculaSuperiorImediato = s.Papel.EmpregadoSAP.SuperiorImediato != null ? s.Papel.EmpregadoSAP.SuperiorImediato.Codigo.ToString() : string.Empty;
                        }
                        IRepositorioGrupoTrabalhoColab repGrpTrabColab = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoTrabalhoColab>(UnidadeTrabalho);
                        var grpTrabColab = repGrpTrabColab.ObterTodos().OrderByDescending(gtc => gtc.GrupoTrabalho.Codigo)
                            .FirstOrDefault(gtc => gtc.Ativo && gtc.Papel.Codigo.Equals(s.Papel.Codigo));
                        if (grpTrabColab != null && grpTrabColab.Codigo > 0)
                            item.NumeroPHT = grpTrabColab.GrupoTrabalho.CodigoPHT;
                    }

                    if (s.Papel is PrestadorServico)
                    {
                        IRepositorioPrestadorServico repPrestadorServico = Fabrica.Instancia.ObterRepositorio<IRepositorioPrestadorServico>(UnidadeTrabalho);
                        item.MaoDeObra = repPrestadorServico.ObterPorId(s.Papel.Codigo).MaoDeObra.ToString();

                        item.Papel = "Prestador de Serviço";
                        if (((PrestadorServico)s.Papel).TipoPrestador == TipoPrestador.Temporario)
                        {
                            if (((PrestadorServico)s.Papel).Empresa != null)
                                item.Empresa = ((PrestadorServico)s.Papel).Empresa.Apelido;
                                item.EmpresaCNPJ = ((PrestadorServico)s.Papel).Empresa.CNPJ;
                        }
                        else
                        {
                            if (((PrestadorServico)s.Papel).Fornecedor != null)
                                item.Empresa = ((PrestadorServico)s.Papel).Fornecedor.NomeFantasia;
                        }
                    }

                    if (s.Papel is Visitante)
                    {
                        item.MaoDeObra = null;
                        item.Papel = "Visitante";
                        item.Empresa = ((Visitante)s.Papel).Empresa;                        
                    }

                    item.Cargo = (s.Papel is Visitante) ?
                                    (s.Papel as Visitante).Funcao : s.Papel.Cargo != null ?
                                                                        s.Papel.Cargo.Nome : null;
                                                            
                    AlocacaoColaborador alocacao = aloc.Where(a => a.Papel.Codigo == s.Papel.Codigo).OrderByDescending(a => a.DataRegistro).FirstOrDefault();
                    PedidoCompra alocada = alocacao != null ? alocacao.ObterContratada() : null;
                    
                    if (alocacao != null)
                    {
                        item.TipoSetor = alocacao.SetorCusto != null ? alocacao.SetorCusto.TipoSetor.ObterDescricaoEnum() : "";
                        item.Setor = alocacao.SetorCusto != null ? alocacao.SetorCusto.Nome : "";
                        item.Contratada = alocada.Empresa != null ? alocada.Empresa.Nome : "";
                        /*item.Contratada = alocacao.PedidoCompra.PedidoCompraPai != null ? alocacao.PedidoCompra.PedidoCompraPai.Empresa.Apelido :
                                            alocacao.PedidoCompra.PedidoCompraPai == null ? alocacao.PedidoCompra.Empresa.Nome : "";*/
                        item.ContratadaCNPJ = alocacao.PedidoCompra.PedidoCompraPai != null ? alocacao.PedidoCompra.PedidoCompraPai.Empresa.CNPJ :
                        alocacao.PedidoCompra.PedidoCompraPai == null ? alocacao.PedidoCompra.Empresa.CNPJ : "";
                    }
                }

                list.Add(item);
            }

            return list;
        }

        //public ActionResult ExportExcel(string data)
        //{
        //    var xt = new XslCompiledTransform();
        //    var submitData = new Ext.Net.SubmitHandler(data);
        //    XmlNode xml = submitData.Xml;

        //    var s = new StringBuilder();
        //    var settings = new XmlWriterSettings()
        //    {
        //        ConformanceLevel = ConformanceLevel.Auto
        //    };

        //    FileContentResult result = null;
        //    xt.Load(Server.MapPath("~/Content/Excel.xsl"));
        //    System.Xml.XmlWriter writer = System.Xml.XmlWriter.Create(s, settings);
        //    xt.Transform(xml, writer);
        //    result = new FileContentResult(Encoding.UTF8.GetBytes(s.ToString()), "application/vnd.ms-excel");
        //    result.FileDownloadName = "Temp.xls";

        //    return result;
        //}

        /// <summary>
        /// Lista status acesso.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ListaStatusAcesso()
        {
            Dictionary<int, string> dic = new Dictionary<int, string>();

            foreach (int item in Enum.GetValues(typeof(StatusAcesso)))
            {
                dic.Add(item, ((StatusAcesso)item).ObterDescricaoEnum());
            }
            return Json(dic.Select(d => new
            {
                Codigo = d.Key,
                Descricao = d.Value
            }).OrderBy(x => x.Descricao).ToList());

        }

    }
}
