﻿using System.Drawing.Imaging;
using Controlsys.Dominio.Pessoas;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Parametros;
using Controlsys.Repositorios.Empresas;
using Controlsys.Repositorios.Pessoas;
using Ext.Net;
using Ext.Net.MVC;
using Globalsys;
using Globalsys.Exceptions;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Image = iTextSharp.text.Image;
using Controlsys.Repositorios.Acesso;
using Controlsys.Dominio.Acesso;



namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar emitir crachas.
    /// </summary>
    [ExtendController]
    public class EmitirCrachaController : Controller, IControllerBase
    {
        /// <summary>
        /// Gets/Sets valor para UnidadeTrabalho.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.EmitirCrachaController.
        /// </summary>
        ///
        /// <param name="unidade">
        /// O(a) unidade.
        /// </param>
        public EmitirCrachaController(IUnidadeTrabalho unidade)
        {
            UnidadeTrabalho = unidade;
        }

        /// <summary>
        /// Retorna a página de emitirCracha.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Retorna todos os objetos do tipo EmitirCracha.
        /// </summary>
        ///
        /// <param name="cpf">
        /// O(a) cpf.
        /// </param>
        /// <param name="cracha">
        /// O(a) cracha.
        /// </param>
        /// <param name="passaporte">
        /// O(a) passaporte.
        /// </param>
        /// <param name="nome">
        /// O(a) nome.
        /// </param>
        /// <param name="dataCadastro">
        /// O(a) data cadastro.
        /// </param>
        ///
        /// <returns>
        /// Detalhes do tipo de objeto ligado ao controller no formato JSON.
        /// </returns>
        public ActionResult ObterTodos(StoreRequestParameters parameters, string cpf = null, string cracha = null, string passaporte = null, string nome = null, DateTime? dataCadastro = null, bool ignoreParameters = true)
        {
            IRepositorioCracha rep = Fabrica.Instancia.ObterRepositorio<IRepositorioCracha>(UnidadeTrabalho);

            //List<CrachaModelView> crachas = new List<CrachaModelView>();

            IQueryable<Cracha> listaCrachas = rep.ObterTodos().OrderByDescending(p => p.Codigo);

            if (!string.IsNullOrEmpty(nome))
                listaCrachas = listaCrachas.Where(p => p.Papel.PessoaFisica.Nome.Contains(nome));

            if (!string.IsNullOrEmpty(cracha))
                listaCrachas = listaCrachas.Where(p => p.Numero == cracha);

            cpf = cpf.Replace(".", "").Replace("-", "");
            if (!string.IsNullOrEmpty(cpf))
                listaCrachas = listaCrachas.Where(p => p.Papel.PessoaFisica.CPF == cpf);

            if (!string.IsNullOrEmpty(passaporte))
                listaCrachas = listaCrachas.Where(p => p.Papel.PessoaFisica.Passaporte == passaporte);

            if (dataCadastro != null)
                listaCrachas = listaCrachas.Where(l => l.DataRegistro.Date == dataCadastro);

            #region [Desabilitado]
            //foreach (var s in listaCrachas)
            //{
            //    string tipoPapel = string.Empty,
            //           empresa = string.Empty,
            //           statusPapel = string.Empty;

            //    if (s.Papel is Colaborador)
            //    {
            //        tipoPapel = "Colaborador";
            //        empresa = (s.Papel as Colaborador).Empresa.Nome;
            //    }

            //    if (s.Papel is PrestadorServico)
            //    {
            //        tipoPapel = "Prestador de serviço";
            //        empresa = (s.Papel as PrestadorServico).Empresa != null ? (s.Papel as PrestadorServico).Empresa.Nome : null;
            //    }

            //    if (s.Papel is Visitante)
            //    {
            //        tipoPapel = "Visitante";
            //        empresa = (s.Papel as Visitante).Empresa;
            //    }

            //    statusPapel = s.Papel.ObterLogPapel().Status.ObterDescricaoEnum();

            //    crachas.Add(new CrachaModelView
            //    {
            //        Codigo = s.Codigo,
            //        Nome = s.Papel.PessoaFisica.Nome,
            //        Cpf = s.Papel.PessoaFisica.CPFFormatado(),
            //        CodigoPapel = s.Papel.Codigo,
            //        CodigoPessoa = s.Papel.PessoaFisica.Codigo,
            //        TipoPapel = tipoPapel,
            //        Empresa = empresa,
            //        StatusPapel = statusPapel,
            //        RFID = s.Numero,
            //        DataRegistro = s.DataRegistro,
            //        DataDescarte = s.DataDescarte,
            //        DataImpressao = s.DataImpressao,
            //        Ativo = s.Ativo
            //    });
            //} 

            //return Json(crachas, JsonRequestBehavior.AllowGet);
            #endregion


            int total = listaCrachas.Count();
            var data = montaListaObj(listaCrachas, parameters);

            return new StoreResult(data, total);
        }

        /// <summary>
        /// Representa um(a) PDFSettings.
        /// </summary>
        internal class PDFSettings
        {
            /// <summary>
            /// Gets valor para MarginLeft.
            /// </summary>
            ///
            /// <value>
            /// Retorna o(a) MarginLeft.
            /// </value>
            public static int MarginLeft { get { return 10; } }

            /// <summary>
            /// Gets valor para MarginRight.
            /// </summary>
            ///
            /// <value>
            /// Retorna o(a) MarginRight.
            /// </value>
            public static int MarginRight { get { return 10; } }

            /// <summary>
            /// Gets valor para MarginTop.
            /// </summary>
            ///
            /// <value>
            /// Retorna o(a) MarginTop.
            /// </value>
            public static int MarginTop { get { return 10; } }

            /// <summary>
            /// Gets valor para MarginBottom.
            /// </summary>
            ///
            /// <value>
            /// Retorna o(a) MarginBottom.
            /// </value>
            public static int MarginBottom { get { return 30; } }

            /// <summary>
            /// Gets valor para LogoScale.
            /// </summary>
            ///
            /// <value>
            /// Retorna o(a) LogoScale.
            /// </value>
            public static int LogoScale { get { return 27; } }

        }

        /// <summary>
        /// Gerar cracha.
        /// </summary>
        ///
        /// <param name="codigoPapel">
        /// O(a) codigo papel.
        /// </param>
        /// <param name="rfid">
        /// O(a) rfid.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult GerarCracha(int codigoPapel, string rfid)
        {
            IRepositorioCracha repCracha = Fabrica.Instancia.ObterRepositorio<IRepositorioCracha>(UnidadeTrabalho);
            IRepositorioPapel rep = Fabrica.Instancia.ObterRepositorio<IRepositorioPapel>(UnidadeTrabalho);
            IRepositorioSincronizaPessoaAcesso repSincronizaPessoaAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioSincronizaPessoaAcesso>(UnidadeTrabalho);
            Papel papel = rep.ObterPorId(codigoPapel);

            //rfid = string.Format("{0:D14}", Convert.ToInt32(rfid));

            Cracha cracha = new Cracha
            {
                Papel = papel,
                ModeloCracha = papel.ModeloCracha,
                DataImpressao = DateTime.Now,
                DataRegistro = DateTime.Now,
                Numero = rfid,
            };

            repCracha.Validar(cracha);
            cracha.Ativar();
            repCracha.Salvar(cracha);

            DateTime? dataInicio = null;

            if (cracha.Papel is Visitante && (cracha.Papel as Visitante).DataInicio != DateTime.Today && (cracha.Papel as Visitante).DataInicio != null)            
                dataInicio = (cracha.Papel as Visitante).DataInicio;            

            repSincronizaPessoaAcesso.Sincronizar(cracha.Papel, TipoSincronizacaoPessoa.Conceder, dataInicio);

            return Json(montaCracha(cracha));
        }

        /// <summary>
        /// Monta cracha.
        /// </summary>
        ///
        /// <param name="cracha">
        /// O(a) cracha.
        /// </param>
        ///
        /// <returns>
        /// Um(a) string.
        /// </returns>
        private CrachaModelView montaCracha(Cracha cracha)
        {
            var crachaModelView = new CrachaModelView();
            crachaModelView.Codigo = cracha.Codigo;
            crachaModelView.CodigoPessoa = cracha.Papel.PessoaFisica.Codigo;
            crachaModelView.Nome = cracha.Papel.PessoaFisica.Nome;
            crachaModelView.Ativo = cracha.Ativo;
            crachaModelView.DataImpressao = cracha.DataImpressao;
            crachaModelView.Modelo = cracha.Papel.ModeloCracha.Nome;
            crachaModelView.RFID = cracha.Numero;
            crachaModelView.CodigoPapel = cracha.Papel.Codigo;
            crachaModelView.DataRegistro = cracha.DataRegistro;
            crachaModelView.DataImpressao = cracha.DataImpressao;
            crachaModelView.DataDescarte = cracha.DataDescarte;

            crachaModelView.Cpf = cracha.Papel.PessoaFisica.CPFFormatado();
            crachaModelView.TipoPapel = cracha.Papel is Colaborador ? "Colaborador" : (cracha.Papel is PrestadorServico ? "Prestador de Serviço" : (cracha.Papel is Visitante ? "Visitante" : ""));
            crachaModelView.Empresa = cracha.Papel is Colaborador ? (cracha.Papel as Colaborador).Empresa.Nome : (cracha.Papel is PrestadorServico ? (cracha.Papel as PrestadorServico).Empresa.Nome : (cracha.Papel is Visitante ? (cracha.Papel as Visitante).Empresa : ""));
            crachaModelView.StatusPapel = (cracha.Papel is Colaborador || cracha.Papel is PrestadorServico || cracha.Papel is Visitante) ? cracha.Papel.ObterLogPapel().Status.ObterDescricaoEnum() : "";
            crachaModelView.HorizontalTemp = cracha.Papel.ModeloCracha.Horizontal;
            return crachaModelView;
        }

        private List<CrachaModelView> montaListaObj(IQueryable<Cracha> lista, StoreRequestParameters parameters = null, bool ignoreParameters = false)
        {
            if (!ignoreParameters)
            {
                if (parameters == null)
                    parameters = new StoreRequestParameters();
                lista = lista.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 25);
            }

            return lista.Select(c => montaCracha(c)).ToList();
        }

        /// <summary>
        /// Get between.
        /// </summary>
        ///
        /// <param name="strSource">
        /// Source for the.
        /// </param>
        /// <param name="strStart">
        /// O(a) start.
        /// </param>
        /// <param name="strEnd">
        /// O(a) end.
        /// </param>
        ///
        /// <returns>
        /// Um(a) string.
        /// </returns>
        public static string getBetween(string strSource, string strStart, string strEnd)
        {
            int Start, End;
            if (strSource.Contains(strStart) && strSource.Contains(strEnd))
            {
                Start = strSource.IndexOf(strStart, 0) + strStart.Length;
                End = strSource.IndexOf(strEnd, Start);
                return strSource.Substring(Start, End - Start);
            }
            else return "";
        }

        /// <summary>
        /// Descartar cracha.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult DescartarCracha(int codigo)
        {
            IRepositorioCracha repCracha = Fabrica.Instancia.ObterRepositorio<IRepositorioCracha>(UnidadeTrabalho);
            IRepositorioSincronizaPessoaAcesso repSincronizaPessoaAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioSincronizaPessoaAcesso>(UnidadeTrabalho);
            Cracha c = repCracha.ObterPorId(codigo);
            repSincronizaPessoaAcesso.Sincronizar(c.Papel, TipoSincronizacaoPessoa.Suspender, null);
            
            c.Inativar();
            repCracha.Atualizar(c);

            return Json(montaCracha(c));
        }

        /// <summary>
        /// Imagem URL.
        /// </summary>
        ///
        /// <param name="img">
        /// O(a) image.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ImagemUrl(string img)
        {
            byte[] toDecodeByte = Convert.FromBase64String(img);

            MemoryStream ms = new MemoryStream(toDecodeByte);
            System.Drawing.Image returnImage = System.Drawing.Image.FromStream(ms);

            return Json(GravarTemporarioArquivo(toDecodeByte));
        }

        /// <summary>
        /// Gerar cracha PDF.
        /// </summary>
        ///
        /// <param name="img">
        /// O(a) image.
        /// </param>
        /// <param name="descs">
        /// O(a) descs.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult GerarCrachaPDF(string img, string descs)
        {
            #region [Imagem]
            byte[] toDecodeByte = Convert.FromBase64String(img);
            #endregion

            #region [PDF]
            const float fValue = 28.3464567f;
            const float hCm = 8.59f * fValue;
            const float wCm = 5.41f * fValue;
            Document newDocument = new Document(new iTextSharp.text.Rectangle(wCm, hCm), 0f, 0f, 0f, 0f);

            MemoryStream PDFData = new MemoryStream();
            PdfWriter newPdfWriter = PdfWriter.GetInstance(newDocument, PDFData);

            newDocument.Open();
            //iTextSharp.text.Image jpg = Image.GetInstance(toDecodeByte);
            MemoryStream ms = new MemoryStream(toDecodeByte);
            var png = Image.GetInstance(System.Drawing.Image.FromStream(ms), ImageFormat.Png);
            png.ScaleToFit(new iTextSharp.text.Rectangle(wCm, hCm));
            png.SetDpi(300, 300);
            png.CompressionLevel = 0;

            newDocument.Add(png);
            List<TextPDF> descList = JSON.Deserialize<List<TextPDF>>(descs);

            if (descList.Count > 0)
                foreach (var textPdf in descList)
                {
                    PdfContentByte cb = newPdfWriter.DirectContent;
                    cb.BeginText();
                    var col = new ColumnText(cb);

                    // Adjust to origin in top-left, like screens 
                    float x = (textPdf.PosLeft + 12f) *72 / 96;
                    float y = (textPdf.PosTop + 7f) *72 / 96;
                    float height = textPdf.Height + 10f;
                    float width = new float();
                    width = textPdf.Width < 90f ? textPdf.Width : textPdf.Width - 15f;
                    float pageHeight = newDocument.PageSize.Height;
                    float lowerLeftX = x;
                    float lowerLeftY = pageHeight - y - height;
                    float upperRightX = x + width;
                    float upperRightY = pageHeight - y;

                    col.SetSimpleColumn(lowerLeftX, lowerLeftY, upperRightX, upperRightY);
                    col.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                    var font = new iTextSharp.text.Font(BuscarFonte(textPdf.FontFamily.Replace("'", "")), textPdf.FontSize > 0f ? textPdf.FontSize : 8f);
                    //col.AddText(new Paragraph(font.Size, new Chunk(textPdf.Descricao, font)));
                    var paragraph = new Paragraph(textPdf.Descricao, font);
                    paragraph.Alignment = iTextSharp.text.Element.ALIGN_LEFT;
                    //paragraph.SetLeading(0.0f, 0.8f);
                    paragraph.SetLeading(1.1f, 2.2f);
                    paragraph.SpacingAfter = 0.0f;
                    col.AddText(paragraph);
                    col.Go();

                    cb.EndText();
                }

            newDocument.Close();

            #endregion

            #region [download disabled]
            //byte[] bytes = PDFData.ToArray();
            //PDFData.Close();
            //Response.Clear();
            //Response.ContentType = "application/pdf";
            //Response.AddHeader("Content-Disposition", "attachment; filename=Cracha.pdf");
            //Response.Buffer = true;
            //Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //Response.BinaryWrite(bytes);
            //Response.BinaryWrite(PDFData.GetBuffer());
            //Response.End();
            //Response.Close();
            return File(PDFData.GetBuffer(), "application/pdf", "Cracha.pdf");
            #endregion
        }

        /// <summary>
        /// Buscar fonte.
        /// </summary>
        ///
        /// <param name="fonte">
        /// O(a) fonte.
        /// </param>
        ///
        /// <returns>
        /// Um(a) BaseFont.
        /// </returns>
        private BaseFont BuscarFonte(string fonte)
        {
            //var fontName = "Tahoma";
            //if (!FontFactory.IsRegistered(fontName))
            //{
            //    FontFactory.Register(fontPath);
            //}
            //return FontFactory.GetFont(fontName, BaseFont.IDENTITY_H, BaseFont.EMBEDDED); 

            string fontPath;
            switch (fonte.ToLower())
            {
                case "arial":
                    fontPath = Server.MapPath("~/Content/font/arial.ttf");
                    break;
                case "calibri":
                    fontPath = Server.MapPath("~/Content/font/calibri.ttf");
                    break;
                case "times new roman":
                    fontPath = Server.MapPath("~/Content/font/times.ttf");
                    break;
                case "tktypebold":
                    fontPath = Server.MapPath("~/Content/font/TKBold.ttf");
                    break;
                case "verdana":
                    fontPath = Server.MapPath("~/Content/font/verdana.ttf");
                    break;
                case "tktypemedium":
                    fontPath = Server.MapPath("~/Content/font/TKTypeMedium.ttf");
                    break;
                case "tktyperegular":
                    fontPath = Server.MapPath("~/Content/font/TKTypeRegular.ttf");
                    break;
                default:
                    fontPath = Server.MapPath("~/Content/font/tahoma.ttf");
                    break;
            }

            return BaseFont.CreateFont(fontPath, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
        }

        /// <summary>
        /// Carregar imagem.
        /// </summary>
        ///
        /// <param name="imagem">
        /// O(a) imagem.
        /// </param>
        ///
        /// <returns>
        /// Um(a) System.Drawing.Image.
        /// </returns>
        private System.Drawing.Image CarregarImagem(string imagem)
        {
            byte[] imagemBytes = null;
            string caminhoCompletoImagem = imagem;

            int posicao = caminhoCompletoImagem.IndexOf("/Content");
            string caminho = caminhoCompletoImagem.Substring(posicao);
            imagemBytes = System.IO.File.ReadAllBytes(Server.MapPath("~" + caminho));

            MemoryStream ms = new MemoryStream(imagemBytes);
            System.Drawing.Image returnImage = System.Drawing.Image.FromStream(ms);

            return returnImage;
        }

        #region public ActionResult GerarCrachaPDF(int codigo, string url)
        //public ActionResult GerarCrachaPDF(int codigo, string url)
        //{
        //    IRepositorioCracha repCracha = Fabrica.Instancia.ObterRepositorio<IRepositorioCracha>(UnidadeTrabalho);
        //    IRepositorioPapel rep = Fabrica.Instancia.ObterRepositorio<IRepositorioPapel>(UnidadeTrabalho);
        //    Papel papel = rep.ObterPorId(codigo);
        //
        //    const float fValue = 28.3464567f;
        //    const float hCm = 8.59f;
        //    const float wCm = 5.41f;
        //    Document newDocument = new Document(new iTextSharp.text.Rectangle(wCm * fValue, hCm * fValue), 0f, 0f, 0f, 0f);
        //    //Document newDocument = new Document(new iTextSharp.text.Rectangle(202f, 321f), 0f, 0f, 0f, 0f);
        //
        //    MemoryStream PDFData = new MemoryStream();
        //    PdfWriter newPdfWriter = PdfWriter.GetInstance(newDocument, PDFData);
        //    iTextSharp.text.html.simpleparser.StyleSheet styles = new iTextSharp.text.html.simpleparser.StyleSheet();
        //    //HTMLWorker hw = new HTMLWorker(newDocument);
        //
        //    newDocument.Open();
        //    iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Content/img/webcam/" + url + ".jpeg"));
        //    jpg.ScaleAbsolute(new iTextSharp.text.Rectangle(wCm * fValue, hCm * fValue));
        //    //jpg.ScaleAbsolute(new iTextSharp.text.Rectangle(202f, 321f));
        //
        //    //jpg.ScalePercent(70f);
        //
        //    newDocument.Add(jpg);
        //
        //    newDocument.Close();
        //
        //    Response.ContentType = "application/pdf";
        //    Response.Cache.SetCacheability(System.Web.HttpCacheability.Public);
        //    Response.AppendHeader("Content-Type", "application/pdf");
        //    Response.AppendHeader("Content-Disposition", "inline; filename=Cracha.pdf");
        //    Response.OutputStream.Write(PDFData.GetBuffer(), 0, PDFData.GetBuffer().Length);
        //    Response.OutputStream.Flush();
        //    Response.OutputStream.Close();
        //
        //    return View();
        //} 
        #endregion

        /// <summary>
        /// Pixels to points.
        /// </summary>
        ///
        /// <param name="value">
        /// O(a) value.
        /// </param>
        /// <param name="dpi">
        /// O(a) DPI.
        /// </param>
        ///
        /// <returns>
        /// Um(a) float.
        /// </returns>
        public static float PixelsToPoints(float value, int dpi)
        {
            return value / dpi * 72;
        }

        /// <summary>
        /// Selecionar pessoa.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult SelecionarPessoa(int codigo)
        {
            IRepositorioPapel rep = Fabrica.Instancia.ObterRepositorio<IRepositorioPapel>(UnidadeTrabalho);

            Papel papel = rep.ObterPorId(codigo);
            if(papel.PessoaFisica.Apelido.Split(' ').Count() > 1 ) { 
                papel.PessoaFisica.ApelidoLast = papel.PessoaFisica.Apelido.ToString().Split()[1];
                papel.PessoaFisica.Apelido = papel.PessoaFisica.Apelido.ToString().Split()[0];
            }else
                papel.PessoaFisica.Apelido = papel.PessoaFisica.Apelido.ToString();
            if (papel.ModeloCracha != null)
            {
                bool? horizontal = papel.ModeloCracha.Horizontal == null ? false : true;
                string html = papel.ModeloCracha.Html;
                html = montaCracha(html, papel);
                string fundo = papel.ModeloCracha.Fundo == null ? null : Convert.ToBase64String(papel.ModeloCracha.Fundo);
                return Json(new
                {
                    html = html,
                    fundo = fundo,
                    horizontal
                });
            }

            throw new CoreException("A pessoa selecionada não possui modelo de crachá cadastrado.");
        }

        /// <summary>
        /// Monta cracha.
        /// </summary>
        ///
        /// <param name="html">
        /// O(a) HTML.
        /// </param>
        /// <param name="papel">
        /// O(a) papel.
        /// </param>
        ///
        /// <returns>
        /// Um(a) string.
        /// </returns>
        private string montaCracha(string html, Papel papel)
        {
            if (html != null)
            {
                string empresa = "";
                if (papel.ModeloCracha.TipoPapel.ToString() == "PrestadorServico")
                {
                    IRepositorioPrestadorServico ts = Fabrica.Instancia.ObterRepositorio<IRepositorioPrestadorServico>(UnidadeTrabalho);
                    var retorno = ts.ObterTodos().Where(t => t.Codigo == papel.Codigo && t.TipoPrestador.ToString() == "Emergencial").FirstOrDefault();
                    if (retorno != null)
                        html = html.Replace("NOME FANTASIA FORNECEDOR", retorno.Fornecedor.NomeFantasia);
                }
                else
                    html = html.Replace("NOME FANTASIA FORNECEDOR", "");
                html = html.Replace("CÓDIGO PESSOA", papel.PessoaFisica.Codigo);
                html = html.Replace("NOME", papel.PessoaFisica.Nome);
                if(papel.PessoaFisica.Apelido.IndexOf(" ") > 0) { 
                    html = html.Replace("PRIMEIROAPELIDO", papel.PessoaFisica.Apelido.Split(' ')[0]);
                    html = html.Replace("SEGUNDOAPELIDO", papel.PessoaFisica.Apelido.Split(' ')[1]);
                }else
                {
                    if(papel.PessoaFisica.Apelido.IndexOf(" ") < 1)
                    {
                        html = html.Replace("PRIMEIROAPELIDO", papel.PessoaFisica.Apelido);
                        if(papel.PessoaFisica.ApelidoLast != null && papel.PessoaFisica.ApelidoLast.Length > 0)
                            html = html.Replace("SEGUNDOAPELIDO", papel.PessoaFisica.ApelidoLast);
                        else
                            html = html.Replace("SEGUNDOAPELIDO", "");
                    }
                }

                html = html.Replace("CÓDIGO PAPEL", papel.Codigo.ToString());
                html = html.Replace("8ID", "");
                IRepositorioCracha cac = Fabrica.Instancia.ObterRepositorio<IRepositorioCracha>(UnidadeTrabalho);
                var cRfid = cac.ObterTodos().Where(p => p.Papel.Codigo == papel.Codigo && p.Ativo).FirstOrDefault();
                html = html.Replace("RFID", cRfid != null ? cRfid.Numero : null);
                //html = html.Replace("RFID", rfid != null ? rfid : "");

                if (papel is Colaborador)
                {
                    Colaborador colab = (Colaborador)papel;
                    var contratada = obterContratada(papel.Codigo);
                    html = html.Replace("CONTRATADA / EMPRESA", colab.Empresa != null ? contratada + " / " + colab.Empresa.Apelido  : "");
                    html = empresa = html.Replace("EMPRESA", colab.Empresa.Apelido);
                    html = (colab.Cargo) != null ? html.Replace("CARGO", colab.Cargo.Nome) : "";
                    html = html.Replace("CONTRATADA", empresa != contratada ? contratada : "");
                    
                }

                if (papel is Visitante)
                {
                    Visitante visi = (Visitante)papel;
                    html = empresa = html.Replace("EMPRESA", visi.Empresa);
                    html = html.Replace("CARGO", visi.Funcao);
                }

                if (papel is PrestadorServico)
                {
                    PrestadorServico prest = (PrestadorServico)papel;
                    html = empresa = html.Replace("EMPRESA", prest.Empresa != null ? prest.Empresa.Apelido : prest.Fornecedor.NomeFantasia);
                    html = (prest.Cargo != null) ? html.Replace("CARGO", prest.Cargo.Nome) : "";
                }
                html = html.Replace("TIPO SANGUÍNEO", papel.PessoaFisica.TipoSanguineo != 0 && papel.PessoaFisica.TipoSanguineo != null ? papel.PessoaFisica.TipoSanguineo.ObterDescricaoEnum() : "");
                html = html.Replace("/Content/avatar.png", GravarTemporario(papel.PessoaFisica.Foto));
            }
            return html;
        }

        /// <summary>
        /// Obter contratada.
        /// </summary>
        ///
        /// <param name="codPessoa">
        /// O(a) cod pessoa.
        /// </param>
        ///
        /// <returns>
        /// Um(a) string.
        /// </returns>
        private string obterContratada(int codPessoa)
        {
            IRepositorioAlocacaoColaborador repAlocColab = Fabrica.Instancia.ObterRepositorio<IRepositorioAlocacaoColaborador>(UnidadeTrabalho);
            var alocao = repAlocColab.ObterAlocacao(codPessoa);

            if (alocao != null)
                return alocao.ObterContratada().Empresa.Apelido;
            else
                return null;
        }

        /// <summary>
        /// Cria um objeto contendo informações relacionadas ao conteudo gerenciado por
        /// EmitirCrachaController.
        /// </summary>
        ///
        /// <param name="pf">
        /// O(a) pf.
        /// </param>
        /// <param name="imageUrl">
        /// URL of the image.
        /// </param>
        ///
        /// <returns>
        /// Um(a) object.
        /// </returns>
        private object montaObj(PessoaFisica pf, string imageUrl = null)
        {
            return new PessoaFisicaModelView()
            {
                CodigoPessoa = pf.Codigo,
                CPF = pf.CPF != null ? pf.CPFFormatado() : null,
                Passaporte = pf.Passaporte,
                Nome = pf.Nome,
                Apelido = pf.Apelido,
                ApelidoLast = pf.ApelidoLast,
                Email = pf.Email,
                TipoSanguineo = pf.TipoSanguineo,

                img = imageUrl

            };
        }

        /// <summary>
        /// Gravar temporario.
        /// </summary>
        ///
        /// <param name="foto">
        /// O(a) foto.
        /// </param>
        ///
        /// <returns>
        /// Um(a) string.
        /// </returns>
        private string GravarTemporario(byte[] foto)
        {
            if (foto != null)
            {
                Random randNum = new Random();
                int number = randNum.Next(DateTime.Now.Millisecond);

                string nomeArquivo = number + ".jpeg";

                byte[] b = foto;

                using (FileStream fs = new FileStream(Server.MapPath("~/Content/img/webcam/") + nomeArquivo, FileMode.Create))
                    fs.Write(b, 0, b.Length);

                return "/Content/img/webcam/" + nomeArquivo;
            }
            return "/Content/img/avatar.png";
        }

        /// <summary>
        /// Gravar temporario arquivo.
        /// </summary>
        ///
        /// <param name="foto">
        /// O(a) foto.
        /// </param>
        ///
        /// <returns>
        /// Um(a) string.
        /// </returns>
        private string GravarTemporarioArquivo(byte[] foto)
        {
            if (foto != null)
            {
                Random randNum = new Random();
                int number = randNum.Next(DateTime.Now.Millisecond);

                string nomeArquivo = number + ".jpeg";

                byte[] b = foto;

                using (FileStream fs = new FileStream(Server.MapPath("~/Content/img/webcam/") + nomeArquivo, FileMode.Create))
                    fs.Write(b, 0, b.Length);

                return number.ToString();
            }
            return "/Content/img/avatar.png";
        }
    }

    /// <summary>
    /// Representa um(a) TextPDF.
    /// </summary>
    public class TextPDF
    {
        //public string Campo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Descricao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Descricao.
        /// </value>
        public string Descricao { get; set; }

        /// <summary>
        /// Gets/Sets valor para FontSize.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) FontSize.
        /// </value>
        public float FontSize { get; set; }

        /// <summary>
        /// Gets/Sets valor para FontFamily.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) FontFamily.
        /// </value>
        public string FontFamily { get; set; }

        /// <summary>
        /// Gets/Sets valor para PosTop.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) PosTop.
        /// </value>
        public float PosTop { get; set; }

        /// <summary>
        /// Gets/Sets valor para PosLeft.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) PosLeft.
        /// </value>
        public float PosLeft { get; set; }

        /// <summary>
        /// Gets/Sets valor para Height.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Height.
        /// </value>
        public float Height { get; set; }

        /// <summary>
        /// Gets/Sets valor para Width.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Width.
        /// </value>
        public float Width { get; set; }
    }
}
