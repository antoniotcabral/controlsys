﻿using Controlsys.Dominio.Parametros;
using Controlsys.Infra;
using Controlsys.Repositorios.Parametros;
using Ext.Net;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar feriadoes.
    /// </summary>
    [ExtendController]
    public class FeriadoController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /Feriado/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.FeriadoController.
        /// </summary>
        ///
        /// <param name="unidade">
        /// O(a) unidade.
        /// </param>
        public FeriadoController(IUnidadeTrabalho unidade)
        {
            UnidadeTrabalho = unidade;
        }

        /// <summary>
        /// Retorna a página de feriado.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Inclui um novo feriado.
        /// </summary>
        ///
        /// <param name="feriado">
        /// O(a) feriado.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Incluir(Feriado feriado)
        {
            IRepositorioFeriado repFeriado = Fabrica.Instancia.ObterRepositorio<IRepositorioFeriado>(UnidadeTrabalho);
            feriado.DataRegistro = DateTime.Now;
            repFeriado.Validar(feriado, EstadoObjeto.Novo);
            feriado.Ativar();
            repFeriado.Salvar(feriado);

            return Json(feriado);
        }

        /// <summary>
        /// Altera um registro de feriado.
        /// </summary>
        ///
        /// <param name="feriado">
        /// O(a) feriado.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Alterar(Feriado feriado)
        {
            IRepositorioFeriado repFeriado = Fabrica.Instancia.ObterRepositorio<IRepositorioFeriado>(UnidadeTrabalho);
            Feriado feriadoA = repFeriado.ObterPorId(feriado.Codigo);
            feriadoA.Descricao = feriado.Descricao;
            feriadoA.Nome = feriado.Nome;
            feriadoA.DataFeriado = feriado.DataFeriado;
            repFeriado.Validar(feriadoA, EstadoObjeto.Alterado);
            repFeriado.Atualizar(feriadoA);

            return Json(feriadoA);
        }

        /// <summary>
        /// Ativar inativar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult AtivarInativar(int codigo)
        {
            IRepositorioFeriado repFeriado = Fabrica.Instancia.ObterRepositorio<IRepositorioFeriado>(UnidadeTrabalho);
            Feriado feriado = repFeriado.ObterPorId(codigo);
            if (feriado.Ativo)
            {
                IRepositorioTurno repTurno = Fabrica.Instancia.ObterRepositorio<IRepositorioTurno>(UnidadeTrabalho);
                bool temTurno = repTurno.ObterTodos().Any(t => t.FeriadosTurno.Any(f => (f.Ativo) && (f.Feriado.Codigo == codigo)));
                if (temTurno)
                    throw new CoreException("Não é possível inativar. Este feriado está ativo em turno.");
                feriado.Inativar();
            }
            else
            {
                repFeriado.Validar(feriado, EstadoObjeto.Ativado);
                feriado.Ativar();
            }
            repFeriado.Atualizar(feriado);

            return Json(feriado);
        }

        /// <summary>
        /// Selecionar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Selecionar(int codigo)
        {
            IRepositorioFeriado repFeriado = Fabrica.Instancia.ObterRepositorio<IRepositorioFeriado>(UnidadeTrabalho);

            Feriado feriado = repFeriado.ObterPorId(codigo);

            feriado.DataFeriado.AddHours(12);

            return Json(feriado);
        }

        /// <summary>
        /// Selecionar lista.
        /// </summary>
        ///
        /// <param name="listaFeriados">
        /// O(a) lista feriados.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult SelecionarLista(string listaFeriados)
        {
            SelectedRowCollection src = JSON.Deserialize<SelectedRowCollection>(listaFeriados);
            IRepositorioFeriado repFeriado = Fabrica.Instancia.ObterRepositorio<IRepositorioFeriado>(UnidadeTrabalho);
            List<Feriado> listaFeriado = new List<Feriado>();
            foreach (SelectedRow linha in src)
            {
                int codigo = 0;
                int.TryParse(linha.RecordID, out codigo);
                Feriado feriado = repFeriado.ObterPorId(codigo);
                feriado.DataFeriado.AddHours(12);
                if (feriado == null || !(feriado.Codigo > 0))
                    continue;

                listaFeriado.Add(feriado);
            }

            return Json(listaFeriado);
        }

        /// <summary>
        /// Retorna todos os objetos do tipo Feriado.
        /// </summary>
        ///
        /// <param name="apenasAtivos">
        /// true to apenas ativos.
        /// </param>
        ///
        /// <returns>
        /// Detalhes do tipo de objeto ligado ao controller no formato JSON.
        /// </returns>
        public ActionResult ObterTodos(bool apenasAtivos = false)
        {
            IRepositorioFeriado repFeriado = Fabrica.Instancia.ObterRepositorio<IRepositorioFeriado>(UnidadeTrabalho);

            IQueryable<Feriado> tipos = repFeriado.ObterTodos();

            if (apenasAtivos)
            {
                tipos = tipos.Where(a => a.Ativo == true);
            }

            return Json(tipos.Select(a => new
            {
                Codigo = a.Codigo,
                DataFeriado = a.DataFeriado.AddHours(12),
                Nome = a.Nome,
                Descricao = a.Descricao,
                Ativo = a.Ativo
            })
            .ToList(), JsonRequestBehavior.AllowGet);
        }
    }
}
