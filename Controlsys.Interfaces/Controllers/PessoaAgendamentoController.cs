﻿using Controlsys.Dominio.AgendamentosPessoa;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Agendamentos;
using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class PessoaAgendamentoController : Controller, IControllerBase
    {
        //
        // GET: /PessoaAgendamento/

         public IUnidadeTrabalho UnidadeTrabalho { get; set; }

         public PessoaAgendamentoController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        public ActionResult ObterPorCpfPassaporte(string cpf = null, string passaporte = null)
        {
            IRepositorioPessoaAgendamento repPessoaAg = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaAgendamento>(UnidadeTrabalho);
            if (cpf != null)
                cpf = cpf.Replace("-", "").Replace(".", "");

            return Json(montaObj(repPessoaAg.ObterPorCpfPassaporte(cpf, passaporte)));
        }

        private PessoaAgendamentoModelView montaObj(PessoaAgendamento pessoaAg)
        {
            if (pessoaAg == null)
                return null;

            PessoaAgendamentoModelView agPes = new PessoaAgendamentoModelView();
           
            agPes.CodigoPessoa = pessoaAg.Codigo;
            agPes.CPF = pessoaAg.CPF != null ? pessoaAg.CPFFormatado() : null;
            agPes.Nome = pessoaAg.Nome;
            agPes.Passaporte = pessoaAg.Passaporte != null ? pessoaAg.Passaporte : null;
            agPes.Apelido = pessoaAg.Apelido;
            agPes.Email = pessoaAg.Email;
            agPes.Sexo = pessoaAg.Sexo.ToString();
            agPes.TelefoneNumEmergencial = pessoaAg.TelefoneNumEmergencial;
            agPes.TelefoneNumCelular = pessoaAg.TelefoneNumCelular;
            agPes.RG = pessoaAg.RG;
            agPes.RGDataEmissao = pessoaAg.RGDataEmissao;
            agPes.RGOrgaoEmissor = pessoaAg.RGOrgaoEmissor;
            agPes.RGOrgaoEmissorUF = pessoaAg.RGOrgaoEmissorUF != null ? pessoaAg.RGOrgaoEmissorUF.Codigo : (int?)0;

            return agPes;
        }

        public ActionResult ObterStatusAgendamento()
        {
            Dictionary<int, string> dic = new Dictionary<int, string>();

            foreach (StatusAgendamento status in Enum.GetValues(typeof(StatusAgendamento)))
            {
                dic.Add(Convert.ToInt32(status), status.ObterDescricaoEnum());
            }

            return Json(dic.Select(d => new
            {
                Codigo = d.Key,
                Nome = d.Value
            }).ToArray());
        }

    }
}
