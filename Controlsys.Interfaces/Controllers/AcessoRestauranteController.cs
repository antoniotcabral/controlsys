﻿using System.Configuration;
using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Acesso;
using Controlsys.Repositorio.Parametros;
using Controlsys.Repositorios.Acesso;
using Controlsys.Repositorios.Empresas;
using Controlsys.Repositorios.Pessoas;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class AcessoRestauranteController : Controller, IControllerBase
    {
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// GET: /AcessoRestaurante/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public AcessoRestauranteController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Pesquisar(StoreRequestParameters parameters, bool apenasAtivos = false, int restaurante = 0, int tipoAlimentacao = 0, string nome = null, string empresa = null, string cracha = null, string cpfPassaporte = null)
        {
            empresa = Empresa.RetornaApenasNomeEmpresa(empresa);
            IQueryable<AcessoRestaurante> listControleAc = ObterTodos(apenasAtivos, restaurante, tipoAlimentacao, nome, empresa, cracha, cpfPassaporte);
            int total = listControleAc.Count();
            List<AcessoRestauranteModelView> data = montaLIsta(listControleAc, parameters, false);

            return new StoreResult(data, total);
        }

        //public ActionResult ObterTodos(bool apenasAtivos = false, int restaurante = 0, int tipoAlimentacao = 0, string nome = null, string empresa = null, string cracha = null, string cpfPassaporte = null, int qtdeLinha = 0)
        public IQueryable<AcessoRestaurante> ObterTodos(bool apenasAtivos = false, int restaurante = 0, int tipoAlimentacao = 0, string nome = null, string empresa = null, string cracha = null, string cpfPassaporte = null, int qtdeLinha = 0)
        {
            IRepositorioAcessoRestaurante repARest = Fabrica.Instancia.ObterRepositorio<IRepositorioAcessoRestaurante>(UnidadeTrabalho);

            IQueryable<AcessoRestaurante> acessoRest = repARest.ObterTodos();
            int total = acessoRest.Count();

            if (apenasAtivos)
            {
                acessoRest = acessoRest.Where(t => t.Ativo == true);
            }
            if (restaurante != 0)
            {
                acessoRest = acessoRest.Where(p => p.TiposAlimentacao.Any(m => m.TipoAlimentacao.Restaurante.Codigo == restaurante && m.Ativo));
            }
            if (tipoAlimentacao != 0)
            {
                acessoRest = acessoRest.Where(p => p.TiposAlimentacao.Any(m => m.TipoAlimentacao.TipoAlimentacao.Codigo == tipoAlimentacao && m.Ativo));
            }
            if (!string.IsNullOrEmpty(nome))
            {
                acessoRest = acessoRest.Where(t => t.Papel.PessoaFisica.Nome.Contains(nome));
            }

            if (!string.IsNullOrEmpty(cpfPassaporte))
            {
                string cpfpassaporteformatado = cpfPassaporte.Replace("-", string.Empty).Replace(".", string.Empty).Replace("/", string.Empty);
                acessoRest = acessoRest.Where(o => (o.Papel.PessoaFisica.CPF != null && o.Papel.PessoaFisica.CPF.Contains(cpfpassaporteformatado)) || (o.Papel.PessoaFisica.Passaporte != null && o.Papel.PessoaFisica.Passaporte.Contains(cpfpassaporteformatado)));
            }

            if (!string.IsNullOrEmpty(empresa))
            {
                var papeis = acessoRest.Select(t => t.Papel);
                var auxCodPapeis = new List<int>();
                empresa = empresa.ToUpper();

                foreach (var item in papeis)
                {
                    if (item is Colaborador)
                        if ((item as Colaborador).Empresa != null &&
                            ((item as Colaborador).Empresa.Nome.ToUpper().Contains(empresa) || (item as Colaborador).Empresa.Apelido.ToUpper().Contains(empresa)))
                            auxCodPapeis.Add(item.Codigo);

                    if (item is PrestadorServico)
                    {
                        if ((item as PrestadorServico).TipoPrestador == TipoPrestador.Temporario)
                        {
                            if ((item as PrestadorServico).Empresa != null &&
                                ((item as PrestadorServico).Empresa.Nome.ToUpper().Contains(empresa) || (item as PrestadorServico).Empresa.Apelido.ToUpper().Contains(empresa)))
                                auxCodPapeis.Add(item.Codigo);
                        }
                        else
                        {
                            if ((item as PrestadorServico).Fornecedor != null && (item as PrestadorServico).Fornecedor.NomeFantasia.ToUpper().Contains(empresa))
                                auxCodPapeis.Add(item.Codigo);
                        }
                    }

                    if (item is Visitante)
                        if ((item as Visitante).Empresa != null && (item as Visitante).Empresa.ToUpper().Contains(empresa))
                            auxCodPapeis.Add(item.Codigo);
                }

                acessoRest = acessoRest.Where(t => auxCodPapeis.Contains(t.Papel.Codigo));
            }
            if (!string.IsNullOrEmpty(cracha))
            {
                IRepositorioCracha rep = Fabrica.Instancia.ObterRepositorio<IRepositorioCracha>(UnidadeTrabalho);
                IQueryable<Cracha> listCracha = rep.ObterTodos().Where(t => t.Ativo && t.Papel.PapelLogs.OrderByDescending(pl => pl.Codigo)
                                                                         .Select(pl => pl.Status)
                                                                         .FirstOrDefault() != StatusPapelLog.BaixaDefinitiva);
                listCracha = listCracha.Where(p => p.Numero.Contains(cracha));
                acessoRest = acessoRest.Where(t => listCracha.Any(g => g.Papel.Codigo == t.Papel.Codigo));
            }
            //acessoRest = acessoRest.Skip(0).Take(qtdeLinha);
            //acessoRest = acessoRest.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 25);
            //acessoRest = acessoRest.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : qtdeLinha);
            //acessoRest = acessoRest.Skip(0).Take(parameters.Limit > 0 ? parameters.Limit : 25);
            //return new StoreResult(acessoRest, total);
            //return Json(acessoRest.Select(a => montaObj(a)).ToList());
            return acessoRest;
        }

        [Transaction]
        public ActionResult Incluir(AcessoRestaurante acesso, TipoAlimentacaoRestaurante[] tipoAlimentacaoRestaurante, int cod_papel, bool crachaMestre, bool naoValidaAcessoHierarquico)
        {
            IRepositorioAcessoRestaurante repAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioAcessoRestaurante>(UnidadeTrabalho);
            IRepositorioPapel repPapel = Fabrica.Instancia.ObterRepositorio<IRepositorioPapel>(UnidadeTrabalho);
            IRepositorioRestaurante repRestaurante = Fabrica.Instancia.ObterRepositorio<IRepositorioRestaurante>(UnidadeTrabalho);
            IRepositorioSincronizaPessoaAcesso repSincronizaPessoaAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioSincronizaPessoaAcesso>(UnidadeTrabalho);
            IRepositorioGrupoAcesso repGrupoAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoAcesso>(UnidadeTrabalho);
            IRepositorioAcessoRestauranteTipoAlimentacao repTpAlRest = Fabrica.Instancia.ObterRepositorio<IRepositorioAcessoRestauranteTipoAlimentacao>(UnidadeTrabalho);

            Papel papel = repPapel.ObterPorId(cod_papel);

            acesso.Papel = papel;
            acesso.DataRegistro = DateTime.Now;
            acesso.Ativar();
            repAcesso.Validar(acesso, EstadoObjeto.Novo);
            repAcesso.Salvar(acesso);

            LogAcessoRestaurante logAcesso = new LogAcessoRestaurante();
            logAcesso.CrachaMestre = crachaMestre;
            logAcesso.NaoValidaAcessoHierarquico = naoValidaAcessoHierarquico;
            logAcesso.DataRegistro = DateTime.Now;
            logAcesso.AcessoRestaurante = acesso;
            repAcesso.SalvarLogAcessoRestaurante(logAcesso);
            acesso.LogAcessoRestaurante.Add(logAcesso);


            if (tipoAlimentacaoRestaurante != null)
            {
                foreach (var item in tipoAlimentacaoRestaurante)
                {
                    AcessoRestauranteTipoAlimentacao acessoRTAli = new AcessoRestauranteTipoAlimentacao();

                    acessoRTAli.AcessoRestaurante = acesso;
                    acessoRTAli.TipoAlimentacao = repRestaurante.TAlimentacaoRestaurante(item);
                    acessoRTAli.DataRegistro = DateTime.Now;
                    acessoRTAli.Ativar();
                    repTpAlRest.Validar(acessoRTAli, EstadoObjeto.Novo);
                    repAcesso.SalvarTipoAlimentacaoRestaurante(acessoRTAli);
                    acesso.TiposAlimentacao.Add(acessoRTAli);
                    // Conceder acesso
                    sincronizaAcesso(acesso, true);
                }
            }



            return Json(montaObj(acesso));
        }

        [Transaction]
        public ActionResult Alterar(AcessoRestaurante acesso, TipoAlimentacaoRestaurante[] tiposAliDel, TipoAlimentacaoRestaurante[] tiposAliNew, bool crachaMestre, bool naoValidaAcessoHierarquico)
        {
            return Alterar(acesso, tiposAliDel, tiposAliNew, crachaMestre, false, naoValidaAcessoHierarquico);
        }

        [Transaction]
        internal ActionResult Alterar(AcessoRestaurante acesso, TipoAlimentacaoRestaurante[] tiposAliDel, TipoAlimentacaoRestaurante[] tiposAliNew, bool crachaMestre, bool alteracaoSistema, bool naoValidaAcessoHierarquico)
        {
            IRepositorioAcessoRestaurante repRest = Fabrica.Instancia.ObterRepositorio<IRepositorioAcessoRestaurante>(UnidadeTrabalho);
            IRepositorioRestaurante repRestaurante = Fabrica.Instancia.ObterRepositorio<IRepositorioRestaurante>(UnidadeTrabalho);
            IRepositorioSincronizaPessoaAcesso repSincronizaPessoaAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioSincronizaPessoaAcesso>(UnidadeTrabalho);
            IRepositorioGrupoAcesso repGrupoAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoAcesso>(UnidadeTrabalho);
            IRepositorioAcessoRestauranteTipoAlimentacao repTpAlRest = Fabrica.Instancia.ObterRepositorio<IRepositorioAcessoRestauranteTipoAlimentacao>(UnidadeTrabalho);

            AcessoRestaurante acessoR = repRest.ObterPorId(acesso.Codigo);

            var logAcessoRestAux = acessoR.LogAcessoRestaurante.OrderByDescending(t => t.DataRegistro).FirstOrDefault();
            if (logAcessoRestAux != null && (logAcessoRestAux.CrachaMestre != crachaMestre || logAcessoRestAux.NaoValidaAcessoHierarquico != naoValidaAcessoHierarquico))
            {
                LogAcessoRestaurante log = new LogAcessoRestaurante();
                log.DataRegistro = DateTime.Now;
                log.AcessoRestaurante = acessoR;
                log.CrachaMestre = crachaMestre;
                log.NaoValidaAcessoHierarquico = naoValidaAcessoHierarquico;
                repRest.SalvarLogAcessoRestaurante(log);
            }

            repRest.Validar(acessoR, EstadoObjeto.Alterado, alteracaoSistema);


            List<Restaurante> restDel = new List<Restaurante>();
            List<Restaurante> restAdd = new List<Restaurante>();
            //TipoAlimentacaoRestaurante
            if (tiposAliDel != null)
            {
                foreach (var item in acessoR.TiposAlimentacao.Where(t => t.Ativo && tiposAliDel.Any(tdd => tdd.Codigo == t.TipoAlimentacao.Codigo)))//t.Codigo)))
                {
                    AcessoRestauranteTipoAlimentacao tpAliRestA = item;
                    tpAliRestA.Inativar();
                    restDel.Add(item.TipoAlimentacao.Restaurante);
                }
            }
            if (tiposAliNew != null)
            {
                foreach (var item in tiposAliNew)
                {
                    AcessoRestauranteTipoAlimentacao acessoRTAli = new AcessoRestauranteTipoAlimentacao();
                    acessoRTAli.AcessoRestaurante = acessoR;
                    acessoRTAli.TipoAlimentacao = repRestaurante.TAlimentacaoRestaurante(item);
                    acessoRTAli.DataRegistro = DateTime.Now;
                    acessoRTAli.Ativar();
                    repTpAlRest.Validar(acessoRTAli, EstadoObjeto.Novo);
                    repRest.SalvarTipoAlimentacaoRestaurante(acessoRTAli);
                    acessoR.TiposAlimentacao.Add(acessoRTAli);
                    restAdd.Add(acessoRTAli.TipoAlimentacao.Restaurante);
                }
            }

            foreach (var item in restDel)
            {
                if (acessoR.TiposAlimentacao.Where(t => t.Ativo && t.TipoAlimentacao.Restaurante.Codigo == item.Codigo).Count() < 1)
                {
                    List<Controladora> listC = new List<Controladora>();
                    List<GrupoLeitora> lGrupoL = new List<GrupoLeitora>();
                    lGrupoL.Add(item.GrupoLeitora);
                    listC = repGrupoAcesso.ObterControladoras(lGrupoL);
                    repSincronizaPessoaAcesso.Sincronizar(acessoR.Papel, TipoSincronizacaoPessoa.Suspender, null, listC);
                }
            }

            foreach (var item in restAdd)
            {
                if (acessoR.TiposAlimentacao.Any(t => t.Ativo && t.TipoAlimentacao.Restaurante.Codigo == item.Codigo))
                {
                    List<Controladora> listC = new List<Controladora>();
                    List<GrupoLeitora> lGrupoL = new List<GrupoLeitora>();
                    lGrupoL.Add(item.GrupoLeitora);
                    listC = repGrupoAcesso.ObterControladoras(lGrupoL);
                    repSincronizaPessoaAcesso.Sincronizar(acessoR.Papel, TipoSincronizacaoPessoa.Conceder, null, listC);
                }
            }


            return Json(montaObj(acessoR));
        }

        public ActionResult Selecionar(int codigo)
        {
            IRepositorioAcessoRestaurante repRest = Fabrica.Instancia.ObterRepositorio<IRepositorioAcessoRestaurante>(UnidadeTrabalho);
            AcessoRestaurante aRestaurante = repRest.ObterPorId(codigo);

            return Json(montaObj(aRestaurante));
        }

        [Transaction]
        public ActionResult AtivarInativar(int codigo)
        {
            IRepositorioAcessoRestaurante repRest = Fabrica.Instancia.ObterRepositorio<IRepositorioAcessoRestaurante>(UnidadeTrabalho);


            AcessoRestaurante aRestaurante = repRest.ObterPorId(codigo);
            if (aRestaurante.Ativo)
            {
                repRest.Validar(aRestaurante, EstadoObjeto.Inativado);
                aRestaurante.Inativar();
                sincronizaAcesso(aRestaurante, false);

            }
            else
            {
                repRest.Validar(aRestaurante, EstadoObjeto.Ativado);
                aRestaurante.Ativar();
                sincronizaAcesso(aRestaurante, true);
            }

            return Json(montaObj(aRestaurante));
        }

        private void sincronizaAcesso(AcessoRestaurante aRestaurante, bool liberaAcesso)
        {
            IRepositorioSincronizaPessoaAcesso repSincronizaPessoaAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioSincronizaPessoaAcesso>(UnidadeTrabalho);
            IRepositorioGrupoAcesso repGrupoAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoAcesso>(UnidadeTrabalho);

            foreach (var item in aRestaurante.TiposAlimentacao.Where(arta => arta.Ativo))
            {
                List<Controladora> listC = new List<Controladora>();
                List<GrupoLeitora> lGrupoL = new List<GrupoLeitora>();
                lGrupoL.Add(item.TipoAlimentacao.Restaurante.GrupoLeitora);
                listC = repGrupoAcesso.ObterControladoras(lGrupoL);

                if (liberaAcesso)
                    repSincronizaPessoaAcesso.Sincronizar(aRestaurante.Papel, TipoSincronizacaoPessoa.Conceder, null, listC);
                else
                    repSincronizaPessoaAcesso.Sincronizar(aRestaurante.Papel, TipoSincronizacaoPessoa.Suspender, null, listC);
            }
        }

        private List<AcessoRestauranteModelView> montaLIsta(IQueryable<AcessoRestaurante> acesso, StoreRequestParameters parameters = null, bool ignoreParameters = false)
        {
            if (!ignoreParameters)
            {
                if (parameters == null)
                    parameters = new StoreRequestParameters();
                acesso = acesso.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 25);
            }
            List<AcessoRestauranteModelView> list = new List<AcessoRestauranteModelView>();
            foreach (var s in acesso)
            {
                AcessoRestauranteModelView acessoMV = new AcessoRestauranteModelView();
                acessoMV.Codigo = s.Codigo;
                if (s.Papel != null)
                {
                    acessoMV.Nome = s.Papel.PessoaFisica.Nome;
                    TipoPapel tipoPapel = s.Papel.ObterTipoPapel();
                    acessoMV.TipoPapel = tipoPapel.ObterDescricaoEnum();
                    acessoMV.StatusPapel = s.Papel.ObterLogPapel().Status.ObterDescricaoEnum();

                    if (tipoPapel == TipoPapel.Colaborador) acessoMV.Empresa = (s.Papel as Colaborador).Empresa.Nome;
                    else if (tipoPapel == TipoPapel.PrestadorServico)
                    {
                        if ((s.Papel as PrestadorServico).TipoPrestador == TipoPrestador.Temporario)
                            acessoMV.Empresa = (s.Papel as PrestadorServico).Empresa.Nome;
                        else
                            acessoMV.Empresa = (s.Papel as PrestadorServico).Fornecedor.NomeFantasia;
                    }
                    else if (tipoPapel == TipoPapel.Visitante) acessoMV.Empresa = (s.Papel as Visitante).Empresa;
                }
                var logAcesso = s.LogAcessoRestaurante.OrderByDescending(t => t.DataRegistro).FirstOrDefault();
                acessoMV.CrachaMestre = logAcesso.CrachaMestre;
                acessoMV.NaoValidaAcessoHierarquico = logAcesso.NaoValidaAcessoHierarquico;
                acessoMV.Ativo = s.Ativo;
                acessoMV.CpfPassaporte = s.Papel.PessoaFisica.CPFFormatado();
                
                Cracha cracha = Fabrica.Instancia.ObterRepositorio<IRepositorioCracha>(UnidadeTrabalho).ObterCracha(s.Papel.Codigo);
                acessoMV.Cracha = cracha == null ? string.Empty : cracha.Numero;


                acessoMV.TipoAlimentacaoRest = s.TiposAlimentacao == null ? null : s.TiposAlimentacao.Where(x => x.Ativo)
                    .Select(td => new AcessoRestauranteTipoAlimentacaoModelView
                    {
                        Codigo = td.TipoAlimentacao.Codigo,
                        CodRestaurante = td.TipoAlimentacao.Restaurante.Codigo,
                        CodTipoAlimento = td.TipoAlimentacao.TipoAlimentacao.Codigo,
                        TipoAlimentacao = td.TipoAlimentacao.TipoAlimentacao.Nome,
                        Restaurante = td.TipoAlimentacao.Restaurante.Nome
                    }).ToList();

                acessoMV.HistCrachaMestre = s.LogAcessoRestaurante == null ? null : s.LogAcessoRestaurante
                    .Select(td => new LogAcessoRestaurante
                    {
                        CrachaMestre = td.CrachaMestre,
                        NaoValidaAcessoHierarquico = td.NaoValidaAcessoHierarquico,
                        DataRegistro = td.DataRegistro
                    }).ToList();
                list.Add(acessoMV);
            }
            //return acessoMV;
            return list;
        }

        private AcessoRestauranteModelView montaObj(AcessoRestaurante acesso)
        {
            AcessoRestauranteModelView acessoMV = new AcessoRestauranteModelView();
            acessoMV.Codigo = acesso.Codigo;
            acessoMV.Nome = acesso.Papel != null ? acesso.Papel.PessoaFisica.Nome : null;
            var logAcesso = acesso.LogAcessoRestaurante.OrderByDescending(t => t.DataRegistro).FirstOrDefault();
            acessoMV.CrachaMestre = logAcesso.CrachaMestre;
            acessoMV.NaoValidaAcessoHierarquico = logAcesso.NaoValidaAcessoHierarquico;
            acessoMV.Ativo = acesso.Ativo;
            acessoMV.CpfPassaporte = acesso.Papel.PessoaFisica.CPFFormatado();

            Cracha cracha = Fabrica.Instancia.ObterRepositorio<IRepositorioCracha>(UnidadeTrabalho).ObterCracha(acesso.Papel.Codigo);
            acessoMV.Cracha = cracha == null ? string.Empty : cracha.Numero;

            if (acesso.Papel is Colaborador)
                acessoMV.Empresa = (acesso.Papel as Colaborador).Empresa.Nome;
            if (acesso.Papel is PrestadorServico)
            {
                if ((acesso.Papel as PrestadorServico).TipoPrestador == TipoPrestador.Temporario)
                {
                    acessoMV.Empresa = (acesso.Papel as PrestadorServico).Empresa.Nome;
                }
                else
                {
                    acessoMV.Empresa = (acesso.Papel as PrestadorServico).Fornecedor.NomeFantasia;
                }
            }

            if (acesso.Papel is Visitante)
                acessoMV.Empresa = (acesso.Papel as Visitante).Empresa;

            acessoMV.TipoAlimentacaoRest = acesso.TiposAlimentacao == null ? null : acesso.TiposAlimentacao.Where(x => x.Ativo)
                .Select(td => new AcessoRestauranteTipoAlimentacaoModelView
                {
                    Codigo = td.TipoAlimentacao.Codigo,
                    CodRestaurante = td.TipoAlimentacao.Restaurante.Codigo,
                    CodTipoAlimento = td.TipoAlimentacao.TipoAlimentacao.Codigo,
                    TipoAlimentacao = td.TipoAlimentacao.TipoAlimentacao.Nome,
                    Restaurante = td.TipoAlimentacao.Restaurante.Nome
                }).ToList();

            acessoMV.HistCrachaMestre = acesso.LogAcessoRestaurante == null ? null : acesso.LogAcessoRestaurante
                .Select(td => new LogAcessoRestaurante
                {
                    CrachaMestre = td.CrachaMestre,
                    NaoValidaAcessoHierarquico = td.NaoValidaAcessoHierarquico,
                    DataRegistro = td.DataRegistro
                }).ToList();

            return acessoMV;
        }
    }
}