﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Pessoas;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Acesso;
using Controlsys.Repositorios.Acesso;
using Controlsys.Repositorios.Pessoas;
using Controlsys.Repositorios.Seguranca;
using Ext.Net.Utilities;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using Controlsys.Dominio.Seguranca;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar grupo acessoes.
    /// </summary>
    [ExtendController]
    public class GrupoAcessoController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /GrupoAcesso/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.GrupoAcessoController.
        /// </summary>
        ///
        /// <param name="unidade">
        /// O(a) unidade.
        /// </param>
        public GrupoAcessoController(IUnidadeTrabalho unidade)
        {
            UnidadeTrabalho = unidade;
        }

        /// <summary>
        /// Inclui um novo grupoAcesso.
        /// </summary>
        ///
        /// <exception cref="Exception">
        /// Lançada quando uma condição de erro exception ocorre.
        /// </exception>
        ///
        /// <param name="grupoAcesso">
        /// O(a) grupo acesso.
        /// </param>
        /// <param name="pessoas">
        /// O(a) pessoas.
        /// </param>
        /// <param name="gruposLeitoras">
        /// O(a) grupos leitoras.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Incluir(GrupoAcesso grupoAcesso, string[] pessoas, int[] gruposLeitoras)
        {
            IRepositorioGrupoAcesso repGrupoAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoAcesso>(UnidadeTrabalho);
            IRepositorioSincronizaPessoaAcesso repSincronizaPessoaAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioSincronizaPessoaAcesso>(UnidadeTrabalho);
            IRepositorioGrupoLeitora repGrupoLeitora = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoLeitora>(UnidadeTrabalho);
            IRepositorioGrupoLeitoraAcesso repGrupoLeitoraAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoLeitoraAcesso>(UnidadeTrabalho);

            List<Papel> papeisNew = new List<Papel>();
            List<GrupoLeitora> listGruposLeitoras = repGrupoLeitora.ObterTodos().Where(x => gruposLeitoras.Contains(x.Codigo)).ToList();
            try
            {
                UnidadeTrabalho.BeginTransaction();

                #region [GrupoAcesso]

                grupoAcesso.DataRegistro = DateTime.Now;
                grupoAcesso.Ativar();
                repGrupoAcesso.Validar(grupoAcesso, EstadoObjeto.Novo);
                repGrupoAcesso.Salvar(grupoAcesso);
                #endregion

                #region [GrupoLeitoraAcesso]

                foreach (GrupoLeitora grupoLeitora in listGruposLeitoras)
                {
                    GrupoLeitoraAcesso grupoLeitoraAcesso = new GrupoLeitoraAcesso();
                    grupoLeitoraAcesso.GrupoAcesso = grupoAcesso;
                    grupoLeitoraAcesso.GrupoLeitora = grupoLeitora;
                    grupoLeitoraAcesso.DataRegistro = DateTime.Now;
                    grupoLeitoraAcesso.Ativar();
                    repGrupoLeitoraAcesso.Salvar(grupoLeitoraAcesso);
                }
                #endregion

                #region [GrupoAcessoPapel]
                IRepositorioGrupoAcessoPapel repGrupoAcessoPapel = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoAcessoPapel>(UnidadeTrabalho);
                IRepositorioPessoaFisica repPessoaFisica = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaFisica>(UnidadeTrabalho);

                foreach (PessoaFisica pessoaFisica in repPessoaFisica.ObterTodos().Where(x => pessoas.Contains(x.Codigo)))
                {
                    GrupoAcessoPapel grupoAcessoPapel = new GrupoAcessoPapel();
                    grupoAcessoPapel.GrupoAcesso = grupoAcesso;
                    grupoAcessoPapel.Papel = pessoaFisica.ObterPapel(false, true, true, true, true);
                    //grupoAcessoPapel.Papel = pessoaFisica.ObterPapel(true);
                    grupoAcessoPapel.DataRegistro = DateTime.Now;
                    grupoAcessoPapel.Ativar();
                    repGrupoAcessoPapel.Validar(EstadoObjeto.Novo, grupoAcessoPapel);
                    repGrupoAcessoPapel.Salvar(grupoAcessoPapel);

                    papeisNew.Add(grupoAcessoPapel.Papel);
                }
                #endregion
                UnidadeTrabalho.Commit();
            }
            catch (Exception ex)
            {
                UnidadeTrabalho.Rollback();
                throw ex;
            }
            finally
            {
                UnidadeTrabalho.Dispose();
            }

            try
            {
                List<GrupoLeitora> listaGrupoLeitorasValidaCNH = listGruposLeitoras.Where(gl => gl.ValidaCNH).ToList();
                List<GrupoLeitora> listaGrupoLeitorasNaoValidaCNH = listGruposLeitoras.Where(gl => !gl.ValidaCNH).ToList();

                foreach (var papel in papeisNew)
                {
                    UnidadeTrabalho.BeginTransaction();

                    DateTime? dataInicio = null;

                    if (papel is Visitante && (papel as Visitante).DataInicio > DateTime.Today)
                        dataInicio = (papel as Visitante).DataInicio;

                    if (!papel.PessoaFisica.CNHDataValidade.HasValue || papel.PessoaFisica.CNHDataValidade.Value.Date < DateTime.Now.Date)
                        repSincronizaPessoaAcesso.Sincronizar(papel, TipoSincronizacaoPessoa.Suspender, null, repGrupoAcesso.ObterControladoras(listaGrupoLeitorasValidaCNH));
                    else
                        repSincronizaPessoaAcesso.Sincronizar(papel, TipoSincronizacaoPessoa.Conceder, dataInicio, repGrupoAcesso.ObterControladoras(listaGrupoLeitorasValidaCNH));

                    repSincronizaPessoaAcesso.Sincronizar(papel, TipoSincronizacaoPessoa.Conceder, dataInicio, repGrupoAcesso.ObterControladoras(listaGrupoLeitorasNaoValidaCNH));

                    UnidadeTrabalho.Commit();
                }                
            }
            catch (Exception ex)
            {
                UnidadeTrabalho.Rollback();
                throw ex;
            }
            finally
            {
                UnidadeTrabalho.Dispose();
            }

            return Json(montaObj(grupoAcesso));
        }

        /// <summary>
        /// Altera um registro de grupoAcesso.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        /// <exception cref="Exception">
        /// Lançada quando uma condição de erro exception ocorre.
        /// </exception>
        ///
        /// <param name="grupoAcesso">
        /// O(a) grupo acesso.
        /// </param>
        /// <param name="propModificadas">
        /// O(a) property modificadas.
        /// </param>
        /// <param name="pessoasNew">
        /// O(a) pessoas new.
        /// </param>
        /// <param name="papeisDel">
        /// O(a) papeis delete.
        /// </param>
        /// <param name="gruposLeitorasNew">
        /// O(a) grupos leitoras new.
        /// </param>
        /// <param name="gruposLeitorasDel">
        /// O(a) grupos leitoras delete.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Alterar(GrupoAcesso grupoAcesso, string[] propModificadas, string[] pessoasNew, int[] papeisDel, int[] gruposLeitorasNew, int[] gruposLeitorasDel)
        {
            List<Papel> papeisConceder = new List<Papel>();
            List<Papel> papeisSuspender = new List<Papel>();

            IRepositorioSincronizaPessoaAcesso repSincronizaPessoaAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioSincronizaPessoaAcesso>(UnidadeTrabalho);
            IRepositorioGrupoAcesso repGrupoAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoAcesso>(UnidadeTrabalho);

            grupoAcesso = repGrupoAcesso.ObterPorId(grupoAcesso, propModificadas);

            #region [Buscando grupos de leitora que possui relação]
            var username = User.Identity.Name;
            IRepositorioGrupoUsuario repGrupoUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoUsuario>(UnidadeTrabalho);

            var gruposUsuarios = repGrupoUsuario.ObterTodos().Where(x => x.Usuario.Login == username && x.Ativo);

            var gruposLeitoraUsuario = gruposUsuarios.Select(x => x.Grupo)
                                              .Where(x => x.Ativo)
                                              .SelectMany(x => x.GruposLeitoras)
                                              .Select(x => x.GrupoLeitora)
                                              .Distinct()
                                              .ToList();

            var gruposLeitoraGrupo = grupoAcesso.GruposLeitora.Select(a => a.GrupoLeitora);

            #endregion

            #region [Alterar Grupo de acesso]
            try
            {
                UnidadeTrabalho.BeginTransaction();

                #region [Pessoa/Papel]
                if (pessoasNew != null || papeisDel != null)
                {
                    if (!grupoAcesso.GruposLeitora.Any(gla => gruposLeitoraUsuario.Any(glu => glu.Codigo == gla.GrupoLeitora.Codigo)))
                        throw new CoreException("Para alterar a lista de usuários, relacione ao menos um Grupo de Leitora, que possua permissão de acesso.");

                    IRepositorioGrupoAcessoPapel repGrupoAcessoPapel = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoAcessoPapel>(UnidadeTrabalho);

                    if (pessoasNew != null)
                    {
                        IRepositorioPessoaFisica repPessoaFisica = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaFisica>(UnidadeTrabalho);

                        foreach (PessoaFisica pessoaFisica in repPessoaFisica.ObterTodos().Where(x => pessoasNew.Contains(x.Codigo)))
                        {
                            GrupoAcessoPapel grupoAcessoPapel = new GrupoAcessoPapel();
                            grupoAcessoPapel.GrupoAcesso = grupoAcesso;
                            grupoAcessoPapel.Papel = pessoaFisica.ObterPapel(false,true,true,true,true);
                            grupoAcessoPapel.DataRegistro = DateTime.Now;
                            grupoAcessoPapel.Ativar();
                            repGrupoAcessoPapel.Validar(EstadoObjeto.Alterado, grupoAcessoPapel);
                            repGrupoAcessoPapel.Salvar(grupoAcessoPapel);

                            papeisConceder.Add(grupoAcessoPapel.Papel);
                        }
                    }

                    if (papeisDel != null)
                    {
                        if (grupoAcesso.Pessoas.Count == papeisDel.Count())
                            throw new CoreException("Não é possível salvar. Informe pelo menos uma pessoa.");

                        foreach (var item in grupoAcesso.Pessoas.Where(x => papeisDel.Any(tdd => tdd == x.Codigo)))
                        {
                            GrupoAcessoPapel grupoAcessoPapel = item;
                            grupoAcessoPapel.Inativar();
                            repGrupoAcessoPapel.Salvar(grupoAcessoPapel);

                            papeisSuspender.Add(grupoAcessoPapel.Papel);
                        }
                    }
                }
                #endregion

                #region [GrupoLeitoraAcesso]
                if (gruposLeitorasNew != null)
                {
                    IRepositorioGrupoLeitora repGrupoLeitora = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoLeitora>(UnidadeTrabalho);
                    IRepositorioGrupoLeitoraAcesso repGrupoLeitoraAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoLeitoraAcesso>(UnidadeTrabalho);
                    List<GrupoLeitora> gpsleitoras = repGrupoLeitora.ObterTodos().Where(x => gruposLeitorasNew.Contains(x.Codigo)).ToList();

                    foreach (GrupoLeitora grupoLeitora in gpsleitoras)
                    {
                        GrupoLeitoraAcesso grupoLeitoraAcesso = new GrupoLeitoraAcesso();
                        grupoLeitoraAcesso.GrupoAcesso = grupoAcesso;
                        grupoLeitoraAcesso.GrupoLeitora = grupoLeitora;
                        grupoLeitoraAcesso.DataRegistro = DateTime.Now;
                        grupoLeitoraAcesso.Ativar();
                        repGrupoLeitoraAcesso.Salvar(grupoLeitoraAcesso);
                    }

                    //foreach (var gp in grupoAcesso.Pessoas)
                    //{
                    //    repSincronizaPessoaAcesso.Sincronizar(gp.Papel, TipoSincronizacaoPessoa.Conceder, null, repGrupoAcesso.ObterControladoras(gpsleitoras));
                    //}                
                }


                if (gruposLeitorasDel != null)
                {
                    IRepositorioGrupoLeitoraAcesso repGrupoLeitoraAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoLeitoraAcesso>(UnidadeTrabalho);

                    List<GrupoLeitora> gruposLeitoraDel = grupoAcesso.GruposLeitora.Where(x => gruposLeitorasDel.Any(tdd => tdd == x.Codigo)).Select(a => a.GrupoLeitora).ToList();

                    foreach (var item in grupoAcesso.GruposLeitora.Where(x => gruposLeitorasDel.Any(tdd => tdd == x.Codigo)))
                    {
                        GrupoLeitoraAcesso grupoLeitoraAcesso = item;
                        grupoLeitoraAcesso.Inativar();
                        repGrupoLeitoraAcesso.Salvar(grupoLeitoraAcesso);
                    }

                    //foreach (var gp in grupoAcesso.Pessoas)
                    //{
                    //    repSincronizaPessoaAcesso.Sincronizar(gp.Papel, TipoSincronizacaoPessoa.Suspender, null, repGrupoAcesso.ObterControladoras(gruposLeitoraDel));
                    //}

                    if (gruposLeitoraUsuario.Any())
                    {
                        var naoPertence = gruposLeitoraDel.Where(a => !gruposLeitoraUsuario.Any(glu => glu.Codigo == a.Codigo)).ToList();
                        if (naoPertence.Any())
                            throw new CoreException((naoPertence.Count == 1 ?
                                "Você não possui acesso ao seguinte grupo de leitoras, para desativa-lo da lista:" :
                                "Você não possui acesso aos seguintes grupos de leitoras, para desativa-los da lista:") +
                                string.Concat(naoPertence.Select(x => "<br />- " + x.Nome).ToArray()));
                    }
                }




                if (papeisDel != null || pessoasNew != null || propModificadas != null)
                {
                    string userLogado = HttpContext.User.Identity.Name;
                    IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
                    Usuario usuario = repUsuario.ObterPorLogin(userLogado);

                    List<GrupoLeitora> gl = usuario.GruposUsuarios.SelectMany(gu => gu.Grupo.GruposLeitoras.Select(gr => gr.GrupoLeitora)).ToList();

                    var temTodas = gruposLeitoraUsuario.All(x => gl.Any(y => x.Codigo == y.Codigo));

                    if (!temTodas)
                        throw new CoreException(
                            "Você não possui acesso para remover essa pesssoa desse grupo de permissão de acesso!"
                            );

                }

                #endregion

                repGrupoAcesso.Validar(grupoAcesso, EstadoObjeto.Alterado);
                repGrupoAcesso.Atualizar(grupoAcesso);
                UnidadeTrabalho.Commit();
            }
            catch (Exception ex)
            {
                UnidadeTrabalho.Rollback();
                throw ex;
            }
            finally
            {
                UnidadeTrabalho.Dispose();
            }
            #endregion

            #region [Sincronizar Papeis]
            try
            {
                List<GrupoLeitora> ListaGrupoLeeitorasValidaCNH = gruposLeitoraGrupo.Where(gl => gl.ValidaCNH).ToList();
                List<GrupoLeitora> ListaGrupoLeeitorasNaoValidaCNH = gruposLeitoraGrupo.Where(gl => !gl.ValidaCNH).ToList();

                foreach (var papel in papeisConceder)
                {
                    UnidadeTrabalho.BeginTransaction();
                    DateTime? dataInicio = null;

                    if (papel is Visitante && (papel as Visitante).DataInicio > DateTime.Today)
                        dataInicio = (papel as Visitante).DataInicio;

                    if (!papel.PessoaFisica.CNHDataValidade.HasValue || papel.PessoaFisica.CNHDataValidade.Value.Date < DateTime.Now.Date)
                        repSincronizaPessoaAcesso.Sincronizar(papel, TipoSincronizacaoPessoa.Suspender, null, repGrupoAcesso.ObterControladoras(ListaGrupoLeeitorasValidaCNH));
                    else
                        repSincronizaPessoaAcesso.Sincronizar(papel, TipoSincronizacaoPessoa.Conceder, dataInicio, repGrupoAcesso.ObterControladoras(ListaGrupoLeeitorasValidaCNH));

                    repSincronizaPessoaAcesso.Sincronizar(papel, TipoSincronizacaoPessoa.Conceder, dataInicio, repGrupoAcesso.ObterControladoras(ListaGrupoLeeitorasNaoValidaCNH));

                    UnidadeTrabalho.Commit();
                }

                foreach (var papel in papeisSuspender)
                {
                    UnidadeTrabalho.BeginTransaction();
                    repSincronizaPessoaAcesso.Sincronizar(papel, TipoSincronizacaoPessoa.Suspender, null, repGrupoAcesso.ObterControladoras(gruposLeitoraGrupo));
                    UnidadeTrabalho.Commit();
                }
            }
            catch (Exception ex)
            {
                UnidadeTrabalho.Rollback();
                throw ex;
            }
            finally
            {
                UnidadeTrabalho.Dispose();
            }
            #endregion

            return Json(montaObj(grupoAcesso));
        }

        /// <summary>
        /// Ativar inativar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult AtivarInativar(int codigo)
        {
            IRepositorioGrupoAcesso repGrupoAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoAcesso>(UnidadeTrabalho);
            GrupoAcesso grupoAcesso = repGrupoAcesso.ObterPorId(codigo);
            if (grupoAcesso.Ativo)
            {
                grupoAcesso.Inativar();
            }
            else
            {
                repGrupoAcesso.Validar(grupoAcesso, EstadoObjeto.Ativado);
                grupoAcesso.Ativar();
            }
            repGrupoAcesso.Atualizar(grupoAcesso);
            return Json(montaObj(grupoAcesso));
        }

        /// <summary>
        /// Selecionar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Selecionar(int codigo)
        {
            IRepositorioGrupoAcesso repGrupoAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoAcesso>(UnidadeTrabalho);
            GrupoAcesso grupoAcesso = repGrupoAcesso.ObterPorId(codigo);
            return Json(montaObj(grupoAcesso));
        }

        /// <summary>
        /// Retorna todos os objetos do tipo GrupoAcesso.
        /// </summary>
        ///
        /// <param name="apenasAtivos">
        /// true to apenas ativos.
        /// </param>
        ///
        /// <returns>
        /// Detalhes do tipo de objeto ligado ao controller no formato JSON.
        /// </returns>
        public ActionResult ObterTodos(bool apenasAtivos = false)
        {
            IRepositorioGrupoAcesso repGrupoAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoAcesso>(UnidadeTrabalho);

            IQueryable<GrupoAcesso> gruposAcessos = repGrupoAcesso.ObterTodos();

            if (apenasAtivos)
            {
                gruposAcessos = gruposAcessos.Where(t => t.Ativo == true);
            }

            return Json(gruposAcessos.Select(a => new
            {
                a.Codigo,
                a.Nome,
                a.Ativo
            }).ToList(), JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// Cria um objeto contendo informações relacionadas ao conteudo gerenciado por
        /// GrupoAcessoController.
        /// </summary>
        ///
        /// <param name="grupoAcesso">
        /// O(a) grupo acesso.
        /// </param>
        ///
        /// <returns>
        /// Um(a) object.
        /// </returns>
        private object montaObj(GrupoAcesso grupoAcesso)
        {
            IRepositorioColaborador repColaborador = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);
            var obj = new GrupoAcessoModelView();
            obj.Codigo = grupoAcesso.Codigo;
            obj.Nome = grupoAcesso.Nome;
            obj.Ativo = grupoAcesso.Ativo;
            
            var pessoas = grupoAcesso.Pessoas.Take(25).OrderBy(p => p.Papel.PessoaFisica.Nome).Select(td => new PessoaModelView()
            {
                Codigo = td.Papel.PessoaFisica.Codigo,
                Nome = td.Papel.PessoaFisica.Nome,
                CodigoPapel = td.Codigo
            }).ToList();
            
            obj.GruposLeitoras = grupoAcesso.GruposLeitora.Select(td => new GrupoLeitoraModelView()
            {
                Codigo = td.GrupoLeitora.Codigo,
                Nome = td.GrupoLeitora.Nome,
                CodigoGrupoLeitoraAcesso = td.Codigo,
                AreaAlfandegada = td.GrupoLeitora.AreaAlfandegada,
            }).ToList();
            List<Controlsys.Interfaces.Models.PessoaModelView> listaPessoas = new List<Controlsys.Interfaces.Models.PessoaModelView>();
            for(int i = 0; i < pessoas.Count(); i++)
            {
                Colaborador colab = repColaborador.ObterTodos().Where(x=>x.PessoaFisica.Codigo == pessoas[i].Codigo).FirstOrDefault();
                if (colab == null || colab.DataValidade > System.DateTime.Now)
                    listaPessoas.Add(pessoas[i]);
            }
            obj.Pessoas = listaPessoas;
            return obj;
        }

        /// <summary>
        /// Pessoa grupo acesso.
        /// </summary>
        ///
        /// <param name="nome">
        /// O(a) nome.
        /// </param>
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult PessoaGrupoAcesso(string nome, int codigo)
        {
            IRepositorioGrupoAcesso repGrupoAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoAcesso>(UnidadeTrabalho);
            IRepositorioPapel repPessoaFisica = Fabrica.Instancia.ObterRepositorio<IRepositorioPapel>(UnidadeTrabalho);

            GrupoAcesso grupoAcesso = repGrupoAcesso.ObterPorId(codigo);

            List<Papel> listaPessoas = repPessoaFisica.ObterTodos().Where(p => p.PessoaFisica.Nome.Contains(nome)).ToList();

            var pessoa = grupoAcesso.Pessoas.Where(p => listaPessoas.Contains(p.Papel));

            return Json(pessoa.Select(td => new PessoaModelView()
            {
                Codigo = td.Papel.PessoaFisica.Codigo,
                Nome = td.Papel.PessoaFisica.Nome,
                CodigoPapel = td.Codigo
            }).ToList(), JsonRequestBehavior.AllowGet);

        }
    }
}