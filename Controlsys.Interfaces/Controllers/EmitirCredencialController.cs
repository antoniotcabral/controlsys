﻿using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using Controlsys.Dominio.CredenciamentoVeiculos;
using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Controlsys.Dominio.Seguranca;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Parametros;
using Controlsys.Repositorios.Empresas;
using Controlsys.Repositorios.Parametros;
using Controlsys.Repositorios.Pessoas;
using Controlsys.Repositorios.Seguranca;
using Ext.Net;
using Ext.Net.MVC;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class EmitirCredencialController : Controller, IControllerBase
    {
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        public EmitirCredencialController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        [ExcludeFromCodeCoverage]
        public ActionResult Index()
        {
            return View("Index", model: StatusCredencial.Recebida.ObterDescricaoEnum());
        }

        [Transaction]
        public ActionResult Incluir(CredencialVeiculo credencialVeiculo, int codCondutor, SituacaoCredencialVeiculo situacao, List<VeiculoCredencialVeiculoModelView> listaVeiculos)
        {
            IRepositorioCredencialVeiculo repCredencialVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioCredencialVeiculo>(UnidadeTrabalho);
            IRepositorioEmpresa repEmpresa = Fabrica.Instancia.ObterRepositorio<IRepositorioEmpresa>(UnidadeTrabalho);
            IRepositorioPedidoCompra repPedidoCompra = Fabrica.Instancia.ObterRepositorio<IRepositorioPedidoCompra>(UnidadeTrabalho);
            IRepositorioArea repArea = Fabrica.Instancia.ObterRepositorio<IRepositorioArea>(UnidadeTrabalho);
            IRepositorioPapel repPapel = Fabrica.Instancia.ObterRepositorio<IRepositorioPapel>(UnidadeTrabalho);
            IRepositorioModeloCredencial repModeloCredencial = Fabrica.Instancia.ObterRepositorio<IRepositorioModeloCredencial>(UnidadeTrabalho);
            IRepositorioTipoValidade repTipoValidade = Fabrica.Instancia.ObterRepositorio<IRepositorioTipoValidade>(UnidadeTrabalho);
            IRepositorioModeloVeiculo repModeloVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioModeloVeiculo>(UnidadeTrabalho);
            IRepositorioVeiculo repVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioVeiculo>(UnidadeTrabalho);
            IRepositorioVeiculoCredencialVeiculo repVeiculoCredencialVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioVeiculoCredencialVeiculo>(UnidadeTrabalho);
            IRepositorioMidia repMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioMidia>(UnidadeTrabalho);
            IRepositorioDocumentoMidia repDocumentoMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioDocumentoMidia>(UnidadeTrabalho);
            IRepositorioVeiculoMidia repVeiculoMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioVeiculoMidia>(UnidadeTrabalho);
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);

            var credencialVeiculoA = new CredencialVeiculo();
            credencialVeiculoA = credencialVeiculo;

            credencialVeiculoA.Empresa = repEmpresa.ObterPorId(credencialVeiculo.Empresa.Codigo);
            credencialVeiculoA.PedidoCompra = credencialVeiculo.PedidoCompra != null ? repPedidoCompra.ObterPorId(credencialVeiculo.PedidoCompra.Codigo) : null;
            credencialVeiculoA.Area = credencialVeiculo.Area.Codigo > 0 || credencialVeiculo.Area.Codigo != null ? repArea.ObterPorId(credencialVeiculo.Area.Codigo) : null;
            credencialVeiculoA.SubContratada = credencialVeiculo.SubContratada.Codigo != null ? repEmpresa.ObterPorId(credencialVeiculo.SubContratada.Codigo) : null;

            credencialVeiculoA.Condutor = repPapel.ObterPorId(codCondutor);

            credencialVeiculoA.ModeloCredencial = repModeloCredencial.ObterPorId(credencialVeiculo.ModeloCredencial.Codigo);
            credencialVeiculoA.TipoValidade = repTipoValidade.ObterPorId(credencialVeiculo.TipoValidade.Codigo);

            Usuario usuarioLogado = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name);
            credencialVeiculoA.Usuario = usuarioLogado;

            credencialVeiculoA.DataRegistro = DateTime.Now;
            repCredencialVeiculo.Validar(credencialVeiculoA, EstadoObjeto.Novo);
            repCredencialVeiculo.Salvar(credencialVeiculoA);

            situacao.Credencial = credencialVeiculoA;
            situacao.Status = StatusCredencial.Recebida;
            situacao = repCredencialVeiculo.SalvarStatus(situacao);
            credencialVeiculoA.Situacoes.Add(situacao);


            #region [Veículo]
            List<VeiculoCredencialVeiculo> vCredencialVeiculos = new List<VeiculoCredencialVeiculo>();
            Dictionary<string, string> listaArquivo = new Dictionary<string, string>();
            foreach (var vcv in listaVeiculos)
            {
                Veiculo veiculoA = new Veiculo();
                veiculoA.AnoFabricacao = vcv.Veiculo.AnoFabricacao;
                veiculoA.AnoModelo = vcv.Veiculo.AnoModelo;
                veiculoA.Cor = vcv.Veiculo.Cor;
                veiculoA.Placa = vcv.Veiculo.Placa.ToUpper();
                veiculoA.Modelo = repModeloVeiculo.ObterPorId(vcv.Veiculo.ModeloCodigo);
                veiculoA.DataRegistro = DateTime.Now;
                repVeiculo.Validar(veiculoA, EstadoObjeto.Novo);
                repVeiculo.Salvar(veiculoA);

                var veiculoCredencialVeiculo = new VeiculoCredencialVeiculo();
                veiculoCredencialVeiculo.DataRegistro = DateTime.Now;
                veiculoCredencialVeiculo.CredencialVeiculo = credencialVeiculoA;
                veiculoCredencialVeiculo.Veiculo = veiculoA;
                veiculoCredencialVeiculo.Ativar();
                repVeiculoCredencialVeiculo.Validar(veiculoCredencialVeiculo, EstadoObjeto.Novo);
                repVeiculoCredencialVeiculo.Salvar(veiculoCredencialVeiculo);

                if (!vcv.Midias.Any())
                {
                    vCredencialVeiculos.Add(veiculoCredencialVeiculo);
                    continue;
                }

                int cont = repVeiculoMidia.ObterTodos().Count(vm => vm.Veiculo.Placa.Equals(veiculoA.Placa)) + 1;
                foreach (var vm in vcv.Midias)
                {
                    string path = ConfigurationManager.AppSettings.Get("pathUploads");
                    if (!System.IO.Directory.Exists(path))
                        throw new CoreException("O diretório onde a midia seria armazanada não está acessivel.");

                    string nomeDisco = string.Format("PJ-{0}-{1}-{2}-{3}.{4}", credencialVeiculoA.Empresa.Codigo,
                        credencialVeiculoA.Empresa.CNPJ, veiculoA.Placa.Replace("-", ""), cont, vm.Extensao);

                    Midia midiaA = new Midia();
                    midiaA.DataRegistro = DateTime.Now;
                    midiaA.Extensao = vm.Extensao;
                    midiaA.MimeType = vm.MIME;
                    midiaA.NomeArquivo = nomeDisco;

                    repMidia.Validar(midiaA, EstadoObjeto.Novo);
                    repMidia.Salvar(midiaA);

                    var veiculoMidia = new VeiculoMidia();
                    veiculoMidia.Midia = midiaA;
                    veiculoMidia.Veiculo = veiculoA;
                    veiculoMidia.DataRegistro = DateTime.Now;
                    veiculoMidia.Ativar();
                    repVeiculoMidia.Validar(veiculoMidia, EstadoObjeto.Novo);
                    repVeiculoMidia.Salvar(veiculoMidia);

                    listaArquivo.Add(path + nomeDisco, vm.Arquivo);

                    veiculoCredencialVeiculo.Veiculo.Midias.Add(veiculoMidia);
                    repVeiculoCredencialVeiculo.Atualizar(veiculoCredencialVeiculo);
                    vCredencialVeiculos.Add(veiculoCredencialVeiculo);

                    cont++;
                }
            }
            foreach (var arquivo in listaArquivo)
            {
                System.IO.File.WriteAllBytes(arquivo.Key, Convert.FromBase64String(arquivo.Value));
            }
            #endregion

            credencialVeiculoA.Veiculos = vCredencialVeiculos;
            //repCredencialVeiculo.ExisteVeiculo(credencialVeiculoA);

            return Json(montaObj(credencialVeiculoA), JsonRequestBehavior.AllowGet);
        }

        [Transaction]
        public ActionResult Alterar(CredencialVeiculo credencialVeiculo, int codCondutor, List<VeiculoCredencialVeiculoModelView> listaVeiculos)
        {
            IRepositorioCredencialVeiculo repCredencialVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioCredencialVeiculo>(UnidadeTrabalho);
            IRepositorioEmpresa repEmpresa = Fabrica.Instancia.ObterRepositorio<IRepositorioEmpresa>(UnidadeTrabalho);
            IRepositorioPedidoCompra repPedidoCompra = Fabrica.Instancia.ObterRepositorio<IRepositorioPedidoCompra>(UnidadeTrabalho);
            IRepositorioArea repArea = Fabrica.Instancia.ObterRepositorio<IRepositorioArea>(UnidadeTrabalho);
            IRepositorioPapel repPapel = Fabrica.Instancia.ObterRepositorio<IRepositorioPapel>(UnidadeTrabalho);
            IRepositorioModeloCredencial repModeloCredencial = Fabrica.Instancia.ObterRepositorio<IRepositorioModeloCredencial>(UnidadeTrabalho);
            IRepositorioTipoValidade repTipoValidade = Fabrica.Instancia.ObterRepositorio<IRepositorioTipoValidade>(UnidadeTrabalho);
            IRepositorioModeloVeiculo repModeloVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioModeloVeiculo>(UnidadeTrabalho);
            IRepositorioVeiculo repVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioVeiculo>(UnidadeTrabalho);
            IRepositorioVeiculoCredencialVeiculo repVeiculoCredencialVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioVeiculoCredencialVeiculo>(UnidadeTrabalho);
            IRepositorioMidia repMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioMidia>(UnidadeTrabalho);
            IRepositorioVeiculoMidia repVeiculoMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioVeiculoMidia>(UnidadeTrabalho);
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);

            CredencialVeiculo credencialVeiculoA = repCredencialVeiculo.ObterPorId(credencialVeiculo.Codigo);

            #region [Dados da Empresa]
            credencialVeiculoA.Empresa = repEmpresa.ObterPorId(credencialVeiculo.Empresa.Codigo);
            credencialVeiculoA.PedidoCompra = credencialVeiculo.PedidoCompra != null ? repPedidoCompra.ObterPorId(credencialVeiculo.PedidoCompra.Codigo) : null;
            credencialVeiculoA.Area = credencialVeiculo.Area != null ? repArea.ObterPorId(credencialVeiculo.Area.Codigo) : null;
            credencialVeiculoA.SubContratada = credencialVeiculo.SubContratada == null || string.IsNullOrEmpty(credencialVeiculo.SubContratada.Codigo) ?
                null : repEmpresa.ObterPorId(credencialVeiculo.SubContratada.Codigo);
            credencialVeiculoA.Fornecedor = credencialVeiculo.Fornecedor;
            #endregion

            #region [Condutor]
            credencialVeiculoA.Condutor = repPapel.ObterPorId(codCondutor);
            //credencialVeiculoA.Condutor = repPapel.ObterPorId(credencialVeiculo.Condutor.Codigo);
            #endregion

            #region [Dados do Modelo da Credencial]
            credencialVeiculoA.ModeloCredencial = repModeloCredencial.ObterPorId(credencialVeiculo.ModeloCredencial.Codigo);
            credencialVeiculoA.TipoValidade = repTipoValidade.ObterPorId(credencialVeiculo.TipoValidade.Codigo);
            credencialVeiculoA.ValidadeDiasCredencial = credencialVeiculo.ValidadeDiasCredencial;
            credencialVeiculoA.DataValidadeLit = credencialVeiculo.DataValidadeLit;
            credencialVeiculoA.DataVencimentoSeguro = credencialVeiculo.DataVencimentoSeguro;
            credencialVeiculoA.Corporativo = credencialVeiculo.Corporativo;
            credencialVeiculoA.Observacao = credencialVeiculo.Observacao;
            #endregion

            credencialVeiculoA.DataVencimento = CalcularValidade(credencialVeiculoA);

            repCredencialVeiculo.Validar(credencialVeiculoA, EstadoObjeto.Alterado);
            repCredencialVeiculo.Atualizar(credencialVeiculoA);


            #region [Veículo]
            //List<string> listaArquivoDeletar = new List<string>();
            Dictionary<string, string> listaArquivoAdicionar = new Dictionary<string, string>();
            int contMidias = 0;

            //Inativando registro de Veículo e Mídia;
            var listaVcvAux = listaVeiculos.Select(v => v.Codigo).ToArray();
            var listaInativar = repVeiculoCredencialVeiculo.ObterTodos().Where(v => v.Ativo && credencialVeiculo.Codigo == v.CredencialVeiculo.Codigo && !listaVcvAux.Contains(v.Codigo));
            if (listaInativar.Any())
            {
                foreach (var vCredVeiDel in listaInativar)
                {
                    var vCredVeiDelAux = vCredVeiDel;
                    foreach (var veiculoMidia in vCredVeiDelAux.Veiculo.Midias)
                    {
                        var veiculoMidiaAux = new VeiculoMidia();
                        veiculoMidiaAux = veiculoMidia;
                        veiculoMidiaAux.Inativar();
                        repVeiculoMidia.Atualizar(veiculoMidiaAux);
                    }

                    vCredVeiDelAux.Inativar();
                    repVeiculoCredencialVeiculo.Atualizar(vCredVeiDelAux);
                }
            }

            //Ativando e Inativando Midias de veículos
            var listaAlterar = repVeiculoCredencialVeiculo.ObterTodos().Where(v => v.Ativo && listaVcvAux.Contains(v.Codigo));
            if (listaAlterar.Any())
            {
                #region [Inativar midias]
                foreach (var item in listaVeiculos)
                {
                    VeiculoCredencialVeiculoModelView item1 = item;
                    int codVeiculo = item1.Veiculo.Codigo;
                    int[] codsMidia = item1.Midias.Select(m => m.CodigoMidia).ToArray();
                    var listaVeiculoMidia = repVeiculoMidia.ObterTodos().Where(vm => vm.Ativo && codVeiculo == vm.Veiculo.Codigo && !codsMidia.Contains(vm.Midia.Codigo));
                    if (!listaVeiculoMidia.Any()) continue;
                    foreach (var veiculoMidia in listaVeiculoMidia)
                    {
                        var veiculoMidiaAux = new VeiculoMidia();
                        veiculoMidiaAux = veiculoMidia;
                        veiculoMidiaAux.Inativar();
                        repVeiculoMidia.Atualizar(veiculoMidiaAux);
                    }
                }
                #endregion

                foreach (var vCredVei in listaAlterar.Select(v => repVeiculoCredencialVeiculo.ObterPorId(v.Codigo)))
                {
                    VeiculoCredencialVeiculo vCredVeiAux = vCredVei;
                    var listaMidiasAdicionar = listaVeiculos.Where(v => v.Codigo == vCredVeiAux.Codigo).SelectMany(vcv => vcv.Midias).Where(v => v.CodigoMidia == 0);
                    var midiaModelViews = listaMidiasAdicionar as MidiaModelView[] ?? listaMidiasAdicionar.ToArray();
                    if (!midiaModelViews.Any()) continue;

                    contMidias = repVeiculoMidia.ObterTodos().Where(vm => vm.Veiculo.Codigo == vCredVeiAux.Veiculo.Codigo).ToList().Count();
                    foreach (var vm in midiaModelViews)
                    {
                        contMidias = contMidias + 1;
                        string nomeDisco = string.Format("PJ-{0}-{1}-{2}-{3}.{4}", credencialVeiculoA.Empresa.Codigo, credencialVeiculoA.Empresa.CNPJ,
                           vCredVei.Veiculo.Placa.ToUpper(), contMidias, vm.Extensao);

                        Midia midiaA = new Midia();
                        midiaA.DataRegistro = DateTime.Now;
                        midiaA.Extensao = vm.Extensao;
                        midiaA.MimeType = vm.MIME;
                        midiaA.NomeArquivo = nomeDisco;
                        repMidia.Validar(midiaA, EstadoObjeto.Novo);
                        repMidia.Salvar(midiaA);

                        var veiculoMidia = new VeiculoMidia();
                        veiculoMidia.Midia = midiaA;
                        veiculoMidia.Veiculo = vCredVeiAux.Veiculo;
                        veiculoMidia.DataRegistro = DateTime.Now;
                        veiculoMidia.Ativar();
                        repVeiculoMidia.Validar(veiculoMidia, EstadoObjeto.Novo);
                        repVeiculoMidia.Salvar(veiculoMidia);

                        listaArquivoAdicionar.Add(midiaA.Url, vm.Arquivo);
                        vCredVeiAux.Veiculo.Midias.Add(veiculoMidia);
                    }

                    //repVeiculoCredencialVeiculo.Atualizar(vCredVeiAux);
                }
            }

            //Inserindo novos registros
            if (listaVeiculos.Any(v => v.Codigo == 0))
            {
                foreach (var vcv in listaVeiculos.Where(vcv => vcv.Codigo == 0))
                {
                    Veiculo veiculoA = new Veiculo();
                    veiculoA.AnoFabricacao = vcv.Veiculo.AnoFabricacao;
                    veiculoA.AnoModelo = vcv.Veiculo.AnoModelo;
                    veiculoA.Cor = vcv.Veiculo.Cor;
                    veiculoA.Placa = vcv.Veiculo.Placa;
                    veiculoA.Modelo = repModeloVeiculo.ObterPorId(vcv.Veiculo.ModeloCodigo);
                    veiculoA.DataRegistro = DateTime.Now;
                    repVeiculo.Validar(veiculoA, EstadoObjeto.Novo);
                    repVeiculo.Salvar(veiculoA);

                    var veiculoCredencialVeiculo = new VeiculoCredencialVeiculo();
                    veiculoCredencialVeiculo.DataRegistro = DateTime.Now;
                    veiculoCredencialVeiculo.CredencialVeiculo = credencialVeiculoA;
                    veiculoCredencialVeiculo.Veiculo = veiculoA;
                    veiculoCredencialVeiculo.Ativar();
                    repVeiculoCredencialVeiculo.Validar(veiculoCredencialVeiculo, EstadoObjeto.Novo);
                    repVeiculoCredencialVeiculo.Salvar(veiculoCredencialVeiculo);

                    if (!vcv.Midias.Any()) continue;

                    //contMidias = vcv.Midias.Count();
                    contMidias = repVeiculoMidia.ObterTodos().Where(vm => vm.Veiculo.Codigo == veiculoA.Codigo).ToList().Count();
                    foreach (var vm in vcv.Midias)
                    {
                        contMidias = contMidias + 1;
                        string nomeDisco = string.Format("PJ-{0}-{1}-{2}.{3}", credencialVeiculoA.Empresa.Codigo, credencialVeiculoA.Empresa.CNPJ, contMidias, vm.Extensao);

                        Midia midiaA = new Midia();
                        midiaA.DataRegistro = DateTime.Now;
                        midiaA.Extensao = vm.Extensao;
                        midiaA.MimeType = vm.MIME;
                        midiaA.NomeArquivo = nomeDisco;
                        repMidia.Validar(midiaA, EstadoObjeto.Novo);
                        repMidia.Salvar(midiaA);

                        var veiculoMidia = new VeiculoMidia();
                        veiculoMidia.Midia = midiaA;
                        veiculoMidia.Veiculo = veiculoA;
                        veiculoMidia.DataRegistro = DateTime.Now;
                        veiculoMidia.Ativar();
                        repVeiculoMidia.Validar(veiculoMidia, EstadoObjeto.Novo);
                        repVeiculoMidia.Salvar(veiculoMidia);

                        listaArquivoAdicionar.Add(midiaA.Url, vm.Arquivo);
                        veiculoCredencialVeiculo.Veiculo.Midias.Add(veiculoMidia);
                    }
                    //repVeiculoCredencialVeiculo.Salvar(veiculoCredencialVeiculo);
                }
            }


            //if (listaArquivoDeletar.Any())
            //    foreach (var arquivo in listaArquivoDeletar)
            //        System.IO.File.Delete(string.Format("{0}{1}", path, arquivo));
            if (listaArquivoAdicionar.Any())
                foreach (var arquivo in listaArquivoAdicionar)
                    System.IO.File.WriteAllBytes(arquivo.Key, Convert.FromBase64String(arquivo.Value));
            #endregion


            //credencialVeiculoA.Veiculos = vCredencialVeiculos;

            //var credVeiAux = repCredencialVeiculo.ObterPorId(credencialVeiculoA.Codigo);

            var obj = montaObj(credencialVeiculoA);
            return Json(obj);
        }

        public ActionResult Selecionar(int codigo)
        {
            IRepositorioCredencialVeiculo repCredencialVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioCredencialVeiculo>(UnidadeTrabalho);
            CredencialVeiculo credencialVeiculo = repCredencialVeiculo.ObterPorId(codigo);

            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            Usuario usuarioLogado = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name, somenteAtivos: true);

            var obj = montaObj(credencialVeiculo, usuarioLogado);
            return Json(obj);
        }

        public ActionResult ObterTodos(StoreRequestParameters parameters = null, string codEmpresa = null, string nomeCondutor = null, string placa = null, int? codModeloCredencial = null, bool apenasAtivos = false,
            DateTime? validadeDataInicial = null, DateTime? validadeDataFinal = null, DateTime? inspecaoDataInicial = null, DateTime? inspecaoDataFinal = null)
        {
            IRepositorioCredencialVeiculo repCredencialVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioCredencialVeiculo>(UnidadeTrabalho);

            IQueryable<CredencialVeiculo> lista = repCredencialVeiculo.ObterTodos();

            if (!string.IsNullOrEmpty(codEmpresa))
                lista = lista.Where(sc => sc.Empresa.Codigo == codEmpresa);

            if (!string.IsNullOrEmpty(nomeCondutor))
                lista = lista.Where(sc => sc.Condutor.PessoaFisica.Nome.Contains(nomeCondutor));

            if (!string.IsNullOrEmpty(placa))
                lista = lista.Where(sc => sc.Veiculos.Any(v => v.Veiculo.Placa.Contains(placa)));

            if (codModeloCredencial.HasValue)
                lista = lista.Where(sc => sc.ModeloCredencial.Codigo == codModeloCredencial.Value);

            if (apenasAtivos)
                lista = lista.Where(sc => sc.Situacoes.OrderByDescending(scv => scv.Codigo).Select(s => s.Status).FirstOrDefault() != StatusCredencial.Devolvida);

            if (validadeDataInicial.HasValue)
                lista = lista.Where(sc => sc.DataVencimento >= validadeDataInicial.Value);

            if (validadeDataFinal.HasValue)
                lista = lista.Where(sc => sc.DataVencimento <= validadeDataFinal.Value);

            if (inspecaoDataInicial.HasValue)
                lista = lista.Where(c => c.Situacoes.Any(scv => scv.Status == StatusCredencial.Inspecionada && scv.DataRegistro >= inspecaoDataInicial.Value));

            if (inspecaoDataFinal.HasValue)
                lista = lista.Where(c => c.Situacoes.Any(scv => scv.Status == StatusCredencial.Inspecionada && scv.DataRegistro <= inspecaoDataFinal.Value));

            if (parameters != null)
            {
                int total = lista.Count();
                List<CredencialVeiculoModelView> data = montaObjts(lista, parameters);

                return new StoreResult(data, total);
            }

            return Json(lista.Select(r => montaObj(r, null)).ToList());
        }

        [Transaction]
        public ActionResult Inspecionar(int codigo, SituacaoCredencialVeiculo situacao)
        {
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            IRepositorioCredencialVeiculo repCredencialVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioCredencialVeiculo>(UnidadeTrabalho);
            var credencialVeiculo = repCredencialVeiculo.ObterPorId(codigo);

            situacao.Credencial = credencialVeiculo;
            situacao.Status = StatusCredencial.Inspecionada;
            situacao = repCredencialVeiculo.SalvarStatus(situacao);

            credencialVeiculo.Situacoes.Add(situacao);

            Usuario usuarioLogado = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name, somenteAtivos: true);
            var obj = montaObj(credencialVeiculo, usuarioLogado);
            return Json(obj);
        }

        [Transaction]
        public ActionResult Aprovar(int codigo, SituacaoCredencialVeiculo situacao)
        {
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            IRepositorioCredencialVeiculo repCredencialVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioCredencialVeiculo>(UnidadeTrabalho);
            var credencialVeiculo = repCredencialVeiculo.ObterPorId(codigo);

            situacao.Credencial = credencialVeiculo;
            situacao.Status = StatusCredencial.Aprovada;
            situacao = repCredencialVeiculo.SalvarStatus(situacao);

            credencialVeiculo.Situacoes.Add(situacao);

            credencialVeiculo.DataVencimento = CalcularValidade(credencialVeiculo);
            repCredencialVeiculo.Atualizar(credencialVeiculo);

            Usuario usuarioLogado = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name, somenteAtivos: true);
            var obj = montaObj(credencialVeiculo, usuarioLogado);
            return Json(obj);
        }

        [Transaction]
        public ActionResult Imprimir(int codigo, SituacaoCredencialVeiculo situacao)
        {
            //IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            IRepositorioCredencialVeiculo repCredencialVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioCredencialVeiculo>(UnidadeTrabalho);
            var credencialVeiculo = repCredencialVeiculo.ObterPorId(codigo);

            //int qtdVeiAtivos = credencialVeiculo.Veiculos.Count(v => v.Ativo);
            int qtdPaginas = credencialVeiculo.ImprimirVerso() ? 2 : 1;
            var status = credencialVeiculo.ObterSituacao().Status;
            if (status == StatusCredencial.Impressa || status == StatusCredencial.Devolvida) return Json(qtdPaginas);

            situacao.DataRegistro = DateTime.Now;
            situacao.Credencial = credencialVeiculo;
            situacao.Status = StatusCredencial.Impressa;
            situacao = repCredencialVeiculo.SalvarStatus(situacao);
            credencialVeiculo.Situacoes.Add(situacao);

            //Usuario usuarioLogado = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name, somenteAtivos: true);
            //var obj = montaObj(credencialVeiculo, usuarioLogado);

            //Retorna quantidade de veículos a serem impressos
            //return Json(qtdVeiAtivos);
            //return Json(new { QtdVeiAtivos = qtdVeiAtivos, CredencialVeiculo = credencialVeiculo });

            //Retorna quantidade de paginasa serem impressas
            return Json(qtdPaginas);
        }

        [Transaction]
        public ActionResult Devolver(int codigo, SituacaoCredencialVeiculo situacao)
        {
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            IRepositorioCredencialVeiculo repCredencialVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioCredencialVeiculo>(UnidadeTrabalho);
            var credencialVeiculo = repCredencialVeiculo.ObterPorId(codigo);

            situacao.Credencial = credencialVeiculo;
            situacao.Status = StatusCredencial.Devolvida;
            situacao = repCredencialVeiculo.SalvarStatus(situacao);

            credencialVeiculo.Situacoes.Add(situacao);

            Usuario usuarioLogado = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name, somenteAtivos: true);
            var obj = montaObj(credencialVeiculo, usuarioLogado);
            return Json(obj);
        }

        private string montaCredencial(string html, CredencialVeiculo entidade)
        {
            #region [Validações]
            if (string.IsNullOrEmpty(html))
                throw new CoreException("Não é possível montar credencial. Modelo nulo ou inválido.");
            var veiculosAtivos = entidade.Veiculos.Where(v => v.Ativo).ToList();
            html = html.Replace("PLACA 2", entidade.Veiculos.Count == 2 ? veiculosAtivos[1].Veiculo.Placa.ToUpper() : string.Empty);
            //if (entidade.DataVencimento == null && !entidade.Situacoes.Any(s => s.Status.Equals(StatusCredencial.Aprovada)))
            //    throw new CoreException("Não é possível montar credencial sem data de validade.");
            if (entidade.DataVencimento == null)
                return string.Empty;
            var inspecao = entidade.Situacoes.Where(s => s.Status == StatusCredencial.Inspecionada).FirstOrDefault();
            //if (inspecao == null)
            //    throw new CoreException("Não é possível montar credencial sem realizar inspeção."); 
            if (inspecao == null)
                return string.Empty;
            #endregion

            html = html.Replace("PLACA 1", veiculosAtivos[0].Veiculo.Placa.ToUpper());
            html = html.Replace("VALIDADE", entidade.DataVencimento.Value.ToString("dd/MM/yyyy"));
            html = html.Replace("NÚMERO REGISTRO", entidade.Codigo.ToString(CultureInfo.InvariantCulture));
            html = html.Replace("DATA INSPEÇÃO", string.Format("Inspeção: {0}", inspecao.DataRegistro.ToString("dd/MM/yyyy")));
            html = html.Replace("CORPORATIVO (S/N)", string.Format("Veículo {0}", entidade.Corporativo ? "corporativo" : "não corporativo"));
            html = html.Replace("CONDUTOR", string.Format("Condutor: {0}", entidade.Condutor.PessoaFisica.Nome));
            html = html.Replace("EMPRESA", string.Format("Empresa: {0}", entidade.Empresa.Nome));
            html = html.Replace("ÁREA", entidade.Area != null ? string.Format("Área: {0} - {1}", entidade.Area.Numero, entidade.Area.Nome) : "Área:");

            return html;
        }

        public CredencialVeiculoModelView montaObj(CredencialVeiculo entidade, Usuario usuarioLogado = null)
        {
            var situacao = entidade.ObterSituacao();
            bool podeInspecionar = false;
            bool podeAprovar = false;
            bool podeImprimir = false;
            bool podeDevolver = false;
            string motivoNaoPodeImprimir = string.Empty;

            #region [Validando Permissões de Ações]
            if (usuarioLogado != null)
            {
                var grupos = usuarioLogado.GruposUsuarios.Select(g => g.Grupo);
                var acoes = grupos.SelectMany(g => g.Acoes);
                if (!(acoes.Where(a => a.Url == "/EmitirCredencial/Imprimir")).Any())
                    motivoNaoPodeImprimir = " Usuário não possui permissão para impressão.";

                switch (situacao.Status)
                {
                    case StatusCredencial.Recebida:
                        if ((acoes.Where(a => a.Url == "/EmitirCredencial/Inspecionar")).Any())
                            podeInspecionar = true;

                        motivoNaoPodeImprimir += " Credencial não foi aprovada";
                        break;
                    case StatusCredencial.Inspecionada:
                        if ((acoes.Where(a => a.Url == "/EmitirCredencial/Aprovar")).Any())
                            podeAprovar = true;

                        motivoNaoPodeImprimir += " Credencial não foi aprovada";
                        break;
                    case StatusCredencial.Aprovada:
                        if (string.IsNullOrEmpty(motivoNaoPodeImprimir))
                            podeImprimir = true;
                        break;
                    case StatusCredencial.Impressa:
                        if ((acoes.Where(a => a.Url == "/EmitirCredencial/Devolver")).Any())
                            podeDevolver = true;

                        if (string.IsNullOrEmpty(motivoNaoPodeImprimir))
                            podeImprimir = true;
                        break;
                    case StatusCredencial.Devolvida:
                        if (string.IsNullOrEmpty(motivoNaoPodeImprimir))
                            podeImprimir = true;
                        break;
                    default:
                        break;
                }
            }
            #endregion

            var credObj = new CredencialVeiculoModelView();

            credObj.Codigo = entidade.Codigo;
            credObj.DataRegistro = entidade.DataRegistro;
            credObj.Vencimento = (DateTime?)entidade.DataVencimento;
            credObj.Usuario = entidade.Usuario.PessoaFisica.Nome;

            #region [Status]

            credObj.Status = situacao.Status.ObterDescricaoEnum();
            credObj.DataStatus = situacao.DataRegistro;
            credObj.ObservacaoStatus = situacao.Observacao;

            #endregion

            #region [Dados da Empresa]
            credObj.Empresa = new EmpresaModelView()
            {
                Codigo = entidade.Empresa.Codigo,
                Nome = entidade.Empresa.Nome
            };
            if (entidade.PedidoCompra != null)
                credObj.PedidoCompra = new PedidoCompraModelView()
                {
                    Codigo = entidade.PedidoCompra.Codigo,
                    NumeroNome = string.Format("{0} {1}", entidade.PedidoCompra.Numero, entidade.PedidoCompra.Nome)
                };
            if (entidade.Area != null)
            {
                credObj.Area = new Area()
                {
                    Codigo = entidade.Area.Codigo,
                    Nome = entidade.Area.Nome,
                };
            }
            if (entidade.SubContratada != null)
            {
                credObj.SubContratada = new EmpresaModelView()
                {
                    Codigo = entidade.SubContratada.Codigo,
                    Nome = entidade.SubContratada.Nome
                };
            }
            credObj.Fornecedor = entidade.Fornecedor;
            #endregion

            #region [Dados do Condutor]

            var condutor = new CondutorModelView();

            condutor.CodigoPapel = entidade.Condutor.Codigo;
            condutor.Nome = entidade.Condutor.PessoaFisica.Nome;
            condutor.CPF = entidade.Condutor.PessoaFisica.CPF;
            condutor.Passaporte = entidade.Condutor.PessoaFisica.Passaporte;
            condutor.Cargo = (entidade.Condutor is Colaborador)
                ? (entidade.Condutor as Colaborador).Cargo.Nome
                : ((entidade.Condutor is PrestadorServico)
                    ? (entidade.Condutor as PrestadorServico).Cargo.Nome
                    : ((entidade.Condutor is Visitante) ? (entidade.Condutor as Visitante).Funcao.ToString() : null)); 
            condutor.Empresa = (entidade.Condutor is Colaborador)
                ? (entidade.Condutor as Colaborador).Empresa.Nome
                : ((entidade.Condutor is PrestadorServico)
                    ? (entidade.Condutor as PrestadorServico).Empresa.Nome
                    : ((entidade.Condutor is Visitante) ? (entidade.Condutor as Visitante).Empresa.ToString() : null));
            condutor.CNH = entidade.Condutor.PessoaFisica.CNH;
            condutor.CNHVencimento =
                entidade.Condutor.PessoaFisica.CNHDataValidade.HasValue
                    ? entidade.Condutor.PessoaFisica.CNHDataValidade.Value
                    : (DateTime?)null;

            credObj.Condutor = condutor;



            //credObj.Condutor = new CondutorModelView()
            //{
            //    CodigoPapel = entidade.Condutor.Codigo,
            //    Nome = entidade.Condutor.PessoaFisica.Nome,
            //    CPF = entidade.Condutor.PessoaFisica.CPF,
            //    Passaporte = entidade.Condutor.PessoaFisica.Passaporte,
            //    Cargo = (entidade.Condutor is Colaborador)
            //        ? (entidade.Condutor as Colaborador).Cargo.Nome
            //        : ((entidade.Condutor is PrestadorServico)
            //            ? (entidade.Condutor as PrestadorServico).Cargo.Nome
            //            : ((entidade.Condutor is Visitante) ? (entidade.Condutor as Visitante).Cargo.ToString() : null)),
            //    Empresa = (entidade.Condutor is Colaborador)
            //        ? (entidade.Condutor as Colaborador).Empresa.Nome
            //        : ((entidade.Condutor is PrestadorServico)
            //            ? (entidade.Condutor as PrestadorServico).Empresa.Nome
            //            : ((entidade.Condutor is Visitante) ? (entidade.Condutor as Visitante).Empresa.ToString() : null)),
            //    CNH = entidade.Condutor.PessoaFisica.CNH,
            //    CNHVencimento =
            //        entidade.Condutor.PessoaFisica.CNHDataValidade.HasValue
            //            ? entidade.Condutor.PessoaFisica.CNHDataValidade.Value
            //            : (DateTime?)null
            //};
            #endregion

            #region [Dados Modelo Credencial]

            credObj.ImprimirVerso = entidade.ImprimirVerso();

            credObj.ModeloCredencial = new ModeloCredencialModelView()
            {
                Codigo = entidade.ModeloCredencial.Codigo,
                Nome = entidade.ModeloCredencial.Nome,
                TipoCredencial = new TipoCredencialModelView()
                {
                    Codigo = entidade.ModeloCredencial.TipoCredencial,
                    Nome = entidade.ModeloCredencial.TipoCredencial.ObterDescricaoEnum()
                },
                HtmlFrente = montaCredencial(entidade.ModeloCredencial.HtmlFrente, entidade),
                HtmlVerso = entidade.ImprimirVerso() ? montaCredencial(entidade.ModeloCredencial.HtmlVerso, entidade) : "",
                FundoFrente = Convert.ToBase64String(entidade.ModeloCredencial.FundoFrente ?? new byte[] { }),
                FundoVerso = Convert.ToBase64String(entidade.ModeloCredencial.FundoVerso ?? new byte[] { }),
                Altura = entidade.ModeloCredencial.Altura,
                Largura = entidade.ModeloCredencial.Largura
            };
            credObj.TipoValidade = new TipoValidade()
            {
                Codigo = entidade.TipoValidade.Codigo,
                Nome = entidade.TipoValidade.Nome
            };
            credObj.ValidadeDiasCredencial = entidade.ValidadeDiasCredencial;
            credObj.DataValidadeLit = entidade.DataValidadeLit;
            credObj.DataVencimentoSeguro = entidade.DataVencimentoSeguro;
            credObj.Corporativo = entidade.Corporativo;
            credObj.Observacao = entidade.Observacao;
            #endregion

            #region [Dados de Veiculo]

            var veiculosAtivos = entidade.Veiculos.Where(v => v.Ativo).ToList();
            credObj.Veiculo1 = veiculosAtivos[0].Veiculo.Placa;
            credObj.Veiculo2 = veiculosAtivos.Count > 1 ? entidade.Veiculos[1].Veiculo.Placa : "";

            credObj.Veiculos = entidade.Veiculos.Where(v => v.Ativo).Select(vcv => new VeiculoCredencialVeiculoModelView()
            {
                Codigo = vcv.Codigo,
                Ativo = vcv.Ativo,
                Veiculo = new VeiculoModelView()
                {
                    Codigo = vcv.Veiculo.Codigo,
                    Placa = vcv.Veiculo.Placa,
                    FabricanteCodigo = vcv.Veiculo.Modelo.Fabricante.Codigo,
                    FabricanteNome = vcv.Veiculo.Modelo.Fabricante.Nome,
                    ModeloCodigo = vcv.Veiculo.Modelo.Codigo,
                    ModeloNome = vcv.Veiculo.Modelo.Nome,
                    AnoModelo = vcv.Veiculo.AnoModelo,
                    AnoFabricacao = vcv.Veiculo.AnoFabricacao,
                    Cor = vcv.Veiculo.Cor
                },
                Midias = vcv.Veiculo.Midias.Select(vm => new MidiaModelView()
                {
                    CodigoMidia = vm.Midia.Codigo,
                    DataRegistro = vm.Midia.DataRegistro,
                    NomeDisco = vm.Midia.NomeArquivo,
                    URL = vm.Midia.Url
                }).ToList()
            }).ToList();
            #endregion

            #region [Situações]

            credObj.Situacoes = entidade.Situacoes.Select(s => new SituacaoCredencialVeiculoModelView()
            {
                Codigo = s.Codigo,
                DataRegistro = s.DataRegistro,
                Status = s.Status.ObterDescricaoEnum(),
                Observacao = s.Observacao
            }).ToList();
            #endregion

            #region [Ações]

            credObj.PodeInspecionar = podeInspecionar;
            credObj.PodeAprovar = podeAprovar;
            credObj.PodeImprimir = podeImprimir;
            credObj.PodeDevolver = podeDevolver;
            credObj.MotivoNaoPodeDevolver = motivoNaoPodeImprimir;
            #endregion

            return credObj;
        }

        private List<CredencialVeiculoModelView> montaObjts(IQueryable<CredencialVeiculo> regs, StoreRequestParameters parameters = null, bool ignoreParameters = false)
        {
            if (!ignoreParameters)
            {
                if (parameters == null)
                    parameters = new StoreRequestParameters();
                regs = regs.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 25);
            }

            //regs.OrderBy(t => t.PessoaAg.Nome);
            return regs.Select(t => montaObj(t, null)).ToList();
        }

        private DateTime? CalcularValidade(CredencialVeiculo entidade)
        {
            DateTime? validade = null;

            if (!entidade.Situacoes.Any(s => s.Status == StatusCredencial.Aprovada))
                return null;
            //throw new CoreException("Não é possível calcular validade. Credencial pendente de aprovação.");

            DateTime dataAprovacao = entidade.Situacoes.Where(s => s.Status == StatusCredencial.Aprovada).First().DataRegistro;

            //Será somado a quantidade dias permitidos com a data de aprovação;
            if (entidade.ValidadeDiasCredencial.HasValue)
                validade = dataAprovacao.AddDays(Convert.ToDouble(entidade.ValidadeDiasCredencial.Value));
            else
            {
                //Será considerado o Tipo de Validade, cuja idade (ano atual menos menor ano do veículo (Fabricação/Modelo)), 
                //deverá estar entre a idade inicial e final previamente especificada no tipo de validade referente;
                int menorAno = 0;
                foreach (var veiculo in entidade.Veiculos.Select(sv => sv.Veiculo))
                {
                    var menorAnoAux = veiculo.AnoFabricacao < veiculo.AnoModelo ? veiculo.AnoFabricacao : veiculo.AnoModelo;
                    if (menorAno == 0)
                        menorAno = menorAnoAux;
                    else if (menorAnoAux < menorAno)
                        menorAno = menorAnoAux;
                }
                int idade = DateTime.Now.Year - menorAno;

                if (Enumerable.Range(entidade.TipoValidade.IdadeInicial, entidade.TipoValidade.IdadeFinal).Contains(idade))
                    validade = dataAprovacao.AddDays(entidade.TipoValidade.DiasValidade);

                //Caso o condutor não possua a data de validade para CNH
                if (!entidade.Condutor.PessoaFisica.CNHDataValidade.HasValue)
                    throw new CoreException("Não é possível calcular validade. Data de vencimento da CNH do condutor nula inválida.");

                //Caso validade da CNH seja inferior a validade previa informada
                if (validade == null || entidade.Condutor.PessoaFisica.CNHDataValidade.Value < validade.Value)
                    validade = entidade.Condutor.PessoaFisica.CNHDataValidade.Value;

                if (entidade.DataValidadeLit.HasValue && entidade.DataValidadeLit.Value < validade.Value)
                    validade = entidade.DataValidadeLit.Value;

                if (validade == null || entidade.DataVencimentoSeguro < validade.Value)
                    validade = entidade.DataVencimentoSeguro;
            }
            return validade;
        }

        public ActionResult ObterListaStatus()
        {
            Dictionary<int, string> dic = new Dictionary<int, string>();

            foreach (StatusCredencial entidade in Enum.GetValues(typeof(StatusCredencial)))
            {
                dic.Add((int)entidade, entidade.ObterDescricaoEnum());
            }

            return Json(dic.Select(d => new
            {
                Codigo = d.Key,
                Nome = d.Value
            }).ToArray());
        }


        /// <summary>
        /// Obter Detalhes do Veículo
        /// </summary>
        /// <returns></returns>
        public ActionResult ObterDetalhesVeiculos(bool apenasAtivos = false, string placa = null, int codigoCredencial = 0)
        {
            IRepositorioVeiculoCredencialVeiculo rep = Fabrica.Instancia.ObterRepositorio<IRepositorioVeiculoCredencialVeiculo>(UnidadeTrabalho);
            IQueryable<VeiculoCredencialVeiculo> vcvs = rep.ObterTodos();
            if (apenasAtivos)
            {
                vcvs = vcvs.Where(vcv => vcv.Ativo);
            }

            if (codigoCredencial > 0)
            {
                vcvs = vcvs.Where(vcv => vcv.CredencialVeiculo.Codigo == codigoCredencial);
            }

            if (!string.IsNullOrEmpty(placa))
            {
                vcvs = vcvs.Where(vcv => vcv.Veiculo.Placa.Replace("-", "").Contains(placa.Replace("-", "")));
            }

            return Json(vcvs
                .Select(vcv => new
                {
                    Codigo = vcv.Veiculo.Codigo,
                    CodigoCredencialVeiculo = vcv.CredencialVeiculo.Codigo,
                    Placa = vcv.Veiculo.Placa,
                    FabricanteCodigo = vcv.Veiculo.Modelo.Fabricante.Codigo,
                    FabricanteNome = vcv.Veiculo.Modelo.Fabricante.Nome,
                    ModeloCodigo = vcv.Veiculo.Modelo.Codigo,
                    ModeloNome = vcv.Veiculo.Modelo.Nome,
                    AnoModelo = vcv.Veiculo.AnoModelo,
                    AnoFabricacao = vcv.Veiculo.AnoFabricacao,
                    Cor = vcv.Veiculo.Cor
                })
                .ToList(), JsonRequestBehavior.AllowGet);
        }
    }
}
