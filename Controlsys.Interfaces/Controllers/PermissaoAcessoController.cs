﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Enderecos;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Seguranca;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Acesso;
using Controlsys.Repositorios.Parametros;
using Controlsys.Repositorios.Pessoas;
using Controlsys.Repositorios.Seguranca;
using Ext.Net;
using Ext.Net.Utilities;
using Globalsys;
using Globalsys.Validacao;
using Controlsys.Dominio.Pessoas;
using Controlsys.Repositorios.Acesso;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar permissao acessoes.
    /// </summary>
    [ExtendController]
    public class PermissaoAcessoController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /PermissaoAcesso/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.PermissaoAcessoController.
        /// </summary>
        ///
        /// <param name="unidade">
        /// O(a) unidade.
        /// </param>
        public PermissaoAcessoController(IUnidadeTrabalho unidade)
        {
            UnidadeTrabalho = unidade;
        }

        /// <summary>
        /// Retorna a página de permissaoAcesso.
        /// </summary>
        ///
        /// <param name="codigos">
        /// O(a) codigos.
        /// </param>
        /// <param name="unico">
        /// O(a) unico.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index(int[] codigos, bool? unico)
        {
            if (codigos == null || !codigos.Any() || unico == null) return View();

            return unico == true ?
                View("Index", model: new { Unico = true, Codigo = codigos[0] }) :
                View("Index", model: new { Unico = false, Lista = codigos });
        }

        /// <summary>
        /// Criar lista.
        /// </summary>
        ///
        /// <param name="codigos">
        /// O(a) codigos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult CriarLista(int[] codigos)
        {
            IRepositorioPapel repPapel = Fabrica.Instancia.ObterRepositorio<IRepositorioPapel>(UnidadeTrabalho);
            List<Papel> listaPapel = repPapel.ObterListaPapel(codigos);

            var lista = new { Pessoas = listaPapel.Select(u => montaObj(u)).ToList() };

            return Json(lista);
        }

        /// <summary>
        /// Obter grupos usuario.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [HttpPost]
        public ActionResult ObterGruposUsuario(int codigo)
        {
            IRepositorioPapel repositorio = Fabrica.Instancia.ObterRepositorio<IRepositorioPapel>(UnidadeTrabalho);
            Papel papel = repositorio.ObterPorId(codigo);
            return Json(montaObj(papel));
        }

        /// <summary>
        /// Cria um objeto contendo informações relacionadas ao conteudo gerenciado por
        /// PermissaoAcessoController.
        /// </summary>
        ///
        /// <param name="papel">
        /// O(a) papel.
        /// </param>
        /// <param name="imageUrl">
        /// URL of the image.
        /// </param>
        ///
        /// <returns>
        /// Um(a) object.
        /// </returns>
        private object montaObj(Papel papel, string imageUrl = null)
        {
            return new
            {
                Codigo = papel.PessoaFisica.Codigo,
                Nome = papel.PessoaFisica.Nome,
                CPF = papel.PessoaFisica.CPF != null ? papel.PessoaFisica.CPFFormatado() : null,
                CodigoPapel = papel.Codigo,
                GruposAcesso = ObterListaGruposUsuario(papel.Codigo)
            };
        }

        /// <summary>
        /// Obter lista grupos usuario.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;GrupoAcessoModelView&gt;
        /// </returns>
        private List<GrupoAcessoModelView> ObterListaGruposUsuario(int codigo)
        {
            IRepositorioGrupoAcessoPapel repAcessoPapel = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoAcessoPapel>(UnidadeTrabalho);
            var listaAcessoPapel = repAcessoPapel.ObterTodos().Where(x => x.Papel.Codigo == codigo && x.Ativo).ToList();
            var listaAcesso = listaAcessoPapel.Select(grupoAcessoPapel => grupoAcessoPapel.GrupoAcesso).ToList();
            return listaAcesso.Select(p => new GrupoAcessoModelView()
            {
                Codigo = p.Codigo,
                Nome = p.Nome
            }).ToList();
        }

        /// <summary>
        /// Obter grupos leitura.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ObterGruposLeitura()
        {
            var username = User.Identity.Name;
            IRepositorioGrupoUsuario repGrupoUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoUsuario>(UnidadeTrabalho);
            var gruposUsuarios = repGrupoUsuario.ObterTodos().Where(x => x.Usuario.Login == username && x.Ativo == true);
            var grupos = gruposUsuarios.Select(x => x.Grupo).Distinct().ToList().Where(x => x.Ativo == true);
            var gruposLeitoraGrupo = grupos.Select(x => x.GruposLeitoras);

            var grpLeitoras = new List<GrupoLeitora>();
            foreach (var grupoLeitoraGrupo in gruposLeitoraGrupo)
            {
                grpLeitoras.AddRange(grupoLeitoraGrupo.Select(x => x.GrupoLeitora));
            }

            grpLeitoras = grpLeitoras.Where(x => x.Ativo == true).Distinct().ToList();

            return Json(grpLeitoras.Select(a => new
            {
                a.Codigo,
                a.Nome,
                HoraInicio = a.HoraInicio.ToString(@"hh\:mm"),
                HoraFim = a.HoraFim.ToString(@"hh\:mm"),
                a.Ativo
            }).ToArray(), JsonRequestBehavior.AllowGet);
        }

    }
}
