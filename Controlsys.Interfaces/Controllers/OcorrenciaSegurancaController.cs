﻿using Controlsys.Dominio.CredenciamentoVeiculos;
using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Ocorrencias;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Controlsys.Dominio.Seguranca;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Ocorrencias;
using Controlsys.Repositorio.Parametros;
using Controlsys.Repositorios.Empresas;
using Controlsys.Repositorios.Pessoas;
using Controlsys.Repositorios.Seguranca;
using Ext.Net;
using Ext.Net.MVC;
using Ext.Net.Utilities;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using iTextSharp.text;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Controlador para tratar Ocorrências de Segurança
    /// </summary>
    [ExtendController]
    public class OcorrenciaSegurancaController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /OcorrenciaSeguranca/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.OcorrenciaSegurancaController.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public OcorrenciaSegurancaController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        /// <summary>
        /// Retorna a pagina de Ocorreência de Seguranças.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Ativar inativar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult AtivarInativar(int codigo)
        {
            IRepositorioOcorrenciaSeguranca rep = Fabrica.Instancia.ObterRepositorio<IRepositorioOcorrenciaSeguranca>(UnidadeTrabalho);
            OcorrenciaSeguranca ocorrencia = rep.ObterPorId(codigo);
            if (ocorrencia.Ativo)
            {
                ocorrencia.Inativar();
            }
            else
            {
                rep.Validar(ocorrencia, EstadoObjeto.Ativado);
                ocorrencia.Ativar();
            }
            rep.Atualizar(ocorrencia);
            return Json(montaObj(ocorrencia));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ocorrencia"></param>
        /// <param name="graduacoes"></param>
        /// <returns></returns>
        [Transaction]
        public ActionResult Incluir(OcorrenciaSeguranca ocorrencia, GraduacaoOcorrencia[] graduacoes, List<OcorrenciaSegurancaAgente> agentesOcorrencia, List<MidiaModelView> midias, List<Veiculo> veiculos)
        {
            IRepositorioOcorrenciaSeguranca rep = Fabrica.Instancia.ObterRepositorio<IRepositorioOcorrenciaSeguranca>(UnidadeTrabalho);
            IRepositorioUsuario repU = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            IRepositorioLocal repL = Fabrica.Instancia.ObterRepositorio<IRepositorioLocal>(UnidadeTrabalho);

            ocorrencia.Ativar();
            ocorrencia.Graduacoes(graduacoes);
            ocorrencia.DataRegistro = DateTime.Now;
            ocorrencia.Ativar();
            ocorrencia.Supervisor = repU.ObterPorLogin(HttpContext.User.Identity.Name);
            ocorrencia.Local = repL.ObterPorId(ocorrencia.Local.Codigo);

            rep.Validar(ocorrencia, EstadoObjeto.Novo);
            rep.Salvar(ocorrencia);

            #region agentes

            if (agentesOcorrencia != null && agentesOcorrencia.Count > 0)
            {
                IRepositorioPessoaFisica repP = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaFisica>(UnidadeTrabalho);
                IRepositorioOcorrenciaSegurancaAgente repA = Fabrica.Instancia.ObterRepositorio<IRepositorioOcorrenciaSegurancaAgente>(UnidadeTrabalho);
                foreach (var item in agentesOcorrencia)
                {
                    item.Ativar();
                    item.Ocorrencia = ocorrencia;
                    item.DataRegistro = DateTime.Now;
                    PessoaFisica pf = null;
                    if (string.IsNullOrEmpty(item.PessoaFisica.Codigo))
                    {
                        pf = new PessoaFisica();
                        pf.Nome = item.PessoaFisica.Nome;
                        pf.Apelido = item.PessoaFisica.Nome;
                        pf.CPF = item.PessoaFisica.CPF.Replace("-", "").Replace(".", ""); ;
                        pf.Passaporte = item.PessoaFisica.Passaporte;

                        repP.Salvar(pf, null);

                        IRepositorioTelefone repT = Fabrica.Instancia.ObterRepositorio<IRepositorioTelefone>(UnidadeTrabalho);

                        foreach (Telefone tel in item.PessoaFisica.Telefones)
                        {

                            tel.Ativar();
                            tel.DataRegistro = DateTime.Now;
                            tel.Pessoa = pf;

                            repT.Validar(EstadoObjeto.Novo, tel);
                            repT.Salvar(tel);
                        }
                    }
                    else
                    {
                        pf = repP.ObterPorId(item.PessoaFisica.Codigo);
                    }

                    item.PessoaFisica = pf;

                    repA.Validar(item, EstadoObjeto.Novo);
                    repA.Salvar(item);

                    ocorrencia.AgentesOcorrencia.Add(item);
                }
            }

            #endregion

            #region Midias

            if (midias != null && midias.Count > 0)
            {
                ocorrencia.Midias = new List<OcorrenciaSegurancaMidia>();

                string path = ConfigurationManager.AppSettings.Get("pathUploads");

                if (!System.IO.Directory.Exists(path))
                    throw new CoreException("O diretório onde a midia seria armazanada não está acessivel.");

                IRepositorioOcorrenciaSegurancaMidia repOcoSegMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioOcorrenciaSegurancaMidia>(UnidadeTrabalho);
                IRepositorioMidia repMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioMidia>(UnidadeTrabalho);

                int cont = 1;

                foreach (MidiaModelView midia in midias ?? Enumerable.Empty<MidiaModelView>())
                {
                    if (midia == null)
                        continue;

                    string nomeDisco = "OS-" + ocorrencia.Codigo + "-" + cont + "." + midia.Extensao;
                    Midia m = new Midia();
                    m.DataRegistro = midia.DataRegistro;
                    m.Extensao = midia.Extensao;
                    m.MimeType = midia.MIME;
                    m.NomeArquivo = nomeDisco;

                    repMidia.Salvar(m);

                    ocorrencia.Midias.Add(repOcoSegMidia.SalvarOcorrenciaSegurancaMidia(ocorrencia, m));

                    System.IO.File.WriteAllBytes(m.Url, Convert.FromBase64String(midia.Arquivo));

                    cont++;
                }

            }

            #endregion

            #region Veículos

            if (veiculos != null && veiculos.Count > 0)
            {
                IRepositorioOcorrenciaSegurancaVeiculo repOSV = Fabrica.Instancia.ObterRepositorio<IRepositorioOcorrenciaSegurancaVeiculo>(UnidadeTrabalho);

                foreach (Veiculo v in veiculos)
                {
                    if (v.Codigo > 0)
                    {
                        repOSV.SalvarOcorrenciaSegurancaVeiculo(ocorrencia, v);
                    }
                    else
                    {
                        v.DataRegistro = DateTime.Now;

                        IRepositorioVeiculo repV = Fabrica.Instancia.ObterRepositorio<IRepositorioVeiculo>(UnidadeTrabalho);

                        repV.Validar(v, EstadoObjeto.Novo);
                        repV.Salvar(v);
                        repOSV.SalvarOcorrenciaSegurancaVeiculo(ocorrencia, v);
                    }
                }
            }

            #endregion

            return Json(montaObj(ocorrencia));
        }

        /// <summary>
        /// Cria um objeto contendo informações relacionadas ao conteudo gerenciado por
        /// OcorrenciaSegurancaController.
        /// </summary>
        ///
        /// <param name="ocorrenciaSeguranca">
        /// O(a) ocorrenciaSeguranca.
        /// </param>
        ///
        /// <returns>
        /// Um(a) object.
        /// </returns>
        private object montaObj(OcorrenciaSeguranca ocorrenciaSeguranca)
        {
            var ocorrenciaSegModelView = new OcorrenciaSegurancaModelView();
            ocorrenciaSegModelView.Codigo = ocorrenciaSeguranca.Codigo;
            ocorrenciaSegModelView.DataRegistro = ocorrenciaSeguranca.DataRegistro;
            ocorrenciaSegModelView.DataRegistroFormatada = ocorrenciaSeguranca.DataRegistro.ToString("dd/MM/yyyy HH:mm:ss");
            ocorrenciaSegModelView.DataOcorrencia = ocorrenciaSeguranca.DataOcorrencia;
            ocorrenciaSegModelView.Descricao = ocorrenciaSeguranca.Descricao;
            ocorrenciaSegModelView.Efetiva = ocorrenciaSeguranca.Efetiva;
            ocorrenciaSegModelView.Tentativa = ocorrenciaSeguranca.Tentativa;
            ocorrenciaSegModelView.Vulnerabilidade = ocorrenciaSeguranca.Vulnerabilidade;
            ocorrenciaSegModelView.AcoesRecomendadas = ocorrenciaSeguranca.AcoesRecomendadas;
            ocorrenciaSegModelView.Procedimentos = ocorrenciaSeguranca.Procedimentos;
            ocorrenciaSegModelView.HoraOcorrencia = ocorrenciaSeguranca.HoraOcorrencia.TotalMilliseconds;
            ocorrenciaSegModelView.HoraOcorrenciaFormatada = ocorrenciaSeguranca.HoraOcorrencia.ToString("hh\\:mm");
            ocorrenciaSegModelView.TipoOcorrenciaDesc = (ocorrenciaSeguranca.TipoOcorrencia != null ? ocorrenciaSeguranca.TipoOcorrencia.Descricao.ToString() : "");
            ocorrenciaSegModelView.Local = (ocorrenciaSeguranca.Local != null ? string.Format("{0} - {1}", ocorrenciaSeguranca.Local.Sigla, ocorrenciaSeguranca.Local.Nome) : null);
            ocorrenciaSegModelView.Supervisor = (ocorrenciaSeguranca.Supervisor != null) ? ocorrenciaSeguranca.Supervisor.Login.ToString() : "";
            ocorrenciaSegModelView.Gravidade = ocorrenciaSeguranca.Gravidade.ObterDescricaoEnum();
            ocorrenciaSegModelView.Ativo = ocorrenciaSeguranca.Ativo;
            ocorrenciaSegModelView.Graduacao = ocorrenciaSegModelView.Graduacoes(ocorrenciaSeguranca.Tentativa,
                                                            ocorrenciaSeguranca.Vulnerabilidade,
                                                            ocorrenciaSeguranca.Efetiva);
            ocorrenciaSegModelView.TipoOcorrencia = ocorrenciaSeguranca.TipoOcorrencia.Codigo;
            ocorrenciaSegModelView.CodigoGravidade = (int)ocorrenciaSeguranca.Gravidade;
            ocorrenciaSegModelView.CodigoLocal = ocorrenciaSeguranca.Local.Codigo;

            ocorrenciaSegModelView.Midias = ocorrenciaSeguranca.Midias.Select(m => new MidiaModelView()
            {
                Arquivo = null,
                CodigoMidia = m.Midia.Codigo,
                DataRegistro = m.Midia.DataRegistro,
                Extensao = m.Midia.Extensao,
                MIME = m.Midia.MimeType,
                NomeDisco = m.Midia.NomeArquivo,
                NomeOriginal = string.Empty,
                URL = m.Midia.Url
            }).ToList();

            if (ocorrenciaSeguranca.Veiculos != null && ocorrenciaSeguranca.Veiculos.Count > 0)
            {

                ocorrenciaSegModelView.Veiculos = ocorrenciaSeguranca.Veiculos.Select(v => new OcorrenciaSegurancaVeiculoModelView()
                    {
                        AnoFabricacao = v.Veiculo.AnoFabricacao,
                        AnoModelo = v.Veiculo.AnoModelo,
                        Codigo = v.Veiculo.Codigo,
                        CodigoCredencialVeiculo = 0,
                        Cor = v.Veiculo.Cor,
                        FabricanteCodigo = v.Veiculo.Modelo.Fabricante.Codigo,
                        FabricanteNome = v.Veiculo.Modelo.Fabricante.Nome,
                        ModeloCodigo = v.Veiculo.Modelo.Codigo,
                        ModeloNome = v.Veiculo.Modelo.Nome,
                        Placa = v.Veiculo.Placa
                    }).ToList();
            }

            foreach (OcorrenciaSegurancaAgente os in ocorrenciaSeguranca.AgentesOcorrencia)
            {
                OcorrenciaSegurancaAgenteModelView osmv = new OcorrenciaSegurancaAgenteModelView();
                osmv.Codigo = os.Codigo;
                osmv.CodigoOcorrencia = os.Ocorrencia.Codigo;
                osmv.CodigoPessoa = os.PessoaFisica.Codigo;

                Papel p = os.PessoaFisica.ObterPapel();

                if (p != null)
                {
                    using (PessoaController pc = new PessoaController(UnidadeTrabalho))
                    {
                        osmv.Contratada = pc.obterContratada(p.Codigo);
                    }

                    osmv.Empresa = (p is Colaborador) ? (p as Colaborador).Empresa.Nome :
                            ((p is PrestadorServico) ? (p as PrestadorServico).Empresa.Nome :
                            ((p is Visitante) ? (p as Visitante).Empresa : null));

                    osmv.TipoPapel = (p is Colaborador) ? "Colaborador" : (p is PrestadorServico) ? "Prestador de Serviço" : "Visitante";

                }

                osmv.CPF = os.PessoaFisica.CPFFormatado();
                osmv.Cracha = os.Cracha;
                osmv.Nome = os.PessoaFisica.Nome;
                osmv.Passaporte = os.PessoaFisica.Passaporte;
                Telefone telefone = os.PessoaFisica.Telefones.Where(t => t.TipoTelefone == TipoTelefone.Residencial && t.Ativo).FirstOrDefault();
                Telefone celular = os.PessoaFisica.Telefones.Where(t => t.TipoTelefone == TipoTelefone.Celular && t.Ativo).FirstOrDefault();

                osmv.Telefone = telefone == null ? string.Empty : telefone.TelefoneNum;
                osmv.Celular = celular == null ? string.Empty : celular.TelefoneNum;
                osmv.TipoAgente = os.TipoAgente.ObterDescricaoEnum();

                ocorrenciaSegModelView.Agentes.Add(osmv);

            }


            return ocorrenciaSegModelView;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ocorrenciaSeguranca">
        /// A ocorrência
        /// </param>
        /// <returns></returns>
        [Transaction]
        public ActionResult Alterar(OcorrenciaSeguranca ocorrenciaSeguranca, GraduacaoOcorrencia[] graduacoes, List<OcorrenciaSegurancaAgente> agentesOcorrencia, List<MidiaModelView> midias, List<Veiculo> veiculos)
        {
            IRepositorioOcorrenciaSeguranca repOcorrenciaSeguranca = Fabrica.Instancia.ObterRepositorio<IRepositorioOcorrenciaSeguranca>(UnidadeTrabalho);
            IRepositorioLocal repLocal = Fabrica.Instancia.ObterRepositorio<IRepositorioLocal>(UnidadeTrabalho);
            IRepositorioTipoOcorrencia repTpOcorrencia = Fabrica.Instancia.ObterRepositorio<IRepositorioTipoOcorrencia>(UnidadeTrabalho);

            OcorrenciaSeguranca ocorrenciaSegurancaA = repOcorrenciaSeguranca.ObterPorId(ocorrenciaSeguranca.Codigo);

            ocorrenciaSegurancaA.Graduacoes(graduacoes);
            ocorrenciaSegurancaA.AcoesRecomendadas = ocorrenciaSeguranca.AcoesRecomendadas;
            ocorrenciaSegurancaA.DataOcorrencia = ocorrenciaSeguranca.DataOcorrencia;
            ocorrenciaSegurancaA.HoraOcorrencia = ocorrenciaSeguranca.HoraOcorrencia;
            ocorrenciaSegurancaA.Descricao = ocorrenciaSeguranca.Descricao;
            ocorrenciaSegurancaA.Local = repLocal.ObterPorNomeSigla(ocorrenciaSeguranca.Local.Nome);
            ocorrenciaSegurancaA.Procedimentos = ocorrenciaSeguranca.Procedimentos;
            ocorrenciaSegurancaA.Supervisor = ocorrenciaSegurancaA.Supervisor;
            ocorrenciaSegurancaA.TipoOcorrencia = repTpOcorrencia.ObterPorNome(ocorrenciaSeguranca.TipoOcorrencia.Descricao);
            ocorrenciaSegurancaA.Gravidade = ocorrenciaSeguranca.Gravidade;

            repOcorrenciaSeguranca.Validar(ocorrenciaSegurancaA, EstadoObjeto.Alterado);
            repOcorrenciaSeguranca.Atualizar(ocorrenciaSegurancaA);

            #region Agentes

            if (agentesOcorrencia != null && agentesOcorrencia.Count > 0)
            {
                IRepositorioOcorrenciaSegurancaAgente repA = Fabrica.Instancia.ObterRepositorio<IRepositorioOcorrenciaSegurancaAgente>(UnidadeTrabalho);

                int[] codigosAgentesOcorrencia = agentesOcorrencia.Select(ao => ao.Codigo).ToArray();

                List<OcorrenciaSegurancaAgente> agentesOcorrenciaNovos = agentesOcorrencia.Where(ao => ao.Codigo <= 0).ToList();
                List<OcorrenciaSegurancaAgente> agentesOcorrenciaInativados = repA.ObterTodos().Where(ao => !codigosAgentesOcorrencia.Contains(ao.Codigo) && ao.Ocorrencia.Codigo == ocorrenciaSeguranca.Codigo).ToList();

                if (agentesOcorrenciaNovos != null && agentesOcorrenciaNovos.Count > 0)
                {
                    IRepositorioPessoaFisica repP = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaFisica>(UnidadeTrabalho);

                    foreach (var item in agentesOcorrenciaNovos)
                    {
                        item.Ativar();
                        item.Ocorrencia = ocorrenciaSegurancaA;
                        item.DataRegistro = DateTime.Now;
                        PessoaFisica pf = null;
                        if (string.IsNullOrEmpty(item.PessoaFisica.Codigo))
                        {
                            pf = new PessoaFisica();
                            pf.Nome = item.PessoaFisica.Nome;
                            pf.Apelido = item.PessoaFisica.Nome;
                            pf.CPF = item.PessoaFisica.CPF.Replace("-", "").Replace(".", ""); ;
                            pf.Passaporte = item.PessoaFisica.Passaporte;

                            repP.Salvar(pf, null);

                            IRepositorioTelefone repT = Fabrica.Instancia.ObterRepositorio<IRepositorioTelefone>(UnidadeTrabalho);

                            foreach (Telefone tel in item.PessoaFisica.Telefones)
                            {
                                tel.Ativar();
                                tel.DataRegistro = DateTime.Now;
                                tel.Pessoa = pf;

                                repT.Validar(EstadoObjeto.Novo, tel);
                                repT.Salvar(tel);
                            }

                        }
                        else
                        {
                            pf = repP.ObterPorId(item.PessoaFisica.Codigo);
                        }

                        item.PessoaFisica = pf;

                        repA.Validar(item, EstadoObjeto.Novo);
                        repA.Salvar(item);
                    }
                }

                if (agentesOcorrenciaInativados != null && agentesOcorrenciaInativados.Count > 0)
                {
                    foreach (var item in agentesOcorrenciaInativados)
                    {
                        item.Inativar();
                        repA.Validar(item, EstadoObjeto.Removido);
                        repA.Salvar(item);

                    }
                }
            }

            #endregion

            #region Midias

            if (midias != null && midias.Count > 0)
            {
                ocorrenciaSeguranca.Midias = new List<OcorrenciaSegurancaMidia>();

                IRepositorioOcorrenciaSegurancaMidia repOcoSegMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioOcorrenciaSegurancaMidia>(UnidadeTrabalho);

                int[] codigosMidias = midias.Select(m => m.CodigoMidia).ToArray();

                List<MidiaModelView> midiasNovas = midias.Where(m => m.CodigoMidia <= 0).ToList();
                List<OcorrenciaSegurancaMidia> midiasInativadas = repOcoSegMidia.ObterTodos().Where(m => m.Ativo && !codigosMidias.Contains(m.Midia.Codigo) && m.Ocorrencia.Codigo == ocorrenciaSeguranca.Codigo).ToList();

                if (midiasNovas != null && midiasNovas.Count > 0)
                {
                    string path = ConfigurationManager.AppSettings.Get("pathUploads");

                    if (!System.IO.Directory.Exists(path))
                        throw new CoreException("O diretório onde a midia seria armazanada não está acessivel.");

                    IRepositorioMidia repMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioMidia>(UnidadeTrabalho);

                    int cont = repOcoSegMidia.ObterTodos().Where(dm => dm.Ocorrencia.Codigo == ocorrenciaSeguranca.Codigo).Count() + 1;

                    foreach (MidiaModelView midia in midiasNovas ?? Enumerable.Empty<MidiaModelView>())
                    {
                        if (midia == null)
                            continue;

                        string nomeDisco = "OS-" + ocorrenciaSeguranca.Codigo + "-" + cont + "." + midia.Extensao;
                        Midia m = new Midia();
                        m.DataRegistro = midia.DataRegistro;
                        m.Extensao = midia.Extensao;
                        m.MimeType = midia.MIME;
                        m.NomeArquivo = nomeDisco;
                        repMidia.Salvar(m);

                        ocorrenciaSeguranca.Midias.Add(repOcoSegMidia.SalvarOcorrenciaSegurancaMidia(ocorrenciaSeguranca, m));

                        System.IO.File.WriteAllBytes(m.Url, Convert.FromBase64String(midia.Arquivo));

                        cont++;
                    }
                }

                InativarOcorrenciaSegurancaMidia(repOcoSegMidia, midiasInativadas);
            }
            else
            {
                IRepositorioOcorrenciaSegurancaMidia repOcoSegMidia = Fabrica.Instancia.ObterRepositorio<IRepositorioOcorrenciaSegurancaMidia>(UnidadeTrabalho);

                List<OcorrenciaSegurancaMidia> midiasInativadas = repOcoSegMidia.ObterTodos().Where(m => m.Ativo && m.Ocorrencia.Codigo == ocorrenciaSeguranca.Codigo).ToList();

                InativarOcorrenciaSegurancaMidia(repOcoSegMidia, midiasInativadas);
            }

            #endregion

            #region Veiculos

            IRepositorioOcorrenciaSegurancaVeiculo repOSV = Fabrica.Instancia.ObterRepositorio<IRepositorioOcorrenciaSegurancaVeiculo>(UnidadeTrabalho);

            if (veiculos != null && veiculos.Count > 0)
            {
                InativarOcorrenciaSegurancaVeiculo(ocorrenciaSeguranca, veiculos, repOSV);
                int[] codigosVeiculo = repOSV.ObterTodos().Where(osv => osv.OcorrenciaSeg.Codigo == ocorrenciaSeguranca.Codigo).Select(osv => osv.Veiculo.Codigo).ToArray();

                foreach (Veiculo v in veiculos)
                {
                    if (codigosVeiculo.Contains(v.Codigo))
                        continue;

                    if (v.Codigo <= 0)
                    {
                        v.DataRegistro = DateTime.Now;
                        IRepositorioVeiculo repV = Fabrica.Instancia.ObterRepositorio<IRepositorioVeiculo>(UnidadeTrabalho);

                        repV.Validar(v, EstadoObjeto.Novo);
                        repV.Salvar(v);

                    }

                    v.Modelo = Fabrica.Instancia.ObterRepositorio<IRepositorioModeloVeiculo>(UnidadeTrabalho).ObterPorId(v.Modelo.Codigo);
                    repOSV.SalvarOcorrenciaSegurancaVeiculo(ocorrenciaSeguranca, v);
                }
            }
            else
            {
                List<OcorrenciaSegurancaVeiculo> listaOSVRemovidos = repOSV.ObterTodos().Where(osv => osv.OcorrenciaSeg.Codigo == ocorrenciaSeguranca.Codigo && osv.Ativo).ToList();

                InativarOcorrenciaSegurancaVeiculo(ocorrenciaSeguranca, veiculos, repOSV);
            }


            #endregion

            return Json(montaObj(ocorrenciaSegurancaA));
        }

        private static void InativarOcorrenciaSegurancaVeiculo(OcorrenciaSeguranca ocorrenciaSeguranca, List<Veiculo> veiculos, IRepositorioOcorrenciaSegurancaVeiculo repOSV)
        {
            int[] codigosVeiculos = new List<int>().ToArray();
            if (veiculos != null)
                codigosVeiculos = veiculos.Where(v => v.Codigo > 0).Select(v => v.Codigo).ToArray();
            List<OcorrenciaSegurancaVeiculo> listaOSVRemovidos = repOSV.ObterTodos().Where(osv => osv.OcorrenciaSeg.Codigo == ocorrenciaSeguranca.Codigo && osv.Ativo && !codigosVeiculos.Contains(osv.Veiculo.Codigo)).ToList();

            foreach (OcorrenciaSegurancaVeiculo vRemovido in listaOSVRemovidos)
            {
                vRemovido.Inativar();
                repOSV.Validar(vRemovido, EstadoObjeto.Removido);
                repOSV.Salvar(vRemovido);
            }
        }

        private static void InativarOcorrenciaSegurancaMidia(IRepositorioOcorrenciaSegurancaMidia repOcoSegMidia, List<OcorrenciaSegurancaMidia> midiasInativadas)
        {
            if (midiasInativadas != null && midiasInativadas.Count > 0)
            {
                foreach (var item in midiasInativadas)
                {
                    item.Inativar();
                    repOcoSegMidia.Validar(item, EstadoObjeto.Removido);
                    repOcoSegMidia.Salvar(item);

                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="codigo"></param>
        /// <returns></returns>
        public ActionResult Selecionar(int codigo)
        {
            IRepositorioOcorrenciaSeguranca rep = Fabrica.Instancia.ObterRepositorio<IRepositorioOcorrenciaSeguranca>(UnidadeTrabalho);
            OcorrenciaSeguranca ocorrencia = rep.ObterPorId(codigo);

            return Json(montaObj(ocorrencia));
        }

        /// <summary>
        /// Retorna todos os objetos do tipo OcorrenciaSeguranca.
        /// </summary>
        /// <returns>
        /// Detalhes do tipo de objeto ligado ao controller no formato JSON.
        /// </returns>
        public ActionResult ObterTodos(bool apenasAtivos = false)
        {
            IRepositorioOcorrenciaSeguranca repOcorrencia = Fabrica.Instancia.ObterRepositorio<IRepositorioOcorrenciaSeguranca>(UnidadeTrabalho);

            IQueryable<OcorrenciaSeguranca> ocorrencias = repOcorrencia.ObterTodos();

            if (apenasAtivos)
            {
                ocorrencias = ocorrencias.Where(t => t.Ativo == true);
            }

            return Json(ocorrencias.Select(o => montaObj(o)).ToList());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="dataInicial"></param>
        /// <param name="dataFinal"></param>
        /// <param name="supervisor"></param>
        /// <param name="tipoOcorrencia"></param>
        /// <param name="local"></param>
        /// <param name="graduacao"></param>
        /// <param name="gravidade"></param>
        /// <param name="agPassivoCPF"></param>
        /// <param name="agPassivoPassaporte"></param>
        /// <param name="agPassivoNome"></param>
        /// <param name="agPassivoEmpresa"></param>
        /// <param name="agAtivoCPF"></param>
        /// <param name="agAtivoPassaporte"></param>
        /// <param name="agAtivoNome"></param>
        /// <param name="agAtivoEmpresa"></param>
        /// <returns></returns>
        public ActionResult Pesquisar(StoreRequestParameters parameters = null, DateTime? dataInicial = null, DateTime? dataFinal = null, string supervisor = null, int tipoOcorrencia = 0, int local = 0, GraduacaoOcorrencia[] graduacao = null, GravidadeOcorrencia? gravidade = null,
            string agPassivoCPF = null, string agPassivoPassaporte = null, string agPassivoNome = null, string agPassivoEmpresa = null, string agPassivoEmpresaCnpj = null, string agPassivoContratada = null, string agPassivoContratadaCnpj = null,
            string agAtivoCPF = null, string agAtivoPassaporte = null, string agAtivoNome = null, string agAtivoEmpresa = null, string agAtivoEmpresaCnpj = null, string agAtivoContratada = null, string agAtivoContratadaCnpj = null)
        {
            IQueryable<OcorrenciaSeguranca> regs = FiltrarOcorrencia(dataInicial, dataFinal, supervisor, tipoOcorrencia, local, graduacao, gravidade,
                agPassivoCPF, agPassivoPassaporte, agPassivoNome, agPassivoEmpresa, agPassivoEmpresaCnpj, agPassivoContratada, agPassivoContratadaCnpj,
                agAtivoCPF, agAtivoPassaporte, agAtivoNome, agAtivoEmpresa, agAtivoEmpresaCnpj, agAtivoContratada, agAtivoContratadaCnpj);
            int total = regs.Count();
            List<OcorrenciaSegurancaModelView> data = montaObjts(regs, parameters);

            return new StoreResult(data, total);
        }

        private IQueryable<OcorrenciaSeguranca> FiltrarOcorrencia(DateTime? dataInicial = null, DateTime? dataFinal = null, string supervisor = null, int tipoOcorrencia = 0, int local = 0, GraduacaoOcorrencia[] graduacao = null, GravidadeOcorrencia? gravidade = null,
            string agPassivoCPF = null, string agPassivoPassaporte = null, string agPassivoNome = null, string agPassivoEmpresa = null, string agPassivoEmpresaCnpj = null, string agPassivoContratada = null, string agPassivoContratadaCnpj = null,
            string agAtivoCPF = null, string agAtivoPassaporte = null, string agAtivoNome = null, string agAtivoEmpresa = null, string agAtivoEmpresaCnpj = null, string agAtivoContratada = null, string agAtivoContratadaCnpj = null)
        {
            IRepositorioOcorrenciaSeguranca rep = Fabrica.Instancia.ObterRepositorio<IRepositorioOcorrenciaSeguranca>(UnidadeTrabalho);
            IRepositorioColaborador repColab = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);
            IRepositorioPrestadorServico repPrest = Fabrica.Instancia.ObterRepositorio<IRepositorioPrestadorServico>(UnidadeTrabalho);
            IRepositorioVisitante repVisit = Fabrica.Instancia.ObterRepositorio<IRepositorioVisitante>(UnidadeTrabalho);
            IRepositorioAlocacaoColaborador repAlocColab = Fabrica.Instancia.ObterRepositorio<IRepositorioAlocacaoColaborador>(UnidadeTrabalho);

            IQueryable<OcorrenciaSeguranca> ocorrencia = rep.ObterTodos();

            if (dataInicial != null)
                ocorrencia = ocorrencia.Where(o => o.DataOcorrencia >= dataInicial);

            if (dataFinal != null)
                ocorrencia = ocorrencia.Where(o => o.DataOcorrencia <= dataFinal);

            if (!string.IsNullOrEmpty(supervisor))
            {
                ocorrencia = ocorrencia.Where(o => o.Supervisor.Login.Contains(supervisor) ||
                    o.Supervisor.PessoaFisica.Nome.Contains(supervisor));
            }

            if (tipoOcorrencia > 0)
                ocorrencia = ocorrencia.Where(o => o.TipoOcorrencia.Codigo == tipoOcorrencia);

            if (local > 0)
                ocorrencia = ocorrencia.Where(o => o.Local.Codigo == local);

            if (tipoOcorrencia > 0)
                ocorrencia = ocorrencia.Where(o => o.TipoOcorrencia.Codigo == tipoOcorrencia);

            if (graduacao != null && graduacao.Length > 0)
            {
                foreach (GraduacaoOcorrencia item in graduacao)
                {
                    switch (item)
                    {
                        case GraduacaoOcorrencia.Tentativa:
                            ocorrencia = ocorrencia.Where(o => o.Tentativa == true);
                            break;
                        case GraduacaoOcorrencia.Efetiva:
                            ocorrencia = ocorrencia.Where(o => o.Efetiva == true);
                            break;
                        case GraduacaoOcorrencia.Vulnerabilidade:
                            ocorrencia = ocorrencia.Where(o => o.Vulnerabilidade == true);
                            break;
                        default:
                            break;
                    }
                }
            }

            if (gravidade != null)
                ocorrencia = ocorrencia.Where(o => o.Gravidade == gravidade);

            agPassivoEmpresaCnpj = agPassivoEmpresaCnpj != null ? agPassivoEmpresaCnpj.Replace(".", "").Replace("/", "").Replace("-", "") : null;
            agPassivoContratadaCnpj = agPassivoContratadaCnpj != null ? agPassivoContratadaCnpj.Replace(".", "").Replace("/", "").Replace("-", "") : null;
            agAtivoEmpresaCnpj = agAtivoEmpresaCnpj != null ? agAtivoEmpresaCnpj.Replace(".", "").Replace("/", "").Replace("-", "") : null;
            agAtivoContratadaCnpj = agAtivoContratadaCnpj != null ? agAtivoContratadaCnpj.Replace(".", "").Replace("/", "").Replace("-", "") : null;

            #region [AGENTE PASSIVO]
            if (!string.IsNullOrWhiteSpace(agPassivoCPF))
                ocorrencia = ocorrencia.Where(o => o.AgentesOcorrencia.Any(ag =>
                    ag.TipoAgente == TipoAgenteOcorrencia.Passivo &&
                    ag.PessoaFisica.CPF.Equals(agPassivoCPF.Replace(".", "").Replace("-", ""))));

            if (!string.IsNullOrWhiteSpace(agPassivoPassaporte))
                ocorrencia = ocorrencia.Where(o => o.AgentesOcorrencia.Any(ag =>
                    ag.TipoAgente == TipoAgenteOcorrencia.Passivo &&
                    ag.PessoaFisica.Passaporte.Equals(agPassivoPassaporte.Replace(".", "").Replace("-", ""))));

            if (!string.IsNullOrWhiteSpace(agPassivoNome))
                ocorrencia = ocorrencia.Where(o => o.AgentesOcorrencia.Any(ag =>
                    ag.TipoAgente == TipoAgenteOcorrencia.Passivo &&
                    ag.PessoaFisica.Nome.Contains(agPassivoNome)));

            if ((!string.IsNullOrWhiteSpace(agPassivoEmpresa) || !string.IsNullOrWhiteSpace(agPassivoEmpresaCnpj) || !string.IsNullOrEmpty(agPassivoContratada) || !string.IsNullOrEmpty(agPassivoContratadaCnpj)) &&
                (string.IsNullOrEmpty(agAtivoEmpresa) && string.IsNullOrEmpty(agAtivoEmpresaCnpj) && string.IsNullOrEmpty(agAtivoContratada) && string.IsNullOrEmpty(agAtivoContratadaCnpj)))
            {
                if (!string.IsNullOrWhiteSpace(agPassivoEmpresa) || !string.IsNullOrWhiteSpace(agPassivoEmpresaCnpj))
                {
                    ocorrencia = ocorrencia.Where(o => o.AgentesOcorrencia.Any(ag =>
                        ag.Ativo &&
                        ag.TipoAgente == TipoAgenteOcorrencia.Passivo && (
                            (repColab.ObterTodos()
                                .Where(c => c.PessoaFisica.Codigo.Equals(ag.PessoaFisica.Codigo) && (c.PapelLogs.Any(pl =>
                                    (pl.DataInicio <= o.DataOcorrencia) &&
                                    (pl.DataFim == null || pl.DataFim > o.DataOcorrencia)
                                    ))).Any(c => (!string.IsNullOrEmpty(agPassivoEmpresa) && c.Empresa.Nome.Contains(agPassivoEmpresa)) ||
                                                 (!string.IsNullOrEmpty(agPassivoEmpresaCnpj) && c.Empresa.CNPJ == agPassivoEmpresaCnpj)) ||

                            (repPrest.ObterTodos()
                                .Where(p => p.PessoaFisica.Codigo.Equals(ag.PessoaFisica.Codigo) && (p.PapelLogs.Any(pl =>
                                    (pl.DataInicio <= o.DataOcorrencia) &&
                                    (pl.DataFim == null || pl.DataFim > o.DataOcorrencia)
                                    ))).Any(p => (!string.IsNullOrEmpty(agPassivoEmpresa) && p.Empresa.Nome.Contains(agPassivoEmpresa)) ||
                                                 (!string.IsNullOrEmpty(agPassivoEmpresaCnpj) && p.Empresa.CNPJ == agPassivoEmpresaCnpj)))) ||

                            (repVisit.ObterTodos()
                                .Where(v => v.PessoaFisica.Codigo.Equals(ag.PessoaFisica.Codigo) && (v.PapelLogs.Any(pl =>
                                    (pl.DataInicio <= o.DataOcorrencia) &&
                                    (pl.DataFim == null || pl.DataFim > o.DataOcorrencia)
                                    ))).Any(v => !string.IsNullOrEmpty(agPassivoEmpresa) && v.Empresa.Contains(agPassivoEmpresa)))
                            )
                        )
                    );
                }

                if (!string.IsNullOrEmpty(agPassivoContratada) || !string.IsNullOrEmpty(agPassivoContratadaCnpj))
                {
                    var osa = ocorrencia.Where(o => o.AgentesOcorrencia.Any(ag =>
                        ag.Ativo &&
                        ag.TipoAgente == TipoAgenteOcorrencia.Passivo))
                        .SelectMany(aux => aux.AgentesOcorrencia);

                    List<string> pessoasFisica = osa.Select(ocorrenciaSegurancaAgente => ocorrenciaSegurancaAgente.PessoaFisica.Codigo).ToList();
                    var codColabs = repColab.ObterTodos().Where(c => pessoasFisica.Contains(c.PessoaFisica.Codigo)).Select(c => c.Codigo);

                    List<int> codsAlocacao = new List<int>();
                    foreach (var cod in codColabs)
                    {
                        var alocao = repAlocColab.ObterAlocacao(cod);
                        if (alocao == null) continue;
                        var empresa = alocao.ObterContratada().Empresa;
                        if (empresa.Nome.Contains(agPassivoContratada) || empresa.Apelido.Contains(agPassivoContratada) || empresa.CNPJ == agPassivoContratadaCnpj)
                            codsAlocacao.Add(alocao.Codigo);
                    }

                    ocorrencia = ocorrencia.Where(o => o.AgentesOcorrencia.Any(ag =>
                        ag.Ativo &&
                        ag.TipoAgente == TipoAgenteOcorrencia.Passivo && (
                            (repAlocColab.ObterTodos()
                                .Where(a => a.Papel.PessoaFisica.Codigo.Equals(ag.PessoaFisica.Codigo))
                                .Any(aux => codsAlocacao.Contains(aux.Codigo)))
                            )
                        )
                    );
                }
            }
            #endregion

            #region [AGENTE ATIVO]
            if (!string.IsNullOrWhiteSpace(agAtivoCPF))
                ocorrencia = ocorrencia.Where(o => o.AgentesOcorrencia.Any(ag =>
                    (ag.TipoAgente == TipoAgenteOcorrencia.Ativo) &&
                    ag.PessoaFisica.CPF.Equals(agAtivoCPF.Replace(".", "").Replace("-", ""))
                ));

            if (!string.IsNullOrWhiteSpace(agAtivoPassaporte))
                ocorrencia = ocorrencia.Where(o => o.AgentesOcorrencia.Any(ag =>
                    (ag.TipoAgente == TipoAgenteOcorrencia.Ativo) && ag.PessoaFisica.Passaporte.Equals(agAtivoPassaporte)));

            if (!string.IsNullOrWhiteSpace(agAtivoNome))
                ocorrencia = ocorrencia.Where(o => o.AgentesOcorrencia.Any(ag =>
                    ag.TipoAgente == TipoAgenteOcorrencia.Ativo &&
                    ag.PessoaFisica.Nome.Contains(agAtivoNome)));

            if ((!string.IsNullOrWhiteSpace(agAtivoEmpresa) || !string.IsNullOrEmpty(agAtivoEmpresaCnpj) || !string.IsNullOrEmpty(agAtivoContratada) || !string.IsNullOrEmpty(agAtivoContratadaCnpj)) &&
                (string.IsNullOrEmpty(agPassivoEmpresa) && string.IsNullOrEmpty(agPassivoEmpresaCnpj) && string.IsNullOrEmpty(agPassivoContratada) && string.IsNullOrEmpty(agPassivoContratadaCnpj)))
            {
                if (!string.IsNullOrWhiteSpace(agAtivoEmpresa) || !string.IsNullOrEmpty(agAtivoEmpresaCnpj))
                {
                    ocorrencia = ocorrencia.Where(o => o.AgentesOcorrencia.Any(ag =>
                        ag.Ativo &&
                        ag.TipoAgente == TipoAgenteOcorrencia.Ativo && (
                            (repColab.ObterTodos()
                                .Where(c => c.PessoaFisica.Codigo.Equals(ag.PessoaFisica.Codigo) && (c.PapelLogs.Any(pl =>
                                    (pl.DataInicio <= o.DataOcorrencia) &&
                                    (pl.DataFim == null || pl.DataFim > o.DataOcorrencia)
                                    ))).Any(c => (!string.IsNullOrEmpty(agAtivoEmpresa) && c.Empresa.Nome.Contains(agAtivoEmpresa)) ||
                                                 (!string.IsNullOrEmpty(agAtivoEmpresaCnpj) && c.Empresa.CNPJ == agAtivoEmpresaCnpj)) ||

                            (repPrest.ObterTodos()
                                .Where(p => p.PessoaFisica.Codigo.Equals(ag.PessoaFisica.Codigo) && (p.PapelLogs.Any(pl =>
                                    (pl.DataInicio <= o.DataOcorrencia) &&
                                    (pl.DataFim == null || pl.DataFim > o.DataOcorrencia)
                                    ))).Any(p => (!string.IsNullOrEmpty(agAtivoEmpresa) && p.Empresa.Nome.Contains(agAtivoEmpresa)) ||
                                                 (!string.IsNullOrEmpty(agAtivoEmpresaCnpj) && p.Empresa.CNPJ == agAtivoEmpresaCnpj)))) ||

                            (repVisit.ObterTodos()
                                .Where(v => v.PessoaFisica.Codigo.Equals(ag.PessoaFisica.Codigo) && (v.PapelLogs.Any(pl =>
                                    (pl.DataInicio <= o.DataOcorrencia) &&
                                    (pl.DataFim == null || pl.DataFim > o.DataOcorrencia)
                                    ))).Any(v => !string.IsNullOrEmpty(agAtivoEmpresa) && v.Empresa.Contains(agAtivoEmpresa)))
                            )
                    ));
                }

                if (!string.IsNullOrEmpty(agAtivoContratada) || !string.IsNullOrEmpty(agAtivoContratadaCnpj))
                {
                    var osa = ocorrencia.Where(o => o.AgentesOcorrencia.Any(ag =>
                        ag.Ativo &&
                        ag.TipoAgente == TipoAgenteOcorrencia.Ativo))
                        .SelectMany(aux => aux.AgentesOcorrencia);

                    List<string> pessoasFisica = osa.Select(ocorrenciaSegurancaAgente => ocorrenciaSegurancaAgente.PessoaFisica.Codigo).ToList();
                    var codColabs = repColab.ObterTodos().Where(c => pessoasFisica.Contains(c.PessoaFisica.Codigo)).Select(c => c.Codigo);

                    List<int> codsAlocacao = new List<int>();
                    foreach (var cod in codColabs)
                    {
                        var alocao = repAlocColab.ObterAlocacao(cod);
                        if (alocao == null) continue;
                        var empresa = alocao.ObterContratada().Empresa;
                        if (empresa.Nome.Contains(agAtivoContratada) || empresa.Apelido.Contains(agAtivoContratada) || empresa.CNPJ == agAtivoContratadaCnpj)
                            codsAlocacao.Add(alocao.Codigo);
                    }

                    ocorrencia = ocorrencia.Where(o => o.AgentesOcorrencia.Any(ag =>
                        ag.Ativo &&
                        ag.TipoAgente == TipoAgenteOcorrencia.Ativo && (
                            (repAlocColab.ObterTodos()
                                .Where(a => a.Papel.PessoaFisica.Codigo.Equals(ag.PessoaFisica.Codigo))
                                .Any(aux => codsAlocacao.Contains(aux.Codigo)))
                            )
                        )
                    );
                }
            }
            #endregion

            #region [CONTRATADA DE AGENSTES PASSIVO E ATIVO]
            if ((!string.IsNullOrEmpty(agPassivoEmpresa) || !string.IsNullOrEmpty(agPassivoEmpresaCnpj) || !string.IsNullOrEmpty(agPassivoContratada) || !string.IsNullOrEmpty(agPassivoContratadaCnpj)) &&
                (!string.IsNullOrEmpty(agAtivoEmpresa) || !string.IsNullOrEmpty(agAtivoEmpresaCnpj) || !string.IsNullOrEmpty(agAtivoContratada) || !string.IsNullOrEmpty(agAtivoContratadaCnpj)))
            {
                //BUSCANDO EMPRESA
                if (!string.IsNullOrEmpty(agPassivoEmpresa) || !string.IsNullOrEmpty(agPassivoEmpresaCnpj) ||
                    !string.IsNullOrEmpty(agAtivoEmpresa) || !string.IsNullOrEmpty(agAtivoEmpresaCnpj))
                {
                    ocorrencia = ocorrencia.Where(o => o.AgentesOcorrencia.Any(ag =>
                        ag.Ativo && (
                            (repColab.ObterTodos()
                                .Where(c => c.PessoaFisica.Codigo.Equals(ag.PessoaFisica.Codigo) && (c.PapelLogs.Any(pl =>
                                    (pl.DataInicio <= o.DataOcorrencia) &&
                                    (pl.DataFim == null || pl.DataFim > o.DataOcorrencia)
                                    ))).Any(c => (!string.IsNullOrEmpty(agPassivoEmpresa) && c.Empresa.Nome.Contains(agPassivoEmpresa)) ||
                                                 (!string.IsNullOrEmpty(agPassivoEmpresaCnpj) && c.Empresa.CNPJ == agPassivoEmpresaCnpj) ||
                                                 (!string.IsNullOrEmpty(agAtivoEmpresa) && c.Empresa.Nome.Contains(agAtivoEmpresa)) ||
                                                 (!string.IsNullOrEmpty(agAtivoEmpresaCnpj) && c.Empresa.CNPJ == agAtivoEmpresaCnpj)) ||

                            (repPrest.ObterTodos()
                                .Where(p => p.PessoaFisica.Codigo.Equals(ag.PessoaFisica.Codigo) && (p.PapelLogs.Any(pl =>
                                    (pl.DataInicio <= o.DataOcorrencia) &&
                                    (pl.DataFim == null || pl.DataFim > o.DataOcorrencia)
                                    ))).Any(p => (!string.IsNullOrEmpty(agPassivoEmpresa) && p.Empresa.Nome.Contains(agPassivoEmpresa)) ||
                                                 (!string.IsNullOrEmpty(agPassivoEmpresaCnpj) && p.Empresa.CNPJ == agPassivoEmpresaCnpj) ||
                                                 (!string.IsNullOrEmpty(agAtivoEmpresa) && p.Empresa.Nome.Contains(agAtivoEmpresa)) ||
                                                 (!string.IsNullOrEmpty(agAtivoEmpresaCnpj) && p.Empresa.CNPJ == agAtivoEmpresaCnpj)))) ||

                            (repVisit.ObterTodos()
                                .Where(v => v.PessoaFisica.Codigo.Equals(ag.PessoaFisica.Codigo) && (v.PapelLogs.Any(pl =>
                                    (pl.DataInicio <= o.DataOcorrencia) &&
                                    (pl.DataFim == null || pl.DataFim > o.DataOcorrencia)
                                    ))).Any(v => (!string.IsNullOrEmpty(agPassivoEmpresa) && v.Empresa.Contains(agPassivoEmpresa) ||
                                                 (!string.IsNullOrEmpty(agAtivoEmpresa) && v.Empresa.Contains(agAtivoEmpresa)))))
                            )
                        )
                    );
                }

                if (!string.IsNullOrEmpty(agPassivoContratada) || !string.IsNullOrEmpty(agPassivoContratadaCnpj) || 
                    !string.IsNullOrEmpty(agAtivoContratada) || !string.IsNullOrEmpty(agAtivoContratadaCnpj))
                {
                    var osa = ocorrencia.Where(o => o.AgentesOcorrencia.Any(ag =>
                        ag.Ativo &&
                        ag.TipoAgente == TipoAgenteOcorrencia.Ativo))
                        .SelectMany(aux => aux.AgentesOcorrencia);

                    List<string> pessoasFisica = osa.Select(ocorrenciaSegurancaAgente => ocorrenciaSegurancaAgente.PessoaFisica.Codigo).ToList();
                    var codColabs = repColab.ObterTodos().Where(c => pessoasFisica.Contains(c.PessoaFisica.Codigo)).Select(c => c.Codigo);

                    List<int> codsAlocacao = new List<int>();
                    foreach (var cod in codColabs)
                    {
                        var alocao = repAlocColab.ObterAlocacao(cod);
                        if (alocao == null) continue;
                        var empresa = alocao.ObterContratada().Empresa;
                        if ((empresa.Nome.Contains(agPassivoContratada) || empresa.Apelido.Contains(agPassivoContratada) || empresa.CNPJ == agPassivoContratadaCnpj) &&
                            (empresa.Nome.Contains(agAtivoContratada) || empresa.Apelido.Contains(agAtivoContratada) || empresa.CNPJ == agAtivoContratadaCnpj))
                            codsAlocacao.Add(alocao.Codigo);
                    }

                    ocorrencia = ocorrencia.Where(o => o.AgentesOcorrencia.Any(ag =>
                        ag.Ativo && (
                            repAlocColab.ObterTodos()
                                .Where(a => a.Papel.PessoaFisica.Codigo.Equals(ag.PessoaFisica.Codigo))
                                .Any(aux => codsAlocacao.Contains(aux.Codigo))
                            )
                        )
                    );
                }

            }
            #endregion

            return ocorrencia.OrderBy(o => o.DataOcorrencia);
        }

        private List<OcorrenciaSegurancaModelView> montaObjts(IQueryable<OcorrenciaSeguranca> regs, StoreRequestParameters parameters = null, bool ignoreParameters = false)
        {
            if (!ignoreParameters)
            {
                if (parameters == null)
                    parameters = new StoreRequestParameters();
                regs = regs.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 25);
            }

            GraduacaoOcorrencia[] graduacoes = new GraduacaoOcorrencia[] { GraduacaoOcorrencia.Efetiva, GraduacaoOcorrencia.Tentativa };

            return regs.ToList().Select(o => new OcorrenciaSegurancaModelView
            {
                Codigo = o.Codigo,
                Descricao = o.Descricao,
                Procedimentos = o.Procedimentos,
                AcoesRecomendadas = o.AcoesRecomendadas,
                DataOcorrencia = o.DataOcorrencia,
                HoraOcorrencia = o.HoraOcorrencia.TotalMilliseconds,
                HoraOcorrenciaFormatada = o.HoraOcorrencia.ToString("hh\\:mm"),
                TipoOcorrenciaDesc = o.TipoOcorrencia.Descricao,
                Vulnerabilidade = o.Vulnerabilidade,
                Tentativa = o.Tentativa,
                Efetiva = o.Efetiva,
                SupervisorLogin = (o.Supervisor != null) ? o.Supervisor.Login.ToString() : "",
                Local = string.Format("{0} - {1}", o.Local.Sigla, o.Local.Nome),
                Gravidade = o.Gravidade.ToString(),
                Graduacao = graduacoes,
                Ativo = o.Ativo
            }).ToList();
        }

        private Empresa ObterEmpresaVigente(string cpf, DateTime data)
        {
            IRepositorioPessoaFisica repPessoaFisica = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaFisica>(UnidadeTrabalho);
            var pessoaFisica = repPessoaFisica.ObterPorCpfPassaporte(false, cpf);
            if (pessoaFisica == null)
                return null;

            var papeis = pessoaFisica.Papeis.Where(p => p.PapelLogs.Any(pl => (pl.DataInicio <= data) && (pl.DataFim == null || pl.DataFim > data)));

            return papeis.Select(p => new Empresa()
            {
                Nome = ((p is Colaborador) ? (p as Colaborador).Empresa.Nome :
                       ((p is PrestadorServico) ? (p as PrestadorServico).Empresa.Nome :
                       ((p is Visitante) ? (p as Visitante).Empresa : "")))
            }).FirstOrDefault();
        }

        /// <summary>
        /// Obter graduações.
        /// </summary>        
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ObterGraduacao()
        {
            Dictionary<int, string> dic = new Dictionary<int, string>();

            foreach (GraduacaoOcorrencia graduacao in Enum.GetValues(typeof(GraduacaoOcorrencia)))
            {
                dic.Add(Convert.ToInt32(graduacao), graduacao.ObterDescricaoEnum());
            }

            return Json(dic.Select(d => new
            {
                Codigo = d.Key,
                Nome = d.Value
            }).ToArray());
        }

        /// <summary>
        /// Obter Gravidade de ocorrência de segurança
        /// </summary>
        /// <returns></returns>
        public ActionResult ObterGravidade()
        {
            Dictionary<int, string> dic = new Dictionary<int, string>();

            foreach (GravidadeOcorrencia gravidade in Enum.GetValues(typeof(GravidadeOcorrencia)))
            {
                dic.Add(Convert.ToInt32(gravidade), gravidade.ObterDescricaoEnum());
            }

            return Json(dic.Select(d => new
            {
                Codigo = d.Key,
                Descricao = d.Value
            }).ToArray());
        }

        /// <summary>
        /// Obter Tipos de Ocorrências
        /// </summary>
        /// <returns></returns>
        public ActionResult ObterTipoOcorrencia()
        {
            IRepositorioTipoOcorrencia rep = Fabrica.Instancia.ObterRepositorio<IRepositorioTipoOcorrencia>(UnidadeTrabalho);
            var regs = rep.ObterTodos().Where(r => r.Ativo);

            return Json(regs.Select(s => new
                {
                    Codigo = s.Codigo,
                    Descricao = string.Format("{0}", s.Descricao)
                })
                                .ToList().OrderBy(to => to.Descricao), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Obter Locais de Ocorrências
        /// </summary>
        /// <returns></returns>
        public ActionResult ObterLocais()
        {
            IRepositorioLocal rep = Fabrica.Instancia.ObterRepositorio<IRepositorioLocal>(UnidadeTrabalho);
            var regs = rep.ObterTodos().Where(r => r.Ativo);
            return Json(regs.Select(s => new
                               {
                                   Codigo = s.Codigo,
                                   Descricao = string.Format("{0} - {1}", s.Sigla, s.Nome)
                               })
                                .ToList(), JsonRequestBehavior.AllowGet);
        }
    }
}
