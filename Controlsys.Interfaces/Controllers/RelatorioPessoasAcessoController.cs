﻿using Controlsys.Infra;
using Ext.Net;
using Ext.Net.MVC;
using Globalsys;
using Globalsys.Relatorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Acesso;
using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Pessoas;
using Controlsys.Repositorios.Pessoas;
using Controlsys.Repositorio.Parametros;
using Controlsys.Repositorios.Empresas;
using Controlsys.Dominio.Empresas;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar relatorio pessoas acessoes.
    /// </summary>
    [ExtendController]
    public class RelatorioPessoasAcessoController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /RelatorioPessoasAcesso/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.RelatorioPessoasAcessoController.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RelatorioPessoasAcessoController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        /// <summary>
        /// Retorna a página de relatorioPessoasAcesso.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Exportar.
        /// </summary>
        ///
        /// <param name="selectedFields">
        /// The selected fields.
        /// </param>
        /// <param name="tipoRelatorio">
        /// O(a) tipo relatorio.
        /// </param>
        /// <param name="nome">
        /// O(a) nome.
        /// </param>
        /// <param name="cpf">
        /// O(a) cpf.
        /// </param>
        /// <param name="empresa">
        /// O(a) empresa.
        /// </param>
        /// <param name="cargo">
        /// O(a) cargo.
        /// </param>
        /// <param name="status">
        /// O(a) status.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Exportar(List<string> selectedFields, string tipoRelatorio, string nome = null, string cpf = null, string empresa = null, string cargo = null, StatusPapelLog? status = null, string maodeobra = null, string fornecedor = null, string contratada = null)
        {
            IQueryable<PessoaFisica> pessoasFisica = FiltrarPessoas(nome, cpf, empresa, cargo, status, maodeobra, fornecedor, contratada);
            ReportConfig reportConfig = ReportHelper.RenderReport<RelatorioPessoaModelView>(selectedFields, tipoRelatorio, pessoasFisica.Select(p => montaObj(p)));

            string fileName = string.Empty;
            switch (tipoRelatorio)
            {
                case "PDF":
                    fileName = "download.pdf";
                    break;
                case "EXCELOPENXML":
                    fileName = "download.xlsx";
                    break;
                case "WORDOPENXML":
                    fileName = "download.docx";
                    break;
            }

            return File(reportConfig.Bytes, reportConfig.MimeType, fileName);
        }

        /// <summary>
        /// Pesquisar.
        /// </summary>
        ///
        /// <param name="parameters">
        /// Options for controlling the operation.
        /// </param>
        /// <param name="nome">
        /// O(a) nome.
        /// </param>
        /// <param name="cpf">
        /// O(a) cpf.
        /// </param>
        /// <param name="empresa">
        /// O(a) empresa.
        /// </param>
        /// <param name="cargo">
        /// O(a) cargo.
        /// </param>
        /// <param name="status">
        /// O(a) status.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Pesquisar(StoreRequestParameters parameters = null, string nome = null, string cpf = null, string empresa = null, string cargo = null, StatusPapelLog? status = null, string maodeobra = null, string fornecedor = null, string contratada = null)
        {
            empresa = Empresa.RetornaApenasNomeEmpresa(empresa);

            IQueryable<PessoaFisica> pessoasFisica = FiltrarPessoas(nome, cpf, empresa, cargo, status, maodeobra, fornecedor, contratada);

            int total = pessoasFisica.Count();
            List<RelatorioPessoaModelView> data = montaListaObj(pessoasFisica, parameters);

            return new StoreResult(data, total);
        }

        /// <summary>
        /// Filtrar pessoas.
        /// </summary>
        ///
        /// <param name="nome">
        /// O(a) nome.
        /// </param>
        /// <param name="cpf">
        /// O(a) cpf.
        /// </param>
        /// <param name="empresa">
        /// O(a) empresa.
        /// </param>
        /// <param name="cargo">
        /// O(a) cargo.
        /// </param>
        /// <param name="status">
        /// O(a) status.
        /// </param>
        ///
        /// <returns>
        /// Um(a) IQueryable&lt;PessoaFisica&gt;
        /// </returns>
        private IQueryable<PessoaFisica> FiltrarPessoas(string nome = null, string cpf = null, string empresa = null, string cargo = null, StatusPapelLog? status = null, string maodeobra = null, string fornecedor = null, string contratada = null)
        {
            string query = @"
            SELECT      PF.[CD_PESSOA_FISICA]
                        ,PF.[TX_CPF]
                        ,PF.[CD_NATURAL] 
                        ,PF.[TX_PASSAPORTE]
                        ,PF.[TX_RG]
                        ,PF.[TX_RG_ORGAO]
                        ,PF.[CD_RG_UF]
                        ,PF.[DT_RG_EMISSAO]
                        ,PF.[DT_NASCIMENTO]
                        ,PF.[TX_MAE]
                        ,PF.[TX_PAI]
                        ,PF.[TX_SANGE]
                        ,PF.[TX_SEXO]
                        --,PF.[ME_FOTO]
                        ,PF.[DT_VISTO]
                        ,PF.[DT_VALIDADE_VISTO]
                        ,PF.[TX_RNE]
                        ,PF.[DT_EXPEDICAO_RNE]
                        ,PF.[TX_ESTADOCIVIL]
                        ,PF.[TX_ESCOLARIDADE]
                        ,PF.[NU_CR_NR]
                        ,PF.[NU_CR_TIPO]
                        ,PF.[TX_CNH]
                        ,PF.[TX_CNH_TP]
                        ,PF.[DT_CNH_VALIDADE]
                        ,PF.[NU_TE]
                        ,PF.[NU_TE_ZONA]
                        ,PF.[NU_TE_SECAO]
                        ,PF.[NU_CT]
                        ,PF.[TX_CT_SERIE]
                        ,PF.[CD_CT_ID_UF]
                        ,PF.[DT_CT_DT]
                        ,PF.[CD_MUNICIPIO]
                        ,PF.[TX_NACIONALIDADE]
                        ,(CASE WHEN CO.[TXT_MAODEOBRA] IS NOT NULL THEN CO.[TXT_MAODEOBRA] 
						       WHEN PR.[TXT_MAODEOBRA] IS NOT NULL THEN PR.[TXT_MAODEOBRA] 
							   ELSE Null END) [TXT_MAODEOBRA] 
                        ,PA.*, CR.*, CO.*, V.*, PR.*, EC.*, EP.*, PEPF.*, PEEC.*, PEEP.*, CA.*
            FROM		PESSOA_FISICA PF
			            INNER JOIN PAPEL PA ON PF.CD_PESSOA_FISICA = PA.CD_PESSOA_FISICA
			            INNER JOIN CRACHA CR ON PA.CD_PAPEL = CR.CD_PAPEL
			            LEFT JOIN COLABORADOR CO ON PA.CD_PAPEL = CO.CD_PAPEL
			            LEFT JOIN VISITANTE V ON PA.CD_PAPEL = V.CD_PAPEL
			            LEFT JOIN PRESTADOR PR ON PA.CD_PAPEL = PR.CD_PAPEL
			            LEFT JOIN EMPRESA EC ON CO.CD_EMPRESA = EC.CD_PESSOA
			            LEFT JOIN EMPRESA EP ON CO.CD_EMPRESA = EP.CD_PESSOA
			            LEFT JOIN PESSOA PEPF ON PEPF.CD_PESSOA = PF.CD_PESSOA_FISICA
			            LEFT JOIN PESSOA PEEC ON EC.CD_PESSOA = PEEC.CD_PESSOA
			            LEFT JOIN PESSOA PEEP ON EP.CD_PESSOA = PEEP.CD_PESSOA
			            LEFT JOIN CARGO CA ON PA.CD_CARGO = CA.CD_CARGO
            WHERE		1 = 1
            AND			CR.BL_ATIVO = 1
            ";

            string queryFiltroAtivo = @"AND			EXISTS (SELECT		PL.CD_PAPEL 
					FROM		PAPEL_LOG PL
								INNER JOIN (select		pl.CD_PAPEL, max(pl.CD_PAPEL_LOG) CD_PAPEL_LOG 
											from		papel_log pl 
											group by	pl.CD_PAPEL) MAX_PL ON PL.CD_PAPEL_LOG = MAX_PL.CD_PAPEL_LOG
					WHERE		UPPER(PL.TX_STATUS) = :status
					and			PA.CD_PAPEL = PL.CD_PAPEL)";

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            List<string> filtros = new List<string>();

            if (!string.IsNullOrEmpty(nome))
            {
                parameters.Add("nome", nome);
                filtros.Add("AND			PEPF.TX_NOME LIKE CONCAT('%', :nome, '%')");
            }

            if (!string.IsNullOrEmpty(cpf))
            {
                cpf = cpf.Replace(".", "").Replace("-", "");
                parameters.Add("cpf", cpf);
                filtros.Add("AND			PF.TX_CPF LIKE CONCAT('%', :cpf, '%')");
            }

            if (!string.IsNullOrEmpty(cargo))
            {
                parameters.Add("cargo", cargo);
                filtros.Add("AND			(CA.TX_NOME = :cargo OR V.TX_FUNCAO LIKE CONCAT('%', :cargo, '%'))");
            }

            if (!string.IsNullOrEmpty(empresa))
            {
                parameters.Add("empresa", empresa);
                filtros.Add("AND			(PEEC.TX_NOME = :empresa OR PEEP.TX_NOME = :empresa OR V.TX_EMPRESA LIKE CONCAT('%', :empresa, '%')) ");
            }

            if (!string.IsNullOrEmpty(fornecedor))
            {
                parameters.Add("fornecedor", fornecedor);
                filtros.Add("AND			(PR.CD_FORNECEDOR = :fornecedor)");
            }

            if (!string.IsNullOrEmpty(contratada))
            {
                parameters.Add("contratada", contratada);
                filtros.Add("And Exists (Select 1 "+
                                        "From ALOCACAO_COLABORADOR A "+
                                        "Where A.CD_PEDIDO in (SELECT CD_PEDIDO FROM VW_PedidosContratada V Where A.CD_Pedido = V.CD_Pedido And V.Contratada = :contratada) "+
                                        "  And A.CD_PAPEL = PA.CD_PAPEL "+
                                        "  And A.BL_ATIVO = 1 ) ");
            }

            if (status.HasValue)
            {
                parameters.Add("status", status.Value.ToString());
                filtros.Add(queryFiltroAtivo);
            }
            
            if (!string.IsNullOrEmpty(maodeobra.ToString()))
            {
                parameters.Add("maodeobra", maodeobra.ToString());
                filtros.Add("AND			((CO.TXT_MAODEOBRA = :maodeobra) OR (PR.TXT_MAODEOBRA = :maodeobra)) ");
            }

            if (filtros.Any())
                query += string.Join(Environment.NewLine, filtros);

            query += string.Join(Environment.NewLine, "ORDER BY ISNULL(PEPF.TX_NOME, ISNULL(PEEC.TX_NOME, ISNULL(PEEP.TX_NOME, '')))");



            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            return unidTrabalho.ExecuteSql<PessoaFisica>(query, parameters, new List<Type>() { typeof(PessoaFisica) }).AsQueryable();
            //return hasValue ? pessoasFisica : null;
        }

        /// <summary>
        /// Monta lista object.
        /// </summary>
        ///
        /// <param name="listaPessoasFisica">
        /// O(a) lista pessoas fisica.
        /// </param>
        /// <param name="parameters">
        /// Options for controlling the operation.
        /// </param>
        /// <param name="ignoreParameters">
        /// true to ignore parameters.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;RelatorioPessoaModelView&gt;
        /// </returns>
        private List<RelatorioPessoaModelView> montaListaObj(IQueryable<PessoaFisica> listaPessoasFisica, StoreRequestParameters parameters = null, bool ignoreParameters = false)
        {
            if (!ignoreParameters)
            {
                if (parameters == null)
                    parameters = new StoreRequestParameters();
                listaPessoasFisica = listaPessoasFisica.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 25);
            }

            return listaPessoasFisica.Select(p => montaObj(p)).ToList();
        }


        private RelatorioPessoaModelView montaObj(PessoaFisica pessoaFisica)
        {
            IRepositorioPrestadorServico repPrestador = Fabrica.Instancia.ObterRepositorio<IRepositorioPrestadorServico>(UnidadeTrabalho);
            IRepositorioAlocacaoColaborador repAlocacao = Fabrica.Instancia.ObterRepositorio<IRepositorioAlocacaoColaborador>(UnidadeTrabalho);
            IRepositorioCracha repCracha = Fabrica.Instancia.ObterRepositorio<IRepositorioCracha>(UnidadeTrabalho);

            IQueryable<AlocacaoColaborador> aloc = repAlocacao.ObterTodos();

            var relPessoa = new RelatorioPessoaModelView();
            string apelidoX = "", cargoX = "";            
            
            Papel papel = pessoaFisica.Papeis.OrderByDescending(x => x.DataRegistro).FirstOrDefault();
            // Papel papel = pessoaFisica.ObterPapel();
            AlocacaoColaborador alocacao = aloc.Where(a => a.Papel.Codigo == papel.Codigo).OrderByDescending(a => a.DataRegistro).FirstOrDefault();
            PedidoCompra alocada = alocacao != null ? alocacao.ObterContratada() : null;
            
            Cracha cracha = repCracha.ObterCracha(papel.Codigo);

            if (cracha != null)
            {
                relPessoa.RFID = cracha.Numero;
                relPessoa.DataRegistroCracha = cracha.DataRegistro.ToString("dd/MM/yyyy");
            }
            relPessoa.DataRegistroPapel = papel.DataRegistro.ToString("dd/MM/yyyy");
            relPessoa.Codigo = papel.Codigo.ToString();
            relPessoa.DataValidadeASO = papel.ASOs == null || papel.ASOs.Count == 0 ? string.Empty : papel.ASOs.Select(aso => aso.DataVencimento).Max().ToString("dd/MM/yyyy");

            if (papel is Colaborador)
            {
                apelidoX = (((papel as Colaborador).Empresa) != null ? (papel as Colaborador).Empresa.Nome : "");
                cargoX = ((papel as Colaborador).Cargo != null) ? (papel as Colaborador).Cargo.Nome : "";

                relPessoa.ConselhoProfissional = (papel as Colaborador).ConselhoProfissional;
                relPessoa.RegistroProfissional = (papel as Colaborador).RegistroProfissional;
                relPessoa.PessoaJuridica = ((papel as Colaborador).PessoaJuridica) ? "Sim" : "Não";
                relPessoa.DataAdmissao = (papel as Colaborador).DataAdmissao.ToString();
                relPessoa.Salario = (papel as Colaborador).Salario.ToString();
                relPessoa.SetorCusto = (alocacao != null) ? (alocacao.SetorCusto != null ? string.Format("{0} - {1}", alocacao.SetorCusto.Numero, alocacao.SetorCusto.Nome) : "") : "";
                relPessoa.Projeto = (alocacao != null) ? alocacao.PedidoCompra.Projeto.Nome : "";
                relPessoa.PedidoCompra = (alocacao != null) ? string.Format("{0} - {1}", alocacao.PedidoCompra.Numero, alocacao.PedidoCompra.Nome) : "";
                relPessoa.Observacao = (papel as Colaborador).Observacao;
                relPessoa.MaoDeObra = (papel as Colaborador).MaoDeObra.ObterDescricaoEnum();
            }
            else if (papel is PrestadorServico)
            {
                var prest = repPrestador.ObterTodos().FirstOrDefault(p => p.Codigo == (papel as PrestadorServico).Codigo);
                apelidoX = prest.TipoPrestador == TipoPrestador.Temporario ? prest.Empresa.Nome : prest.Fornecedor.NomeFantasia;

                cargoX = (prest.Cargo != null) ? prest.Cargo.Nome : "";
                relPessoa.DataAdmissao = prest.DataAdmissao.ToString();
                relPessoa.MaoDeObra = (papel as PrestadorServico).MaoDeObra.ObterDescricaoEnum();
            }
            else if (papel is Visitante)
            {
                apelidoX = (papel as Visitante).Empresa;
                cargoX = (papel as Visitante).Funcao;
            }

            relPessoa.CPF = papel.PessoaFisica.CPFFormatado();
            relPessoa.Passaporte = papel.PessoaFisica.Passaporte;
            relPessoa.Nome = papel.PessoaFisica.Nome;
            relPessoa.Apelido = papel.PessoaFisica.Apelido;
            relPessoa.Email = papel.PessoaFisica.Email;
            relPessoa.DataNascimento = papel.PessoaFisica.DataNascimento.ToString();
            relPessoa.DataVisto = papel.PessoaFisica.DataVisto.ToString();
            relPessoa.DataValidadeVisto = papel.PessoaFisica.DataValidadeVisto.ToString();
            relPessoa.Sexo = papel.PessoaFisica.Sexo.ObterDescricaoEnum();
            relPessoa.NomeMae = papel.PessoaFisica.NomeMae;
            relPessoa.NomePai = papel.PessoaFisica.NomePai;
            relPessoa.RNE = papel.PessoaFisica.RNE;
            relPessoa.DataExpedicaoRNE = papel.PessoaFisica.DataExpedicaoRNE.ToString();
            relPessoa.EstadoCivil = (papel.PessoaFisica.EstadoCivil != 0 && papel.PessoaFisica.EstadoCivil != null) ? papel.PessoaFisica.EstadoCivil.ObterDescricaoEnum() : "";
            relPessoa.Escolaridade = (papel.PessoaFisica.Escolaridade != 0 && papel.PessoaFisica.Escolaridade != null) ? papel.PessoaFisica.Escolaridade.ObterDescricaoEnum() : "";
            relPessoa.TipoSanguineo = (papel.PessoaFisica.TipoSanguineo != 0 && papel.PessoaFisica.TipoSanguineo != null) ? papel.PessoaFisica.TipoSanguineo.ObterDescricaoEnum() : "";
            relPessoa.Nacionalidade = papel.PessoaFisica.Nacionalidade;

            relPessoa.NaturalidadeUF = (papel.PessoaFisica.Naturalidade != null) ? papel.PessoaFisica.Naturalidade.Estado.Nome : "";
            relPessoa.Naturalidade = (papel.PessoaFisica.Naturalidade != null) ? papel.PessoaFisica.Naturalidade.Nome : "";

            relPessoa.RG = papel.PessoaFisica.RG;
            relPessoa.RGOrgaoEmissor = papel.PessoaFisica.RGOrgaoEmissor;
            relPessoa.RGOrgaoEmissorUF = (papel.PessoaFisica.RGOrgaoEmissorUF != null) ? papel.PessoaFisica.RGOrgaoEmissorUF.Nome : "";

            relPessoa.TituloEleitor = papel.PessoaFisica.TituloEleitor.ToString();
            relPessoa.TituloEleitorSecao = papel.PessoaFisica.TituloEleitorSecao.ToString();
            relPessoa.TituloEleitorZona = papel.PessoaFisica.TituloEleitorZona.ToString();
            relPessoa.TituloEleitorCidade = (papel.PessoaFisica.TituloEleitorCidade != null) ? papel.PessoaFisica.TituloEleitorCidade.Nome : "";
            relPessoa.TituloEleitorEstado = (papel.PessoaFisica.TituloEleitorCidade != null) ? papel.PessoaFisica.TituloEleitorCidade.Estado.Nome : "";

            relPessoa.CTPS = papel.PessoaFisica.CTPS.ToString();
            relPessoa.CTPSData = papel.PessoaFisica.CTPSData.ToString();
            relPessoa.CTPSSerie = papel.PessoaFisica.CTPSSerie;
            relPessoa.CTPSEstado = (papel.PessoaFisica.CTPSEstado != null) ? papel.PessoaFisica.CTPSEstado.Nome : "";
            relPessoa.CNH = papel.PessoaFisica.CNH;
            relPessoa.CNHCategoria = (papel.PessoaFisica.CNHCategoria != 0 && papel.PessoaFisica.CNHCategoria != null) ? papel.PessoaFisica.CNHCategoria.ObterDescricaoEnum() : "";
            relPessoa.CNHDataValidade = papel.PessoaFisica.CNHDataValidade.ToString();

            relPessoa.CertificadoReservista = papel.PessoaFisica.CertificadoReservista;
            relPessoa.CertificadoReservistaCat = papel.PessoaFisica.CertificadoReservistaCat;

            relPessoa.TelefoneCelular = papel.PessoaFisica.Telefones.Where(p => p.TipoTelefone == TipoTelefone.Celular).Select(x => x.TelefoneNum).FirstOrDefault();
            relPessoa.TelefoneEmergencial = papel.PessoaFisica.Telefones.Where(p => p.TipoTelefone == TipoTelefone.Celular).Select(x => x.TelefoneNum).FirstOrDefault();
            relPessoa.TelefoneResidencial = papel.PessoaFisica.Telefones.Where(p => p.TipoTelefone == TipoTelefone.Residencial).Select(x => x.TelefoneNum).FirstOrDefault();

            if (papel.PessoaFisica.Endereco != null)
            {
                relPessoa.Complemento = papel.PessoaFisica.Endereco.Complemento;
                relPessoa.Numero = papel.PessoaFisica.Endereco.Numero;
                if (papel.PessoaFisica.Endereco.Logradouro != null)
                {
                    relPessoa.CEP = papel.PessoaFisica.Endereco.Logradouro.CEP;
                    relPessoa.Logradouro = papel.PessoaFisica.Endereco.Logradouro.Nome;
                    relPessoa.Bairro = papel.PessoaFisica.Endereco.Logradouro.Bairro.Nome;
                    relPessoa.Cidade = papel.PessoaFisica.Endereco.Logradouro.Bairro.Cidade.Nome;
                    relPessoa.Estado = papel.PessoaFisica.Endereco.Logradouro.Bairro.Cidade.Estado.Nome;
                    relPessoa.Pais = papel.PessoaFisica.Endereco.Logradouro.Bairro.Cidade.Estado.Pais.Nome;
                }
            }

            PapelLog _papelLog = papel.ObterLogPapel();

            if (_papelLog != null)
            {
                relPessoa.StatusPapel = _papelLog.Status.ObterDescricaoEnum();
                relPessoa.DataStatusPapel = _papelLog.DataInicio.ToString();
                relPessoa.Status = _papelLog.Status.ObterDescricaoEnum();
            }

            relPessoa.Empresa = apelidoX;
            relPessoa.Cargo = cargoX;

            relPessoa.ModeloCracha = (papel.ModeloCracha != null) ? papel.ModeloCracha.Nome : "";

            relPessoa.Papel = papel != null ? papel is Colaborador ? "Colaborador" : papel is PrestadorServico ? "Prestador de Serviço" : "Visitante" : "";


            if (alocada != null)
            {
                relPessoa.Contratada = alocada.Empresa.Nome;
            }

            return relPessoa;
        }

    }

}