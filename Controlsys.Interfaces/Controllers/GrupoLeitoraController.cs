﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Controlsys.Infra;
using Controlsys.Repositorios.Acesso;
using Globalsys;
using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Parametros;
using Globalsys.Validacao;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorios.Seguranca;
using Controlsys.Dominio.Seguranca;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar grupo leitoras.
    /// </summary>
    [ExtendController]
    public class GrupoLeitoraController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /GrupoLeitora/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.GrupoLeitoraController.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public GrupoLeitoraController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        /// <summary>
        /// Retorna a página de grupoLeitora.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Inclui um novo grupoLeitora.
        /// </summary>
        ///
        /// <param name="grupoLeitora">
        /// O(a) grupo leitora.
        /// </param>
        /// <param name="leitoras">
        /// O(a) leitoras.
        /// </param>
        /// <param name="treinamentos">
        /// O(a) treinamentos.
        /// </param>
        /// <param name="grpsLeitoraHir">
        /// O(a) grps leitora hir.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Incluir(GrupoLeitora grupoLeitora, List<Leitora> leitoras, List<Treinamento> treinamentos, List<GrupoLeitora> grpsLeitoraHir)
        {
            IRepositorioGrupoLeitora repGrpLeitora = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoLeitora>(UnidadeTrabalho);
            grupoLeitora.DataRegistro = DateTime.Now;
            grupoLeitora.Ativar();
            repGrpLeitora.Validar(grupoLeitora, EstadoObjeto.Novo);
            repGrpLeitora.Salvar(grupoLeitora);

            if (leitoras != null)
            {
                foreach (var itemLeitoras in leitoras)
                {
                    LeitoraGrupoLeitora leitoraGrpLeitoras = new LeitoraGrupoLeitora();
                    leitoraGrpLeitoras.GrupoLeitora = grupoLeitora;
                    leitoraGrpLeitoras.Leitora = itemLeitoras;
                    leitoraGrpLeitoras.DataRegistro = DateTime.Now;
                    leitoraGrpLeitoras.Ativar();
                    repGrpLeitora.SalvarLeitoraGrupoLeitora(leitoraGrpLeitoras);
                }
            }
            if (grpsLeitoraHir != null)
            {
                foreach (var itemGrupoLeitoraHir in grpsLeitoraHir)
                {
                    GrupoLeitoraHierarq grupoLeitoraHirar = new GrupoLeitoraHierarq();
                    grupoLeitoraHirar.GrupoLeitoraPai = grupoLeitora;
                    grupoLeitoraHirar.GrupoLeitoraFilho = itemGrupoLeitoraHir;
                    grupoLeitoraHirar.DataRegistro = DateTime.Now;
                    grupoLeitoraHirar.Ativar();
                    repGrpLeitora.SalvaGrupoLeitoraHierarq(grupoLeitoraHirar);

                }
            }
            if (treinamentos != null)
            {
                foreach (var itemTreinamento in treinamentos)
                {
                    GrupoLeitoraTreinamento treinamentoProjeto = new GrupoLeitoraTreinamento();
                    treinamentoProjeto.GrupoLeitora = grupoLeitora;
                    treinamentoProjeto.Treinamento = itemTreinamento;
                    treinamentoProjeto.DataRegistro = DateTime.Now;
                    treinamentoProjeto.Ativar();
                    repGrpLeitora.SalvarGrupoLeitoraTreinamento(treinamentoProjeto);
                }
            }
            return Json(montaObj(grupoLeitora));
        }

        /// <summary>
        /// Altera um registro de grupoLeitora.
        /// </summary>
        ///
        /// <param name="grupoLeitora">
        /// O(a) grupo leitora.
        /// </param>
        /// <param name="leitoraDel">
        /// O(a) leitora delete.
        /// </param>
        /// <param name="leitoraNew">
        /// O(a) leitora new.
        /// </param>
        /// <param name="treinamentoDel">
        /// O(a) treinamento delete.
        /// </param>
        /// <param name="treinamentoNew">
        /// O(a) treinamento new.
        /// </param>
        /// <param name="grupoDel">
        /// O(a) grupo delete.
        /// </param>
        /// <param name="grupoNew">
        /// O(a) grupo new.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Alterar(GrupoLeitora grupoLeitora, int[] leitoraDel, int[] leitoraNew, int[] treinamentoDel, int[] treinamentoNew, int[] grupoDel, int[] grupoNew)
        {
            IRepositorioGrupoLeitora repGrpLeitora = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoLeitora>(UnidadeTrabalho);
            IRepositorioLeitora repLeitora = Fabrica.Instancia.ObterRepositorio<IRepositorioLeitora>(UnidadeTrabalho);
            GrupoLeitora grpLeitora = repGrpLeitora.ObterPorId(grupoLeitora.Codigo);
            
            grpLeitora.Nome = grupoLeitora.Nome;
            grpLeitora.HoraInicio = grupoLeitora.HoraInicio;
            grpLeitora.HoraFim = grupoLeitora.HoraFim;
            grpLeitora.Externa = grupoLeitora.Externa;
            grpLeitora.ValidaCNH = grupoLeitora.ValidaCNH;
            grpLeitora.Supervisao = grupoLeitora.Supervisao;
            grpLeitora.AcessoMonitorado = grupoLeitora.AcessoMonitorado;
            grpLeitora.AntiPassBack = grupoLeitora.AntiPassBack;
            grpLeitora.AreaAlfandegada = grupoLeitora.AreaAlfandegada;
            repGrpLeitora.Validar(grpLeitora, EstadoObjeto.Alterado);

            if (leitoraDel != null)
            {
                foreach (var item in grpLeitora.Leitoras.Where(t => leitoraDel.Any(tdd => tdd == t.Leitora.Codigo)))
                {
                    LeitoraGrupoLeitora leitoraGrpLeitoras = item;
                    leitoraGrpLeitoras.Inativar();
                }
            }
            if (leitoraNew != null)
            {
                foreach (var item in leitoraNew)
                {
                    LeitoraGrupoLeitora leitoraGrpLeitoras = new LeitoraGrupoLeitora();
                    leitoraGrpLeitoras.GrupoLeitora = grpLeitora;
                    //leitoraGrpLeitoras.Leitora = new Leitora { Codigo = item };
                    leitoraGrpLeitoras.Leitora = repLeitora.ObterPorId(item);
                    leitoraGrpLeitoras.DataRegistro = DateTime.Now;
                    leitoraGrpLeitoras.Ativar();
                    repGrpLeitora.SalvarLeitoraGrupoLeitora(leitoraGrpLeitoras);
                }
            }

            if (grupoDel != null)
            {
                foreach (var item in grpLeitora.GrupoLeitoraFilho.Where(t => grupoDel.Any(tdd => tdd == t.GrupoLeitoraFilho.Codigo)))
                {
                    GrupoLeitoraHierarq grupoLeitoraHirar = item;
                    grupoLeitoraHirar.Inativar();
                }
            }
            if (grupoNew != null)
            {
                foreach (var item in grupoNew)
                {
                    GrupoLeitoraHierarq grupoLeitoraHirar = new GrupoLeitoraHierarq();
                    grupoLeitoraHirar.GrupoLeitoraPai = grpLeitora;
                    grupoLeitoraHirar.GrupoLeitoraFilho = new GrupoLeitora { Codigo = item };
                    grupoLeitoraHirar.DataRegistro = DateTime.Now;
                    grupoLeitoraHirar.Ativar();
                    repGrpLeitora.SalvaGrupoLeitoraHierarq(grupoLeitoraHirar);
                }
            }

            if (treinamentoDel != null)
            {
                foreach (var item in grpLeitora.Treinamentos.Where(t => treinamentoDel.Any(tdd => tdd == t.Treinamento.Codigo)))
                {
                    GrupoLeitoraTreinamento treinamentoGrpLeitora = item;
                    treinamentoGrpLeitora.Inativar();
                }
            }
            if (treinamentoNew != null)
            {
                foreach (var item in treinamentoNew)
                {
                    GrupoLeitoraTreinamento treinamentoGrpLeitora = new GrupoLeitoraTreinamento();
                    treinamentoGrpLeitora.GrupoLeitora = grpLeitora;
                    treinamentoGrpLeitora.Treinamento = new Treinamento { Codigo = item };
                    treinamentoGrpLeitora.DataRegistro = DateTime.Now;
                    treinamentoGrpLeitora.Ativar();
                    repGrpLeitora.SalvarGrupoLeitoraTreinamento(treinamentoGrpLeitora);
                }
            }

            return Json(montaObj(grpLeitora));
        }

        /// <summary>
        /// Cria um objeto contendo informações relacionadas ao conteudo gerenciado por
        /// GrupoLeitoraController.
        /// </summary>
        ///
        /// <param name="grupoLeitora">
        /// O(a) grupo leitora.
        /// </param>
        ///
        /// <returns>
        /// Um(a) object.
        /// </returns>
        private object montaObj(GrupoLeitora grupoLeitora)
        {
            return new
            {
                Codigo = grupoLeitora.Codigo,
                Nome = grupoLeitora.Nome,
                Ativo = grupoLeitora.Ativo,
                HoraInicio = grupoLeitora.HoraInicio.ToString(@"hh\:mm"),
                HoraFim = grupoLeitora.HoraFim.ToString(@"hh\:mm"),
                Externa = grupoLeitora.Externa,
                ValidaCNH = grupoLeitora.ValidaCNH,
                Supervisao = grupoLeitora.Supervisao,
                AcessoMonitorado = grupoLeitora.AcessoMonitorado,
                AntiPassBack = grupoLeitora.AntiPassBack,
                AreaAlfandegada = grupoLeitora.AreaAlfandegada,
                Leitora = grupoLeitora.Leitoras
                                .Select(td => new LeitoraModelView
                                {
                                    Codigo = td.Leitora.Codigo,
                                    IDLeitora = td.Leitora.IDLeitora,
                                    DirecaoDesc = obterDesc(td.Leitora.Direcao),
                                    Local = td.Leitora.Local,
                                    IP = td.Leitora.Controladora.IP,
                                    TipoControladora = obterDesc(td.Leitora.Controladora.TipoControladora)

                                }).ToList(),
                Treinamento = grupoLeitora.Treinamentos
                                .Select(td => new
                                {
                                    Codigo = td.Treinamento.Codigo,
                                    Nome = td.Treinamento.Nome

                                }).ToList(),
                GruposLeitoras = grupoLeitora.GrupoLeitoraFilho
                                .Select(td => new
                                {
                                    Codigo = td.GrupoLeitoraFilho.Codigo,
                                    Nome = td.GrupoLeitoraFilho.Nome

                                }).ToList(),
            };
        }

        /// <summary>
        /// Obter description.
        /// </summary>
        ///
        /// <param name="direcao">
        /// O(a) direcao.
        /// </param>
        ///
        /// <returns>
        /// Um(a) string.
        /// </returns>
        private string obterDesc(DirecaoLeitora direcao)
        {
            return EnumExtensoes.ObterDescricaoEnum(direcao);
        }

        /// <summary>
        /// Obter description.
        /// </summary>
        ///
        /// <param name="tipoControladora">
        /// O(a) tipo controladora.
        /// </param>
        ///
        /// <returns>
        /// Um(a) string.
        /// </returns>
        private string obterDesc(TipoControladora tipoControladora)
        {
            return EnumExtensoes.ObterDescricaoEnum(tipoControladora);
        }

        /// <summary>
        /// Ativar inativar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult AtivarInativar(int codigo)
        {
            IRepositorioGrupoLeitora repGrpLeitora = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoLeitora>(UnidadeTrabalho);
            GrupoLeitora grpLeitora = repGrpLeitora.ObterPorId(codigo);
            if (grpLeitora.Ativo)
            {
                grpLeitora.Inativar();
            }
            else
            {
                repGrpLeitora.Validar(grpLeitora, EstadoObjeto.Ativado);
                grpLeitora.Ativar();

            }

            return Json(montaObj(grpLeitora));

        }

        /// <summary>
        /// Selecionar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Selecionar(int codigo)
        {
            IRepositorioGrupoLeitora repGrpLeitora = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoLeitora>(UnidadeTrabalho);
            GrupoLeitora grpLeitora = repGrpLeitora.ObterPorId(codigo);
            return Json(montaObj(grpLeitora));
        }

        /// <summary>
        /// Retorna todos os objetos do tipo GrupoLeitora.
        /// </summary>
        ///
        /// <param name="apenasAtivos">
        /// true to apenas ativos.
        /// </param>
        ///
        /// <returns>
        /// Detalhes do tipo de objeto ligado ao controller no formato JSON.
        /// </returns>
        public ActionResult ObterTodos(bool apenasAtivos = false, bool apenasLeitorasUsuario = false)
        {
            IRepositorioGrupoLeitora repGrupoLeitora = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoLeitora>(UnidadeTrabalho);
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);

            IQueryable<GrupoLeitora> grpLeitoras = repGrupoLeitora.ObterTodos();

            if (apenasLeitorasUsuario)
            {
                Usuario usuario = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name);

                grpLeitoras = usuario.GruposUsuarios
                                        .SelectMany(gu => gu.Grupo.GruposLeitoras.Select(gr => gr.GrupoLeitora))
                                        .Distinct().AsQueryable();
            }
            else
                grpLeitoras = repGrupoLeitora.ObterTodos();

            if (apenasAtivos)
            {
                grpLeitoras = grpLeitoras.Where(a => a.Ativo == true);
            }

            return Json(grpLeitoras.Select(a => new GrupoLeitoraModelView()
                                       {
                                           Codigo = a.Codigo,
                                           Nome = a.Nome,
                                           HoraInicio = (a.HoraInicio == null) ? null : a.HoraInicio.ToString(@"hh\:mm"),
                                           HoraFim = (a.HoraFim == null) ? null : a.HoraFim.ToString(@"hh\:mm"),
                                           Ativo = a.Ativo,
                                           AreaAlfandegada = a.AreaAlfandegada
                                       }).ToArray(), JsonRequestBehavior.AllowGet);
        }

         
    }
}
