﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Controlsys.Dominio.Enderecos;
using Controlsys.Dominio.Pessoas;
using Controlsys.Dominio.Seguranca;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorios.Enderecos;
using Controlsys.Repositorios.Pessoas;
using Controlsys.Repositorios.Seguranca;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System.IO;
using System.Web.Helpers;
using Ext.Net;
using Ext.Net.MVC;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar usuarioes.
    /// </summary>
    [ExtendController]
    public class UsuarioController : Controller, IControllerBase
    {
        private int ADMINISTRATORGROUPCODE = 2;
        private int AGENDAMENTOVISITANTEGROUPCODE = 13;
        private int CONTROLEACESSOGROUPCODE = 3;
        /// <summary>
        /// GET: /Usuario/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.UsuarioController.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public UsuarioController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        /// <summary>
        /// Retorna a página de usuario.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Pesquisar(StoreRequestParameters parameters = null, string nome = null, string cpf = null, string passaporte = null, string login = null)
        {
            IQueryable<Usuario> colaboradores = FiltrarUsuarios(nome, cpf, passaporte, login);
            int total = colaboradores.Count();
            List<UsuarioModelView> data = montaObjUsuario(colaboradores, parameters);

            return new StoreResult(data, total);
        }

        private IQueryable<Usuario> FiltrarUsuarios(string nome = null, string cpf = null, string passaporte = null, string login = null)
        {
            IRepositorioUsuario rep = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);

            IQueryable<Usuario> usuarios = rep.ObterTodos();

            if (!string.IsNullOrEmpty(nome))
                usuarios = usuarios.Where(u => u.PessoaFisica.Nome.Contains(nome));

            if (!string.IsNullOrEmpty(cpf))
            {
                cpf = cpf.Replace(".", "").Replace("-", "");
                usuarios = usuarios.Where(u => u.PessoaFisica.CPF == cpf);
            }

            if (!string.IsNullOrEmpty(passaporte))
                usuarios = usuarios.Where(u => u.PessoaFisica.Passaporte.Contains(passaporte));

            if (!string.IsNullOrEmpty(login))
                usuarios = usuarios.Where(u => u.Login.Contains(login));

            return usuarios.OrderBy(u => u.PessoaFisica.Nome);
        }

        /// <summary>
        /// Retorna todos os objetos do tipo Usuario.
        /// </summary>
        ///
        /// <param name="apenasValidos">
        /// true to apenas validos.
        /// </param>
        ///
        /// <returns>
        /// Detalhes do tipo de objeto ligado ao controller no formato JSON.
        /// </returns>
        public ActionResult ObterTodos(bool apenasValidos = false)
        {
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);

            IQueryable<Usuario> usuarios = repUsuario.ObterTodos().OrderBy(p => p.PessoaFisica.Nome);

            if (apenasValidos)
                usuarios = usuarios.Where(u => !u.DataDesativacao.HasValue);

            return Json(usuarios.Select(u => new
                                             {
                                                 u.Codigo,
                                                 CPF = u.PessoaFisica.CPF != null ? u.PessoaFisica.CPFFormatado() : null,
                                                 u.PessoaFisica.Passaporte,
                                                 u.PessoaFisica.Nome,
                                                 u.Login,
                                                 u.Ativo,
                                             })
                                .ToArray());
        }


        /// <summary>
        /// Retorna todos os objetos do tipo Usuario solicitantes.
        /// </summary>
        ///
        /// <param name="apenasValidos">
        /// true to apenas validos.
        /// </param>
        ///
        /// <returns>
        /// Detalhes do tipo de objeto ligado ao controller no formato JSON.
        /// </returns>
        public ActionResult listarUsuarioSolicitante(bool apenasValidos = false)
        {
            List<Usuario> listaUsuariosAux = new List<Usuario>();

            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            IRepositorioGrupoUsuario repGrupoUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoUsuario>(UnidadeTrabalho);
            IQueryable <GrupoUsuario> grupousuario = repGrupoUsuario.ObterTodos().Where(p => (p.Grupo.Codigo == ADMINISTRATORGROUPCODE || p.Grupo.Codigo == CONTROLEACESSOGROUPCODE || p.Grupo.Codigo == AGENDAMENTOVISITANTEGROUPCODE)).OrderBy(t => t.Usuario.PessoaFisica.Nome);
            // IQueryable<Usuario> usuarios = repUsuario.ObterTodos().OrderBy(p => p.PessoaFisica.Nome);
            //IQueryable<Usuario> usuarios = repUsuario.ObterTodos().OrderBy(p => p.PessoaFisica.Nome).Where(x => x.GruposUsuarios.Where(y => y.Grupo.Codigo.Equals(ADMINISTRATORGROUPCODE)));
            //IQueryable<Usuario> usuarios = repUsuario.ObterTodos().OrderBy(p => p.PessoaFisica.Nome).Where(x=>x.GruposUsuarios.Any<GrupoUsuario>.Equals(grupousuario));
            //Where(x => x.GruposUsuarios.Where(g=>g.Codigo.Equals(grupousuario.Codigo)));

            //usuarios = usuarios.Where(x => x.GruposUsuarios.Contains(grupousuario));

            //for (int i = 0; i < usuarios.ToList().Count; i++)
            //{
            //    foreach (var item in usuarios.ToList()[i].GruposUsuarios)
            //    {
            //        if (item.Grupo.Codigo.Equals(ADMINISTRATORGROUPCODE))
            //            listaUsuariosAux.Add(usuarios.ToList()[i]);
            //    }
            //}
            //usuarios = listaUsuariosAux.AsQueryable<Usuario>();

            if (apenasValidos)
                grupousuario = grupousuario.Where(u => !u.DataDesativacao.HasValue);

          
            return Json(grupousuario.Select(u => new
            {
                Codigo = u.Usuario.Codigo,
                Descricao = (u.Usuario.PessoaFisica.Nome + " - " + u.Usuario.PessoaFisica.CPF)

            })
            .ToList().OrderBy(x=>x.Descricao));
        }

        /// <summary>
        /// Inclui um novo usuario.
        /// </summary>
        ///
        /// <param name="usuario">
        /// O(a) usuario.
        /// </param>
        /// <param name="propModificadas">
        /// O(a) property modificadas.
        /// </param>
        /// <param name="imagem">
        /// O(a) imagem.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Incluir(Usuario usuario, string[] propModificadas, string imagem)
        {
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            IRepositorioPessoaFisica repPessoaFisica = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaFisica>(UnidadeTrabalho);
            IRepositorioEndereco repEndereco = Fabrica.Instancia.ObterRepositorio<IRepositorioEndereco>(UnidadeTrabalho);

            PessoaFisica pessoaFisica = usuario.PessoaFisica;
            Endereco end = usuario.PessoaFisica.Endereco;

            //foto
            if (!string.IsNullOrEmpty(imagem))
                pessoaFisica.Foto = Convert.FromBase64String(imagem);

            if (pessoaFisica.CPF != null)
                pessoaFisica.CPF = pessoaFisica.CPF.Replace(".", "").Replace("-", "");

            if (!string.IsNullOrEmpty(pessoaFisica.Codigo))
            {
                pessoaFisica = repPessoaFisica.ObterPorId(pessoaFisica, propModificadas);
                repPessoaFisica.Atualizar(pessoaFisica);
            }            

            //endereço
            pessoaFisica.Endereco = repEndereco.VerificarEndereco(end);
            pessoaFisica.Endereco.DataRegistro = DateTime.Now;
            pessoaFisica.Endereco.Ativar();
            repEndereco.Salvar(pessoaFisica.Endereco);

            //validações referentes à pessoa
            if (string.IsNullOrEmpty(pessoaFisica.Codigo))
            {
                repPessoaFisica.Salvar(pessoaFisica, null);
            }

            if (propModificadas != null)            
                pessoaFisica = insereTelefone(propModificadas, pessoaFisica, usuario);           

            //validações referentes ao usuário
            usuario.DataRegistro = DateTime.Now;
            usuario.Ativar();

            string senha = usuario.GerarSenha();

            usuario.Codigo = pessoaFisica.Codigo;
            usuario.Login = usuario.Login;//usuario.AD ? usuario.Login : usuario.PessoaFisica.CPF;
            repUsuario.Validar(EstadoObjeto.Novo, usuario);
            repUsuario.Salvar(usuario);

            if (!string.IsNullOrEmpty(senha))
                enviarEmail(usuario, senha);

            return Json(montaObj(usuario));
        }

        /// <summary>
        /// Validar login.
        /// </summary>
        ///
        /// <param name="login">
        /// O(a) login.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult ValidarLogin(string login)
        {
            string email = null;

            try
            {
                email = Globalsys.Util.Tools.ValidarLogin(login);
            }
            catch (CoreException)
            {
                return Json(new { Encontrado = false, Email = email });
            }

            return Json(new { Encontrado = true, Email = email });
        }

        /// <summary>
        /// Selecionar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Selecionar(string codigo)
        {
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);

            Usuario usuario = repUsuario.ObterPorId(codigo);

            return Json(montaObj(usuario));
        }

        /// <summary>
        /// Altera um registro de usuario.
        /// </summary>
        ///
        /// <param name="usuario">
        /// O(a) usuario.
        /// </param>
        /// <param name="propModificadas">
        /// O(a) property modificadas.
        /// </param>
        /// <param name="imagem">
        /// O(a) imagem.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Alterar(Usuario usuario, string[] propModificadas, string imagem)
        {
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            IRepositorioPessoaFisica repPessoaFisica = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaFisica>(UnidadeTrabalho);
            IRepositorioEndereco repEndereco = Fabrica.Instancia.ObterRepositorio<IRepositorioEndereco>(UnidadeTrabalho);
            

            PessoaFisica pessoaFisica = usuario.PessoaFisica;
            Endereco enderecoNovo = pessoaFisica.Endereco;

            pessoaFisica = repPessoaFisica.ObterPorId(pessoaFisica, propModificadas);

            //endereço
            repEndereco.Remover(pessoaFisica.Endereco.Codigo);
            pessoaFisica.Endereco = repEndereco.VerificarEndereco(enderecoNovo);
            pessoaFisica.Endereco.DataRegistro = DateTime.Now;
            pessoaFisica.Endereco.Ativar();
            repEndereco.Salvar(pessoaFisica.Endereco);

            //foto
            if (!string.IsNullOrEmpty(imagem))
                pessoaFisica.Foto = Convert.FromBase64String(imagem);
            
            if (propModificadas != null)
            {
                pessoaFisica = insereTelefone(propModificadas, pessoaFisica, usuario);
            }

            repPessoaFisica.Validar(EstadoObjeto.Alterado, pessoaFisica);
            repPessoaFisica.Atualizar(pessoaFisica);

            usuario = repUsuario.ObterPorId(usuario, propModificadas);
            repUsuario.Validar(EstadoObjeto.Alterado, usuario);


            return Json(montaObj(usuario));
        }

        /// <summary>
        /// Ativar inativar.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult AtivarInativar(string codigo)
        {
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);

            Usuario usuario = repUsuario.ObterPorId(codigo);
            if (usuario.Ativo)
                usuario.Inativar();
            else
            {
                repUsuario.Validar(EstadoObjeto.Ativado, usuario);
                usuario.Ativar();
            }

            repUsuario.Atualizar(usuario);
            return Json(montaObj(usuario));
        }

        /// <summary>
        /// Insere telefone.
        /// </summary>
        ///
        /// <param name="propModificadas">
        /// O(a) property modificadas.
        /// </param>
        /// <param name="pessoaFisica">
        /// O(a) pessoa fisica.
        /// </param>
        /// <param name="usuario">
        /// O(a) usuario.
        /// </param>
        ///
        /// <returns>
        /// Um(a) PessoaFisica.
        /// </returns>
        private PessoaFisica insereTelefone(string[] propModificadas, PessoaFisica pessoaFisica, Usuario usuario)
        {
            IRepositorioTelefone repTelefone = Fabrica.Instancia.ObterRepositorio<IRepositorioTelefone>(UnidadeTrabalho);
            List<Telefone> telefones = new List<Telefone>(usuario.PessoaFisica.Telefones);

            if (propModificadas.Any(a => a == "TelefoneNumResidencial"))
            {
                Telefone telefoneResidencial = pessoaFisica.Telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Residencial);

                if (telefoneResidencial == null || (telefoneResidencial.Codigo == 0 && !string.IsNullOrEmpty(telefoneResidencial.TelefoneNum)))
                {
                    telefoneResidencial = new Telefone
                    {
                        DataRegistro = DateTime.Now,
                        Ativo = true,
                        TelefoneNum = (telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Residencial) != null) ? telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Residencial).TelefoneNum : "",
                        TipoTelefone = TipoTelefone.Residencial,
                        Pessoa = pessoaFisica
                    };

                    pessoaFisica.Telefones.Add(telefoneResidencial);
                    repTelefone.Salvar(telefoneResidencial);
                }
                else
                {
                    telefoneResidencial.TelefoneNum = (telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Residencial) != null) ? telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Residencial).TelefoneNum : "";
                    repTelefone.Atualizar(telefoneResidencial);
                }
            }

            if (propModificadas.Any(a => a == "TelefoneNumEmergencial"))
            {
                Telefone telefoneResidencial = pessoaFisica.Telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Emergencial);

                if (telefoneResidencial == null || (telefoneResidencial.Codigo == 0 && !string.IsNullOrEmpty(telefoneResidencial.TelefoneNum)))
                {
                    telefoneResidencial = new Telefone
                    {
                        DataRegistro = DateTime.Now,
                        Ativo = true,
                        TelefoneNum = (telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Emergencial) != null) ? telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Emergencial).TelefoneNum : "",
                        TipoTelefone = TipoTelefone.Emergencial,
                        Pessoa = pessoaFisica
                    };

                    repTelefone.Salvar(telefoneResidencial);
                    pessoaFisica.Telefones.Add(telefoneResidencial);
                }
                else
                {
                    telefoneResidencial.TelefoneNum = (telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Emergencial) != null) ? telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Emergencial).TelefoneNum : "";
                    repTelefone.Atualizar(telefoneResidencial);
                }
            }

            if (propModificadas.Any(a => a == "TelefoneNumCelular"))
            {
                Telefone telefoneResidencial = pessoaFisica.Telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Celular);

                if (telefoneResidencial == null || (telefoneResidencial.Codigo == 0 && !string.IsNullOrEmpty(telefoneResidencial.TelefoneNum)))
                {
                    telefoneResidencial = new Telefone
                    {
                        DataRegistro = DateTime.Now,
                        Ativo = true,
                        TelefoneNum = (telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Celular) != null) ? telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Celular).TelefoneNum : "",
                        TipoTelefone = TipoTelefone.Celular,
                        Pessoa = pessoaFisica
                    };

                    repTelefone.Salvar(telefoneResidencial);
                    pessoaFisica.Telefones.Add(telefoneResidencial);
                }
                else
                {
                    telefoneResidencial.TelefoneNum = (telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Celular) != null) ? telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Celular).TelefoneNum : "";
                    repTelefone.Atualizar(telefoneResidencial);
                }
            }

            return pessoaFisica;
        }

        /// <summary>
        /// Enviar email.
        /// </summary>
        ///
        /// <param name="usuario">
        /// O(a) usuario.
        /// </param>
        /// <param name="senha">
        /// O(a) senha.
        /// </param>
        private void enviarEmail(Usuario usuario, string senha)
        {
            Dictionary<string, string> valoresSubs = new Dictionary<string, string>();

            valoresSubs.Add("$$Login$$", usuario.Login);
            valoresSubs.Add("$$Senha$$", senha);

            Globalsys.Util.Email.Enviar("[CONTROLSYS] - Dados de acesso",
                                        Server.MapPath("/Content/templates/acessoUsuario.htm"),
                                        valoresSubs,
                                        string.Empty,
                                        null,
                                        usuario.PessoaFisica.Email);
        }

        /// <summary>
        /// Cria um objeto contendo informações relacionadas ao conteudo gerenciado por
        /// UsuarioController.
        /// </summary>
        ///
        /// <param name="usuario">
        /// O(a) usuario.
        /// </param>
        ///
        /// <returns>
        /// Um(a) object.
        /// </returns>
        private object montaObj(Usuario usuario)
        {
            Endereco endereco = usuario.PessoaFisica.Endereco;

            Telefone telefoneRes = usuario.PessoaFisica.Telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Residencial);
            Telefone telefoneEmer = usuario.PessoaFisica.Telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Emergencial);
            Telefone telefoneCel = usuario.PessoaFisica.Telefones.FirstOrDefault(t => t.TipoTelefone == TipoTelefone.Celular);

            var usuarioModelView = new UsuarioModelView();
            usuarioModelView.Codigo = usuario.Codigo;
            usuarioModelView.Login = usuario.Login;
            usuarioModelView.AD = usuario.AD;
            usuarioModelView.Ativo = usuario.Ativo;

            usuarioModelView.CodigoPessoa = usuario.PessoaFisica.Codigo;
            usuarioModelView.CPF = usuario.PessoaFisica.CPF != null ? usuario.PessoaFisica.CPFFormatado() : null;
            usuarioModelView.Passaporte = usuario.PessoaFisica.Passaporte;
            usuarioModelView.Nome = usuario.PessoaFisica.Nome;
            usuarioModelView.Apelido = usuario.PessoaFisica.Apelido;
            usuarioModelView.Email = usuario.PessoaFisica.Email;
            usuarioModelView.DataNascimento = usuario.PessoaFisica.DataNascimento;
            usuarioModelView.DataVisto = usuario.PessoaFisica.DataVisto;
            usuarioModelView.DataValidadeVisto = usuario.PessoaFisica.DataValidadeVisto;
            usuarioModelView.Sexo = usuario.PessoaFisica.Sexo;
            usuarioModelView.NomeMae = usuario.PessoaFisica.NomeMae;
            usuarioModelView.NomePai = usuario.PessoaFisica.NomePai;
            usuarioModelView.RNE = usuario.PessoaFisica.RNE;
            usuarioModelView.DataExpedicaoRNE = usuario.PessoaFisica.DataExpedicaoRNE;
            usuarioModelView.EstadoCivil = usuario.PessoaFisica.EstadoCivil;
            usuarioModelView.Escolaridade = usuario.PessoaFisica.Escolaridade;
            usuarioModelView.TipoSanguineo = usuario.PessoaFisica.TipoSanguineo;
            usuarioModelView.Nacionalidade = usuario.PessoaFisica.Nacionalidade;
            usuarioModelView.Img = (usuario.PessoaFisica.Foto != null) ? Convert.ToBase64String(usuario.PessoaFisica.Foto) : "";

            usuarioModelView.NaturalidadeUF = usuario.PessoaFisica.Naturalidade != null && usuario.PessoaFisica.Naturalidade.Estado != null ? usuario.PessoaFisica.Naturalidade.Estado.Codigo : (int?)null;
            usuarioModelView.Naturalidade = usuario.PessoaFisica.Naturalidade != null ? usuario.PessoaFisica.Naturalidade.Codigo : (int?)null;

            //RG
            usuarioModelView.RG = usuario.PessoaFisica.RG != null ? usuario.PessoaFisica.RG : null;
            usuarioModelView.RGOrgaoEmissor = usuario.PessoaFisica.RGOrgaoEmissor != null ? usuario.PessoaFisica.RGOrgaoEmissor : null;
            usuarioModelView.RGOrgaoEmissorUF = usuario.PessoaFisica.RGOrgaoEmissorUF != null ? usuario.PessoaFisica.RGOrgaoEmissorUF.Codigo : (int?)null;
            usuarioModelView.RGDataEmissao = usuario.PessoaFisica.RGDataEmissao != null ? usuario.PessoaFisica.RGDataEmissao : (DateTime?)null;

            //Titulo de eleitor
            usuarioModelView.TituloEleitor = usuario.PessoaFisica.TituloEleitor != null ? usuario.PessoaFisica.TituloEleitor : (int?)null;
            usuarioModelView.TituloEleitorSecao = usuario.PessoaFisica.TituloEleitorSecao != null ? usuario.PessoaFisica.TituloEleitorSecao : (int?)null;
            usuarioModelView.TituloEleitorZona = usuario.PessoaFisica.TituloEleitorZona != null ? usuario.PessoaFisica.TituloEleitorZona : (int?)null;
            usuarioModelView.TituloEleitorEstado =  (usuario.PessoaFisica.TituloEleitorCidade != null && usuario.PessoaFisica.TituloEleitorCidade.Estado != null) ? usuario.PessoaFisica.TituloEleitorCidade.Estado.Codigo : (int?)null;
            usuarioModelView.TituloEleitorCidade = usuario.PessoaFisica.TituloEleitorCidade != null ? usuario.PessoaFisica.TituloEleitorCidade.Codigo : (int?)null;


                //CTPS                
            usuarioModelView.CTPS = usuario.PessoaFisica.CTPS != null ? usuario.PessoaFisica.CTPS : (int?)null;
            usuarioModelView.CTPSData = usuario.PessoaFisica.CTPSData != null ? usuario.PessoaFisica.CTPSData : (DateTime?)null;
            usuarioModelView.CTPSSerie = usuario.PessoaFisica.CTPSSerie != null ? usuario.PessoaFisica.CTPSSerie : null;
            usuarioModelView.CTPSEstado = usuario.PessoaFisica.CTPSEstado != null ? usuario.PessoaFisica.CTPSEstado.Codigo : (int?)null;


            //CNH
            usuarioModelView.CNH = usuario.PessoaFisica.CNH != null ? usuario.PessoaFisica.CNH : null;
            usuarioModelView.CNHCategoria = usuario.PessoaFisica.CNHCategoria != null ? usuario.PessoaFisica.CNHCategoria : null;
            usuarioModelView.CNHDataValidade = usuario.PessoaFisica.CNHDataValidade != null ? usuario.PessoaFisica.CNHDataValidade : (DateTime?)null;

            //Certificado de Reservista
            usuarioModelView.CertificadoReservista = usuario.PessoaFisica.CertificadoReservista != null ? usuario.PessoaFisica.CertificadoReservista : null;
            usuarioModelView.CertificadoReservistaCat = usuario.PessoaFisica.CertificadoReservistaCat != null ? usuario.PessoaFisica.CertificadoReservistaCat : null;

            //Telefones
            usuarioModelView.TelefoneNumResidencial = telefoneRes != null ? telefoneRes.TelefoneNum : null;
            usuarioModelView.TelefoneNumEmergencial = telefoneEmer != null ? telefoneEmer.TelefoneNum : null;
            usuarioModelView.TelefoneNumCelular = telefoneCel != null ? telefoneCel.TelefoneNum : null;

               if (endereco != null)
                {
                    if (!string.IsNullOrEmpty(endereco.Complemento) && endereco.Logradouro == null)
                    {
                        usuarioModelView.Endereco = new EnderecoMovelView()
                            {
                                CodigoEndereco = endereco.Codigo,
                                Complemento = endereco.Complemento
                            };
                    }
                    else
                    {
                        usuarioModelView.Endereco = new EnderecoMovelView()
                        {
                            CodigoEndereco = endereco.Codigo,
                            CodigoLog = endereco.Logradouro.Codigo,
                            Pais = endereco.Logradouro.Bairro.Cidade.Estado.Pais.Nome,
                            Estado = endereco.Logradouro.Bairro.Cidade.Estado.Nome,
                            Cidade = endereco.Logradouro.Bairro.Cidade.Nome,
                            Bairro = endereco.Logradouro.Bairro.Nome,
                            Logradouro = endereco.Logradouro.Nome,
                            Numero = endereco.Numero,
                            Complemento = endereco.Complemento,
                            CEP = endereco.Logradouro.CEP //,
                            //TipoLogradouro = endereco.Logradouro.TipoLogradouro.Nome
                        };
                    }
            
            };

               return usuarioModelView;
        }

        private List<UsuarioModelView> montaObjUsuario(IQueryable<Usuario> usuarios, StoreRequestParameters parameters = null, bool ignoreParameters = false)
        {
            if (!ignoreParameters)
            {
                if (parameters == null)
                    parameters = new StoreRequestParameters();
                usuarios = usuarios.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 25);
            }

            return usuarios.Select(s => new UsuarioModelView()
            {
                Codigo = s.Codigo,
                Nome = s.PessoaFisica.Nome,
                Login = s.Login,
                CPF = s.PessoaFisica.CPF != null ? s.PessoaFisica.CPFFormatado() : null,
                Passaporte = s.PessoaFisica.Passaporte,
                Ativo = s.Ativo
            }).ToList();
        }

        #region Webcam / Jcrop

        /// <summary>
        /// Edit.
        /// </summary>
        ///
        /// <param name="x">
        /// The x coordinate.
        /// </param>
        /// <param name="y">
        /// The y coordinate.
        /// </param>
        /// <param name="w">
        /// The width.
        /// </param>
        /// <param name="h">
        /// The height.
        /// </param>
        /// <param name="imageUrl">
        /// URL of the image.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [HttpPost]
        public ActionResult Edit(int x, int y, int w, int h, string imageUrl)
        {
            var image = new WebImage("~" + imageUrl);
            var height = image.Height;
            var width = image.Width;

            image.Crop((int)x, (int)y, (int)(height - w), (int)(width - h));

            int indiceDoPonto = imageUrl.IndexOf(".");
            string antesPonto = imageUrl.Substring(0, indiceDoPonto);
            string depoisPonto = imageUrl.Substring(indiceDoPonto, imageUrl.Length - indiceDoPonto);
            string nome = antesPonto + "_crop" + depoisPonto;

            image.Save(Server.MapPath("~" + nome));

            return Json(nome);

        }

        /// <summary>
        /// Upload.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Upload()
        {
            HttpPostedFileBase postedFile = Request.Files[0];

            Random randNum = new Random();
            int number = randNum.Next(DateTime.Now.Millisecond);

            var fileName = Path.GetFileName(postedFile.FileName);
            string nomeArquivo = (number + Path.GetExtension(postedFile.FileName)).ToString();
            postedFile.SaveAs(Server.MapPath("~/Content/img/webcam/" + nomeArquivo));

            return Json(nomeArquivo);

        }

        /// <summary>
        /// Carregar imagem.
        /// </summary>
        ///
        /// <param name="imagem">
        /// O(a) imagem.
        /// </param>
        ///
        /// <returns>
        /// Um(a) byte[].
        /// </returns>
        private byte[] CarregarImagem(string imagem)
        {
            byte[] imagemBytes = null;
            string caminhoCompletoImagem = imagem;

            int posicao = caminhoCompletoImagem.IndexOf("/Content");
            string caminho = caminhoCompletoImagem.Substring(posicao);
            imagemBytes = System.IO.File.ReadAllBytes(Server.MapPath("~" + caminho));
            return imagemBytes;
        }

        //private string GravarTemporario
        //    if (foto != null)
        //    {
        //        Random randNum = new Random();
        //        int number = randNum.Next(DateTime.Now.Millisecond);

        //        string nomeArquivo = number + ".jpeg";

        //        byte[] b = foto;

        //        using (FileStream fs = new FileStream(Server.MapPath("~/Content/img/webcam/") + nomeArquivo, FileMode.Create))
        //            fs.Write(b, 0, b.Length);

        //        return "/Content/img/webcam/" + nomeArquivo;
        //    }
        //    return "/Content/img/avatar.png";
        //}

        #endregion



        public ActionResult UsuarioLogado()
        {
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            Usuario usuario = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name);
            return Json(usuario.PessoaFisica.Nome);
        }
    }
}
