﻿
using Controlsys.Dominio.Pessoas;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorios.Pessoas;
using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Controlsys.Repositorios.Empresas;
using Controlsys.Dominio.Empresas;
using Controlsys.Repositorio.Parametros;
using Controlsys.Repositorios.Seguranca;
using Controlsys.Dominio.Seguranca;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Acesso;
using Controlsys.Repositorio.Acesso;
using Controlsys.Repositorios.Acesso;
using Globalsys.Exceptions;

namespace Controlsys.Interfaces.Controllers
{
    /// <summary>
    /// Um controlador para tratar acesso customizadoes.
    /// </summary>
    [ExtendController]
    public class AcessoCustomizadoController : Controller, IControllerBase
    {
        /// <summary>
        /// GET: /AcessoCustomizado/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Controllers.AcessoCustomizadoController.
        /// </summary>
        ///
        /// <param name="unidade">
        /// O(a) unidade.
        /// </param>
        public AcessoCustomizadoController(IUnidadeTrabalho unidade) { UnidadeTrabalho = unidade; }

        /// <summary>
        /// Retorna a página de acessoCustomizado.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        public ActionResult Index(string codigo, string opcao)
        {
            //return View("Index", model: codigo );
            return View("Index", model: codigo + '#' + opcao);
        }

        /// <summary>
        /// Altera um registro de acessoCustomizado.
        /// </summary>
        ///
        /// <param name="papel">
        /// O(a) papel.
        /// </param>
        /// <param name="papelLog">
        /// O(a) papel log.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ActionResult.
        /// </returns>
        [Transaction]
        public ActionResult Alterar(PapelModelView papel, PapelLog papelLog, string opcao = null)
        {
            IRepositorioPapel repPapel = Fabrica.Instancia.ObterRepositorio<IRepositorioPapel>(UnidadeTrabalho);
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            IRepositorioSincronizaPessoaAcesso repSincronizaPessoaAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioSincronizaPessoaAcesso>(UnidadeTrabalho);

            IRepositorioGrupoUsuario repGrupoUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoUsuario>(UnidadeTrabalho);

            Papel papelA = repPapel.ObterPorId(papel.Codigo);
            PapelLog papelLogA = papelA.ObterLogPapel();

            papelA.SobAnalise = papel.SobAnalise;
            papelA.Supervisionado = papel.Supervisionado;
            papelA.Supervisor = papel.Supervisor;

            if (!string.IsNullOrEmpty(papel.SenhaLeitora) && papelA.SenhaLeitora != papel.SenhaLeitora)
                papelA.SenhaLeitora = papel.SenhaLeitora;

            if (papelLog.Status.ToString() == "BaixaDefinitiva")
            {
                Usuario usuarioLogado = repUsuario.ObterPorLogin(HttpContext.User.Identity.Name, somenteAtivos: true);
                if (usuarioLogado != null)
                {
                    var grupos = usuarioLogado.GruposUsuarios.Select(g => g.Grupo);
                    var acoes = grupos.SelectMany(g => g.Acoes);
                    var ok = false;
                    switch (opcao)
                    {
                        case "v":
                            if (!(acoes.Where(a => a.Url == "/Visitante/ExecutarBDvisitante")).Any()) ok = true; break;
                        case "c":
                            if (!(acoes.Where(a => a.Url == "/Colaborador/ExecutarBDcolaborador")).Any()) ok = true; break;
                        case "p":
                            if (!(acoes.Where(a => a.Url == "/PrestadorServico/ExecutarBDprestador")).Any()) ok = true; break;
                    }
                    if(ok) throw new CoreException("Você não tem permissão para alterar o status para baixa definitiva.");

                    IQueryable<GrupoUsuario> grupousuario = repGrupoUsuario.ObterTodos().Where(p => (p.Usuario.PessoaFisica.Codigo == papelA.PessoaFisica.Codigo));
                    foreach (var item in grupousuario)
                    {
                        item.Inativar();                        
                    }
                }
            }

            if (papelLogA.Status != papelLog.Status)
            {
                papelLog.Papel = papelA;
                papelLog.DataRegistro = DateTime.Now;
                repPapel.ValidarPapelLog(papelLogA, papelLog);
                repPapel.SalvarPapelLog(papelLog);
                papelA.PapelLogs.Add(papelLog);

                DateTime? dataInicio = null;

                if (papelLog.DataInicio != DateTime.Today)
                    dataInicio = papelLog.DataInicio;

                if (papelLog.Status != StatusPapelLog.Ativo)
                {
                    Usuario usuario = repUsuario.ObterPorPessoa(papelLog.Papel.PessoaFisica.Codigo);
                    if (usuario != null && papelLog.Status != StatusPapelLog.Ferias)
                    {
                        usuario.Inativar();
                        repUsuario.Atualizar(usuario);
                    }

                    repSincronizaPessoaAcesso.Sincronizar(papelA, TipoSincronizacaoPessoa.Conceder, dataInicio);

                    if (papelLog.Status == StatusPapelLog.BaixaDefinitiva)
                    {
                        repPapel.DarBaixaDefinitiva(papelLog);
                        return null;
                    }
                }
                else
                {
                    repSincronizaPessoaAcesso.Sincronizar(papelA, TipoSincronizacaoPessoa.Conceder, dataInicio);
                }
            }
            else
            {
                papelLogA.DataInicio = papelLog.DataInicio;
                papelLogA.DataFim = papelLog.DataFim;
                papelLogA.Motivo = papelLog.Motivo;
            }
            return null;
        }
    }
}