﻿using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Parametros;
using Controlsys.Infra;
using Controlsys.Interfaces.App_Start;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Empresas;
using Controlsys.Repositorios.Empresas;
using Controlsys.Repositorios.Parametros;
using Ext.Net;
using Ext.Net.MVC;
using Globalsys;
using Microsoft.Reporting.WebForms;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Controllers
{
    [ExtendController]
    public class RelatorioClassificacaoJuridicaController : Controller, IControllerBase
    {

        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        public RelatorioClassificacaoJuridicaController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Pesquisar(StoreRequestParameters parameters, string cnpj = null, string[] codigoEmpresa = null, string classificacao = null, DateTime? dataInicio = null, DateTime? dataFim = null)
        {
            List<EmpresaClassificacaoJuridica> resultadoFiltro = Filtrar(cnpj, codigoEmpresa, classificacao, dataInicio, dataFim);
            int total = resultadoFiltro.Count();
            EmpresaClassificacaoJuridicaModelView[] modelView = MontarObjetos(resultadoFiltro, parameters);
            return new StoreResult(modelView, total);
        }

        public List<EmpresaClassificacaoJuridica> Filtrar(string cnpj = null, string[] codigoEmpresas = null, string classificacao = null, DateTime? dataInicio = null, DateTime? dataFim = null)
        {
            IQueryable<Empresa> empresas = Fabrica.Instancia.ObterRepositorio<IRepositorioEmpresa>(UnidadeTrabalho).ObterTodos();
            List<EmpresaClassificacaoJuridica> empresasClassificacaoJuridica = new List<EmpresaClassificacaoJuridica>();

            if (!string.IsNullOrEmpty(cnpj))
            {
                string cnpjNaoFormatado = Regex.Replace(cnpj, "[^0-9]", "");
                empresas = empresas.Where(e => e.CNPJ.Equals(cnpjNaoFormatado));
            }

            if (codigoEmpresas != null && codigoEmpresas.Any(e => !string.IsNullOrEmpty(e)))
                empresas = empresas.Where(e => codigoEmpresas.Contains(e.Codigo));

            foreach (Empresa empresa in empresas)
            {
                EmpresaClassificacaoJuridica ecj = new EmpresaClassificacaoJuridica(empresa, dataInicio, dataFim);
                ecj.DarNotaEmpresa();
                empresasClassificacaoJuridica.Add(ecj);
            }

            if (!string.IsNullOrEmpty(classificacao))
                empresasClassificacaoJuridica = empresasClassificacaoJuridica.Where(ecj => !string.IsNullOrEmpty(ecj.Classificacao) && ecj.Classificacao.Equals(classificacao)).ToList();

            return empresasClassificacaoJuridica;
        }

        public ActionResult Exportar(string tipoRelatorio, string cnpj = null, string[] codigoEmpresa = null, string classificacao = null, DateTime? dataInicio = null, DateTime? dataFim = null)
        {
            LocalReport relatorio = new LocalReport();

            List<EmpresaClassificacaoJuridica> resultadoFiltro = Filtrar(cnpj, codigoEmpresa, classificacao, dataInicio, dataFim);
            EmpresaClassificacaoJuridicaModelView[] modelView = MontarObjetos(resultadoFiltro, null, true);

            List<object> relatorioModelView = new List<object>();

            foreach (EmpresaClassificacaoJuridicaModelView ecj in modelView)
            {
                foreach (ItemEmpresaClassificacaoJuridicaModelView ecji in ecj.Items)
                {
                    relatorioModelView.Add(new
                    {
                        Apelido = ecj.Apelido,
                        Classificacao = ecj.Classificacao,
                        CNPJ = ecj.CNPJ,
                        Codigo = ecj.Codigo,
                        Formula = ecji.Formula,
                        Letra = ecji.Letra,
                        Nome = ecji.Nome,
                        Peso = ecji.Peso,
                        Pontua = ecji.Pontua,
                        Pontuacao = ecji.Pontuacao,
                        Quantidade = ecji.Quantidade,
                        Nota = ecj.Nota
                    }
                    );
                }
            }

            relatorio.DataSources.Add(new ReportDataSource("DTOClassificacaoJuridica", relatorioModelView));
            relatorio.ReportPath = Server.MapPath("~/Reports/RelatorioClassificacaoJuridica.rdlc");
            CultureInfo cultura = new CultureInfo("pt-BR");
            IRepositorioParametro repParam = Fabrica.Instancia.ObterRepositorio<IRepositorioParametro>(UnidadeTrabalho);
            double classBomMax = Convert.ToDouble(repParam.RecuperarParametro(ParametroSistema.ClassificacaoJuridicaBomMaximo).Valor, cultura);
            double classBomMin = Convert.ToDouble(repParam.RecuperarParametro(ParametroSistema.ClassificacaoJuridicaBomMinimo).Valor, cultura);
            double classMedMax = Convert.ToDouble(repParam.RecuperarParametro(ParametroSistema.ClassificacaoJuridicaMedioMaximo).Valor, cultura);
            double classMedMin = Convert.ToDouble(repParam.RecuperarParametro(ParametroSistema.ClassificacaoJuridicaMedioMinimo).Valor, cultura);
            double classRuiMax = Convert.ToDouble(repParam.RecuperarParametro(ParametroSistema.ClassificacaoJuridicaRuimMaximo).Valor, cultura);
            double classRuiMin = Convert.ToDouble(repParam.RecuperarParametro(ParametroSistema.ClassificacaoJuridicaRuimMinimo).Valor, cultura);
            relatorio.SetParameters(new ReportParameter("ClassificacaoBomMax", classBomMax.ToString()));
            relatorio.SetParameters(new ReportParameter("ClassificacaoBomMin", classBomMin.ToString()));
            relatorio.SetParameters(new ReportParameter("ClassificacaoMedMax", classMedMax.ToString()));
            relatorio.SetParameters(new ReportParameter("ClassificacaoMedMin", classMedMin.ToString()));
            relatorio.SetParameters(new ReportParameter("ClassificacaoRuiMax", classRuiMax.ToString()));
            relatorio.SetParameters(new ReportParameter("ClassificacaoRuiMin", classRuiMin.ToString()));


            string mimeType;
            string fileName = string.Empty;
            switch (tipoRelatorio)
            {
                case "PDF":
                    mimeType = "application/pdf";
                    fileName = "download.pdf";
                    break;
                case "Excel":
                    mimeType = "application/vnd.ms-excel";
                    fileName = "download.xls";
                    break;
            }

            string encoding;
            string fileNameExtension;

            Warning[] warnings;
            string[] streams;
            byte[] bytes;

            bytes = relatorio.Render(
            tipoRelatorio,
            null,
            out mimeType,
            out encoding,
            out fileNameExtension,
            out streams,
            out warnings);

            return File(bytes, mimeType, fileName);
        }

        public EmpresaClassificacaoJuridicaModelView[] MontarObjetos(List<EmpresaClassificacaoJuridica> resultadoFiltro, StoreRequestParameters parameters, bool ignoreParameters = false)
        {
            //if (!ignoreParameters)
            //{
            //    if (parameters == null)
            //        parameters = new StoreRequestParameters();
            //    resultadoFiltro = resultadoFiltro.Skip(parameters.Start > 0 ? parameters.Start : 0).Take(parameters.Limit > 0 ? parameters.Limit : 25).ToList();
            //}

            EmpresaClassificacaoJuridicaModelView[] objeto = resultadoFiltro.Select(f => new EmpresaClassificacaoJuridicaModelView
            {
                Apelido = f.Empresa.Nome,
                Classificacao = f.Classificacao,
                CNPJ = f.Empresa.CNPJFormatado(),
                Codigo = f.Empresa.Codigo,
                Items = f.ClassificacoesJuridicas.Select(cj => new ItemEmpresaClassificacaoJuridicaModelView
                {
                    Formula = cj.ClassificacaoJuridica.Formula,
                    Letra = cj.ClassificacaoJuridica.Letra,
                    Nome = cj.ClassificacaoJuridica.ItemClassificacao.ObterDescricaoEnum(),
                    Peso = cj.ClassificacaoJuridica.Peso,
                    Pontua = cj.ClassificacaoJuridica.Pontua,
                    Pontuacao = cj.Pontuacao.HasValue ? Math.Truncate(cj.Pontuacao.Value * 100000) / 100000 : cj.Pontuacao,
                    Quantidade = cj.Quantidade
                }).ToArray(),
                Nota = Math.Truncate(f.NotaFinal * 100000) / 100000
            }).OrderBy(ecj => ecj.Apelido).ToArray();
            return objeto;
        }
    }
}
