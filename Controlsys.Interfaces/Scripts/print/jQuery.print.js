﻿// Create a jquery plugin that prints the given element.
jQuery.fn.print = function () {
    // NOTE: We are trimming the jQuery collection down to the
    // first element in the collection.
    if (this.size() > 1) {
        this.eq(0).print();
        return;
    } else if (!this.size()) {
        return;
    }

    // ASSERT: At this point, we know that the current jQuery
    // collection (as defined by THIS), contains only one
    // printable element.

    // Create a random name for the print frame.
    var strFrameName = ("printer-" + (new Date()).getTime());

    // Create an iFrame with the new name.
    var jFrame = $("<iframe name='" + strFrameName + "'>");

    // Hide the frame (sort of) and attach to the body.
    jFrame
    .css("width", "1px")
    .css("height", "1px")
    .css("position", "absolute")
    .css("left", "-9999px")
    .appendTo($("body:first"))
    ;

    // Get a FRAMES reference to the new frame.
    var objFrame = window.frames[strFrameName];

    // Get a reference to the DOM in the new frame.
    var objDoc = objFrame.document;

    // Grab all the style tags and copy to the new
    // document so that we capture look and feel of
    // the current document.

    // Create a temp document DIV to hold the style tags.
    // This is the only way I could find to get the style
    // tags into IE.
    var jStyleDiv = $("<div>").append(
    $("style").clone()
    );

    // Write the HTML for the document. In this, we will
    // write out the HTML of the current element.
    var windowContent = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">";
    windowContent += "<html>";
    windowContent += "<head>";
    windowContent += "<title>";
    windowContent += document.title;
    windowContent += "</title>";
    windowContent += "<script type='text/javascript' src='/Scripts/fontsmoothie/fontsmoothie.min.js' async></script>";
    windowContent += "<style type='text/css'>";
    windowContent += "html,body,div,span,applet,object,iframe,h1,h2,h3,h4,h5,h6,p,blockquote,pre,a,abbr,acronym,address,big,cite,code,del,dfn,em,img,ins,kbd,q,s,samp,small,strike,strong,sub,sup,tt,var,b,u,i,center,dl,dt,dd,ol,ul,li,fieldset,form,label,legend,table,caption,tbody,tfoot,thead,tr,th,td,article,aside,canvas,details,embed,figure,figcaption,footer,header,hgroup,menu,nav,output,ruby,section,summary,time,mark,audio,video{border:0;font-size:100%;font:inherit;vertical-align:baseline;margin:0;padding:0}article,aside,details,figcaption,figure,footer,header,hgroup,menu,nav,section{display:block}body{line-height:1}ol,ul{list-style:none}blockquote,q{quotes:none}blockquote:before,blockquote:after,q:before,q:after{content:none}table{border-collapse:collapse;border-spacing:0}";
    windowContent += "@font-face {font-family:'TKTypeBold';src: url('/Content/font/TKTypeBold.eot');src: url('/Content/font/TKTypeBold.eot?#iefix') format('embedded-opentype'),url('/Content/font/TKTypeBold.woff') format('woff'),url('/Content/font/TKTypeBold.ttf') format('truetype'),url('/Content/font/TKTypeBold.otf') format('opentype'),url('/Content/font/TKTypeBold.svg#TKTypeBold') format('svg');font-weight: 700;font-style: normal;font-stretch: normal;unicode-range: U+0020-2215;}";
    windowContent += "#content,#preview-p{width: 294.21669291355px;height: 461.27937007897px;padding: 10px 10px 0px 10px;}#mainRoom,#mainRoomWin{width:284.22px;height:451.28px;float:left;border:1px solid #AAA;background-color:#d8d8d8}#mainRoomWin{float:left;width:284.22px;height:451.28px;border:0 solid #AAA}#mainRoomChild{width:100%;height:100%;background-repeat:no-repeat;background-size:cover}.campo{background-color:transparent;border-radius:2px;font-size:8pt;width:auto;height:auto;margin:10px;color:#000;text-align:left;display:inline-block;word-wrap:break-word}#mainRoomWin .campo{margin-left:20px;margin-top:20px}#mainRoom .imagem,#mainRoomWin .imagem,#tables .imagem{height:173px;width:133px}#mainRoom .logo,#mainRoomWin .logo{height:63px;width:203px}";
    windowContent += "</style>";
    windowContent += "</head>";
    windowContent += "<body>";
    windowContent += this.html();
    windowContent += "</body>";
    windowContent += "</html>";

    objDoc.open();
    objDoc.write(windowContent);
    objDoc.close();

    // Print the document.
    try {
        objDoc.focus();
        var result = objDoc.execCommand('print', true, null);
        if (!result) {
            objFrame.focus();
            objFrame.print();
        }
    } catch (error) {
        objFrame.focus();
        objFrame.print();
    }
    //if ((navigator.userAgent.indexOf("MSIE") != -1)) {
    //    objDoc.focus();
    //    objDoc.execCommand('print', false, null);
    //} else {
    //    objFrame.focus();
    //    objFrame.print();
    //}

    // Have the frame remove itself in about a minute so that
    // we don't build up too many of these frames.
    setTimeout(
    function () {
        jFrame.remove();
    },
    (60 * 1000)
    );
}