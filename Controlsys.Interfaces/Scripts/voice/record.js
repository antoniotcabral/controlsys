function restore() {
    $("#record,#live").removeClass("disabled");
    $(".one").addClass("disabled"); $.voice.stop();
}

$(document).ready(function () {

	$(document).on("click", "#record:not(.disabled)", function(){
		elem = $(this);
		$.voice.record($("#live").is(":checked"), function(){
			elem.addClass("disabled");
			$("#live").addClass("disabled");
			$("#stop,#play,#download").removeClass("disabled");
		});
	});
	$(document).on("click", "#stop:not(.disabled)", function(){
		restore();
	});

});