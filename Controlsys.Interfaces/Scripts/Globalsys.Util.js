﻿var Globalsys = Globalsys || {};

Globalsys.Util = {

    getModifiedProperties: function (form) {
        var fields = form.form.getFields().filterBy(function (field) {
            return field.isDirty();
        });

        return fields.items.map(function (field) {
            return field.name;
        });
    },

    setFieldValues: function (form, data, dontSetOriginalValue) {
        form.form.getFields().each(function (field) {
            var fieldValue = data[field.name];

            if (fieldValue) {
                field.setValue(fieldValue);

                if (!dontSetOriginalValue)
                    field.originalValue = fieldValue;
            }

        }, this);
    },

    resetFieldValues: function (form, dontSetOriginalValue) {
        form.form.getFields().each(function (field) {
            field.setValue(undefined);

            if (!dontSetOriginalValue)
                field.originalValue = undefined;
        });

        form.reset();
    },

    updateModel: function (grid, modelNew, dontCommit) {

        var codigo = modelNew.Codigo;

        if ('NaN' != parseInt(codigo).toString())
            codigo = parseInt(codigo);

        var modelOld = grid.store.getById(codigo);

        for (var i in modelNew)
            modelOld.set(i, modelNew[i]);

        if (!dontCommit)
            modelOld.commit();
    },

    updateModelByValue: function (grid, modelNew, dontCommit, value) {

        var modelOld = grid.store.getById(value);

        for (var i in modelNew)
            modelOld.set(i, modelNew[i]);

        if (!dontCommit)
            modelOld.commit();
    },

    updateModelByRowIndex: function (grid, modelNew, dontCommit, rowIndex) {

        var modelOld = grid.store.getAt(rowIndex);

        for (var i in modelNew)
            modelOld.set(i, modelNew[i]);

        if (!dontCommit)
            modelOld.commit();
    },

    request: function (config) {

        if (!config.bloqMask)
            Ext.net.Mask.show({ msg: 'Carregando...' });

        var pTimeout = config.personalTimeout || 120000;

        Ext.Ajax.request({
            timeout: pTimeout,
            url: config.url,
            scope: this,
            jsonData: config.params,
            form: config.form,
            //timeout: ,
            isUpload: config.isUpload || false,
            success: function (response, opts) {

                Ext.net.Mask.hide();

                var objResponse = Ext.decode(response.responseText);
                //var objResponse = JSON.parseWithDate(response.responseText);

                if (objResponse.Success) {
                    var data = objResponse.Data || objResponse.data;
                    config.success.call(this, data, opts);
                } else {
                    Ext.Msg.show({
                        title: 'Crítica',
                        msg: objResponse.Message,
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.WARNING
                    });
                    return false;
                }
            },
            failure: function (response, opts) {
                Ext.net.Mask.hide();

                var objResponse = null;

                if (response.responseText)
                    objResponse = Ext.decode(response.responseText);
                
                if ((opts == 'timeout'))
                    Console.log("timeour error - " + new Date().toString());
                else if (!objResponse || !objResponse.Success) {
                    Ext.Msg.show({
                        title: 'Crítica',
                        msg: 'Houve um problema no servidor. Contacte o administrador do sistema ou tente novamente mais tarde.',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.WARNING
                    });
                }
            }
        });
    },

    onBeforeRequest: function (conn, options, eOpts) {
        
    },

    onRequestComplete: function (conn, response, options, eOpts) {
        var objResponse = null;

        if (response.responseText) {
            objResponse = Ext.decode(response.responseText);

            if (!objResponse.Success) {
                debugger
                Ext.Msg.show({
                    title: 'Crítica',
                    msg: objResponse.Message,
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.WARNING
                });
                return false;
            }
        }
    },

    onRequestException: function (conn, response, options, eOpts) {
        Ext.Msg.show({
            title: 'Crítica',
            msg: "Houve um problema no servidor. Contacte o administrador do sistema ou tente novamente mais tarde." + response.responseText,
            width: 1500,            
            buttons: Ext.Msg.OK,
            icon: Ext.MessageBox.WARNING
        });

        return false;
    },

    documentReady: function (scope) {
        Ext.Ajax.on('beforerequest', Globalsys.Util.onBeforeRequest, scope);
        Ext.Ajax.on('requestcomplete', Globalsys.Util.onRequestComplete, scope);
        Ext.Ajax.on('requestexception', Globalsys.Util.onRequestException, scope);
    },

    createWindow: function (config) {
        new Ext.Window({
            title: config.title,
            width: config.width,
            height: config.height,
            closeAction: 'destroy',
            items: [{
                xtype: "component",
                layout: "fit",
                autoEl: {
                    tag: "iframe",
                    src: config.url,
                    width: '100%',
                    height: '100%'
                }
            }]
        }).show();
    },

    downloadFile: function (url, params) {

        var body = Ext.getBody(),
            frame = body.createChild({
                tag: 'iframe',
                cls: 'x-hidden',
                id: 'hiddenform-iframe',
                name: 'iframe'
            }),

            form = body.createChild({
                tag: 'form',
                method: 'POST',
                cls: 'x-hidden',
                id: 'hiddenform-form',
                action: url,
                target: 'iframe'
            });

        for (var i in params) {
            if (Ext.isArray(params[i])) {
                for (var j = 0; j < params[i].length; j++) {
                    form.createChild({
                        tag: 'input',
                        type: 'hidden',
                        cls: 'x-hidden',
                        id: 'hiddenform-' + i,
                        name: i,
                        value: params[i][j]
                    });
                }
            } else {
                if (params[i] instanceof Date) {
                    params[i] = Ext.encode(params[i]).replace('"', "").replace('"', "");
                }
                form.createChild({
                    tag: 'input',
                    type: 'hidden',
                    cls: 'x-hidden',
                    id: 'hiddenform-' + i,
                    name: i,
                    value: params[i]
                });
            }
        }

        form.dom.submit();

        return frame;
    },

    showFileOnWindow: function (config) {
        var wind = Ext.create("Ext.window.Window", {
            closeAction: 'destroy',
            width: config.width || '100%',
            height: config.height || '100%',
            loader: {
                url: config.url,
                autoLoad: true,
                params: config.params,
                ajaxOptions: {
                    headers: config.contentType,
                    binary: true
                }
            }
        });

        wind.loader.on('load', function (scope, response, options, eOpts) {

            var blob = new Blob([response.responseBytes], { type: config.contentType }),
			url = window.URL.createObjectURL(blob),
            iframe = document.createElement('iframe');

            iframe.width = '100%';
            iframe.height = '100%';
            iframe.src = url;

            wind.getBody().appendChild(iframe);
        });

        wind.show();
    },

    base64ToBlob: function (b64Data, contentType, sliceSize) {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;

        var byteCharacters = atob(b64Data);
        var byteArrays = [];

        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
        }

        var blob = new Blob(byteArrays, { type: contentType });
        return blob;
    },

    
    obterBlob: function(url, cb) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'blob';
    xhr.onload = function (e) {
        if (this.status == 200) {
            var myBlob = this.response;

            // myBlob is now the blob that the object URL pointed to.
            var reader = new FileReader();
            reader.onload = function () {
                var dataUrl = reader.result;
                var base64 = dataUrl.split(',')[1];
                cb(base64);
            };
            reader.readAsDataURL(myBlob);
        }
    };
    xhr.send();
    },

    validaCnh: function (numeroCnh) {
        
        var Soma, Mult, Digito1, Digito2, Incr_dig2; //: integer;
        var CNH_Forn, Dig_Forn, Dig_Enc;// : string;
   
        if (numeroCnh.length < 11) {
            //return Globalsys.Util.validaCnhAntiga(numeroCnh);
            return false;
        }
   
        CNH_Forn = numeroCnh.substr(0,9);
        Dig_Forn = numeroCnh.substr(9, 2);
   
        Incr_dig2 = 0;
   
        Soma = 0;
        Mult = 9;
   
        for (j = 0; j < 9;j++)
        {
            Soma = Soma + (parseInt(CNH_Forn.substr(j, 1)) * Mult);
            Mult = Mult - 1;
        }

        Digito1 = Soma % 11;
	
        if(Digito1 == 10)
        {
            Incr_dig2 = -2;
        }
	
        if(Digito1 > 9)
        {
            Digito1 = 0;
        }
	
        Soma = 0;
        Mult = 1;
	
        for(j = 0;j < 9; j++)
        {
            Soma = Soma + (parseInt(CNH_Forn.substr(j, 1)) * Mult);
            Mult = Mult + 1;
        }
		
        if((Soma % 11) + Incr_dig2 >= 0)
        {
            Digito2 = (Soma % 11) + Incr_dig2;
        }
        if(Digito2 > 9)
        {
            Digito2 = 0;
        }
	
        Dig_Enc = Digito1.toString() + Digito2.toString();
	
        if(Dig_Forn == Dig_Enc)
        {
            return true;
        }
        else
        {
            return false;
        }
    },


    validaCnhAntiga: function(numeroCnh) {

        var Soma, Mult, Digito, Incr_dig2; //: integer;
        var CNH_Forn, Dig_Forn, Dig_Enc;// : string;
   
        if(numeroCnh.length != 9){
            return false;
        }
	 
        CNH_Forn = numeroCnh.substr(0, 8);
        Dig_Forn = numeroCnh.substr(8, 1);
   
        Soma = 0;
        Mult = 2;
   
        for(j= 0;j < 8;j++)
        {
            Soma = Soma + (parseInt(CNH_Forn.substr(j, 1)) * Mult);
        
        }
  
        Digito = Soma % 11;
	 
        if(Digito > 9)
        {
            Digito = 0;
        }
        Dig_Enc = Digito.toString();
	 
        if(Dig_Forn == Dig_Enc)
        {
            return true;
        }
        else
        {
            return false;
        }
    },

    preenchePlacaVeiculo: function (txtPlaca) {
        
        var placa = txtPlaca.getValue();
        if (placa.length == 4) {
            var novoValor = placa[0] + placa[1] + placa[2] + '-' + placa[3];
            txtPlaca.setValue(novoValor);
        } else {
            txtPlaca.setValue(placa.toUpperCase());
        }
    },
    //Ex: soNumero(campo.value);
    soNumero: function (valor) {
        var er = new RegExp(/^[0-9]{1,}$/);
        return er.test(valor);
    }
};
