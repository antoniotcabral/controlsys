﻿// Setup JSON parsing to handle .NET DateTime format
Ext.decode = function (json) {
    return JSON.parse(json, function (key, value) {
        if (typeof value === "string") {
            //tratamento para data
            var re = /^\/Date\((d|-|.*)\)[\/|\\]$/;
            var m = re.exec(value);

            if (m && m[0]) {
                var b = m[1].split(/[-+,.]/);
                value = new Date(b[0] ? +b[0] : 0 - +b[1]);
            }
            return value;
        }
        return value;
    });
};

Date.prototype.toJSON = function (key) {
    // For JSON.NET it is required to remove rounding backslashes to parse it correctly
    return isFinite(this.valueOf()) ? "/Date(" + this.getTime() + ")/" : null;
};

if (Ext.net.InputMask)
    Ext.override(Ext.net.InputMask, {
        getErrors: function () {
            var field = this.getCmp(),
                errors = this.fieldGetErrors.call(field, field.processRawValue(field.getRawValue()));

            if (errors.length == 0 && field.getRawValue().length == 0 && field.allowBlank)
                return errors;

            if (!this.isValueValid())
                errors.push(this.invalidMaskText || field.invalidText);

            return errors;
        }
    });

//Criando vtypes para validação de cpf, cnpj e number
Ext.apply(Ext.form.VTypes, {
    cnpj: function (val, field) {
        return verifyCNPJ(val);
    },
    cnpjText: 'CNPJ inválido!',
    cpf: function (b, a) {
        return validacpf(b);
    },
    cpfText: "CPF inválido!",
    number: function(val, field) {
        return validaNumero(val);
    },
    numberText: "Número inválido!",
    numberMask: /^[0-9]/i
});

function verifyCNPJ(cnpj) {
    cnpj = cnpj.replace(/\D/g, "").replace(/^0+/, "");

    if (isNaN(parseInt(cnpj))) {
        return false;
    } else {
        tam = cnpj.length - 2;
        if (realTestCNPJ(cnpj, tam) == 1) {
            tam = cnpj.length - 1;
            if (realTestCNPJ(cnpj, tam) == 1) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
};

function realTestCNPJ(cnpj, tam) {
    var verCNPJ = 0;
    var ind = 2;
    var tam;

    for (f = tam; f > 0; f--) {
        verCNPJ += parseInt(cnpj.charAt(f - 1)) * ind;
        if (ind > 8) {
            ind = 2;
        } else {
            ind++;
        }
    }

    verCNPJ %= 11;
    if (verCNPJ == 0 || verCNPJ == 1) {
        verCNPJ = 0;
    } else {
        verCNPJ = 11 - verCNPJ;
    }

    if (verCNPJ != parseInt(cnpj.charAt(tam))) {
        return (0);
    } else {
        return (1);
    }
};

function validacpf(e) {
    if (e == "")
        return true;
    var b;
    s = e.replace(/\D/g, "");
    if (parseInt(s, 10) == 0) {
        return false;
    }

    var iguais = true;
    for (i = 0; i < s.length - 1; i++) {
        if (s.charAt(i) != s.charAt(i + 1)) {
            iguais = false;
        }
    }

    if (iguais)
        return false;

    var h = s.substr(0, 9);
    var a = s.substr(9, 2);
    var d = 0;
    for (b = 0; b < 9; b++) {
        d += h.charAt(b) * (10 - b);
    }
    if (d == 0) {
        return false;
    }
    d = 11 - (d % 11);
    if (d > 9) {
        d = 0;
    }
    if (a.charAt(0) != d) {
        return false;
    }
    d *= 2;
    for (b = 0; b < 9; b++) {
        d += h.charAt(b) * (11 - b);
    }
    d = 11 - (d % 11);
    if (d > 9) {
        d = 0;
    }
    if (a.charAt(1) != d) {
        return false;
    }
    return true;
}

function validaNumero(val) {
    var reg = /^\d+$/i;
    return reg.test(val);
}


String.prototype.replaceAll = function(de, para) {
    var str = this;
    var pos = str.indexOf(de);
    while (pos > -1) {
        str = str.replace(de, para);
        pos = str.indexOf(de);
    }
    return (str).toString();
};

var onComboBoxSelect = function (combo) {
    var store = combo.up("gridpanel").getStore();
    store.pageSize = parseInt(combo.getValue(), 10);
    //store.reload();
};

Array.prototype.remove = function () {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};