﻿using System.Web;
using System.Web.Mvc;
using Globalsys;

namespace Controlsys.Interfaces
{
    /// <summary>
    /// Representa um(a) FilterConfig.
    /// </summary>
    public class FilterConfig
    {
        /// <summary>
        /// Register global filters.
        /// </summary>
        ///
        /// <param name="filters">
        /// O(a) filters.
        /// </param>
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new CustomAuthorizeAttribute());
        }
    }
}