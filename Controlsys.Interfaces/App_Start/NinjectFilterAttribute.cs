﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ninject;

namespace Globalsys
{
    /// <summary>
    /// atributos para ninject filter.
    /// </summary>
    public class NinjectFilterAttribute : FilterAttributeFilterProvider
    {
        /// <summary>
        /// O(a) kernel.
        /// </summary>
        private readonly IKernel _kernel;

        /// <summary>
        /// Construtor para Globalsys.NinjectFilterAttribute.
        /// </summary>
        ///
        /// <param name="kernel">
        /// O(a) kernel.
        /// </param>
        public NinjectFilterAttribute(IKernel kernel)
        {
            this._kernel = kernel;
        }

        /// <summary>
        /// Get controller attributes.
        /// </summary>
        ///
        /// <param name="controllerContext">
        /// Context for the controller.
        /// </param>
        /// <param name="actionDescriptor">
        /// Information describing the action.
        /// </param>
        ///
        /// <returns>
        /// Um(a) IEnumerable&lt;FilterAttribute&gt;
        /// </returns>
        protected override IEnumerable<FilterAttribute> GetControllerAttributes(ControllerContext controllerContext, ActionDescriptor actionDescriptor)
        {
            var attributes = base.GetControllerAttributes(controllerContext, actionDescriptor);
            foreach (var attribute in attributes)
            {
                this._kernel.Inject(attribute);
            }

            return attributes;
        }

        /// <summary>
        /// Get action attributes.
        /// </summary>
        ///
        /// <param name="controllerContext">
        /// Context for the controller.
        /// </param>
        /// <param name="actionDescriptor">
        /// Information describing the action.
        /// </param>
        ///
        /// <returns>
        /// Um(a) IEnumerable&lt;FilterAttribute&gt;
        /// </returns>
        protected override IEnumerable<FilterAttribute> GetActionAttributes(ControllerContext controllerContext, ActionDescriptor actionDescriptor)
        {
            var attributes = base.GetActionAttributes(controllerContext, actionDescriptor);
            foreach (var attribute in attributes)
            {
                this._kernel.Inject(attribute);
            }

            return attributes;
        }
    }
}