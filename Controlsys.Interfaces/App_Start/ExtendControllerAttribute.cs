﻿using System;
using System.Diagnostics;
using System.Web.Mvc;
using Controlsys.Dominio.Audit;
using Controlsys.Infra;
using Controlsys.Repositorio.Audit;
using Ext.Net.MVC;
using Globalsys.Exceptions;

namespace Globalsys
{
    /// <summary>
    /// Um controlador para tratar extends.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public class ExtendController : ActionFilterAttribute, IActionFilter
    {
        /// <summary>
        /// On action executing.
        /// </summary>
        ///
        /// <param name="filterContext">
        /// Context for the filter.
        /// </param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
        }

        /// <summary>
        /// On action executed.
        /// </summary>
        ///
        /// <param name="filterContext">
        /// Context for the filter.
        /// </param>
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            string url = string.Format("/{0}/{1}", filterContext.ActionDescriptor.ActionName, filterContext.ActionDescriptor.ControllerDescriptor.ControllerName);

            if (filterContext.Result is JsonResult || filterContext.Result is EmptyResult || filterContext.Result is StoreResult)
            {
                JsonResult result = new JsonResult();

                string msg = string.Empty;
                bool success = true;

                if (filterContext.Exception != null)
                {
                    success = false;

                    Exception exception = filterContext.Exception;

                    filterContext.ExceptionHandled = true;

                    msg = exception is CoreException ?
                                    exception.Message :
                                        "Houve um erro no servidor. Contate o administrador ou tente novamente mais tarde.";

                    if (!(exception is CoreException))
                    {
                        Exception ex = exception;

                        if (ex.InnerException != null)
                            ex = ex.InnerException;

                        LogError logError = new LogError
                        {
                            Message = ex.Message,
                            StackTrace = ex.StackTrace,
                            Url = url,
                            DataRegistro = DateTime.Now,
                            UserName = filterContext.HttpContext.User.Identity.Name
                        };

                        IUnidadeTrabalho uow = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
                        IRepositorioLogError repLogError = Fabrica.Instancia.ObterRepositorio<IRepositorioLogError>(uow);
                        repLogError.Salvar(logError);
                    }
                }

                result.MaxJsonLength = int.MaxValue;

                if (filterContext.Result is StoreResult)
                {
                    result.Data = new
                    {
                        Success = success,
                        total = (filterContext.Result as StoreResult).Total,
                        data = (filterContext.Result as StoreResult).Data,
                        message = msg
                    };
                }
                else
                {
                    result.Data = new
                    {
                        Success = success,
                        Data = filterContext.Result is JsonResult ? (filterContext.Result as JsonResult).Data : filterContext.Result,
                        Message = msg
                    };
                }


                if (filterContext.Result is JsonResult)
                    result.ContentType = (filterContext.Result as JsonResult).ContentType;

                if (filterContext.Result is FileContentResult)
                    result.ContentType = (filterContext.Result as FileContentResult).ContentType;

                //if (filterContext.Result is StoreResult)
                //    result.ContentType = (filterContext.Result as StoreResult).ToString();

                result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;

                filterContext.Result = result;
            }

            base.OnActionExecuted(filterContext);
        }
    }
}