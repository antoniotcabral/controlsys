﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Globalsys
{
    /// <summary>
    /// Interface IControllerBase.
    /// </summary>
    public interface IControllerBase
    {
        /// <summary>
        /// Gets/Sets valor para UnidadeTrabalho.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        IUnidadeTrabalho UnidadeTrabalho { get; set; }
    }
}
