﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Controlsys.Dominio.Audit;
using Controlsys.Dominio.Seguranca;
using Controlsys.Infra;
using Controlsys.Repositorios.Audit;
using Controlsys.Repositorios.Seguranca;
using Globalsys.Exceptions;

namespace Globalsys
{
    /// <summary>
    /// atributos para custom authorize.
    /// </summary>
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        /// <summary>
        /// On authorization.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="filterContext">
        /// Context for the filter.
        /// </param>
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);

            string usuarioLogado = filterContext.HttpContext.User.Identity.Name;

            IUnidadeTrabalho unidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();

            IRepositorioPermissao repPermissao = Fabrica.Instancia.ObterRepositorio<IRepositorioPermissao>(unidadeTrabalho);
            IRepositorioPagina repPagina = Fabrica.Instancia.ObterRepositorio<IRepositorioPagina>(unidadeTrabalho);
            IRepositorioAcao repAcao = Fabrica.Instancia.ObterRepositorio<IRepositorioAcao>(unidadeTrabalho);

            ActionDescriptor actionDescriptor = filterContext.ActionDescriptor;
            ControllerDescriptor controllerDescriptor = actionDescriptor.ControllerDescriptor;

            if (!actionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true) &&
                !controllerDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true))
            {
                string url = string.Format("/{0}/{1}", controllerDescriptor.ControllerName, actionDescriptor.ActionName);

                try
                {
                    //verifica se o usuário está autenticado
                    if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
                        filterContext.Result = new RedirectResult(Globalsys.Seguranca.Config.UrlLogin);
                    else
                    {
                        Acao acao = repAcao.ObterPorUrl(url);

                        if (!Globalsys.Seguranca.Config.DefaultUrls.Contains(url))
                        {
                            //verifica se a action é uma pagina
                            if (actionDescriptor.ActionName == Globalsys.Seguranca.Config.ActionIndex)
                            {
                                //usuário possui permissão para alguma ação desta página.
                                if (!repPermissao.PossuiPermissaoPagina(usuarioLogado, url))
                                    throw new CoreException("Acesso negado. Você não possui permissão para acessar a página selecionada. " + url);
                            }
                            else
                            {

                                //verifica se o usuário possui permissão para a ação executada.
                                if (!repPermissao.PossuiPermissao(usuarioLogado, url))
                                    throw new CoreException(string.Format("Acesso negado. Você não possui permissão para executar a ação {0}.", acao != null ? acao.Nome : url));
                            }
                        }
                        if (acao != null && acao.Auditavel)
                            registrarAudit(filterContext.HttpContext.User.Identity.Name, unidadeTrabalho, url);
                    }
                }
                catch (CoreException ex)
                {
                    filterContext.Result = new JsonResult
                    {
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                        Data = new { Success = false, Message = ex.Message },
                        ContentType = "application/json"
                    };
                }
            }
        }

        /// <summary>
        /// Registrar audit.
        /// </summary>
        ///
        /// <exception cref="Exception">
        /// Lançada quando uma condição de erro exception ocorre.
        /// </exception>
        ///
        /// <param name="userIdentName">
        /// Name of the user identifier.
        /// </param>
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        /// <param name="url">
        /// URL of the document.
        /// </param>
        public void registrarAudit(string userIdentName, IUnidadeTrabalho unidadeTrabalho, string url)
        {
            try
            {
                unidadeTrabalho.BeginTransaction();

                IRepositorioAuditoria repAudit = Fabrica.Instancia.ObterRepositorio<IRepositorioAuditoria>(unidadeTrabalho);
                IRepositorioAcao repAcao = Fabrica.Instancia.ObterRepositorio<IRepositorioAcao>(unidadeTrabalho);
                IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(unidadeTrabalho);

                Acao acao = repAcao.ObterPorUrl(url);
                Usuario usuario = repUsuario.ObterPorLogin(userIdentName);

                if (acao == null)
                    return;

                Auditoria audit = new Auditoria
                {
                    Acao = acao,
                    Url = url,
                    Usuario = usuario,
                    DataRegistro = DateTime.Now,
                    Descricao = acao != null ? !string.IsNullOrEmpty(acao.Descricao) ? acao.Descricao : string.Empty : string.Empty
                };

                repAudit.Salvar(audit);
                unidadeTrabalho.Commit();
            }
            catch (Exception ex)
            {
                unidadeTrabalho.Rollback();
                throw ex;
            }
            finally
            {
                unidadeTrabalho.Dispose();
            }
        }
    }
}