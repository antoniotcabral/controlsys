﻿using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.CredenciamentoVeiculos;
using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Ocorrencias;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Controlsys.Infra;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Acesso;
using Controlsys.Repositorio.Empresas;
using Controlsys.Repositorio.Ocorrencias;
using Controlsys.Repositorio.Parametros;
using Controlsys.Repositorios.Empresas;
using Controlsys.Repositorios.Parametros;
using Controlsys.Repositorios.Pessoas;
using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
namespace Controlsys.Interfaces.App_Start
{
    public class EmpresaClassificacaoJuridica
    {
        #region Propriedades

        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        public Empresa Empresa { get; set; }

        public List<ItemClassificacaoJuridicaModelView> ClassificacoesJuridicas { get; set; }

        public double NotaFinal { get; set; }

        public string Classificacao { get; set; }

        private DateTime? DataInicio { get; set; }

        private DateTime? DataFim { get; set; }

        #endregion

        #region Construtores

        public EmpresaClassificacaoJuridica()
        {
            UnidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            ClassificacoesJuridicas = new List<ItemClassificacaoJuridicaModelView>();

        }

        public EmpresaClassificacaoJuridica(string codigoEmpresa, DateTime? dataInicio = null, DateTime? dataFim = null)
        {
            UnidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            Empresa = Fabrica.Instancia.ObterRepositorio<IRepositorioEmpresa>(UnidadeTrabalho).ObterPorId(codigoEmpresa);
            ClassificacoesJuridicas = new List<ItemClassificacaoJuridicaModelView>();
            DataInicio = dataInicio;
            DataFim = dataFim;
        }

        public EmpresaClassificacaoJuridica(Empresa _empresa, DateTime? dataInicio = null, DateTime? dataFim = null)
        {
            UnidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            Empresa = _empresa;
            ClassificacoesJuridicas = new List<ItemClassificacaoJuridicaModelView>();
            DataInicio = dataInicio;
            DataFim = dataFim;
        }

        #endregion

        public void DarNotaEmpresa()
        {
            ObterQuantidades();
            ObterPontuacoes();
            ObterNotaEClassificacao();
        }

        private void ObterNotaEClassificacao()
        {
            NotaFinal = 10 - this.ClassificacoesJuridicas.Select(cj => cj.Pontuacao).Sum().Value;

            IRepositorioParametro repParam = Fabrica.Instancia.ObterRepositorio<IRepositorioParametro>(UnidadeTrabalho);

            double classBomMax = Convert.ToDouble(repParam.RecuperarParametro(ParametroSistema.ClassificacaoJuridicaBomMaximo).Valor);
            double classBomMin = Convert.ToDouble(repParam.RecuperarParametro(ParametroSistema.ClassificacaoJuridicaBomMinimo).Valor);
            double classMedMax = Convert.ToDouble(repParam.RecuperarParametro(ParametroSistema.ClassificacaoJuridicaMedioMaximo).Valor);
            double classMedMin = Convert.ToDouble(repParam.RecuperarParametro(ParametroSistema.ClassificacaoJuridicaMedioMinimo).Valor);
            double classRuiMax = Convert.ToDouble(repParam.RecuperarParametro(ParametroSistema.ClassificacaoJuridicaRuimMaximo).Valor);
            double classRuiMin = Convert.ToDouble(repParam.RecuperarParametro(ParametroSistema.ClassificacaoJuridicaRuimMinimo).Valor);

            Classificacao = NotaFinal >= classBomMin && NotaFinal <= classBomMax ? "Verde" : NotaFinal >= classMedMin && NotaFinal <= classMedMax ? "Amarelo" : "Vermelho";

        }

        private void ObterPontuacoes()
        {
            foreach (ItemClassificacaoJuridicaModelView item in ClassificacoesJuridicas)
            {
                if (item.ClassificacaoJuridica.Pontua)
                {
                    var expressionAux = NCalc.Expression.Compile(item.ClassificacaoJuridica.Formula, false);

                    ParameterExtractionVisitor visitor = new ParameterExtractionVisitor();
                    expressionAux.Accept(visitor);

                    var extractedParameters = visitor.Parameters;

                    List<string> letras = new List<string>();
                    NCalc.Expression exp = new NCalc.Expression(item.ClassificacaoJuridica.Formula);

                    foreach (string param in extractedParameters)
                    {
                        ItemClassificacaoJuridicaModelView icj = ClassificacoesJuridicas.Where(cj => cj.ClassificacaoJuridica.Letra == param).FirstOrDefault();
                        exp.Parameters.Add(param, icj.Quantidade);
                    }

                    object resultado = exp.Evaluate();

                    if (resultado is double)
                    {
                        item.Pontuacao = double.IsNaN((double)resultado) || double.IsInfinity((double)resultado) ? 0 : (double)resultado;
                    }
                    else
                        item.Pontuacao = Convert.ToDouble(resultado);

                    item.Pontuacao = item.Pontuacao * item.ClassificacaoJuridica.Peso;
                }
            }
        }

        private void ObterQuantidades()
        {
            IQueryable<ClassificacaoJuridica> classificacoesJuridicas = Fabrica.Instancia.ObterRepositorio<IRepositorioClassificacaoJuridica>(UnidadeTrabalho).ObterTodos();
            classificacoesJuridicas = classificacoesJuridicas.Where(cj => cj.Ativo);

            foreach (ClassificacaoJuridica item in classificacoesJuridicas)
            {
                switch (item.ItemClassificacao)
                {
                    case ItemClassificacaoJuridica.CNPJAtivos:
                        ClassificacoesJuridicas.Add(new ItemClassificacaoJuridicaModelView() { ClassificacaoJuridica = item, Quantidade = this.ObterQuantidadeCNPJAtivos() });
                        break;
                    case ItemClassificacaoJuridica.PedidosDeCompraAtivos:
                        ClassificacoesJuridicas.Add(new ItemClassificacaoJuridicaModelView() { ClassificacaoJuridica = item, Quantidade = this.ObterQuantidadePedidosDeCompraAtivos(Empresa.Codigo) });
                        break;
                    case ItemClassificacaoJuridica.Colaboradores:
                        ClassificacoesJuridicas.Add(new ItemClassificacaoJuridicaModelView() { ClassificacaoJuridica = item, Quantidade = this.ObterQuantidadeColaboradores(Empresa.Codigo) });
                        break;
                    case ItemClassificacaoJuridica.CrachasColaboradoresInativosNaoDevolvidos:
                        ClassificacoesJuridicas.Add(new ItemClassificacaoJuridicaModelView() { ClassificacaoJuridica = item, Quantidade = this.ObterQuantidadeCrachasColaboradoresInativosNaoDevolvidos(Empresa.Codigo) });
                        break;
                    case ItemClassificacaoJuridica.Prestadores:
                        ClassificacoesJuridicas.Add(new ItemClassificacaoJuridicaModelView() { ClassificacaoJuridica = item, Quantidade = this.ObterQuantidadePrestadores(Empresa.Codigo) });
                        break;
                    case ItemClassificacaoJuridica.CrachasPrestadoresInativosNaoDevolvidos:
                        ClassificacoesJuridicas.Add(new ItemClassificacaoJuridicaModelView() { ClassificacaoJuridica = item, Quantidade = this.ObterQuantidadeCrachasPrestadoresInativosNaoDevolvidos(Empresa.Codigo) });
                        break;
                    case ItemClassificacaoJuridica.Visitantes:
                        ClassificacoesJuridicas.Add(new ItemClassificacaoJuridicaModelView() { ClassificacaoJuridica = item, Quantidade = this.ObterQuantidadePrestadores(Empresa.Codigo) });
                        break;
                    case ItemClassificacaoJuridica.CrachasVisitantesInativosNaoDevolvidos:
                        ClassificacoesJuridicas.Add(new ItemClassificacaoJuridicaModelView() { ClassificacaoJuridica = item, Quantidade = this.ObterQuantidadeCrachasPrestadoresInativosNaoDevolvidos(Empresa.Codigo) });
                        break;
                    case ItemClassificacaoJuridica.CredencialVeicularEmitidas:
                        ClassificacoesJuridicas.Add(new ItemClassificacaoJuridicaModelView() { ClassificacaoJuridica = item, Quantidade = this.ObterQuantidadeCredencialVeicularEmitidas(Empresa.Codigo) });
                        break;
                    case ItemClassificacaoJuridica.CredenciaisVencidas:
                        ClassificacoesJuridicas.Add(new ItemClassificacaoJuridicaModelView() { ClassificacaoJuridica = item, Quantidade = this.ObterQuantidadeCredenciaisVencidas(Empresa.Codigo) });
                        break;
                    case ItemClassificacaoJuridica.PessoasCriticas:
                        ClassificacoesJuridicas.Add(new ItemClassificacaoJuridicaModelView() { ClassificacaoJuridica = item, Quantidade = this.ObterQuantidadePessoasCriticas(Empresa.Codigo) });
                        break;
                    case ItemClassificacaoJuridica.OcorrenciaSegLeve:
                        ClassificacoesJuridicas.Add(new ItemClassificacaoJuridicaModelView() { ClassificacaoJuridica = item, Quantidade = this.ObterQuantidadeOcorrenciaSegLeve(Empresa.Codigo) });
                        break;
                    case ItemClassificacaoJuridica.OcorrenciaSegMedia:
                        ClassificacoesJuridicas.Add(new ItemClassificacaoJuridicaModelView() { ClassificacaoJuridica = item, Quantidade = this.ObterQuantidadeOcorrenciaSegMedia(Empresa.Codigo) });
                        break;
                    case ItemClassificacaoJuridica.OcorrenciaSegGrave:
                        ClassificacoesJuridicas.Add(new ItemClassificacaoJuridicaModelView() { ClassificacaoJuridica = item, Quantidade = this.ObterQuantidadeOcorrenciaSegGrave(Empresa.Codigo) });
                        break;
                    case ItemClassificacaoJuridica.OcorrenciaSegGravissima:
                        ClassificacoesJuridicas.Add(new ItemClassificacaoJuridicaModelView() { ClassificacaoJuridica = item, Quantidade = this.ObterQuantidadeOcorrenciaSegGravissima(Empresa.Codigo) });
                        break;
                    case ItemClassificacaoJuridica.OcorrenciaTransLeve:
                        ClassificacoesJuridicas.Add(new ItemClassificacaoJuridicaModelView() { ClassificacaoJuridica = item, Quantidade = this.ObterQuantidadeOcorrenciaTransLeve(Empresa.Codigo) });
                        break;
                    case ItemClassificacaoJuridica.OcorrenciaTransMedia:
                        ClassificacoesJuridicas.Add(new ItemClassificacaoJuridicaModelView() { ClassificacaoJuridica = item, Quantidade = this.ObterQuantidadeOcorrenciaTransMedia(Empresa.Codigo) });
                        break;
                    case ItemClassificacaoJuridica.OcorrenciaTransGrave:
                        ClassificacoesJuridicas.Add(new ItemClassificacaoJuridicaModelView() { ClassificacaoJuridica = item, Quantidade = this.ObterQuantidadeOcorrenciaTransGrave(Empresa.Codigo) });
                        break;
                    case ItemClassificacaoJuridica.OcorrenciaTransGravissima:
                        ClassificacoesJuridicas.Add(new ItemClassificacaoJuridicaModelView() { ClassificacaoJuridica = item, Quantidade = this.ObterQuantidadeOcorrenciaTransGravissima(Empresa.Codigo) });
                        break;
                    default:
                        break;
                }
            }
        }

        private Boolean ValidarData(DateTime dataRegistro)
        {
            
            // Estou filtrando por Data INICIO e FIM, traga os que foram registrados entre essas datas
            if ((DataInicio != null && DataFim != null))
                return (dataRegistro.Date >= DataInicio.Value.Date && dataRegistro <= DataFim.Value.Date);

            // Estou filtrando apenas pela data INICIO, traga os registros com data maior ou igual data INICIO
            else if (DataInicio != null && DataFim == null)
                return dataRegistro.Date >= DataInicio.Value.Date;

            // Estou filtrando apenas pela data FIM, traga os registros com data menor ou igual data INICIO
            else if (DataInicio == null && DataFim != null)
                return dataRegistro.Date <= DataFim.Value.Date;
            // Nao estou filtrando por data, traga todos
            else 
                return true;
        }

        public int ObterQuantidadeCNPJAtivos()
        {
            if (Empresa.Ativo && ValidarData(Empresa.DataRegistro))
                return 1;
            else
                return 0;
        }

        public int ObterQuantidadePedidosDeCompraAtivos(string codigoEmpresa)
        {
            IQueryable<PedidoCompra> pedidos = Fabrica.Instancia.ObterRepositorio<IRepositorioPedidoCompra>(UnidadeTrabalho).ObterTodos();
            return pedidos.Where(pc => pc.Empresa.Codigo.Contains(codigoEmpresa) && pc.Status == StatusPedido.Ativo).ToList().Count(pc => ValidarData(pc.DataRegistro));
        }

        public int ObterQuantidadeColaboradores(string codigoEmpresa)
        {
            IQueryable<Colaborador> colaboradores = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho).ObterTodos();
            return colaboradores.Where(c => c.Empresa.Codigo.Contains(codigoEmpresa) && 
                (c.PapelLogs.OrderByDescending(x => x.Codigo).Select(m => m.Status).FirstOrDefault() == StatusPapelLog.Ativo ||
                c.PapelLogs.OrderByDescending(x => x.Codigo).Select(m => m.Status).FirstOrDefault() == StatusPapelLog.Ferias ||
                c.PapelLogs.OrderByDescending(x => x.Codigo).Select(m => m.Status).FirstOrDefault() == StatusPapelLog.Afastamento)
            ).ToList().Count(x => ValidarData(x.DataRegistro));
        }

        public int ObterQuantidadeCrachasColaboradoresInativosNaoDevolvidos(string codigoEmpresa)
        {
            IQueryable<Colaborador> colaboradores = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho).ObterTodos();
            colaboradores = colaboradores.Where(c => c.Empresa.Codigo.Contains(codigoEmpresa) &&
                c.PapelLogs.OrderByDescending(x => x.Codigo).Select(m => m.Status).FirstOrDefault() == StatusPapelLog.Inativacao);

            IQueryable<Cracha> crachasInativosNaoDevolvidos = Fabrica.Instancia.ObterRepositorio<IRepositorioCracha>(UnidadeTrabalho).ObterTodos();
            crachasInativosNaoDevolvidos = crachasInativosNaoDevolvidos.Where(c => colaboradores.Select(col => col.Codigo).Contains(c.Papel.Codigo) &&
                !c.DataDesativacao.HasValue);

            return crachasInativosNaoDevolvidos.ToList().Count(x => ValidarData(x.DataRegistro));
        }

        public int ObterQuantidadePrestadores(string codigoEmpresa)
        {
            IQueryable<PrestadorServico> prestadores = Fabrica.Instancia.ObterRepositorio<IRepositorioPrestadorServico>(UnidadeTrabalho).ObterTodos();
            return prestadores.Where(p => p.Empresa.Codigo.Contains(codigoEmpresa) &&
                (p.PapelLogs.OrderByDescending(x => x.Codigo).Select(m => m.Status).FirstOrDefault() == StatusPapelLog.Ativo ||
                p.PapelLogs.OrderByDescending(x => x.Codigo).Select(m => m.Status).FirstOrDefault() == StatusPapelLog.Ferias ||
                p.PapelLogs.OrderByDescending(x => x.Codigo).Select(m => m.Status).FirstOrDefault() == StatusPapelLog.Afastamento)
                ).ToList().Count(x => ValidarData(x.DataRegistro));
        }

        public int ObterQuantidadeCrachasPrestadoresInativosNaoDevolvidos(string codigoEmpresa)
        {
            IQueryable<PrestadorServico> prestadores = Fabrica.Instancia.ObterRepositorio<IRepositorioPrestadorServico>(UnidadeTrabalho).ObterTodos();
            prestadores = prestadores.Where(c => c.Empresa.Codigo.Contains(codigoEmpresa) && c.PapelLogs.OrderByDescending(x => x.Codigo).Select(m => m.Status).FirstOrDefault() == StatusPapelLog.Inativacao);

            IQueryable<Cracha> crachasInativosNaoDevolvidos = Fabrica.Instancia.ObterRepositorio<IRepositorioCracha>(UnidadeTrabalho).ObterTodos();
            crachasInativosNaoDevolvidos = crachasInativosNaoDevolvidos.Where(c => prestadores.Select(col => col.Codigo).Contains(c.Papel.Codigo)
                && !c.DataDesativacao.HasValue);

            return crachasInativosNaoDevolvidos.ToList().Count(x => ValidarData(x.DataRegistro));
        }

        public int ObterQuantidadeVisitantes()
        {
            IQueryable<Visitante> visitantes = Fabrica.Instancia.ObterRepositorio<IRepositorioVisitante>(UnidadeTrabalho).ObterTodos();
            return visitantes.Where(v => v.Solicitante.Empresa.Codigo.Contains(Empresa.Codigo)).ToList().Count(x => ValidarData(x.DataRegistro));
        }

        public int ObterQuantidadeCrachasVisitantesInativosNaoDevolvidos(string codigoEmpresa)
        {
            IQueryable<Visitante> visitantes = Fabrica.Instancia.ObterRepositorio<IRepositorioVisitante>(UnidadeTrabalho).ObterTodos();
            visitantes = visitantes.Where(c => c.Solicitante.Empresa.Codigo.Contains(codigoEmpresa) && c.PapelLogs.OrderByDescending(x => x.Codigo).Select(m => m.Status).FirstOrDefault() == StatusPapelLog.Inativacao);

            IQueryable<Cracha> crachasInativosNaoDevolvidos = Fabrica.Instancia.ObterRepositorio<IRepositorioCracha>(UnidadeTrabalho).ObterTodos();
            crachasInativosNaoDevolvidos = crachasInativosNaoDevolvidos.Where(c => visitantes.Select(vis => vis.Codigo).Contains(c.Papel.Codigo)
                && !c.DataDesativacao.HasValue);

            return crachasInativosNaoDevolvidos.ToList().Count(x => ValidarData(x.DataRegistro));
        }

        public int ObterQuantidadeCredencialVeicularEmitidas(string codigoEmpresa)
        {
            IQueryable<CredencialVeiculo> credenciais = Fabrica.Instancia.ObterRepositorio<IRepositorioCredencialVeiculo>(UnidadeTrabalho).ObterTodos();
            return credenciais.Where(c => c.Empresa.Codigo == codigoEmpresa && c.Situacoes.Any(s => s.Status == StatusCredencial.Aprovada)).ToList().Count(x => ValidarData(x.DataRegistro));
        }

        public int ObterQuantidadeCredenciaisVencidas(string codigoEmpresa)
        {
            IQueryable<CredencialVeiculo> credenciais = Fabrica.Instancia.ObterRepositorio<IRepositorioCredencialVeiculo>(UnidadeTrabalho).ObterTodos();
            return credenciais.Where(c => c.Empresa.Codigo == codigoEmpresa && c.DataVencimento.Value < DateTime.Now.Date).ToList().Count(x => ValidarData(x.DataRegistro));
        }

        public int ObterQuantidadePessoasCriticas(string codigoEmpresa)
        {
            List<int> listaAux = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaCritica>(UnidadeTrabalho).ObterTodos().ToList().Where(x => ValidarData(x.DataRegistro)).Select(pc => pc.Codigo).ToList();
            IQueryable<PessoaCritica> pessoasCriticas = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaCritica>(UnidadeTrabalho).ObterTodos().Where(pc => listaAux.Contains(pc.Codigo));
            IQueryable<Papel> papeis = pessoasCriticas.SelectMany(pc => pc.PessoaFisica.Papeis).OrderByDescending(p => p.DataRegistro);

            IEnumerable<Colaborador> colaboradores = papeis.Where(p => p is Colaborador).Select(p => p as Colaborador).ToList().Where(c => c.Empresa.Codigo.Equals(codigoEmpresa));
            colaboradores = colaboradores.GroupBy(p => p.PessoaFisica.Codigo).Select(p => p.First());

            IEnumerable<PrestadorServico> prestadores = papeis.Where(p => p is PrestadorServico).Select(p => p as PrestadorServico).ToList().Where(p => p.Empresa != null && p.Empresa.Codigo.Equals(codigoEmpresa));
            prestadores = prestadores.GroupBy(p => p.PessoaFisica.Codigo).Select(p => p.First());

            return colaboradores.Count() + prestadores.Count(pr => !colaboradores.Any(c => c.PessoaFisica.Codigo.Equals(pr.PessoaFisica.Codigo)));
        }

        public int ObterQuantidadeOcorrenciaSegLeve(string codigoEmpresa)
        {
            Empresa empresa = Fabrica.Instancia.ObterRepositorio<IRepositorioEmpresa>(UnidadeTrabalho).ObterPorId(codigoEmpresa);
            IQueryable<OcorrenciaSeguranca> regs = Fabrica.Instancia.ObterRepositorio<IRepositorioOcorrenciaSeguranca>(UnidadeTrabalho).ObterTodos();
            return regs.ToList().Where(o => o.AgentesOcorrencia.Any(a => a.Ativo && a.TipoAgente == TipoAgenteOcorrencia.Ativo &&
                    o.Gravidade == GravidadeOcorrencia.Leve && (
                    (a.PessoaFisica.ObterPapel() is Colaborador && (a.PessoaFisica.ObterPapel() as Colaborador).Empresa.Nome == empresa.Nome) ||
                    (a.PessoaFisica.ObterPapel() is PrestadorServico && (a.PessoaFisica.ObterPapel() as PrestadorServico).Empresa.Nome == empresa.Nome) ||
                    (a.PessoaFisica.ObterPapel() is Visitante && (a.PessoaFisica.ObterPapel() as Visitante).Empresa == empresa.Nome)
                )
            )).ToList().Count(x => ValidarData(x.DataRegistro));
        }

        public int ObterQuantidadeOcorrenciaSegMedia(string codigoEmpresa)
        {
            Empresa empresa = Fabrica.Instancia.ObterRepositorio<IRepositorioEmpresa>(UnidadeTrabalho).ObterPorId(codigoEmpresa);
            IQueryable<OcorrenciaSeguranca> regs = Fabrica.Instancia.ObterRepositorio<IRepositorioOcorrenciaSeguranca>(UnidadeTrabalho).ObterTodos();
            return regs.ToList().Where(o => o.AgentesOcorrencia.Any(a => a.Ativo && a.TipoAgente == TipoAgenteOcorrencia.Ativo &&
                    o.Gravidade == GravidadeOcorrencia.Media && (
                    (a.PessoaFisica.ObterPapel() is Colaborador && (a.PessoaFisica.ObterPapel() as Colaborador).Empresa.Nome == empresa.Nome) ||
                    (a.PessoaFisica.ObterPapel() is PrestadorServico && (a.PessoaFisica.ObterPapel() as PrestadorServico).Empresa.Nome == empresa.Nome) ||
                    (a.PessoaFisica.ObterPapel() is Visitante && (a.PessoaFisica.ObterPapel() as Visitante).Empresa == empresa.Nome)
                )
            )).ToList().Count(x => ValidarData(x.DataRegistro));
        }

        public int ObterQuantidadeOcorrenciaSegGrave(string codigoEmpresa)
        {
            Empresa empresa = Fabrica.Instancia.ObterRepositorio<IRepositorioEmpresa>(UnidadeTrabalho).ObterPorId(codigoEmpresa);
            IQueryable<OcorrenciaSeguranca> regs = Fabrica.Instancia.ObterRepositorio<IRepositorioOcorrenciaSeguranca>(UnidadeTrabalho).ObterTodos();
            return regs.ToList().Where(o => o.AgentesOcorrencia.Any(a => a.Ativo && a.TipoAgente == TipoAgenteOcorrencia.Ativo &&
                    o.Gravidade == GravidadeOcorrencia.Grave && (
                    (a.PessoaFisica.ObterPapel() is Colaborador && (a.PessoaFisica.ObterPapel() as Colaborador).Empresa.Nome == empresa.Nome) ||
                    (a.PessoaFisica.ObterPapel() is PrestadorServico && (a.PessoaFisica.ObterPapel() as PrestadorServico).Empresa.Nome == empresa.Nome) ||
                    (a.PessoaFisica.ObterPapel() is Visitante && (a.PessoaFisica.ObterPapel() as Visitante).Empresa == empresa.Nome)
                )
            )).ToList().Count(x => ValidarData(x.DataRegistro));
        }

        public int ObterQuantidadeOcorrenciaSegGravissima(string codigoEmpresa)
        {
            Empresa empresa = Fabrica.Instancia.ObterRepositorio<IRepositorioEmpresa>(UnidadeTrabalho).ObterPorId(codigoEmpresa);
            IQueryable<OcorrenciaSeguranca> regs = Fabrica.Instancia.ObterRepositorio<IRepositorioOcorrenciaSeguranca>(UnidadeTrabalho).ObterTodos();
            return regs.ToList().Where(o => o.AgentesOcorrencia.Any(a => a.Ativo && a.TipoAgente == TipoAgenteOcorrencia.Ativo &&
                    o.Gravidade == GravidadeOcorrencia.Gravissima && (
                    (a.PessoaFisica.ObterPapel() is Colaborador && (a.PessoaFisica.ObterPapel() as Colaborador).Empresa.Nome == empresa.Nome) ||
                    (a.PessoaFisica.ObterPapel() is PrestadorServico && (a.PessoaFisica.ObterPapel() as PrestadorServico).Empresa.Nome == empresa.Nome) ||
                    (a.PessoaFisica.ObterPapel() is Visitante && (a.PessoaFisica.ObterPapel() as Visitante).Empresa == empresa.Nome)
                )
            )).ToList().Count(x => ValidarData(x.DataRegistro));
        }

        public int ObterQuantidadeOcorrenciaTransLeve(string codigoEmpresa)
        {
            Empresa empresa = Fabrica.Instancia.ObterRepositorio<IRepositorioEmpresa>(UnidadeTrabalho).ObterPorId(codigoEmpresa);
            IQueryable<OcorrenciaTransito> regs = Fabrica.Instancia.ObterRepositorio<IRepositorioOcorrenciaTransito>(UnidadeTrabalho).ObterTodos();
            return regs.ToList().Where(ot => ot.Infracao.Grau == GrauInfracao.Leve && (
                    (ot.Papel is Colaborador && (ot.Papel as Colaborador).Empresa.Nome == empresa.Nome) ||
                    (ot.Papel is PrestadorServico && (ot.Papel as PrestadorServico).Empresa.Nome == empresa.Nome) ||
                    (ot.Papel is Visitante && (ot.Papel as Visitante).Empresa == empresa.Nome)
                )
            ).ToList().Count(x => ValidarData(x.DataRegistro));
        }

        public int ObterQuantidadeOcorrenciaTransMedia(string codigoEmpresa)
        {
            Empresa empresa = Fabrica.Instancia.ObterRepositorio<IRepositorioEmpresa>(UnidadeTrabalho).ObterPorId(codigoEmpresa);
            IQueryable<OcorrenciaTransito> regs = Fabrica.Instancia.ObterRepositorio<IRepositorioOcorrenciaTransito>(UnidadeTrabalho).ObterTodos();
            return regs.ToList().Where(ot => ot.Infracao.Grau == GrauInfracao.Media && (
                    (ot.Papel is Colaborador && (ot.Papel as Colaborador).Empresa.Nome == empresa.Nome) ||
                    (ot.Papel is PrestadorServico && (ot.Papel as PrestadorServico).Empresa.Nome == empresa.Nome) ||
                    (ot.Papel is Visitante && (ot.Papel as Visitante).Empresa == empresa.Nome)
                )
            ).ToList().Count(x => ValidarData(x.DataRegistro));
        }

        public int ObterQuantidadeOcorrenciaTransGrave(string codigoEmpresa)
        {
            Empresa empresa = Fabrica.Instancia.ObterRepositorio<IRepositorioEmpresa>(UnidadeTrabalho).ObterPorId(codigoEmpresa);
            IQueryable<OcorrenciaTransito> regs = Fabrica.Instancia.ObterRepositorio<IRepositorioOcorrenciaTransito>(UnidadeTrabalho).ObterTodos();
            return regs.ToList().Where(ot => ot.Infracao.Grau == GrauInfracao.Grave && (
                    (ot.Papel is Colaborador && (ot.Papel as Colaborador).Empresa.Nome == empresa.Nome) ||
                    (ot.Papel is PrestadorServico && (ot.Papel as PrestadorServico).Empresa.Nome == empresa.Nome) ||
                    (ot.Papel is Visitante && (ot.Papel as Visitante).Empresa == empresa.Nome)
                )
            ).ToList().Count(x => ValidarData(x.DataRegistro));
        }

        public int ObterQuantidadeOcorrenciaTransGravissima(string codigoEmpresa)
        {
            Empresa empresa = Fabrica.Instancia.ObterRepositorio<IRepositorioEmpresa>(UnidadeTrabalho).ObterPorId(codigoEmpresa);
            IQueryable<OcorrenciaTransito> regs = Fabrica.Instancia.ObterRepositorio<IRepositorioOcorrenciaTransito>(UnidadeTrabalho).ObterTodos();
            return regs.ToList().Where(ot => ot.Infracao.Grau == GrauInfracao.Gravissima && (
                    (ot.Papel is Colaborador && (ot.Papel as Colaborador).Empresa.Nome == empresa.Nome) ||
                    (ot.Papel is PrestadorServico && (ot.Papel as PrestadorServico).Empresa.Nome == empresa.Nome) ||
                    (ot.Papel is Visitante && (ot.Papel as Visitante).Empresa == empresa.Nome)
                )
            ).ToList().Count(x => ValidarData(x.DataRegistro));
        }
    }
}