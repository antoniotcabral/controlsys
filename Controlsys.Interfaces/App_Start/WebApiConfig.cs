﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Controlsys.Interfaces
{
    /// <summary>
    /// Representa um(a) WebApiConfig.
    /// </summary>
    public static class WebApiConfig
    {
        /// <summary>
        /// Register.
        /// </summary>
        ///
        /// <param name="config">
        /// O(a) configuration.
        /// </param>
        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
