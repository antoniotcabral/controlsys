﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ninject;
using Ninject.Parameters;

namespace Globalsys
{
    /// <summary>
    /// Representa um(a) NinjectDependencyResolver.
    /// </summary>
    public class NinjectDependencyResolver : IDependencyResolver
    {
        /// <summary>
        /// O(a) kernel.
        /// </summary>
        private readonly IKernel kernel;

        /// <summary>
        /// Construtor para Globalsys.NinjectDependencyResolver.
        /// </summary>
        ///
        /// <param name="kernel">
        /// O(a) kernel.
        /// </param>
        public NinjectDependencyResolver(IKernel kernel)
        {
            this.kernel = kernel;
        }

        /// <summary>
        /// Get service.
        /// </summary>
        ///
        /// <param name="serviceType">
        /// Type of the service.
        /// </param>
        ///
        /// <returns>
        /// Um(a) object.
        /// </returns>
        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }

        /// <summary>
        /// Get services.
        /// </summary>
        ///
        /// <param name="serviceType">
        /// Type of the service.
        /// </param>
        ///
        /// <returns>
        /// Um(a) IEnumerable&lt;object&gt;
        /// </returns>
        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }
    }
}