﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using Controlsys.Infra;
using Ninject;

namespace Globalsys
{
    /// <summary>
    /// atributos para transaction.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class TransactionAttribute : ActionFilterAttribute, IActionFilter
    {
        /// <summary>
        /// On action executing.
        /// </summary>
        ///
        /// <param name="filterContext">
        /// Context for the filter.
        /// </param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            IUnidadeTrabalho unidadeTrabalho = ((IControllerBase)filterContext.Controller).UnidadeTrabalho;

            unidadeTrabalho.BeginTransaction();
        }

        /// <summary>
        /// On action executed.
        /// </summary>
        ///
        /// <exception cref="Exception">
        /// Lançada quando uma condição de erro exception ocorre.
        /// </exception>
        ///
        /// <param name="filterContext">
        /// Context for the filter.
        /// </param>
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            IUnidadeTrabalho unidadeTrabalho = ((IControllerBase)filterContext.Controller).UnidadeTrabalho;

            try
            {
                base.OnActionExecuted(filterContext);

                if (filterContext.Exception != null)
                    unidadeTrabalho.Rollback();
                else unidadeTrabalho.Commit();

            }
            catch (Exception ex) { throw ex; }
            finally
            {
                unidadeTrabalho.Dispose();
            }
        }
    }
}