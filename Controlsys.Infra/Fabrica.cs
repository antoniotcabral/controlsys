﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Persistencia;
using Ninject;
using Ninject.Parameters;

namespace Controlsys.Infra
{
    /// <summary>
    /// Representa um(a) Fabrica.
    /// </summary>
    public class Fabrica
    {
        /// <summary>
        /// Gets/Sets valor para fabrica.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) fabrica.
        /// </value>
        private static Fabrica fabrica { get; set; }

        /// <summary>
        /// Singleton.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Instancia.
        /// </value>
        public static Fabrica Instancia
        {
            get
            {
                if (fabrica == null)
                    fabrica = new Fabrica();

                return fabrica;
            }
        }

        /// <summary>
        /// Gets/Sets valor para Kernel.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Kernel.
        /// </value>
        public virtual StandardKernel Kernel { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Infra.Fabrica.
        /// </summary>
        public Fabrica()
        {
            ModuloControlsys modulo = new ModuloControlsys();
            Kernel = new StandardKernel(modulo);
        }

        /// <summary>
        /// Obter.
        /// </summary>
        ///
        /// <typeparam name="T">
        /// Generic type parameter.
        /// </typeparam>
        ///
        /// <returns>
        /// Um(a) T.
        /// </returns>
        public T Obter<T>()
        {
            return Kernel.Get<T>();
        }

        /// <summary>
        /// Obter repositorio.
        /// </summary>
        ///
        /// <typeparam name="T">
        /// Generic type parameter.
        /// </typeparam>
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        ///
        /// <returns>
        /// Um(a) T.
        /// </returns>
        public T ObterRepositorio<T>(object unidadeTrabalho)
        {
            return Kernel.Get<T>(new ConstructorArgument("unidadeTrabalho", unidadeTrabalho));
        }
    }
}
