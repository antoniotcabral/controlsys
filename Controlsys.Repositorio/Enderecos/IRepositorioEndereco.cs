﻿using Controlsys.Dominio.Enderecos;
using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Repositorios.Enderecos
{
    /// <summary>
    /// Interface IRepositorioEndereco.
    /// </summary>
    public interface IRepositorioEndereco : IRepositorio<Endereco>
    {
        /// <summary>
        /// Pesquisar.
        /// </summary>
        ///
        /// <param name="cep">
        /// O(a) cep.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Logradouro.
        /// </returns>
        Logradouro Pesquisar(string cep);

        /// <summary>
        /// Obter paises.
        /// </summary>
        ///
        /// <param name="descricao">
        /// O(a) descricao.
        /// </param>
        ///
        /// <returns>
        /// Um(a) IQueryable&lt;Pais&gt;
        /// </returns>
        IQueryable<Pais> ObterPaises(string descricao);

        /// <summary>
        /// Obter paise por nome.
        /// </summary>
        ///
        /// <param name="descricao">
        /// O(a) descricao.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Pais.
        /// </returns>
        Pais ObterPaisePorNome(string descricao);

        /// <summary>
        /// Obter estados.
        /// </summary>
        ///
        /// <param name="descricao">
        /// O(a) descricao.
        /// </param>
        /// <param name="codigoPais">
        /// O(a) codigo pais.
        /// </param>
        ///
        /// <returns>
        /// Um(a) IQueryable&lt;Estado&gt;
        /// </returns>
        IQueryable<Estado> ObterEstados(string descricao, int? codigoPais);

        /// <summary>
        /// Obter estado por nome.
        /// </summary>
        ///
        /// <param name="descricao">
        /// O(a) descricao.
        /// </param>
        /// <param name="pais">
        /// O(a) pais.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Estado.
        /// </returns>
        Estado ObterEstadoPorNome(string descricao, string pais);
        /// <summary>
        /// obter estado por sigla
        /// </summary>
        /// <param name="sigla"></param>
        /// <returns></returns>
        Estado ObterEstadosSigla(string sigla);
        /// <summary>
        /// Obter cidades.
        /// </summary>
        ///
        /// <param name="descricao">
        /// O(a) descricao.
        /// </param>
        /// <param name="codigoEstado">
        /// O(a) codigo estado.
        /// </param>        
        ///  <param name="apenasAtivos">
        /// O(a) status ativo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) IQueryable&lt;Cidade&gt;
        /// </returns>
        IQueryable<Cidade> ObterCidades(string descricao, int? codigoEstado, bool apenasAtivos = false);

        /// <summary>
        /// Obter cidade por nome.
        /// </summary>
        ///
        /// <param name="descricao">
        /// O(a) descricao.
        /// </param>
        /// <param name="estado">
        /// O(a) estado.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Cidade.
        /// </returns>
        Cidade ObterCidadePorNome(string descricao, string estado);

        /// <summary>
        /// Obter bairros.
        /// </summary>
        ///
        /// <param name="descricao">
        /// O(a) descricao.
        /// </param>
        /// <param name="codigoBairro">
        /// O(a) codigo bairro.
        /// </param>
        ///
        /// <returns>
        /// Um(a) IQueryable&lt;Bairro&gt;
        /// </returns>
        IQueryable<Bairro> ObterBairros(string descricao, int? codigoBairro);

        /// <summary>
        /// Obter bairro por nome.
        /// </summary>
        ///
        /// <param name="descricao">
        /// O(a) descricao.
        /// </param>
        /// <param name="cidade">
        /// O(a) cidade.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Bairro.
        /// </returns>
        Bairro ObterBairroPorNome(string descricao, string cidade);

        /// <summary>
        /// Obter logradouros.
        /// </summary>
        ///
        /// <param name="descricao">
        /// O(a) descricao.
        /// </param>
        /// <param name="codigoBairro">
        /// O(a) codigo bairro.
        /// </param>
        ///
        /// <returns>
        /// Um(a) IQueryable&lt;Logradouro&gt;
        /// </returns>
        IQueryable<Logradouro> ObterLogradouros(string descricao, int? codigoBairro);

        /// <summary>
        /// Obter logradouro por nome.
        /// </summary>
        ///
        /// <param name="descricao">
        /// O(a) descricao.
        /// </param>
        /// <param name="bairro">
        /// O(a) bairro.
        /// </param>
        /// <param name="cidade">
        /// O(a) cidade.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Logradouro.
        /// </returns>
        Logradouro ObterLogradouroPorNome(string descricao, string bairro, string cidade);

        //IQueryable<TipoLogradouro> ObterTiposLogradouro(string descricao);

        /// <summary>
        /// Existe pais.
        /// </summary>
        ///
        /// <param name="pais">
        /// O(a) pais.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        bool ExistePais(Pais pais);

        /// <summary>
        /// Existe estado.
        /// </summary>
        ///
        /// <param name="estado">
        /// O(a) estado.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        bool ExisteEstado(Estado estado);

        /// <summary>
        /// Existe cidade.
        /// </summary>
        ///
        /// <param name="cidade">
        /// O(a) cidade.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        bool ExisteCidade(Cidade cidade);

        /// <summary>
        /// Existe bairro.
        /// </summary>
        ///
        /// <param name="bairro">
        /// O(a) bairro.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        bool ExisteBairro(Bairro bairro);

        /// <summary>
        /// Existe logradouro.
        /// </summary>
        ///
        /// <param name="logradouro">
        /// O(a) logradouro.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        bool ExisteLogradouro(Logradouro logradouro);

        /// <summary>
        /// Verificar endereco.
        /// </summary>
        ///
        /// <param name="endereco">
        /// O(a) endereco.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Endereco.
        /// </returns>
        Endereco VerificarEndereco(Endereco endereco);


    }
}
