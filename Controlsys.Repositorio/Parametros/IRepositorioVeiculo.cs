﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.CredenciamentoVeiculos;
using Globalsys;
using Globalsys.Validacao;

namespace Controlsys.Repositorio.Parametros
{
    public interface IRepositorioVeiculo : IRepositorio<Veiculo>
    {
        void Validar(Veiculo entidade, EstadoObjeto estadoObjeto);
    }
}
