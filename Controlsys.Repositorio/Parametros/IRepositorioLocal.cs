﻿using Controlsys.Dominio.Parametros;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Repositorio.Parametros
{
    /// <summary>
    /// Interface ILocal
    /// </summary>
    public interface IRepositorioLocal : IRepositorio<Local>
    {
        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        /// 
        /// <param name="local">
        /// O local.
        /// </param>
        /// 
        /// <returns>
        /// Um bool
        /// </returns>
        bool Existe(Local local);

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        void Validar(Local entidade, EstadoObjeto estadoObjeto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="login"></param>
        /// <param name="somenteAtivos"></param>
        /// <returns></returns>
        Local ObterPorNomeSigla(string nomeSigla, bool somenteAtivos = false);
    }
}
