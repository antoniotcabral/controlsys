﻿using Controlsys.Dominio.Parametros;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Repositorios.Parametros
{
    /// <summary>
    /// Interface IRepositorioHoraTurno.
    /// </summary>
    public interface IRepositorioHoraTurno : IRepositorio<HoraTurno>
    {

    }
}
