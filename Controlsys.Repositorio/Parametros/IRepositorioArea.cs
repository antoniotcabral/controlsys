﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Empresas;
using Globalsys;
using Globalsys.Validacao;

namespace Controlsys.Repositorios.Parametros
{
    /// <summary>
    /// Interface IRepositorioArea.
    /// </summary>
    public interface IRepositorioArea : IRepositorio<Area>
    {
        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="area">
        /// O(a) area.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        bool Existe(Area area);

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        void Validar(Area entidade, EstadoObjeto estadoObjeto);
    }

}
