﻿using Controlsys.Dominio.CredenciamentoVeiculos;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Repositorio.Parametros
{
    public interface IRepositorioCredencialVeiculo : IRepositorio<CredencialVeiculo>
    {
        SituacaoCredencialVeiculo SalvarStatus(SituacaoCredencialVeiculo entidade);

        //void ExisteVeiculo(CredencialVeiculo entidade);
        
        void Validar(CredencialVeiculo entidade, EstadoObjeto estadoObjeto);
    }
}
