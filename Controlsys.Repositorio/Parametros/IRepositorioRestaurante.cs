﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Parametros;
using Globalsys;
using Globalsys.Validacao;

namespace Controlsys.Repositorio.Parametros
{
    public interface IRepositorioRestaurante : IRepositorio<Restaurante>
    {
        bool Existe(Restaurante restaurante);

        void Validar(Restaurante entidade, EstadoObjeto estadoObjeto);

        void SalvarTipoAlimentacaoRestaurante(TipoAlimentacaoRestaurante tipoAlimentacaoRestaurante);

        TipoAlimentacaoRestaurante TAlimentacaoRestaurante(TipoAlimentacaoRestaurante tAlimRestaurante);
    }
}
