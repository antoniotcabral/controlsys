﻿using Controlsys.Dominio.Parametros;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Repositorios.Parametros
{
    /// <summary>
    /// Interface IRepositorioTurno.
    /// </summary>
    public interface IRepositorioTurno : IRepositorio<Turno>
    {
        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="turno">
        /// O(a) turno.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        bool Existe(Turno turno);

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        void Validar(Turno entidade, EstadoObjeto estadoObjeto);

        /// <summary>
        /// Salvar hora turno.
        /// </summary>
        ///
        /// <param name="horaTurno">
        /// O(a) hora turno.
        /// </param>
        void SalvarHoraTurno(HoraTurno horaTurno);

        /// <summary>
        /// Salvar feriado turno.
        /// </summary>
        ///
        /// <param name="feriado">
        /// O(a) feriado.
        /// </param>
        void SalvarFeriadoTurno(FeriadoTurno feriado);
    }
}
