﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Parametros;
using Globalsys;

namespace Controlsys.Repositorios.Parametros
{
    /// <summary>
    /// Interface IRepositorioEscalaTrabalho.
    /// </summary>
    public interface IRepositorioEscalaTrabalho : IRepositorio<EscalaTrabalho>
    {
        void GerarEscala(GrupoTrabalho grupoTrabalho);
    }
}
