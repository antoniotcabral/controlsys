﻿using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Repositorios.Parametros
{
    /// <summary>
    /// Interface IRepositorioTreinamento.
    /// </summary>
    public interface IRepositorioTreinamento : IRepositorio<Treinamento> 
    {
        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="treinamento">
        /// O(a) treinamento.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        bool Existe(Treinamento treinamento);

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        void Validar(Treinamento entidade, EstadoObjeto estadoObjeto);

        /// <summary>
        /// Salvar treinamento pessoa.
        /// </summary>
        ///
        /// <param name="papel">
        /// O(a) papel.
        /// </param>
        /// <param name="item">
        /// O(a) item.
        /// </param>
        void SalvarTreinamentoPessoa(Papel papel, TreinamentoPessoa item);
    }
}
