﻿using Controlsys.Dominio.Parametros;
using Globalsys;
using Globalsys.Validacao;

namespace Controlsys.Repositorio.Parametros
{
    /// <summary>
    /// Interface IRepositorioDocumentoMidia.
    /// </summary>
    public interface IRepositorioDocumentoMidia : IRepositorio<DocumentoMidia>
    {
        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="documentoMidia">
        /// O(a) area.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        bool Existe(DocumentoMidia documentoMidia);

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="documentoMidia">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        void Validar(DocumentoMidia documentoMidia, EstadoObjeto estadoObjeto);

        DocumentoMidia SalvarDocMidia(Documento doc, Midia midia);
    }
}
