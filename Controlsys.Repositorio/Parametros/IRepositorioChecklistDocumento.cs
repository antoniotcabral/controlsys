﻿using Controlsys.Dominio.Parametros;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Repositorios.Parametros
{
    /// <summary>
    /// Interface IRepositorioChecklistDocumento.
    /// </summary>
    public interface IRepositorioChecklistDocumento : IRepositorio<ChecklistDocumento>
    {
        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="checklistDoc">
        /// O(a) checklist document.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        bool Existe(ChecklistDocumento checklistDoc);

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        void Validar(ChecklistDocumento entidade, EstadoObjeto estadoObjeto);

        /// <summary>
        /// Salvar check list tipo document.
        /// </summary>
        ///
        /// <param name="checkListTipoDoc">
        /// O(a) check list tipo document.
        /// </param>
        void SalvarCheckListTipoDoc(ChecklistTipoDocumento checkListTipoDoc);

        /// <summary>
        /// Obter todos por tipo cadastro.
        /// </summary>
        ///
        /// <param name="tipoCadastro">
        /// O(a) tipo cadastro.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ChecklistDocumento.
        /// </returns>
        ChecklistDocumento ObterTodosPorTipoCadastro(TipoCadastro tipoCadastro);
    }
}
