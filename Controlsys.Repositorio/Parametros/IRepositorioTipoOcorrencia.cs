﻿using Controlsys.Dominio.Parametros;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Repositorio.Parametros
{
    /// <summary>
    /// Interface IRepositorioTipoOcorrencia
    /// </summary>
    public interface IRepositorioTipoOcorrencia : IRepositorio<TipoOcorrencia>
    {
        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="tipoOcorrencia">
        /// O(a) area.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        bool Existe(TipoOcorrencia tipoOcorrencia);

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        void Validar(TipoOcorrencia entidade, EstadoObjeto estadoObjeto);

        TipoOcorrencia ObterPorNome(string nome, bool somenteAtivos = false);
    }
}
