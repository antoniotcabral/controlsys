﻿using Controlsys.Dominio.Pessoas;
using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Repositorio.Parametros
{
    /// <summary>
    /// Interface IRepositorioCracha.
    /// </summary>
    public interface IRepositorioCracha : IRepositorio<Cracha>
    {
        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="cracha">
        /// O(a) cracha.
        /// </param>
        void Validar(Cracha cracha);

        /// <summary>
        /// Obter cracha.
        /// </summary>
        ///
        /// <param name="p">
        /// The int to process.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Cracha.
        /// </returns>
        Cracha ObterCracha(int p);

    }
}
