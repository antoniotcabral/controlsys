﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Empresas;
using Globalsys;
using Globalsys.Validacao;
using Controlsys.Dominio.Parametros;

namespace Controlsys.Repositorio.Parametros
{
    /// <summary>
    /// Interface IRepositorioInfracao.
    /// </summary>
    public interface IRepositorioInfracao : IRepositorio<Infracao>
    {
        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="infracao">
        /// O(a) infracao.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        bool Existe(Infracao infracao);

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        void Validar(Infracao entidade, EstadoObjeto estadoObjeto);
    }
}
