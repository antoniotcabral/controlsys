﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Parametros;
using Globalsys;
using Globalsys.Validacao;

namespace Controlsys.Repositorio.Parametros
{
    public interface IRepositorioSindicato : IRepositorio<Sindicato>
    {
        bool Existe(Sindicato sindicato);
        void Validar(Sindicato entidade, EstadoObjeto estadoObjeto);       
    }
}