﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Parametros;
using Globalsys;
using Globalsys.Validacao;

namespace Controlsys.Repositorio.Parametros
{
    public interface IRepositorioAtendimentoReceptivo : IRepositorio<AtendimentoReceptivo>
    {

        bool Existe(AtendimentoReceptivo entidade);

        void Validar(AtendimentoReceptivo entidade, EstadoObjeto estadoObjeto);

    }
}
