﻿using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Repositorios.Parametros
{
    /// <summary>
    /// Interface IRepositorioGrupoTrabalho.
    /// </summary>
    public interface IRepositorioGrupoTrabalho : IRepositorio<GrupoTrabalho>
    {
        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="checklistDoc">
        /// O(a) checklist document.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        bool Existe(GrupoTrabalho checklistDoc);

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        void Validar(GrupoTrabalho entidade, EstadoObjeto estadoObjeto);

        /// <summary>
        /// Salvar grupo trabalho turno.
        /// </summary>
        ///
        /// <param name="checkListTipoDoc">
        /// O(a) check list tipo document.
        /// </param>
        void SalvarGrupoTrabalhoTurno(GrupoTrabalhoTurno checkListTipoDoc);

        /// <summary>
        /// Salvar grupo trabalho colaborador.
        /// </summary>
        ///
        /// <param name="grupoTrabalhoColab">
        /// O(a) grupo trabalho colab.
        /// </param>
        void SalvarGrupoTrabalhoColaborador(GrupoTrabalhoColab grupoTrabalhoColab);
    }
}
