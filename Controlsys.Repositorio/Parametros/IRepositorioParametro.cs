﻿using Controlsys.Dominio.Parametros;
using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Repositorios.Parametros
{
    /// <summary>
    /// Interface IRepositorioParametro.
    /// </summary>
    public interface IRepositorioParametro : IRepositorio<Parametro>
    {
        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        void Validar(Parametro entidade);

        /// <summary>
        /// Recuperar parametro.
        /// </summary>
        ///
        /// <param name="eNum">
        /// Number of.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Parametro.
        /// </returns>
        Parametro RecuperarParametro(ParametroSistema eNum);
    }
}
