﻿using Controlsys.Dominio.Enderecos;
using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Repositorio.Parametros
{
    /// <summary>
    /// Interface IRepositorioCidade.
    /// </summary>
    public interface IRepositorioCidade : IRepositorio<Cidade>
    {
        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="cidade">
        /// O(a) cidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        void Validar(Cidade cidade, Globalsys.Validacao.EstadoObjeto estadoObjeto);
    }
}
