﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Parametros;
using Globalsys;
using Globalsys.Validacao;

namespace Controlsys.Repositorio.Parametros
{
    public interface IRepositorioTipoAlimentacaoRestaurante : IRepositorio<TipoAlimentacaoRestaurante>
    {
        void Validar(TipoAlimentacaoRestaurante entidade, TipoAlimentacaoRestaurante[] alterados, EstadoObjeto estadoObjeto);

        //void ValidarAlteracao(TipoAlimentacaoRestaurante entidade, TipoAlimentacaoRestaurante[] todosGrid, EstadoObjeto estadoObjeto);
    }
}
