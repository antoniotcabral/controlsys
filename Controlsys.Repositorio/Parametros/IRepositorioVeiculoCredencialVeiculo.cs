﻿using Controlsys.Dominio.CredenciamentoVeiculos;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Repositorio.Parametros
{
    public interface IRepositorioVeiculoCredencialVeiculo : IRepositorio<VeiculoCredencialVeiculo>
    {
        void Validar(VeiculoCredencialVeiculo entidade, EstadoObjeto estadoObjeto);
    }
}
