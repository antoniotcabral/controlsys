﻿using Controlsys.Dominio.Parametros;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Repositorios.Parametros
{
    /// <summary>
    /// Interface IRepositorioTipoDocumento.
    /// </summary>
    public interface IRepositorioTipoDocumento : IRepositorio<TipoDocumento>
    {
        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="tipoDoc">
        /// O(a) tipo document.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        bool Existe(TipoDocumento tipoDoc);

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        void Validar(TipoDocumento entidade, EstadoObjeto estadoObjeto);
    }
}
