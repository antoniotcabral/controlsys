﻿using Controlsys.Dominio.Parametros;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Repositorio.Parametros
{
    public interface IRepositorioTipoAlimentacao: IRepositorio<TipoAlimentacao>
    {
        bool Existe(TipoAlimentacao tipoA);

        void Validar(TipoAlimentacao entidade, EstadoObjeto estadoObjeto);
    }
}
