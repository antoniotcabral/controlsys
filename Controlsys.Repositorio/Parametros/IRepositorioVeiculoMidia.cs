﻿using Controlsys.Dominio.CredenciamentoVeiculos;
using Controlsys.Dominio.Parametros;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Repositorio.Parametros
{
    public interface IRepositorioVeiculoMidia : IRepositorio<VeiculoMidia>
    {

        bool Existe(VeiculoMidia entidade);

        void Validar(VeiculoMidia entidade, EstadoObjeto estadoObjeto);
    }
}
