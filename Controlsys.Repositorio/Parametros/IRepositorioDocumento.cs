﻿using Controlsys.Dominio.AgendamentosPessoa;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Repositorios.Parametros
{
    /// <summary>
    /// Interface IRepositorioDocumento.
    /// </summary>
    public interface IRepositorioDocumento : IRepositorio<Documento>
    {
        //DocumentoPessoa SalvarDocPessoa(Pessoa pessoa, Documento item);

        /// <summary>
        /// Salvar document papel.
        /// </summary>
        ///
        /// <param name="papel">
        /// O(a) papel.
        /// </param>
        /// <param name="item">
        /// O(a) item.
        /// </param>
        ///
        /// <returns>
        /// Um(a) DocumentoPapel.
        /// </returns>
        DocumentoPapel SalvarDocPapel(Papel papel, Documento item, PapelAgendamento papelAgendamento = null);

        /// <summary>
        /// Salvar document empresa.
        /// </summary>
        ///
        /// <param name="empresa">
        /// O(a) empresa.
        /// </param>
        /// <param name="item">
        /// O(a) item.
        /// </param>
        ///
        /// <returns>
        /// Um(a) DocumentoEmpresa.
        /// </returns>
        DocumentoEmpresa SalvarDocEmpresa(Dominio.Empresas.Empresa empresa, Documento item);
    }
}
