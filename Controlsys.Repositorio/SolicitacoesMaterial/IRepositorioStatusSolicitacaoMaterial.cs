﻿using Controlsys.Dominio.SolicitacoesMaterial;
using Globalsys;
using Globalsys.Validacao;

namespace Controlsys.Repositorio.SolicitacoesMaterial
{
    public interface IRepositorioStatusSolicitacaoMaterial : IRepositorio<StatusSolicitacaoMaterial>
    {
        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="solicitacaoMaterial">
        /// O(a) Solicitacao Material.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        void Validar(StatusSolicitacaoMaterial statusSolicitacaoMaterial, EstadoObjeto estadoObjeto);
    }
}
