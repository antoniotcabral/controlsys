﻿using Controlsys.Dominio.SolicitacoesMaterial;
using Globalsys;
using Globalsys.Validacao;

namespace Controlsys.Repositorio.SolicitacoesMaterial
{
    public interface IRepositorioSolicitacaoMaterialMidia : IRepositorio<SolicitacaoMaterialMidia>
    {
        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="solicitacaoMaterialMidia">
        /// O(a) Solicitacao Material.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        void Validar(SolicitacaoMaterialMidia solicitacaoMaterialMidia, EstadoObjeto estadoObjeto);
    }
}
