﻿using Controlsys.Dominio.SolicitacoesMaterial;
using Globalsys;
using Globalsys.Validacao;

namespace Controlsys.Repositorio.SolicitacoesMaterial
{
    public interface IRepositorioItemSolicitacaoMaterial : IRepositorio<ItemSolicitacaoMaterial>
    {
        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="solicitacaoMaterial">
        /// O(a) Solicitacao Material.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        void Validar(ItemSolicitacaoMaterial itemSolicitacaoMaterial, EstadoObjeto estadoObjeto);
    }
}
