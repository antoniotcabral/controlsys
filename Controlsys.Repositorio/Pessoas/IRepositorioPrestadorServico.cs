﻿using Controlsys.Dominio.Pessoas;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Repositorios.Pessoas
{
    /// <summary>
    /// Interface IRepositorioPrestadorServico.
    /// </summary>
    public interface IRepositorioPrestadorServico  : IRepositorio<PrestadorServico>
    {
        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        /// <param name="prestador">
        /// O(a) prestador.
        /// </param>
        void Validar(EstadoObjeto estadoObjeto, PrestadorServico prestador);

        //void SalvarAso(ASO aso, PrestadorServico colaborador);

        //void RemoverAso(ASO aso);
    }
}
