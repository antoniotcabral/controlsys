﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Pessoas;
using Globalsys;

namespace Controlsys.Repositorios.Pessoas
{
    /// <summary>
    /// Interface IRepositorioPapel.
    /// </summary>
    public interface IRepositorioPapel : IRepositorio<Papel>
    {
        /// <summary>
        /// Salvar papel log.
        /// </summary>
        ///
        /// <param name="papelLog">
        /// O(a) papel log.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Papel.
        /// </returns>
        Papel SalvarPapelLog(PapelLog papelLog);

        /// <summary>
        /// Validar papel log.
        /// </summary>
        ///
        /// <param name="papelLogOld">
        /// O(a) papel log old.
        /// </param>
        /// <param name="papelLog">
        /// O(a) papel log.
        /// </param>
        void ValidarPapelLog(PapelLog papelLogOld, PapelLog papelLog);

        /// <summary>
        /// Obter lista papel.
        /// </summary>
        ///
        /// <param name="codigos">
        /// O(a) codigos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;Papel&gt;
        /// </returns>
        List<Papel> ObterListaPapel(int[] codigos);

        /// <summary>
        /// Salvar aso.
        /// </summary>
        ///
        /// <param name="aso">
        /// O(a) aso.
        /// </param>
        /// <param name="papel">
        /// O(a) papel.
        /// </param>
        void SalvarAso(ASO aso, Papel papel);

        /// <summary>
        /// Remover aso.
        /// </summary>
        ///
        /// <param name="aso">
        /// O(a) aso.
        /// </param>
        void RemoverAso(ASO aso);

        /// <summary>
        /// Salvar excecao acesso.
        /// </summary>
        ///
        /// <param name="papel">
        /// O(a) papel.
        /// </param>
        /// <param name="excecao">
        /// O(a) excecao.
        /// </param>
        void SalvarExcecaoAcesso(Papel papel, ExcecaoAcesso excecao);

        /// <summary>
        /// Obter por identifier excecao.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ExcecaoAcesso.
        /// </returns>
        ExcecaoAcesso ObterPorIdExcecao(int codigo);

        /// <summary>
        /// Dar baixa definitiva.
        /// </summary>
        ///
        /// <param name="papelLog">
        /// O(a) papel log.
        /// </param>
        void DarBaixaDefinitiva(PapelLog papelLog);

    }
}
