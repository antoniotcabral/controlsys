﻿using Controlsys.Dominio.Pessoas;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Repositorios.Pessoas
{
    /// <summary>
    /// Interface IRepositorioPessoa.
    /// </summary>
    public interface IRepositorioPessoa : IRepositorio<Pessoa>
    {
        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="pessoa">
        /// O(a) pessoa.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        bool Existe(Pessoa pessoa);

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        void Validar(Pessoa entidade, EstadoObjeto estadoObjeto);
    }
}
