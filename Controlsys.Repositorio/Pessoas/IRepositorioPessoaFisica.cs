﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Pessoas;
using Globalsys;
using Globalsys.Validacao;

namespace Controlsys.Repositorios.Pessoas
{
    /// <summary>
    /// Interface IRepositorioPessoaFisica.
    /// </summary>
    public interface IRepositorioPessoaFisica : IRepositorio<PessoaFisica>
    {
        /// <summary>
        /// Obter por cpf passaporte.
        /// </summary>
        ///
        /// <param name="verificarPapelAtivo">
        /// true to verificar papel ativo.
        /// </param>
        /// <param name="cpf">
        /// O(a) cpf.
        /// </param>
        /// <param name="passaporte">
        /// O(a) passaporte.
        /// </param>
        /// <param name="usuario">
        /// true to usuario.
        /// </param>
        ///
        /// <returns>
        /// Um(a) PessoaFisica.
        /// </returns>
        PessoaFisica ObterPorCpfPassaporte(bool verificarPapelAtivo = false, string cpf = null, string passaporte = null, bool usuario = false);

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="estado">
        /// O(a) estado.
        /// </param>
        /// <param name="pessoaFisica">
        /// O(a) pessoa fisica.
        /// </param>
        /// <param name="papel">
        /// O(a) papel.
        /// </param>
        void Validar(EstadoObjeto estado, PessoaFisica pessoaFisica, Papel papel = null);

        //void SalvarPapelLog(PapelLog papelLog);

        /// <summary>
        /// Obter lista pessoa.
        /// </summary>
        ///
        /// <param name="pessoaFisica">
        /// O(a) pessoa fisica.
        /// </param>
        /// <param name="verificaStatus">
        /// true to verifica status.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;PessoaFisica&gt;
        /// </returns>
        List<PessoaFisica> ObterListaPessoa(PessoaFisica pessoaFisica, bool verificaStatus = false);

        /// <summary>
        /// Salvar.
        /// </summary>
        ///
        /// <param name="pessoaFisica">
        /// O(a) pessoa fisica.
        /// </param>
        /// <param name="papel">
        /// O(a) papel.
        /// </param>
        void Salvar(PessoaFisica pessoaFisica, Papel papel);
    }
}
