﻿using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Pessoas;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Repositorios.Pessoas
{
    /// <summary>
    /// Interface IRepositorioColaborador.
    /// </summary>
    public interface IRepositorioColaborador : IRepositorio<Colaborador>
    {
        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        /// <param name="colaborador">
        /// O(a) colaborador.
        /// </param>
        void Validar(EstadoObjeto estadoObjeto, Colaborador colaborador);

        bool Existe(string Cpf);
        /// <summary>
        /// Obter lista colaborador.
        /// </summary>
        ///
        /// <param name="colaborador">
        /// O(a) colaborador.
        /// </param>
        /// <param name="consideraAlocados">
        /// true to considera alocados.
        /// </param>
        ///
        /// <returns>
        /// Um(a) IQueryable&lt;Colaborador&gt;
        /// </returns>
        IQueryable<Colaborador> ObterListaColaborador(Colaborador colaborador, bool consideraAlocados = true);

        //void SalvarAso(ASO aso, Colaborador colaborador);

        //void RemoverAso(ASO aso);

        IEnumerable<Colaborador> FiltrarColaborador(StatusPapelLog? status = null, string cpf = null, string empresa = null, string nome = null, int cargo = 0, string id = null, string passaporte = null, MaoDeObra? maodeobra = null, string area = null, long? empregadoSap = null, string crachaativo = null);
    }
}
