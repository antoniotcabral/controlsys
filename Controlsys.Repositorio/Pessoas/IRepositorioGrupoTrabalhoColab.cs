﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Pessoas;
using Globalsys;

namespace Controlsys.Repositorios.Pessoas
{
    /// <summary>
    /// Interface IRepositorioGrupoTrabalhoColab.
    /// </summary>
    public interface IRepositorioGrupoTrabalhoColab : IRepositorio<GrupoTrabalhoColab>
    {
        /// <summary>
        /// Obter grupo trabalho.
        /// </summary>
        ///
        /// <param name="codigoColaborador">
        /// O(a) codigo colaborador.
        /// </param>
        ///
        /// <returns>
        /// Um(a) GrupoTrabalhoColab.
        /// </returns>
        GrupoTrabalhoColab ObterGrupoTrabalho(int codigoColaborador);
    }
}
