﻿using Controlsys.Dominio.Pessoas;
using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Globalsys.Validacao;

namespace Controlsys.Repositorios.Pessoas
{
    /// <summary>
    /// Interface IRepositorioTelefone.
    /// </summary>
    public interface IRepositorioTelefone : IRepositorio<Telefone>
    {
        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        /// <param name="telefone">
        /// O(a) telefone.
        /// </param>
        void Validar(EstadoObjeto estadoObjeto, Telefone telefone);
    }
}
