﻿using Controlsys.Dominio.Pessoas;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Repositorios.Pessoas
{
    /// <summary>
    /// Interface IRepositorioVisitante.
    /// </summary>
    public interface IRepositorioVisitante : IRepositorio<Visitante>
    {
        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        /// <param name="visitante">
        /// O(a) visitante.
        /// </param>
        void Validar(EstadoObjeto estadoObjeto, Visitante visitante);
    }
}
