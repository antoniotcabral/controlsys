﻿using Controlsys.Dominio.AgendamentosPessoa;
using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Repositorio.Agendamentos
{
    public interface IRepositorioPapelAgendamento : IRepositorio<PapelAgendamento>
    {
        PapelAgendamento SalvarStatusPapelAg(StatusPapelAgendamento statusPapelAg);

    }
}
