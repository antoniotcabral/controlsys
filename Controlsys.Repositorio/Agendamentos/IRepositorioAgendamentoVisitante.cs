﻿using Controlsys.Dominio.AgendamentosPessoa;
using Controlsys.Dominio.Pessoas;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Repositorios.Agendamentos
{
    public interface IRepositorioAgendamentoVisitante : IRepositorio<VisitanteAgendamento>
    {

        void Validar(EstadoObjeto estadoObjeto, VisitanteAgendamento visitante);
    }
}
