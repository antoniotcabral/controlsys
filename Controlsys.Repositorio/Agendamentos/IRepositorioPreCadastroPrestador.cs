﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Globalsys;
using Controlsys.Dominio.AgendamentosPessoa;
using Globalsys.Validacao;

namespace Controlsys.Repositorio.Agendamentos
{
    public interface IRepositorioPreCadastroPrestador : IRepositorio<PrestadorServicoAgendamento>
    {
        void Validar(EstadoObjeto estadoObjeto, PrestadorServicoAgendamento preCadastro);
    }
}
