﻿using Controlsys.Dominio.AgendamentosPessoa;
using Controlsys.Dominio.Pessoas;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Repositorio.Agendamentos
{
    public interface IRepositorioPessoaAgendamento : IRepositorio<PessoaAgendamento>
    {
        void Validar(EstadoObjeto estado, PessoaAgendamento pessoa, bool visitante = false);

        bool Existe(PessoaAgendamento pessoa);

        PessoaAgendamento ObterPorCpfPassaporte(string cpf = null, string passaporte = null);

    }
}
