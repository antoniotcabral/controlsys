﻿using Controlsys.Dominio.AgendamentosPessoa;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Globalsys;
using Globalsys.Validacao;

namespace Controlsys.Repositorio.Agendamentos
{
    public interface IRepositorioPreCadastroColaborador : IRepositorio<ColaboradorAgendamento>
    {
        void Validar(EstadoObjeto estadoObjeto, ColaboradorAgendamento preCadastro);
    }
}
