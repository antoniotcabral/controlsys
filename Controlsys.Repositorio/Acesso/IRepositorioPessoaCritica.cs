﻿using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Pessoas;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Repositorio.Acesso
{
    /// <summary>
    /// Interface IRepositorioPessoaCritica.
    /// </summary>
    public interface IRepositorioPessoaCritica : IRepositorio<PessoaCritica>
    {
        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="setor">
        /// O(a) setor.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        bool Existe(PessoaCritica setor);

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        void Validar(PessoaCritica entidade, EstadoObjeto estadoObjeto);

        /// <summary>
        /// Obter por cpf.
        /// </summary>
        ///
        /// <param name="cpf">
        /// O(a) cpf.
        /// </param>
        ///
        /// <returns>
        /// Um(a) PessoaCritica.
        /// </returns>
        PessoaCritica ObterPorCPF(string cpf, bool apenasAtivo);
    }
}
