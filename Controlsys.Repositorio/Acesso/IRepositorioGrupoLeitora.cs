﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Acesso;
using Globalsys;
using Globalsys.Validacao;

namespace Controlsys.Repositorios.Acesso
{
    /// <summary>
    /// Interface IRepositorioGrupoLeitora.
    /// </summary>
    public interface IRepositorioGrupoLeitora : IRepositorio<GrupoLeitora>
    {
        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        void Validar(GrupoLeitora entidade, EstadoObjeto estadoObjeto);

        /// <summary>
        /// Salvar leitora grupo leitora.
        /// </summary>
        ///
        /// <param name="leitorasGrpLeitora">
        /// O(a) leitoras group leitora.
        /// </param>
        void SalvarLeitoraGrupoLeitora(LeitoraGrupoLeitora leitorasGrpLeitora);

        /// <summary>
        /// Salva grupo leitora.
        /// </summary>
        ///
        /// <param name="grpLeitora">
        /// O(a) group leitora.
        /// </param>
        void SalvaGrupoLeitora(GrupoLeitora grpLeitora);

        /// <summary>
        /// Salvar grupo leitora treinamento.
        /// </summary>
        ///
        /// <param name="treinamentoGrpLeitora">
        /// O(a) treinamento group leitora.
        /// </param>
        void SalvarGrupoLeitoraTreinamento(GrupoLeitoraTreinamento treinamentoGrpLeitora);

        /// <summary>
        /// Salva grupo leitora hierarq.
        /// </summary>
        ///
        /// <param name="grupoLeitoraHirar">
        /// O(a) grupo leitora hirar.
        /// </param>
        void SalvaGrupoLeitoraHierarq(GrupoLeitoraHierarq grupoLeitoraHirar);

    }
}
