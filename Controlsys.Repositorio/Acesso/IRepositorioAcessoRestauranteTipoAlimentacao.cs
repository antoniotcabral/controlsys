﻿using Controlsys.Dominio.Acesso;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Repositorio.Acesso
{
    public interface IRepositorioAcessoRestauranteTipoAlimentacao : IRepositorio<AcessoRestauranteTipoAlimentacao>
    {

        void Validar(AcessoRestauranteTipoAlimentacao entidade, EstadoObjeto estadoObjeto);
    }
}
