﻿using Controlsys.Dominio.Acesso;
using Globalsys;
using Globalsys.Validacao;


namespace Controlsys.Repositorio.Acesso
{
    /// <summary>
    /// Interface IRepositorioAcessoTemporario.
    /// </summary>
    public interface IRepositorioAcessoTemporario : IRepositorio<AcessoTemporario>
    {
        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        void Validar(AcessoTemporario entidade, EstadoObjeto estadoObjeto);
    }
}
