﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Acesso;
using Globalsys;

namespace Controlsys.Repositorios.Acesso
{
    public interface IRepositorioLogControladora : IRepositorio<LogControladora>
    {
        IQueryable<LogControladora> ObterPorControladora(int codigoControladora);
    }
}
