﻿using Controlsys.Dominio.Acesso;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Controlsys.Repositorio.Acesso
{
    /// <summary>
    /// 
    /// </summary>
   public interface IRepositorioCameraImagem : IRepositorio<CameraImagem>
    {
        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        /// 
        /// <param name="cameraImagem">
        /// O local.
        /// </param>
        /// 
        /// <returns>
        /// Um bool
        /// </returns>
        bool Existe(CameraImagem cameraImagem);

            /// <summary>
            /// Valida a modificação no objeto.
            /// </summary>
            ///
            /// <param name="entidade">
            /// O(a) entidade.
            /// </param>
            /// <param name="estadoObjeto">
            /// O(a) estado objeto.
            /// </param>
            void Validar(CameraImagem entidade, EstadoObjeto estadoObjeto);

        }
    }

