﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Pessoas;
using Globalsys;

namespace Controlsys.Repositorios.Acesso
{
    /// <summary>
    /// Interface IRepositorioSincronizaPessoaAcesso.
    /// </summary>
    public interface IRepositorioSincronizaPessoaAcesso : IRepositorio<SincronizaPessoaAcesso>
    {
        /// <summary>
        /// Sincronizar.
        /// </summary>
        ///
        /// <param name="papel">
        /// O(a) papel.
        /// </param>
        /// <param name="tipoSincronizacao">
        /// O(a) tipo sincronizacao.
        /// </param>
        /// <param name="dataInicio">
        /// O(a) data inicio.
        /// </param>
        /// <param name="controladoras">
        /// O(a) controladoras.
        /// </param>
        void Sincronizar(Dominio.Pessoas.Papel papel, TipoSincronizacaoPessoa tipoSincronizacao, DateTime? dataInicio, List<Controladora> controladoras = null);

        /// <summary>
        /// Retransmitir.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        /// <param name="dataInicio">
        /// O(a) data inicio.
        /// </param>
        ///
        /// <returns>
        /// Um(a) SincronizaPessoaAcesso.
        /// </returns>
        SincronizaPessoaAcesso Retransmitir(int codigo, DateTime? dataInicio);

        /// <summary>
        /// Obtem última sincronização de lista da controladora
        /// </summary>
        /// <param name="codigoControladora">Código da controladora</param>
        /// <returns></returns>
        SincronizaPessoaAcesso ObterUltimaSincronizacao(int codigoControladora);

        /// <summary>
        /// Obtem última sincronização de lista da controladora
        /// </summary>
        /// <param name="codigoControladora">Código da controladora</param>
        /// <returns></returns>
        SincronizaPessoaAcesso LiberarControladoraAcessoDireto(Controladora controladora, TipoSincronizacaoPessoa tipoSincronizacao);
    }
}
