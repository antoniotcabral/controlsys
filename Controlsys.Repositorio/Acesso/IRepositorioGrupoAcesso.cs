﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Acesso;
using Globalsys;
using Globalsys.Validacao;
using Controlsys.Dominio.Pessoas;

namespace Controlsys.Repositorio.Acesso
{
    /// <summary>
    /// Interface IRepositorioGrupoAcesso.
    /// </summary>
    public interface IRepositorioGrupoAcesso : IRepositorio<GrupoAcesso>
    {
        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        void Validar(GrupoAcesso entidade, EstadoObjeto estadoObjeto);

        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="grupoAcesso">
        /// O(a) grupo acesso.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        bool Existe(GrupoAcesso grupoAcesso);

        //void SalvaGrupoAcesso(GrupoAcesso grupoAcesso);

        /// <summary>
        /// Salvar grupo acesso papel.
        /// </summary>
        ///
        /// <param name="grupoAcessoPapel">
        /// O(a) grupo acesso papel.
        /// </param>
        void SalvarGrupoAcessoPapel(GrupoAcessoPapel grupoAcessoPapel);

        /// <summary>
        /// Salvar grupo leitora acesso.
        /// </summary>
        ///
        /// <param name="grupoLeitoraAcesso">
        /// O(a) grupo leitora acesso.
        /// </param>
        void SalvarGrupoLeitoraAcesso(GrupoLeitoraAcesso grupoLeitoraAcesso);

        /// <summary>
        /// Obter controladoras.
        /// </summary>
        ///
        /// <param name="gruposLeitoras">
        /// O(a) grupos leitoras.
        /// </param>
        /// <param name="validaCNH">
        /// true to valida cnh.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;Controladora&gt;
        /// </returns>
        List<Controladora> ObterControladoras(IEnumerable<GrupoLeitora> gruposLeitoras, bool validaCNH = false);

        /// <summary>
        /// Obter permissoes.
        /// </summary>
        ///
        /// <param name="papel">
        /// O(a) papel.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;Controladora&gt;
        /// </returns>
        List<Controladora> ObterPermissoes(Papel papel);

        /// <summary>
        /// Obter controladoras cnh.
        /// </summary>
        ///
        /// <param name="papel">
        /// O(a) papel.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;Controladora&gt;
        /// </returns>
        List<Controladora> ObterControladorasCNH(Papel papel);
    }
}
