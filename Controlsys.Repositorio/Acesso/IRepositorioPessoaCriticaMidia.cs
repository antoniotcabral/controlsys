﻿using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Parametros;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Controlsys.Repositorio.Acesso
{
    public interface IRepositorioPessoaCriticaMidia : IRepositorio<PessoaCriticaMidia>
    {
        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="pessoaCriticaMidia">
        /// O(a) Pessoa Critica Midia.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Pessoa Critica Midia.
        /// </returns>
        bool Existe(PessoaCriticaMidia pessoaCriticaMidia);

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        void Validar(PessoaCriticaMidia entidade, EstadoObjeto estadoObjeto);


        PessoaCriticaMidia SalvarPessoaCriticaMidia(PessoaCritica pessoaCritica, Midia midia);
    }
}
