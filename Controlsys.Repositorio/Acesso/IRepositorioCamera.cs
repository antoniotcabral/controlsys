﻿using Controlsys.Dominio.Acesso;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Repositorio.Acesso
{
    /// <summary>
    /// Interface ILocal
    /// </summary>
    public interface IRepositorioCamera : IRepositorio<Camera>
    {
        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        /// 
        /// <param name="camera">
        /// O local.
        /// </param>
        /// 
        /// <returns>
        /// Um bool
        /// </returns>
        bool Existe(Camera camera);

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        void Validar(Camera entidade, EstadoObjeto estadoObjeto);

    }
}
