﻿using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Pessoas;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

    namespace Controlsys.Repositorio.Acesso
    {
        /// <summary>
        /// Interface ILocal
        /// </summary>
        public interface IRepositorioAcessoVeiculo : IRepositorio<AcessoVeiculo>
        {
        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        /// 
        /// <param name="acessoVeiculo">
        /// O local.
        /// </param>
        /// 
        /// <returns>
        /// Um bool
        /// </returns>
        bool Existe(AcessoVeiculo acessoVeiculo);

            /// <summary>
            /// Valida a modificação no objeto.
            /// </summary>
            ///
            /// <param name="entidade">
            /// O(a) entidade.
            /// </param>
            /// <param name="estadoObjeto">
            /// O(a) estado objeto.
            /// </param>
            void Validar(AcessoVeiculo entidade, EstadoObjeto estadoObjeto);

        /// <summary>
        /// Obter lista acesso.
        /// </summary>
        ///
        /// <param name="status">
        /// O(a) status.
        /// </param>
        /// <param name="cracha">
        /// O(a) cracha.
        /// </param>
        /// <param name="cpfPassaporte">
        /// O(a) cpfPassaporte.
        /// </param>
        /// <param name="empresa">
        /// O(a) empresa.
        /// </param>
        /// <param name="nome">
        /// O(a) nome.
        /// </param>
        /// <param name="cargo">
        /// O(a) cargo.
        /// </param>
        /// <param name="dataInicio">
        /// O(a) data inicio.
        /// </param>
        /// <param name="dataFim">
        /// O(a) data fim.
        /// </param>
        /// <param name="userLogado">
        /// O(a) user logado.
        /// </param>
        /// <param name="leitoras">
        /// O(a) leitoras.
        /// </param>
        /// <param name="aloc">
        /// O(a) aloc.
        /// </param>
        ///
        /// <returns>
        /// Um(a) IQueryable&lt;ControleAcesso&gt;
        /// </returns>
        IQueryable<AcessoVeiculo> ObterListaAcesso(List<StatusAcesso> status, string cracha, string cpfPassaporte, string empresa, string nome, int cargo, DateTime? dataInicio, DateTime? dataFim, string userLogado, List<int> leitoras = null, IQueryable<AlocacaoColaborador> aloc = null, bool validaGestor = false, long? empregadoSap = null, long? gestorPonto = null, long? superiorImediato = null, string codigoPht = null, IList<string> localLeitora = null, string passaporte = null, string maodeobra = null, string fornecedor = null, string contratada = null, string placa = null, string modeloCredencial = null, string cor = null, string modeloVeiculo = null, string fabricante = null, string nomeCamera = null);

        IQueryable<AcessoVeiculo> ObterListaAcesso(string nome = null, string cpf = null, string passaporte = null, string empresa = null, string cnpj = null, TipoPapel? tipoPapel = null, DateTime? dataInicio = null, DateTime? dataFim = null, long? empregadoSap = null, long? gestorPonto = null, long? superiorImediato = null, string codigoPht = null, IList<string> localLeitora = null, string placa = null, string modeloCredencial = null, string cor = null, string modeloVeiculo = null, string fabricante = null, string nomeCamera = null);

    }
    }


