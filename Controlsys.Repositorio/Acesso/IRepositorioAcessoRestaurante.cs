﻿using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Parametros;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Repositorio.Acesso
{
    public interface IRepositorioAcessoRestaurante : IRepositorio<AcessoRestaurante>
    {
        bool Existe(AcessoRestaurante acRestaurante);

        void Validar(AcessoRestaurante entidade, EstadoObjeto estadoObjeto, bool alteracaoSistema = false);

        void SalvarTipoAlimentacaoRestaurante(AcessoRestauranteTipoAlimentacao tipoAlimentacaoRestaurante);

        void SalvarLogAcessoRestaurante(LogAcessoRestaurante logAcessoRestaurante);
    }
}
