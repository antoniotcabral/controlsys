﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Acesso;
using Globalsys;

namespace Controlsys.Repositorio.Acesso
{
    /// <summary>
    /// Interface IRepositorioGrupoLeitoraAcesso.
    /// </summary>
    public interface IRepositorioGrupoLeitoraAcesso : IRepositorio<GrupoLeitoraAcesso>
    {

    }
}
