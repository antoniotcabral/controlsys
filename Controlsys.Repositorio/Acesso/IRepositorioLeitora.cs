﻿using Controlsys.Dominio.Acesso;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Repositorios.Acesso
{
    /// <summary>
    /// Interface IRepositorioLeitora.
    /// </summary>
    public interface IRepositorioLeitora: IRepositorio<Leitora>
    {
        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="setor">
        /// O(a) setor.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        bool Existe(Leitora setor);

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        void Validar(Leitora entidade, EstadoObjeto estadoObjeto);
    }
}
