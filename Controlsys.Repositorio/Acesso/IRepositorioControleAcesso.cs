﻿using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Pessoas;
using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Repositorio.Acesso
{
    /// <summary>
    /// Interface IRepositorioControleAcesso.
    /// </summary>
    public interface IRepositorioControleAcesso : IRepositorio<ControleAcesso>
    {
        /// <summary>
        /// Obter lista acesso.
        /// </summary>
        ///
        /// <param name="status">
        /// O(a) status.
        /// </param>
        /// <param name="cracha">
        /// O(a) cracha.
        /// </param>
        /// <param name="cpfPassaporte">
        /// O(a) cpfPassaporte.
        /// </param>
        /// <param name="empresa">
        /// O(a) empresa.
        /// </param>
        /// <param name="nome">
        /// O(a) nome.
        /// </param>
        /// <param name="cargo">
        /// O(a) cargo.
        /// </param>
        /// <param name="dataInicio">
        /// O(a) data inicio.
        /// </param>
        /// <param name="dataFim">
        /// O(a) data fim.
        /// </param>
        /// <param name="userLogado">
        /// O(a) user logado.
        /// </param>
        /// <param name="leitoras">
        /// O(a) leitoras.
        /// </param>
        /// <param name="aloc">
        /// O(a) aloc.
        /// </param>
        ///
        /// <returns>
        /// Um(a) IQueryable&lt;ControleAcesso&gt;
        /// </returns>
        IQueryable<ControleAcesso> ObterListaAcesso(List<StatusAcesso> status, string cracha, string cpfPassaporte, string empresa, string nome, int cargo, DateTime? dataInicio, DateTime? dataFim, string userLogado, List<int> leitoras = null, IQueryable<AlocacaoColaborador> aloc = null, bool validaGestor = false, long? empregadoSap = null, long? gestorPonto = null, long? superiorImediato = null, string codigoPht = null, IList<string> localLeitora = null, string passaporte = null, string maodeobra = null, string fornecedor = null, string contratada = null);

        IQueryable<ControleAcesso> ObterListaAcesso(string nome = null, string cpf = null, string passaporte = null, string empresa = null, string cnpj = null, TipoPapel? tipoPapel = null, DateTime? dataInicio = null, DateTime? dataFim = null, long? empregadoSap = null, long? gestorPonto = null, long? superiorImediato = null, string codigoPht = null, IList<string> localLeitora = null);

        IQueryable<ControleAcesso> ObterListaAcessoRestaurantePorRefeicao(string nome = null, string cracha = null, string cpfPassaporte = null, string empresa = null, int restaurante = 0, int tipoAlimentacao = 0, int setor = 0, DateTime? dataInicio = null, DateTime? dataFim = null, IQueryable<AlocacaoColaborador> aloc = null);

        IQueryable<ControleAcesso> ObterListaAcessoAlertaTempoPermanencia(TipoPapel? tipoPapel = null, DateTime? dataInicio = null, DateTime? dataFim = null, IList<string> localLeitora = null);
    }
}
