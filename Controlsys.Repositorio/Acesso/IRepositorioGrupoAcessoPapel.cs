﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Acesso;
using Globalsys;
using Globalsys.Validacao;

namespace Controlsys.Repositorio.Acesso
{
    /// <summary>
    /// Interface IRepositorioGrupoAcessoPapel.
    /// </summary>
    public interface IRepositorioGrupoAcessoPapel : IRepositorio<GrupoAcessoPapel>
    {
        /// <summary>
        /// Obter grupos de acesso.
        /// </summary>
        ///
        /// <param name="codigoPapel">
        /// O(a) codigo papel.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;GrupoAcessoPapel&gt;
        /// </returns>
        List<GrupoAcessoPapel> ObterGruposDeAcesso(int codigoPapel);

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="estado">
        /// O(a) estado.
        /// </param>
        /// <param name="grupoAcessopPapel">
        /// O(a) papel.
        /// </param>
        void Validar(EstadoObjeto estado, GrupoAcessoPapel entidade);
    }
}
