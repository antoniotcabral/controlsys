﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Seguranca;
using Globalsys;

namespace Controlsys.Repositorios.Acesso
{
    /// <summary>
    /// Interface IRepositorioControladora.
    /// </summary>
    public interface IRepositorioControladora : IRepositorio<Controladora>
    {
        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="controladora">
        /// O(a) controladora.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        void Validar(Controladora controladora, Globalsys.Validacao.EstadoObjeto estadoObjeto);

        /// <summary>
        /// Alterar status da controladora. Registra log da controladora.
        /// </summary>
        /// <param name="controladora"></param>
        /// <param name="status"></param>
        void AlterarStatus(Controladora controladora, StatusControladora status, Usuario usuario);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="controladora"></param>
        /// <param name="status"></param>
        /// <param name="usuario"></param>
        void AlterarStatusControladoraIntegrador(Controladora controladora, StatusControladora status, Usuario usuario);
    }
}
