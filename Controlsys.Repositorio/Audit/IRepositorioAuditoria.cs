﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Audit;
using Globalsys;

namespace Controlsys.Repositorios.Audit
{
    /// <summary>
    /// Interface IRepositorioAuditoria.
    /// </summary>
    public interface IRepositorioAuditoria : IRepositorio<Auditoria>
    {
    }
}
