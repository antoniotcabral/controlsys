﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Audit;
using Globalsys;

namespace Controlsys.Repositorio.Audit
{
    /// <summary>
    /// Interface IRepositorioLogError.
    /// </summary>
    public interface IRepositorioLogAvigilon: IRepositorio<Log_Avigilon>
    {
    }
}
