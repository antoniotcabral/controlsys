﻿using Controlsys.Dominio.Audit;
using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Repositorio.Audit
{
    /// <summary>
    /// Interface IRepositorioIntegracaoSAP.
    /// </summary>
    public interface IRepositorioIntegracaoSAPAcesso : IRepositorio<IntegracaoSAPAcesso>
    {
    }
}
