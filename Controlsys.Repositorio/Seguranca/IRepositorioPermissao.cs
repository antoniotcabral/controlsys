﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Seguranca;
using Globalsys;

namespace Controlsys.Repositorios.Seguranca
{
    /// <summary>
    /// Interface IRepositorioPermissao.
    /// </summary>
    public interface IRepositorioPermissao : IRepositorio<Permissao>
    {
        /// <summary>
        /// Possui permissao pagina.
        /// </summary>
        ///
        /// <param name="usuarioLogado">
        /// O(a) usuario logado.
        /// </param>
        /// <param name="url">
        /// URL of the document.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        bool PossuiPermissaoPagina(string usuarioLogado, string url);

        /// <summary>
        /// Possui permissao.
        /// </summary>
        ///
        /// <param name="usuarioLogado">
        /// O(a) usuario logado.
        /// </param>
        /// <param name="url">
        /// URL of the document.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        bool PossuiPermissao(string usuarioLogado, string url);

        /// <summary>
        /// Obter acoes usuario.
        /// </summary>
        ///
        /// <param name="usuarioLogado">
        /// O(a) usuario logado.
        /// </param>
        /// <param name="codigoModulo">
        /// O(a) codigo modulo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) IQueryable&lt;Acao&gt;
        /// </returns>
        IQueryable<Acao> ObterAcoesUsuario(string usuarioLogado, int? codigoModulo = null);
    }
}
