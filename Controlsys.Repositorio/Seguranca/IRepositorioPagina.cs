﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Globalsys;
using Controlsys.Dominio.Seguranca;

namespace Controlsys.Repositorios.Seguranca
{
    /// <summary>
    /// Interface IRepositorioPagina.
    /// </summary>
    public interface IRepositorioPagina : IRepositorio<Pagina>
    {
        
    }
}
