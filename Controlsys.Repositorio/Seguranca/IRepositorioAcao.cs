﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Seguranca;
using Globalsys;

namespace Controlsys.Repositorios.Seguranca
{
    /// <summary>
    /// Interface IRepositorioAcao.
    /// </summary>
    public interface IRepositorioAcao : IRepositorio<Acao>
    {
        /// <summary>
        /// Obter por URL.
        /// </summary>
        ///
        /// <param name="url">
        /// URL of the document.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Acao.
        /// </returns>
        Acao ObterPorUrl(string url);
    }
}
