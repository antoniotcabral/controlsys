﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Seguranca;
using Globalsys;

namespace Controlsys.Repositorios.Seguranca
{
    /// <summary>
    /// Interface IRepositorioGrupoLeitoraGrupo.
    /// </summary>
    public interface IRepositorioGrupoLeitoraGrupo : IRepositorio<GrupoLeitoraGrupo>
    {

    }
}
