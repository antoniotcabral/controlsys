﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Seguranca;
using Globalsys;

namespace Controlsys.Repositorios.Seguranca
{
    /// <summary>
    /// Interface IRepositorioGrupo.
    /// </summary>
    public interface IRepositorioGrupo : IRepositorio<Grupo>
    {
        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="grupo">
        /// O(a) grupo.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        void Validar(Grupo grupo, Globalsys.Validacao.EstadoObjeto estadoObjeto);

        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="grupo">
        /// O(a) grupo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        bool Existe(Grupo grupo);
    }
}
