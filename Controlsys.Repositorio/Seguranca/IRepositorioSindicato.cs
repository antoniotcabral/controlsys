﻿using Controlsys.Dominio.Parametros;
using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Repositorio.Seguranca
{
    public interface IRepositorioSindicato : IRepositorio<Sindicato>
    {
        Sindicato ObterPorUrl(string url);
    }
}