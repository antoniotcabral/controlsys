﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Seguranca;
using Globalsys;
using Globalsys.Validacao;

namespace Controlsys.Repositorios.Seguranca
{
    /// <summary>
    /// Interface IRepositorioUsuario.
    /// </summary>
    public interface IRepositorioUsuario : IRepositorio<Usuario>
    {
        /// <summary>
        /// Obter por login.
        /// </summary>
        ///
        /// <param name="login">
        /// O(a) login.
        /// </param>
        /// <param name="senha">
        /// O(a) senha.
        /// </param>
        /// <param name="somenteAtivos">
        /// true to somente ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Usuario.
        /// </returns>
        Usuario ObterPorLogin(string login, string senha = null, bool somenteAtivos = false);

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        /// <param name="usuario">
        /// O(a) usuario.
        /// </param>
        void Validar(EstadoObjeto estadoObjeto, Usuario usuario);

        /// <summary>
        /// Alterar senha.
        /// </summary>
        ///
        /// <param name="login">
        /// O(a) login.
        /// </param>
        /// <param name="senhaNova">
        /// O(a) senha nova.
        /// </param>
        /// <param name="senhaNovaConf">
        /// O(a) senha nova conf.
        /// </param>
        /// <param name="senhaAtual">
        /// O(a) senha atual.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Usuario.
        /// </returns>
        Usuario AlterarSenha(string login, string senhaNova, string senhaNovaConf, string senhaAtual = null);

        /// <summary>
        /// Obter por email document.
        /// </summary>
        ///
        /// <param name="email">
        /// O(a) email.
        /// </param>
        /// <param name="cpf">
        /// O(a) cpf.
        /// </param>
        /// <param name="passaporte">
        /// O(a) passaporte.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Usuario.
        /// </returns>
        Usuario ObterPorEmailDoc(string email, string cpf = null, string passaporte = null);

        /// <summary>
        /// Obter por pessoa.
        /// </summary>
        ///
        /// <param name="codigoPessoa">
        /// O(a) codigo pessoa.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Usuario.
        /// </returns>
        Usuario ObterPorPessoa(string codigoPessoa);
    }
}
