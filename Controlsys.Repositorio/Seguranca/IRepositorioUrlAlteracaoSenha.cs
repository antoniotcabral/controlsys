﻿using Controlsys.Dominio.Seguranca;
using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Repositorios.Seguranca
{
    /// <summary>
    /// Interface IRepositorioUrlAlteracaoSenha.
    /// </summary>
    public interface IRepositorioUrlAlteracaoSenha : IRepositorio<UrlAlteracaoSenha>
    {
        /// <summary>
        /// Obter por login.
        /// </summary>
        ///
        /// <param name="login">
        /// O(a) login.
        /// </param>
        /// <param name="urlCrip">
        /// O(a) URL crip.
        /// </param>
        /// <param name="somenteAtivo">
        /// true to somente ativo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) UrlAlteracaoSenha.
        /// </returns>
        UrlAlteracaoSenha ObterPorLogin(string login, string urlCrip, bool somenteAtivo = true);
    }
}
