﻿using Controlsys.Dominio.Ocorrencias;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Repositorio.Ocorrencias
{
    public interface IRepositorioOcorrenciaTransito : IRepositorio<OcorrenciaTransito>
    {

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        void Validar(OcorrenciaTransito entidade, EstadoObjeto estadoObjeto);       
    }
}
