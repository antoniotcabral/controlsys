﻿using Controlsys.Dominio.Ocorrencias;
using Controlsys.Dominio.Parametros;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Repositorio.Ocorrencias
{
    /// <summary>
    /// Interface IRepositorioOcorrenciaSeguranca.
    /// </summary>
    public interface IRepositorioOcorrenciaSeguranca : IRepositorio<OcorrenciaSeguranca>
    {
        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="ocorrenciaSeguranca">
        /// O(a) ocorrêcia de segurança.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        bool Existe(OcorrenciaSeguranca ocorrenciaSeguranca);

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        void Validar(OcorrenciaSeguranca entidade, EstadoObjeto estadoObjeto);       
    }
}
