﻿using Controlsys.Dominio.Ocorrencias;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Repositorio.Ocorrencias
{
    public interface IRepositorioTreinamentoOcorrenciaTransito : IRepositorio<TreinamentoOcorrenciaTransito>
    {
        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="treinamentoOcorrenciaTransito">
        /// O(a) treinamento Ocorrencia Transito.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        bool Existe(TreinamentoOcorrenciaTransito treinamentoOcorrenciaTransito);

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        void Validar(TreinamentoOcorrenciaTransito entidade, EstadoObjeto estadoObjeto);       
    }
}
