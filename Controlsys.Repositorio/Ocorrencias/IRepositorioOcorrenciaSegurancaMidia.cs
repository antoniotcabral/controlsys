﻿using Controlsys.Dominio.Ocorrencias;
using Controlsys.Dominio.Parametros;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Controlsys.Repositorio.Ocorrencias
{
    public interface IRepositorioOcorrenciaSegurancaMidia : IRepositorio<OcorrenciaSegurancaMidia>
    {
        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        void Validar(OcorrenciaSegurancaMidia entidade, EstadoObjeto estadoObjeto);

        OcorrenciaSegurancaMidia SalvarOcorrenciaSegurancaMidia(OcorrenciaSeguranca ocorrencia, Midia midia);
    }
}
