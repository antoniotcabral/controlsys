﻿using Controlsys.Dominio.Ocorrencias;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Controlsys.Repositorio.Ocorrencias
{
    public interface IRepositorioOcorrenciaSegurancaAgente : IRepositorio<OcorrenciaSegurancaAgente>
    {
        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="ocorrenciaSeguranca">
        /// O(a) ocorrêcia de segurança.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        bool Existe(OcorrenciaSegurancaAgente ocorrenciaSegurancaAgente);

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        void Validar(OcorrenciaSegurancaAgente entidade, EstadoObjeto estadoObjeto);
    }
}
