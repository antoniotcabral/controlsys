﻿using Controlsys.Dominio.CredenciamentoVeiculos;
using Controlsys.Dominio.Ocorrencias;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Controlsys.Repositorio.Ocorrencias
{
    public interface IRepositorioOcorrenciaSegurancaVeiculo : IRepositorio<OcorrenciaSegurancaVeiculo>
    {
        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        void Validar(OcorrenciaSegurancaVeiculo entidade, EstadoObjeto estadoObjeto);

        OcorrenciaSegurancaVeiculo SalvarOcorrenciaSegurancaVeiculo(OcorrenciaSeguranca ocorrencia, Veiculo veiculo);
    }
}
