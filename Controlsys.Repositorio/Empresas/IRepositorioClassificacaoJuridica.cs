﻿using Controlsys.Dominio.Parametros;
using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Repositorio.Empresas
{
    public interface IRepositorioClassificacaoJuridica : IRepositorio<ClassificacaoJuridica>
    {
        void Validar(ClassificacaoJuridica entidade, string[] letrasFormula);
    }
}
