﻿using Controlsys.Dominio.Empresas;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Repositorios.Empresas
{
    /// <summary>
    /// Interface IRepositorioProjeto.
    /// </summary>
    public interface IRepositorioProjeto : IRepositorio<Projeto>
    {
        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="checklistDoc">
        /// O(a) checklist document.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        bool Existe(Projeto checklistDoc);

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        void Validar(Projeto entidade, EstadoObjeto estadoObjeto);

        /// <summary>
        /// Salvar area projeto.
        /// </summary>
        ///
        /// <param name="areaProjeto">
        /// O(a) area projeto.
        /// </param>
        void SalvarAreaProjeto(AreaProjeto areaProjeto);

        /// <summary>
        /// Salvar grupo trabalho projeto.
        /// </summary>
        ///
        /// <param name="grupoTrabProjeto">
        /// O(a) grupo trab projeto.
        /// </param>
        void SalvarGrupoTrabalhoProjeto(GrupoTrabalhoProjeto grupoTrabProjeto);

        /// <summary>
        /// Salvar treinamento projeto.
        /// </summary>
        ///
        /// <param name="treinamentoProjeto">
        /// O(a) treinamento projeto.
        /// </param>
        void SalvarTreinamentoProjeto(TreinamentoProjeto treinamentoProjeto);
    }
}
