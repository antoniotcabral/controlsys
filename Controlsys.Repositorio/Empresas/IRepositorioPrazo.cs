﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Empresas;
using Globalsys;
using Globalsys.Validacao;

namespace Controlsys.Repositorios.Empresas
{
    /// <summary>
    /// Interface IRepositorioPrazo.
    /// </summary>
    public interface IRepositorioPrazo : IRepositorio<Prazo>
    {
        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="prazo">
        /// O(a) prazo.
        /// </param>
        /// <param name="estadoObj">
        /// O(a) estado object.
        /// </param>
        void Validar(Prazo prazo, EstadoObjeto estadoObj);
    }
}
