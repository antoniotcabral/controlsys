﻿using Controlsys.Dominio.Empresas;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Controlsys.Repositorio.Empresas
{
    public interface IRepositorioEmpresaSindicato : IRepositorio<EmpresaSindicato>
    {
        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="empresaSindicato">
        /// O(a) Empresa Sindicato.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Empresa Sindicato.
        /// </returns>
        bool Existe(EmpresaSindicato empresaSindicato);

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        void Validar(EmpresaSindicato entidade, EstadoObjeto estadoObjeto);
    }
}
