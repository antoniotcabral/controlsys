﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Empresas;
using Globalsys;
using Globalsys.Validacao;

namespace Controlsys.Repositorios.Empresas
{
    /// <summary>
    /// Interface IRepositorioPedidoCompra.
    /// </summary>
    public interface IRepositorioPedidoCompra : IRepositorio<PedidoCompra>
    {
        /// <summary>
        /// Obter por projeto.
        /// </summary>
        ///
        /// <param name="codigoProjeto">
        /// O(a) codigo projeto.
        /// </param>
        ///
        /// <returns>
        /// Um(a) PedidoCompra.
        /// </returns>
        PedidoCompra ObterPorProjeto(int codigoProjeto);

        /// <summary>
        /// Obter contratada.
        /// </summary>
        ///
        /// <param name="codigoPedido">
        /// O(a) codigo pedido.
        /// </param>
        ///
        /// <returns>
        /// Um(a) PedidoCompra.
        /// </returns>
        PedidoCompra ObterContratada(int codigoPedido);

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="pedidoCompra">
        /// O(a) pedido compra.
        /// </param>
        /// <param name="estadoObj">
        /// O(a) estado object.
        /// </param>
        void Validar(PedidoCompra pedidoCompra, EstadoObjeto estadoObj);

        /// <summary>
        /// Inativa o objeto.
        /// </summary>
        ///
        /// <param name="pedidoCompra">
        /// O(a) pedido compra.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;int&gt;
        /// </returns>
        List<int> Inativar(PedidoCompra pedidoCompra);

        /// <summary>
        /// Ativa o objeto.
        /// </summary>
        ///
        /// <param name="pedidoCompra">
        /// O(a) pedido compra.
        /// </param>
        void Ativar(PedidoCompra pedidoCompra);

        /// <summary>
        /// Remover.
        /// </summary>
        ///
        /// <param name="pedidoCompra">
        /// O(a) pedido compra.
        /// </param>
        /// <param name="status">
        /// O(a) status.
        /// </param>
        void Remover(PedidoCompra pedidoCompra, StatusPedido status);

        /// <summary>
        /// Lista todas as empresas associadas ao pedido de compras indicado, incluindo a empresa do proprio pedido e todas as empresas de todos dos pedidos filhos.
        /// </summary>
        /// <param name="codigoPedido">Código do Pedido de Compra</param>
        /// <returns></returns>
        List<Empresa> ObterEmpresasRelacionadasPO(int codigoPedido);
    }
}
