﻿using Controlsys.Dominio.Empresas;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Repositorios.Empresas
{
    /// <summary>
    /// Interface IRepositorioFornecedor.
    /// </summary>
    public interface IRepositorioFornecedor : IRepositorio<Fornecedor>
    {
        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="fornecedor">
        /// O(a) fornecedor.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        void Validar(Fornecedor fornecedor, EstadoObjeto estadoObjeto);

        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="area">
        /// O(a) area.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        bool Existe(Fornecedor area);

        /// <summary>
        /// Salvar fornecedor empresa.
        /// </summary>
        ///
        /// <param name="fe">
        /// O(a) fe.
        /// </param>
        void SalvarFornecedorEmpresa(FornecedorEmpresa fe);

        /// <summary>
        /// Obter por cnpj.
        /// </summary>
        ///
        /// <param name="cpnj">
        /// O(a) cpnj.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Fornecedor.
        /// </returns>
        Fornecedor ObterPorCnpj(string cpnj);
    }
}
