﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Empresas;
using Globalsys;

namespace Controlsys.Repositorios.Empresas
{
    /// <summary>
    /// Interface IRepositorioSetorCustoPedido.
    /// </summary>
    public interface IRepositorioSetorCustoPedido : IRepositorio<SetorCustoPedido>
    {

    }
}
