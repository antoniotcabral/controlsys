﻿using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Enderecos;
using Globalsys;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Repositorios.Empresas
{
    /// <summary>
    /// Interface IRepositorioEmpresa.
    /// </summary>
    public interface IRepositorioEmpresa : IRepositorio<Empresa>
    {
        //bool Existe(Empresa empresa);

        /// <summary>
        /// Obter todos cnae.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) List&lt;CNAE&gt;
        /// </returns>
        List<CNAE> ObterTodosCNAE();

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        void Validar(Empresa entidade, EstadoObjeto estadoObjeto);

        /// <summary>
        /// Salvar empresa cnae.
        /// </summary>
        ///
        /// <param name="empresaCnae">
        /// O(a) empresa cnae.
        /// </param>
        void SalvarEmpresaCnae(EmpresaCNAE empresaCnae);

        /// <summary>
        /// Obter por cnpj.
        /// </summary>
        ///
        /// <param name="cpnj">
        /// O(a) cpnj.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Empresa.
        /// </returns>
        Empresa ObterPorCnpj(string cpnj);
    }
}
