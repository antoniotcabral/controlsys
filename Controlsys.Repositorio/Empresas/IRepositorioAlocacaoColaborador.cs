﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Empresas;
using Globalsys;
using Globalsys.Validacao;

namespace Controlsys.Repositorios.Empresas
{
    /// <summary>
    /// Interface IRepositorioAlocacaoColaborador.
    /// </summary>
    public interface IRepositorioAlocacaoColaborador : IRepositorio<AlocacaoColaborador>
    {
        /// <summary>
        /// Obter alocacao.
        /// </summary>
        ///
        /// <param name="cod_colaborador">
        /// O(a) cod colaborador.
        /// </param>
        /// <param name="todosStatus">
        /// true to todos status.
        /// </param>
        ///
        /// <returns>
        /// Um(a) AlocacaoColaborador.
        /// </returns>
        AlocacaoColaborador ObterAlocacao(int cod_colaborador, bool todosStatus = false);

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        void Validar(AlocacaoColaborador entidade, EstadoObjeto estadoObjeto);
    }
}
