﻿using Controlsys.Dominio.Relatorio;
using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Repositorio.Relatorio
{
    public interface IRepositorioRelatorioCrachaPeriodo : IRepositorio<RelatorioCrachaPeriodo>
    {
        IQueryable<RelatorioCrachaPeriodo> ObterDados();
    }
}
