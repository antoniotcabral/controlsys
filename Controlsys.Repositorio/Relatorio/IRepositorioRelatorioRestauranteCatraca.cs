﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Relatorio;
using Globalsys;

namespace Controlsys.Repositorio.Relatorio
{
    public interface IRepositorioRelatorioRestauranteCatraca : IRepositorio<RelatorioRestauranteCatraca>
    {
        IQueryable<RelatorioRestauranteCatraca> ObterDados();
    }
}
