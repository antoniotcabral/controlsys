﻿using Controlsys.Dominio.Relatorio;
using Globalsys;
using System.Linq;

namespace Controlsys.Repositorio.Relatorio
{
    public interface IRepositorioRelatorioUltimoCrachaPapel : IRepositorio<RelatorioUltimoCrachaPapel>
    {
        IQueryable<RelatorioUltimoCrachaPapel> ObterDados();
    }
}
