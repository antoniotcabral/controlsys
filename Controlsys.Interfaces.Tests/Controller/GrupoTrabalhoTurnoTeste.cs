﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Controlsys.Dominio.Parametros;
using Controlsys.Infra;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Controlsys.Persistencia;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) GrupoTrabalhoTurnoTeste.
    /// </summary>
    [TestClass]
    public class GrupoTrabalhoTurnoTeste
    {
        /// <summary>
        /// Busca aleatóriamente um GrupoTrabalhoTurno cadastrado na base de dados.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// Filtrar GrupoTrabalhoTurno ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) GrupoTrabalhoTurno.
        /// </returns>
        internal static GrupoTrabalhoTurno ObterGrupoTrabalhoTurno(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repGrupoTrabalhoTurno = new Repositorio<GrupoTrabalhoTurno>(unidTrabalho);
            var gruposTrabalhoTurno = repGrupoTrabalhoTurno.ObterTodos();
            Assert.IsNotNull(gruposTrabalhoTurno);
            Assert.IsTrue(gruposTrabalhoTurno.Any());

            Random rand = new Random();
            GrupoTrabalhoTurno grupoTrabalho = null;
            gruposTrabalhoTurno = gruposTrabalhoTurno.OrderBy(x => rand.Next());
            foreach (var item in gruposTrabalhoTurno)
            {
                grupoTrabalho = item;

                if (somenteAtivos)
                {
                    if (grupoTrabalho.Ativo)
                        break;
                    grupoTrabalho = null;
                }
                else
                    break;
            }

            return grupoTrabalho;
        }
    }
}
