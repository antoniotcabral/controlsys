﻿using Controlsys.Dominio.Parametros;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Persistencia;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// Summary description for TipoOcorrenciaTeste.
    /// </summary>
    [TestClass]
    public class TipoOcorrenciaTeste
    {
        /// <summary>
        /// Pode incluir.
        /// </summary>
        [TestMethod]
        public void Pode_Incluir()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();

            TipoOcorrenciaController controller = new TipoOcorrenciaController(unidTrabalho);
            TipoOcorrencia registro = new TipoOcorrencia
            {
                Descricao = "Teste"
            };
            var result = controller.Incluir(registro) as JsonResult;
            var registroGerado = JsonHelper.GetJsonObjectRepresentation<TipoOcorrencia>(result);

            Assert.IsNotNull(registro);
            Assert.AreEqual(registro.Descricao, registroGerado.Descricao);

            unidTrabalho.Rollback();
        }

        /// <summary>
        /// Pode alterar.
        /// </summary>
        [TestMethod]
        public void Pode_Alterar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();

            TipoOcorrenciaController controller = new TipoOcorrenciaController(unidTrabalho);

            #region [ObterTodos]
            var result1 = controller.ObterTodos() as JsonResult;
            var lista = JsonHelper.GetJsonObjectRepresentation<List<TipoOcorrencia>>(result1);

            Assert.IsNotNull(lista);
            Assert.IsTrue(lista.Count > 0);
            #endregion

            var registro = lista.Where(x => x.Ativo).OrderBy(x => new Random().Next()).FirstOrDefault();

            Assert.IsNotNull(registro, "Nenhum registro encontrado para ser utilizado como referência de teste no Alterar");

            registro.Descricao = new Random().Next().ToString();

            var result = controller.Alterar(registro) as JsonResult;
            var registroAlterado = JsonHelper.GetJsonObjectRepresentation<TipoOcorrencia>(result);

            Assert.IsNotNull(registro);
            Assert.AreEqual(registro.Descricao, registroAlterado.Descricao);

            unidTrabalho.Rollback();
        }

        /// <summary>
        /// 
        /// </summary>
        [TestMethod]
        public void AtivarInativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();

            TipoOcorrenciaController controller = new TipoOcorrenciaController(unidTrabalho);

            #region [ObterTodos]
            var result1 = controller.ObterTodos() as JsonResult;
            var lista = JsonHelper.GetJsonObjectRepresentation<List<TipoOcorrencia>>(result1);

            Assert.IsNotNull(lista);
            Assert.IsTrue(lista.Count > 0);
            #endregion

            var registro = lista.OrderBy(x => new Random().Next()).FirstOrDefault();

            Assert.IsNotNull(registro, "Nenhum registro encontrado para ser utilizado como referência de teste no AtivarInativar");

            var result = controller.AtivarInativar(registro.Codigo) as JsonResult;
            var registroAlterado = JsonHelper.GetJsonObjectRepresentation<TipoOcorrencia>(result);

            Assert.IsNotNull(registro, "Retornando registro nulo ao AtivarInativar");
            Assert.AreNotEqual(registro.Ativo, registroAlterado.Ativo, "Valor diferente do esperado.");

            unidTrabalho.Rollback();
        }

        /// <summary>
        /// 
        /// </summary>
        [TestMethod]
        public void Selecionar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            TipoOcorrenciaController controller = new TipoOcorrenciaController(unidTrabalho);

            #region [ObterTodos]
            var result1 = controller.ObterTodos() as JsonResult;
            var lista = JsonHelper.GetJsonObjectRepresentation<List<TipoOcorrencia>>(result1);

            Assert.IsNotNull(lista);
            Assert.IsTrue(lista.Count > 0);
            #endregion

            var registro = lista.OrderBy(x => new Random().Next()).FirstOrDefault();

            Assert.IsNotNull(registro, "Nenhum registro encontrado para ser utilizado como referência de teste no Selecionar");

            var result2 = controller.Selecionar(registro.Codigo) as JsonResult;
            var registroSelecionado = JsonHelper.GetJsonObjectRepresentation<TipoOcorrencia>(result2);

            Assert.IsNotNull(registroSelecionado, "Selecionar retornando nulo");
            Assert.AreEqual(registro.Descricao, registroSelecionado.Descricao, "Nome difere do valor utilizado como referência");
        }

        /// <summary>
        /// 
        /// </summary>
        [TestMethod]
        public void ObterTodos()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();

            TipoOcorrenciaController controller = new TipoOcorrenciaController(unidTrabalho);
            var result = controller.ObterTodos() as JsonResult;
            var lista = JsonHelper.GetJsonObjectRepresentation<List<TipoOcorrencia>>(result);

            Assert.IsNotNull(lista);
            Assert.IsTrue(lista.Count > 0);
        }

        /// <summary>
        /// Obter TipoOcorrencia.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// true to somente ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) TipoOcorrencia.
        /// </returns>
        public static TipoOcorrencia ObterTipoOcorrencia(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            TipoOcorrenciaController controller = new TipoOcorrenciaController(unidTrabalho);
            JsonResult result = controller.ObterTodos(somenteAtivos) as JsonResult;
            var Locais = JsonHelper.GetJsonObjectRepresentation<List<TipoOcorrencia>>(result);
            Assert.IsNotNull(Locais);

            Random rand = new Random();
            Locais = Locais.OrderBy(x => rand.Next()).ToList();

            TipoOcorrencia TipoOcorrenciaAux = null;
            foreach (var item in Locais)
            {
                result = controller.Selecionar(item.Codigo) as JsonResult;
                Assert.IsNotNull(result.Data);
                TipoOcorrenciaAux = JsonHelper.GetJsonObjectRepresentation<TipoOcorrencia>(result);

                if (somenteAtivos)
                {
                    if (TipoOcorrenciaAux.Ativo)
                        break;
                    TipoOcorrenciaAux = null;
                }
                else
                    break;
            }

            return TipoOcorrenciaAux;
        }

        /// <summary>
        /// Obter lista TipoOcorrencia.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// true to somente ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;TipoOcorrencia&gt;
        /// </returns>
        public static List<TipoOcorrencia> ObterListaTipoOcorrencia(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<TipoOcorrencia>(unidTrabalho);
            var Locais = repositorio.ObterTodos().ToList();
            Assert.IsTrue(Locais.Any());

            var rand = new Random();
            Locais = Locais.OrderBy(x => rand.Next()).ToList();

            return somenteAtivos ? Locais.Where(x => x.Ativo).ToList() : Locais;
        }
    }
}
