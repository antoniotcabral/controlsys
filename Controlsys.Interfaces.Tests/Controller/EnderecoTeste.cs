﻿using System;
using System.Linq;
using System.Web.Mvc;
using Controlsys.Dominio.Enderecos;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Controlsys.Persistencia;
using Globalsys;
using Globalsys.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) EnderecoTeste.
    /// </summary>
    [TestClass]
    public class EnderecoTeste
    {
        /// <summary>
        /// Verifica pesquisa a Logradouro.
        /// </summary>
        [TestMethod]
        public void Pode_Pesquisar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new EnderecoController(unidTrabalho);
            var repLogradouro = new Repositorio<Logradouro>(unidTrabalho);

            Random rand = new Random();
            List<Logradouro> listaLogradouro = repLogradouro.ObterTodos().Take(100).ToList();
            Assert.IsNotNull(listaLogradouro);
            Assert.IsTrue(listaLogradouro.Any());

            listaLogradouro = listaLogradouro.OrderBy(x => rand.Next()).ToList();
            var logradouroAux = listaLogradouro[0];
            Assert.IsNotNull(logradouroAux);

            var result = controller.Pesquisar(logradouroAux.CEP) as JsonResult;
            Assert.IsNotNull(result);

            var logradouro = JsonHelper.GetJsonObjectRepresentation<LogradouroModelView>(result);
            Assert.IsNotNull(logradouro);
            Assert.AreEqual(logradouroAux.Codigo, logradouro.CodigoLog);
        }

        /// <summary>
        /// Verifica pesquisa de Logradouro não cadastrado.
        /// </summary>
        [TestMethod]
        public void Valida_Pesquisar_Logradouro_Nao_Cadastrado()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new EnderecoController(unidTrabalho);
            var repLogradouro = new Repositorio<Logradouro>(unidTrabalho);

            var cep = Global.GerarCep();
            var listaLogradouro = repLogradouro.ObterTodos().Where(x => x.CEP == cep); ;
            while (listaLogradouro.Any())
            {
                cep = Global.GerarCep();
                listaLogradouro = repLogradouro.ObterTodos().Where(x => x.CEP == cep);
            }
            Assert.IsFalse(listaLogradouro.Any());

            try
            {
                var result = controller.Pesquisar(cep);
                Assert.Fail("Exceção esperada não lançada");
            }
            catch (CoreException ex)
            {
                StringAssert.Contains(ex.Message.ToLower(), "cep não existe");
            }
        }

        /// <summary>
        /// Buscar aleatóriamente um endereço já cadastrado na base de dados.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) Endereco.
        /// </returns>
        public static Endereco ObterEndereco(IUnidadeTrabalho unidTrabalho)
        {            
            var rep = new Repositorio<Endereco>(unidTrabalho);
            var enderecos = rep.ObterTodos()
                               .Where(e => e.Logradouro != null)
                               .OrderBy(x => x.Codigo);

            List<Endereco> ends = enderecos.ToList();

            Random rd = new Random();

            Endereco endereco = null;

            while (endereco == null)
            {
                int pos = rd.Next();
                if(pos < ends.Count)
                    endereco = ends.ElementAt(pos);    
            }
            
            return endereco;
        }

        /// <summary>
        /// Gerar um endereço novo, sem persistir na base de dados.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) Endereco.
        /// </returns>
        public static Endereco ObterEnderecoNovo()
        {
            var rand = new Random();
            var data = DateTime.Now.ToString("MM/dd HH:mm:ss.fff");

            // Descomentar para utilizar um logradouro já cadastrado;
            //
            //var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            //Repositorio<Logradouro> repLogradouro = new Repositorio<Logradouro>(unidTrabalho);
            //var logradouros = repLogradouro.ObterTodos();
            //Logradouro logradouro = logradouros.OrderBy(x => rand.Next()).ToList()[0];

            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repLogradouro = new Repositorio<Logradouro>(unidTrabalho);
            var logradourosAtivos = repLogradouro.ObterTodos().Where(x => x.Ativo).Take(100).ToList();
            Assert.IsTrue(logradourosAtivos.Any());

            logradourosAtivos = logradourosAtivos.OrderBy(x => rand.Next()).ToList();
            Assert.IsTrue(logradourosAtivos.Any());

            Logradouro logradouro = logradourosAtivos.OrderBy(x => rand.Next()).ToList()[0];
            //foreach (var item in logradourosAtivos.OrderByDescending(x=>rand.Next()))
            //{
            //    logradouro = item;
            //}

            //var logradouro = logradouros.First();
            Assert.IsNotNull(logradouro);

            Endereco endereco = new Endereco()
            {
                Numero = rand.Next(1, 999).ToString(),
                Complemento = "Complemento-" + data,
                CaixaPostal = rand.Next(1, 999).ToString(),
                Logradouro = logradouro
            };

            return endereco;
        }
    }
}
