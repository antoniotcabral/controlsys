﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Ext.Net;
using Ext.Net.MVC;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Controlsys.Interfaces.Tests.Controller
{
    [TestClass]
    public class RelatorioVeiculosTeste
    {
        /// <summary>
        /// Pode exportar PDF.
        /// </summary>
        [TestMethod]
        public void ExportarPDF()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RelatorioVeiculosController controller = new RelatorioVeiculosController(unidTrabalho);

            List<string> selectedFields = typeof(RelatorioVeiculosModelView).GetProperties().Select(pi => pi.Name).ToList();

            var result = controller.Exportar(selectedFields, "PDF") as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/pdf", result.ContentType);
            Assert.AreEqual("download.pdf", result.FileDownloadName);
        }

        /// <summary>
        /// Pode exportar excel.
        /// </summary>
        [TestMethod]
        public void ExportarExcel()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RelatorioVeiculosController controller = new RelatorioVeiculosController(unidTrabalho);

            List<string> selectedFields = typeof(RelatorioVeiculosModelView).GetProperties().Select(pi => pi.Name).ToList();

            var result = controller.Exportar(selectedFields, "Excel") as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/vnd.ms-excel", result.ContentType);
            Assert.AreEqual("download.xls", result.FileDownloadName);
        }

        /// <summary>
        /// Pode exportar word.
        /// </summary>
        [TestMethod]
        public void ExportarWord()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RelatorioVeiculosController controller = new RelatorioVeiculosController(unidTrabalho);

            List<string> selectedFields = typeof(RelatorioVeiculosModelView).GetProperties().Select(pi => pi.Name).ToList();

            var result = controller.Exportar(selectedFields, "Word") as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/msword", result.ContentType);
            Assert.AreEqual("download.doc", result.FileDownloadName);
        }

        /// <summary>
        /// Pode pesquisar.
        /// </summary>
        [TestMethod]
        public void Pesquisar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RelatorioVeiculosController controller = new RelatorioVeiculosController(unidTrabalho);

            var ListEmi = EmitirCredencialTeste.ObterListaCredencialVeiculos();
            Assert.IsNotNull(ListEmi);

            var emi = ListEmi.FirstOrDefault();
            var x = new Dictionary<string, object>() { };
            x.Add("start", (int)0);
            x.Add("limit", (int)25);

            var result = controller.Pesquisar(new StoreRequestParameters(x), emi.Empresa.Nome, emi.Condutor.PessoaFisica.Nome, 
                emi.Veiculos[0].Veiculo.Placa, emi.ModeloCredencial.Codigo) as StoreResult;

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Data as List<RelatorioVeiculosModelView>);
        }
    }
}
