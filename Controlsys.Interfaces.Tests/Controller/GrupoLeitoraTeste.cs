﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Parametros;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorios.Acesso;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) GrupoLeitoraTeste.
    /// </summary>
    [TestClass]
    public class GrupoLeitoraTeste
    {
        /// <summary>
        /// Pode incluir.
        /// </summary>
        [TestMethod]
        public void Pode_Incluir()
        {
            IUnidadeTrabalho unidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new GrupoLeitoraController(unidadeTrabalho);

            var data = DateTime.Now.ToString("MM/dd HH:mm:ss.fff");
            var nome = "Nome-" + data;
            var descricao = "Descrição-" + data;

            var grupoAux = new GrupoLeitora()
            {
                Nome = nome,
                DataRegistro = DateTime.Now
            };

            grupoAux.Ativar();

            List<GrupoLeitora> gruposLeitoras = new List<GrupoLeitora>();
            var grupoLeitora = GrupoLeitoraTeste.ObterGrupoLeitora(true);
            if (grupoLeitora != null)
                gruposLeitoras.Add(grupoLeitora);

            var resultLeit = new LeitoraController(unidadeTrabalho).ObterTodos(true) as JsonResult;
            var leitorasAux = JsonHelper.GetJsonObjectRepresentation<List<LeitoraModelView>>(resultLeit);
            var leitoras = leitorasAux.Select(l => new Leitora { Codigo = l.Codigo }).ToList();


            var resultTrein = new TreinamentoController(unidadeTrabalho).ObterTodos(true) as JsonResult;
            var treinamentos = JsonHelper.GetJsonObjectRepresentation<List<Treinamento>>(resultTrein);

            controller.Incluir(grupoAux, leitoras, treinamentos, gruposLeitoras);
        }

        /// <summary>
        /// Pode alterar.
        /// </summary>
        [TestMethod]
        public void Pode_Alterar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            GrupoLeitoraController controller = new GrupoLeitoraController(unidTrabalho);
            GrupoLeitora grupoAux = ObterGrupoLeitora(true);
            Assert.IsNotNull(grupoAux);
            Assert.IsTrue(grupoAux.Ativo);

            var data = DateTime.Now.ToString("MM/dd HH:mm:ss.fff");
            var nome = "Nome-" + data;
            grupoAux.Nome = nome;

            int[] gruposLeitora = null;
            var grupoLeitora = GrupoLeitoraTeste.ObterGrupoLeitora(true);
            if (grupoLeitora != null)
                gruposLeitora = new int[] { grupoLeitora.Codigo };

            var leitoraResult = new LeitoraController(unidTrabalho).ObterTodos(true) as JsonResult;
            var leitora = JsonHelper.GetJsonObjectRepresentation<List<LeitoraModelView>>(leitoraResult).FirstOrDefault();

            var leitoras = new int[] { leitora.Codigo };

            var treinamento = TreinamentoTeste.ObterTreinamento(true);
            Assert.IsNotNull(treinamento);
            var treinamentos = new int[] { treinamento.Codigo };

            var result = controller.Alterar(grupoAux, leitoras, null, treinamentos, null, gruposLeitora, null) as JsonResult;
            Assert.IsNotNull(result);

            var grupo = JsonHelper.GetJsonObjectRepresentation<GrupoLeitora>(result);
            Assert.AreEqual(nome, grupo.Nome);
        }

        /// <summary>
        /// Pode inativar.
        /// </summary>
        [TestMethod]
        public void Pode_Inativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new GrupoLeitoraController(unidTrabalho);
            var repGrupoLeitora = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoLeitora>(unidTrabalho);

            var grupoLeitora = repGrupoLeitora.ObterTodos()
                                              .Where(gl => gl.Ativo)
                                              .OrderBy(gl => gl.Codigo)
                                              .FirstOrDefault();

            Assert.IsTrue(grupoLeitora.Ativo);

            var result = controller.AtivarInativar(grupoLeitora.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var grupo = JsonHelper.GetJsonObjectRepresentation<GrupoModelView>(result);
            Assert.IsNotNull(grupo);
            Assert.AreEqual(grupoLeitora.Codigo, grupo.Codigo);
            Assert.IsFalse(grupo.Ativo);
        }

        /// <summary>
        /// Pode ativar.
        /// </summary>
        [TestMethod]
        public void Pode_Ativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new GrupoLeitoraController(unidTrabalho);
            var repGrupoLeitora = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoLeitora>(unidTrabalho);

            var grupoLeitora = repGrupoLeitora.ObterTodos()
                                              .FirstOrDefault();

            if (grupoLeitora.Ativo)
            {
                grupoLeitora.Inativar();
                repGrupoLeitora.Atualizar(grupoLeitora);
            }

            Assert.IsNotNull(grupoLeitora);
            Assert.IsFalse(grupoLeitora.Ativo);

            var result = controller.AtivarInativar(grupoLeitora.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var grupo = JsonHelper.GetJsonObjectRepresentation<GrupoLeitora>(result);
            Assert.IsNotNull(grupo);
            Assert.AreEqual(grupoLeitora.Codigo, grupo.Codigo);
            Assert.IsTrue(grupo.Ativo);
        }

        /// <summary>
        /// Busca aleatóriamente um GrupoLeitora cadastrado na base de dados.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// Filtrar GruposLeitora ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) GrupoLeitora.
        /// </returns>
        internal static GrupoLeitora ObterGrupoLeitora(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            GrupoLeitoraController controller = new GrupoLeitoraController(unidTrabalho);
            JsonResult result = controller.ObterTodos() as JsonResult;
            Assert.IsNotNull(result.Data);

            Random rand = new Random();
            var grupoLeitorasLeitora = JsonHelper.GetJsonObjectRepresentation<List<GrupoLeitora>>(result);
            grupoLeitorasLeitora = grupoLeitorasLeitora.OrderBy(x => rand.Next()).ToList();

            GrupoLeitora grupoLeitora = null;
            foreach (var item in grupoLeitorasLeitora)
            {
                grupoLeitora = item;
                if (somenteAtivos)
                {
                    if (grupoLeitora.Ativo)
                        break;
                    grupoLeitora = null;
                }
                else
                    break;
            }

            return grupoLeitora;
        }


        #region Testes específicos


        [TestMethod]
        public void LeitoraCheckBoxUsaAntipassback()
        {
            bool antiPassbackEsperado = true;
            string gLeitora = "ACESSO_PORTO";
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new GrupoLeitoraController(unidTrabalho);
            var repGrupoLeitora = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoLeitora>(unidTrabalho);

            var grupoLeitora = repGrupoLeitora.ObterTodos().Where(g => g.Ativo == true && g.Nome == gLeitora).FirstOrDefault();
            Assert.IsNotNull(grupoLeitora);

            bool usaAntipassback = grupoLeitora.AntiPassBack;
            if (antiPassbackEsperado) Assert.IsTrue(usaAntipassback);
            else Assert.IsFalse(usaAntipassback);

        }


        #endregion
    }
}
