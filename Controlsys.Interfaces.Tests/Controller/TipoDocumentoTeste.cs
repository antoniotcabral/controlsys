﻿using System;
using System.Collections.Generic;
using System.Linq;
using Controlsys.Dominio.Parametros;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Tests.Properties;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using Controlsys.Persistencia;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) TipoDocumentoTeste.
    /// </summary>
    [TestClass]
    public class TipoDocumentoTeste
    {
        /// <summary>
        /// Valida incluir um novo TipoDocumento, informando Nome, Bloqueio, DiasValidade e Sigla.
        /// </summary>
        [TestMethod]
        public void Pode_Incluir()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new TipoDocumentoController(unidTrabalho);

            var rand = new Random();
            var dt = DateTime.Now;
            var data = dt.ToString("MM/dd HH:mm:ss.fff");
            var nome = "Nome-" + data;
            var bloqueio = Global.ObterBoolean();
            //var diasValid = rand.Next(1, 365);
            var sigla = "EX" + rand.Next(1, 999);

            var tipoDocumentoAux = new TipoDocumento
            {
                Nome = nome,
                Bloqueio = bloqueio,
                //DiasValidade = diasValid,
                Sigla = sigla
            };

            var result = controller.Incluir(tipoDocumentoAux) as JsonResult;
            Assert.IsNotNull(result);

            var tipoDocumento = result.Data as TipoDocumento;
            Assert.IsNotNull(tipoDocumento);
            Assert.AreEqual(nome, tipoDocumento.Nome);
            Assert.AreEqual(bloqueio, tipoDocumento.Bloqueio);
            //Assert.AreEqual(diasValid, tipoDocumento.DiasValidade);
            Assert.AreEqual(sigla, tipoDocumento.Sigla);
        }

        /// <summary>
        /// Valida alterar um TipoDocumento, modificando Nome, Bloqueio, DiasValidade e Sigla.
        /// </summary>
        [TestMethod]
        public void Pode_Alterar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new TipoDocumentoController(unidTrabalho);
            var tipoDocumentoAux = ObterTipoDocumento(true);
            Assert.IsNotNull(tipoDocumentoAux);
            Assert.IsTrue(tipoDocumentoAux.Ativo);

            var rand = new Random();
            var dt = DateTime.Now;
            var data = dt.ToString("MM/dd HH:mm:ss.fff");
            var nome = "Nome-" + data;
            var bloqueio = Global.ObterBoolean();
            var diasValid = rand.Next(1, 365);
            var sigla = "Sigla-" + data;

            tipoDocumentoAux.Nome = nome;
            tipoDocumentoAux.Bloqueio = bloqueio;
            //tipoDocumentoAux.DiasValidade = diasValid;
            tipoDocumentoAux.Sigla = sigla;

            var result = controller.Alterar(tipoDocumentoAux) as JsonResult;
            Assert.IsNotNull(result.Data);

            var tipoDocumento = JsonHelper.GetJsonObjectRepresentation<TipoDocumento>(result);
            Assert.AreEqual(nome, tipoDocumento.Nome);
            Assert.AreEqual(bloqueio, tipoDocumento.Bloqueio);
            //Assert.AreEqual(diasValid, tipoDocumento.DiasValidade);
            Assert.AreEqual(sigla, tipoDocumento.Sigla);
        }

        #region [Desativado]
        /*
        /// <summary>
        /// Valida ativação ou inativação de TipoDocumento, conforme status corrente;
        /// </summary>
        [TestMethod]
        public void Pode_AtivarInativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new TipoDocumentoController(unidTrabalho);
            var tipoDocumentoAux = ObterTipoDocumento();
            Assert.IsNotNull(tipoDocumentoAux);

            var status = !tipoDocumentoAux.Ativo;
            var result = controller.AtivarInativar(tipoDocumentoAux.Codigo) as JsonResult;
            Assert.IsNotNull(result.Data);

            var tipoDocumento = JsonHelper.GetJsonObjectRepresentation<TipoDocumento>(result);
            Assert.AreEqual(status, tipoDocumento.Ativo);
        }

        /// <summary>
        /// Busca aleatóriamente um TipoDocumento cadastrado na base de dados
        /// </summary>
        /// <param name="somenteAtivos">Filtrar TipoDocumento ativos</param>
        /// <returns></returns>
        public static TipoDocumento ObterTipoDocumento(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new TipoDocumentoController(unidTrabalho);
            var result = controller.ObterTodos() as JsonResult;
            var tiposDocumento = JsonHelper.GetJsonObjectRepresentation<List<TipoDocumento>>(result);
            Assert.IsNotNull(tiposDocumento);

            var rand = new Random();
            tiposDocumento = tiposDocumento.OrderBy(x => rand.Next()).ToList();

            TipoDocumento tipoDocumentoAux = null;
            foreach (var item in tiposDocumento)
            {
                result = controller.Selecionar(item.Codigo) as JsonResult;
                Assert.IsNotNull(result.Data);

                tipoDocumentoAux = JsonHelper.GetJsonObjectRepresentation<TipoDocumento>(result);

                //tipoDocumentoAux = (controller.Selecionar(item.Codigo) as JsonResult).Data as TipoDocumento;
                //if (tipoDocumentoAux == null)
                //    continue;
                if (somenteAtivos)
                {
                    if (tipoDocumentoAux.Ativo)
                        break;
                    tipoDocumentoAux = null;
                }
                else
                    break;
            }

            return tipoDocumentoAux;
        }
        */

        #endregion

        /// <summary>
        /// Valida Ativar TipoDocumento.
        /// </summary>
        [TestMethod]
        public void Pode_Ativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new TipoDocumentoController(unidTrabalho);
            var lista = ObterListaTipoDocumento();
            Assert.IsTrue(lista.Any(), Resources.Lista_vazia);

            var tipoDocumentoAux = lista.First(x => x.Ativo == false);
            Assert.IsNotNull(tipoDocumentoAux);
            Assert.IsFalse(tipoDocumentoAux.Ativo);

            var result = controller.AtivarInativar(tipoDocumentoAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var tipoDocumento = result.Data as TipoDocumento;
            Assert.IsNotNull(tipoDocumento);
            Assert.AreEqual(tipoDocumentoAux.Codigo, tipoDocumento.Codigo);
            Assert.IsTrue(tipoDocumento.Ativo);
        }

        /// <summary>
        /// Valida Inativar TipoDocumento.
        /// </summary>
        [TestMethod]
        public void Pode_Inativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new TipoDocumentoController(unidTrabalho);
            var tipoDocumentoAux = ObterTipoDocumento(true);
            Assert.IsTrue(tipoDocumentoAux.Ativo);

            var result = controller.AtivarInativar(tipoDocumentoAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var tipoDocumento = result.Data as TipoDocumento;
            Assert.IsNotNull(tipoDocumento);
            Assert.AreEqual(tipoDocumentoAux.Codigo, tipoDocumento.Codigo);
            Assert.IsFalse(tipoDocumento.Ativo);
        }

        /// <summary>
        /// Busca aleatóriamente um TipoDocumento cadastrado na base de dados.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// Filtrar TipoDocumentos ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) TipoDocumento.
        /// </returns>
        public static TipoDocumento ObterTipoDocumento(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<TipoDocumento>(unidTrabalho);
            var listaTipoDocumento = repositorio.ObterTodos().ToList();
            Assert.IsTrue(listaTipoDocumento.Any());

            var rand = new Random();
            listaTipoDocumento = listaTipoDocumento.OrderBy(x => rand.Next()).ToList();

            return listaTipoDocumento.First(x => !somenteAtivos || x.Ativo);
        }

        /// <summary>
        /// Obter lista tipo documento.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// Filtrar TipoDocumentos ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;TipoDocumento&gt;
        /// </returns>
        public static List<TipoDocumento> ObterListaTipoDocumento(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<TipoDocumento>(unidTrabalho);
            var listaTipoDocumento = repositorio.ObterTodos().ToList();
            Assert.IsTrue(listaTipoDocumento.Any());

            var rand = new Random();
            listaTipoDocumento = listaTipoDocumento.OrderBy(x => rand.Next()).ToList();

            return somenteAtivos ? listaTipoDocumento.Where(x => x.Ativo).ToList() : listaTipoDocumento;
        }

    }
}
