﻿using Controlsys.Dominio.Ocorrencias;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Seguranca;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Controlsys.Persistencia;
using Controlsys.Repositorios.Seguranca;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// Summary description for OcorrenciaSegurancaTeste.
    /// </summary>
    [TestClass]
    public class OcorrenciaSegurancaTeste
    {
        CultureInfo cultbr = new CultureInfo("pt-BR");
        /// <summary>
        /// Pode incluir.
        /// </summary>
        [TestMethod]
        public void Pode_Incluir()
        {
            var rand = new Random();
            var randValue = rand.Next(1, 9999);            

            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();           

            #region [fakeContext]
            var fakeHttpContext = new Mock<HttpContextBase>();
            var fakeIdentity = new GenericIdentity("daniel.cunha");
            var principal = new GenericPrincipal(fakeIdentity, null);

            fakeHttpContext.Setup(t => t.User).Returns(principal);
            var controllerContext = new Mock<ControllerContext>();
            controllerContext.Setup(t => t.HttpContext).Returns(fakeHttpContext.Object);

            OcorrenciaSegurancaController controller = new OcorrenciaSegurancaController(unidTrabalho);

            controller.ControllerContext = controllerContext.Object;
            #endregion

            unidTrabalho.BeginTransaction();

            IRepositorioUsuario repUsu = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(unidTrabalho);
            Usuario usuariLogado = repUsu.ObterPorLogin(fakeIdentity.Name);

            OcorrenciaSeguranca registro = new OcorrenciaSeguranca
            {
                AcoesRecomendadas = "Teste",
                Ativo = true,
                DataOcorrencia = DateTime.Now,
                Descricao = "Teste descrição",
                Efetiva = true,
                Gravidade = Global.ObterEnumAleatorio<GravidadeOcorrencia>(),
                HoraOcorrencia = new TimeSpan(10,10,00),
                Local = new Local()
                {
                    Codigo = 1,
                    Nome = "Teste",
                    Sigla = "T"
                },
                Supervisor = usuariLogado,
                Procedimentos = "Teste Procedimentos",
                Tentativa = true,
                TipoOcorrencia = new TipoOcorrencia()
                {
                    Codigo = 1,
                    Descricao = "Teste"
                },
                Vulnerabilidade = true
            };

            GraduacaoOcorrencia[] graduacoes = new GraduacaoOcorrencia[] { };

            var result = controller.Incluir(registro, graduacoes, null, null,null) as JsonResult;
            Assert.IsNotNull(result);

            var registroGerado = JsonHelper.GetJsonObjectRepresentation<OcorrenciaSegurancaModelView>(result);

            Assert.IsNotNull(registro);
            Assert.AreEqual(registro.AcoesRecomendadas, registroGerado.AcoesRecomendadas);
            Assert.AreEqual(registro.Ativo, registroGerado.Ativo);
            Assert.AreEqual(registro.DataOcorrencia.ToString("dd/MM/yyyy", cultbr), registroGerado.DataOcorrencia.ToString("dd/MM/yyyy", cultbr));
            Assert.AreEqual(registro.Descricao, registroGerado.Descricao);
            Assert.AreEqual(registro.Efetiva, registroGerado.Efetiva);
            Assert.AreEqual(registro.Gravidade.ObterDescricaoEnum(), registroGerado.Gravidade);
            Assert.AreEqual(registro.HoraOcorrencia.ToString("hh\\:mm"), registroGerado.HoraOcorrencia);
            Assert.AreEqual(registro.Local.Sigla + " - " + registro.Local.Nome, registroGerado.Local);
            Assert.AreEqual(registro.Procedimentos, registroGerado.Procedimentos);
            Assert.AreEqual(registro.Supervisor.Login, registroGerado.Supervisor);
            Assert.AreEqual(registro.Tentativa, registroGerado.Tentativa);
            Assert.AreEqual(registro.TipoOcorrencia.Descricao, registroGerado.TipoOcorrencia);
            Assert.AreEqual(registro.Vulnerabilidade, registroGerado.Vulnerabilidade);

            unidTrabalho.Rollback();
        }

        /// <summary>
        /// Pode alterar.
        /// </summary>
        [TestMethod]
        public void Pode_Alterar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();

            #region [fakeContext]
            var fakeHttpContext = new Mock<HttpContextBase>();
            var fakeIdentity = new GenericIdentity("daniel.cunha");
            var principal = new GenericPrincipal(fakeIdentity, null);

            fakeHttpContext.Setup(t => t.User).Returns(principal);
            var controllerContext = new Mock<ControllerContext>();
            controllerContext.Setup(t => t.HttpContext).Returns(fakeHttpContext.Object);

            OcorrenciaSegurancaController controller = new OcorrenciaSegurancaController(unidTrabalho);
            controller.ControllerContext = controllerContext.Object;
            #endregion

            IRepositorioUsuario repUsu = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(unidTrabalho);
            Usuario usuariLogado = repUsu.ObterPorLogin(fakeIdentity.Name);

            unidTrabalho.BeginTransaction();

            var repositorio = new Repositorio<OcorrenciaSeguranca>(unidTrabalho);
            var ocorrencias = repositorio.ObterTodos();

            OcorrenciaSeguranca ocorrenciaAux = ocorrencias.ToList()[0];

            ocorrenciaAux.Descricao = new Random().Next().ToString();
            ocorrenciaAux.AcoesRecomendadas = "Teste2";
            ocorrenciaAux.Ativo = true;
            ocorrenciaAux.DataOcorrencia = DateTime.Now;
            ocorrenciaAux.Descricao = "Teste2 descrição";
            ocorrenciaAux.Efetiva = true;
            ocorrenciaAux.Gravidade = Global.ObterEnumAleatorio<GravidadeOcorrencia>();
            ocorrenciaAux.HoraOcorrencia = new TimeSpan(10, 10, 00);
            ocorrenciaAux.Local = new Local()
            {
                Codigo = 1,
                Nome = "Teste2",
                Sigla = "T2"
            };
            ocorrenciaAux.Supervisor = usuariLogado;
            ocorrenciaAux.Procedimentos = "Teste2 Procedimentos";
            ocorrenciaAux.Tentativa = true;
            ocorrenciaAux.TipoOcorrencia = new TipoOcorrencia()
            {
                Codigo = 1,
                Descricao = "Teste2"
            };
            ocorrenciaAux.Vulnerabilidade = true;

            GraduacaoOcorrencia[] graduacoes = new GraduacaoOcorrencia[] { };

            var result = controller.Alterar(ocorrenciaAux, graduacoes, null, null, null) as JsonResult;
            Assert.IsNotNull(result);

            var ocorrenciaA = JsonHelper.GetJsonObjectRepresentation<OcorrenciaSegurancaModelView>(result);
            Assert.IsNotNull(ocorrenciaAux);
            Assert.AreEqual(ocorrenciaAux.AcoesRecomendadas, ocorrenciaA.AcoesRecomendadas);
            Assert.AreEqual(ocorrenciaAux.Ativo, ocorrenciaA.Ativo);
            Assert.AreEqual(ocorrenciaAux.DataOcorrencia.ToString("dd/MM/yyyy", cultbr), ocorrenciaA.DataOcorrencia.ToString("dd/MM/yyyy", cultbr));
            Assert.AreEqual(ocorrenciaAux.Descricao, ocorrenciaA.Descricao);
            Assert.AreEqual(ocorrenciaAux.Efetiva, ocorrenciaA.Efetiva);
            Assert.AreEqual(ocorrenciaAux.Gravidade.ObterDescricaoEnum(), ocorrenciaA.Gravidade);
            Assert.AreEqual(ocorrenciaAux.HoraOcorrencia.ToString("hh\\:mm"), ocorrenciaA.HoraOcorrencia);
            Assert.AreEqual((ocorrenciaAux.Local != null ? ocorrenciaAux.Local.Sigla.ToString() + " - " + ocorrenciaAux.Local.Nome.ToString() : ""), ocorrenciaA.Local);
            Assert.AreEqual(ocorrenciaAux.Procedimentos, ocorrenciaA.Procedimentos);
            Assert.AreEqual(ocorrenciaAux.Supervisor.Login, ocorrenciaA.Supervisor);
            Assert.AreEqual(ocorrenciaAux.Tentativa, ocorrenciaA.Tentativa);
            Assert.AreEqual((ocorrenciaAux.TipoOcorrencia != null ? ocorrenciaAux.TipoOcorrencia.Descricao.ToString() : ""), ocorrenciaA.TipoOcorrencia);
            Assert.AreEqual(ocorrenciaAux.Vulnerabilidade, ocorrenciaA.Vulnerabilidade);

            unidTrabalho.Rollback();
        }
       
        /// <summary>
        /// 
        /// </summary>
        [TestMethod]
        public void Selecionar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            OcorrenciaSegurancaController controller = new OcorrenciaSegurancaController(unidTrabalho);

            #region [ObterTodos]
            var result1 = controller.ObterTodos() as JsonResult;
            var lista = JsonHelper.GetJsonObjectRepresentation<List<OcorrenciaSegurancaModelView>>(result1);

            Assert.IsNotNull(lista);
            Assert.IsTrue(lista.Count > 0);
            #endregion

            var registro = lista.OrderBy(x => new Random().Next()).FirstOrDefault();

            Assert.IsNotNull(registro, "Nenhum registro encontrado para ser utilizado como referência de teste no Selecionar");

            var result2 = controller.Selecionar(registro.Codigo) as JsonResult;
            var registroSelecionado = JsonHelper.GetJsonObjectRepresentation<OcorrenciaSegurancaModelView>(result2);

            Assert.IsNotNull(registroSelecionado, "Selecionar retornando nulo");
            Assert.AreEqual(registro.Descricao, registroSelecionado.Descricao, "Nome difere do valor utilizado como referência");
        }

        /// <summary>
        /// 
        /// </summary>
        [TestMethod]
        public void ObterTodos()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();

            OcorrenciaSegurancaController controller = new OcorrenciaSegurancaController(unidTrabalho);
            var result = controller.ObterTodos() as JsonResult;
            var lista = JsonHelper.GetJsonObjectRepresentation<List<OcorrenciaSegurancaModelView>>(result);

            Assert.IsNotNull(lista);
            Assert.IsTrue(lista.Count > 0);
        }

        public static OcorrenciaSeguranca ObterOcorrencia(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<OcorrenciaSeguranca>(unidTrabalho);
            var ocorrencias = repositorio.ObterTodos().ToList();
            Assert.IsTrue(ocorrencias.Any());

            Random rand = new Random();
            ocorrencias = ocorrencias.OrderBy(x => rand.Next()).ToList();

            OcorrenciaSeguranca ocorrencia = null;
            foreach (var item in ocorrencias)
            {
                ocorrencia = item;

                if (ocorrencia == null)
                    continue;
                if (somenteAtivos)
                {
                    if (ocorrencia.Ativo)
                        break;
                    ocorrencia = null;
                }
                else
                    break;
            }

            return ocorrencia;
        }
    }
}
