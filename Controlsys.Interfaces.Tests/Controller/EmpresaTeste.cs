﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using System.Web.Mvc;
using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Enderecos;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Tests.Properties;
using Controlsys.Interfaces.Models;
using Controlsys.Persistencia;
using Globalsys;
using Globalsys.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) EmpresaTeste.
    /// </summary>
    [TestClass]
    public class EmpresaTeste
    {
        /// <summary>
        /// Pode incluir.
        /// </summary>
        [TestMethod]
        public void Pode_Incluir()
        {
            #region [Empresa]
            var data = DateTime.Now.ToString("MM/dd HH:mm:ss.fff");
            var nome = "Nome-" + data;
            var apelido = "Apelido";
            var email = Global.GerarEmail();
            var endereco = EnderecoTeste.ObterEnderecoNovo();
            var cnpj = Global.GerarCnpj();
            var ie = "IE-" + data;
            var im = "IM-" + data;


            var empresaAux = new Empresa()
            {
                // Pessoa
                Nome = nome,
                Apelido = apelido,
                Email = email,
                Endereco = endereco,

                // Empresa
                CNPJ = cnpj,
                InscEstadual = ie,
                InscMunicipal = im
                //,
                //NaturezaJuridica = nj
            };
            #endregion

            #region [Lista Documentos]
            var tipoDocumento = TipoDocumentoTeste.ObterTipoDocumento(true);
            Assert.IsNotNull(tipoDocumento);
            var extensao = "Extens" + data;
            var descricao = "Descrição";
            var nomeDoc = "NomDoc-" + data;
            var dataValid = DateTime.Now.AddDays(300);
            empresaAux.Documentos = new List<DocumentoEmpresa>()
                {
                    new DocumentoEmpresa()
                    {
                        Documento = new Documento()
                        {
                            TipoDocumento = tipoDocumento,
                            //Extensao = extensao,
                            Descricao = descricao,
                            NomeDocumento = nomeDoc,
                            DataValidade = dataValid,
                            Status = Global.ObterEnumAleatorio<StatusDocumento>()
                        },
                        Empresa = empresaAux
                    }
                };
            List<DocumentoMidiaModelView> documentos = new List<DocumentoMidiaModelView>();

            #endregion

            #region [Lista Telefones]
            Random rand = new Random();
            empresaAux.Telefones = new Telefone[]
            {
                new Telefone()
                {
                    TipoTelefone = TipoTelefone.Residencial,
                    TelefoneNum = Global.GerarTelefone(TipoTelefone.Residencial),
                    Pessoa = empresaAux
                },
                new Telefone()
                {
                    TipoTelefone = TipoTelefone.Emergencial,
                    TelefoneNum = Global.GerarTelefone(TipoTelefone.Emergencial),
                    Pessoa = empresaAux
                },
                new Telefone()
                {
                    TipoTelefone = TipoTelefone.Celular,
                    TelefoneNum = Global.GerarTelefone(TipoTelefone.Celular),
                    Pessoa = empresaAux
                }
            };
            #endregion

            #region [Lista CNAEs]
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            EmpresaController empresaController = new EmpresaController(unidTrabalho);
            var result = empresaController.ObterTodosCNAE() as JsonResult;
            Assert.IsNotNull(result.Data);
            var cnaesAux = JsonHelper.GetJsonObjectRepresentation<List<CNAE>>(result);
            var cnae = cnaesAux.OrderBy(x => rand.Next()).ToList()[0];
            empresaAux.CNAEs = new EmpresaCNAE[]
            {
                new EmpresaCNAE()
                {
                    CNAE = cnae,
                    Empresa = empresaAux
                }
            };
            var cnaes = new List<CNAE>()
            {
                empresaAux.CNAEs[0].CNAE
            };
            #endregion

            result = empresaController.Incluir(empresaAux, cnaes, documentos, null) as JsonResult;
            Assert.IsNotNull(result);

            var empresa = JsonHelper.GetJsonObjectRepresentation<EmpresaModelView>(result);
            Assert.IsNotNull(empresa);
            Assert.AreEqual(nome, empresa.Nome);
            Assert.AreEqual(apelido, empresa.Apelido);
            Assert.AreEqual(email, empresa.Email);
            Assert.AreEqual(cnpj, empresa.CNPJ);
            Assert.AreEqual(ie, empresa.InscEstadual);
            Assert.AreEqual(im, empresa.InscMunicipal);
            //Assert.AreEqual(nj, empresa.NaturezaJuridica);

            //var repEndereco = new Repositorio<Endereco>(unidTrabalho);
            //result = repEndereco.ObterTodos() as JsonResult;
            //var enderecoAux = JsonHelper.GetJsonObjectRepresentation<Endereco>(result);

            //Assert.IsNotNull(empresa.Endereco);
            //Assert.AreEqual(endereco.Numero, empresa.Endereco.Numero);
            //Assert.AreEqual(endereco.Complemento, empresa.Endereco.Complemento);
            //Assert.AreEqual(endereco.CaixaPostal, empresa.Endereco.CaixaPostal);

            // Tratar logradouro, Bairro,....

        }

        /// <summary>
        /// Pode alterar.
        /// </summary>
        [TestMethod]
        public void Pode_Alterar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            EmpresaController controller = new EmpresaController(unidTrabalho);

            var repositorio = new Repositorio<Empresa>(unidTrabalho);
            var empresas = repositorio.ObterTodos();
            Assert.IsTrue(empresas.Any(), Resources.EmpresaTeste_lista_vazia);

            var empresasAtivas = empresas.Where(x => x.Ativo).ToList();
            Assert.IsTrue(empresasAtivas.Any(), Resources.EmpresaTeste_lista_inativa_vazia);

            var rand = new Random();
            empresasAtivas = new List<Empresa>(empresasAtivas.OrderBy(x => rand.Next()));

            Empresa empresaAux = empresasAtivas.ToList()[0];

            var data = DateTime.Now.ToString("MM/dd HH:mm:ss.fff");
            var nome = "Nome-" + data;
            var apelido = "Apelido";
            var cnpj = Global.GerarCnpj(true);
            var ie = "IE-" + data;
            var im = "IM-" + data;
            var email = "nome@dominio.com.br";
            var endereco = EnderecoTeste.ObterEnderecoNovo();

            Repositorio<CNAE> repCNAE = new Repositorio<CNAE>(unidTrabalho);
            CNAE[] cnaes = repCNAE.ObterTodos().ToArray();
            Assert.IsNotNull(cnaes);
            cnaes = cnaes.OrderBy(x => rand.Next()).ToArray();
            cnaes = new CNAE[] { cnaes[0] };

            Repositorio<Documento> repDocumento = new Repositorio<Documento>(unidTrabalho);
            var documentosAux = repDocumento.ObterTodos();
            Assert.IsTrue(documentosAux.Any());
            DocumentoMidiaModelView[] documentos = new List<DocumentoMidiaModelView>().ToArray();
            Assert.IsNotNull(documentos);
            documentos = documentos.OrderBy(x => rand.Next()).ToArray();

            Repositorio<Telefone> repTelefone = new Repositorio<Telefone>(unidTrabalho);
            Telefone[] telefones = repTelefone.ObterTodos().Where(x => x.Pessoa.Codigo == empresaAux.Codigo).ToArray();
            Assert.IsNotNull(telefones);
            telefones = telefones.OrderBy(x => rand.Next()).ToArray();

            empresaAux.Nome = nome;
            empresaAux.Apelido = apelido;
            empresaAux.CNPJ = cnpj;
            empresaAux.InscEstadual = ie;
            empresaAux.InscMunicipal = im;
            empresaAux.Email = email;
            //empresaAux.NaturezaJuridica = nj;
            empresaAux.Telefones = telefones;
            empresaAux.Endereco = endereco;

            var result = controller.Alterar(empresaAux, null, cnaes, documentos.ToList(), null) as JsonResult;
            Assert.IsNotNull(result);

            var empresa = JsonHelper.GetJsonObjectRepresentation<EmpresaModelView>(result);
            Assert.IsNotNull(empresa);
            Assert.AreEqual(empresaAux.Codigo, empresa.Codigo);
            Assert.AreEqual(nome, empresa.Nome);
            Assert.AreEqual(apelido, empresa.Apelido);
            Assert.AreEqual(empresaAux.CNPJFormatado(), empresa.CNPJ);
            Assert.AreEqual(ie, empresa.InscEstadual);
            Assert.AreEqual(im, empresa.InscMunicipal);
            Assert.AreEqual(email, empresa.Email);
            //Assert.AreEqual(nj, empresa.NaturezaJuridica);

            // Endereço...
            //Assert.IsNotNull(empresa.Endereco);
            //Assert.AreEqual(endereco.Numero, empresa.Endereco.Numero);
        }

        /// <summary>
        /// Pode ativar.
        /// </summary>
        [TestMethod]
        public void Pode_Ativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            EmpresaController controller = new EmpresaController(unidTrabalho);

            var repositorio = new Repositorio<Empresa>(unidTrabalho);
            var empresasAux = repositorio.ObterTodos();
            Assert.IsTrue(empresasAux.Any(), Resources.EmpresaTeste_lista_vazia);

            var empresas = empresasAux.Where(x => x.Ativo == false).ToList();
            Assert.IsTrue(empresas.Any(), Resources.EmpresaTeste_lista_inativa_vazia);

            var rand = new Random();
            empresas = new List<Empresa>(empresas.OrderBy(x => rand.Next()));

            Empresa empresaAux = empresas.ToList()[0];
            Assert.IsNotNull(empresaAux);
            Assert.IsFalse(empresaAux.Ativo);

            var result = controller.AtivarInativar(empresaAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var empresa = JsonHelper.GetJsonObjectRepresentation<EmpresaModelView>(result);
            Assert.IsNotNull(empresa);
            Assert.AreEqual(empresaAux.Codigo, empresa.Codigo);
            Assert.IsTrue(empresa.Ativo);
        }

        /// <summary>
        /// Pode inativar.
        /// </summary>
        [TestMethod]
        public void Pode_Inativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            EmpresaController controller = new EmpresaController(unidTrabalho);
            EmpresaModelView empresaAux = ObterEmpresaModelView(true);
            Assert.IsNotNull(empresaAux);
            Assert.IsTrue(empresaAux.Ativo);

            try
            {
                var result = controller.AtivarInativar(empresaAux.Codigo) as JsonResult;
                Assert.IsNotNull(result);

                var empresa = JsonHelper.GetJsonObjectRepresentation<EmpresaModelView>(result);
                Assert.IsNotNull(empresa);
                Assert.AreEqual(empresaAux.Codigo, empresa.Codigo);
                Assert.IsFalse(empresa.Ativo);
            }
            catch (CoreException ex)
            {
                Assert.IsTrue(ex.Message.Contains("Não é possível inativar, a empresa tem Pedido de Compra (PO) ativo"));
            }
        }

        /// <summary>
        /// Busca aleatóriamente uma empresa cadastrada na base de dados.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// Filtrar empresas ativas.
        /// </param>
        ///
        /// <returns>
        /// Um(a) EmpresaModelView.
        /// </returns>
        public static EmpresaModelView ObterEmpresaModelView(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            EmpresaController controller = new EmpresaController(unidTrabalho);
            JsonResult result = controller.ObterTodos() as JsonResult;
            Assert.IsNotNull(result.Data);

            Random rand = new Random();
            var empresas = JsonHelper.GetJsonObjectRepresentation<List<EmpresaModelView>>(result);
            empresas = empresas.OrderBy(x => rand.Next()).ToList();

            EmpresaModelView empresaAux = null;
            foreach (var item in empresas)
            {
                //result = controller.Selecionar(item.Codigo) as JsonResult;
                //Assert.IsNotNull(result.Data);
                //empresaAux = JsonHelper.GetJsonObjectRepresentation<EmpresaModelView>(result);

                empresaAux = item;

                if (empresaAux == null)
                    continue;
                if (somenteAtivos)
                {
                    if (empresaAux.Ativo)
                        break;
                    empresaAux = null;
                }
                else
                    break;
            }

            return empresaAux;
        }

        /// <summary>
        /// Busca aleatóriamente uma empresa cadastrada na base de dados.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// Filtrar empresas ativas.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Empresa.
        /// </returns>
        public static Empresa ObterEmpresa(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<Empresa>(unidTrabalho);
            var empresas = repositorio.ObterTodos().ToList();
            Assert.IsTrue(empresas.Any());

            Random rand = new Random();
            empresas = empresas.OrderBy(x => rand.Next()).ToList();

            Empresa empresa = null;
            foreach (var item in empresas)
            {
                empresa = item;

                if (empresa == null)
                    continue;
                if (somenteAtivos)
                {
                    if (empresa.Ativo)
                        break;
                    empresa = null;
                }
                else
                    break;
            }

            return empresa;
        }

    }
}
