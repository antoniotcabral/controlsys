﻿using System;
using System.Data.SqlTypes;
using System.Linq;
using System.Web.Mvc;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Controlsys.Interfaces.Tests.App_Start;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Controlsys.Interfaces.Tests.Controller
{
    [TestClass]
    public class RelatorioPessoaCriticaFotoTeste
    {
        [TestMethod]
        public void Exportar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RelatorioPessoaCriticaFotoController controller = new RelatorioPessoaCriticaFotoController(unidTrabalho);
            Mock.ServerMapPath(controller, "RelatorioPessoaCriticaFoto.rdlc");

            List<string> selectedFields = typeof(RelatorioPessoaCriticaFotoModelView).GetProperties().Select(pi => pi.Name).ToList();
            var dtInicio = (DateTime)SqlDateTime.MinValue;
            var dtfim = DateTime.Now;

            var result = controller.Exportar(selectedFields, "PDF", dtInicio, dtfim) as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/pdf", result.ContentType);
            Assert.AreEqual(@"D:\Projetos\Controlsys\controlsys-develop-fase2\Controlsys.Interfaces\Reports\download.pdf", result.FileDownloadName);

        }

    }
}
