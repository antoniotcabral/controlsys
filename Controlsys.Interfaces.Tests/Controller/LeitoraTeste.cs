﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.Mvc;
using Controlsys.Dominio.Acesso;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Controlsys.Persistencia.Repositorios.Acesso;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) LeitoraTeste.
    /// </summary>
    [TestClass]
    public class LeitoraTeste
    {
        /// <summary>
        /// Pode incluir.
        /// </summary>
        [TestMethod]
        public void Pode_Incluir()
        {
            string data = DateTime.Now.ToString("MM/dd HH:mm:ss.fff");
            string nome = string.Format("Nome-{0}", data);
            string descricao = string.Format("Descriçao-{0}", data);

            Leitora leitora = new Leitora
            {
                IDLeitora = nome,
                Descricao = descricao,
                ExigeSupervisao = false,
                Local = "Portaria 1A",
                //TipoLeitora = TipoLeitora.Leitora,
                Direcao = DirecaoLeitora.Entrada,
                DataRegistro = DateTime.Now
            };

            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            LeitoraController controller = new LeitoraController(unidTrabalho);

            JsonResult result = controller.Incluir(leitora) as JsonResult;
            LeitoraModelView leitoraResult = JsonHelper.GetJsonObjectRepresentation<LeitoraModelView>(result);

            Assert.IsNotNull(result.Data);

            Assert.AreEqual("Nome-" + data, leitoraResult.IDLeitora);
            Assert.AreEqual("Descriçao-" + data, leitoraResult.Descricao);
            Assert.AreEqual("Portaria 1A", leitoraResult.Local);
            Assert.AreEqual(TipoLeitora.Leitora.ToString(), leitoraResult.TipoLeitora);
            Assert.AreEqual(DirecaoLeitora.Entrada.ToString(), leitoraResult.Direcao);
        }

        /// <summary>
        /// Pode alterar.
        /// </summary>
        [TestMethod]
        public void Pode_Alterar()
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();

            LeitoraController controller = new LeitoraController(unidTrabalho);
            JsonResult result = controller.ObterTodos() as JsonResult;

            var leits = JsonHelper.GetJsonObjectRepresentation<List<LeitoraModelView>>(result);

            Random rand = new Random();
            leits = leits.OrderBy(x => rand.Next()).ToList();

            LeitoraModelView leitoraAux = null;
            foreach (var item in leits)
            {
                //areaAux = (controller.Selecionar(item.Codigo) as JsonResult).Data as Area;
                result = controller.Selecionar(item.Codigo) as JsonResult;
                leitoraAux = JsonHelper.GetJsonObjectRepresentation<LeitoraModelView>(result);
                if (leitoraAux == null)
                    continue;
                if (leitoraAux.Ativo)
                    break;
                leitoraAux = null;
            }
            Assert.IsNotNull(leitoraAux);

            Assert.IsTrue(leitoraAux.Ativo);
            string data = DateTime.Now.ToString("MM/dd HH:mm:ss.fff");

            var leitora = new Leitora
            {
                Codigo = leitoraAux.Codigo,
                IDLeitora = "Nome-" + data,
                Descricao = "Desc-" + data,
                //TipoLeitora = TipoLeitora.LeitoraTeclado,
                Direcao = DirecaoLeitora.Saida,
            };

            result = controller.Alterar(leitora) as JsonResult;
            var leitoraResult = JsonHelper.GetJsonObjectRepresentation<Leitora>(result);

            Assert.AreEqual("Nome-" + data, leitoraResult.IDLeitora);
            Assert.AreEqual("Desc-" + data, leitoraResult.Descricao);
            //Assert.AreEqual(TipoLeitora.LeitoraTeclado, leitoraResult.TipoLeitora);
            Assert.AreEqual(DirecaoLeitora.Saida, leitoraResult.Direcao);
        }

        /// <summary>
        /// Pode ativar.
        /// </summary>
        [TestMethod]
        public void Pode_Ativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            LeitoraController controller = new LeitoraController(unidTrabalho);

            var repositorio = new RepositorioLeitora(unidTrabalho);
            var leitoras = repositorio.ObterTodos().Where(l => !l.Ativo).ToList();

            Assert.IsNotNull(leitoras);
            Assert.IsTrue(leitoras.Any());

            Leitora leitoraAux = leitoras.FirstOrDefault();
            Assert.IsNotNull(leitoraAux);
            Assert.IsFalse(leitoraAux.Ativo);

            var result = controller.AtivarInativar(leitoraAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            LeitoraModelView leitora = JsonHelper.GetJsonObjectRepresentation<LeitoraModelView>(result);
            Assert.IsNotNull(leitora);
            Assert.AreEqual(leitora.Codigo, leitoraAux.Codigo);
            Assert.IsTrue(leitora.Ativo);
        }

        /// <summary>
        /// Pode inativar.
        /// </summary>
        [TestMethod]
        public void Pode_Inativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            LeitoraController controller = new LeitoraController(unidTrabalho);

            var repositorio = new RepositorioLeitora(unidTrabalho);
            var leitoras = repositorio.ObterTodos().Where(l => l.Ativo).ToList();

            Leitora leitoraAux = leitoras.FirstOrDefault();
            Assert.IsNotNull(leitoraAux);
            Assert.IsTrue(leitoraAux.Ativo);

            var result = controller.AtivarInativar(leitoraAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            
            var leitora = JsonHelper.GetJsonObjectRepresentation<LeitoraModelView>(result);
            Assert.IsNotNull(leitora);
            Assert.AreEqual(leitora.Codigo, leitoraAux.Codigo);
            Assert.IsFalse(leitora.Ativo);
        }
    }
}
