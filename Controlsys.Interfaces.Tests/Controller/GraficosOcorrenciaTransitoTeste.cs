﻿using System;
using System.Data.SqlTypes;
using System.Web.Mvc;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Tests.App_Start;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Controlsys.Interfaces.Tests.Controller
{
    [TestClass]
    public class GraficosOcorrenciaTransitoTeste
    {
        [TestMethod]
        public void ExportarPorClassificacao()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new RelatorioGraficosOcorrenciaTransitoController(unidTrabalho);
            Mock.ServerMapPath(controller, "GraficoInfracoesPorClassificacao.rdlc");

            var dtInicio = (DateTime)SqlDateTime.MinValue;
            var dtfim = DateTime.Now;

            var result = controller.Exportar("PDF", "INFRACOESCLASSIFICACAO", dtInicio, dtfim) as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/pdf", result.ContentType);
            Assert.AreEqual("download.pdf", result.FileDownloadName);
        }

        [TestMethod]
        public void ExportarPorPenalidade()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new RelatorioGraficosOcorrenciaTransitoController(unidTrabalho);
            Mock.ServerMapPath(controller, "GraficoInfracoesPorPenalidade.rdlc");

            var dtInicio = (DateTime)SqlDateTime.MinValue;
            var dtfim = DateTime.Now;

            var result = controller.Exportar("PDF", "INFRACOESPORPENALIDADE", dtInicio, dtfim) as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/pdf", result.ContentType);
            Assert.AreEqual("download.pdf", result.FileDownloadName);
        }

        [TestMethod]
        public void ExportarPorTreinamentos()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new RelatorioGraficosOcorrenciaTransitoController(unidTrabalho);
            Mock.ServerMapPath(controller, "GraficoResultadoDosTreinamentos.rdlc");

            var dtInicio = (DateTime)SqlDateTime.MinValue;
            var dtfim = DateTime.Now;

            var result = controller.Exportar("PDF", "RESULTADOSTREINAMENTO", dtInicio, dtfim) as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/pdf", result.ContentType);
            Assert.AreEqual("download.pdf", result.FileDownloadName);
        }

        [TestMethod]
        public void ExportarPorPessoa()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new RelatorioGraficosOcorrenciaTransitoController(unidTrabalho);
            Mock.ServerMapPath(controller, "GraficoInfracoesPorPessoa.rdlc");

            var dtInicio = (DateTime)SqlDateTime.MinValue;
            var dtfim = DateTime.Now;

            var result = controller.Exportar("PDF", "INFRACOESPORPESSOA", dtInicio, dtfim) as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/pdf", result.ContentType);
            Assert.AreEqual("download.pdf", result.FileDownloadName);
        }

        [TestMethod]
        public void ExportarPorEmpresasMaisNotificadas()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new RelatorioGraficosOcorrenciaTransitoController(unidTrabalho);
            Mock.ServerMapPath(controller, "GraficoEmpresaMaisNotificacoes.rdlc");

            var dtInicio = (DateTime)SqlDateTime.MinValue;
            var dtfim = DateTime.Now;

            var result = controller.Exportar("PDF", "EMPRESASMAISNOTIFICACOES", dtInicio, dtfim) as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/pdf", result.ContentType);
            Assert.AreEqual("download.pdf", result.FileDownloadName);
        }
    }
}
