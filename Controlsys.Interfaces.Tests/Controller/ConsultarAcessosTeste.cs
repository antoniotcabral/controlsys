﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Controlsys.Dominio.Acesso;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Controlsys.Persistencia;
using Controlsys.Dominio.Pessoas;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) ConsultarAcessosTeste.
    /// </summary>
    [TestClass]
    public class ConsultarAcessosTeste
    {
        /// <summary>
        /// Pode pesquisar.
        /// </summary>
        [TestMethod]
        public void Pode_Pesquisar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new ConsultarAcessosController(unidTrabalho);
            var rep = new Repositorio<ControleAcesso>(unidTrabalho);

            var rand = new Random();
            var controleAcessos = rep.ObterTodos().ToList();
            Assert.IsNotNull(controleAcessos);
            Assert.IsTrue(controleAcessos.Any());

            controleAcessos = controleAcessos.OrderBy(x => rand.Next()).ToList();
            controleAcessos = controleAcessos.Take(100).ToList();
            var acessosAux = controleAcessos[0];
            Assert.IsNotNull(acessosAux);
            if (acessosAux.Papel == null)
                return;

            var result = controller.Pesquisar(null, null, null) as JsonResult;
            Assert.IsNotNull(result);

            var acesso = result.Data as List<ControleAcessoModelView>;
            Assert.IsNotNull(acesso);
            Assert.IsTrue(acesso.Any());
            foreach (var item in acesso)
            {
                Assert.AreEqual(acessosAux.Papel.PessoaFisica.Nome.ToLower(), item.Nome.ToLower());
            }
        }

        /// <summary>
        /// Obter lista controle acesso.
        /// </summary>
        [TestMethod]
        public void ObterListaControleAcesso()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new ConsultarAcessosController(unidTrabalho);

            var result = controller.Pesquisar(null, null, null) as JsonResult;

            Assert.IsNotNull(result);
            var lista = result.Data as List<ControleAcessoModelView>;
            Assert.IsNotNull(lista);
            Assert.IsTrue(lista.Any());
        }


        #region Testes específicos


        [TestMethod]
        public void UltimoAcessoNoGrupoLeitora()
        {
            StatusAcesso? ultimoAcessoEsperado = StatusAcesso.OCO_UR_ENTRADA_COMPLETA;
            string cpf = "28293961899";
            string nomeGrupoLeitora = "ACESSO_PORTO";

            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repVisitante = new Repositorio<Visitante>(unidTrabalho);
            var listaVisitante = repVisitante.ObterTodos().Where(v => v.PessoaFisica.CPF == cpf).ToList();
            var visitante = listaVisitante.Where(v => v.ObterLogPapel().Status == StatusPapelLog.Ativo).FirstOrDefault();

            var repGrupoLeitora = new Repositorio<GrupoLeitora>(unidTrabalho);
            var listaLgl = repGrupoLeitora.ObterTodos().Where(gl => gl.Nome == nomeGrupoLeitora && gl.Ativo == true &&
                gl.Leitoras.Any(lgl => lgl.Ativo == true && lgl.Leitora.Ativo == true)).SelectMany(gl => gl.Leitoras);
            var leitoras = listaLgl.Select(lgl => lgl.Leitora.Codigo).Distinct().ToList();

            var repControleAcesso = new Repositorio<ControleAcesso>(unidTrabalho);
            var ultimoAcesso = repControleAcesso.ObterTodos().Where(c => c.Papel.PessoaFisica.Codigo == visitante.PessoaFisica.Codigo)
                .OrderByDescending(a => a.DataAcesso).FirstOrDefault();

            if (ultimoAcessoEsperado == null) Assert.IsFalse(leitoras.Contains(ultimoAcesso.Leitora.Codigo));
            else Assert.IsTrue(leitoras.Contains(ultimoAcesso.Leitora.Codigo) && ultimoAcesso.StatusAcesso == ultimoAcessoEsperado);
        }


        #endregion
    }
}
