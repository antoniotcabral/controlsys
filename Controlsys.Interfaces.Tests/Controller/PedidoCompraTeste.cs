﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Pessoas;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Controlsys.Persistencia;
using Ext.Net;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) PedidoCompraTeste.
    /// </summary>
    [TestClass]
    public class PedidoCompraTeste
    {
        /// <summary>
        /// Valida incluir um novo PedidoCompra.
        /// </summary>
        [TestMethod]
        public void Pode_Incluir()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new PedidoCompraController(unidTrabalho);

            var rand = new Random();
            var dt = DateTime.Now;
            var data = dt.ToString("MM/dd HH:mm:ss.fff");
            var randValue = rand.Next(1, 9999);

            #region [CNAE]
            var repCNAE = new Repositorio<CNAE>(unidTrabalho);
            var listaCNAE = repCNAE.ObterTodos().ToList();
            Assert.IsTrue(listaCNAE.Any());

            listaCNAE = listaCNAE.Where(x => x.Ativo).ToList();
            Assert.IsTrue(listaCNAE.Any());

            listaCNAE = listaCNAE.OrderBy(x => rand.Next()).ToList();
            var cnae = listaCNAE.First();
            #endregion
            #region [listaPrazo]
            int countAux = rand.Next(1, 3);
            var listaPrazo = new List<Prazo>();
            for (int i = 0; i < countAux; i++)
            {
                var dtInicio = dt.AddDays(rand.Next(1, 7));
                listaPrazo.Add(new Prazo()
                {
                    DataInicio = dtInicio,
                    DataFim = dtInicio.AddDays(rand.Next(1, 7)),
                    //Aditivo = Global.ObterBoolean(),
                    Observacao = "Observação-" + randValue
                });
            }
            #endregion
            #region [listaSetorCustoPedido]
            countAux = rand.Next(1, 3);
            var listaSetorCusto = new List<SetorCusto>();
            for (int i = 0; i < countAux; i++)
            {
                var setorCusto = SetorCustoTeste.ObterSetorCusto(true);
                if (listaSetorCusto.Contains(setorCusto))
                    continue;
                listaSetorCusto.Add(setorCusto);
            }
            var listaSetorCustoPedido = listaSetorCusto.Select(setorCusto => new SetorCustoPedido()
            {
                SetorCusto = setorCusto
            }).ToList();
            #endregion
            #region [listaColaboradorPedido]
            countAux = rand.Next(1, 3);
            var listaColaborador = new List<Colaborador>();
            for (int i = 0; i < countAux; i++)
            {
                var colaborador = ColaboradorTeste.ObterColaborador();
                if (listaColaborador.Contains(colaborador))
                    continue;
                listaColaborador.Add(colaborador);
            }
            var listaColaboradorPedido = listaColaborador.Select(colaborador => new ColaboradorPedido()
            {
                Colaborador = colaborador
            }).ToList();
            #endregion

            var pedidoCompraAux = new PedidoCompra
            {
                Numero = "Num-" + randValue,
                Nome = "Nome-" + randValue,
                Empresa = EmpresaTeste.ObterEmpresa(),
                CNAE = cnae,
                Observacao = "Observação-" + randValue,
                Prazos = listaPrazo,
                Setores = listaSetorCustoPedido,
                GestoresResponsaveis = listaColaboradorPedido,
                Projeto = ProjetoTeste.ObterProjeto(true)
            };

            var result = controller.Incluir(pedidoCompraAux) as JsonResult;
            Assert.IsNotNull(result);

            var node = result.Data as Globalsys.Tree.Node;
            Assert.IsNotNull(node);

            var repPedidoCompra = new Repositorio<PedidoCompra>(unidTrabalho);
            var pedidoCompra = repPedidoCompra.ObterPorId(node.Codigo);
            Assert.IsNotNull(pedidoCompra);
            Assert.AreEqual(pedidoCompraAux.Numero, pedidoCompra.Numero);
            Assert.AreEqual(pedidoCompraAux.Nome, pedidoCompra.Nome);
            Assert.AreEqual(pedidoCompraAux.Observacao, pedidoCompra.Observacao);

            Assert.AreEqual(pedidoCompraAux.Empresa, pedidoCompra.Empresa);
            Assert.AreEqual(pedidoCompraAux.CNAE, pedidoCompra.CNAE);
            Assert.AreEqual(pedidoCompraAux.Projeto, pedidoCompra.Projeto);

            for (int i = 0; i < pedidoCompraAux.Prazos.Count; i++)
            {
                Assert.AreEqual(pedidoCompraAux.Prazos[i], pedidoCompra.Prazos[i]);
            }

            //TODO: Validar lista SetorCusto
            //TODO: Validar lista GestoresResponsaveis
        }

        /// <summary>
        /// Valida alterar um PedidoCompra.
        /// </summary>
        [TestMethod]
        public void Pode_Alterar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new PedidoCompraController(unidTrabalho);

            var rand = new Random();
            var dt = DateTime.Now;
            var data = dt.ToString("MM/dd HH:mm:ss.fff");
            var randValue = rand.Next(1, 9999);

            #region [CNAE]
            var repCNAE = new Repositorio<CNAE>(unidTrabalho);
            var listaCNAE = repCNAE.ObterTodos().ToList();
            Assert.IsTrue(listaCNAE.Any());

            listaCNAE = listaCNAE.Where(x => x.Ativo).ToList();
            Assert.IsTrue(listaCNAE.Any());

            listaCNAE = listaCNAE.OrderBy(x => rand.Next()).ToList();
            var cnae = listaCNAE.First();
            #endregion
            #region [listaPrazo]
            int countAux = rand.Next(1, 3);
            var listaPrazo = new List<Prazo>();
            for (int i = 0; i < countAux; i++)
            {
                var dtInicio = dt.AddDays(rand.Next(1, 7));
                listaPrazo.Add(new Prazo()
                {
                    DataInicio = dtInicio,
                    DataFim = dtInicio.AddDays(rand.Next(1, 7)),
                    //Aditivo = Global.ObterBoolean(),
                    Observacao = "Observação-" + randValue
                });
            }
            #endregion
            #region [listaSetorCustoPedido]
            countAux = rand.Next(1, 3);
            var listaSetorCusto = new List<SetorCusto>();
            for (int i = 0; i < countAux; i++)
            {
                var setorCusto = SetorCustoTeste.ObterSetorCusto(true);
                if (listaSetorCusto.Contains(setorCusto))
                    continue;
                listaSetorCusto.Add(setorCusto);
            }
            var listaSetorCustoPedido = listaSetorCusto.Select(setorCusto => new SetorCustoPedido()
            {
                SetorCusto = setorCusto
            }).ToList();
            #endregion
            #region [listaColaboradorPedido]
            countAux = rand.Next(1, 3);
            var listaColaborador = new List<Colaborador>();
            for (int i = 0; i < countAux; i++)
            {
                var colaborador = ColaboradorTeste.ObterColaborador();
                if (listaColaborador.Contains(colaborador))
                    continue;
                listaColaborador.Add(colaborador);
            }
            var listaColaboradorPedido = listaColaborador.Select(colaborador => new ColaboradorPedido()
            {
                Colaborador = colaborador
            }).ToList();
            #endregion

            var pedidoCompraAux = ObterPedidoCompra(StatusPedido.Ativo);
            Assert.IsNotNull(pedidoCompraAux);

            pedidoCompraAux.Numero = "Num-" + randValue;
            pedidoCompraAux.Nome = "Nome-" + randValue;
            pedidoCompraAux.CNAE = cnae;
            pedidoCompraAux.Observacao = "Observação-" + randValue;

            foreach (var prazo in listaPrazo)
            {
                pedidoCompraAux.Prazos.Add(prazo);
            }
            pedidoCompraAux.Setores = listaSetorCustoPedido;
            pedidoCompraAux.GestoresResponsaveis = listaColaboradorPedido;

            var result = controller.Alterar(pedidoCompraAux, new string[] { "Numero", "Nome", "CNAE", "Observacao" },
                listaSetorCusto.Select(x => x.Codigo).ToArray(), pedidoCompraAux.Setores.Select(x => x.Codigo).ToArray(),
                listaColaborador.Select(x => x.Codigo).ToArray(), pedidoCompraAux.GestoresResponsaveis.Select(x => x.Codigo).ToArray(),
                listaPrazo.Select(x => x.Codigo).ToArray(), pedidoCompraAux.Prazos.Select(x => x.Codigo).ToArray()) as JsonResult;
            Assert.IsNotNull(result);

            var pedidoCompra = result.Data as PedidoCompraModelView;
            Assert.IsNotNull(pedidoCompra);

            //var repPedidoCompra = new Repositorio<PedidoCompra>(unidTrabalho);
            //pedidoCompra = repPedidoCompra.ObterPorId(node.Codigo);
            Assert.IsNotNull(pedidoCompra);
            Assert.AreEqual(pedidoCompraAux.Numero, pedidoCompra.Numero);
            Assert.AreEqual(pedidoCompraAux.Nome, pedidoCompra.Nome);
            Assert.AreEqual(pedidoCompraAux.Observacao, pedidoCompra.Observacao);

            Assert.AreEqual(pedidoCompraAux.Empresa.Codigo, pedidoCompra.Empresa);
            Assert.AreEqual(pedidoCompraAux.CNAE.Codigo, pedidoCompra.CNAE);
            Assert.AreEqual(pedidoCompraAux.Projeto.Nome, pedidoCompra.Projeto);

            //TODO: Validar lista Prazo
            //TODO: Validar lista SetorCusto
            //TODO: Validar lista GestoresResponsaveis
        }

        /// <summary>
        /// Valida Ativar PedidoCompra.
        /// </summary>
        [TestMethod]
        public void Pode_Ativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new PedidoCompraController(unidTrabalho);
            var pedidoCompraAux = ObterPedidoCompra(StatusPedido.Inativo);
            Assert.IsTrue(pedidoCompraAux.Status == StatusPedido.Inativo);

            var result = controller.AtivarInativar(pedidoCompraAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var listaIds = result.Data as PedidoCompraAtivarInativarModelView;
            Assert.IsNotNull(listaIds);
            Assert.IsNull(listaIds.IdsInativar);

            var repPedidoCompra = new Repositorio<PedidoCompra>(unidTrabalho);
            var listaPedidoCompra = repPedidoCompra.ObterTodos().ToList();
            Assert.IsTrue(listaPedidoCompra.Any());

            var pedidoCompra = listaPedidoCompra.First(x => x.Codigo == pedidoCompraAux.Codigo);
            Assert.IsNotNull(pedidoCompra);
            Assert.IsTrue(pedidoCompra.Status == StatusPedido.Ativo);
        }

        /// <summary>
        /// Valida Inativar PedidoCompra.
        /// </summary>
        [TestMethod]
        public void Pode_Inativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new PedidoCompraController(unidTrabalho);
            var pedidoCompraAux = ObterPedidoCompra(StatusPedido.Ativo);
            Assert.IsTrue(pedidoCompraAux.Status == StatusPedido.Ativo);

            var result = controller.AtivarInativar(pedidoCompraAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var listaIds = result.Data as PedidoCompraAtivarInativarModelView;
            Assert.IsNotNull(listaIds);
            Assert.IsTrue(listaIds.IdsInativar.Any());

            var repPedidoCompra = new Repositorio<PedidoCompra>(unidTrabalho);
            var listaPedidoCompra = repPedidoCompra.ObterTodos().ToList();
            Assert.IsTrue(listaPedidoCompra.Any());

            listaPedidoCompra = listaPedidoCompra.Where(x => listaIds.IdsInativar.Contains(x.Codigo)).ToList();
            Assert.IsTrue(listaPedidoCompra.Any());

            foreach (var pedidoCompra in listaPedidoCompra)
            {
                Assert.IsTrue(pedidoCompra.Status == StatusPedido.Inativo);
            }
        }

        /// <summary>
        /// Valida Remover PedidoCompra.
        /// </summary>
        [TestMethod]
        public void Pode_Remover()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new PedidoCompraController(unidTrabalho);

            var repPedidoCompra = new Repositorio<PedidoCompra>(unidTrabalho);
            var listaPedidoCompra = repPedidoCompra.ObterTodos().ToList();
            Assert.IsTrue(listaPedidoCompra.Any());

            var rand = new Random();
            listaPedidoCompra = listaPedidoCompra.OrderBy(x => rand.Next()).ToList();
            var pedidoCompraAux = listaPedidoCompra.First(x => (x.Status == StatusPedido.Ativo) || (x.Status == StatusPedido.Inativo));
            Assert.IsNotNull(pedidoCompraAux);
            Assert.IsTrue((pedidoCompraAux.Status == StatusPedido.Ativo) || (pedidoCompraAux.Status == StatusPedido.Inativo));

            var result = controller.Remover(pedidoCompraAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);
            Assert.IsNull(result.Data);

            var repositorio = new Repositorio<PedidoCompra>(unidTrabalho);
            var pedidoCompra = repositorio.ObterPorId(pedidoCompraAux.Codigo);

            //var listaPedidoCompraRemovido = ObterListaPedidoCompra(StatusPedido.Removido);
            //Assert.IsTrue(listaPedidoCompraRemovido.Any());

            //var pedidoCompra = listaPedidoCompraRemovido.First(x => x.Codigo == pedidoCompraAux.Codigo);
            Assert.IsNotNull(pedidoCompra);
            Assert.IsTrue(pedidoCompra.Status == StatusPedido.Removido);
        }

        /// <summary>
        /// Pode alocar colaborador.
        /// </summary>
        [TestMethod]
        public void Pode_AlocarColaborador()
        {
            var listaColaborador = ColaboradorTeste.ObterListaColaborador();
            Assert.IsNotNull(listaColaborador);
            Assert.IsTrue(listaColaborador.Any());

            listaColaborador = listaColaborador.Count > 3 ? 
                listaColaborador.GetRange(0, 3) : 
                new List<Colaborador>() { listaColaborador[0] };
            Assert.IsNotNull(listaColaborador);
            Assert.IsTrue(listaColaborador.Any());
            var listaColabAux = listaColaborador.Select(x => x.Codigo).ToArray();

            var setorCusto = SetorCustoTeste.ObterSetorCusto(true);
            Assert.IsNotNull(setorCusto);

            var pedidoCompra = ObterPedidoCompra(StatusPedido.Ativo);
            Assert.IsNotNull(pedidoCompra);

            var grupoTrabalho = GrupoTrabalhoTeste.ObterGrupoTrabalho(true);
            Assert.IsNotNull(grupoTrabalho);

            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new PedidoCompraController(unidTrabalho);

            var result = controller.AlocarColaborador(listaColabAux, setorCusto.Codigo, pedidoCompra.Codigo) as JsonResult;
            Assert.IsNotNull(result);
        }

        /// <summary>
        /// Busca aleatóriamente um PedidoCompra cadastrado na base de dados.
        /// </summary>
        ///
        /// <param name="statusPedido">
        /// O(a) status pedido.
        /// </param>
        ///
        /// <returns>
        /// Um(a) PedidoCompra.
        /// </returns>
        public static PedidoCompra ObterPedidoCompra(StatusPedido statusPedido)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<PedidoCompra>(unidTrabalho);
            var listaPedidoCompra = repositorio.ObterTodos().ToList();
            Assert.IsTrue(listaPedidoCompra.Any());

            listaPedidoCompra = listaPedidoCompra.Where(x => x.Status == statusPedido).ToList();
            if (listaPedidoCompra.Count > 0)
            {
                Assert.IsTrue(listaPedidoCompra.Any());

                var rand = new Random();
                listaPedidoCompra = listaPedidoCompra.OrderBy(x => rand.Next()).ToList();

                return listaPedidoCompra.First();
            }

            return null;
        }

        /// <summary>
        /// Obter lista pedido compra.
        /// </summary>
        ///
        /// <param name="statusPedido">
        /// O(a) status pedido.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;PedidoCompra&gt;
        /// </returns>
        public static List<PedidoCompra> ObterListaPedidoCompra(StatusPedido statusPedido)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<PedidoCompra>(unidTrabalho);
            var listaPedidoCompra = repositorio.ObterTodos().ToList();
            Assert.IsTrue(listaPedidoCompra.Any());

            listaPedidoCompra = listaPedidoCompra.Where(x => x.Status == statusPedido).ToList();
            Assert.IsTrue(listaPedidoCompra.Any());

            var rand = new Random();
            listaPedidoCompra = listaPedidoCompra.OrderBy(x => rand.Next()).ToList();
            return listaPedidoCompra;
        }
    }
}
