﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Controlsys.Infra;
using Globalsys;
using System.Collections.Generic;
using Controlsys.Interfaces.Controllers;
using Ext.Net;
using Controlsys.Dominio.Pessoas;
using System.Web.Mvc;
using Controlsys.Interfaces.Models;
using Ext.Net.MVC;
using Controlsys.Persistencia;
using System.Linq;

namespace Controlsys.Interfaces.Tests.Controller
{
    [TestClass]
    public class ArquivoPapelTeste
    {
        [TestMethod]
        public void Pode_Pesquisar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            ArquivoPapelController controller = new ArquivoPapelController(unidTrabalho);

            var x = new Dictionary<string, object>() { };
            x.Add("start", (int)0);
            x.Add("limit", (int)25);

            var result = controller.Pesquisar(new StoreRequestParameters(x), null, null, null, null, null, 0, "Inativação") as StoreResult;

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Data as ArquivoPapelModelView[]);
        }

        [TestMethod]
        public void Pode_Exportar_Pesquisar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            ArquivoPapelController controller = new ArquivoPapelController(unidTrabalho);

            Array values = Enum.GetValues(typeof(StatusPapelLog));
            Random random = new Random();
            StatusPapelLog status = (StatusPapelLog)values.GetValue(random.Next(values.Length));

            var result = controller.ExportarPesquisa(null, null, null, null, null, 0, status.ObterDescricaoEnum()) as FileResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Pode_Exportar_Selecao()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<Papel>(unidTrabalho);
            ArquivoPapelController controller = new ArquivoPapelController(unidTrabalho);

            IQueryable<Papel> papeis = repositorio.ObterTodos().Where(p => p.Documentos.Select(d => d.Documento).Any(dm => dm.Midias.Any()));
            
            var result = controller.ExportarSelecao(papeis.Select(p => p.Codigo).ToArray(), null) as FileResult;

            Assert.IsNotNull(result);
        }
    }
}
