﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Controlsys.Dominio.CredenciamentoVeiculos;
using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Pessoas;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Parametros;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Controlsys.Infra;

namespace Controlsys.Interfaces.Tests.Controller
{
    [TestClass]
    public class EmitirCredencialTeste
    {
        [TestMethod]
        public void Incluir()
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();

            var rand = new Random();
            CredencialVeiculo registro = new CredencialVeiculo();

            #region [Situação]
            SituacaoCredencialVeiculo situacao = new SituacaoCredencialVeiculo();
            situacao.DataRegistro = DateTime.Now.AddDays(Convert.ToDouble(rand.Next(-7, -1)));
            situacao.Observacao = string.Format("OBS {0}", rand.Next(1, 9999)); 
            #endregion

            #region [Dados Empresa]
            registro.Empresa = EmpresaTeste.ObterEmpresa(true);
            registro.PedidoCompra = PedidoCompraTeste.ObterListaPedidoCompra(StatusPedido.Ativo).Where(x => x.Empresa.Codigo == registro.Empresa.Codigo).FirstOrDefault();
            registro.Area = AreaTeste.ObterArea(true);
            registro.SubContratada = EmpresaTeste.ObterEmpresa(true);
            registro.Fornecedor = string.Format("FORNECEDOR {0}", rand.Next(1, 9999)); 
            #endregion

            #region [Condutor]
            var pessoas = PessoaTeste.ObterListaPessoasFisica().Where(x => x.Papeis.Any(y => y.PapelLogs.Where(h => h.Status == StatusPapelLog.Ativo).Any()));
            Assert.IsNotNull(pessoas);
            var pessoa = pessoas.FirstOrDefault();
            Assert.IsNotNull(pessoa);
            registro.Condutor = pessoas.FirstOrDefault().ObterPapel();
            #endregion

            #region [Dados Modelo Credencial]
            registro.ModeloCredencial = ModeloCredencialTeste.ObterModeloCredencial(true);
            registro.TipoValidade = TipoValidadeTeste.ObterTipoValidade(true);
            registro.ValidadeDiasCredencial = rand.Next(1, 365);
            registro.DataValidadeLit = DateTime.Now.AddDays(rand.Next(1, 365));
            registro.DataVencimentoSeguro = DateTime.Now.AddDays(rand.Next(1, 365));
            registro.Corporativo = rand.Next(0, 1) != 0;
            registro.Observacao = string.Format("OBS {0}", rand.Next(1, 9999));  
            #endregion

            #region [Veículos]
            List<Veiculo> listaVeiculos = new List<Veiculo>();
            for (int i = 0; i < rand.Next(1, 2); i++)
            {
                Veiculo veiculo = new Veiculo();
                veiculo.Modelo = ModeloVeiculoTeste.ObterListaModeloVeiculo(true).FirstOrDefault();
                veiculo.AnoFabricacao = rand.Next(2000, DateTime.Now.Year);
                veiculo.AnoModelo = rand.Next(veiculo.AnoFabricacao, veiculo.AnoFabricacao + 1);
                veiculo.Cor = "COR";
                veiculo.Placa = string.Format("AAA-{0}", rand.Next(1000, 9999));
                listaVeiculos.Add(veiculo);
            } 
            #endregion

            EmitirCredencialController controller = new EmitirCredencialController(unidTrabalho);
            App_Start.Mock.UserHttpContext(controller, "rubem.almeida");

            var result = controller.Incluir(registro, registro.Condutor.Codigo, situacao, null) as JsonResult;
            Assert.IsNotNull(result);

            var registroGerado = JsonHelper.GetJsonObjectRepresentation<CredencialVeiculoModelView>(result);

            Assert.IsNotNull(registroGerado);
            Assert.IsTrue(registroGerado.Codigo > 0);
            Assert.IsTrue(registroGerado.Status == StatusCredencial.Recebida.ObterDescricaoEnum());

            unidTrabalho.Rollback();
        }

        [TestMethod]
        public void Alterar()
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();

            CredencialVeiculo registro = ObterListaCredencialVeiculos().FirstOrDefault();
            registro.Empresa = EmpresaTeste.ObterEmpresa(true);
            registro.ModeloCredencial = ModeloCredencialTeste.ObterModeloCredencial(true);

            EmitirCredencialController controller = new EmitirCredencialController(unidTrabalho);
            App_Start.Mock.UserHttpContext(controller, "rubem.almeida");

            var result = controller.Alterar(registro,registro.Condutor.Codigo, null) as JsonResult;
            Assert.IsNotNull(result);

            var registroAlterado = JsonHelper.GetJsonObjectRepresentation<CredencialVeiculoModelView>(result);

            Assert.IsNotNull(registroAlterado);
            Assert.AreEqual(registro.Codigo, registroAlterado.Codigo);
            Assert.AreEqual(registro.Empresa.Codigo, registroAlterado.Empresa.Codigo);
            Assert.AreEqual(registro.ModeloCredencial.Codigo, registroAlterado.ModeloCredencial.Codigo);

            unidTrabalho.Rollback();
        }

        [TestMethod]
        public void Selecionar()
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            EmitirCredencialController controller = new EmitirCredencialController(unidTrabalho);
            App_Start.Mock.UserHttpContext(controller, "rubem.almeida");

            CredencialVeiculo registro = ObterListaCredencialVeiculos().FirstOrDefault();
            var result = controller.Selecionar(registro.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var registroEncontrado = JsonHelper.GetJsonObjectRepresentation<CredencialVeiculoModelView>(result);

            Assert.IsNotNull(registroEncontrado);
            Assert.AreEqual(registro.Codigo, registroEncontrado.Codigo);

            unidTrabalho.Rollback();
        }

        [TestMethod]
        public void ObterTodos()
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            EmitirCredencialController controller = new EmitirCredencialController(unidTrabalho);
            App_Start.Mock.UserHttpContext(controller, "rubem.almeida");

            var result = controller.ObterTodos() as JsonResult;
            Assert.IsNotNull(result);

            var registros = JsonHelper.GetJsonObjectRepresentation<List<CredencialVeiculoModelView>>(result);

            Assert.IsNotNull(registros);
            Assert.IsTrue(registros.Any());

            unidTrabalho.Rollback();
        }

        [TestMethod]
        public void Inspecionar()
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            EmitirCredencialController controller = new EmitirCredencialController(unidTrabalho);
            App_Start.Mock.UserHttpContext(controller, "rubem.almeida");

            var rand = new Random();
            SituacaoCredencialVeiculo situacao = new SituacaoCredencialVeiculo();
            situacao.DataRegistro = DateTime.Now;
            situacao.Observacao = string.Format("OBS {0}", DateTime.Now.Ticks);

            CredencialVeiculo registro = ObterListaCredencialVeiculos(StatusCredencial.Recebida).FirstOrDefault();
            Assert.IsNotNull(registro);

            var result = controller.Inspecionar(registro.Codigo, situacao) as JsonResult;
            Assert.IsNotNull(result);

            var registroEncontrado = JsonHelper.GetJsonObjectRepresentation<CredencialVeiculoModelView>(result);

            Assert.IsNotNull(registroEncontrado);
            Assert.AreEqual(registro.Codigo, registroEncontrado.Codigo);
            Assert.AreEqual(registroEncontrado.Status, StatusCredencial.Inspecionada.ObterDescricaoEnum());

            unidTrabalho.Rollback();
        }

        [TestMethod]
        public void Aprovar()
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            EmitirCredencialController controller = new EmitirCredencialController(unidTrabalho);
            App_Start.Mock.UserHttpContext(controller, "rubem.almeida");

            var rand = new Random();
            SituacaoCredencialVeiculo situacao = new SituacaoCredencialVeiculo();
            situacao.DataRegistro = DateTime.Now;
            situacao.Observacao = string.Format("OBS {0}", DateTime.Now.Ticks);

            CredencialVeiculo registro = ObterListaCredencialVeiculos(StatusCredencial.Inspecionada).FirstOrDefault();
            Assert.IsNotNull(registro);

            var result = controller.Aprovar(registro.Codigo, situacao) as JsonResult;
            Assert.IsNotNull(result);

            var registroEncontrado = JsonHelper.GetJsonObjectRepresentation<CredencialVeiculoModelView>(result);

            Assert.IsNotNull(registroEncontrado);
            Assert.AreEqual(registro.Codigo, registroEncontrado.Codigo);
            Assert.AreEqual(registroEncontrado.Status, StatusCredencial.Aprovada.ObterDescricaoEnum());

            unidTrabalho.Rollback();
        }

        [TestMethod]
        public void Imprimir()
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            EmitirCredencialController controller = new EmitirCredencialController(unidTrabalho);
            App_Start.Mock.UserHttpContext(controller, "rubem.almeida");

            var rand = new Random();
            SituacaoCredencialVeiculo situacao = new SituacaoCredencialVeiculo();
            situacao.DataRegistro = DateTime.Now;
            situacao.Observacao = string.Format("OBS {0}", DateTime.Now.Ticks);

            CredencialVeiculo registro = ObterListaCredencialVeiculos(StatusCredencial.Aprovada).FirstOrDefault();
            Assert.IsNotNull(registro);

            var result = controller.Imprimir(registro.Codigo, situacao) as JsonResult;
            Assert.IsNotNull(result);

            var registroEncontrado = JsonHelper.GetJsonObjectRepresentation<CredencialVeiculoModelView>(result);

            Assert.IsNotNull(registroEncontrado);
            Assert.AreEqual(registro.Codigo, registroEncontrado.Codigo);
            Assert.AreEqual(registroEncontrado.Status, StatusCredencial.Impressa.ObterDescricaoEnum());

            unidTrabalho.Rollback();
        }

        [TestMethod]
        public void Devolver()
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            EmitirCredencialController controller = new EmitirCredencialController(unidTrabalho);
            App_Start.Mock.UserHttpContext(controller, "rubem.almeida");

            var rand = new Random();
            SituacaoCredencialVeiculo situacao = new SituacaoCredencialVeiculo();
            situacao.DataRegistro = DateTime.Now;
            situacao.Observacao = string.Format("OBS {0}", DateTime.Now.Ticks);

            CredencialVeiculo registro = ObterListaCredencialVeiculos(StatusCredencial.Impressa).FirstOrDefault();
            Assert.IsNotNull(registro);

            var result = controller.Devolver(registro.Codigo, situacao) as JsonResult;
            Assert.IsNotNull(result);

            var registroEncontrado = JsonHelper.GetJsonObjectRepresentation<CredencialVeiculoModelView>(result);

            Assert.IsNotNull(registroEncontrado);
            Assert.AreEqual(registro.Codigo, registroEncontrado.Codigo);
            Assert.AreEqual(registroEncontrado.Status, StatusCredencial.Devolvida.ObterDescricaoEnum());

            unidTrabalho.Rollback();
        }

        [TestMethod]
        public static List<CredencialVeiculo> ObterListaCredencialVeiculos(StatusCredencial? statusCredencial = null)
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            IRepositorioCredencialVeiculo repCredencialVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioCredencialVeiculo>(unidTrabalho);

            IQueryable<CredencialVeiculo> lista = repCredencialVeiculo.ObterTodos();

            if (statusCredencial.HasValue)
                lista = lista.Where(x => x.Situacoes.OrderByDescending(scv => scv.Codigo).Select(sc => sc.Status).FirstOrDefault() == statusCredencial);

            return lista.ToList().OrderBy(x => new Random().Next()).ToList();
        }

        private StatusCredencial ObterStatus()
        {
            var rand = new Random();
            StatusCredencial option = StatusCredencial.Recebida;
            switch (rand.Next(1, 5))
            {
                case 1:
                    option = StatusCredencial.Recebida;
                    break;
                case 2:
                    option = StatusCredencial.Inspecionada;
                    break;
                case 3:
                    option = StatusCredencial.Aprovada;
                    break;
                case 4:
                    option = StatusCredencial.Impressa;
                    break;
                case 5:
                    option = StatusCredencial.Devolvida;
                    break;
                default:
                    break;
            }
            return option;
        }
    }
}
