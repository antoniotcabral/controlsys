﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using Controlsys.Interfaces.Tests.App_Start;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Controlsys.Infra;
using Globalsys;
using Controlsys.Dominio.Relatorio;
using System.Web;
using Controlsys.Interfaces.Controllers;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// Summary description for RelatorioRestauranteSinteticoTeste
    /// </summary>
    [TestClass]
    public class RelatorioRestauranteSinteticoTeste
    {
        /// <summary>
        /// Pode exportar PDF.
        /// </summary>
        [TestMethod]
        public void Pode_ExportarPDF()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            List<string> selectedFields = typeof(RelatorioRestauranteCatraca).GetProperties().Select(pi => pi.Name).ToList();
            RelatorioRestauranteSinteticoController controller = new RelatorioRestauranteSinteticoController(unidTrabalho);
            Mock.ServerMapPath(controller, "RelatorioRestauranteSintetico.rdlc");

            var result = controller.Exportar("PDF", dataInicial: DateTime.Now.AddDays(-5), dataFinal: DateTime.Now.AddDays(19)) as FileContentResult;

            Assert.IsNotNull(result);
            Assert.AreEqual("application/pdf", result.ContentType);
            Assert.AreEqual("download.pdf", result.FileDownloadName);
        }

        /// <summary>
        /// Pode exportar excel.
        /// </summary>
        [TestMethod]
        public void Pode_ExportarExcel()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            List<string> selectedFields = typeof(RelatorioRestauranteCatraca).GetProperties().Select(pi => pi.Name).ToList();
            RelatorioRestauranteSinteticoController controller = new RelatorioRestauranteSinteticoController(unidTrabalho);
            Mock.ServerMapPath(controller, "RelatorioRestauranteSintetico.rdlc");
            var result = controller.Exportar("Excel", dataInicial: DateTime.Now.AddDays(-5), dataFinal: DateTime.Now.AddDays(19)) as FileContentResult;

            Assert.IsNotNull(result);
            Assert.AreEqual("application/vnd.ms-excel", result.ContentType);
            Assert.AreEqual("download.xls", result.FileDownloadName);
        }

        /// <summary>
        /// Pode exportar word.
        /// </summary>
        [TestMethod]
        public void Pode_ExportarWord()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            List<string> selectedFields = typeof(RelatorioRestauranteCatraca).GetProperties().Select(pi => pi.Name).ToList();
            RelatorioRestauranteSinteticoController controller = new RelatorioRestauranteSinteticoController(unidTrabalho);
            Mock.ServerMapPath(controller, "RelatorioRestauranteSintetico.rdlc");
            var result = controller.Exportar("Word", dataInicial: DateTime.Now.AddDays(-5), dataFinal: DateTime.Now.AddDays(19)) as FileContentResult;

            Assert.IsNotNull(result);
            Assert.AreEqual("application/msword", result.ContentType);
            Assert.AreEqual("download.doc", result.FileDownloadName);
        }
    }
}
