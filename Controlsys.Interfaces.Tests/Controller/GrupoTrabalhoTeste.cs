﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Seguranca;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Controlsys.Interfaces.Tests.Properties;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Controlsys.Persistencia;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) GrupoTrabalhoTeste.
    /// </summary>
    [TestClass]
    public class GrupoTrabalhoTeste
    {
        /// <summary>
        /// Valida incluir um novo grupo, informando o código de um turno já cadastrado na base de dados.
        /// </summary>
        [TestMethod]
        public void Pode_Incluir()
        {
            var dt = DateTime.Now;
            var data = dt.ToString("MM/dd HH:mm:ss.fff");
            var nome = "Nome-" + data;
            var grupoTrabalhoAux = new GrupoTrabalho { Nome = nome };
            var turno = TurnoTeste.ObterTurno(true);
            Assert.IsNotNull(turno);

            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new GrupoTrabalhoController(unidTrabalho);

            var result = controller.Incluir(grupoTrabalhoAux, turno.Codigo, dt, null, null) as JsonResult;
            Assert.IsNotNull(result);

            var grupoTrabalho = JsonHelper.GetJsonObjectRepresentation<GrupoTrabalhoModelView>(result);
            Assert.IsNotNull(grupoTrabalho);
            Assert.AreEqual(grupoTrabalhoAux.Nome, grupoTrabalho.Nome);

            //TODO: Validar Turno em GrupoTrabalhoTurno
        }

        /// <summary>
        /// Valida alterar um Grupo, modificando o nome e adicionando um turno aletório que já fora
        /// cadastrado na base de dados;
        /// </summary>
        [TestMethod]
        public void Pode_Alterar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new GrupoTrabalhoController(unidTrabalho);
            var grupoTrabalhoAux = ObterGrupoTrabalho(true);
            Assert.IsNotNull(grupoTrabalhoAux);
            Assert.IsTrue(grupoTrabalhoAux.Ativo);

            var dt = DateTime.Now;
            var data = dt.ToString("MM/dd HH:mm:ss.fff");
            grupoTrabalhoAux.Nome = "Nome-" + data;

            var turno = TurnoTeste.ObterTurno(true);
            Assert.IsNotNull(turno);

            var result = controller.Alterar(grupoTrabalhoAux, turno.Codigo, dt, null, null, null) as JsonResult;
            Assert.IsNotNull(result);

            var grupoTrabalho = JsonHelper.GetJsonObjectRepresentation<GrupoTrabalhoModelView>(result);
            Assert.IsNotNull(grupoTrabalho);
            Assert.AreEqual(grupoTrabalhoAux.Codigo, grupoTrabalho.Codigo);
            Assert.AreEqual(grupoTrabalhoAux.Nome, grupoTrabalho.Nome);

            //TODO: Validar Turno em GrupoTrabalhoTurno
        }

        /// <summary>
        /// Valida Ativar GrupoTrabalho.
        /// </summary>
        [TestMethod]
        public void Pode_Ativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new GrupoTrabalhoController(unidTrabalho);
            var lista = ObterListaGrupoTrabalho();
            Assert.IsTrue(lista.Any(), Resources.Lista_vazia);

            var listaAtivo = lista.Where(x => x.Ativo).ToList();
            var listaInativo = lista.Where(x => x.Ativo == false).ToList();
            Assert.IsTrue(listaInativo.Any(), Resources.Lista_vazia);

            GrupoTrabalhoModelView grupoTrabalhoAux = null;
            for (int i = 0; i < listaInativo.Count; i++)
            {
                if (listaAtivo.Any())
                    if (listaAtivo.Exists(x => x.Nome == listaInativo[i].Nome)) continue;
                grupoTrabalhoAux = listaInativo[i];
                break;
            }
            Assert.IsNotNull(grupoTrabalhoAux);
            Assert.IsFalse(grupoTrabalhoAux.Ativo);

            var result = controller.AtivarInativar(grupoTrabalhoAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var grupoTrabalho = JsonHelper.GetJsonObjectRepresentation<GrupoTrabalhoModelView>(result);
            Assert.IsNotNull(grupoTrabalho);
            Assert.AreEqual(grupoTrabalhoAux.Codigo, grupoTrabalho.Codigo);
            Assert.IsTrue(grupoTrabalho.Ativo);
        }

        /// <summary>
        /// Valida Inativar GrupoTrabalho.
        /// </summary>
        [TestMethod]
        public void Pode_Inativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new GrupoTrabalhoController(unidTrabalho);
            var grupoTrabalhoAux = ObterGrupoTrabalho(true);
            Assert.IsTrue(grupoTrabalhoAux.Ativo);

            var result = controller.AtivarInativar(grupoTrabalhoAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var grupoTrabalho = JsonHelper.GetJsonObjectRepresentation<GrupoTrabalhoModelView>(result);
            Assert.IsNotNull(grupoTrabalho);
            Assert.AreEqual(grupoTrabalhoAux.Codigo, grupoTrabalho.Codigo);
            Assert.IsFalse(grupoTrabalho.Ativo);
        }

        /// <summary>
        /// Busca aleatóriamente um Grupo de Trabalho cadastrado na base de dados.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// Filtrar Grupos de Trabalho ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) GrupoTrabalho.
        /// </returns>
        public static GrupoTrabalho ObterGrupoTrabalho(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<GrupoTrabalho>(unidTrabalho);
            var gruposTrabalho = repositorio.ObterTodos().ToList();
            Assert.IsTrue(gruposTrabalho.Any());

            Random rand = new Random();
            gruposTrabalho = gruposTrabalho.OrderBy(x => rand.Next()).ToList();

            GrupoTrabalho grupoTrabalho = null;
            foreach (var item in gruposTrabalho)
            {
                grupoTrabalho = item;

                if (grupoTrabalho == null)
                    continue;
                if (somenteAtivos)
                {
                    if (grupoTrabalho.Ativo)
                        break;
                    grupoTrabalho = null;
                }
                else
                    break;
            }

            return grupoTrabalho;
        }

        /// <summary>
        /// Obter lista grupo trabalho.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// Filtrar Grupos de Trabalho ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;GrupoTrabalhoModelView&gt;
        /// </returns>
        private List<GrupoTrabalhoModelView> ObterListaGrupoTrabalho(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new GrupoTrabalhoController(unidTrabalho);
            var result = controller.ObterTodos() as JsonResult;
            Assert.IsNotNull(result);

            var gruposTrabalho = JsonHelper.GetJsonObjectRepresentation<List<GrupoTrabalhoModelView>>(result);
            var rand = new Random();
            gruposTrabalho = gruposTrabalho.OrderBy(x => rand.Next()).ToList();

            return somenteAtivos ? gruposTrabalho.Where(x => x.Ativo).ToList() : gruposTrabalho;
        }
    }
}
