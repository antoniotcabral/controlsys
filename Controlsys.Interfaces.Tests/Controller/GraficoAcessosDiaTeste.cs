﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Web.Mvc;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Controlsys.Interfaces.Tests.App_Start;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Controlsys.Interfaces.Tests.Controller
{
    [TestClass]
    public class GraficoAcessosDiaTeste
    {
        [TestMethod]
        public void Exportar()
        {

            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new GraficoAcessosDiaController(unidTrabalho);
            Mock.ServerMapPath(controller, "GraficoAcessosDia.rdlc");

            var dtInicio = (DateTime)SqlDateTime.MinValue;
            var dtfim = DateTime.Now;

            var result = controller.Exportar("PDF", dtInicio, dtfim) as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/pdf", result.ContentType);
            Assert.AreEqual("download.pdf", result.FileDownloadName);
        }
    }
}
