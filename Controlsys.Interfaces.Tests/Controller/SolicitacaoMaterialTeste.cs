﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Globalsys;
using Controlsys.Persistencia;
using Controlsys.Dominio.SolicitacoesMaterial;
using Controlsys.Interfaces.Models;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) SolicitacaoMaterialTeste.
    /// </summary>
    [TestClass]
    public class SolicitacaoMaterialTeste
    {

        [TestMethod]
        public void Pode_solicitar()
        {
            TipoSolicitacao tipo = ObterTioSolicitacaoAleatorio();
            DateTime dataEntradaSaida = DateTime.Now.Date;
            int codigoSolicitacaoMaterial = 0;
            Modalidade? modalidade = (tipo == TipoSolicitacao.Entrada ? null : (Modalidade?)Modalidade.Desmobilizacao);
            DateTime? retorno = null;
            int? setorCusto = SetorCustoTeste.ObterSetorCusto(true).Codigo;
            string contratada = EmpresaTeste.ObterEmpresa(true).Codigo;
            int pedidoCompra = 0;
            string subcontratada = null;
            string placa = SolicitacaoMaterialTeste.CriarString(7, true);
            string destino = SolicitacaoMaterialTeste.CriarString(20, false);
            string portador = SolicitacaoMaterialTeste.CriarString(30, false);
            string motivo = SolicitacaoMaterialTeste.CriarString(50, false);

            MaterialModelView[] materiais = SolicitacaoMaterialTeste.GerarListaMateriaisNovos();
            MidiaModelView[] midias = new List<MidiaModelView>().ToArray();

            int codigoColaborador = ColaboradorTeste.ObterColaborador().Codigo;
            int codigoGestor = 0;

            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new SolicitacaoMaterialController(unidTrabalho);
            App_Start.Mock.UserHttpContext(controller, "ivan.penna");
            var result = controller.Solicitar(tipo, dataEntradaSaida, 0, modalidade, retorno, setorCusto, contratada, pedidoCompra, subcontratada, placa, destino, portador, motivo, materiais, midias, codigoColaborador, codigoGestor) as JsonResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Pode_aprovar()
        {
            this.Pode_solicitar();

            SolicitacaoMaterial solicitacao = SolicitacaoMaterialTeste.ObterSolicitacaoMaterial(true, SituacaoSolicitacao.AguardandoAprovacao);

            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new SolicitacaoMaterialController(unidTrabalho);

            var result = controller.Aprovar(solicitacao.Codigo) as JsonResult;

            Assert.IsNotNull(result);
        }


        [TestMethod]
        public void Pode_reprovar()
        {
            this.Pode_solicitar();

            SolicitacaoMaterial solicitacao = SolicitacaoMaterialTeste.ObterSolicitacaoMaterial(true, SituacaoSolicitacao.AguardandoAprovacao);

            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new SolicitacaoMaterialController(unidTrabalho);

            var result = controller.Receber(solicitacao.Codigo, ColaboradorTeste.ObterColaborador().Codigo) as JsonResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Pode_imprimir()
        {
            this.Pode_solicitar();

            this.Pode_aprovar();

            SolicitacaoMaterial solicitacao = SolicitacaoMaterialTeste.ObterSolicitacaoMaterial(true, SituacaoSolicitacao.Aprovada);

            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new SolicitacaoMaterialController(unidTrabalho);

            var result = controller.Imprimir(solicitacao.Codigo) as JsonResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Pode_receber()
        {
            this.Pode_solicitar();

            this.Pode_aprovar();

            this.Pode_imprimir();

            this.Pode_registrar();

            SolicitacaoMaterial solicitacao = SolicitacaoMaterialTeste.ObterSolicitacaoMaterial(true, SituacaoSolicitacao.Registrada);

            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new SolicitacaoMaterialController(unidTrabalho);

            var result = controller.Receber(solicitacao.Codigo, ColaboradorTeste.ObterColaborador().Codigo) as JsonResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Pode_registrar()
        {
            this.Pode_solicitar();

            this.Pode_aprovar();

            this.Pode_imprimir();

            SolicitacaoMaterial solicitacao = SolicitacaoMaterialTeste.ObterSolicitacaoMaterial(true, SituacaoSolicitacao.Impressa);

            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new SolicitacaoMaterialController(unidTrabalho);

            var result = controller.Registrar(solicitacao.Codigo, solicitacao.Itens.Select(i => new MaterialModelView() { AutorizacaoVigilante = new Random().Next(0,1) == 0 ? false : true, CodigoMaterial = i.Codigo, Descricao = i.Descricao, NotaFiscalEntrada = i.NotaFiscalEntrada, NotaFiscalSaida = i.NotaFiscalSaida, NotaServico = i.NotaServico, Patrimonio = i.NumeroPatrimonio, Quantidade = i.Quantidade }).ToArray()) as JsonResult;

            Assert.IsNotNull(result);
        }

        public static SolicitacaoMaterial ObterSolicitacaoMaterial(bool somenteAtivos = false, SituacaoSolicitacao? situacao = null)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<SolicitacaoMaterial>(unidTrabalho);

            var listaSolciitacoes = repositorio.ObterTodos().ToList();

            Assert.IsTrue(listaSolciitacoes.Any());

            if (situacao.HasValue)
            {
                listaSolciitacoes = listaSolciitacoes.Where(s => s.Status.OrderByDescending(st => st.Codigo).FirstOrDefault().Situacao == situacao.Value).ToList();
            }

            var rand = new Random();
            listaSolciitacoes = listaSolciitacoes.OrderBy(x => rand.Next()).ToList();

            return listaSolciitacoes.FirstOrDefault(x => !somenteAtivos || x.Ativo);
        }

        public TipoSolicitacao ObterTioSolicitacaoAleatorio() 
        {

            Array values = Enum.GetValues(typeof(TipoSolicitacao));
            Random random = new Random();
            return (TipoSolicitacao)values.GetValue(random.Next(values.Length));

        }

        public static string CriarString(int stringLength, bool apenasAlfanumerica = false)
        {
            string allowedChars = "ABCDEFGHJKLMNOPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz0123456789!@$?_- ";
            if (apenasAlfanumerica)
                allowedChars = "ABCDEFGHIJKLMNOPQRSTUWVXYZ0123456789";

            char[] chars = new char[stringLength];
            var rd = new Random(828340238);

            for (int i = 0; i < stringLength; i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }

            return new string(chars);
        }

        public static MaterialModelView[] GerarListaMateriaisNovos()
        {
            var aleatorio = new Random();
            int qtdMateriais = aleatorio.Next(1, 10);
            List<MaterialModelView> materiais = new List<MaterialModelView>();
            for (int i = 0; i < qtdMateriais; i++)
			{
			    MaterialModelView novoMaterial = new MaterialModelView() { 
                    AutorizacaoVigilante = false, 
                    CodigoMaterial = 0, 
                    Descricao = SolicitacaoMaterialTeste.CriarString(20),
                    NotaFiscalEntrada = "",
                    NotaFiscalSaida = "",
                    NotaServico = SolicitacaoMaterialTeste.CriarString(50, true),
                    Patrimonio = SolicitacaoMaterialTeste.CriarString(50, true),
                    Quantidade = aleatorio.Next(1, 10)
                };
                materiais.Add(novoMaterial);
			}

            return materiais.ToArray();
        }
    }
}
