﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Seguranca;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Controlsys.Interfaces.Tests.Properties;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Controlsys.Persistencia;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) GrupoTeste.
    /// </summary>
    [TestClass]
    public class GrupoTeste
    {
        /// <summary>
        /// Valida incluir um novo grupo, inserindo um usuário e uma ação, já cadastrados, nas
        /// respectivas listas, e um grupo de leitora, apenas caso exista, por não ser um campo
        /// obrigatório;
        /// </summary>
        [TestMethod]
        public void Pode_Incluir()
        {
            var data = DateTime.Now.ToString("MM/dd HH:mm:ss.fff");
            var nome = "Nome-" + data;
            var descricao = "Descrição-" + data;
            var grupoAux = new Grupo()
            {
                Nome = nome,
                Descricao = descricao
            };

            int[] gruposLeitora = null;
            var grupoLeitora = GrupoLeitoraTeste.ObterGrupoLeitora(true);
            if (grupoLeitora != null)
                gruposLeitora = new int[] { grupoLeitora.Codigo };

            var usuario = UsuarioTeste.ObterUsuario(true);
            Assert.IsNotNull(usuario);
            var usuarios = new string[] { usuario.Codigo };

            var acao = AcaoTeste.ObterAcao(true);
            Assert.IsNotNull(acao);
            var acoes = new int[] { acao.Codigo };

            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            GrupoController controller = new GrupoController(unidTrabalho);
            var result = controller.Incluir(grupoAux, acoes, usuarios, gruposLeitora) as JsonResult;
            Assert.IsNotNull(result.Data);

            var grupo = JsonHelper.GetJsonObjectRepresentation<Grupo>(result);
            Assert.AreEqual(nome, grupo.Nome);
            Assert.AreEqual(descricao, grupo.Descricao);

            //TODO: Validar GruposLeitora (caso tenha sido cadastrada)
            //TODO: Validar Usuários
            //TODO: Validar Ações
        }

        /// <summary>
        /// Valida alteração de um Grupo, modificando o nome e a descrição do mesmo, adicionando um
        /// usuário e uma ação nas respectivas listas, e um grupo de leitora, caso exista, por não ser um
        /// campo obrigatório;
        /// </summary>
        [TestMethod]
        public void Pode_Alterar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            GrupoController controller = new GrupoController(unidTrabalho);
            Grupo grupoAux = ObterGrupo(true);
            Assert.IsNotNull(grupoAux);
            Assert.IsTrue(grupoAux.Ativo);

            var data = DateTime.Now.ToString("MM/dd HH:mm:ss.fff");
            var nome = "Nome-" + data;
            var descricao = "Descrição-" + data;
            grupoAux.Nome = nome;
            grupoAux.Descricao = descricao;

            int[] gruposLeitora = null;
            var grupoLeitora = GrupoLeitoraTeste.ObterGrupoLeitora(true);
            if (grupoLeitora != null)
                gruposLeitora = new int[] { grupoLeitora.Codigo };

            var usuario = UsuarioTeste.ObterUsuario(true);
            Assert.IsNotNull(usuario);
            var usuarios = new string[] { usuario.Codigo };

            var acao = AcaoTeste.ObterAcao(true);
            Assert.IsNotNull(acao);
            var acoes = new int[] { acao.Codigo };

            var result = controller.Alterar(grupoAux, new[] { "Nome", "Descricao" }, acoes, null, usuarios, null, gruposLeitora, null) as JsonResult;
            Assert.IsNotNull(result);

            var grupo = JsonHelper.GetJsonObjectRepresentation<Grupo>(result);
            Assert.AreEqual(nome, grupo.Nome);
            Assert.AreEqual(descricao, grupo.Descricao);

            //TODO: Validar GruposLeitora (caso tenha sido cadastrada)
            //TODO: Validar Usuários
            //TODO: Validar Ações
        }

        /// <summary>
        /// Valida Ativar Grupo.
        /// </summary>
        [TestMethod]
        public void Pode_Ativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new GrupoController(unidTrabalho);
            var lista = ObterListaGrupo();
            Assert.IsTrue(lista.Any(), Resources.Lista_vazia);

            lista = lista.Where(x => x.Ativo == false).ToList();
            if (lista.Count <= 0) return;
            Assert.IsTrue(lista.Any(), Resources.Lista_vazia);

            var grupoAux = lista[0];
            Assert.IsFalse(grupoAux.Ativo);

            var result = controller.AtivarInativar(grupoAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var grupo = JsonHelper.GetJsonObjectRepresentation<GrupoModelView>(result);
            Assert.IsNotNull(grupo);
            Assert.AreEqual(grupoAux.Codigo, grupo.Codigo);
            Assert.IsTrue(grupo.Ativo);
        }

        /// <summary>
        /// Valida Inativar Grupo.
        /// </summary>
        [TestMethod]
        public void Pode_Inativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new GrupoController(unidTrabalho);
            var grupoAux = ObterGrupo(true);
            Assert.IsTrue(grupoAux.Ativo);

            var result = controller.AtivarInativar(grupoAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var grupo = JsonHelper.GetJsonObjectRepresentation<GrupoModelView>(result);
            Assert.IsNotNull(grupo);
            Assert.AreEqual(grupoAux.Codigo, grupo.Codigo);
            Assert.IsFalse(grupo.Ativo);
        }

        /*
        /// <summary>
        /// Valida ativação ou inativação de Grupo, conforme status corrente;
        /// </summary>
        [TestMethod]
        public void Pode_AtivarInativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            GrupoController controller = new GrupoController(unidTrabalho);
            Grupo grupoAux = ObterGrupo();
            Assert.IsNotNull(grupoAux);

            bool status = !grupoAux.Ativo;
            var result = controller.AtivarInativar(grupoAux.Codigo) as JsonResult;
            Assert.IsNotNull(result.Data);

            var grupo = JsonHelper.GetJsonObjectRepresentation<Grupo>(result);
            Assert.AreEqual(status, grupo.Ativo);
        }
        */

        /// <summary>
        /// Busca aleatóriamente um grupo cadastrado na base de dados.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// Filtrar grupos ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Grupo.
        /// </returns>
        public static Grupo ObterGrupo(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<Grupo>(unidTrabalho);
            var grupos = repositorio.ObterTodos().ToList();
            Assert.IsTrue(grupos.Any());

            Random rand = new Random();
            grupos = grupos.OrderBy(x => rand.Next()).ToList();

            Grupo grupo = null;
            foreach (var item in grupos)
            {
                grupo = item;

                if (grupo == null)
                    continue;
                if (somenteAtivos)
                {
                    if (grupo.Ativo)
                        break;
                    grupo = null;
                }
                else
                    break;
            }

            return grupo;
        }

        /// <summary>
        /// Obter lista grupo.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// Filtrar grupos ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;GrupoModelView&gt;
        /// </returns>
        private List<GrupoModelView> ObterListaGrupo(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new GrupoController(unidTrabalho);
            var result = controller.ObterTodos() as JsonResult;
            Assert.IsNotNull(result);

            var grupos = JsonHelper.GetJsonObjectRepresentation<List<GrupoModelView>>(result);
            var rand = new Random();
            grupos = grupos.OrderBy(x => rand.Next()).ToList();

            return somenteAtivos ? grupos.Where(x => x.Ativo).ToList() : grupos;
        }
    }
}
