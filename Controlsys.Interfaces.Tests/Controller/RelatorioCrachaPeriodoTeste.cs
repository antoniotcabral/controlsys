﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Controlsys.Dominio.Pessoas;
using Controlsys.Dominio.Relatorio;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Ext.Net;
using Ext.Net.MVC;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.SqlTypes;

namespace Controlsys.Interfaces.Tests.Controller
{
    [TestClass]
    public class RelatorioCrachaPeriodoTeste
    {
        [TestMethod]
        public void Pesquisar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new RelatorioCrachaPeriodoController(unidTrabalho);

            var dtInicio = (DateTime)SqlDateTime.MinValue;
            var dtfim = DateTime.Now;

            var x = new Dictionary<string, object>() { };
            x.Add("start", (int)0);
            x.Add("limit", (int)25);

            var result = controller.Pesquisar(new StoreRequestParameters(x), dtInicio, dtfim, "TKCSA", tipoPapel: TipoPapel.Visitante) as StoreResult;
            Assert.IsNotNull(result);

            var registros = result.Data as List<RelatorioCrachaPeriodo>;
            Assert.IsNotNull(registros);
        }

        [TestMethod]
        public void Exportar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new RelatorioCrachaPeriodoController(unidTrabalho);

            List<string> selectedFields = typeof(RelatorioCrachaPeriodoModelView).GetProperties().Select(pi => pi.Name).ToList();
            
            var dtInicio = (DateTime)SqlDateTime.MinValue;
            var dtfim = DateTime.Now;

            var result = controller.Exportar(selectedFields, "PDF", dtInicio, dtfim, "TKCSA", tipoPapel: TipoPapel.Visitante) as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/pdf", result.ContentType);
            Assert.AreEqual("download.pdf", result.FileDownloadName);
        }
    }
}
