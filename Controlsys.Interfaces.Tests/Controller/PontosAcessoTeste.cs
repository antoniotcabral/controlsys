﻿using System;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Controlsys.Interfaces.Tests.Controller
{
    [TestClass]
    public class PontosAcessoTeste
    {
        [TestMethod]
        public void Pode_AlterarStatus()
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();

            unidTrabalho.BeginTransaction();

            var fakeHttpContext = new Mock<HttpContextBase>();
            var fakeIdentity = new GenericIdentity("theo.ladislau");
            var principal = new GenericPrincipal(fakeIdentity, null);

            fakeHttpContext.Setup(t => t.User).Returns(principal);
            var controllerContext = new Mock<ControllerContext>();
            controllerContext.Setup(t => t.HttpContext).Returns(fakeHttpContext.Object);

            PontosAcessoController controller = new PontosAcessoController(unidTrabalho);
            controller.ControllerContext = controllerContext.Object;

            ControladoraController controladControll = new ControladoraController(unidTrabalho);

            ControladoraModelView controladora = (controladControll.Selecionar(1) as JsonResult).Data as ControladoraModelView;

            //Alterando status para Ativo
            controller.AlterarStatus(controladora.Codigo, Dominio.Acesso.StatusControladora.Ativo);

            Assert.AreEqual(controladora.Status, Dominio.Acesso.StatusControladora.Ativo.ObterDescricaoEnum());

            //Alterando status para Inativo
            controller.AlterarStatus(controladora.Codigo, Dominio.Acesso.StatusControladora.Inativo);

            controladora = (controladControll.Selecionar(controladora.Codigo) as JsonResult).Data as ControladoraModelView;

            Assert.AreEqual(controladora.Status, Dominio.Acesso.StatusControladora.Inativo.ObterDescricaoEnum());

            //Alterando status para Liberado
            controller.AlterarStatus(controladora.Codigo, Dominio.Acesso.StatusControladora.Liberado);

            controladora = (controladControll.Selecionar(controladora.Codigo) as JsonResult).Data as ControladoraModelView;

            Assert.AreEqual(controladora.Status, Dominio.Acesso.StatusControladora.Liberado.ObterDescricaoEnum());

            //Alterando status para Interditado
            controller.AlterarStatus(controladora.Codigo, Dominio.Acesso.StatusControladora.Interditado);

            controladora = (controladControll.Selecionar(controladora.Codigo) as JsonResult).Data as ControladoraModelView;

            Assert.AreEqual(controladora.Status, Dominio.Acesso.StatusControladora.Interditado.ObterDescricaoEnum());

            unidTrabalho.Rollback();            

        }

        [TestMethod]
        public void Pode_ObterTodos()
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            PontosAcessoController controller = new PontosAcessoController(unidTrabalho);

             Assert.IsNotNull((controller.ObterTodos() as JsonResult).Data);
        }
    }
}
