﻿using System;
using System.Data.SqlTypes;
using System.Linq;
using System.Web.Mvc;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Controlsys.Interfaces.Tests.App_Start;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Controlsys.Interfaces.Tests.Controller
{
    [TestClass]
    public class NotificaValidadeDocumentosTeste
    {
        [TestMethod]
        public void EnviarNotificacao()
        {

            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            NotificaValidadeDocumentosController controller = new NotificaValidadeDocumentosController(unidTrabalho);
            Mock.ServerMapPath(controller, "NotificaValidadeDocumentos.rdlc");

            var result = controller.Index(true) as object;
            Assert.IsNotNull(result);

            var retorno = (result is int ? (int) result : 0);

            Assert.AreEqual(1, retorno);

        }
    }
}
