﻿using System;
using System.Linq;
using System.Collections.Generic;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Ext.Net;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using Moq;
using System.Web;
using System.Security.Principal;
using Ext.Net.MVC;

namespace Controlsys.Interfaces.Tests.Controller
{
    [TestClass]
    public class RelatorioRefeicoesTeste
    {
        /// <summary>
        /// Pode exportar PDF.
        /// </summary>
        [TestMethod]
        public void Pode_ExportarPDF()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();

            List<string> selectedFields = typeof(ControleAcessoRefeicaoModelView).GetProperties().Select(pi => pi.Name).ToList();

            var fakeHttpContext = new Mock<HttpContextBase>();
            var fakeIdentity = new GenericIdentity("theo.ladislau");
            var principal = new GenericPrincipal(fakeIdentity, null);

            fakeHttpContext.Setup(t => t.User).Returns(principal);
            var controllerContext = new Mock<ControllerContext>();
            controllerContext.Setup(t => t.HttpContext).Returns(fakeHttpContext.Object);

            RelatorioRefeicoesController controller = new RelatorioRefeicoesController(unidTrabalho);
            controller.ControllerContext = controllerContext.Object;

            var result = controller.Exportar(null, selectedFields, "PDF", cpfPassaporte: "") as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/pdf", result.ContentType);
            Assert.AreEqual("download.pdf", result.FileDownloadName);
        }

        /// <summary>
        /// Pode exportar excel.
        /// </summary>
        [TestMethod]
        public void Pode_ExportarExcel()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();

            List<string> selectedFields = typeof(ControleAcessoRefeicaoModelView).GetProperties().Select(pi => pi.Name).ToList();

            var fakeHttpContext = new Mock<HttpContextBase>();
            var fakeIdentity = new GenericIdentity("theo.ladislau");
            var principal = new GenericPrincipal(fakeIdentity, null);

            fakeHttpContext.Setup(t => t.User).Returns(principal);
            var controllerContext = new Mock<ControllerContext>();
            controllerContext.Setup(t => t.HttpContext).Returns(fakeHttpContext.Object);

            RelatorioRefeicoesController controller = new RelatorioRefeicoesController(unidTrabalho);
            controller.ControllerContext = controllerContext.Object;

            var result = controller.Exportar(null, selectedFields, "Excel", cpfPassaporte: "") as FileContentResult;
            Assert.IsNotNull(result);            
            Assert.AreEqual("application/vnd.ms-excel", result.ContentType);
            Assert.AreEqual("download.xls", result.FileDownloadName);
        }

        /// <summary>
        /// Pode exportar word.
        /// </summary>
        [TestMethod]
        public void Pode_ExportarWord()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();

            var fakeHttpContext = new Mock<HttpContextBase>();
            var fakeIdentity = new GenericIdentity("theo.ladislau");
            var principal = new GenericPrincipal(fakeIdentity, null);

            fakeHttpContext.Setup(t => t.User).Returns(principal);
            var controllerContext = new Mock<ControllerContext>();
            controllerContext.Setup(t => t.HttpContext).Returns(fakeHttpContext.Object);                      

            RelatorioRefeicoesController controller = new RelatorioRefeicoesController(unidTrabalho);
            controller.ControllerContext = controllerContext.Object;

            List<string> selectedFields = typeof(ControleAcessoRefeicaoModelView).GetProperties().Select(pi => pi.Name).ToList();

            var result = controller.Exportar(null, selectedFields, "Word", cpfPassaporte: "") as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/msword", result.ContentType);
            Assert.AreEqual("download.doc", result.FileDownloadName);
        }

        /// <summary>
        /// Pode pesquisar.
        /// </summary>
        [TestMethod]
        public void Pode_Pesquisar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();

            var fakeHttpContext = new Mock<HttpContextBase>();
            var fakeIdentity = new GenericIdentity("theo.ladislau");
            var principal = new GenericPrincipal(fakeIdentity, null);

            fakeHttpContext.Setup(t => t.User).Returns(principal);
            var controllerContext = new Mock<ControllerContext>();
            controllerContext.Setup(t => t.HttpContext).Returns(fakeHttpContext.Object);      

            RelatorioRefeicoesController controller = new RelatorioRefeicoesController(unidTrabalho);
            controller.ControllerContext = controllerContext.Object;            

            var result = controller.Pesquisar(null, cpfPassaporte: "", ignoreParameters: true)  as StoreResult;
            Assert.IsNotNull(result);

            var registros = result.Data as List<ControleAcessoRefeicaoModelView>;
            Assert.IsNotNull(registros);
        }


    }
}
