﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Pessoas;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Controlsys.Interfaces.Tests.Properties;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections;
using Rhino.Mocks;
using Controlsys.Persistencia;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) FornecedorTeste.
    /// </summary>
    [TestClass]
    public class FornecedorTeste
    {
        /// <summary>
        /// Pode incluir.
        /// </summary>
        [TestMethod]
        public void Pode_Incluir()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new FornecedorController(unidTrabalho);
            
            var fornecedorAux = new Fornecedor();
            var fornecedorEmpresa = new List<FornecedorEmpresa>()
            {
                new FornecedorEmpresa()
                {
                    Empresa = EmpresaTeste.ObterEmpresa(true),
                    Fornecedor = fornecedorAux
                }
            };
            var empresas = new List<Empresa>
            {
                fornecedorEmpresa[0].Empresa
            };

            var data = DateTime.Now.ToString("MM/dd HH:mm:ss.fff");
            fornecedorAux.CNPJ = Global.GerarCnpj(true);
            fornecedorAux.RazaoSocial = "RazaoSocial-" + data;
            fornecedorAux.NomeFantasia = "NomeFantasia-" + data;
            fornecedorAux.NomeContato = "NomeContato-" + data;
            fornecedorAux.TelefoneContato = Global.GerarTelefone(Global.ObterEnumAleatorio<TipoTelefone>());
            fornecedorAux.EmailContato = Global.GerarEmail();
            fornecedorAux.Empresas = fornecedorEmpresa;

            var result = controller.Incluir(fornecedorAux, empresas) as JsonResult;
            Assert.IsNotNull(result);

            var fornecedor = JsonHelper.GetJsonObjectRepresentation<FornecedorModelView>(result);
            Assert.IsNotNull(fornecedor);
            Assert.AreEqual(fornecedorAux.CNPJ, fornecedor.CNPJ.Replace(".", "").Replace("-", "").Replace("/", ""));
            Assert.AreEqual(fornecedorAux.RazaoSocial, fornecedor.RazaoSocial);
            Assert.AreEqual(fornecedorAux.NomeFantasia, fornecedor.NomeFantasia);
            Assert.AreEqual(fornecedorAux.NomeContato, fornecedor.NomeContato);
            Assert.AreEqual(fornecedorAux.TelefoneContato, fornecedor.TelefoneContato);
            Assert.AreEqual(fornecedorAux.EmailContato, fornecedor.EmailContato);

            //TODO: Validar Empresas
        }

        /// <summary>
        /// Pode alterar.
        /// </summary>
        [TestMethod]
        public void Pode_Alterar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new FornecedorController(unidTrabalho);
            var fornecedorAux = ObterFornecedor(true);
            Assert.IsNotNull(fornecedorAux);
            Assert.IsTrue(fornecedorAux.Ativo);

            var data = DateTime.Now.ToString("MM/dd HH:mm:ss.fff");
            var razaoSocial = "RazaoSocial-" + data;
            var nomeFantasia = "NomeFantasia-" + data;
            var nomeContato = "NomeContato-" + data;
            var telefone = Global.GerarTelefone(Global.ObterEnumAleatorio<TipoTelefone>());
            var email = Global.GerarEmail();
            var fornecedorEmpresa = new List<FornecedorEmpresa>()
            {
                new FornecedorEmpresa()
                {
                    Empresa = EmpresaTeste.ObterEmpresa(true),
                    Fornecedor = fornecedorAux
                }
            };
            var empresas = new Empresa[]
            {
                fornecedorEmpresa[0].Empresa
            };

            fornecedorAux.RazaoSocial = razaoSocial;
            fornecedorAux.NomeFantasia = nomeFantasia;
            fornecedorAux.NomeContato = nomeContato;
            fornecedorAux.TelefoneContato = telefone;
            fornecedorAux.EmailContato = email;
            fornecedorAux.Empresas = fornecedorEmpresa;

            var result = controller.Alterar(fornecedorAux, empresas, null) as JsonResult;
            Assert.IsNotNull(result.Data);

            var fornecedor = JsonHelper.GetJsonObjectRepresentation<FornecedorModelView>(result);
            Assert.IsNotNull(fornecedor);
            Assert.AreEqual(fornecedorAux.Codigo, fornecedor.Codigo);
            Assert.AreEqual(razaoSocial, fornecedor.RazaoSocial);
            Assert.AreEqual(nomeFantasia, fornecedor.NomeFantasia);
            Assert.AreEqual(nomeContato, fornecedor.NomeContato);
            Assert.AreEqual(telefone.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", ""), fornecedor.TelefoneContato);
            Assert.AreEqual(email, fornecedor.EmailContato);

            //TODO: Validar Empresas
        }

        /// <summary>
        /// Valida Ativar Fornecedor.
        /// </summary>
        [TestMethod]
        public void Pode_Ativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new FornecedorController(unidTrabalho);
            var lista = ObterListaFornecedor();
            Assert.IsTrue(lista.Any(), Resources.Lista_vazia);

            lista = lista.Where(x => x.Ativo == false).ToList();
            Assert.IsTrue(lista.Any(), Resources.Lista_vazia);

            var fornecedorAux = lista[0];
            Assert.IsFalse(fornecedorAux.Ativo);

            var result = controller.AtivarInativar(fornecedorAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var fornecedor = JsonHelper.GetJsonObjectRepresentation<FornecedorModelView>(result);
            Assert.IsNotNull(fornecedor);
            Assert.AreEqual(fornecedorAux.Codigo, fornecedor.Codigo);
            Assert.IsTrue(fornecedor.Ativo);
        }

        /// <summary>
        /// Valida Inativar Fornecedor.
        /// </summary>
        [TestMethod]
        public void Pode_Inativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new FornecedorController(unidTrabalho);
            var fornecedorAux = ObterFornecedor(true);
            Assert.IsTrue(fornecedorAux.Ativo);

            var result = controller.AtivarInativar(fornecedorAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var fornecedor = JsonHelper.GetJsonObjectRepresentation<FornecedorModelView>(result);
            Assert.IsNotNull(fornecedor);
            Assert.AreEqual(fornecedorAux.Codigo, fornecedor.Codigo);
            Assert.IsFalse(fornecedor.Ativo);
        }

        /// <summary>
        /// Busca aleatóriamente um fornecedor cadastrado na base de dados.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// Filtrar fornecedores ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Fornecedor.
        /// </returns>
        public static Fornecedor ObterFornecedor(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<Fornecedor>(unidTrabalho);
            var fornecedores = repositorio.ObterTodos().ToList();
            Assert.IsTrue(fornecedores.Any());

            Random rand = new Random();
            fornecedores = fornecedores.OrderBy(x => rand.Next()).ToList();

            Fornecedor fornecedor = null;
            foreach (var item in fornecedores)
            {
                fornecedor = item;

                if (fornecedor == null)
                    continue;
                if (somenteAtivos)
                {
                    if (fornecedor.Ativo)
                        break;
                    fornecedor = null;
                }
                else
                    break;
            }

            return fornecedor;
        }

        /// <summary>
        /// Obter lista fornecedor.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// Filtrar fornecedores ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;FornecedorModelView&gt;
        /// </returns>
        private List<FornecedorModelView> ObterListaFornecedor(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new FornecedorController(unidTrabalho);
            var result = controller.ObterTodos() as JsonResult;
            Assert.IsNotNull(result);

            var fornecedores = JsonHelper.GetJsonObjectRepresentation<List<FornecedorModelView>>(result);
            var rand = new Random();
            fornecedores = fornecedores.OrderBy(x => rand.Next()).ToList();

            return somenteAtivos ? fornecedores.Where(x => x.Ativo).ToList() : fornecedores;
        }
    }
}
