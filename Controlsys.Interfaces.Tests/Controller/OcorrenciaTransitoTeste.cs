﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Controlsys.Dominio.Ocorrencias;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Ext.Net;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Controlsys.Repositorio.Ocorrencias;
using Ext.Net.MVC;

namespace Controlsys.Interfaces.Tests.Controller
{
    [TestClass]
    public class OcorrenciaTransitoTeste
    {

        [TestMethod]
        public void Incluir()
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();

            var rand = new Random();            
            var papel = ColaboradorTeste.ObterColaborador();
            var cracha = rand.Next(100000, 9999999).ToString();
            var local = LocalTeste.ObterLocal(true);
            var infracao = InfracaoTeste.ObterInfracao(true);
            var penalidade = PenalidadeTeste.ObterPenalidade(true);

            OcorrenciaTransitoController controller = new OcorrenciaTransitoController(unidTrabalho);
            //App_Start.Mock.UserHttpContext(controller, "rubem.almeida");
            
            var result = controller.Incluir(0, papel.PessoaFisica.Codigo, papel.PessoaFisica.Nome, papel.PessoaFisica.CPF, papel.PessoaFisica.Passaporte, cracha, DateTime.Now.Date, DateTime.MinValue.AddHours(rand.Next(1, 23)), false, local.Codigo,
                infracao.Codigo, penalidade.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var registroGerado = JsonHelper.GetJsonObjectRepresentation<OcorrenciaTransitoModelView[]>(result);
            Assert.IsNotNull(registroGerado);

            unidTrabalho.Rollback();
        }

        [TestMethod]
        public void Alterar()
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();

            OcorrenciaTransito registro = ObterListaOcorrenciaTransitos().FirstOrDefault();

            var cracha = new Random().Next(100000, 9999999).ToString();

            OcorrenciaTransitoController controller = new OcorrenciaTransitoController(unidTrabalho);
            App_Start.Mock.UserHttpContext(controller, "rubem.almeida");

            var result = controller.Alterar(registro.Codigo, registro.PessoaFisica.Codigo ,registro.Papel.Codigo, cracha, DateTime.Now, DateTime.Now, registro.Anulada,registro.Local.Codigo,
                registro.Infracao.Codigo, registro.Penalidade.Codigo, registro.Treinamentos[0].CompareceuNotificacao, registro.Treinamentos[0].DataAgendamento,
                registro.Treinamentos[0].CompareceuTreinamento, registro.Treinamentos[0].Apto) as JsonResult;
            Assert.IsNotNull(result);

            var registroAlterado = JsonHelper.GetJsonObjectRepresentation<OcorrenciaTransitoModelView[]>(result);

            Assert.IsNotNull(registroAlterado);
            Assert.AreEqual(registro.Codigo, registroAlterado[0].CodigoOcorrencia);

            unidTrabalho.Rollback();
        }

        [TestMethod]
        public void Selecionar()
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();

            OcorrenciaTransito registro = ObterListaOcorrenciaTransitos().FirstOrDefault();

            OcorrenciaTransitoController controller = new OcorrenciaTransitoController(unidTrabalho);
            var result = controller.Selecionar(registro.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var registroAux = JsonHelper.GetJsonObjectRepresentation<OcorrenciaTransitoModelView[]>(result);

            Assert.IsNotNull(registroAux);
            Assert.AreEqual(registro.Codigo, registroAux[0].CodigoOcorrencia);

            unidTrabalho.Rollback();
        }

        [TestMethod]
        public void Ativar()
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();

            OcorrenciaTransito registro = ObterListaOcorrenciaTransitos().Where(x=>x.Ativo == false).FirstOrDefault();

            OcorrenciaTransitoController controller = new OcorrenciaTransitoController(unidTrabalho);
            var result = controller.AtivarInativar(registro.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var registroAux = JsonHelper.GetJsonObjectRepresentation<OcorrenciaTransitoModelView[]>(result);

            Assert.IsNotNull(registroAux);
            Assert.AreEqual(registro.Codigo, registroAux[0].CodigoOcorrencia);
            Assert.IsTrue(registroAux[0].Ativo);

            unidTrabalho.Rollback();
        }

        [TestMethod]
        public void Inativar()
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();

            OcorrenciaTransito registro = ObterListaOcorrenciaTransitos(true).FirstOrDefault();

            OcorrenciaTransitoController controller = new OcorrenciaTransitoController(unidTrabalho);
            var result = controller.AtivarInativar(registro.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var registroAux = JsonHelper.GetJsonObjectRepresentation<OcorrenciaTransitoModelView[]>(result);

            Assert.IsNotNull(registroAux);
            Assert.AreEqual(registro.Codigo, registroAux[0].CodigoOcorrencia);
            Assert.IsFalse(registroAux[0].Ativo);

            unidTrabalho.Rollback();
        }

        [TestMethod]
        public void Pesquisar()
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();

            OcorrenciaTransitoController controller = new OcorrenciaTransitoController(unidTrabalho);

            var x = new Dictionary<string, object>() { };
            x.Add("start", (int)0);
            x.Add("limit", (int)25);

            var result = controller.Pesquisar(new StoreRequestParameters(x)) as StoreResult;
            Assert.IsNotNull(result);

            var emissaoCracha = result.Data as OcorrenciaTransitoModelView[];
            Assert.IsNotNull(emissaoCracha);

            unidTrabalho.Rollback();
        }

        public static List<OcorrenciaTransito> ObterListaOcorrenciaTransitos(bool somenteAtivos = false)
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            IRepositorioOcorrenciaTransito repOcorrenciaTransito = Fabrica.Instancia.ObterRepositorio<IRepositorioOcorrenciaTransito>(unidTrabalho);

            IQueryable<OcorrenciaTransito> lista = repOcorrenciaTransito.ObterTodos();

            if (somenteAtivos)
                lista = lista.Where(x => x.Ativo);

            return lista.ToList().OrderBy(x => new Random().Next()).ToList();
        }
    }
}
