﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Controlsys.Dominio.Pessoas;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Controlsys.Persistencia;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) PessoaTeste.
    /// </summary>
    [TestClass]
    public class PessoaTeste
    {
        /// <summary>
        /// Valida incluir selecionar uma Pessoa pelo código.
        /// </summary>
        [TestMethod]
        public void Pode_Selecionar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repPessoaFisica = new Repositorio<PessoaFisica>(unidTrabalho);
            var listPessoaFisica = repPessoaFisica.ObterTodos().ToList();
            Assert.IsNotNull(listPessoaFisica);
            Assert.IsTrue(listPessoaFisica.Any());

            var rand = new Random();
            listPessoaFisica = listPessoaFisica.OrderBy(x => rand.Next()).ToList();

            var pFisicaAux = listPessoaFisica[0];

            var controller = new PessoaController(unidTrabalho);
            var result = controller.Selecionar(pFisicaAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var pFisica = JsonHelper.GetJsonObjectRepresentation<PessoaFisicaModelView>(result);
            Assert.AreEqual(pFisicaAux.Codigo, pFisica.Codigo);
        }

        /// <summary>
        /// Valida obter uma pessoa informando CPF.
        /// </summary>
        [TestMethod]
        public void Obter_Por_Cpf()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repPessoaFisica = new Repositorio<PessoaFisica>(unidTrabalho);
            var listPessoaFisica = repPessoaFisica.ObterTodos().ToList();
            Assert.IsNotNull(listPessoaFisica);
            Assert.IsTrue(listPessoaFisica.Any());

            var rand = new Random();
            listPessoaFisica = listPessoaFisica.OrderBy(x => rand.Next()).ToList();

            var pFisicaAux = listPessoaFisica.First(x => !string.IsNullOrEmpty(x.CPF));
            Assert.IsNotNull(pFisicaAux);

            var controller = new PessoaController(unidTrabalho);
            var result = controller.ObterPorCpfPassaporte(false, pFisicaAux.CPF, null) as JsonResult;
            Assert.IsNotNull(result);

            var pFisica = JsonHelper.GetJsonObjectRepresentation<PessoaFisicaModelView>(result);
            Assert.AreEqual(pFisicaAux.Codigo, pFisica.Codigo);
            Assert.AreEqual(pFisicaAux.CPF, pFisica.CPF.Replace(".", "").Replace("-", ""));
        }

        /// <summary>
        /// Valida obter uma pessoa informando PASSAPORTE.
        /// </summary>
        [TestMethod]
        public void Obter_Por_Passaporte()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repPessoaFisica = new Repositorio<PessoaFisica>(unidTrabalho);
            var listPessoaFisica = repPessoaFisica.ObterTodos().ToList();
            Assert.IsNotNull(listPessoaFisica);
            Assert.IsTrue(listPessoaFisica.Any());

            var rand = new Random();
            listPessoaFisica = listPessoaFisica.OrderBy(x => rand.Next()).ToList();

            var pFisicaAux = listPessoaFisica.FirstOrDefault(x => !string.IsNullOrEmpty(x.Passaporte));
            Assert.IsNotNull(pFisicaAux);

            var controller = new PessoaController(unidTrabalho);
            var result = controller.ObterPorCpfPassaporte(false, null, pFisicaAux.Passaporte) as JsonResult;
            Assert.IsNotNull(result);

            var pFisica = JsonHelper.GetJsonObjectRepresentation<PessoaFisicaModelView>(result);
            Assert.AreEqual(pFisicaAux.Codigo, pFisica.Codigo);
            Assert.AreEqual(pFisicaAux.Passaporte, pFisica.Passaporte);
        }

        /// <summary>
        /// Busca aleatóriamente um objeto Pessoa cadastrado na base de dados.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) Pessoa.
        /// </returns>
        public static Pessoa ObterPessoa(bool ativo = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            PessoaController controller = new PessoaController(unidTrabalho);
            JsonResult result = controller.ObterTodos(ativo) as JsonResult;
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Data);

            var pessoas = JsonHelper.GetJsonObjectRepresentation<List<Pessoa>>(result);
            Assert.IsTrue(pessoas.Any());

            Random rand = new Random();
            pessoas = pessoas.OrderBy(x => rand.Next()).ToList();
            return pessoas[0];
        }

        /// <summary>
        /// Obter lista pessoas fisica.
        /// </summary>
        ///
        /// <param name="verificapapel">
        /// true to verificapapel.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;PessoaFisica&gt;
        /// </returns>
        public static List<PessoaFisica> ObterListaPessoasFisica(bool verificapapel = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repPessoaFisica = new Repositorio<PessoaFisica>(unidTrabalho);
            var listaPessoaFisica = repPessoaFisica.ObterTodos().ToList();
            Assert.IsNotNull(listaPessoaFisica);
            Assert.IsTrue(listaPessoaFisica.Any());

            var rand = new Random();
            listaPessoaFisica = listaPessoaFisica.OrderBy(x => rand.Next()).ToList();

            if (verificapapel)
            {
                listaPessoaFisica = listaPessoaFisica.Where(pf => pf.Papeis.Any() && pf.Papeis.Any(p => p.PapelLogs.OrderByDescending(pl => pl.Codigo).Select(pl => pl.Status).FirstOrDefault() != StatusPapelLog.BaixaDefinitiva)).ToList();
            }
            return listaPessoaFisica;
        }
    }
}
