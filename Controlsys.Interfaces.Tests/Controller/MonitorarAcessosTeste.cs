﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web.Mvc;
using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Seguranca;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Controlsys.Interfaces.Tests.Properties;
using Controlsys.Persistencia.Repositorios.Acesso;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Controlsys.Persistencia;
using Ninject.Activation;
using Rhino.Mocks;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) MonitorarAcessosTeste.
    /// </summary>
    [TestClass]
    public class MonitorarAcessosTeste
    {
        /// <summary>
        /// Pode selecionar.
        /// </summary>
        [TestMethod]
        public void Pode_Selecionar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new MonitorarAcessosController(unidTrabalho);
            var rep = new RepositorioControleAcesso(unidTrabalho);
            var rand = new Random();
            var listaAux = rep.ObterTodos().ToList();
            Assert.IsNotNull(listaAux);
            Assert.IsTrue(listaAux.Any());

            listaAux = listaAux.OrderBy(x => rand.Next()).ToList();
            var controlAux = listaAux.FirstOrDefault();
            Assert.IsNotNull(controlAux);

            JsonResult result = controller.Selecionar(controlAux.Codigo, false) as JsonResult;
            Assert.IsNotNull(result);

            var acesso = result.Data as MonitorarAcessosModelView;
            Assert.IsNotNull(acesso);
            Assert.AreEqual(controlAux.Codigo, acesso.Codigo);
        }

        /// <summary>
        /// Retorna todos os objetos do tipo MonitorarAcessos.
        /// </summary>
        [TestMethod]
        public void ObterTodos()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new MonitorarAcessosController(unidTrabalho);

            //controller.ControllerContext.HttpContext = new MockHttpContext("", "");
            //controller.ControllerContext.HttpContext.Server.Expect(a => a.MapPath(Arg<string>.Is.Anything)).Return("FakePath");

            var result = controller.ObterTodos(null, false) as JsonResult;

            Assert.IsNotNull(result);
            var lista = result.Data as List<MonitorarAcessosModelView>;
            Assert.IsNotNull(lista);
            Assert.IsTrue(lista.Any());
        }
    }
}
