﻿using System;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Collections.Generic;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Controlsys.Infra;
using Globalsys;
using Controlsys.Interfaces.Controllers;
using Controlsys.Dominio.Parametros;
using System.Web.Mvc;
using System.Collections.Generic;
using Controlsys.Persistencia;

namespace Controlsys.Interfaces.Tests.Controller
{
    [TestClass]
    public class PenalidadeTeste
    {
        /// <summary>
        /// Pode incluir.
        /// </summary>
        [TestMethod]
        public void Pode_Incluir()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            PenalidadeController controller = new PenalidadeController(unidTrabalho);
            string data = DateTime.Now.ToString("MM/dd HH:mm:ss.fff");
            Penalidade penalidadeNovo = new Penalidade()
            {
                Nome = "Nome-" + data,
                Sigla = "Sigla-" + data
            };

            var result = controller.Incluir(penalidadeNovo) as JsonResult;
            var penalidade = JsonHelper.GetJsonObjectRepresentation<Penalidade>(result);
            Assert.IsNotNull(penalidade);

            Assert.AreEqual("Nome-" + data, penalidade.Nome);
            Assert.AreEqual("Sigla-" + data, penalidade.Sigla);
        }

        /// <summary>
        /// Pode alterar.
        /// </summary>
        [TestMethod]
        public void Pode_Alterar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            PenalidadeController controller = new PenalidadeController(unidTrabalho);
            JsonResult result = controller.ObterTodos() as JsonResult;
            var penalidades = JsonHelper.GetJsonObjectRepresentation<List<Penalidade>>(result);
            Assert.IsNotNull(penalidades);

            Random rand = new Random();
            penalidades = penalidades.OrderBy(x => rand.Next()).ToList();

            Penalidade penalidadeAux = null;
            foreach (var item in penalidades)
            {
                result = controller.Selecionar(item.Codigo) as JsonResult;
                penalidadeAux = JsonHelper.GetJsonObjectRepresentation<Penalidade>(result);
                if (penalidadeAux == null)
                    continue;
                if (penalidadeAux.Ativo)
                    break;
                penalidadeAux = null;
            }
            Assert.IsNotNull(penalidadeAux);

            Assert.IsTrue(penalidadeAux.Ativo);
            string data = DateTime.Now.ToString("MM/dd HH:mm:ss.fff");
            penalidadeAux.Nome = "Nome-" + data;
            penalidadeAux.Sigla = "Sigla-" + data;
            //var penalidade = (controller.Alterar(penalidadeAux) as JsonResult).Data as penalidade;
            result = controller.Alterar(penalidadeAux) as JsonResult;
            var penalidade = JsonHelper.GetJsonObjectRepresentation<Penalidade>(result);
            Assert.AreEqual("Nome-" + data, penalidade.Nome);
            Assert.AreEqual("Sigla-" + data, penalidade.Sigla);

        }

        /// <summary>
        /// Pode ativar.
        /// </summary>
        [TestMethod]
        public void Pode_Ativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            PenalidadeController controller = new PenalidadeController(unidTrabalho);

            var repositorio = new Repositorio<Penalidade>(unidTrabalho);
            var penalidades = repositorio.ObterTodos().Where(x => x.Ativo == false).ToList();
            Assert.IsNotNull(penalidades);
            Assert.IsTrue(penalidades.Any());

            var rand = new Random();
            penalidades = new List<Penalidade>(penalidades.OrderBy(x => rand.Next()));

            Penalidade penalidadeAux = penalidades.ToList()[0];
            Assert.IsNotNull(penalidadeAux);
            Assert.IsFalse(penalidadeAux.Ativo);

            var result = controller.AtivarInativar(penalidadeAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            //var penalidade = result.Data as penalidade;
            var penalidade = JsonHelper.GetJsonObjectRepresentation<Penalidade>(result);
            Assert.IsNotNull(penalidade);
            Assert.AreEqual(penalidadeAux.Codigo, penalidade.Codigo);
            Assert.IsTrue(penalidade.Ativo);
        }

        /// <summary>
        /// Pode inativar.
        /// </summary>
        [TestMethod]
        public void Pode_Inativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            PenalidadeController controller = new PenalidadeController(unidTrabalho);

            Penalidade penalidadeAux = ObterPenalidade(true);
            Assert.IsNotNull(penalidadeAux);
            Assert.IsTrue(penalidadeAux.Ativo);

            var result = controller.AtivarInativar(penalidadeAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            //var penalidade = result.Data as penalidade;
            var penalidade = JsonHelper.GetJsonObjectRepresentation<Penalidade>(result);
            Assert.IsNotNull(penalidade);
            Assert.AreEqual(penalidadeAux.Codigo, penalidade.Codigo);
            Assert.IsFalse(penalidade.Ativo);
        }

        /// <summary>
        /// Obter penalidade.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// true to somente ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) penalidade.
        /// </returns>
        public static Penalidade ObterPenalidade(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            PenalidadeController controller = new PenalidadeController(unidTrabalho);
            JsonResult result = controller.ObterTodos(somenteAtivos) as JsonResult;
            var penalidades = JsonHelper.GetJsonObjectRepresentation<List<Penalidade>>(result);
            Assert.IsNotNull(penalidades);

            Random rand = new Random();
            penalidades = penalidades.OrderBy(x => rand.Next()).ToList();

            Penalidade penalidadeAux = null;
            foreach (var item in penalidades)
            {
                result = controller.Selecionar(item.Codigo) as JsonResult;
                Assert.IsNotNull(result.Data);
                penalidadeAux = JsonHelper.GetJsonObjectRepresentation<Penalidade>(result);

                if (somenteAtivos)
                {
                    if (penalidadeAux.Ativo)
                        break;
                    penalidadeAux = null;
                }
                else
                    break;
            }

            return penalidadeAux;
        }

        /// <summary>
        /// Obter lista penalidade.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// true to somente ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;penalidade&gt;
        /// </returns>
        public static List<Penalidade> ObterListaPenalidade(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<Penalidade>(unidTrabalho);
            var penalidades = repositorio.ObterTodos().ToList();
            Assert.IsTrue(penalidades.Any());

            var rand = new Random();
            penalidades = penalidades.OrderBy(x => rand.Next()).ToList();

            return somenteAtivos ? penalidades.Where(x => x.Ativo).ToList() : penalidades;
        }
    }
}
