﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Controlsys.Dominio.Empresas;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) RelatorioHomemHoraTeste.
    /// </summary>
    [TestClass]
    public class RelatorioHomemHoraTeste
    {
        /// <summary>
        /// Pode exportar PDF.
        /// </summary>
        [TestMethod]
        public void Pode_ExportarPDF()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RelatorioHomemHoraController controller = new RelatorioHomemHoraController(unidTrabalho);

            List<string> selectedFields = typeof(RelatorioHomemHoraModelView).GetProperties().Select(pi => pi.Name).ToList();

            Empresa empresa = EmpresaTeste.ObterEmpresa();
            Assert.IsNotNull(empresa);

            var result = controller.Exportar(selectedFields, "PDF", "", empresa.Codigo, null, DateTime.Now.AddYears(-10), DateTime.Now) as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/pdf", result.ContentType);
            Assert.AreEqual("download", result.FileDownloadName);
        }

        /// <summary>
        /// Pode exportar excel.
        /// </summary>
        [TestMethod]
        public void Pode_ExportarExcel()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RelatorioHomemHoraController controller = new RelatorioHomemHoraController(unidTrabalho);

            List<string> selectedFields = typeof(RelatorioHomemHoraModelView).GetProperties().Select(pi => pi.Name).ToList();

            Empresa empresa = EmpresaTeste.ObterEmpresa();
            Assert.IsNotNull(empresa);

            var result = controller.Exportar(selectedFields, "Excel", "", empresa.Codigo, null, DateTime.Now.AddYears(-10), DateTime.Now) as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/vnd.ms-excel", result.ContentType);
            Assert.AreEqual("download", result.FileDownloadName);
        }

        /// <summary>
        /// Pode exportar word.
        /// </summary>
        [TestMethod]
        public void Pode_ExportarWord()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RelatorioHomemHoraController controller = new RelatorioHomemHoraController(unidTrabalho);

            List<string> selectedFields = typeof(RelatorioHomemHoraModelView).GetProperties().Select(pi => pi.Name).ToList();

            Empresa empresa = EmpresaTeste.ObterEmpresa();
            Assert.IsNotNull(empresa);

            var result = controller.Exportar(selectedFields, "Word", "", empresa.Codigo, null, DateTime.Now.AddYears(-10), DateTime.Now) as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/msword", result.ContentType);
            Assert.AreEqual("download", result.FileDownloadName);
        }

        /// <summary>
        /// Pode pesquisar.
        /// </summary>
        [TestMethod]
        public void Pode_Pesquisar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RelatorioHomemHoraController controller = new RelatorioHomemHoraController(unidTrabalho);

            Empresa empresa = EmpresaTeste.ObterEmpresa();
            Assert.IsNotNull(empresa);

            var result = controller.Pesquisar(null, "", empresa.Codigo, null, DateTime.Now.AddYears(-10), DateTime.Now) as JsonResult;
            Assert.IsNotNull(result);

            var registros = result.Data as List<RelatorioHomemHoraModelView>;
            Assert.IsNotNull(registros);
        }

        /// <summary>
        /// Obter hht.
        /// </summary>
        ///
        /// <param name="responsavel">
        /// O(a) responsavel.
        /// </param>
        /// <param name="empresa">
        /// O(a) empresa.
        /// </param>
        /// <param name="setor">
        /// O(a) setor.
        /// </param>
        /// <param name="dataInicio">
        /// O(a) data inicio.
        /// </param>
        /// <param name="dataFim">
        /// O(a) data fim.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;RelatorioHomemHoraModelView&gt;
        /// </returns>
        private List<RelatorioHomemHoraModelView> obterHHT(string responsavel, string empresa, int? setor, DateTime? dataInicio, DateTime? dataFim)
        {
            string sql = @"DECLARE @table TABLE(DT_ENTRADA DATETIME, DT_SAIDA DATETIME, HORAS INT, CD_PAPEL INT)

                           INSERT INTO @table (DT_ENTRADA, DT_SAIDA, HORAS, CD_PAPEL)
                           SELECT AC_E.DT_LEITORA DT_ENTRADA, MIN(AC_S.DT_LEITORA) DT_SAIDA, (DATEDIFF(SECOND, AC_E.DT_LEITORA, MIN(AC_S.DT_LEITORA)) / 3600) AS HORAS, AC_E.CD_PAPEL
                             FROM ACESSO AC_E
                           INNER JOIN ACESSO AC_S ON AC_S.CD_ACESSO > AC_E.CD_ACESSO AND AC_E.CD_PAPEL = AC_S.CD_PAPEL
                           WHERE AC_E.TX_DIRECAO = 'Entrada' AND AC_S.TX_DIRECAO = 'Saida'
                           GROUP BY AC_E.DT_LEITORA, AC_E.CD_PAPEL

                           SELECT SC.TX_DESCRICAO AS Responsavel, 
                                  AC.CD_SETOR AS SetorCodigo, 
                                  SC.TX_NOME AS Setor, 
                                  PED.CD_PEDIDO AS PedidoCodigo, 
                                  EMP.TX_NOME AS Empresa , 
                                  EMP_PAI.TX_NOME EmpresaPai,	   	   
                                  COUNT(AC.CD_PAPEL) QtdColab,
                                  SUM(ACSS.HORAS) HorasTrabalhadas
                           FROM PEDIDO PED
                           INNER JOIN PESSOA EMP ON EMP.CD_PESSOA = PED.CD_EMPRESA
                           INNER JOIN PEDIDO PED_PAI ON PED_PAI.CD_PEDIDO = PED.CD_PEDIDO_PAI
                           INNER JOIN PESSOA EMP_PAI ON EMP_PAI.CD_PESSOA = PED_PAI.CD_EMPRESA
                           INNER JOIN ALOCACAO_COLABORADOR AC ON AC.CD_PEDIDO = PED.CD_PEDIDO
                           INNER JOIN SETOR_CUSTO SC ON SC.CD_SETOR = AC.CD_SETOR
                           INNER JOIN (
                               SELECT SUM(HORAS) HORAS, CD_PAPEL FROM @table
                               WHERE (:dtInicio IS NULL OR DT_ENTRADA >= :dtInicio)
	                             AND (:dtFim IS NULL OR DT_SAIDA <= :dtFim)
                               GROUP BY CD_PAPEL    
                           ) ACSS ON  ACSS.CD_PAPEL = AC.CD_PAPEL";

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            List<string> filtros = new List<string>();

            parameters.Add("dtInicio", dataInicio);
            parameters.Add("dtFim", dataFim);

            if (!string.IsNullOrEmpty(responsavel))
            {
                parameters.Add("responsavel", responsavel);
                filtros.Add("SC.TX_DESCRICAO = :responsavel");
            }

            if (!string.IsNullOrEmpty(empresa))
            {
                parameters.Add("empresa", empresa);
                filtros.Add("EMP_PAI.CD_PESSOA = :empresa OR EMP.CD_PESSOA = :empresa");
            }

            if (setor.HasValue)
            {
                parameters.Add("setor", setor);
                filtros.Add("AC.CD_SETOR = :setor");
            }

            if (filtros.Any())
            {
                sql += " WHERE " + string.Join("AND ", filtros);
            }

            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            return unidTrabalho.ExecuteSql<RelatorioHomemHoraModelView>(sql, parameters).ToList();
        }
    }
}
