﻿using System;
using System.Linq;
using System.Web.Mvc;
using Controlsys.Dominio.Parametros;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Controlsys.Dominio.Acesso;

namespace Controlsys.Interfaces.Tests.Controller
{
    [TestClass]
    public class RestauranteTeste
    {
        [TestMethod]
        public void Incluir()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            
            #region [Obter GrupoLeitora]
            GrupoLeitoraController grupoLeitoraController = new GrupoLeitoraController(unidTrabalho);

            var result1 = grupoLeitoraController.ObterTodos() as JsonResult;
            var lista1 = JsonHelper.GetJsonObjectRepresentation<List<GrupoLeitoraModelView>>(result1);

            Assert.IsNotNull(lista1);
            Assert.IsTrue(lista1.Count > 0);

            var grupoLeitora = lista1.OrderBy(x => new Random().Next()).Select(x => new GrupoLeitora()
            {
                Codigo = x.Codigo,
                Nome = x.Nome
            }).FirstOrDefault();

            Assert.IsNotNull(grupoLeitora, "Nenhum registro encontrado para ser utilizado como referência de teste no AtivarInativar");
            #endregion


            #region [Obter TipoAlimentacao]
            TipoAlimentacaoController tipoAlimentacaoController = new TipoAlimentacaoController(unidTrabalho);

            var result2 = tipoAlimentacaoController.ObterTodos() as JsonResult;
            var lista2 = JsonHelper.GetJsonObjectRepresentation<List<TipoAlimentacao>>(result2);

            Assert.IsNotNull(lista2);
            Assert.IsTrue(lista2.Count > 0);

            var lista3 = new List<TipoAlimentacaoRestaurante>();
            for (int i = 0; i < new Random().Next(1, lista2.Count); i++)
            {

                var tipoAlimentacaoRestaurante = new TipoAlimentacaoRestaurante
                {
                    HoraInicio = new TimeSpan(new Random().Next(0, 22), new Random().Next(0, 59), 0),
                    HoraFim = new TimeSpan(new Random().Next(0, 23), new Random().Next(0, 59), 0),
                    TipoAlimentacao = new TipoAlimentacao
                    {
                        Codigo = lista2[i].Codigo,
                        Nome = lista2[i].Nome
                    }
                };
                lista3.Add(tipoAlimentacaoRestaurante);
            }
            #endregion

            RestauranteController controller = new RestauranteController(unidTrabalho);
            Restaurante registro = new Restaurante
            {
                Nome = new Random().Next().ToString(),
                GrupoLeitora = grupoLeitora
            };
            //var result = controller.Incluir(registro, lista3) as JsonResult;
            //var registroGerado = JsonHelper.GetJsonObjectRepresentation<RestauranteModelView>(result);

            //Assert.IsNotNull(registro);
            //Assert.AreEqual(registro.Nome, registroGerado.Nome);

            //Assert.AreEqual(registro.GrupoLeitora.Codigo, registroGerado.GrupoLeitora);            

            //CollectionAssert.Contains(
            //    new[] { lista3.Select(y => y.TipoAlimentacao.Codigo) },
            //    new[] { registroGerado.TiposAlimentacao.Select(x => x.Codigo) }
            //);

            unidTrabalho.Rollback();
        }

        [TestMethod]
        public void Alterar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();

            var controller = new RestauranteController(unidTrabalho);

            #region [Obter Restaurante]
            var result0 = controller.ObterTodos() as JsonResult;
            var lista0 = JsonHelper.GetJsonObjectRepresentation<List<RestauranteModelView>>(result0);

            Assert.IsNotNull(lista0);
            Assert.IsTrue(lista0.Count > 0);

            var registro0 = lista0.OrderBy(x => new Random().Next()).FirstOrDefault();

            Assert.IsNotNull(registro0, "Nenhum registro encontrado para ser utilizado como referência de teste no Alterar");
            #endregion

            #region [Obter GrupoLeitora]
            GrupoLeitoraController grupoLeitoraController = new GrupoLeitoraController(unidTrabalho);

            var result1 = grupoLeitoraController.ObterTodos() as JsonResult;
            var lista1 = JsonHelper.GetJsonObjectRepresentation<List<GrupoLeitoraModelView>>(result1);

            Assert.IsNotNull(lista1);
            Assert.IsTrue(lista1.Count > 0);

            var grupoLeitora = lista1.OrderBy(x => new Random().Next()).Select(x => new GrupoLeitora()
            {
                Codigo = x.Codigo,
                Nome = x.Nome
            }).FirstOrDefault();

            Assert.IsNotNull(grupoLeitora, "Nenhum registro encontrado para ser utilizado como referência de teste no AtivarInativar");
            #endregion


            #region [Obter TipoAlimentacao]
            TipoAlimentacaoController tipoAlimentacaoController = new TipoAlimentacaoController(unidTrabalho);

            var result2 = tipoAlimentacaoController.ObterTodos() as JsonResult;
            var lista2 = JsonHelper.GetJsonObjectRepresentation<List<TipoAlimentacao>>(result2);

            Assert.IsNotNull(lista2);
            Assert.IsTrue(lista2.Count > 0);

            var lista3 = new List<TipoAlimentacaoRestaurante>();
            for (int i = 0; i < new Random().Next(1, lista2.Count); i++)
            {

                var tipoAlimentacaoRestaurante = new TipoAlimentacaoRestaurante
                {
                    HoraInicio = new TimeSpan(new Random().Next(0, 22), new Random().Next(0, 59), 0),
                    HoraFim = new TimeSpan(new Random().Next(0, 23), new Random().Next(0, 59), 0),
                    TipoAlimentacao = new TipoAlimentacao
                    {
                        Codigo = lista2[i].Codigo,
                        Nome = lista2[i].Nome
                    }
                };
                lista3.Add(tipoAlimentacaoRestaurante);
            }
            #endregion

            Restaurante registro = new Restaurante
            {
                Codigo = registro0.Codigo,
                Nome = new Random().Next().ToString(),
                GrupoLeitora = grupoLeitora
            };

            //TODO: Falta testar a removeção de registros de TipoAlimentação

            //var result = controller.Alterar(registro, null,lista3.ToArray(),null, null) as JsonResult;
            //var registroGerado = JsonHelper.GetJsonObjectRepresentation<RestauranteModelView>(result);

            //Assert.IsNotNull(registro);
            //Assert.AreEqual(registro.Nome, registroGerado.Nome);

            //Assert.AreEqual(registro.GrupoLeitora.Codigo, registroGerado.GrupoLeitora);            

            //CollectionAssert.Contains(
            //    new[] { lista3.Select(y => y.TipoAlimentacao.Codigo) },
            //    new[] { registroGerado.TiposAlimentacao.Select(x => x.Codigo) }
            //);

            unidTrabalho.Rollback();
        }

        private RestauranteModelView incluirRestaurante(IUnidadeTrabalho unidTrabalho, RestauranteController controller)
        {
            #region [Obter GrupoLeitora]
            GrupoLeitoraController grupoLeitoraController = new GrupoLeitoraController(unidTrabalho);

            var result1 = grupoLeitoraController.ObterTodos() as JsonResult;
            var lista1 = JsonHelper.GetJsonObjectRepresentation<List<GrupoLeitoraModelView>>(result1);

            Assert.IsNotNull(lista1);
            Assert.IsTrue(lista1.Count > 0);

            var grupoLeitora = lista1.OrderBy(x => new Random().Next()).Select(x => new GrupoLeitora()
            {
                Codigo = x.Codigo,
                Nome = x.Nome
            }).FirstOrDefault();

            Assert.IsNotNull(grupoLeitora, "Nenhum registro encontrado para ser utilizado como referência de teste no AtivarInativar");
            #endregion


            #region [Obter TipoAlimentacao]
            TipoAlimentacaoController tipoAlimentacaoController = new TipoAlimentacaoController(unidTrabalho);

            var result2 = tipoAlimentacaoController.ObterTodos() as JsonResult;
            var lista2 = JsonHelper.GetJsonObjectRepresentation<List<TipoAlimentacao>>(result2);

            Assert.IsNotNull(lista2);
            Assert.IsTrue(lista2.Count > 0);

            var lista3 = new List<TipoAlimentacaoRestaurante>();
            for (int i = 0; i < new Random().Next(1, lista2.Count); i++)
            {

                var tipoAlimentacaoRestaurante = new TipoAlimentacaoRestaurante
                {
                    HoraInicio = new TimeSpan(new Random().Next(0, 22), new Random().Next(0, 59), 0),
                    HoraFim = new TimeSpan(new Random().Next(0, 23), new Random().Next(0, 59), 0),
                    TipoAlimentacao = new TipoAlimentacao
                    {
                        Codigo = lista2[i].Codigo,
                        Nome = lista2[i].Nome
                    }
                };
                lista3.Add(tipoAlimentacaoRestaurante);
            }
            #endregion
            
            //Restaurante registro = new Restaurante
            //{
            //    Nome = new Random().Next().ToString(),
            //    GrupoLeitora = grupoLeitora
            //};
            //var result = controller.Incluir(registro, lista3) as JsonResult;
            //var registroGerado = JsonHelper.GetJsonObjectRepresentation<RestauranteModelView>(result);

            //return registroGerado;
            return null;
        }

        [TestMethod]
        public void AtivarInativar()
        {
            
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();

            var controller = new RestauranteController(unidTrabalho);

            RestauranteModelView rest = incluirRestaurante(unidTrabalho, controller);            

            #region [ObterTodos]
            var result1 = controller.ObterTodos() as JsonResult;
            var lista = JsonHelper.GetJsonObjectRepresentation<List<RestauranteModelView>>(result1);

            Assert.IsNotNull(lista);
            Assert.IsTrue(lista.Count > 0);
            #endregion

            var registro = lista.Where(r => r.Codigo == rest.Codigo).FirstOrDefault();

            Assert.IsNotNull(registro, "Nenhum registro encontrado para ser utilizado como referência de teste no AtivarInativar");

            var result = controller.AtivarInativar(registro.Codigo) as JsonResult;
            var registroAlterado = JsonHelper.GetJsonObjectRepresentation<RestauranteModelView>(result);

            Assert.IsNotNull(registro, "Retornando registro nulo ao AtivarInativar");
            Assert.AreEqual(registro.Codigo,registroAlterado.Codigo,"Código diferente do esperado.");
            Assert.AreNotEqual(registro.Ativo, registroAlterado.Ativo, "Status diferente do esperado.");

            unidTrabalho.Rollback();
        }

        [TestMethod]
        public void Selecionar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RestauranteController controller = new RestauranteController(unidTrabalho);

            #region [ObterTodos]
            var result1 = controller.ObterTodos() as JsonResult;
            var lista = JsonHelper.GetJsonObjectRepresentation<List<RestauranteModelView>>(result1);

            Assert.IsNotNull(lista);
            Assert.IsTrue(lista.Count > 0);
            #endregion

            var registro = lista.OrderBy(x => new Random().Next()).FirstOrDefault();

            Assert.IsNotNull(registro, "Nenhum registro encontrado para ser utilizado como referência de teste no Selecionar");

            var result2 = controller.Selecionar(registro.Codigo) as JsonResult;
            var registroSelecionado = JsonHelper.GetJsonObjectRepresentation<TipoAlimentacao>(result2);

            Assert.IsNotNull(registroSelecionado, "Selecionar retornando nulo");
            Assert.AreEqual(registro.Codigo, registroSelecionado.Codigo, "Codigo diferente do valor utilizado como referência");
            Assert.AreEqual(registro.Nome, registroSelecionado.Nome, "Nome diferente do valor utilizado como referência");
        }

        [TestMethod]
        public void ObterTodos()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();

            RestauranteController controller = new RestauranteController(unidTrabalho);
            var result = controller.ObterTodos() as JsonResult;
            var lista = JsonHelper.GetJsonObjectRepresentation<List<RestauranteModelView>>(result);

            Assert.IsNotNull(lista);
            Assert.IsTrue(lista.Count > 0);
        }
    }
}
