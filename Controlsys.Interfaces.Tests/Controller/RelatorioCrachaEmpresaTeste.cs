﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Controlsys.Dominio.Seguranca;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Ext.Net;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Controlsys.Dominio.Empresas;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) RelatorioCrachaEmpresaTeste.
    /// </summary>
    [TestClass]
    public class RelatorioCrachaEmpresaTeste
    {
        /// <summary>
        /// Pode exportar PDF.
        /// </summary>
        [TestMethod]
        public void Pode_ExportarPDF()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RelatorioCrachaEmpresaController controller = new RelatorioCrachaEmpresaController(unidTrabalho);

            Empresa empresa = EmpresaTeste.ObterEmpresa(true);
            Assert.IsNotNull(empresa);

            List<string> selectedFields = typeof(RelatorioCrachaEmpresaModelView).GetProperties().Select(pi => pi.Name).ToList();

            var result = controller.Exportar(null, selectedFields, "PDF", empresa.Codigo) as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/pdf", result.ContentType);
            Assert.AreEqual("download", result.FileDownloadName);
        }

        /// <summary>
        /// Pode exportar excel.
        /// </summary>
        [TestMethod]
        public void Pode_ExportarExcel()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RelatorioCrachaEmpresaController controller = new RelatorioCrachaEmpresaController(unidTrabalho);

            Empresa empresa = EmpresaTeste.ObterEmpresa(true);
            Assert.IsNotNull(empresa);

            List<string> selectedFields = typeof(RelatorioCrachaEmpresaModelView).GetProperties().Select(pi => pi.Name).ToList();

            var result = controller.Exportar(null, selectedFields, "Excel", empresa.Codigo) as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/vnd.ms-excel", result.ContentType);
            Assert.AreEqual("download", result.FileDownloadName);
        }

        /// <summary>
        /// Pode exportar word.
        /// </summary>
        [TestMethod]
        public void Pode_ExportarWord()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RelatorioCrachaEmpresaController controller = new RelatorioCrachaEmpresaController(unidTrabalho);

            Empresa empresa = EmpresaTeste.ObterEmpresa(true);
            Assert.IsNotNull(empresa);

            List<string> selectedFields = typeof(RelatorioCrachaEmpresaModelView).GetProperties().Select(pi => pi.Name).ToList();

            var result = controller.Exportar(null, selectedFields, "Word", empresa.Codigo) as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/msword", result.ContentType);
            Assert.AreEqual("download", result.FileDownloadName);
        }

        /// <summary>
        /// Pode pesquisar.
        /// </summary>
        [TestMethod]
        public void Pode_Pesquisar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RelatorioCrachaEmpresaController controller = new RelatorioCrachaEmpresaController(unidTrabalho);

            Empresa empresa = EmpresaTeste.ObterEmpresa(true);
            Assert.IsNotNull(empresa);

            var x = new Dictionary<string, object>() { };
            x.Add("start", (int)0);
            x.Add("limit", (int)25);

            var result = controller.Pesquisar(new StoreRequestParameters(x), null, empresa.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var crachaEmpresa = result.Data as List<RelatorioCrachaEmpresaModelView>;
            Assert.IsNotNull(crachaEmpresa);
        }
    }
}
