﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Controlsys.Interfaces.Tests.Controller;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Controlsys.Dominio.Ocorrencias;
using Ext.Net;
using Ext.Net.MVC;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) RelatorioOcorrenciaSegurancaTeste.
    /// </summary>
    [TestClass]
    public class RelatorioOcorrenciaSegurancaTeste
    {
        /// <summary>
        /// Pode exportar PDF.
        /// </summary>
        [TestMethod]
        public void Pode_ExportarPDF()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RelatorioOcorrenciaSegurancaController controller = new RelatorioOcorrenciaSegurancaController(unidTrabalho);

            OcorrenciaSeguranca ocorrencia = OcorrenciaSegurancaTeste.ObterOcorrencia(true);
            Assert.IsNotNull(ocorrencia);

            List<string> selectedFields = typeof(OcorrenciaSegurancaModelView).GetProperties().Select(pi => pi.Name).ToList();

            var result = controller.Exportar("PDF", selectedFields, null,  null, ocorrencia.Supervisor.Login, ocorrencia.TipoOcorrencia.Codigo , ocorrencia.Local.Codigo, null,  null) as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/pdf", result.ContentType);
            Assert.AreEqual("download.pdf", result.FileDownloadName);
        }

        /// <summary>
        /// Pode exportar excel.
        /// </summary>
        [TestMethod]
        public void Pode_ExportarExcel()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RelatorioOcorrenciaSegurancaController controller = new RelatorioOcorrenciaSegurancaController(unidTrabalho);

            OcorrenciaSeguranca ocorrencia = OcorrenciaSegurancaTeste.ObterOcorrencia(true);
            Assert.IsNotNull(ocorrencia);

            List<string> selectedFields = typeof(OcorrenciaSegurancaModelView).GetProperties().Select(pi => pi.Name).ToList();

            var result = controller.Exportar("Excel", selectedFields, null, null, ocorrencia.Supervisor.Login, ocorrencia.TipoOcorrencia.Codigo, ocorrencia.Local.Codigo, null, null) as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/vnd.ms-excel", result.ContentType);
            Assert.AreEqual("download.xls", result.FileDownloadName);         
        }

        /// <summary>
        /// Pode exportar word.
        /// </summary>
        [TestMethod]
        public void Pode_ExportarWord()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RelatorioOcorrenciaSegurancaController controller = new RelatorioOcorrenciaSegurancaController(unidTrabalho);

            OcorrenciaSeguranca ocorrencia = OcorrenciaSegurancaTeste.ObterOcorrencia(true);
            Assert.IsNotNull(ocorrencia);

            List<string> selectedFields = typeof(OcorrenciaSegurancaModelView).GetProperties().Select(pi => pi.Name).ToList();

            var result = controller.Exportar("Word", selectedFields, null, null, ocorrencia.Supervisor.Login, ocorrencia.TipoOcorrencia.Codigo, ocorrencia.Local.Codigo, null, null) as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/msword", result.ContentType);
            Assert.AreEqual("download.doc", result.FileDownloadName); 
        }

        /// <summary>
        /// Pode pesquisar.
        /// </summary>
        [TestMethod]
        public void Pode_Pesquisar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RelatorioOcorrenciaSegurancaController controller = new RelatorioOcorrenciaSegurancaController(unidTrabalho);

            var x = new Dictionary<string, object>() { };
            x.Add("start", (int)0);
            x.Add("limit", (int)25);

            var result = controller.Pesquisar(new StoreRequestParameters(x), null, null, null, 0, 0, null, null) as StoreResult;
            Assert.IsNotNull(result);

            var ocorrencias = result.Data as List<RelatorioOcorrenciaSegurancaModelView>;
            Assert.IsNotNull(ocorrencias);
        }
    }
}
