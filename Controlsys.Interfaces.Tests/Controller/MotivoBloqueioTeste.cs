﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web.Mvc;
using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Seguranca;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Tests.Properties;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Controlsys.Persistencia;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) MotivoBloqueioTeste.
    /// </summary>
    [TestClass]
    public class MotivoBloqueioTeste
    {
        /// <summary>
        /// Valida incluir um novo MotivoBloqueio, informando nome e descricao.
        /// </summary>
        [TestMethod]
        public void Pode_Incluir()
        {
            var dt = DateTime.Now;
            var data = dt.ToString("MM/dd HH:mm:ss.fff");
            var nome = "Nome-" + data;
            var descricao = "Descricao-" + data;
            var motivoBloqueioAux = new MotivoBloqueio();
            motivoBloqueioAux.Nome = nome;
            motivoBloqueioAux.Descricao = descricao;

            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new MotivoBloqueioController(unidTrabalho);

            var result = controller.Incluir(motivoBloqueioAux) as JsonResult;
            Assert.IsNotNull(result);

            var motivoBloqueio = JsonHelper.GetJsonObjectRepresentation<MotivoBloqueio>(result);
            Assert.AreEqual(nome, motivoBloqueio.Nome);
            Assert.AreEqual(descricao, motivoBloqueio.Descricao);
        }

        /// <summary>
        /// Valida alterar um MotivoBloqueio, modificando o nome e descrição.
        /// </summary>
        [TestMethod]
        public void Pode_Alterar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new MotivoBloqueioController(unidTrabalho);
            var motivoBloqueioAux = ObterMotivoBloqueio(true);
            Assert.IsNotNull(motivoBloqueioAux);
            Assert.IsTrue(motivoBloqueioAux.Ativo);

            var dt = DateTime.Now;
            var data = dt.ToString("MM/dd HH:mm:ss.fff");
            var nome = "Nome-" + data;
            var descricao = "Descricao-" + data;
            motivoBloqueioAux.Nome = nome;
            motivoBloqueioAux.Descricao = descricao;

            var result = controller.Alterar(motivoBloqueioAux) as JsonResult;
            Assert.IsNotNull(result.Data);

            var motivoBloqueio = JsonHelper.GetJsonObjectRepresentation<MotivoBloqueio>(result);
            Assert.AreEqual(nome, motivoBloqueio.Nome);
            Assert.AreEqual(descricao, motivoBloqueio.Descricao);
        }

        /// <summary>
        /// Valida Ativar MotivoBloqueio.
        /// </summary>
        [TestMethod]
        public void Pode_Ativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new MotivoBloqueioController(unidTrabalho);
            var lista = ObterListaMotivoBloqueio();
            Assert.IsTrue(lista.Any(), Resources.Lista_vazia);
            
            var motivoBloqueioAux = lista.First(x=> x.Ativo == false);
            Assert.IsNotNull(motivoBloqueioAux);
            Assert.IsFalse(motivoBloqueioAux.Ativo);

            var result = controller.AtivarInativar(motivoBloqueioAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var motivoBloqueio = result.Data as MotivoBloqueio;
            Assert.IsNotNull(motivoBloqueio);
            Assert.AreEqual(motivoBloqueioAux.Codigo, motivoBloqueio.Codigo);
            Assert.IsTrue(motivoBloqueio.Ativo);
        }

        /// <summary>
        /// Valida Inativar MotivoBloqueio.
        /// </summary>
        [TestMethod]
        public void Pode_Inativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new MotivoBloqueioController(unidTrabalho);
            var motivoBloqueioAux = ObterMotivoBloqueio(true);
            Assert.IsTrue(motivoBloqueioAux.Ativo);

            var result = controller.AtivarInativar(motivoBloqueioAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var motivoBloqueio = result.Data as MotivoBloqueio;
            Assert.IsNotNull(motivoBloqueio);
            Assert.AreEqual(motivoBloqueioAux.Codigo, motivoBloqueio.Codigo);
            Assert.IsFalse(motivoBloqueio.Ativo);
        }

        /// <summary>
        /// Busca aleatóriamente um Motivo Bloqueio cadastrado na base de dados.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// Filtrar Motivos Bloqueio ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) MotivoBloqueio.
        /// </returns>
        public static MotivoBloqueio ObterMotivoBloqueio(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<MotivoBloqueio>(unidTrabalho);
            var motivosBloqueio = repositorio.ObterTodos().ToList();
            Assert.IsTrue(motivosBloqueio.Any());

            var rand = new Random();
            motivosBloqueio = motivosBloqueio.OrderBy(x => rand.Next()).ToList();

            MotivoBloqueio motivoBloqueio = null;
            foreach (var item in motivosBloqueio)
            {
                motivoBloqueio = item;

                if (motivoBloqueio == null)
                    continue;
                if (somenteAtivos)
                {
                    if (motivoBloqueio.Ativo)
                        break;
                    motivoBloqueio = null;
                }
                else
                    break;
            }

            return motivoBloqueio;
        }

        /// <summary>
        /// Obter lista motivo bloqueio.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// Filtrar Motivos Bloqueio ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;MotivoBloqueio&gt;
        /// </returns>
        private List<MotivoBloqueio> ObterListaMotivoBloqueio(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<MotivoBloqueio>(unidTrabalho);
            var motivosBloqueio = repositorio.ObterTodos().ToList();
            Assert.IsTrue(motivosBloqueio.Any());

            var rand = new Random();
            motivosBloqueio = motivosBloqueio.OrderBy(x => rand.Next()).ToList();

            return somenteAtivos ? motivosBloqueio.Where(x => x.Ativo).ToList() : motivosBloqueio;
        }

    }
}
