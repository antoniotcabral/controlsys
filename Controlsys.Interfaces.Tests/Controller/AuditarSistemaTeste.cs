﻿using System;
using System.Linq;
using Controlsys.Dominio.Seguranca;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorios.Audit;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using System.Collections.Generic;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) AuditarSistemaTeste.
    /// </summary>
    [TestClass]
    public class AuditarSistemaTeste
    {
        /// <summary>
        /// Gravar log.
        /// </summary>
        [TestMethod]
        public void GravarLog()
        {
            var usuario = UsuarioTeste.ObterUsuario(true);
            Assert.IsNotNull(usuario);

            var listaAcao = AcaoTeste.ObterListaAcao(true);
            Assert.IsNotNull(listaAcao);
            Assert.IsTrue(listaAcao.Any());

            var acao = listaAcao.FirstOrDefault(x => x.Nome.Contains("Lista") || x.Nome.Contains("Obter") || x.Nome.Contains("Selecionar"));
            Assert.IsNotNull(listaAcao);

            var cat = new CustomAuthorizeAttribute();
            var unidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            cat.registrarAudit(usuario.Login, unidadeTrabalho, acao.Url);

            IRepositorioAuditoria repAudit = Fabrica.Instancia.ObterRepositorio<IRepositorioAuditoria>(unidadeTrabalho);
            var auditoria = repAudit.ObterTodos().Where(x => 
                (x.Usuario.Login == usuario.Login) && 
                (x.Acao.Url == acao.Url) && 
                (x.DataRegistro > DateTime.Now.AddMinutes(-2))
            ).ToList();
            Assert.IsNotNull(auditoria);
            Assert.IsTrue(auditoria.Any());
        }

        /// <summary>
        /// Retorna todos os objetos do tipo AuditarSistema.
        /// </summary>
        ///
        /// <exception cref="AssertFailedException">
        /// Lançada quando uma condição de erro Assert Failed ocorre.
        /// </exception>
        [TestMethod]
        public void ObterTodos()
        {
            var listaUsuario = UsuarioTeste.ObterListaUsuario(true);
            Assert.IsNotNull(listaUsuario);
            Assert.IsTrue(listaUsuario.Any());

            var unidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new AuditarSistemaController(unidadeTrabalho);
            foreach (var usuario in listaUsuario)
            {
                var result = controller.ObterTodos(null, usuario.Codigo, null, null, null, null) as JsonResult;
                if (result == null) continue;

                var listaAuditarSistema = JsonHelper.GetJsonObjectRepresentation<List<AuditarSistemaModelView>>(result);
                Assert.IsNotNull(listaAuditarSistema);

                if (!listaAuditarSistema.Any()) continue;
                Assert.IsTrue(listaAuditarSistema.Any());

                foreach (var item in listaAuditarSistema)
                {
                    Assert.AreEqual(usuario.Codigo, item.CodigoUsuario);
                }
                return;
            }

            throw new AssertFailedException();
        }

    }
}
