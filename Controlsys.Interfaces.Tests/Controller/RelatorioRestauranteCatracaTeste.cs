﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Controlsys.Infra;
using Globalsys;
using Controlsys.Dominio.Relatorio;
using Controlsys.Interfaces.Controllers;
using System.Web.Mvc;
using Moq;
using System.Web;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// Summary description for RelatorioRestauranteCatracaTeste
    /// </summary>
    [TestClass]
    public class RelatorioRestauranteCatracaTeste
    {
        /// <summary>
        /// Pode exportar PDF.
        /// </summary>
        [TestMethod]
        public void Pode_ExportarPDF()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            List<string> selectedFields = typeof(RelatorioRestauranteCatraca).GetProperties().Select(pi => pi.Name).ToList();
            RelatorioRestauranteCatracaController controller = new RelatorioRestauranteCatracaController(unidTrabalho);
            App_Start.Mock.ServerMapPath(controller, "RelatorioRestauranteCatraca.rdlc");

            var result = controller.Exportar(DateTime.Now, "PDF") as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/pdf", result.ContentType);
            Assert.AreEqual("download.pdf", result.FileDownloadName);
        }

        /// <summary>
        /// Pode exportar excel.
        /// </summary>
        [TestMethod]
        public void Pode_ExportarExcel()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            List<string> selectedFields = typeof(RelatorioRestauranteCatraca).GetProperties().Select(pi => pi.Name).ToList();
            RelatorioRestauranteCatracaController controller = new RelatorioRestauranteCatracaController(unidTrabalho);
            App_Start.Mock.ServerMapPath(controller, "RelatorioRestauranteCatraca.rdlc");

            var result = controller.Exportar(DateTime.Now, "Excel") as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/vnd.ms-excel", result.ContentType);
            Assert.AreEqual("download.xls", result.FileDownloadName);
        }

        /// <summary>
        /// Pode exportar word.
        /// </summary>
        [TestMethod]
        public void Pode_ExportarWord()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            List<string> selectedFields = typeof(RelatorioRestauranteCatraca).GetProperties().Select(pi => pi.Name).ToList();
            RelatorioRestauranteCatracaController controller = new RelatorioRestauranteCatracaController(unidTrabalho);
            App_Start.Mock.ServerMapPath(controller, "RelatorioRestauranteCatraca.rdlc");

            var result = controller.Exportar(DateTime.Now, "Word") as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/msword", result.ContentType);
            Assert.AreEqual("download.doc", result.FileDownloadName);
        }
    }
}
