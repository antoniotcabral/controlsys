﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Controlsys.Dominio.Parametros;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Repositorio.Parametros;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Controlsys.Interfaces.Tests.Controller
{
    [TestClass]
    public class ModeloVeiculoTeste
    {
        [TestMethod]
        public void Incluir()
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();

            ModeloVeiculo registro = new ModeloVeiculo();
            var rand = new Random();
            registro.Nome = "Nome" + rand.Next(1, 9999);
            registro.Fabricante = FabricanteVeiculoTeste.ObterFabricanteVeiculo(true);

            ModeloVeiculoController controller = new ModeloVeiculoController(unidTrabalho);

            var result = controller.Incluir(registro, registro.Fabricante.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var registroGerado = JsonHelper.GetJsonObjectRepresentation<ModeloVeiculo>(result);

            Assert.IsNotNull(registroGerado);
            Assert.IsTrue(registroGerado.Codigo > 0);
            Assert.IsTrue(registroGerado.Ativo);
            Assert.AreEqual(registro.Nome, registroGerado.Nome);
            Assert.AreEqual(registro.Fabricante.Codigo, registroGerado.Fabricante.Codigo);
            Assert.IsNull(registroGerado.DataDesativacao);

            unidTrabalho.Rollback();
        }

        [TestMethod]
        public void Alterar()
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();

            ModeloVeiculo registro = ObterListaModeloVeiculo(true).FirstOrDefault();
            var rand = new Random();
            registro.Nome = "Nome" + rand.Next(1, 9999);
            registro.Fabricante = FabricanteVeiculoTeste.ObterFabricanteVeiculo(true);

            ModeloVeiculoController controller = new ModeloVeiculoController(unidTrabalho);

            var result = controller.Alterar(registro) as JsonResult;
            Assert.IsNotNull(result);

            var registroAlterado = JsonHelper.GetJsonObjectRepresentation<ModeloVeiculo>(result);

            Assert.IsNotNull(registroAlterado);
            Assert.AreEqual(registro.Codigo, registroAlterado.Codigo);
            Assert.AreEqual(registro.Nome, registroAlterado.Nome);
            //Assert.AreEqual(registro.Fabricante.Codigo, registroAlterado.Fabricante.Codigo);


            unidTrabalho.Rollback();
        }

        [TestMethod]
        public void Ativar()
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();

            ModeloVeiculo registro = ObterListaModeloVeiculo().Where(x=> x.Ativo == false).FirstOrDefault();

            ModeloVeiculoController controller = new ModeloVeiculoController(unidTrabalho);

            var result = controller.AtivarInativar(registro.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var registroAlterado = JsonHelper.GetJsonObjectRepresentation<ModeloVeiculo>(result);

            Assert.IsNotNull(registroAlterado);
            Assert.AreEqual(registro.Codigo, registroAlterado.Codigo);
            Assert.IsTrue(registroAlterado.Ativo);

            unidTrabalho.Rollback();
        }

        [TestMethod]
        public void Inativar()
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();

            ModeloVeiculo registro = ObterListaModeloVeiculo(true).FirstOrDefault();

            ModeloVeiculoController controller = new ModeloVeiculoController(unidTrabalho);

            var result = controller.AtivarInativar(registro.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var registroAlterado = JsonHelper.GetJsonObjectRepresentation<ModeloVeiculo>(result);

            Assert.IsNotNull(registroAlterado);
            Assert.AreEqual(registro.Codigo, registroAlterado.Codigo);
            Assert.IsFalse(registroAlterado.Ativo);

            unidTrabalho.Rollback();
        }

        [TestMethod]
        public void Selecionar()
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            ModeloVeiculoController controller = new ModeloVeiculoController(unidTrabalho);

            ModeloVeiculo registro = ObterListaModeloVeiculo().FirstOrDefault();
            var result = controller.Selecionar(registro.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var registroEncontrado = JsonHelper.GetJsonObjectRepresentation<ModeloVeiculo>(result);

            Assert.IsNotNull(registroEncontrado);
            Assert.AreEqual(registro.Codigo, registroEncontrado.Codigo);

            unidTrabalho.Rollback();
        }

        [TestMethod]
        public void ObterTodos()
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            ModeloVeiculoController controller = new ModeloVeiculoController(unidTrabalho);

            var result = controller.ObterTodos() as JsonResult;
            Assert.IsNotNull(result);

            var registros = JsonHelper.GetJsonObjectRepresentation<List<ModeloVeiculo>>(result);

            Assert.IsNotNull(registros);
            Assert.IsTrue(registros.Any());

            unidTrabalho.Rollback();
        }

        public static List<ModeloVeiculo> ObterListaModeloVeiculo(bool somenteAtivo = false)
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            IRepositorioModeloVeiculo repModeloVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioModeloVeiculo>(unidTrabalho);

            IQueryable<ModeloVeiculo> lista = repModeloVeiculo.ObterTodos();

            if (somenteAtivo)
                lista = lista.Where(x => x.Ativo == true);

            return lista.ToList().OrderBy(x => new Random().Next()).ToList();
        }
    }
}
