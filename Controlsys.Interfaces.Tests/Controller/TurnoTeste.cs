﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Controlsys.Dominio.Parametros;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Tests.Properties;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Controlsys.Persistencia;
using Controlsys.Interfaces.Models;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) TurnoTeste.
    /// </summary>
    [TestClass]
    public class TurnoTeste
    {
        /// <summary>
        /// Valida alterar um Turno, modificando Nome, Descricao e DiasValidade.
        /// </summary>
        [TestMethod]
        public void Pode_Incluir()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new TurnoController(unidTrabalho);
            var turnoAux = new Turno();

            var rand = new Random();
            var horasTurno = new List<HoraTurno>();
            var qtdAux = rand.Next(1, 10);
            for (int i = 0; i < qtdAux; i++)
            {
                horasTurno.Add(HoraTurnoTeste.ObterHoraTurnoNovo(turnoAux));
            }

            var feriados = new List<Feriado>();
            qtdAux = rand.Next(1, 5);
            for (int i = 0; i < qtdAux; i++)
            {
                feriados.Add(FeriadoTeste.ObterFeriado(true));
            }

            var dt = DateTime.Now;
            var data = dt.ToString("MM/dd HH:mm:ss.fff");
            var nome = "Nome-" + data;
            var consideraFeriado = Global.ObterBoolean();
            var obedeceEscala = Global.ObterBoolean();

            turnoAux.Nome = nome;
            turnoAux.ConsideraFeriado = consideraFeriado;
            turnoAux.ObedeceEscala = obedeceEscala;

            var result = controller.Incluir(turnoAux, horasTurno, feriados) as JsonResult;
            Assert.IsNotNull(result);

            var turno = result.Data as TurnoModelView;
            Assert.IsNotNull(turno);
            Assert.AreEqual(nome, turno.Nome);
            //Assert.AreEqual(consideraFeriado, turno.ConsideraFeriado);
            Assert.AreEqual(obedeceEscala, turno.ObedeceEscala);

            // TODO: Validar lista de HorasTurno
            // TODO: Validar lista de Feriados
        }

        /// <summary>
        /// Valida alterar um Turno, modificando Nome, Descricao e DiasValidade.
        /// </summary>
        [TestMethod]
        public void Pode_Alterar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new TurnoController(unidTrabalho);
            var turnoAux = ObterTurno(true);
            Assert.IsNotNull(turnoAux);
            Assert.IsTrue(turnoAux.Ativo);

            var rand = new Random();
            var dt = DateTime.Now;
            var data = dt.ToString("MM/dd HH:mm:ss.fff");
            var nome = "Nome-" + data;
            var consideraFeriado = Global.ObterBoolean();
            var obedeceEscala = Global.ObterBoolean();

            var horasTurno = new List<HoraTurno>();
            var qtdAux = rand.Next(1, 10);
            for (int i = 0; i < qtdAux; i++)
            {
                horasTurno.Add(HoraTurnoTeste.ObterHoraTurnoNovo(turnoAux));
            }

            qtdAux = rand.Next(1, 5);
            var feriadosTurno = new int[qtdAux];
            for (int i = 0; i < qtdAux; i++)
            {
                feriadosTurno[i] = FeriadoTeste.ObterFeriado(true).Codigo;
            }

            turnoAux.Nome = nome;
            turnoAux.ConsideraFeriado = consideraFeriado;
            turnoAux.ObedeceEscala = obedeceEscala;

            var result = controller.Alterar(
                turnoAux,
                null, horasTurno,
                null, feriadosTurno
            ) as JsonResult;
            Assert.IsNotNull(result);

            var turno = result.Data as TurnoModelView;
            Assert.IsNotNull(turno);
            Assert.AreEqual(turnoAux.Nome, turno.Nome);
            //Assert.AreEqual(turnoAux.ConsideraFeriado, turno.ConsideraFeriado);
            Assert.AreEqual(turnoAux.ObedeceEscala, turno.ObedeceEscala);

            // TODO: Validar lista de HorasTurno
            // TODO: Validar lista de Feriados
        }

        #region [Desativado]
        /*
        /// <summary>
        /// Valida ativação ou inativação de Turno, conforme status corrente;
        /// </summary>
        [TestMethod]
        public void Pode_AtivarInativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new TurnoController(unidTrabalho);
            var turnoAux = ObterTurno();
            Assert.IsNotNull(turnoAux);

            var status = !turnoAux.Ativo;
            var result = controller.AtivarInativar(turnoAux.Codigo) as JsonResult;
            Assert.IsNotNull(result.Data);

            var turno = JsonHelper.GetJsonObjectRepresentation<Turno>(result);
            Assert.AreEqual(turnoAux.Codigo, turno.Codigo);
            Assert.AreEqual(status, turno.Ativo);
        }

        /// <summary>
        /// Busca aleatóriamente um Turno de Trabalho cadastrado na base de dados
        /// </summary>
        /// <param name="somenteAtivos">Filtrar Turnos ativos</param>
        /// <returns></returns>
        public static Turno ObterTurno(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<Turno>(unidTrabalho);
            var turnos = repositorio.ObterTodos().ToList();
            Assert.IsTrue(turnos.Any());

            var rand = new Random();
            turnos = turnos.OrderBy(x => rand.Next()).ToList();

            Turno turno = null;
            foreach (var item in turnos)
            {
                turno = item;

                if (turno == null)
                    continue;
                if (somenteAtivos)
                {
                    if (turno.Ativo)
                        break;
                    turno = null;
                }
                else
                    break;
            }

            return turno;
        }

        private List<TurnoModelView> ObterListaTurno(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new TurnoController(unidTrabalho);
            var result = controller.ObterTodos() as JsonResult;
            Assert.IsNotNull(result);

            var turnos = JsonHelper.GetJsonObjectRepresentation<List<TurnoModelView>>(result);
            var rand = new Random();
            turnos = turnos.OrderBy(x => rand.Next()).ToList();

            return somenteAtivos ? turnos.Where(x => x.Ativo).ToList() : turnos;
        }
        */
        #endregion

        /// <summary>
        /// Valida Ativar Turno.
        /// </summary>
        [TestMethod]
        public void Pode_Ativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new TurnoController(unidTrabalho);
            var lista = ObterListaTurno();
            Assert.IsTrue(lista.Any(), Resources.Lista_vazia);

            var listaAtivo = lista.Where(x => x.Ativo).ToList();
            var listaInativo = lista.Where(x => x.Ativo == false).ToList();
            Assert.IsTrue(listaInativo.Any(), Resources.Lista_vazia);

            Turno turnoAux = null;
            for (int i = 0; i < listaInativo.Count; i++)
            {
                if (listaAtivo.Any())
                    if (listaAtivo.Exists(x => x.Nome == listaInativo[i].Nome)) continue;
                turnoAux = listaInativo[i];
                break;
            }
            Assert.IsNotNull(turnoAux);
            Assert.IsFalse(turnoAux.Ativo);

            var result = controller.AtivarInativar(turnoAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var turno = result.Data as TurnoModelView;
            Assert.IsNotNull(turno);
            Assert.AreEqual(turnoAux.Codigo, turno.Codigo);
            Assert.IsTrue(turno.Ativo);
        }

        /// <summary>
        /// Valida Inativar Turno.
        /// </summary>
        [TestMethod]
        public void Pode_Inativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new TurnoController(unidTrabalho);
            var turnoAux = ObterTurno(true);
            Assert.IsTrue(turnoAux.Ativo);

            var result = controller.AtivarInativar(turnoAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var turno = result.Data as TurnoModelView;
            Assert.IsNotNull(turno);
            Assert.AreEqual(turnoAux.Codigo, turno.Codigo);
            Assert.IsFalse(turno.Ativo);
        }

        /// <summary>
        /// Busca aleatóriamente um Turno cadastrado na base de dados.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// Filtrar Turnos ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Turno.
        /// </returns>
        public static Turno ObterTurno(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<Turno>(unidTrabalho);
            var listaTurno = repositorio.ObterTodos().ToList();
            Assert.IsTrue(listaTurno.Any());

            var rand = new Random();
            listaTurno = listaTurno.OrderBy(x => rand.Next()).ToList();

            return listaTurno.First(x => !somenteAtivos || x.Ativo);
        }

        /// <summary>
        /// Obter lista turno.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// Filtrar Turnos ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;Turno&gt;
        /// </returns>
        public static List<Turno> ObterListaTurno(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<Turno>(unidTrabalho);
            var listaTurno = repositorio.ObterTodos().ToList();
            Assert.IsTrue(listaTurno.Any());

            var rand = new Random();
            listaTurno = listaTurno.OrderBy(x => rand.Next()).ToList();

            return somenteAtivos ? listaTurno.Where(x => x.Ativo).ToList() : listaTurno;
        }
    }
}
