﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Controlsys.Dominio.Parametros;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Controlsys.Persistencia;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks.Constraints;
using Controlsys.Interfaces.Tests.Properties;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) HoraTurnoTeste.
    /// </summary>
    [TestClass]
    public class HoraTurnoTeste
    {
        /// <summary>
        /// Valida Ativar HoraTurno.
        /// </summary>
        [TestMethod]
        public void Pode_Ativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new HoraTurnoController(unidTrabalho);
            var lista = ObterListaHoraTurno();
            Assert.IsTrue(lista.Any(), Resources.Lista_vazia);

            lista = lista.Where(x => x.Ativo == false).ToList();
            Assert.IsTrue(lista.Any(), Resources.Lista_vazia);

            var horaTurnoAux = lista[0];
            Assert.IsFalse(horaTurnoAux.Ativo);

            var result = controller.AtivarInativar(horaTurnoAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var horaTurno = result.Data as HoraTurnoModelView;
            Assert.IsNotNull(horaTurno);
            Assert.AreEqual(horaTurnoAux.Codigo, horaTurno.Codigo);
            Assert.IsTrue(horaTurno.Ativo);
        }

        /// <summary>
        /// Valida Inativar HoraTurno.
        /// </summary>
        [TestMethod]
        public void Pode_Inativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new HoraTurnoController(unidTrabalho);
            var horaTurnoAux = ObterHoraTurno(true);
            Assert.IsTrue(horaTurnoAux.Ativo);

            var result = controller.AtivarInativar(horaTurnoAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var horaTurno = result.Data as HoraTurnoModelView;
            Assert.IsNotNull(horaTurno);
            Assert.AreEqual(horaTurnoAux.Codigo, horaTurno.Codigo);
            Assert.IsFalse(horaTurno.Ativo);
        }

        /// <summary>
        /// Gera um novo registro de HoraTurno sem persistir na base de dados.
        /// </summary>
        ///
        /// <param name="turno">
        /// O(a) turno.
        /// </param>
        ///
        /// <returns>
        /// Um(a) HoraTurno.
        /// </returns>
        public static HoraTurno ObterHoraTurnoNovo(Turno turno)
        {
            var rand = new Random();

            var horaInicio = new TimeSpan(rand.Next(0, 23), rand.Next(0, 59), 0);
            var horaFim = new TimeSpan(rand.Next(0, 23), rand.Next(0, 59), 0);
            var ordem = rand.Next(1, 10);
            var diaSemana = Global.ObterEnumAleatorio<DiaSemana>();
            var trabalha = Global.ObterBoolean();
            var maior24h = false;
            if (horaInicio == horaFim)
                horaFim.Add(new TimeSpan(rand.Next(0, 23), rand.Next(0, 59), 0));
            else if (horaInicio > horaFim)
                maior24h = true;

            var horaTurno = new HoraTurno()
            {
                Turno = turno,
                HoraInicio = horaInicio,
                HoraFim = horaFim,
                Ordem = ordem,
                DiaSemana = diaSemana,
                Trabalha = trabalha,
                Maior24h = maior24h
            };
            horaTurno.Ativar();

            return horaTurno;
        }

        /// <summary>
        /// Busca aleatóriamente um Hora Turno cadastrado na base de dados.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// Filtrar Horas Turno ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) HoraTurno.
        /// </returns>
        public static HoraTurno ObterHoraTurno(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<HoraTurno>(unidTrabalho);
            var horasTurno = repositorio.ObterTodos().ToList();
            Assert.IsTrue(horasTurno.Any());

            var rand = new Random();
            horasTurno = horasTurno.OrderBy(x => rand.Next()).ToList();

            HoraTurno horaTurno = null;
            foreach (var item in horasTurno)
            {
                horaTurno = item;

                if (horaTurno == null)
                    continue;
                if (somenteAtivos)
                {
                    if (horaTurno.Ativo)
                        break;
                    horaTurno = null;
                }
                else
                    break;
            }

            return horaTurno;
        }

        /// <summary>
        /// Obter lista hora turno.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// Filtrar Horas Turno ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;HoraTurno&gt;
        /// </returns>
        private List<HoraTurno> ObterListaHoraTurno(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<HoraTurno>(unidTrabalho);
            var horasTurno = repositorio.ObterTodos().ToList();
            Assert.IsTrue(horasTurno.Any());

            var rand = new Random();
            horasTurno = horasTurno.OrderBy(x => rand.Next()).ToList();

            return somenteAtivos ? horasTurno.Where(x => x.Ativo).ToList() : horasTurno;
        }

    }
}
