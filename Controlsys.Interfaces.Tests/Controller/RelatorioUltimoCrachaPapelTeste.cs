﻿using System;
using System.Linq;
using System.Collections.Generic;
using Controlsys.Dominio.Relatorio;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Tests.Controller
{
    [TestClass]
    public class RelatorioUltimoCrachaPapelTeste
    {
        [TestMethod]
        public void Pode_Pesquisar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RelatorioUltimoCrachaPapelController controller = new RelatorioUltimoCrachaPapelController(unidTrabalho);

            var result = controller.Pesquisar(null, DateTime.Now.AddDays(-60), DateTime.Now, ignoreParameters: true);

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Pode_ExportarPDF()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();

            List<string> selectedFields = typeof(RelatorioUltimoCrachaPapel).GetProperties().Select(pi => pi.Name).ToList();

            RelatorioUltimoCrachaPapelController controller = new RelatorioUltimoCrachaPapelController(unidTrabalho);

            var result = controller.Exportar(selectedFields, "PDF", DateTime.Now.AddDays(-60), DateTime.Now) as FileContentResult;            

            Assert.IsNotNull(result);
            Assert.AreEqual("application/pdf", result.ContentType);
            Assert.AreEqual("download.pdf", result.FileDownloadName);
        }

        [TestMethod]
        public void Pode_ExportarExcel()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();

            List<string> selectedFields = typeof(RelatorioUltimoCrachaPapel).GetProperties().Select(pi => pi.Name).ToList();

            RelatorioUltimoCrachaPapelController controller = new RelatorioUltimoCrachaPapelController(unidTrabalho);

            var result = controller.Exportar(selectedFields, "Excel", DateTime.Now.AddDays(-60), DateTime.Now) as FileContentResult;

            Assert.IsNotNull(result);
            Assert.AreEqual("application/vnd.ms-excel", result.ContentType);
            Assert.AreEqual("download.xls", result.FileDownloadName);
        }

        [TestMethod]
        public void Pode_ExportarWord()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();

            List<string> selectedFields = typeof(RelatorioUltimoCrachaPapel).GetProperties().Select(pi => pi.Name).ToList();

            RelatorioUltimoCrachaPapelController controller = new RelatorioUltimoCrachaPapelController(unidTrabalho);

            var result = controller.Exportar(selectedFields, "Word", DateTime.Now.AddDays(-60), DateTime.Now) as FileContentResult;

            Assert.IsNotNull(result);
            Assert.AreEqual("application/msword", result.ContentType);
            Assert.AreEqual("download.doc", result.FileDownloadName);
        }
    }
}
