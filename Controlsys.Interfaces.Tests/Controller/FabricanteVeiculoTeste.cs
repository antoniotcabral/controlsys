﻿using System;
using System.Linq;
using System.Web.Mvc;
using Controlsys.Dominio.Parametros;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Repositorio.Parametros;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Controlsys.Interfaces.Tests.Controller
{
    [TestClass]
    public class FabricanteVeiculoTeste
    {

        [TestMethod]
        public void Incluir()
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();

            FabricanteVeiculo registro = new FabricanteVeiculo();
            var rand = new Random();
            registro.Nome = "Nome" + rand.Next(1, 9999);

            FabricanteVeiculoController controller = new FabricanteVeiculoController(unidTrabalho);

            var result = controller.Incluir(registro) as JsonResult;
            Assert.IsNotNull(result);

            var registroGerado = result.Data as FabricanteVeiculo;

            Assert.IsNotNull(registroGerado);
            Assert.IsTrue(registroGerado.Codigo > 0);
            Assert.IsTrue(registroGerado.Ativo);
            Assert.AreEqual(registro.Nome, registroGerado.Nome);
            Assert.IsNull(registroGerado.DataDesativacao);

            unidTrabalho.Rollback();
        }

        [TestMethod]
        public void Alterar()
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();

            FabricanteVeiculo registro = ObterFabricanteVeiculo(true);
            var rand = new Random();
            registro.Nome = "Nome" + rand.Next(1, 9999);

            FabricanteVeiculoController controller = new FabricanteVeiculoController(unidTrabalho);

            var result = controller.Alterar(registro) as JsonResult;
            Assert.IsNotNull(result);

            var registroAlterado = result.Data as FabricanteVeiculo;

            Assert.IsNotNull(registroAlterado);
            Assert.AreEqual(registro.Codigo, registroAlterado.Codigo);
            Assert.AreEqual(registro.DataRegistro, registroAlterado.DataRegistro);
            Assert.AreEqual(registro.DataDesativacao, registroAlterado.DataDesativacao);
            Assert.AreEqual(registro.Nome, registroAlterado.Nome);

            unidTrabalho.Rollback();
        }

        [TestMethod]
        public void Selecionar()
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            FabricanteVeiculoController controller = new FabricanteVeiculoController(unidTrabalho);

            FabricanteVeiculo registro = ObterFabricanteVeiculo();
            var result = controller.Selecionar(registro.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var registroEncontrado = result.Data as FabricanteVeiculo;

            Assert.IsNotNull(registroEncontrado);
            Assert.AreEqual(registro.Codigo, registroEncontrado.Codigo);

            unidTrabalho.Rollback();
        }

        [TestMethod]
        public void ObterTodos()
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            FabricanteVeiculoController controller = new FabricanteVeiculoController(unidTrabalho);

            var result = controller.ObterTodos() as JsonResult;
            Assert.IsNotNull(result);

            var registros = JsonHelper.GetJsonObjectRepresentation<List<FabricanteVeiculo>>(result);

            Assert.IsNotNull(registros);
            Assert.IsTrue(registros.Any());

            unidTrabalho.Rollback();
        }

        public static FabricanteVeiculo ObterFabricanteVeiculo(bool somenteAtivo = false)
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            IRepositorioFabricanteVeiculo repFabricanteVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioFabricanteVeiculo>(unidTrabalho);

            IQueryable<FabricanteVeiculo> lista = repFabricanteVeiculo.ObterTodos();

            if (somenteAtivo)
                lista = lista.Where(x => x.Ativo == true);

            return lista.ToList().OrderBy(x => new Random().Next()).FirstOrDefault();
        }
    }
}
