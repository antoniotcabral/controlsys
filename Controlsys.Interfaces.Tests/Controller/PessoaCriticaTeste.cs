﻿using System;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Collections.Generic;
using System.Web.Mvc;
using Controlsys.Dominio.Empresas;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Controlsys.Persistencia.Repositorios.Pessoas;
using Globalsys;
using Globalsys.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Controlsys.Persistencia;
using Controlsys.Dominio.Acesso;
using System.Web;
using Moq;
using System.Security.Principal;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) PessoaCriticaTeste.
    /// </summary>
    [TestClass]
    public class PessoaCriticaTeste
    {
        /// <summary>
        /// Pode incluir.
        /// </summary>
        [TestMethod]
        public void Pode_Incluir()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new PessoaCriticaController(unidTrabalho);

            var fakeHttpContext = new Mock<HttpContextBase>();
            var fakeIdentity = new GenericIdentity(UsuarioTeste.ObterUsuario().Login);
            var principal = new GenericPrincipal(fakeIdentity, null);

            fakeHttpContext.Setup(t => t.User).Returns(principal);
            var controllerContext = new Mock<ControllerContext>();
            controllerContext.Setup(t => t.HttpContext).Returns(fakeHttpContext.Object);
            controller.ControllerContext = controllerContext.Object;

            var repPessoaCritica = new RepositorioPessoaCritica(unidTrabalho);
            var listaPessoaCritica = repPessoaCritica.ObterTodos().ToList();
            Assert.IsNotNull(listaPessoaCritica);
            Assert.IsTrue(listaPessoaCritica.Any());

            var listaColab = ColaboradorTeste.ObterListaColaborador().ToList();
            Assert.IsNotNull(listaColab);
            Assert.IsTrue(listaColab.Any());

            var listaColabAux = listaColab.Select(x => new { x.PessoaFisica.Codigo }).ToArray();
            var listaPessoaCriticaAux = listaPessoaCritica.Select(x => new { x.PessoaFisica.Codigo }).ToArray();
            var lista = listaColabAux.Except(listaPessoaCriticaAux).ToList();
            Assert.IsNotNull(lista);
            if (!lista.Any()) return; //TODO: Remover isso e caso todos os colaboradores sejam críticos, remover um antes;
            Assert.IsTrue(lista.Any());

            var rand = new Random();
            lista = lista.OrderBy(x => rand.Next()).ToList();
            var colab = listaColab.FirstOrDefault(x => x.PessoaFisica.Codigo == lista[0].Codigo);
            Assert.IsNotNull(colab);

            string data = DateTime.Now.ToString("MM/dd HH:mm:ss.fff");
            var dt = DateTime.Now;
            var pessoaCriticaNovo = new PessoaCritica();
            pessoaCriticaNovo.PessoaFisica = colab.PessoaFisica;
            pessoaCriticaNovo.MotivoBloqueio = MotivoBloqueioTeste.ObterMotivoBloqueio();
            pessoaCriticaNovo.DataBloqueio = dt.AddDays(rand.Next(-365, -1));
            pessoaCriticaNovo.Responsavel = UsuarioTeste.ObterUsuario();
            pessoaCriticaNovo.Observacao = "Obs" + data;

            var result = controller.Incluir(pessoaCriticaNovo, null) as JsonResult;
            Assert.IsNotNull(result);

            var pessoaCritica = result.Data as PessoaCriticaModelView;
            Assert.IsNotNull(pessoaCritica);

            Assert.AreEqual(pessoaCriticaNovo.PessoaFisica.Codigo, pessoaCritica.PessoaFisica.Codigo);
            Assert.AreEqual(pessoaCriticaNovo.MotivoBloqueio.Nome.ToLower(), pessoaCritica.MotivoBloqueio.ToLower());
            Assert.AreEqual(pessoaCriticaNovo.DataBloqueio, pessoaCritica.DataBloqueio);
            Assert.AreEqual(pessoaCriticaNovo.Responsavel.PessoaFisica.Nome.ToLower(), pessoaCritica.Responsavel.ToLower());
            Assert.AreEqual(pessoaCriticaNovo.Observacao.ToLower(), pessoaCritica.Observacao.ToLower());
        }

        /// <summary>
        /// Pode alterar.
        /// </summary>
        [TestMethod]
        public void Pode_Alterar()
        {
            var lista = ObterListaPessoaCritica(true);
            Assert.IsNotNull(lista);
            Assert.IsTrue(lista.Any());

            var dt = DateTime.Now;
            string data = dt.ToString("MM/dd HH:mm:ss.fff");
            var pessoaCriticaAux = lista[0];
            pessoaCriticaAux.Observacao = "Obs" + data;
            pessoaCriticaAux.MotivoBloqueio = MotivoBloqueioTeste.ObterMotivoBloqueio();

            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            PessoaCriticaController controller = new PessoaCriticaController(unidTrabalho);

            #region [FakeIdentity]
            var fakeHttpContext = new Mock<HttpContextBase>();
            var fakeIdentity = new GenericIdentity(UsuarioTeste.ObterUsuario().Login);
            var principal = new GenericPrincipal(fakeIdentity, null);

            fakeHttpContext.Setup(t => t.User).Returns(principal);
            var controllerContext = new Mock<ControllerContext>();
            controllerContext.Setup(t => t.HttpContext).Returns(fakeHttpContext.Object);
            controller.ControllerContext = controllerContext.Object;
            #endregion

            var result = controller.Alterar(pessoaCriticaAux, null) as JsonResult;
            Assert.IsNotNull(result);

            var pessoaCritica = result.Data as PessoaCriticaModelView;
            Assert.IsNotNull(pessoaCritica);
            Assert.AreEqual(pessoaCriticaAux.Codigo, pessoaCritica.Codigo);
            Assert.AreEqual(pessoaCriticaAux.Ativo, pessoaCritica.Ativo);
            Assert.AreEqual(pessoaCriticaAux.Observacao.ToLower(), pessoaCritica.Observacao.ToLower());
            Assert.AreEqual(pessoaCriticaAux.MotivoBloqueio.Nome.ToLower(), pessoaCritica.MotivoBloqueio.ToLower());
        }

        /// <summary>
        /// Pode ativar.
        /// </summary>
        [TestMethod]
        public void Pode_Ativar()
        {
            var lista = ObterListaPessoaCritica().Where(x => x.Ativo == false).ToList();
            Assert.IsNotNull(lista);
            Assert.IsTrue(lista.Any());

            PessoaCritica pessoaCriticaAux = lista[0];
            Assert.IsNotNull(pessoaCriticaAux);
            Assert.IsFalse(pessoaCriticaAux.Ativo);

            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new PessoaCriticaController(unidTrabalho);
            var result = controller.AtivarInativar(pessoaCriticaAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var pessoaCritica = result.Data as PessoaCriticaModelView;
            Assert.IsNotNull(pessoaCritica);
            Assert.AreEqual(pessoaCriticaAux.Codigo, pessoaCritica.Codigo);
            Assert.IsTrue(pessoaCritica.Ativo);
        }

        /// <summary>
        /// Pode inativar.
        /// </summary>
        [TestMethod]
        public void Pode_Inativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            PessoaCriticaController controller = new PessoaCriticaController(unidTrabalho);

            var pessoaCriticaAux = ObterPessoaCritica(true);
            Assert.IsNotNull(pessoaCriticaAux);
            Assert.IsTrue(pessoaCriticaAux.Ativo);

            var result = controller.AtivarInativar(pessoaCriticaAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var pessoaCritica = result.Data as PessoaCriticaModelView;
            Assert.IsNotNull(pessoaCritica);
            Assert.AreEqual(pessoaCriticaAux.Codigo, pessoaCritica.Codigo);
            Assert.IsFalse(pessoaCritica.Ativo);
        }

        /// <summary>
        /// Obter pessoa critica.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// true to somente ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) PessoaCriticaModelView.
        /// </returns>
        public static PessoaCriticaModelView ObterPessoaCritica(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            PessoaCriticaController controller = new PessoaCriticaController(unidTrabalho);
            JsonResult result = controller.ObterTodos(somenteAtivos) as JsonResult;
            var pessoaCriticas = JsonHelper.GetJsonObjectRepresentation<List<PessoaCriticaModelView>>(result);
            Assert.IsNotNull(pessoaCriticas);

            Random rand = new Random();
            pessoaCriticas = pessoaCriticas.OrderBy(x => rand.Next()).ToList();

            PessoaCriticaModelView pessoaCriticaAux = null;
            foreach (var item in pessoaCriticas)
            {
                result = controller.Selecionar(item.Codigo) as JsonResult;
                Assert.IsNotNull(result.Data);
                pessoaCriticaAux = JsonHelper.GetJsonObjectRepresentation<PessoaCriticaModelView>(result);

                //pessoaCriticaAux = (controller.Selecionar(item.Codigo) as JsonResult).Data as PessoaCritica;
                //Assert.IsNotNull(pessoaCriticaAux);

                if (somenteAtivos)
                {
                    if (pessoaCriticaAux.Ativo)
                        break;
                    pessoaCriticaAux = null;
                }
                else
                    break;
            }

            return pessoaCriticaAux;
        }

        /// <summary>
        /// Obter lista pessoa critica.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// true to somente ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;PessoaCritica&gt;
        /// </returns>
        public static List<PessoaCritica> ObterListaPessoaCritica(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<PessoaCritica>(unidTrabalho);
            var pessoaCriticas = repositorio.ObterTodos().ToList();
            Assert.IsTrue(pessoaCriticas.Any());

            var rand = new Random();
            pessoaCriticas = pessoaCriticas.OrderBy(x => rand.Next()).ToList();

            return somenteAtivos ? pessoaCriticas.Where(x => x.Ativo).ToList() : pessoaCriticas;
        }
    }
}

