﻿using System;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using Controlsys.Dominio.Seguranca;
using Controlsys.Interfaces.Controllers;
using Controlsys.Repositorios.Seguranca;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Controlsys.Infra;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) EsqueciSenhaTeste.
    /// </summary>
    [TestClass]
    public class EsqueciSenhaTeste
    {
        /// <summary>
        /// Recuperar acesso.
        /// </summary>
        ///
        /// <exception cref="AssertFailedException">
        /// Lançada quando uma condição de erro Assert Failed ocorre.
        /// </exception>
        [TestMethod]
        public void RecuperarAcesso()
        {
            string cpf = "38570031629"; // 385.700.316-29
            const string senha1 = "Abcd1234@";
            const string senha2 = "@4321dcbA";

            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(unidTrabalho);
            Usuario usuarioAux = repUsuario.ObterPorLogin(cpf, Globalsys.Util.Tools.CriptografarMD5(senha1)) ??
                                 repUsuario.ObterPorLogin(cpf, Globalsys.Util.Tools.CriptografarMD5(senha2));
            Assert.IsNotNull(usuarioAux);

            var mockControllerContext = new Mock<ControllerContext>();
            var mockSession = new Mock<HttpSessionStateBase>();
            mockSession.SetupGet(s => s["Captcha"]).Returns("1"); //somevalue
            mockControllerContext.Setup(p => p.HttpContext.Session).Returns(mockSession.Object);
            var controller = new EsqueciSenhaController(unidTrabalho);
            //Set your controller ControllerContext with fake context
            controller.ControllerContext = mockControllerContext.Object;
            try
            {
                controller.RecuperarAcesso(usuarioAux.PessoaFisica.Email, cpf, null, 1);
            }
            catch (NullReferenceException)
            {
                
            }
            catch (Exception ex)
            {
                throw new AssertFailedException(ex.Message);
            }
        }
    }
}
