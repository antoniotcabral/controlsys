﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Controlsys.Dominio.Pessoas;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Globalsys;
using Globalsys.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Controlsys.Persistencia;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) EmitirCrachaTeste.
    /// </summary>
    [TestClass]
    public class EmitirCrachaTeste
    {
        /// <summary>
        /// Pode gerar cracha.
        /// </summary>
        ///
        /// <exception cref="AssertFailedException">
        /// Lançada quando uma condição de erro Assert Failed ocorre.
        /// </exception>
        [TestMethod]
        public void Pode_GerarCracha()
        {
            List<PessoaFisica> listaPessoaFisica = PessoaTeste.ObterListaPessoasFisica(true);
            Assert.IsNotNull(listaPessoaFisica);
            Assert.IsTrue(listaPessoaFisica.Any());

            PessoaFisica pessoaFisica = listaPessoaFisica[0];
            Papel papel = pessoaFisica.Papeis.OrderByDescending(p => p.Codigo).FirstOrDefault();
            Assert.IsNotNull(papel);

            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new EmitirCrachaController(unidTrabalho);

            var rand = new Random();
            string rfid = rand.Next(1, int.MaxValue).ToString(CultureInfo.InvariantCulture);
            try
            {
                var result = controller.GerarCracha(papel.Codigo, rfid) as JsonResult;
                Assert.IsNotNull(result);

                var emitirCracha = result.Data as CrachaModelView;
                Assert.IsNotNull(emitirCracha);
                Assert.AreEqual(pessoaFisica.Nome.ToLower(), emitirCracha.Nome.ToLower());
                Assert.AreEqual(papel.ModeloCracha.Nome.ToLower(), emitirCracha.Modelo.ToLower());
                Assert.IsTrue(emitirCracha.Ativo);
            }
            catch (CoreException ex)
            {
                const string reference = "RFID já existe cadastrado e ativo.";
                Assert.AreEqual(ex.Message.ToLower(), reference.ToLower());
            }
            catch (Exception ex)
            {
                throw new AssertFailedException(ex.Message);
            }
        }

        /// <summary>
        /// Pode descartar cracha.
        /// </summary>
        [TestMethod]
        public void Pode_DescartarCracha()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new EmitirCrachaController(unidTrabalho);

            Cracha cracha = ObterCracha(true);
            Assert.IsNotNull(cracha);
            var papel = cracha.Papel;
            Assert.IsNotNull(papel);
            var pessoaFisica = papel.PessoaFisica;
            Assert.IsNotNull(pessoaFisica);

            var result = controller.DescartarCracha(cracha.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var pfModelView = result.Data as PessoaFisicaModelView;
            Assert.IsNotNull(pfModelView);
            Assert.AreEqual(pessoaFisica.Codigo, pfModelView.CodigoPessoa);
            Assert.AreEqual(pessoaFisica.Nome.ToLower(), pfModelView.Nome.ToLower());
        }

        /// <summary>
        /// Pode gerar cracha PDF.
        /// </summary>
        ///
        /// <exception cref="AssertFailedException">
        /// Lançada quando uma condição de erro Assert Failed ocorre.
        /// </exception>
        [TestMethod]
        public void Pode_GerarCrachaPDF()
        {
            List<PessoaFisica> listaPessoaFisica = PessoaTeste.ObterListaPessoasFisica(true);
            Assert.IsNotNull(listaPessoaFisica);
            Assert.IsTrue(listaPessoaFisica.Any());

            PessoaFisica pessoaFisica = listaPessoaFisica[0];
            Papel papel = pessoaFisica.Papeis.OrderByDescending(p => p.Codigo).FirstOrDefault();
            Assert.IsNotNull(papel);

            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new EmitirCrachaController(unidTrabalho);

            try
            {
                //var result = controller.GerarCrachaPDF(papel.Codigo, "8") as JsonResult;
                //Assert.IsNotNull(result);
            }
            catch (Exception ex)
            {
                var methodError = ((System.Reflection.MemberInfo)(ex.TargetSite)).Name.ToLower();
                if (string.IsNullOrEmpty(methodError))
                    throw new AssertFailedException(ex.Message);
                Assert.IsTrue(methodError == "gerarcrachapdf" || methodError == "obterdescricaoenum");
            }
        }

        /// <summary>
        /// Pode selecionar pessoa.
        /// </summary>
        ///
        /// <exception cref="AssertFailedException">
        /// Lançada quando uma condição de erro Assert Failed ocorre.
        /// </exception>
        [TestMethod]
        public void Pode_SelecionarPessoa()
        {
            List<PessoaFisica> listaPessoaFisica = PessoaTeste.ObterListaPessoasFisica(true);
            Assert.IsNotNull(listaPessoaFisica);
            Assert.IsTrue(listaPessoaFisica.Any());

            PessoaFisica pessoaFisica = listaPessoaFisica[0];
            Papel papel = pessoaFisica.Papeis.OrderByDescending(p => p.Codigo).FirstOrDefault();
            Assert.IsNotNull(papel);

            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new EmitirCrachaController(unidTrabalho);

            try
            {
                var result = controller.SelecionarPessoa(papel.Codigo) as JsonResult;
                Assert.IsNotNull(result);

                var html = result.Data as String;
                Assert.IsNotNull(html);
                html = html.ToLower();
                StringAssert.Contains(html, pessoaFisica.Nome.ToLower());
                StringAssert.Contains(html, pessoaFisica.Nome.ToLower());
            }
            catch (Exception ex)
            {
                var methodError = ((System.Reflection.MemberInfo)(ex.TargetSite)).Name.ToLower();
                if (!string.IsNullOrEmpty(methodError))
                    Assert.IsTrue(methodError == "obterdescricaoenum" || methodError == "gravartemporario");
                else
                    throw new AssertFailedException(ex.Message);
            }
        }

        /// <summary>
        /// Busca aleatóriamente um Cracha cadastrado na base de dados.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// Filtrar Cracha ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Cracha.
        /// </returns>
        public static Cracha ObterCracha(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<Cracha>(unidTrabalho);
            var listaCracha = repositorio.ObterTodos().ToList();
            Assert.IsNotNull(listaCracha);
            Assert.IsTrue(listaCracha.Any());

            var rand = new Random();
            listaCracha = listaCracha.OrderBy(x => rand.Next()).ToList();

            return listaCracha.FirstOrDefault(x => !somenteAtivos || x.Ativo);
        }
    }
}
