﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.SessionState;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Seguranca;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Tests.Properties;
using Controlsys.Persistencia;
using Globalsys;
using Globalsys.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Web.Mvc;
using Moq;
using Rhino.Mocks;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) LoginTeste.
    /// </summary>
    [TestClass]
    public class LoginTeste
    {
        /// <summary>
        /// Bloqueia login invalido.
        /// </summary>
        ///
        /// <exception cref="AssertFailedException">
        /// Lançada quando uma condição de erro Assert Failed ocorre.
        /// </exception>
        [TestMethod]
        public void Bloqueia_Login_invalido()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            LoginController controller = new LoginController(unidTrabalho);
            var usuario = UsuarioTeste.ObterUsuario(true);
            Assert.IsNotNull(usuario);
            Assert.IsTrue(usuario.Ativo);

            var rand = new Random();
            var login = rand.Next(1, int.MaxValue).ToString();

            if (usuario.Login == login)
                login += rand.Next(1, int.MaxValue);

            if (usuario.Senha == null)
                usuario.Senha = login;

            try
            {
                var mockControllerContext = new Mock<ControllerContext>();
                var mockSession = new Mock<HttpSessionStateBase>();
                mockSession.SetupGet(s => s["Captcha"]).Returns("1"); //somevalue
                mockControllerContext.Setup(p => p.HttpContext.Session).Returns(mockSession.Object);
                controller.ControllerContext = mockControllerContext.Object;

                var result = controller.Login(login, usuario.Senha, usuario.AD, 1) as JsonResult;
                throw new AssertFailedException("Exceção esperada não lançada");
            }
            catch (CoreException ex)
            {
                StringAssert.Contains(ex.Message.ToLower(), Resources.Usuario_Ou_Senha_Invalida.ToLower());
            }
        }

        /// <summary>
        /// Bloqueia senha invalida.
        /// </summary>
        ///
        /// <exception cref="AssertFailedException">
        /// Lançada quando uma condição de erro Assert Failed ocorre.
        /// </exception>
        [TestMethod]
        public void Bloqueia_Senha_invalida()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            LoginController controller = new LoginController(unidTrabalho);
            var usuario = UsuarioTeste.ObterUsuario(true);
            Assert.IsNotNull(usuario);
            Assert.IsTrue(usuario.Ativo);

            var rand = new Random();
            var senha = rand.Next(1, int.MaxValue).ToString();

            if (usuario.Senha == null)
                usuario.Senha = senha;
            else if (usuario.Senha == senha)
                usuario.Senha += rand.Next(1, 999);

            try
            {
                var mockControllerContext = new Mock<ControllerContext>();
                var mockSession = new Mock<HttpSessionStateBase>();
                mockSession.SetupGet(s => s["Captcha"]).Returns("1"); //somevalue
                mockControllerContext.Setup(p => p.HttpContext.Session).Returns(mockSession.Object);
                controller.ControllerContext = mockControllerContext.Object;

                var result = controller.Login(usuario.Login, usuario.Senha, usuario.AD, 1) as JsonResult;
                throw new AssertFailedException("Exceção esperada não lançada");
            }
            catch (CoreException ex)
            {
                StringAssert.Contains(ex.Message.ToLower(), Resources.Usuario_Ou_Senha_Invalida.ToLower());
            }
        }

        /// <summary>
        /// Bloqueia captcha invalido.
        /// </summary>
        ///
        /// <exception cref="AssertFailedException">
        /// Lançada quando uma condição de erro Assert Failed ocorre.
        /// </exception>
        [TestMethod]
        public void Bloqueia_Captcha_Invalido()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            LoginController controller = new LoginController(unidTrabalho);
            var usuario = UsuarioTeste.ObterUsuario(true);
            Assert.IsNotNull(usuario);
            Assert.IsTrue(usuario.Ativo);

            var rand = new Random();
            var senha = rand.Next(1, int.MaxValue).ToString();

            if (usuario.Senha == null)
                usuario.Senha = senha;
            else if (usuario.Senha == senha)
                usuario.Senha += rand.Next(1, 999);

            try
            {
                var mockControllerContext = new Mock<ControllerContext>();
                var mockSession = new Mock<HttpSessionStateBase>();
                mockSession.SetupGet(s => s["Captcha"]).Returns("1"); //somevalue
                mockControllerContext.Setup(p => p.HttpContext.Session).Returns(mockSession.Object);
                controller.ControllerContext = mockControllerContext.Object;

                var result = controller.Login(usuario.Login, usuario.Senha, usuario.AD, 2) as JsonResult;
                throw new AssertFailedException("Exceção esperada não lançada");
            }
            catch (CoreException ex)
            {
                StringAssert.Contains(ex.Message.ToLower(), "o resultado da soma não confere");
            }
        }

        /// <summary>
        /// Bloqueia usuario inativado.
        /// </summary>
        ///
        /// <exception cref="AssertFailedException">
        /// Lançada quando uma condição de erro Assert Failed ocorre.
        /// </exception>
        [TestMethod]
        public void Bloqueia_Usuario_Inativado()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new LoginController(unidTrabalho);
            var usuarios = UsuarioTeste.ObterListaUsuario();
            Assert.IsTrue(usuarios.Any());

            var usuario = usuarios.First(x => x.Ativo == false);
            Assert.IsNotNull(usuario);
            Assert.IsFalse(usuario.Ativo);

            var rand = new Random();
            var senha = rand.Next(1, int.MaxValue).ToString();

            if (usuario.Senha == null)
                usuario.Senha = senha;
            else if (usuario.Senha == senha)
                usuario.Senha += rand.Next(1, 999);

            try
            {
                var mockControllerContext = new Mock<ControllerContext>();
                var mockSession = new Mock<HttpSessionStateBase>();
                mockSession.SetupGet(s => s["Captcha"]).Returns("1"); //somevalue
                mockControllerContext.Setup(p => p.HttpContext.Session).Returns(mockSession.Object);
                controller.ControllerContext = mockControllerContext.Object;

                var result = controller.Login(usuario.Login, usuario.Senha, usuario.AD, 1) as JsonResult;
                throw new AssertFailedException("Exceção esperada não lançada");
            }
            catch (CoreException ex)
            {
                StringAssert.Contains(ex.Message.ToLower(), Resources.Usuario_Ou_Senha_Invalida.ToLower());
            }
        }

        //[TestMethod]

        /// <summary>
        /// Pode logar.
        /// </summary>
        ///
        /// <exception cref="AssertFailedException">
        /// Lançada quando uma condição de erro Assert Failed ocorre.
        /// </exception>
        public void Pode_Logar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new LoginController(unidTrabalho);
            var usuarios = UsuarioTeste.ObterListaUsuario(true);
            Assert.IsTrue(usuarios.Any());

            var usuario = usuarios.First(x => !string.IsNullOrEmpty(x.Senha));
            Assert.IsNotNull(usuario);
            Assert.IsTrue(usuario.Ativo);

            var rand = new Random();
            var senha = rand.Next(1, int.MaxValue).ToString();

            if (usuario.Senha == null)
                usuario.Senha = senha;
            else if (usuario.Senha == senha)
                usuario.Senha += rand.Next(1, 999);

            try
            {
                var mockControllerContext = new Mock<ControllerContext>();
                var mockSession = new Mock<HttpSessionStateBase>();
                mockSession.SetupGet(s => s["Captcha"]).Returns("1"); //somevalue
                mockControllerContext.Setup(p => p.HttpContext.Session).Returns(mockSession.Object);
                controller.ControllerContext = mockControllerContext.Object;

                var result = controller.Login(usuario.Login, usuario.Senha, usuario.AD, 1) as JsonResult;
                throw new AssertFailedException("Exceção esperada não lançada");
            }
            catch (NullReferenceException ex)
            {
                StringAssert.Contains(ex.Message.ToLower(), "object reference not set to an instance of an object");
            }
        }

    }


}
