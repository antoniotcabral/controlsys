﻿using System;
using System.Collections.Generic;
using System.Linq;
using Controlsys.Dominio.Seguranca;
using Controlsys.Infra;
using Controlsys.Persistencia.Repositorios.Seguranca;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) PaginaTeste.
    /// </summary>
    [TestClass]
    public class PaginaTeste
    {
        /// <summary>
        /// Obter pagina.
        /// </summary>
        ///
        /// <param name="somenteAtivas">
        /// true to somente ativas.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Pagina.
        /// </returns>
        [TestMethod]
        public static Pagina ObterPagina(bool somenteAtivas = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new RepositorioPagina(unidTrabalho);
            var paginas = repositorio.ObterTodos();
            if (somenteAtivas == true)
                paginas = paginas.Where(x => x.Ativo == true);
            Assert.IsTrue(paginas.Any());

            var rand = new Random();
            List<Pagina> listaPaginas = paginas.ToList();
            listaPaginas.OrderBy(x => rand.Next());

            Pagina pagina = listaPaginas.FirstOrDefault();

            return pagina;
        }
    }
}
