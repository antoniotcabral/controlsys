﻿using System;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Collections.Generic;
using System.Web.Mvc;
using Controlsys.Dominio.Empresas;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Globalsys;
using Globalsys.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Controlsys.Persistencia;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// Summary description for AreaTeste.
    /// </summary>
    [TestClass]
    public class AreaTeste
    {
        /// <summary>
        /// Pode incluir.
        /// </summary>
        [TestMethod]
        public void Pode_Incluir()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            AreaController controller = new AreaController(unidTrabalho);
            string data = DateTime.Now.ToString("MM/dd HH:mm:ss.fff");
            Area areaNovo = new Area()
            {
                Nome = "Nome-" + data,
                Numero = "Num-" + data
            };

            //var area = (controller.Incluir(areaNovo) as JsonResult).Data as Area;
            var result = controller.Incluir(areaNovo) as JsonResult;
            var area = JsonHelper.GetJsonObjectRepresentation<Area>(result);
            Assert.IsNotNull(area);

            Assert.AreEqual("Nome-" + data, area.Nome);
            Assert.AreEqual("Num-" + data, area.Numero);
        }

        /// <summary>
        /// Pode alterar.
        /// </summary>
        [TestMethod]
        public void Pode_Alterar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            AreaController controller = new AreaController(unidTrabalho);
            JsonResult result = controller.ObterTodos() as JsonResult;
            var areas = JsonHelper.GetJsonObjectRepresentation<List<Area>>(result);
            Assert.IsNotNull(areas);

            Random rand = new Random();
            areas = areas.OrderBy(x => rand.Next()).ToList();

            Area areaAux = null;
            foreach (var item in areas)
            {
                //areaAux = (controller.Selecionar(item.Codigo) as JsonResult).Data as Area;
                result = controller.Selecionar(item.Codigo) as JsonResult;
                areaAux = JsonHelper.GetJsonObjectRepresentation<Area>(result);
                if (areaAux == null)
                    continue;
                if (areaAux.Ativo)
                    break;
                areaAux = null;
            }
            Assert.IsNotNull(areaAux);

            Assert.IsTrue(areaAux.Ativo);
            string data = DateTime.Now.ToString("MM/dd HH:mm:ss.fff");
            areaAux.Nome = "Nome-" + data;
            areaAux.Numero = "Num-" + data;
            //var area = (controller.Alterar(areaAux) as JsonResult).Data as Area;
            result = controller.Alterar(areaAux) as JsonResult;
            var area = JsonHelper.GetJsonObjectRepresentation<Area>(result);
            Assert.AreEqual("Nome-" + data, area.Nome);
            Assert.AreEqual("Num-" + data, area.Numero);

            //else
            //{
            //    areaAux.Ativar();
            //    var area = (controller.Alterar(areaAux) as JsonResult).Data as Area;
            //    Assert.AreEqual(true, area.Ativo);
            //}
        }

        /// <summary>
        /// Pode ativar.
        /// </summary>
        [TestMethod]
        public void Pode_Ativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            AreaController controller = new AreaController(unidTrabalho);

            var repositorio = new Repositorio<Area>(unidTrabalho);
            var areas = repositorio.ObterTodos().Where(x => x.Ativo == false).ToList();
            Assert.IsNotNull(areas);
            Assert.IsTrue(areas.Any());

            var rand = new Random();
            areas = new List<Area>(areas.OrderBy(x => rand.Next()));

            Area areaAux = areas.ToList()[0];
            Assert.IsNotNull(areaAux);
            Assert.IsFalse(areaAux.Ativo);

            var result = controller.AtivarInativar(areaAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            //var area = result.Data as Area;
            var area = JsonHelper.GetJsonObjectRepresentation<Area>(result);
            Assert.IsNotNull(area);
            Assert.AreEqual(areaAux.Codigo, area.Codigo);
            Assert.IsTrue(area.Ativo);
        }

        /// <summary>
        /// Pode inativar.
        /// </summary>
        [TestMethod]
        public void Pode_Inativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            AreaController controller = new AreaController(unidTrabalho);

            Area areaAux = ObterArea(true);
            Assert.IsNotNull(areaAux);
            Assert.IsTrue(areaAux.Ativo);

            var result = controller.AtivarInativar(areaAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            //var area = result.Data as Area;
            var area = JsonHelper.GetJsonObjectRepresentation<Area>(result);
            Assert.IsNotNull(area);
            Assert.AreEqual(areaAux.Codigo, area.Codigo);
            Assert.IsFalse(area.Ativo);
        }

        /// <summary>
        /// Obter area.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// true to somente ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Area.
        /// </returns>
        public static Area ObterArea(bool somenteAtivos = false, string nome = "")
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            AreaController controller = new AreaController(unidTrabalho);
            JsonResult result = controller.ObterTodos(somenteAtivos, nome) as JsonResult;
            var areas = JsonHelper.GetJsonObjectRepresentation<List<Area>>(result);
            Assert.IsNotNull(areas);

            Random rand = new Random();
            areas = areas.OrderBy(x => rand.Next()).ToList();

            Area areaAux = null;
            foreach (var item in areas)
            {
                result = controller.Selecionar(item.Codigo) as JsonResult;
                Assert.IsNotNull(result.Data);
                areaAux = JsonHelper.GetJsonObjectRepresentation<Area>(result);

                //areaAux = (controller.Selecionar(item.Codigo) as JsonResult).Data as Area;
                //Assert.IsNotNull(areaAux);

                if (somenteAtivos)
                {
                    if (areaAux.Ativo)
                        break;
                    areaAux = null;
                }
                else
                    break;
            }

            return areaAux;
        }

        /// <summary>
        /// Obter lista area.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// true to somente ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;Area&gt;
        /// </returns>
        public static List<Area> ObterListaArea(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<Area>(unidTrabalho);
            var areas = repositorio.ObterTodos().ToList();
            Assert.IsTrue(areas.Any());

            var rand = new Random();
            areas = areas.OrderBy(x => rand.Next()).ToList();

            return somenteAtivos ? areas.Where(x => x.Ativo).ToList() : areas;
        }
    }
}
