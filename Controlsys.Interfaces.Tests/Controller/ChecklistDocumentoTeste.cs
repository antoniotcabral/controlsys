﻿using System;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Collections.Generic;
using System.Web.Mvc;
using Controlsys.Dominio.Parametros;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Controlsys.Interfaces.Tests.Properties;
using Globalsys;
using Globalsys.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;
using Controlsys.Persistencia;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) ChecklistDocumentoTeste.
    /// </summary>
    [TestClass]
    public class ChecklistDocumentoTeste
    {
        /// <summary>
        /// Pode incluir.
        /// </summary>
        [TestMethod]
        public void Pode_Incluir()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new ChecklistDocumentoController(unidTrabalho);
            var data = DateTime.Now.ToString("MM/dd HH:mm:ss.fff");
            var checklistDocumento = new ChecklistDocumento()
            {
                Nome = "Nome-" + data,
                Descricao = "Desc-" + data,
                TipoCadastro = Global.ObterEnumAleatorio<TipoCadastro>()
            };

            TipoDocumento tipoDocumento = TipoDocumentoTeste.ObterTipoDocumento(true);
            checklistDocumento.TiposDocumento.Add(new ChecklistTipoDocumento()
            {
                ChecklistDocumento = checklistDocumento,
                TipoDocumento = tipoDocumento
            });

            var tiposDocumento = new List<TipoDocumento> { tipoDocumento };
            JsonResult result = null;
            try
            {
                result = controller.Incluir(checklistDocumento, tiposDocumento) as JsonResult;
                Assert.IsNotNull(result);

                var checklistdoc = result.Data as ChecklistDocumentoModelView;
                Assert.IsNotNull(checklistdoc);
                Assert.AreEqual(checklistDocumento.Nome, checklistdoc.Nome);
                Assert.AreEqual(checklistDocumento.Descricao, checklistdoc.Descricao);
                Assert.AreEqual(checklistDocumento.TipoCadastro.ObterDescricaoEnum(), checklistdoc.TipoCadastro);
            }
            catch (CoreException ex)
            {
                StringAssert.Contains(ex.Message.ToLower(), "já existe um checklist");
            }
        }

        /// <summary>
        /// Pode alterar.
        /// </summary>
        [TestMethod]
        public void Pode_Alterar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new ChecklistDocumentoController(unidTrabalho);
            var checklistdocAux = ObterChecklistDocumento(true);
            Assert.IsTrue(checklistdocAux.Ativo);

            var data = DateTime.Now.ToString("MM/dd HH:mm:ss.fff");
            checklistdocAux.Nome = "Nome-" + data;
            checklistdocAux.Descricao = "Desc-" + data;

            var checklistdocConvertido = ChecklistDocumentoModelView.ObterChecklistDocumento(checklistdocAux);
            var result = controller.Alterar(checklistdocConvertido, null, null) as JsonResult;
            Assert.IsNotNull(result);

            var checklistdoc = result.Data as ChecklistDocumentoModelView;
            Assert.IsNotNull(checklistdoc);
            Assert.AreEqual(checklistdocAux.Codigo, checklistdoc.Codigo);
            Assert.AreEqual("Nome-" + data, checklistdoc.Nome);
            Assert.AreEqual("Desc-" + data, checklistdoc.Descricao);
        }

        /*
        [TestMethod]
        public void Pode_AtivarInativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            ChecklistDocumentoController controller = new ChecklistDocumentoController(unidTrabalho);
            ChecklistDocumento checklistdocAux = ObterChecklistDocumento();
            Assert.IsNotNull(checklistdocAux);

            bool status = !checklistdocAux.Ativo;
            var result = controller.AtivarInativar(checklistdocAux.Codigo) as JsonResult;
            var checklistdoc = JsonHelper.GetJsonObjectRepresentation<ChecklistDocumento>(result);
            Assert.IsNotNull(checklistdoc);
            Assert.AreEqual(status, checklistdoc.Ativo);
        }
        */

        /// <summary>
        /// Pode ativar.
        /// </summary>
        [TestMethod]
        public void Pode_Ativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new ChecklistDocumentoController(unidTrabalho);
            var lista = ObterListaChecklistDocumento();
            Assert.IsTrue(lista.Any());

            lista = lista.Where(x => x.Ativo == false).ToList();
            Assert.IsTrue(lista.Any(), Resources.Lista_vazia);

            var checklistdocAux = lista[0];
            Assert.IsFalse(checklistdocAux.Ativo);

            JsonResult result = null;
            try
            {
                result = controller.AtivarInativar(checklistdocAux.Codigo) as JsonResult;
                Assert.IsNotNull(result);

                var checklistdoc = result.Data as ChecklistDocumentoModelView;
                Assert.IsNotNull(checklistdoc);
                Assert.IsTrue(checklistdoc.Ativo);
            }
            catch (CoreException ex)
            {
                StringAssert.Contains(ex.Message.ToLower(), "já existe um checklist");
            }

            //var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            //ChecklistDocumentoController controller = new ChecklistDocumentoController(unidTrabalho);
            //
            //var repositorio = new Repositorio<ChecklistDocumento>(unidTrabalho);
            //var checklistDocumentos = repositorio.ObterTodos().Where(x => x.Ativo == false).ToList();
            //Assert.IsTrue(checklistDocumentos.Any());
            //
            //var rand = new Random();
            //checklistDocumentos = new List<ChecklistDocumento>(checklistDocumentos.OrderBy(x => rand.Next()));
            //
            //ChecklistDocumento checklistDocumentoAux = checklistDocumentos.ToList()[0];
            //Assert.IsNotNull(checklistDocumentoAux);
            //Assert.IsFalse(checklistDocumentoAux.Ativo);
            //
            //var result = controller.AtivarInativar(checklistDocumentoAux.Codigo) as JsonResult;
            //Assert.IsNotNull(result);
            //
            //var checklistDocumento = JsonHelper.GetJsonObjectRepresentation<ChecklistDocumento>(result);
            //Assert.IsNotNull(checklistDocumento);
            //Assert.AreEqual(checklistDocumentoAux.Codigo, checklistDocumento.Codigo);
            //Assert.IsTrue(checklistDocumento.Ativo);
        }

        /// <summary>
        /// Pode inativar.
        /// </summary>
        [TestMethod]
        public void Pode_Inativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new ChecklistDocumentoController(unidTrabalho);
            var checklistDocumentoAux = ObterChecklistDocumento(true);
            Assert.IsTrue(checklistDocumentoAux.Ativo);

            var result = controller.AtivarInativar(checklistDocumentoAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var checklistDocumento = JsonHelper.GetJsonObjectRepresentation<ChecklistDocumentoModelView>(result);
            Assert.IsNotNull(checklistDocumento);
            Assert.AreEqual(checklistDocumentoAux.Codigo, checklistDocumento.Codigo);
            Assert.IsFalse(checklistDocumento.Ativo);
        }

        /// <summary>
        /// Obter checklist documento.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// true to somente ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ChecklistDocumentoModelView.
        /// </returns>
        private ChecklistDocumentoModelView ObterChecklistDocumento(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new ChecklistDocumentoController(unidTrabalho);
            var result = controller.ObterTodos() as JsonResult;
            Assert.IsNotNull(result);

            var checklistDocs = JsonHelper.GetJsonObjectRepresentation<List<ChecklistDocumentoModelView>>(result);
            Assert.IsTrue(checklistDocs.Any());

            var rand = new Random();
            checklistDocs = checklistDocs.OrderBy(x => rand.Next()).ToList();

            ChecklistDocumentoModelView checklistDocAux = null;
            foreach (var item in checklistDocs)
            {
                result = controller.Selecionar(item.Codigo) as JsonResult;
                Assert.IsNotNull(result);

                checklistDocAux = result.Data as ChecklistDocumentoModelView;
                Assert.IsNotNull(checklistDocAux);

                if (somenteAtivos)
                {
                    if (checklistDocAux.Ativo)
                        break;
                    checklistDocAux = null;
                }
                else
                    break;
            }

            return checklistDocAux;
        }

        /// <summary>
        /// Obter lista checklist documento.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// true to somente ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;ChecklistDocumentoModelView&gt;
        /// </returns>
        private List<ChecklistDocumentoModelView> ObterListaChecklistDocumento(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new ChecklistDocumentoController(unidTrabalho);
            var result = controller.ObterTodos() as JsonResult;
            Assert.IsNotNull(result);

            var checklistDocs = JsonHelper.GetJsonObjectRepresentation<List<ChecklistDocumentoModelView>>(result);
            //var checklistDocs = result.Data as List<ChecklistDocumentoModelView>;
            //Assert.IsNotNull(checklistDocs);

            var rand = new Random();
            checklistDocs = checklistDocs.OrderBy(x => rand.Next()).ToList();

            return somenteAtivos ? checklistDocs.Where(x => x.Ativo).ToList() : checklistDocs;
        }

        //public static TipoCadastro ObterTipoCadastro()
        //{
        //    Array values = Enum.GetValues(typeof(TipoCadastro));
        //    Random random = new Random();
        //    return (TipoCadastro)values.GetValue(random.Next(values.Length));
        //}
    }
}
