﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Controlsys.Dominio.Empresas;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Controlsys.Interfaces.Tests.Properties;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Controlsys.Persistencia;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) SetorCustoTeste.
    /// </summary>
    [TestClass]
    public class SetorCustoTeste
    {
        /// <summary>
        /// Valida incluir um novo SetorCusto, informando TipoSetor, Numero, Nome e Descricao.
        /// </summary>
        [TestMethod]
        public void Pode_Incluir()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new SetorCustoController(unidTrabalho);

            var rand = new Random();
            var dt = DateTime.Now;
            var data = dt.ToString("MM/dd HH:mm:ss.fff");
            var tipoSetor = Global.ObterEnumAleatorio<TipoSetorCusto>();
            var numero = "Numero-" + data;
            var nome = "Nome-" + data;
            var descricao = "Descricao" + data;

            var setorCustoAux = new SetorCusto
            {
                TipoSetor = tipoSetor,
                Numero = numero,
                Nome = nome,
                Descricao = descricao
            };

            var result = controller.Incluir(setorCustoAux) as JsonResult;
            Assert.IsNotNull(result);

            var setorCusto = result.Data as SetorCustoModelView;
            Assert.IsNotNull(setorCusto);
            //TODO: Assert.AreEqual(setorCustoAux.TipoSetor, setorCusto.TipoSetor);
            Assert.AreEqual(setorCustoAux.Numero, setorCusto.Numero);
            Assert.AreEqual(setorCustoAux.Nome, setorCusto.Nome);
            Assert.AreEqual(setorCustoAux.Descricao, setorCusto.Descricao);
        }

        /// <summary>
        /// Valida alterar um SetorCusto, modificando TipoSetor, Numero, Nome e Descricao.
        /// </summary>
        [TestMethod]
        public void Pode_Alterar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new SetorCustoController(unidTrabalho);
            var setorCustoAux = ObterSetorCusto(true);
            Assert.IsNotNull(setorCustoAux);
            Assert.IsTrue(setorCustoAux.Ativo);

            var rand = new Random();
            var dt = DateTime.Now;
            var data = dt.ToString("MM/dd HH:mm:ss.fff");
            var tipoSetor = Global.ObterEnumAleatorio<TipoSetorCusto>();
            var numero = "Numero-" + data;
            var nome = "Nome-" + data;
            var descricao = "Descricao" + data;

            setorCustoAux.TipoSetor = tipoSetor;
            setorCustoAux.Numero = numero;
            setorCustoAux.Nome = nome;
            setorCustoAux.Descricao = descricao;

            var result = controller.Alterar(setorCustoAux) as JsonResult;
            Assert.IsNotNull(result);

            var setorCusto = result.Data as SetorCustoModelView;
            Assert.IsNotNull(setorCusto);
            //TODO: Assert.AreEqual(setorCustoAux.TipoSetor, setorCusto.TipoSetor);
            Assert.AreEqual(setorCustoAux.Numero, setorCusto.Numero);
            Assert.AreEqual(setorCustoAux.Nome, setorCusto.Nome);
            Assert.AreEqual(setorCustoAux.Descricao, setorCusto.Descricao);
        }

        #region [Desabilitado]

        /*
        /// <summary>
        /// Valida ativação ou inativação de SetorCusto, conforme status corrente;
        /// </summary>
        [TestMethod]
        public void Pode_AtivarInativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            SetorCustoController controller = new SetorCustoController(unidTrabalho);
            SetorCusto setorCustoAux = ObterSetorCusto();
            Assert.IsNotNull(setorCustoAux);

            bool status = !setorCustoAux.Ativo;
            var result = controller.AtivarInativar(setorCustoAux.Codigo) as JsonResult;
            Assert.IsNotNull(result.Data);

            var setorCusto = JsonHelper.GetJsonObjectRepresentation<SetorCusto>(result);
            Assert.AreEqual(status, setorCusto.Ativo);
        }

        /// <summary>
        /// Busca aleatóriamente um SetorCusto cadastrado na base de dados
        /// </summary>
        /// <param name="somenteAtivos">Filtrar SetorCusto ativos</param>
        /// <returns></returns>
        public static SetorCusto ObterSetorCusto(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            SetorCustoController controller = new SetorCustoController(unidTrabalho);
            JsonResult result = controller.ObterTodos() as JsonResult;
            Assert.IsNotNull(result.Data);

            Random rand = new Random();
            var listaSetorCusto = JsonHelper.GetJsonObjectRepresentation<List<SetorCusto>>(result);
            listaSetorCusto = listaSetorCusto.OrderBy(x => rand.Next()).ToList();

            SetorCusto setorCusto = null;
            foreach (var item in listaSetorCusto)
            {
                result = controller.Selecionar(item.Codigo) as JsonResult;
                Assert.IsNotNull(result.Data);

                setorCusto = JsonHelper.GetJsonObjectRepresentation<SetorCusto>(result);
                if (somenteAtivos)
                {
                    if (setorCusto.Ativo)
                        break;
                    setorCusto = null;
                }
                else
                    break;
            }

            return setorCusto;
        }
        */

        #endregion

        /// <summary>
        /// Valida Ativar SetorCusto.
        /// </summary>
        [TestMethod]
        public void Pode_Ativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new SetorCustoController(unidTrabalho);
            var lista = ObterListaSetorCusto();
            Assert.IsTrue(lista.Any(), Resources.Lista_vazia);

            var setorCustoAux = lista.First(x => x.Ativo == false);
            Assert.IsNotNull(setorCustoAux);
            Assert.IsFalse(setorCustoAux.Ativo);

            var result = controller.AtivarInativar(setorCustoAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var setorCusto = result.Data as SetorCustoModelView;
            Assert.IsNotNull(setorCusto);
            Assert.AreEqual(setorCustoAux.Codigo, setorCusto.Codigo);
            Assert.IsTrue(setorCusto.Ativo);
        }

        /// <summary>
        /// Valida Inativar SetorCusto.
        /// </summary>
        [TestMethod]
        public void Pode_Inativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new SetorCustoController(unidTrabalho);
            var setorCustoAux = ObterSetorCusto(true);
            Assert.IsTrue(setorCustoAux.Ativo);

            var result = controller.AtivarInativar(setorCustoAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var setorCusto = result.Data as SetorCustoModelView;
            Assert.IsNotNull(setorCusto);
            Assert.AreEqual(setorCustoAux.Codigo, setorCusto.Codigo);
            Assert.IsFalse(setorCusto.Ativo);
        }

        /// <summary>
        /// Busca aleatóriamente um SetorCusto cadastrado na base de dados.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// Filtrar SetorCustos ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) SetorCusto.
        /// </returns>
        public static SetorCusto ObterSetorCusto(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<SetorCusto>(unidTrabalho);
            var listaSetorCusto = repositorio.ObterTodos().ToList();
            Assert.IsTrue(listaSetorCusto.Any());

            var rand = new Random();
            listaSetorCusto = listaSetorCusto.OrderBy(x => rand.Next()).ToList();

            return listaSetorCusto.First(x => !somenteAtivos || x.Ativo);
        }

        /// <summary>
        /// Obter lista setor custo.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// Filtrar SetorCustos ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;SetorCusto&gt;
        /// </returns>
        public static List<SetorCusto> ObterListaSetorCusto(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<SetorCusto>(unidTrabalho);
            var listaSetorCusto = repositorio.ObterTodos().ToList();
            Assert.IsTrue(listaSetorCusto.Any());

            var rand = new Random();
            listaSetorCusto = listaSetorCusto.OrderBy(x => rand.Next()).ToList();

            return somenteAtivos ? listaSetorCusto.Where(x => x.Ativo).ToList() : listaSetorCusto;
        }
    }
}
