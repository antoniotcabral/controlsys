﻿using System;
using System.Linq;
using Controlsys.Interfaces.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Controlsys.Infra;
using Globalsys;
using Controlsys.Interfaces.Controllers;
using Controlsys.Repositorio.Empresas;
using Controlsys.Dominio.Parametros;
using Controlsys.Persistencia;
using System.Web.Mvc;
using System.Collections.Generic;
using Controlsys.Persistencia.Repositorios.Empresas;

namespace Controlsys.Interfaces.Tests.Controller
{
    [TestClass]
    public class ClassificacaoJuridicaTeste
    {
        [TestMethod]
        public void Ativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();

            var reg1 = Obter(false, true, true);
            Assert.IsNotNull(reg1);

            var controller = new ClassificacaoJuridicaController(unidTrabalho);
            JsonResult result = controller.AtivarInativar(reg1.Codigo) as JsonResult;
            var reg2 = JsonHelper.GetJsonObjectRepresentation<ClassificacaoJuridicaModelView>(result);
            Assert.IsNotNull(reg2);
            Assert.AreEqual(reg1.Codigo, reg2.Codigo);
            Assert.IsTrue(reg2.Ativo);

            unidTrabalho.Rollback();
        }

        [TestMethod]
        public void Inativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();

            var reg1 = Obter(true, true, true);
            Assert.IsNotNull(reg1);

            var controller = new ClassificacaoJuridicaController(unidTrabalho);
            JsonResult result = controller.AtivarInativar(reg1.Codigo) as JsonResult;
            var reg2 = JsonHelper.GetJsonObjectRepresentation<ClassificacaoJuridicaModelView>(result);
            Assert.IsNotNull(reg2);
            Assert.AreEqual(reg1.Codigo, reg2.Codigo);
            Assert.IsFalse(reg2.Ativo);

            unidTrabalho.Rollback();
        }

        [TestMethod]
        public void Alterar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();

            var reg1 = Obter(false, true, true);
            Assert.IsNotNull(reg1);
            reg1.Formula = Obter(false, true, true).Letra;
            reg1.Peso = new Random().Next(1, 10);

            var controller = new ClassificacaoJuridicaController(unidTrabalho);
            JsonResult result = controller.Alterar(reg1) as JsonResult;
            var reg2 = JsonHelper.GetJsonObjectRepresentation<ClassificacaoJuridicaModelView>(result);
            Assert.IsNotNull(reg2);
            Assert.AreEqual(reg1.Codigo, reg2.Codigo);
            Assert.AreEqual(reg1.Formula, reg2.Formula);
            Assert.AreEqual(reg1.Peso, reg2.Peso);

            unidTrabalho.Rollback();
        }

        public static ClassificacaoJuridica Obter(bool ativo, bool pontua, bool comFormula) {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();

            var rep = new RepositorioClassificacaoJuridica(unidTrabalho);
            var lista = rep.ObterTodos().Where(x => 
                (x.Ativo == ativo) &&
                (x.Pontua == pontua)
            ).ToList();

            lista = lista.Where(x => !string.IsNullOrEmpty(x.Formula) == comFormula).ToList();
            Assert.IsNotNull(lista);
            Assert.IsTrue(lista.Any());

            Random rand = new Random();
            lista = lista.OrderBy(x => rand.Next()).ToList();
            return lista.FirstOrDefault();
        }
    }
}
