﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Controlsys.Dominio.Parametros;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Controlsys.Persistencia;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) ParametroTeste.
    /// </summary>
    [TestClass]
    public class ParametroTeste
    {
        /// <summary>
        /// Valida alterar um Parametro, modificando o nome e descrição.
        /// </summary>
        [TestMethod]
        public void Pode_Alterar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new ParametroController(unidTrabalho);
            var parametroAux = ObterParametro();
            Assert.IsNotNull(parametroAux);

            var rand = new Random();
            var randValue = rand.Next(1, 999);
            var nome = Global.ObterEnumAleatorio<ParametroSistema>();
            var valor = randValue.ToString();
            var tipoParametro = Global.ObterEnumAleatorio<TipoParametro>();

            parametroAux.Nome = nome;
            parametroAux.Valor = valor;
            parametroAux.TipoParametro = tipoParametro;

            var result = controller.Alterar(parametroAux) as JsonResult;
            Assert.IsNotNull(result);

            var parametro = JsonHelper.GetJsonObjectRepresentation<ParametroModelView>(result);
            Assert.IsNotNull(parametro);
            Assert.AreEqual(parametroAux.Codigo, parametro.Codigo);
            //Assert.AreEqual(parametroAux.Nome.ObterDescricaoEnum(), Global.GetValueFromDescription<ParametroSistema>(parametro.Nome));
            Assert.AreEqual(parametroAux.Valor, parametro.Valor);
            //Assert.AreEqual(parametroAux.TipoParametro.ObterDescricaoEnum(), Global.GetValueFromDescription<TipoParametro>(parametro.Tipo));
        }

        /// <summary>
        /// Busca aleatóriamente um Parametro cadastrado na base de dados.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) Parametro.
        /// </returns>
        public static Parametro ObterParametro()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<Parametro>(unidTrabalho);
            var parametros = repositorio.ObterTodos().ToList();
            Assert.IsTrue(parametros.Any());

            var rand = new Random();
            parametros = parametros.OrderBy(x => rand.Next()).ToList();

            return parametros[0];
        }
    }
}
