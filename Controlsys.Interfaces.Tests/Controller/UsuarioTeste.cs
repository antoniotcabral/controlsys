﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using Controlsys.Dominio.Enderecos;
using Controlsys.Dominio.Pessoas;
using Controlsys.Dominio.Seguranca;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Controlsys.Interfaces.Tests.Properties;
using Controlsys.Persistencia;
using Ext.Net.MVC;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Rhino.Mocks;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) UsuarioTeste.
    /// </summary>
    [TestClass]
    public class UsuarioTeste
    {
        /// <summary>
        /// Valida incluir um novo Usuario.
        /// </summary>
        [TestMethod]
        public void Pode_Incluir()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new UsuarioController(unidTrabalho);

            var dt = DateTime.Now;
            var rand = new Random();
            var randValue = rand.Next(1, 9999);

            #region [Estado]
            var repEstado = new Repositorio<Estado>(unidTrabalho);
            var estados = repEstado.ObterTodos().ToList();
            Assert.IsTrue(estados.Any());
            estados = estados.OrderBy(x => rand.Next()).ToList();
            var estado = estados[0];
            #endregion
            #region [Cidade]
            var repCidade = new Repositorio<Cidade>(unidTrabalho);
            var cidades = repCidade.ObterTodos().ToList();
            Assert.IsTrue(cidades.Any());
            cidades = cidades.Where(x => x.Estado.Codigo == estado.Codigo).OrderBy(x => rand.Next()).ToList();
            Assert.IsTrue(cidades.Any());
            var cidade = cidades[0];
            #endregion
            #region [Telefones]
            var telefones = new List<Telefone>()
            {
                new Telefone()
                {
                    TipoTelefone = TipoTelefone.Celular,
                    TelefoneNum = Global.GerarTelefone(TipoTelefone.Celular)
                },
                new Telefone()
                {
                    TipoTelefone = TipoTelefone.Emergencial,
                    TelefoneNum = Global.GerarTelefone(TipoTelefone.Emergencial)
                },
                new Telefone()
                {
                    TipoTelefone = TipoTelefone.Celular,
                    TelefoneNum = Global.GerarTelefone(TipoTelefone.Residencial)
                },
                new Telefone()
                {
                    TipoTelefone = TipoTelefone.Empresarial,
                    TelefoneNum = Global.GerarTelefone(TipoTelefone.Empresarial)
                }

            };
            #endregion
            #region [Emails]
            var emails = new List<Email>();
            for (int i = 0; i < rand.Next(1, 3); i++)
            {
                emails.Add(new Email()
                {
                    EmailDesc = Global.GerarEmail()
                });
            }
            #endregion

            var pessoaFisica = new PessoaFisica()
            {
                CPF = Global.GerarCpf(true),
                Passaporte = "Passaport" + randValue,
                Nome = "Nome" + randValue,
                Apelido = "Apelido" + randValue,
                Email = Global.GerarEmail(),
                DataNascimento = dt.AddYears(rand.Next(-60, -20)),
                DataVisto = dt.AddDays(rand.Next(-365, -1)),
                DataValidadeVisto = dt.AddDays(rand.Next(1, 365)),
                Sexo = Global.ObterEnumAleatorio<Sexo>(),
                NomeMae = "Mae" + randValue,
                NomePai = "Pai" + randValue,
                RNE = "RNE" + randValue,
                DataExpedicaoRNE = dt.AddDays(rand.Next(1, 365)),
                EstadoCivil = Global.ObterEnumAleatorio<EstadoCivil>(),
                Escolaridade = Global.ObterEnumAleatorio<Escolaridade>(),
                TipoSanguineo = Global.ObterEnumAleatorio<TipoSanguineo>(),
                Naturalidade = cidade,

                RG = randValue.ToString(),
                RGOrgaoEmissor = "SSP",
                RGOrgaoEmissorUF = estado,
                RGDataEmissao = dt.AddDays(rand.Next(-730, -30)),

                TituloEleitor = rand.Next(987654, 987654321),
                TituloEleitorSecao = randValue,
                TituloEleitorZona = randValue,
                TituloEleitorCidade = cidade,

                CTPS = randValue,
                CTPSData = dt.AddDays(rand.Next(-999, -365)),
                CTPSSerie = "CTPS" + randValue,
                CTPSEstado = estado,

                CNH = "cnh-" + randValue,
                CNHCategoria = Global.ObterBoolean() ? CategoriaCNH.A : CategoriaCNH.B,
                CNHDataValidade = dt.AddDays(rand.Next(365, 999)),

                CertificadoReservista = "R-" + randValue,
                CertificadoReservistaCat = Global.ObterBoolean() ? "A" : "B",

                Telefones = telefones,
                Emails = emails,
                Endereco = EnderecoTeste.ObterEnderecoNovo()
            };

            var usuarioAux = new Usuario()
                {
                    Login = "Login" + randValue,
                    PessoaFisica = pessoaFisica,
                    AD = true
                };

            var result = controller.Incluir(usuarioAux, new string[] { }, null) as JsonResult;
            Assert.IsNotNull(result);

            var usuario = JsonHelper.GetJsonObjectRepresentation<UsuarioModelView>(result);
            Assert.IsNotNull(usuario);
            Assert.AreEqual(pessoaFisica.CPF, usuario.CPF.Replace(".", "").Replace("-", ""));
            Assert.AreEqual(pessoaFisica.TipoSanguineo, usuario.TipoSanguineo);
            Assert.AreEqual(pessoaFisica.Sexo, usuario.Sexo);
            Assert.AreEqual(pessoaFisica.EstadoCivil, usuario.EstadoCivil);
            //Assert.AreEqual(pessoaFisica.DataExpedicaoRNE, usuario.DataExpedicaoRNE);
            //Assert.AreEqual(pessoaFisica.DataNascimento, usuario.DataNascimento);
            Assert.AreEqual(pessoaFisica.NomePai, usuario.NomePai);
            Assert.AreEqual(pessoaFisica.NomeMae, usuario.NomeMae);
            Assert.AreEqual(pessoaFisica.RG, usuario.RG);
            Assert.AreEqual(pessoaFisica.RGOrgaoEmissor, usuario.RGOrgaoEmissor);
            Assert.AreEqual(pessoaFisica.RGOrgaoEmissorUF.Codigo, usuario.RGOrgaoEmissorUF);
            //Assert.AreEqual(pessoaFisica.RGDataEmissao, usuario.RGDataEmissao);
            Assert.AreEqual(pessoaFisica.Passaporte, usuario.Passaporte);
            Assert.AreEqual(pessoaFisica.Naturalidade.Codigo, usuario.Naturalidade);
            Assert.AreEqual(pessoaFisica.CTPS, usuario.CTPS);
            Assert.AreEqual(pessoaFisica.CTPSSerie, usuario.CTPSSerie);
            Assert.AreEqual(pessoaFisica.CTPSEstado.Codigo, usuario.CTPSEstado);
            Assert.AreEqual(pessoaFisica.CNH, usuario.CNH);
            Assert.AreEqual(pessoaFisica.CNHCategoria, usuario.CNHCategoria);
            Assert.AreEqual(pessoaFisica.TituloEleitor, usuario.TituloEleitor);
            Assert.AreEqual(pessoaFisica.TituloEleitorCidade.Codigo, usuario.TituloEleitorCidade);
            Assert.AreEqual(pessoaFisica.CertificadoReservista, usuario.CertificadoReservista);
            Assert.AreEqual(pessoaFisica.CertificadoReservistaCat, usuario.CertificadoReservistaCat);
            Assert.AreEqual(pessoaFisica.RNE, usuario.RNE);
            Assert.AreEqual(pessoaFisica.Escolaridade, usuario.Escolaridade);

            // TODO: Validar lista de Emails
            // TODO: Validar lista de Papeis
        }

        /// <summary>
        /// Valida alterar um Usuario.
        /// </summary>
        [TestMethod]
        public void Pode_Alterar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new UsuarioController(unidTrabalho);

            var dt = DateTime.Now;
            var rand = new Random();
            var randValue = rand.Next(1, 9999);

            #region [Estado]
            var repEstado = new Repositorio<Estado>(unidTrabalho);
            var estados = repEstado.ObterTodos().ToList();
            Assert.IsTrue(estados.Any());
            estados = estados.OrderBy(x => rand.Next()).ToList();
            var estado = estados[0];
            #endregion
            #region [Cidade]
            var repCidade = new Repositorio<Cidade>(unidTrabalho);
            var cidades = repCidade.ObterTodos().ToList();
            Assert.IsTrue(cidades.Any());
            cidades = cidades.Where(x => x.Estado.Codigo == estado.Codigo).OrderBy(x => rand.Next()).ToList();
            Assert.IsTrue(cidades.Any());
            var cidade = cidades[0];
            #endregion
            #region [Telefones]
            var telefones = new List<Telefone>()
            {
                new Telefone()
                {
                    TipoTelefone = TipoTelefone.Celular,
                    TelefoneNum = Global.GerarTelefone(TipoTelefone.Celular)
                },
                new Telefone()
                {
                    TipoTelefone = TipoTelefone.Emergencial,
                    TelefoneNum = Global.GerarTelefone(TipoTelefone.Emergencial)
                },
                new Telefone()
                {
                    TipoTelefone = TipoTelefone.Celular,
                    TelefoneNum = Global.GerarTelefone(TipoTelefone.Residencial)
                },
                new Telefone()
                {
                    TipoTelefone = TipoTelefone.Empresarial,
                    TelefoneNum = Global.GerarTelefone(TipoTelefone.Empresarial)
                }

            };
            #endregion
            #region [Emails]
            var emails = new List<Email>();
            for (int i = 0; i < rand.Next(1, 3); i++)
            {
                emails.Add(new Email()
                {
                    EmailDesc = Global.GerarEmail()
                });
            }
            #endregion

            var usuarioAux = ObterUsuario(true);
            Assert.IsNotNull(usuarioAux);

            var repPF = new Repositorio<PessoaFisica>(unidTrabalho);
            var pessoaFisica = repPF.ObterTodos().FirstOrDefault(x => x.Codigo == usuarioAux.Codigo);
            Assert.IsNotNull(pessoaFisica);

            pessoaFisica.CPF = Global.GerarCpf(true);
            pessoaFisica.Passaporte = "Passaport" + randValue;
            pessoaFisica.Nome = "Nome" + randValue;
            pessoaFisica.Apelido = "Apelido" + randValue;
            pessoaFisica.Email = Global.GerarEmail();
            pessoaFisica.DataNascimento = dt.AddYears(rand.Next(-60, -20));
            pessoaFisica.DataVisto = dt.AddDays(rand.Next(-365, -1));
            pessoaFisica.DataValidadeVisto = dt.AddDays(rand.Next(1, 365));
            pessoaFisica.Sexo = Global.ObterEnumAleatorio<Sexo>();
            pessoaFisica.NomeMae = "Mae" + randValue;
            pessoaFisica.NomePai = "Pai" + randValue;
            pessoaFisica.RNE = "RNE" + randValue;
            pessoaFisica.DataExpedicaoRNE = dt.AddDays(rand.Next(1, 365));
            pessoaFisica.EstadoCivil = Global.ObterEnumAleatorio<EstadoCivil>();
            pessoaFisica.Escolaridade = Global.ObterEnumAleatorio<Escolaridade>();
            pessoaFisica.TipoSanguineo = Global.ObterEnumAleatorio<TipoSanguineo>();
            pessoaFisica.Naturalidade = cidade;

            pessoaFisica.RG = randValue.ToString();
            pessoaFisica.RGOrgaoEmissor = "SSP";
            pessoaFisica.RGOrgaoEmissorUF = estado;
            pessoaFisica.RGDataEmissao = dt.AddDays(rand.Next(-730, -30));

            pessoaFisica.TituloEleitor = rand.Next(987654, 987654321);
            pessoaFisica.TituloEleitorSecao = randValue;
            pessoaFisica.TituloEleitorZona = randValue;
            pessoaFisica.TituloEleitorCidade = cidade;
            pessoaFisica.CTPS = randValue;
            pessoaFisica.CTPSData = dt.AddDays(rand.Next(-999, -365));
            pessoaFisica.CTPSSerie = "CTPS" + randValue;
            pessoaFisica.CTPSEstado = estado;
            pessoaFisica.CNH = "cnh-" + randValue;
            pessoaFisica.CNHCategoria = Global.ObterBoolean() ? CategoriaCNH.A : CategoriaCNH.B;
            pessoaFisica.CNHDataValidade = dt.AddDays(rand.Next(365, 999));
            pessoaFisica.CertificadoReservista = "R-" + randValue;
            pessoaFisica.CertificadoReservistaCat = Global.ObterBoolean() ? "A" : "B";

            pessoaFisica.Telefones = telefones;
            pessoaFisica.Emails = emails;
            pessoaFisica.Endereco = EnderecoTeste.ObterEnderecoNovo();

            usuarioAux.PessoaFisica = pessoaFisica;

            var result = controller.Alterar(usuarioAux, new string[] { }, null) as JsonResult;
            Assert.IsNotNull(result);

            var usuario = JsonHelper.GetJsonObjectRepresentation<UsuarioModelView>(result);
            Assert.IsNotNull(usuario);
            Assert.AreEqual(pessoaFisica.CPF, usuario.CPF.Replace(".", "").Replace("-", ""));
            Assert.AreEqual(pessoaFisica.TipoSanguineo, usuario.TipoSanguineo);
            Assert.AreEqual(pessoaFisica.Sexo, usuario.Sexo);
            Assert.AreEqual(pessoaFisica.EstadoCivil, usuario.EstadoCivil);
            //Assert.AreEqual(pessoaFisica.DataExpedicaoRNE, usuario.DataExpedicaoRNE);
            //Assert.AreEqual(pessoaFisica.DataNascimento, usuario.DataNascimento);
            Assert.AreEqual(pessoaFisica.NomePai, usuario.NomePai);
            Assert.AreEqual(pessoaFisica.NomeMae, usuario.NomeMae);
            Assert.AreEqual(pessoaFisica.RG, usuario.RG);
            Assert.AreEqual(pessoaFisica.RGOrgaoEmissor, usuario.RGOrgaoEmissor);
            Assert.AreEqual(pessoaFisica.RGOrgaoEmissorUF.Codigo, usuario.RGOrgaoEmissorUF);
            //Assert.AreEqual(pessoaFisica.RGDataEmissao, usuario.RGDataEmissao);
            Assert.AreEqual(pessoaFisica.Passaporte, usuario.Passaporte);
            Assert.AreEqual(pessoaFisica.Naturalidade.Codigo, usuario.Naturalidade);
            Assert.AreEqual(pessoaFisica.CTPS, usuario.CTPS);
            Assert.AreEqual(pessoaFisica.CTPSSerie, usuario.CTPSSerie);
            Assert.AreEqual(pessoaFisica.CTPSEstado.Codigo, usuario.CTPSEstado);
            Assert.AreEqual(pessoaFisica.CNH, usuario.CNH);
            Assert.AreEqual(pessoaFisica.CNHCategoria, usuario.CNHCategoria);
            Assert.AreEqual(pessoaFisica.TituloEleitor, usuario.TituloEleitor);
            Assert.AreEqual(pessoaFisica.TituloEleitorCidade.Codigo, usuario.TituloEleitorCidade);
            Assert.AreEqual(pessoaFisica.CertificadoReservista, usuario.CertificadoReservista);
            Assert.AreEqual(pessoaFisica.CertificadoReservistaCat, usuario.CertificadoReservistaCat);
            Assert.AreEqual(pessoaFisica.RNE, usuario.RNE);
            Assert.AreEqual(pessoaFisica.Escolaridade, usuario.Escolaridade);

            // TODO: Validar lista de Emails
            // TODO: Validar lista de Papeis
        }

        /// <summary>
        /// Valida Ativar Usuario.
        /// </summary>
        [TestMethod]
        public void Pode_Ativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new UsuarioController(unidTrabalho);
            var lista = ObterListaUsuario();
            Assert.IsTrue(lista.Any(), Resources.Lista_vazia);

            lista = lista.Where(x => x.Ativo == false).ToList();
            Assert.IsTrue(lista.Any(), Resources.Lista_vazia);

            var usuarioAux = lista[0];
            Assert.IsFalse(usuarioAux.Ativo);

            var result = controller.AtivarInativar(usuarioAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var usuario = JsonHelper.GetJsonObjectRepresentation<UsuarioModelView>(result);
            Assert.IsNotNull(usuario);
            Assert.IsTrue(usuario.Ativo);
        }

        /// <summary>
        /// Valida Inativar Usuario.
        /// </summary>
        [TestMethod]
        public void Pode_Inativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new UsuarioController(unidTrabalho);
            var usuarioAux = ObterUsuario(true);
            Assert.IsTrue(usuarioAux.Ativo);

            var result = controller.AtivarInativar(usuarioAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var usuario = JsonHelper.GetJsonObjectRepresentation<UsuarioModelView>(result);
            Assert.IsNotNull(usuario);
            Assert.AreEqual(usuarioAux.Codigo, usuario.Codigo);
            Assert.IsFalse(usuario.Ativo);
        }

        /// <summary>
        /// Busca aleatóriamente um usuario cadastrado na base de dados.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// Filtrar usuarios ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Usuario.
        /// </returns>
        public static Usuario ObterUsuario(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<Usuario>(unidTrabalho);
            var usuarios = repositorio.ObterTodos().ToList();
            Assert.IsTrue(usuarios.Any());

            var rand = new Random();
            usuarios = usuarios.OrderBy(x => rand.Next()).ToList();

            Usuario usuario = null;
            foreach (var item in usuarios)
            {
                usuario = item;

                if (usuario == null)
                    continue;
                if (somenteAtivos)
                {
                    if (usuario.Ativo)
                        break;
                    usuario = null;
                }
                else
                    break;
            }

            return usuario;
        }

        /// <summary>
        /// Obter lista usuario.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// Filtrar usuarios ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;Usuario&gt;
        /// </returns>
        public static List<Usuario> ObterListaUsuario(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<Usuario>(unidTrabalho);
            var usuarios = repositorio.ObterTodos().ToList();
            Assert.IsTrue(usuarios.Any());

            var rand = new Random();
            usuarios = usuarios.OrderBy(x => rand.Next()).ToList();

            return somenteAtivos ? usuarios.Where(x => x.Ativo).ToList() : usuarios;
        }
    }
}
