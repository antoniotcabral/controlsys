﻿using System;
using System.Linq;
using System.Web.Mvc;
using Controlsys.Dominio.Parametros;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Repositorio.Parametros;
using FluentNHibernate.Conventions;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks.Constraints;
using System.Collections.Generic;

namespace Controlsys.Interfaces.Tests.Controller
{
    [TestClass]
    public class TipoAlimentacaoTeste
    {
        #region [modelo]
        //[TestMethod]
        //public void Incluir()
        //{

        //}

        //[TestMethod]
        //public void Alterar()
        //{

        //}

        //[TestMethod]
        //public void AtivarInativar()
        //{

        //}

        //[TestMethod]
        //public void Selecionar()
        //{

        //}

        //[TestMethod]
        //public void ObterTodos()
        //{

        //} 
        #endregion

        [TestMethod]
        public void Incluir()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();

            TipoAlimentacaoController controller = new TipoAlimentacaoController(unidTrabalho);
            TipoAlimentacao registro = new TipoAlimentacao
            {
                Nome = "Teste"
            };
            var result = controller.Incluir(registro) as JsonResult;
            var registroGerado = JsonHelper.GetJsonObjectRepresentation<TipoAlimentacao>(result);

            Assert.IsNotNull(registro);
            Assert.AreEqual(registro.Nome, registroGerado.Nome);

            unidTrabalho.Rollback();
        }

        [TestMethod]
        public void Alterar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();

            TipoAlimentacaoController controller = new TipoAlimentacaoController(unidTrabalho);

            #region [ObterTodos]
            var result1 = controller.ObterTodos() as JsonResult;
            var lista = JsonHelper.GetJsonObjectRepresentation<List<TipoAlimentacao>>(result1);

            Assert.IsNotNull(lista);
            Assert.IsTrue(lista.Count > 0);
            #endregion

            var registro = lista.Where(x => x.Ativo).OrderBy(x => new Random().Next()).FirstOrDefault();

            Assert.IsNotNull(registro, "Nenhum registro encontrado para ser utilizado como referência de teste no Alterar");

            registro.Nome = new Random().Next().ToString();

            var result = controller.Alterar(registro) as JsonResult;
            var registroAlterado = JsonHelper.GetJsonObjectRepresentation<TipoAlimentacao>(result);

            Assert.IsNotNull(registro);
            Assert.AreEqual(registro.Nome, registroAlterado.Nome);

            unidTrabalho.Rollback();
        }

        [TestMethod]
        public void AtivarInativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();

            TipoAlimentacaoController controller = new TipoAlimentacaoController(unidTrabalho);

            #region [ObterTodos]
            var result1 = controller.ObterTodos() as JsonResult;
            var lista = JsonHelper.GetJsonObjectRepresentation<List<TipoAlimentacao>>(result1);

            Assert.IsNotNull(lista);
            Assert.IsTrue(lista.Count > 0);
            #endregion

            var registro = lista.OrderBy(x => new Random().Next()).FirstOrDefault();

            Assert.IsNotNull(registro, "Nenhum registro encontrado para ser utilizado como referência de teste no AtivarInativar");

            var result = controller.AtivarInativar(registro.Codigo) as JsonResult;
            var registroAlterado = JsonHelper.GetJsonObjectRepresentation<TipoAlimentacao>(result);

            Assert.IsNotNull(registro, "Retornando registro nulo ao AtivarInativar");
            Assert.AreNotEqual(registro.Ativo, registroAlterado.Ativo, "Valor diferente do esperado.");

            unidTrabalho.Rollback();
        }

        [TestMethod]
        public void Selecionar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            TipoAlimentacaoController controller = new TipoAlimentacaoController(unidTrabalho);

            #region [ObterTodos]
            var result1 = controller.ObterTodos() as JsonResult;
            var lista = JsonHelper.GetJsonObjectRepresentation<List<TipoAlimentacao>>(result1);

            Assert.IsNotNull(lista);
            Assert.IsTrue(lista.Count > 0);
            #endregion

            var registro = lista.OrderBy(x => new Random().Next()).FirstOrDefault();

            Assert.IsNotNull(registro, "Nenhum registro encontrado para ser utilizado como referência de teste no Selecionar");

            var result2 = controller.Selecionar(registro.Codigo) as JsonResult;
            var registroSelecionado = JsonHelper.GetJsonObjectRepresentation<TipoAlimentacao>(result2);

            Assert.IsNotNull(registroSelecionado, "Selecionar retornando nulo");
            Assert.AreEqual(registro.Nome, registroSelecionado.Nome, "Nome difere do valor utilizado como referência");
        }

        [TestMethod]
        public void ObterTodos()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();

            TipoAlimentacaoController controller = new TipoAlimentacaoController(unidTrabalho);
            var result = controller.ObterTodos() as JsonResult;
            var lista = JsonHelper.GetJsonObjectRepresentation<List<TipoAlimentacao>>(result);

            Assert.IsNotNull(lista);
            Assert.IsTrue(lista.Count > 0);
        }
    }
}
