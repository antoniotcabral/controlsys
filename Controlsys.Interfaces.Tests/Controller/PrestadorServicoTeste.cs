﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Controlsys.Dominio.Enderecos;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Controlsys.Persistencia;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) PrestadorServicoTeste.
    /// </summary>
    [TestClass]
    public class PrestadorServicoTeste
    {
        /// <summary>
        /// Valida incluir um novo PrestadorServico.
        /// </summary>
        [TestMethod]
        public void Pode_Incluir()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new PrestadorServicoController(unidTrabalho);

            var dt = DateTime.Now;
            var rand = new Random();
            var randValue = rand.Next(1, 9999);

            #region [PessoaFisica]
            #region [Estado]
            var repEstado = new Repositorio<Estado>(unidTrabalho);
            var estados = repEstado.ObterTodos().ToList();
            Assert.IsTrue(estados.Any());
            estados = estados.OrderBy(x => rand.Next()).ToList();
            var estado = estados[0];
            #endregion
            #region [Cidade]
            var repCidade = new Repositorio<Cidade>(unidTrabalho);
            var cidades = repCidade.ObterTodos().ToList();
            Assert.IsTrue(cidades.Any());
            cidades = cidades.Where(x => x.Estado.Codigo == estado.Codigo).OrderBy(x => rand.Next()).ToList();
            Assert.IsTrue(cidades.Any());
            var cidade = cidades[0];
            #endregion
            #region [Telefones]
            var telefones = new List<Telefone>()
            {
                new Telefone()
                {
                    TipoTelefone = TipoTelefone.Residencial,
                    TelefoneNum = Global.GerarTelefone(TipoTelefone.Residencial)
                },
                new Telefone()
                {
                    TipoTelefone = TipoTelefone.Emergencial,
                    TelefoneNum = Global.GerarTelefone(TipoTelefone.Emergencial)
                },
                new Telefone()
                {
                    TipoTelefone = TipoTelefone.Celular,
                    TelefoneNum = Global.GerarTelefone(TipoTelefone.Celular)
                }

            };
            #endregion
            #region [Emails]
            var emails = new List<Email>();
            for (int i = 0; i < rand.Next(1, 3); i++)
            {
                emails.Add(new Email()
                {
                    EmailDesc = Global.GerarEmail()
                });
            }
            #endregion
            #region [Treinamento]
            var countAux = rand.Next(1, 3);
            var listaTreinamento = new List<TreinamentoPessoa>();
            for (int i = 0; i < countAux; i++)
            {
                var treinamento = TreinamentoTeste.ObterTreinamento(true);
                if (listaTreinamento.Exists(x => x.Treinamento == treinamento))
                {
                    countAux++;
                    continue;
                }
                var dtRealizacao = dt.AddDays(rand.Next(-365, -1));
                listaTreinamento.Add(new TreinamentoPessoa()
                {
                    Treinamento = treinamento,
                    DataRealizacao = dtRealizacao,
                    DataValidade = dtRealizacao.AddYears(1)
                });
            }
            #endregion
            #region [Documentações]
            var repCheckList = new Repositorio<ChecklistDocumento>(unidTrabalho);
            var listaCheckListDocumento = repCheckList.ObterTodos().ToList();
            listaCheckListDocumento = listaCheckListDocumento.Where(x => x.TipoCadastro == TipoCadastro.PrestadorServicoEmergencial).ToList();
            Assert.IsTrue(listaCheckListDocumento.Any());

            var listaDocumento = new List<DocumentoMidiaModelView>();
            var listaTipoDocumento = new List<TipoDocumento>();
            foreach (var checkListDocumento in listaCheckListDocumento)
            {
                foreach (var checkListTipoDocumento in checkListDocumento.TiposDocumento)
                {
                    var tipoDocumento = checkListTipoDocumento.TipoDocumento;
                    if (listaTipoDocumento.Contains(tipoDocumento))
                        continue;
                    listaTipoDocumento.Add(tipoDocumento);
                    listaDocumento.Add(new DocumentoMidiaModelView()
                    {
                        Documento = new Documento
                        {
                            TipoDocumento = tipoDocumento,
                            Descricao = "Descrição-" + randValue,
                            Status = Global.ObterEnumAleatorio<StatusDocumento>()
                        },
                        Midias = null
                    });
                }
            }
            Assert.IsTrue(listaDocumento.Any());
            #endregion

            var pessoaFisica = new PessoaFisica()
            {
                CPF = Global.GerarCpf(true),
                Passaporte = "Passaport" + randValue,
                Nome = "Nome" + randValue,
                Apelido = "Apelido" + randValue,
                Email = Global.GerarEmail(),
                DataNascimento = dt.AddDays(rand.Next(-21900, -7300)),
                DataVisto = dt.AddDays(rand.Next(-365, -1)),
                DataValidadeVisto = dt.AddDays(rand.Next(1, 365)),
                Sexo = Global.ObterEnumAleatorio<Sexo>(),

                NomeMae = "Mae" + randValue,
                NomePai = "Pai" + randValue,
                RNE = "RNE" + randValue,
                DataExpedicaoRNE = dt.AddDays(rand.Next(1, 365)),
                EstadoCivil = Global.ObterEnumAleatorio<EstadoCivil>(),
                Escolaridade = Global.ObterEnumAleatorio<Escolaridade>(),
                TipoSanguineo = Global.ObterEnumAleatorio<TipoSanguineo>(),
                Nacionalidade = "Brasileiro",
                Naturalidade = cidade,

                RG = randValue.ToString(),
                RGOrgaoEmissor = "SSP",
                RGOrgaoEmissorUF = estado,
                RGDataEmissao = dt.AddDays(rand.Next(-5475, -365)),

                TituloEleitor = rand.Next(100, 999999),
                TituloEleitorSecao = randValue,
                TituloEleitorZona = randValue,
                TituloEleitorCidade = cidade,

                CTPS = randValue,
                CTPSData = dt.AddDays(rand.Next(-999, -365)),
                CTPSSerie = "CTPS" + randValue,
                CTPSEstado = estado,

                CNH = "cnh-" + randValue,
                CNHCategoria = Global.ObterBoolean() ? CategoriaCNH.A : CategoriaCNH.B,
                CNHDataValidade = dt.AddDays(rand.Next(365, 999)),

                CertificadoReservista = randValue.ToString(),
                CertificadoReservistaCat = "CAT-" + randValue,

                Endereco = EnderecoTeste.ObterEnderecoNovo(),
                //Documentos = listaDocumento,
                Telefones = telefones,
                Emails = emails
            };
            #endregion

            #region [PrestadorServico]
            #region [ASO]
            var listaASO = new List<ASO>();
            for (int i = 1; i <= rand.Next(1, 3); i++)
            {
                var data = dt.AddYears(-(i * 2));
                listaASO.Add(new ASO()
                {
                    DataRealizacao = data,
                    DataVencimento = data.AddYears(1)
                });
            }
            #endregion
            var dtVisita = dt.AddDays(rand.Next(-365, -1));
            var prestadorServicoAux = new PrestadorServico()
            {
                TipoPrestador = Global.ObterEnumAleatorio<TipoPrestador>(),
                Solicitante = ColaboradorTeste.ObterColaborador(),
                AntiPassBack = Global.ObterBoolean(),
                PessoaJuridica = Global.ObterBoolean(),
                Observacao = "Observação-" + randValue,
                DataValidade = dtVisita.AddDays(rand.Next(1, 7)),
                PessoaFisica = pessoaFisica
            };
            switch (prestadorServicoAux.TipoPrestador)
            {
                case TipoPrestador.Emergencial:
                    prestadorServicoAux.Fornecedor = FornecedorTeste.ObterFornecedor(true);
                    break;

                case TipoPrestador.Temporario:
                    prestadorServicoAux.Empresa = EmpresaTeste.ObterEmpresa(true);
                    prestadorServicoAux.DataAdmissao = dtVisita;
                    prestadorServicoAux.ASOs = listaASO;
                    break;

                default:
                    break;
            }
            #endregion

            //TODO: Fazer upload da foto

            var result = controller.Incluir(prestadorServicoAux, null, listaTreinamento, listaDocumento, null, null) as JsonResult;
            Assert.IsNotNull(result);

            var prestadorServico = result.Data as PrestadorServicoModelView;
            Assert.IsNotNull(prestadorServico);

            // Validando Pessoa
            Assert.AreEqual(pessoaFisica.CPF, prestadorServico.CPF.Replace(".", "").Replace("-", ""));
            Assert.AreEqual(pessoaFisica.TipoSanguineo, prestadorServico.TipoSanguineo);
            Assert.AreEqual(pessoaFisica.Sexo.ToString(), prestadorServico.Sexo);
            Assert.AreEqual(pessoaFisica.EstadoCivil, prestadorServico.EstadoCivil);
            //Assert.AreEqual(pessoaFisica.DataExpedicaoRNE, prestadorServico.DataExpedicaoRNE);
            //Assert.AreEqual(pessoaFisica.DataNascimento, prestadorServico.DataNascimento);
            Assert.AreEqual(pessoaFisica.NomePai, prestadorServico.NomePai);
            Assert.AreEqual(pessoaFisica.NomeMae, prestadorServico.NomeMae);
            Assert.AreEqual(pessoaFisica.RG, prestadorServico.RG);
            Assert.AreEqual(pessoaFisica.RGOrgaoEmissor, prestadorServico.RGOrgaoEmissor);
            Assert.AreEqual(pessoaFisica.RGOrgaoEmissorUF.Codigo, prestadorServico.RGOrgaoEmissorUF);
            //Assert.AreEqual(pessoaFisica.RGDataEmissao, prestadorServico.RGDataEmissao);
            Assert.AreEqual(pessoaFisica.Passaporte, prestadorServico.Passaporte);
            Assert.AreEqual(pessoaFisica.Naturalidade.Codigo, prestadorServico.Naturalidade);
            Assert.AreEqual(pessoaFisica.CTPS, prestadorServico.CTPS);
            //Assert.AreEqual(pessoaFisica.CTPSSerie, prestadorServico.CTPSSerie);
            Assert.AreEqual(pessoaFisica.CTPSEstado.Codigo, prestadorServico.CTPSEstado);
            Assert.AreEqual(pessoaFisica.CNH, prestadorServico.CNH);
            Assert.AreEqual(pessoaFisica.CNHCategoria, prestadorServico.CNHCategoria);
            Assert.AreEqual(pessoaFisica.TituloEleitor, prestadorServico.TituloEleitor);
            Assert.AreEqual(pessoaFisica.TituloEleitorCidade.Codigo, prestadorServico.TituloEleitorCidade);
            Assert.AreEqual(pessoaFisica.CertificadoReservista, prestadorServico.CertificadoReservista);
            Assert.AreEqual(pessoaFisica.CertificadoReservistaCat, prestadorServico.CertificadoReservistaCat);
            Assert.AreEqual(pessoaFisica.RNE, prestadorServico.RNE);
            Assert.AreEqual(pessoaFisica.Escolaridade, prestadorServico.Escolaridade);

            // Validando PrestadorServico
            Assert.AreEqual(prestadorServicoAux.TipoPrestador.ObterDescricaoEnum(), prestadorServico.TipoPrestador);
            Assert.AreEqual(prestadorServicoAux.Solicitante.Codigo, prestadorServico.Solicitante.Codigo);
            Assert.AreEqual(prestadorServicoAux.AntiPassBack, prestadorServico.AntiPassBack);
            Assert.AreEqual(prestadorServicoAux.PessoaJuridica, prestadorServico.PessoaJuridica);
            Assert.AreEqual(prestadorServicoAux.Observacao, prestadorServico.Observacao);
            Assert.AreEqual(prestadorServicoAux.DataValidade, prestadorServico.DataValidade);

            // TODO: Validar lista ASOs
            // TODO: Validar lista de Emails
            // TODO: Validar lista de Papeis
        }

        /// <summary>
        /// Valida alterar um PrestadorServico.
        /// </summary>
        [TestMethod]
        public void Pode_Alterar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new PrestadorServicoController(unidTrabalho);

            var dt = DateTime.Now;
            var rand = new Random();
            var randValue = rand.Next(1, 9999);

            #region [Estado]
            var repEstado = new Repositorio<Estado>(unidTrabalho);
            var estados = repEstado.ObterTodos().ToList();
            Assert.IsTrue(estados.Any());
            estados = estados.OrderBy(x => rand.Next()).ToList();
            var estado = estados[0];
            #endregion
            #region [Cidade]
            var repCidade = new Repositorio<Cidade>(unidTrabalho);
            var cidades = repCidade.ObterTodos().ToList();
            Assert.IsTrue(cidades.Any());
            cidades = cidades.Where(x => x.Estado.Codigo == estado.Codigo).OrderBy(x => rand.Next()).ToList();
            Assert.IsTrue(cidades.Any());
            var cidade = cidades[0];
            #endregion
            #region [Telefones]
            var telefones = new List<Telefone>()
            {
                new Telefone()
                {
                    TipoTelefone = TipoTelefone.Emergencial,
                    TelefoneNum = Global.GerarTelefone(TipoTelefone.Emergencial)
                },
                new Telefone()
                {
                    TipoTelefone = TipoTelefone.Residencial,
                    TelefoneNum = Global.GerarTelefone(TipoTelefone.Residencial)
                },
                new Telefone()
                {
                    TipoTelefone = TipoTelefone.Celular,
                    TelefoneNum = Global.GerarTelefone(TipoTelefone.Celular)
                }

            };
            #endregion
            #region [Emails]
            var emails = new List<Email>();
            for (int i = 0; i < rand.Next(1, 3); i++)
            {
                emails.Add(new Email()
                {
                    EmailDesc = Global.GerarEmail()
                });
            }
            #endregion

            #region [Treinamento]
            var countAux = rand.Next(1, 3);
            var listaTreinamento = new List<TreinamentoPessoa>();
            for (int i = 0; i < countAux; i++)
            {
                var treinamento = TreinamentoTeste.ObterTreinamento(true);
                if (listaTreinamento.Exists(x => x.Treinamento == treinamento))
                {
                    countAux++;
                    continue;
                }
                var dtRealizacao = dt.AddDays(rand.Next(-365, -1));
                listaTreinamento.Add(new TreinamentoPessoa()
                {
                    Treinamento = treinamento,
                    DataRealizacao = dtRealizacao,
                    DataValidade = dtRealizacao.AddYears(1)
                });
            }
            #endregion
            #region [Documentações]
            var repCheckList = new Repositorio<ChecklistDocumento>(unidTrabalho);
            var listaCheckListDocumento = repCheckList.ObterTodos().ToList();
            listaCheckListDocumento = listaCheckListDocumento.Where(x => x.TipoCadastro == TipoCadastro.PrestadorServicoTemp).ToList();
            Assert.IsTrue(listaCheckListDocumento.Any());

            var listaDocumento = new List<DocumentoMidiaModelView>();
            var listaTipoDocumento = new List<TipoDocumento>();
            foreach (var checkListDocumento in listaCheckListDocumento)
            {
                foreach (var checkListTipoDocumento in checkListDocumento.TiposDocumento)
                {
                    var tipoDocumento = checkListTipoDocumento.TipoDocumento;
                    if (listaTipoDocumento.Contains(tipoDocumento))
                        continue;
                    listaTipoDocumento.Add(tipoDocumento);
                    listaDocumento.Add(new DocumentoMidiaModelView()
                    {
                        Documento = new Documento
                        {
                            TipoDocumento = tipoDocumento,
                            Descricao = "Descrição-" + randValue,
                            Status = Global.ObterEnumAleatorio<StatusDocumento>()
                        },
                        Midias = null
                    });
                }
            }
            Assert.IsTrue(listaDocumento.Any());
            #endregion

            #region [ASO]
            var listaASO = new List<ASO>();
            for (int i = 1; i <= rand.Next(1, 3); i++)
            {
                var data = dt.AddYears(-(i * 2));
                listaASO.Add(new ASO()
                {
                    DataRealizacao = data,
                    DataVencimento = data.AddYears(1)
                });
            }
            #endregion
            #region [PrestadorServico]
            var prestadorServicoAux = ObterPrestadorServico();
            Assert.IsNotNull(prestadorServicoAux);
            var dtVisita = dt.AddDays(rand.Next(-365, -1));
            prestadorServicoAux.TipoPrestador = Global.ObterEnumAleatorio<TipoPrestador>();
            prestadorServicoAux.Solicitante = ColaboradorTeste.ObterColaborador();
            prestadorServicoAux.AntiPassBack = Global.ObterBoolean();
            prestadorServicoAux.PessoaJuridica = Global.ObterBoolean();
            prestadorServicoAux.Observacao = "Observação-" + randValue;
            prestadorServicoAux.DataValidade = dtVisita.AddDays(rand.Next(1, 7));
            string[] alteracoes = null;
            switch (prestadorServicoAux.TipoPrestador)
            {
                case TipoPrestador.Emergencial:
                    prestadorServicoAux.Fornecedor = FornecedorTeste.ObterFornecedor(true);
                    alteracoes = new string[] { "TipoPrestador", "Solicitante", "AntiPassBack", "PessoaJuridica", "Observacao", "DataValidade", "Fornecedor" };
                    break;

                case TipoPrestador.Temporario:
                    prestadorServicoAux.Empresa = EmpresaTeste.ObterEmpresa(true);
                    prestadorServicoAux.DataAdmissao = dtVisita;
                    prestadorServicoAux.ASOs = listaASO;
                    alteracoes = new string[] { "TipoPrestador", "Solicitante", "AntiPassBack", "PessoaJuridica", "Observacao", "DataValidade", "Empresa", "DataAdmissao", "ASOs" };
                    break;

                default:
                    alteracoes = new string[] { "TipoPrestador", "Solicitante", "AntiPassBack", "PessoaJuridica", "Observacao", "DataValidade" };
                    break;
            }
            #endregion


            var result = controller.Alterar(prestadorServicoAux,
                null, alteracoes, listaDocumento,
                prestadorServicoAux.Treinamentos.ToArray(), listaTreinamento.ToArray(),
                prestadorServicoAux.ASOs.ToArray(), listaASO.ToArray(), null) as JsonResult;
            Assert.IsNotNull(result);

            var prestadorServico = result.Data as PrestadorServicoModelView;
            Assert.IsNotNull(prestadorServico);
            Assert.AreEqual(prestadorServicoAux.TipoPrestador.ObterDescricaoEnum(), prestadorServico.TipoPrestador);
            Assert.AreEqual(prestadorServicoAux.Solicitante.Codigo, prestadorServico.Solicitante.Codigo);
            Assert.AreEqual(prestadorServicoAux.AntiPassBack, prestadorServico.AntiPassBack);
            Assert.AreEqual(prestadorServicoAux.PessoaJuridica, prestadorServico.PessoaJuridica);
            Assert.AreEqual(prestadorServicoAux.Observacao, prestadorServico.Observacao);
            Assert.AreEqual(prestadorServicoAux.DataValidade, prestadorServico.DataValidade);

            // TODO: Validar lista de ASOs
            // TODO: Validar lista de Trainamentos
            // TODO: Validar lista de Emails
            // TODO: Validar lista de Papeis
        }

        /// <summary>
        /// Busca aleatóriamente um prestadorServico cadastrado na base de dados.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) PrestadorServico.
        /// </returns>
        public static PrestadorServico ObterPrestadorServico()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<PrestadorServico>(unidTrabalho);
            var listaPrestadorServico = repositorio.ObterTodos().ToList();
            Assert.IsTrue(listaPrestadorServico.Any());

            var rand = new Random();
            listaPrestadorServico = listaPrestadorServico.OrderBy(x => rand.Next()).ToList();
            return listaPrestadorServico.First();
        }

        /// <summary>
        /// Obter lista prestador servico.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) List&lt;PrestadorServico&gt;
        /// </returns>
        public static List<PrestadorServico> ObterListaPrestadorServico()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<PrestadorServico>(unidTrabalho);
            var listaPrestadorServico = repositorio.ObterTodos().ToList();
            Assert.IsTrue(listaPrestadorServico.Any());

            var rand = new Random();
            listaPrestadorServico = listaPrestadorServico.OrderBy(x => rand.Next()).ToList();
            return listaPrestadorServico;
        }
    }
}
