﻿using System;
using System.Data.SqlTypes;
using System.Web.Mvc;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Tests.App_Start;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Controlsys.Interfaces.Tests.Controller
{
    [TestClass]
    public class GraficoAcessosHoraTeste
    {
        [TestMethod]
        public void Exportar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new GraficoAcessosHoraController(unidTrabalho);
            Mock.ServerMapPath(controller, "GraficoAcessosHora.rdlc");
            
            var result = controller.Exportar("PDF", new DateTime(2015, 2, 2), 0, 23) as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/pdf", result.ContentType);
            Assert.AreEqual("download.pdf", result.FileDownloadName);
        }
    }
}
