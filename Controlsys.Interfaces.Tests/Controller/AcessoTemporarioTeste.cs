﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Controlsys.Infra;
using Controlsys.Dominio.Pessoas;
using Globalsys;
using Controlsys.Persistencia;
using Controlsys.Dominio.Acesso;
using System.Linq;

namespace Controlsys.Interfaces.Tests.Controller
{
    [TestClass]
    public class AcessoTemporarioTeste
    {

        #region Testes específicos


        [TestMethod]
        public void AcessoTemporarioNoGrupoLeitora()
        {
            bool acessoTempEsperado = true;
            string cpf = "28293961899";
            string nomeGrupoLeitora = "ACESSO_PORTO";

            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repVisitante = new Repositorio<Visitante>(unidTrabalho);
            var listaVisitante = repVisitante.ObterTodos().Where(v => v.PessoaFisica.CPF == cpf).ToList();
            var visitante = listaVisitante.Where(v => v.ObterLogPapel().Status == StatusPapelLog.Ativo).FirstOrDefault();

            var repAcessoTemp = new Repositorio<AcessoTemporario>(unidTrabalho);
            var temAcessoTemp = repAcessoTemp.ObterTodos().Any(at => at.Ativo == true && at.Papel.Codigo == visitante.Codigo);

            if(acessoTempEsperado) Assert.IsTrue(temAcessoTemp);
            else Assert.IsFalse(temAcessoTemp);
        }


        #endregion
    }
}
