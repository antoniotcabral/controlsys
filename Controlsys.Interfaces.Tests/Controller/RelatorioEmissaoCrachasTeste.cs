﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Controlsys.Infra;
using Globalsys;
using Controlsys.Interfaces.Controllers;
using Ext.Net;
using Controlsys.Dominio.Relatorio;
using System.Collections.Generic;
using Ext.Net.MVC;
using System.Web.Mvc;
using System.Linq;

namespace Controlsys.Interfaces.Tests.Controller
{
    [TestClass]
    public class RelatorioEmissaoCrachasTeste
    {
        [TestMethod]
        public void Pode_Pesquisar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RelatorioEmissaoCrachasController controller = new RelatorioEmissaoCrachasController(unidTrabalho);

            var x = new Dictionary<string, object>() { };
            x.Add("start", (int)0);
            x.Add("limit", (int)25);


            var result = controller.Pesquisar(new StoreRequestParameters(x), new DateTime(2015, 03, 03), new DateTime(2015, 03, 23)) as StoreResult;
            Assert.IsNotNull(result);

            var emissaoCracha = result.Data as List<RelatorioEmissaoCrachas>;
            Assert.IsNotNull(emissaoCracha);

        }

        [TestMethod]
        public void Pode_ExportarPDF()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RelatorioEmissaoCrachasController controller = new RelatorioEmissaoCrachasController(unidTrabalho);

            List<string> selectedFields = typeof(RelatorioEmissaoCrachas).GetProperties().Select(pi => pi.Name).ToList();

            var result = controller.Exportar(selectedFields, "PDF", new DateTime(2015, 03, 03), new DateTime(2015, 03, 23)) as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/pdf", result.ContentType);
            Assert.AreEqual("download.pdf", result.FileDownloadName);
        }

        [TestMethod]
        public void Pode_ExportarExcel()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RelatorioEmissaoCrachasController controller = new RelatorioEmissaoCrachasController(unidTrabalho);

            List<string> selectedFields = typeof(RelatorioEmissaoCrachas).GetProperties().Select(pi => pi.Name).ToList();


            var result = controller.Exportar(selectedFields, "Excel", new DateTime(2015, 03, 03), new DateTime(2015, 03, 23)) as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/vnd.ms-excel", result.ContentType);
            Assert.AreEqual("download.xls", result.FileDownloadName);
        }

        [TestMethod]
        public void Pode_ExportarWord()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RelatorioEmissaoCrachasController controller = new RelatorioEmissaoCrachasController(unidTrabalho);

            List<string> selectedFields = typeof(RelatorioEmissaoCrachas).GetProperties().Select(pi => pi.Name).ToList();


            var result = controller.Exportar(selectedFields, "Word", new DateTime(2015, 03, 03), new DateTime(2015, 03, 23)) as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/msword", result.ContentType);
            Assert.AreEqual("download.doc", result.FileDownloadName);
        }
    }
}
