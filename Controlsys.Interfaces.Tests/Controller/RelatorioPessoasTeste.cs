﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Pessoas;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) RelatorioPessoasTeste.
    /// </summary>
    [TestClass]
    public class RelatorioPessoasTeste
    {
        /// <summary>
        /// Pode exportar PDF.
        /// </summary>
        [TestMethod]
        public void Pode_ExportarPDF()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RelatorioPessoasController controller = new RelatorioPessoasController(unidTrabalho);

            List<string> selectedFields = typeof(RelatorioPessoaModelView).GetProperties().Select(pi => pi.Name).ToList();

            PessoaFisica pessoa = PessoaTeste.ObterListaPessoasFisica(true).FirstOrDefault();
            Assert.IsNotNull(pessoa);

            var result = controller.Exportar(null, selectedFields, null, "PDF", pessoa.Nome) as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/pdf", result.ContentType);
            Assert.AreEqual("download", result.FileDownloadName);
        }

        /// <summary>
        /// Pode exportar excel.
        /// </summary>
        [TestMethod]
        public void Pode_ExportarExcel()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RelatorioPessoasController controller = new RelatorioPessoasController(unidTrabalho);

            List<string> selectedFields = typeof(RelatorioPessoaModelView).GetProperties().Select(pi => pi.Name).ToList();

            PessoaFisica pessoa = PessoaTeste.ObterListaPessoasFisica(true).FirstOrDefault();
            Assert.IsNotNull(pessoa);

            var result = controller.Exportar(null, selectedFields, null, "Excel", pessoa.Nome) as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/vnd.ms-excel", result.ContentType);
            Assert.AreEqual("download", result.FileDownloadName);
        }

        /// <summary>
        /// Pode exportar word.
        /// </summary>
        [TestMethod]
        public void Pode_ExportarWord()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RelatorioPessoasController controller = new RelatorioPessoasController(unidTrabalho);

            List<string> selectedFields = typeof(RelatorioPessoaModelView).GetProperties().Select(pi => pi.Name).ToList();

            PessoaFisica pessoa = PessoaTeste.ObterListaPessoasFisica(true).FirstOrDefault();
            Assert.IsNotNull(pessoa);

            var result = controller.Exportar(null, selectedFields, null, "Word", pessoa.Nome) as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/msword", result.ContentType);
            Assert.AreEqual("download", result.FileDownloadName);
        }
    }
}
