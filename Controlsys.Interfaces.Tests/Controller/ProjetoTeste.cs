﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web.Mvc;
using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Seguranca;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Controlsys.Interfaces.Tests.Properties;
using Globalsys;
using Globalsys.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Controlsys.Persistencia;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) ProjetoTeste.
    /// </summary>
    [TestClass]
    public class ProjetoTeste
    {
        /// <summary>
        /// Valida incluir um novo Projeto, informando lista de Area, GrupoTrabalho e TreinamentoProjeto.
        /// </summary>
        [TestMethod]
        public void Pode_Incluir()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new ProjetoController(unidTrabalho);

            var rand = new Random();
            var dt = DateTime.Now;
            var data = dt.ToString("MM/dd HH:mm:ss.fff");

            var projetoAux = new Projeto();
            projetoAux.Nome = "Projeto " + data;
            projetoAux.Descricao = "Descricao-" + data;
            projetoAux.DataInicio = dt.AddDays(rand.Next(1, 365));
            projetoAux.DataFim = dt.AddDays(rand.Next(366, 730));

            #region [Area]
            var area = AreaTeste.ObterArea(true);
            Assert.IsNotNull(area);
            #endregion

            #region [GrupoTrabalho]
            var grupoTrabalho = GrupoTrabalhoTeste.ObterGrupoTrabalho(true);
            Assert.IsNotNull(grupoTrabalho);
            #endregion

            #region [TreinamentoProjeto
            var treinaProjeto = new TreinamentoProjeto();
            treinaProjeto.Obrigatorio = Global.ObterBoolean();
            treinaProjeto.ImpressoCracha = Global.ObterBoolean();
            treinaProjeto.Observacao = "Observação-" + data;
            treinaProjeto.Treinamento = TreinamentoTeste.ObterTreinamento(true);
            treinaProjeto.Projeto = projetoAux;
            #endregion

            var result = controller.Incluir(
                projetoAux,
                new List<Area>() { area },
                new List<GrupoTrabalho>() { grupoTrabalho },
                new List<TreinamentoProjeto>() { treinaProjeto }
            ) as JsonResult;
            Assert.IsNotNull(result);

            var projeto = result.Data as ProjetoModelView;
            Assert.IsNotNull(projeto);
            Assert.AreEqual(projetoAux.Nome, projeto.Nome);
            Assert.AreEqual(projetoAux.Descricao, projeto.Descricao);
            Assert.AreEqual(projetoAux.DataInicio, projeto.DataInicio);
            Assert.AreEqual(projetoAux.DataFim, projeto.DataFim);
        }

        /// <summary>
        /// Valida alterar um Projeto, modificando nome, descricao, data Inicio e Fim, além de adicionar
        /// Area, GrupoTrabalho, e TreinamentoProjeto, que já fora cadastrado na base de dados;
        /// </summary>
        [TestMethod]
        public void Pode_Alterar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new ProjetoController(unidTrabalho);

            #region [Projeto]
            var rand = new Random();
            var dt = DateTime.Now;
            var data = dt.ToString("MM/dd HH:mm:ss.fff");

            var projetoAux = ProjetoTeste.ObterProjeto(true);
            projetoAux.Nome = "Projeto " + data;
            projetoAux.Descricao = "Descricao-" + data;
            projetoAux.DataInicio = dt.AddDays(rand.Next(1, 365));
            projetoAux.DataFim = dt.AddDays(rand.Next(366, 730));
            #endregion

            #region [Area]
            var repArea = new Repositorio<AreaProjeto>(unidTrabalho);
            var areasProjeto = repArea.ObterTodos();
            Assert.IsTrue(areasProjeto.Any());
            var areasDel = areasProjeto.Where(x => x.Projeto.Codigo == projetoAux.Codigo).Select(x => x.Area.Codigo).ToArray();
            var areaNew = AreaTeste.ObterArea(true);
            #endregion

            #region [GrupoTrabalho]
            var repGrupoTrabProj = new Repositorio<GrupoTrabalhoProjeto>(unidTrabalho);
            var gruposTrabProjeto = repGrupoTrabProj.ObterTodos();
            Assert.IsTrue(gruposTrabProjeto.Any());
            var gruposDel = gruposTrabProjeto.Where(x => x.Projeto.Codigo == projetoAux.Codigo).Select(x => x.GrupoTrabalho.Codigo).ToArray();
            var grupoNew = GrupoTrabalhoTeste.ObterGrupoTrabalho(true);
            #endregion

            #region [TreinamentoProjeto]
            var repTreinamentoProj = new Repositorio<TreinamentoProjeto>(unidTrabalho);
            var treinamentosProjeto = repTreinamentoProj.ObterTodos();
            Assert.IsTrue(treinamentosProjeto.Any());
            var treinamentosDel = treinamentosProjeto.Where(x => x.Projeto.Codigo == projetoAux.Codigo).Select(x => x.Treinamento.Codigo).ToArray();
            
            var treinamento = TreinamentoTeste.ObterTreinamento(true);
            var treinamentoProj = new TreinamentoProjeto();
            treinamentoProj.Projeto = projetoAux;
            treinamentoProj.Treinamento = new Treinamento { Codigo = treinamento.Codigo };
            treinamentoProj.Observacao = "Observação-" + data;
            treinamentoProj.Obrigatorio = Global.ObterBoolean();
            treinamentoProj.ImpressoCracha = Global.ObterBoolean();
            #endregion

            try
            {
                var result = controller.Alterar(
                    projetoAux,
                    areasDel, new int[] { areaNew.Codigo },
                    gruposDel, new int[] { grupoNew.Codigo },
                    treinamentosDel, new TreinamentoProjeto[] { treinamentoProj }
                ) as JsonResult;
                Assert.IsNotNull(result);

                var projeto = result.Data as ProjetoModelView;
                Assert.IsNotNull(projeto);
                Assert.AreEqual(projetoAux.Nome, projeto.Nome);
                Assert.AreEqual(projetoAux.Descricao, projeto.Descricao);
                Assert.AreEqual(projetoAux.DataInicio, projeto.DataInicio);
                Assert.AreEqual(projetoAux.DataFim, projeto.DataFim);
            }
            catch (CoreException ex)
            {
                Assert.IsTrue(ex.Message.Contains("Não é possível alterar a Data Fim do Projeto. Existe um Pedido Compra (PO) com prazo maior que a data informada"));
            }

            //Assert.AreEqual(areaNew.Codigo, projeto.Area[0].Codigo);
            //Assert.AreEqual(grupoNew.Codigo, projeto.GrupoTrabalho[0].Codigo);
            //Assert.AreEqual(treinamentoProj.Codigo, projeto.Treinamento[0].Codigo);
            //Assert.AreEqual(treinamentoNew.Treinamento.Nome.ToUpper(), projeto.Treinamento[0].Nome.ToUpper());
            //Assert.AreEqual(treinamentoNew.Obrigatorio, projeto.Treinamento[0].Obrigatorio);
            //Assert.AreEqual(treinamentoNew.ImpressoCracha, projeto.Treinamento[0].ImpressoCracha);
            //Assert.AreEqual(treinamentoNew.Observacao.ToUpper(), projeto.Treinamento[0].Observacao.ToUpper());
        }

        #region [Desativado]

        /*
        /// <summary>
        /// Valida ativação ou inativação de Projeto, conforme status corrente;
        /// </summary>
        [TestMethod]
        public void Pode_AtivarInativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            ProjetoController controller = new ProjetoController(unidTrabalho);
            Projeto projetoAux = ObterProjeto();
            Assert.IsNotNull(projetoAux);

            bool status = !projetoAux.Ativo;
            var result = controller.AtivarInativar(projetoAux.Codigo) as JsonResult;
            Assert.IsNotNull(result.Data);

            var projeto = JsonHelper.GetJsonObjectRepresentation<Projeto>(result);
            Assert.AreEqual(status, projeto.Ativo);
        }

        /// <summary>
        /// Busca aleatóriamente um Projeto cadastrado na base de dados
        /// </summary>
        /// <param name="somenteAtivos">Filtrar Projetos ativos</param>
        /// <returns></returns>
        internal static Projeto ObterProjeto(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            ProjetoController controller = new ProjetoController(unidTrabalho);
            JsonResult result = controller.ObterTodos() as JsonResult;
            Assert.IsNotNull(result.Data);

            Random rand = new Random();
            var projetos = JsonHelper.GetJsonObjectRepresentation<List<Projeto>>(result);
            projetos = projetos.OrderBy(x => rand.Next()).ToList();

            Projeto projeto = null;
            foreach (var item in projetos)
            {
                result = controller.Selecionar(item.Codigo) as JsonResult;
                Assert.IsNotNull(result.Data);

                projeto = JsonHelper.GetJsonObjectRepresentation<Projeto>(result);
                if (somenteAtivos)
                {
                    if (projeto.Ativo)
                        break;
                    projeto = null;
                }
                else
                    break;
            }

            return projeto;
        }
        */

        #endregion

        /// <summary>
        /// Valida Ativar Projeto.
        /// </summary>
        [TestMethod]
        public void Pode_Ativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new ProjetoController(unidTrabalho);
            var lista = ObterListaProjeto();
            Assert.IsTrue(lista.Any(), Resources.Lista_vazia);

            var projetoAux = lista.FirstOrDefault(x => x.Ativo == false);
            Assert.IsNotNull(projetoAux);
            Assert.IsFalse(projetoAux.Ativo);

            var result = controller.AtivarInativar(projetoAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var projeto = result.Data as ProjetoModelView;
            Assert.IsNotNull(projeto);
            Assert.AreEqual(projetoAux.Codigo, projeto.Codigo);
            Assert.IsTrue(projeto.Ativo);
        }

        /// <summary>
        /// Valida Inativar Projeto.
        /// </summary>
        [TestMethod]
        public void Pode_Inativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new ProjetoController(unidTrabalho);
            var projetoAux = ObterProjeto(true);
            Assert.IsTrue(projetoAux.Ativo);

            try
            {
                var result = controller.AtivarInativar(projetoAux.Codigo) as JsonResult;
                Assert.IsNotNull(result);

                var projeto = result.Data as ProjetoModelView;
                Assert.IsNotNull(projeto);
                Assert.AreEqual(projetoAux.Codigo, projeto.Codigo);
                Assert.IsFalse(projeto.Ativo);
            }
            catch (CoreException ex)
            {
                Assert.IsTrue(ex.Message.Contains("Não é possível inativar, o projeto tem Pedido de Compra (PO) ativo"));
            }
        }

        /// <summary>
        /// Busca aleatóriamente um Projeto cadastrado na base de dados.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// Filtrar Projetos ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Projeto.
        /// </returns>
        public static Projeto ObterProjeto(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<Projeto>(unidTrabalho);
            var projetos = repositorio.ObterTodos().ToList();
            Assert.IsTrue(projetos.Any());

            var rand = new Random();
            projetos = projetos.OrderBy(x => rand.Next()).ToList();

            return projetos.FirstOrDefault(x => !somenteAtivos || x.Ativo);

            //Projeto projeto = null;
            //foreach (var item in projetos)
            //{
            //    projeto = item;

            //    if (projeto == null)
            //        continue;
            //    if (somenteAtivos)
            //    {
            //        if (projeto.Ativo)
            //            break;
            //        projeto = null;
            //    }
            //    else
            //        break;
            //}

            //return projeto;
        }

        /// <summary>
        /// Obter lista projeto.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// Filtrar Projetos ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;Projeto&gt;
        /// </returns>
        public static List<Projeto> ObterListaProjeto(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<Projeto>(unidTrabalho);
            var projetos = repositorio.ObterTodos().ToList();
            Assert.IsTrue(projetos.Any());

            var rand = new Random();
            projetos = projetos.OrderBy(x => rand.Next()).ToList();

            return somenteAtivos ? projetos.Where(x => x.Ativo).ToList() : projetos;
        }

    }
}
