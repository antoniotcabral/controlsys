﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Parametros;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Repositorio.Parametros;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Tests.Controller
{
    [TestClass]
    public class TipoValidadeTeste
    {
        CultureInfo cultbr = new CultureInfo("pt-BR");

        [TestMethod]
        public void Incluir()
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();

            TipoValidade registro = new TipoValidade();
            var rand = new Random();
            registro.Nome = "Nome" + rand.Next(1, 9999);
            registro.DiasValidade = rand.Next(1, 9999);
            registro.IdadeInicial = rand.Next(1, 5000);
            registro.IdadeFinal = rand.Next(5001, 9999);

            TipoValidadeController controller = new TipoValidadeController(unidTrabalho);

            var result = controller.Incluir(registro) as JsonResult;
            Assert.IsNotNull(result);

            var registroGerado = result.Data as TipoValidade;

            Assert.IsNotNull(registroGerado);
            Assert.IsTrue(registroGerado.Codigo > 0);
            Assert.IsTrue(registroGerado.Ativo);
            Assert.AreEqual(registro.Nome, registroGerado.Nome);
            Assert.AreEqual(registro.DiasValidade, registroGerado.DiasValidade);
            Assert.AreEqual(registro.IdadeInicial, registroGerado.IdadeInicial);
            Assert.AreEqual(registro.IdadeFinal, registroGerado.IdadeFinal);
            Assert.IsNull(registroGerado.DataDesativacao);

            unidTrabalho.Rollback();
        }

        [TestMethod]
        public void Alterar()
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();

            TipoValidade registro = ObterTipoValidade(true);
            var rand = new Random();
            registro.Nome = "Nome" + rand.Next(1, 9999);
            registro.DiasValidade = rand.Next(1, 9999);
            registro.IdadeInicial = rand.Next(1, 5000);
            registro.IdadeFinal = rand.Next(5001, 9999);

            TipoValidadeController controller = new TipoValidadeController(unidTrabalho);

            var result = controller.Alterar(registro) as JsonResult;
            Assert.IsNotNull(result);

            var registroAlterado = result.Data as TipoValidade;

            Assert.IsNotNull(registroAlterado);
            Assert.AreEqual(registro.Codigo, registroAlterado.Codigo);
            Assert.AreEqual(registro.DataRegistro, registroAlterado.DataRegistro);
            Assert.AreEqual(registro.DataDesativacao, registroAlterado.DataDesativacao);
            Assert.AreEqual(registro.Nome, registroAlterado.Nome);
            Assert.AreEqual(registro.DiasValidade, registroAlterado.DiasValidade);
            Assert.AreEqual(registro.IdadeInicial, registroAlterado.IdadeInicial);
            Assert.AreEqual(registro.IdadeFinal, registroAlterado.IdadeFinal);

            unidTrabalho.Rollback();
        }

        [TestMethod]
        public void Selecionar()
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            TipoValidadeController controller = new TipoValidadeController(unidTrabalho);

            TipoValidade registro = ObterTipoValidade();
            var result = controller.Selecionar(registro.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var registroEncontrado = result.Data as TipoValidade;

            Assert.IsNotNull(registroEncontrado);
            Assert.AreEqual(registro.Codigo, registroEncontrado.Codigo);

            unidTrabalho.Rollback();
        }

        [TestMethod]
        public void ObterTodos()
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();
            TipoValidadeController controller = new TipoValidadeController(unidTrabalho);

            var result = controller.ObterTodos() as JsonResult;
            Assert.IsNotNull(result);

            var registros = JsonHelper.GetJsonObjectRepresentation<List<TipoValidade>>(result);

            Assert.IsNotNull(registros);
            Assert.IsTrue(registros.Any());

            unidTrabalho.Rollback();
        }

        public static TipoValidade ObterTipoValidade(bool somenteAtivo = false)
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            IRepositorioTipoValidade repTipoValidade = Fabrica.Instancia.ObterRepositorio<IRepositorioTipoValidade>(unidTrabalho);

            List<TipoValidade> lista = repTipoValidade.ObterTodos().ToList(); 

            if (somenteAtivo)
                lista = lista.Where(x => x.Ativo == true).ToList();

            return lista.OrderBy(x => new Random().Next()).FirstOrDefault();
        }

    }
}
