﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Controlsys.Dominio.Enderecos;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Controlsys.Persistencia;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) VisitanteTeste.
    /// </summary>
    [TestClass]
    public class VisitanteTeste
    {
        /// <summary>
        /// Valida incluir um novo Visitante.
        /// </summary>
        [TestMethod]
        public void Pode_Incluir()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new VisitanteController(unidTrabalho);

            var dt = DateTime.Now;
            var rand = new Random();
            var randValue = rand.Next(1, 9999);

            #region [PessoaFisica]
            #region [Estado]
            var repEstado = new Repositorio<Estado>(unidTrabalho);
            var estados = repEstado.ObterTodos().ToList();
            Assert.IsTrue(estados.Any());
            estados = estados.OrderBy(x => rand.Next()).ToList();
            var estado = estados[0];
            #endregion
            #region [Cidade]
            var repCidade = new Repositorio<Cidade>(unidTrabalho);
            var cidades = repCidade.ObterTodos().ToList();
            Assert.IsTrue(cidades.Any());
            cidades = cidades.Where(x => x.Estado.Codigo == estado.Codigo).OrderBy(x => rand.Next()).ToList();
            Assert.IsTrue(cidades.Any());
            var cidade = cidades[0];
            #endregion
            #region [Telefones]
            var telefones = new List<Telefone>()
            {
                new Telefone()
                {
                    TipoTelefone = TipoTelefone.Residencial,
                    TelefoneNum = Global.GerarTelefone(TipoTelefone.Residencial)
                },
                new Telefone()
                {
                    TipoTelefone = TipoTelefone.Emergencial,
                    TelefoneNum = Global.GerarTelefone(TipoTelefone.Emergencial)
                },
                new Telefone()
                {
                    TipoTelefone = TipoTelefone.Celular,
                    TelefoneNum = Global.GerarTelefone(TipoTelefone.Residencial)
                }

            };
            #endregion
            #region [Emails]
            var emails = new List<Email>();
            for (int i = 0; i < rand.Next(1, 3); i++)
            {
                emails.Add(new Email()
                {
                    EmailDesc = Global.GerarEmail()
                });
            }
            #endregion
            #region [Treinamento]
            var countAux = rand.Next(1, 3);
            var listaTreinamento = new List<TreinamentoPessoa>();
            for (int i = 0; i < countAux; i++)
            {
                var treinamento = TreinamentoTeste.ObterTreinamento(true);
                if (listaTreinamento.Exists(x => x.Treinamento == treinamento))
                {
                    countAux++;
                    continue;
                }
                var dtRealizacao = dt.AddDays(rand.Next(-365, -1));
                listaTreinamento.Add(new TreinamentoPessoa()
                {
                    Treinamento = treinamento,
                    DataRealizacao = dtRealizacao,
                    DataValidade = dtRealizacao.AddYears(1)
                });
            }
            #endregion
            #region [Documentações]
            var repCheckList = new Repositorio<ChecklistDocumento>(unidTrabalho);
            var listaCheckListDocumento = repCheckList.ObterTodos().ToList();
            listaCheckListDocumento = listaCheckListDocumento.Where(x => x.TipoCadastro == TipoCadastro.Visitante).ToList();
            Assert.IsTrue(listaCheckListDocumento.Any());

            var listaDocumento = new List<Documento>();
            var listaTipoDocumento = new List<TipoDocumento>();
            foreach (var checkListDocumento in listaCheckListDocumento)
            {
                foreach (var checkListTipoDocumento in checkListDocumento.TiposDocumento)
                {
                    var tipoDocumento = checkListTipoDocumento.TipoDocumento;
                    if (listaTipoDocumento.Contains(tipoDocumento))
                        continue;
                    listaTipoDocumento.Add(tipoDocumento);
                    listaDocumento.Add(new Documento()
                    {
                        TipoDocumento = tipoDocumento,
                        Descricao = "Descrição-" + randValue,
                        Status = Global.ObterEnumAleatorio<StatusDocumento>(),
                        Midias = null
                    });
                }
            }
            Assert.IsTrue(listaDocumento.Any());
            #endregion

            var pessoaFisica = new PessoaFisica()
            {
                CPF = Global.GerarCpf(true),
                Passaporte = "Passaport" + randValue,
                Nome = "Nome" + randValue,
                Apelido = "Apelido" + randValue,
                Email = Global.GerarEmail(),
                DataNascimento = dt.AddDays(rand.Next(-21900, -7300)),
                DataVisto = dt.AddDays(rand.Next(-365, -1)),
                DataValidadeVisto = dt.AddDays(rand.Next(1, 365)),
                Sexo = Global.ObterEnumAleatorio<Sexo>(),

                NomeMae = "Mae" + randValue,
                NomePai = "Pai" + randValue,
                RNE = "RNE" + randValue,
                DataExpedicaoRNE = dt.AddDays(rand.Next(1, 365)),
                EstadoCivil = Global.ObterEnumAleatorio<EstadoCivil>(),
                Escolaridade = Global.ObterEnumAleatorio<Escolaridade>(),
                TipoSanguineo = Global.ObterEnumAleatorio<TipoSanguineo>(),
                Nacionalidade = "Brasileiro",
                Naturalidade = cidade,

                RG = randValue.ToString(),
                RGOrgaoEmissor = "SSP",
                RGOrgaoEmissorUF = estado,
                RGDataEmissao = dt.AddDays(rand.Next(-5475, -365)),

                TituloEleitor = rand.Next(100, 999999),
                TituloEleitorSecao = randValue,
                TituloEleitorZona = randValue,
                TituloEleitorCidade = cidade,

                CTPS = randValue,
                CTPSData = dt.AddDays(rand.Next(-999, -365)),
                CTPSSerie = "CTPS" + randValue,
                CTPSEstado = estado,

                CNH = "cnh-" + randValue,
                CNHCategoria = Global.ObterBoolean() ? CategoriaCNH.A : CategoriaCNH.AB,
                CNHDataValidade = dt.AddDays(rand.Next(365, 999)),

                CertificadoReservista = randValue.ToString(),
                CertificadoReservistaCat = "CAT-" + randValue,

                Endereco = EnderecoTeste.ObterEnderecoNovo(),

                //Documentos = 
                Telefones = telefones,
                Emails = emails
            };
            #endregion

            #region [Visitante]
            var dtVisita = dt.AddDays(rand.Next(-365, -1));
            var visitanteAux = new Visitante()
            {
                TipoVisitante = Global.ObterEnumAleatorio<TipoVisitante>(),
                Empresa = "Empresa-" + randValue,
                AntiPassBack = Global.ObterBoolean(),
                AcessoPorto = Global.ObterBoolean(),
                Solicitante = ColaboradorTeste.ObterColaborador(),
                ResponsavelVisita = ColaboradorTeste.ObterColaborador(),
                Motivo = "Motivo-" + randValue,

                DataInicio = dtVisita,
                DataFim = dtVisita.AddDays(rand.Next(1, 7)),
                Funcao = "Função-" + randValue,

                PessoaFisica = pessoaFisica
            };
            #endregion

            //TODO: Fazer upload da foto

            var result = controller.Incluir(visitanteAux, null, listaTreinamento, new List<DocumentoMidiaModelView>()/*listaDocumento*/, null, new string[] { }, null) as JsonResult;
            Assert.IsNotNull(result);

            var visitante = result.Data as VisitanteModelView;
            Assert.IsNotNull(visitante);

            // Validando Pessoa
            Assert.AreEqual(pessoaFisica.CPF, visitante.CPF.Replace(".", "").Replace("-", ""));
            Assert.AreEqual(pessoaFisica.TipoSanguineo, visitante.TipoSanguineo);
            Assert.AreEqual(pessoaFisica.Sexo.ToString(), visitante.Sexo);
            Assert.AreEqual(pessoaFisica.EstadoCivil, visitante.EstadoCivil);
            //Assert.AreEqual(pessoaFisica.DataExpedicaoRNE, visitante.DataExpedicaoRNE);
            //Assert.AreEqual(pessoaFisica.DataNascimento, visitante.DataNascimento);
            Assert.AreEqual(pessoaFisica.NomePai, visitante.NomePai);
            Assert.AreEqual(pessoaFisica.NomeMae, visitante.NomeMae);
            Assert.AreEqual(pessoaFisica.RG, visitante.RG);
            Assert.AreEqual(pessoaFisica.RGOrgaoEmissor, visitante.RGOrgaoEmissor);
            Assert.AreEqual(pessoaFisica.RGOrgaoEmissorUF.Codigo, visitante.RGOrgaoEmissorUF);
            //Assert.AreEqual(pessoaFisica.RGDataEmissao, visitante.RGDataEmissao);
            Assert.AreEqual(pessoaFisica.Passaporte, visitante.Passaporte);
            Assert.AreEqual(pessoaFisica.Naturalidade.Codigo, visitante.Naturalidade);
            Assert.AreEqual(pessoaFisica.CTPS, visitante.CTPS);
            //Assert.AreEqual(pessoaFisica.CTPSSerie, visitante.CTPSSerie);
            Assert.AreEqual(pessoaFisica.CTPSEstado.Codigo, visitante.CTPSEstado);
            Assert.AreEqual(pessoaFisica.CNH, visitante.CNH);
            Assert.AreEqual(pessoaFisica.CNHCategoria, visitante.CNHCategoria);
            Assert.AreEqual(pessoaFisica.TituloEleitor, visitante.TituloEleitor);
            Assert.AreEqual(pessoaFisica.TituloEleitorCidade.Codigo, visitante.TituloEleitorCidade);
            Assert.AreEqual(pessoaFisica.CertificadoReservista, visitante.CertificadoReservista);
            Assert.AreEqual(pessoaFisica.CertificadoReservistaCat, visitante.CertificadoReservistaCat);
            Assert.AreEqual(pessoaFisica.RNE, visitante.RNE);
            Assert.AreEqual(pessoaFisica.Escolaridade, visitante.Escolaridade);

            // Validando Visitante
            Assert.AreEqual(visitanteAux.TipoVisitante.ObterDescricaoEnum(), visitante.TipoVisitante);
            Assert.AreEqual(visitanteAux.Empresa, visitante.Empresa);
            Assert.AreEqual(visitanteAux.AntiPassBack, visitante.AntiPassBack);
            Assert.AreEqual(visitanteAux.AcessoPorto, visitante.AcessoPorto);
            Assert.AreEqual(visitanteAux.Solicitante.Codigo, visitante.Solicitante.Codigo);
            Assert.AreEqual(visitanteAux.ResponsavelVisita.Codigo, visitante.ResponsavelVisita.Codigo);
            Assert.AreEqual(visitanteAux.Motivo, visitante.Motivo);
            Assert.AreEqual(visitanteAux.DataInicio, visitante.DataInicio);
            Assert.AreEqual(visitanteAux.DataFim, visitante.DataFim);
            Assert.AreEqual(visitanteAux.Funcao, visitante.Funcao);

            // TODO: Validar lista de Emails
            // TODO: Validar lista de Papeis
        }

        /// <summary>
        /// Valida alterar um Visitante.
        /// </summary>
        [TestMethod]
        public void Pode_Alterar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new VisitanteController(unidTrabalho);

            var dt = DateTime.Now;
            var rand = new Random();
            var randValue = rand.Next(1, 9999);

            #region [Estado]
            var repEstado = new Repositorio<Estado>(unidTrabalho);
            var estados = repEstado.ObterTodos().ToList();
            Assert.IsTrue(estados.Any());
            estados = estados.OrderBy(x => rand.Next()).ToList();
            var estado = estados[0];
            #endregion
            #region [Cidade]
            var repCidade = new Repositorio<Cidade>(unidTrabalho);
            var cidades = repCidade.ObterTodos().ToList();
            Assert.IsTrue(cidades.Any());
            cidades = cidades.Where(x => x.Estado.Codigo == estado.Codigo).OrderBy(x => rand.Next()).ToList();
            Assert.IsTrue(cidades.Any());
            var cidade = cidades[0];
            #endregion
            #region [Telefones]
            var telefones = new List<Telefone>()
            {
                new Telefone()
                {
                    TipoTelefone = TipoTelefone.Celular,
                    TelefoneNum = Global.GerarTelefone(TipoTelefone.Celular)
                },
                new Telefone()
                {
                    TipoTelefone = TipoTelefone.Emergencial,
                    TelefoneNum = Global.GerarTelefone(TipoTelefone.Emergencial)
                },
                new Telefone()
                {
                    TipoTelefone = TipoTelefone.Celular,
                    TelefoneNum = Global.GerarTelefone(TipoTelefone.Residencial)
                },
                new Telefone()
                {
                    TipoTelefone = TipoTelefone.Empresarial,
                    TelefoneNum = Global.GerarTelefone(TipoTelefone.Empresarial)
                }

            };
            #endregion
            #region [Emails]
            var emails = new List<Email>();
            for (int i = 0; i < rand.Next(1, 3); i++)
            {
                emails.Add(new Email()
                {
                    EmailDesc = Global.GerarEmail()
                });
            }
            #endregion
            #region [Visitante]
            var visitanteAux = ObterVisitante();
            Assert.IsNotNull(visitanteAux);
            var dtVisita = dt.AddDays(rand.Next(-365, -1));
            visitanteAux.TipoVisitante = Global.ObterEnumAleatorio<TipoVisitante>();
            visitanteAux.Empresa = "Empresa-" + randValue;
            visitanteAux.AntiPassBack = Global.ObterBoolean();
            visitanteAux.AcessoPorto = Global.ObterBoolean();
            visitanteAux.Solicitante = ColaboradorTeste.ObterColaborador();
            visitanteAux.ResponsavelVisita = ColaboradorTeste.ObterColaborador();
            visitanteAux.Motivo = "Motivo-" + randValue;
            visitanteAux.DataInicio = dtVisita;
            visitanteAux.DataFim = dtVisita.AddDays(rand.Next(1, 7));
            visitanteAux.Funcao = "Função-" + randValue;
            #endregion

            var result = controller.Alterar(visitanteAux,
                null,
                new string[] { "TipoVisitante", "Empresa", "AntiPassBack", "AcessoPorto", "Solicitante", "ResponsavelVisita", "Motivo", "DataInicio", "DataFim", "Funcao" },
                null, null, null, null, null, null) as JsonResult;
            Assert.IsNotNull(result);

            var visitante = result.Data as VisitanteModelView;
            Assert.IsNotNull(visitante);
            Assert.AreEqual(visitanteAux.TipoVisitante.ObterDescricaoEnum(), visitante.TipoVisitante);
            Assert.AreEqual(visitanteAux.Empresa, visitante.Empresa);
            Assert.AreEqual(visitanteAux.AntiPassBack, visitante.AntiPassBack);
            Assert.AreEqual(visitanteAux.AcessoPorto, visitante.AcessoPorto);
            Assert.AreEqual(visitanteAux.Solicitante.Codigo, visitante.Solicitante.Codigo);
            Assert.AreEqual(visitanteAux.ResponsavelVisita.Codigo, visitante.ResponsavelVisita.Codigo);
            Assert.AreEqual(visitanteAux.Motivo, visitante.Motivo);
            Assert.AreEqual(visitanteAux.DataInicio, visitante.DataInicio);
            Assert.AreEqual(visitanteAux.DataFim, visitante.DataFim);
            Assert.AreEqual(visitanteAux.Funcao, visitante.Funcao);

            // TODO: Validar lista de Emails
            // TODO: Validar lista de Papeis
        }

        /// <summary>
        /// Busca aleatóriamente um visitante cadastrado na base de dados.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) Visitante.
        /// </returns>
        public static Visitante ObterVisitante()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<Visitante>(unidTrabalho);
            var listaVisitante = repositorio.ObterTodos().ToList();
            Assert.IsTrue(listaVisitante.Any());

            var rand = new Random();
            listaVisitante = listaVisitante.OrderBy(x => rand.Next()).ToList();
            return listaVisitante.First();
        }

        /// <summary>
        /// Obter lista visitante.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) List&lt;Visitante&gt;
        /// </returns>
        public static List<Visitante> ObterListaVisitante()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<Visitante>(unidTrabalho);
            var listaVisitante = repositorio.ObterTodos().ToList();
            Assert.IsTrue(listaVisitante.Any());

            var rand = new Random();
            listaVisitante = listaVisitante.OrderBy(x => rand.Next()).ToList();
            return listaVisitante;
        }

        #region Testes específicos


        [TestMethod]
        public void VisitanteAntipassback()
        {
            bool antipassbackEsperado = true;
            string cpf = "28293961899";

            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<Visitante>(unidTrabalho);
            var listaVisitante = repositorio.ObterTodos().Where(v => v.PessoaFisica.CPF == cpf).ToList();
            Assert.IsTrue(listaVisitante.Any());

            var visitante = listaVisitante.Where(v => v.ObterLogPapel().Status == StatusPapelLog.Ativo).FirstOrDefault();
            Assert.IsNotNull(visitante);

            bool usaAntipassback = visitante.AntiPassBack;
            if (antipassbackEsperado) Assert.IsTrue(usaAntipassback);
            else Assert.IsFalse(usaAntipassback);
        }


        #endregion
    }
}
