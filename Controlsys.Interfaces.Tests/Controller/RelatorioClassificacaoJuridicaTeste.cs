﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Tests.App_Start;
using Controlsys.Repositorios.Empresas;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using Rhino.Mocks;
using Rhino.Mocks.Constraints;
using Controlsys.Dominio.Empresas;

namespace Controlsys.Interfaces.Tests.Controller
{
    [TestClass]
    public class RelatorioClassificacaoJuridicaTeste
    {
        [TestMethod]
        public void Exportar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new RelatorioClassificacaoJuridicaController(unidTrabalho);

            var qtd = new Random().Next(1, 3);
            List<Empresa> empresas = new List<Empresa>();
            for (int i = 0; i < qtd; i++)
            {
                empresas.Add(EmpresaTeste.ObterEmpresa());
            }

            Mock.ServerMapPath(controller, "RelatorioClassificacaoJuridica.rdlc");

            var result = controller.Exportar("PDF", null, empresas.Select(x => x.Codigo).ToArray()) as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/pdf", result.ContentType);
            Assert.AreEqual("download.pdf", result.FileDownloadName);
        }
    }
}
