﻿using System;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using Controlsys.Dominio.AgendamentosPessoa;
using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Pessoas;
using Controlsys.Dominio.Seguranca;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Agendamentos;
using Controlsys.Repositorios.Empresas;
using Controlsys.Repositorios.Seguranca;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Controlsys.Interfaces.Tests.Controller
{
    [TestClass]
    public class PreCadastroPrestadorServicoTeste
    {
        [TestMethod]
        public void Incluir()
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();

            #region [fakeContext]
            var fakeHttpContext = new Mock<HttpContextBase>();
            var fakeIdentity = new GenericIdentity("rubem.almeida");
            var principal = new GenericPrincipal(fakeIdentity, null);

            fakeHttpContext.Setup(t => t.User).Returns(principal);
            var controllerContext = new Mock<ControllerContext>();
            controllerContext.Setup(t => t.HttpContext).Returns(fakeHttpContext.Object);

            PreCadastroPrestadorServicoController controller = new PreCadastroPrestadorServicoController(unidTrabalho);
            controller.ControllerContext = controllerContext.Object;
            #endregion

            unidTrabalho.BeginTransaction();

            IRepositorioUsuario repUsu = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(unidTrabalho);
            Usuario usuariLogado = repUsu.ObterPorLogin(fakeIdentity.Name);
            Papel papel = usuariLogado.PessoaFisica.ObterPapel();
            Empresa empresa = (papel is Colaborador ? (papel as Colaborador).Empresa : (papel is PrestadorServico ? (papel as PrestadorServico).Empresa : null));

            IRepositorioPedidoCompra repPO = Fabrica.Instancia.ObterRepositorio<IRepositorioPedidoCompra>(unidTrabalho);
            var po = repPO.ObterTodos().Where(x => x.GestoresResponsaveis.Any() && x.Empresa.Codigo == empresa.Codigo).FirstOrDefault();
            var gestor = po.GestoresResponsaveis.FirstOrDefault();
            var regAux = gerarUmRegistro(unidTrabalho);


            var result = controller.Incluir(regAux, new string[] { }, /*po.Codigo, */gestor.Colaborador.Codigo, null, path: @"D:\Projetos\Controlsys\controlsys-develop-fase2\Controlsys.Interfaces\Content\templates\preCadastro.htm") as JsonResult;
            Assert.IsNotNull(result);

            var reg = result.Data as PrestadorServicoAgendamentoModelView;
            Assert.IsNotNull(reg);
            Assert.IsTrue(reg.Codigo > 0);

            unidTrabalho.Rollback();
        }

        [TestMethod]
        public void Alterar()
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();

            #region [fakeContext]
            var fakeHttpContext = new Mock<HttpContextBase>();
            var fakeIdentity = new GenericIdentity("rubem.almeida");
            var principal = new GenericPrincipal(fakeIdentity, null);

            fakeHttpContext.Setup(t => t.User).Returns(principal);
            var controllerContext = new Mock<ControllerContext>();
            controllerContext.Setup(t => t.HttpContext).Returns(fakeHttpContext.Object);

            PreCadastroPrestadorServicoController controller = new PreCadastroPrestadorServicoController(unidTrabalho);
            controller.ControllerContext = controllerContext.Object;
            #endregion

            unidTrabalho.BeginTransaction();

            var regAux = ObterUmRegistro(unidTrabalho, StatusAgendamento.ContratacaoAutorizada, StatusAgendamento.AguardandoAprovacao, StatusAgendamento.CadastroNaoAprovado);
            Assert.IsNotNull(regAux);

            regAux.PessoaAg.NomeMae = "Teste " + new Random().Next();
            
            var result = controller.Alterar(regAux, new string[] { "NomeMae" }, null, 0, null) as JsonResult;
            var reg = result.Data as PrestadorServicoAgendamentoModelView;

            Assert.IsNotNull(reg);
            Assert.AreEqual(regAux.PessoaAg.NomeMae, reg.NomeMae);

            unidTrabalho.Rollback();
        }

        [TestMethod]
        public void NaoAutorizar()
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();

            #region [fakeContext]
            var fakeHttpContext = new Mock<HttpContextBase>();
            var fakeIdentity = new GenericIdentity("rubem.almeida");
            var principal = new GenericPrincipal(fakeIdentity, null);

            fakeHttpContext.Setup(t => t.User).Returns(principal);
            var controllerContext = new Mock<ControllerContext>();
            controllerContext.Setup(t => t.HttpContext).Returns(fakeHttpContext.Object);

            PreCadastroPrestadorServicoController controller = new PreCadastroPrestadorServicoController(unidTrabalho);
            controller.ControllerContext = controllerContext.Object;
            #endregion

            unidTrabalho.BeginTransaction();

            var regAux = ObterUmRegistro(unidTrabalho, StatusAgendamento.AguardandoAutorizacao);

            string motivo = "Descrição " + new Random().Next();

            var result = controller.NaoAutorizar(regAux.Codigo, motivo, path: @"D:\Projetos\Controlsys\controlsys-develop-fase2\Controlsys.Interfaces\Content\templates\preCadastroMotivo.htm") as JsonResult;
            var reg = result.Data as PrestadorServicoAgendamentoModelView;

            Assert.IsNotNull(reg);
            Assert.IsTrue(regAux.ObterStatusAtual().StatusAgendamento == StatusAgendamento.ContratacaoNaoAutorizada);

            unidTrabalho.Rollback();
        }

        [TestMethod]
        public void Autorizar()
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();

            #region [fakeContext]
            var fakeHttpContext = new Mock<HttpContextBase>();
            var fakeIdentity = new GenericIdentity("rubem.almeida");
            var principal = new GenericPrincipal(fakeIdentity, null);

            fakeHttpContext.Setup(t => t.User).Returns(principal);
            var controllerContext = new Mock<ControllerContext>();
            controllerContext.Setup(t => t.HttpContext).Returns(fakeHttpContext.Object);

            PreCadastroPrestadorServicoController controller = new PreCadastroPrestadorServicoController(unidTrabalho);
            controller.ControllerContext = controllerContext.Object;
            #endregion

            unidTrabalho.BeginTransaction();

            var regAux = ObterUmRegistro(unidTrabalho, StatusAgendamento.AguardandoAutorizacao, StatusAgendamento.ContratacaoNaoAutorizada);

            var result = controller.Autorizar(regAux.Codigo) as JsonResult;
            var reg = result.Data as PrestadorServicoAgendamentoModelView;

            Assert.IsNotNull(reg);
            Assert.IsTrue(regAux.ObterStatusAtual().StatusAgendamento == StatusAgendamento.ContratacaoAutorizada);

            unidTrabalho.Rollback();
        }

        [TestMethod]
        public void Reprovar()
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();

            #region [fakeContext]
            var fakeHttpContext = new Mock<HttpContextBase>();
            var fakeIdentity = new GenericIdentity("rubem.almeida");
            var principal = new GenericPrincipal(fakeIdentity, null);

            fakeHttpContext.Setup(t => t.User).Returns(principal);
            var controllerContext = new Mock<ControllerContext>();
            controllerContext.Setup(t => t.HttpContext).Returns(fakeHttpContext.Object);

            PreCadastroPrestadorServicoController controller = new PreCadastroPrestadorServicoController(unidTrabalho);
            controller.ControllerContext = controllerContext.Object;
            #endregion

            unidTrabalho.BeginTransaction();

            var regAux = ObterUmRegistro(unidTrabalho, StatusAgendamento.AguardandoAprovacao, StatusAgendamento.ContratacaoAutorizada);

            string motivo = "Lista de pendências: " + new Random().Next();

            var result = controller.Reprovar(regAux.Codigo, motivo, path: @"D:\Projetos\Controlsys\controlsys-develop-fase2\Controlsys.Interfaces\Content\templates\preCadastroMotivo.htm") as JsonResult;
            var reg = result.Data as PrestadorServicoAgendamentoModelView;

            Assert.IsNotNull(reg);
            Assert.IsTrue(regAux.ObterStatusAtual().StatusAgendamento == StatusAgendamento.CadastroNaoAprovado);

            unidTrabalho.Rollback();
        }

        [TestMethod]
        public void Aprovar()
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();

            #region [fakeContext]
            var fakeHttpContext = new Mock<HttpContextBase>();
            var fakeIdentity = new GenericIdentity("rubem.almeida");
            var principal = new GenericPrincipal(fakeIdentity, null);

            fakeHttpContext.Setup(t => t.User).Returns(principal);
            var controllerContext = new Mock<ControllerContext>();
            controllerContext.Setup(t => t.HttpContext).Returns(fakeHttpContext.Object);

            PreCadastroPrestadorServicoController controller = new PreCadastroPrestadorServicoController(unidTrabalho);
            controller.ControllerContext = controllerContext.Object;
            #endregion

            unidTrabalho.BeginTransaction();

            var regAux = ObterUmRegistro(unidTrabalho, StatusAgendamento.AguardandoAprovacao, StatusAgendamento.ContratacaoAutorizada);

            var result = controller.Aprovar(regAux.Codigo) as JsonResult;
            var reg = result.Data as PrestadorServicoAgendamentoModelView;

            Assert.IsNotNull(reg);
            Assert.IsNotNull(reg.Codigo > 0);
            //Assert.IsTrue(regAux.ObterStatusAtual().StatusAgendamento == StatusAgendamento.CadastroAprovado);

            unidTrabalho.Rollback();
        }


        public static PrestadorServicoAgendamento gerarUmRegistro(IUnidadeTrabalho unidTrabalho)
        {
            PrestadorServicoAgendamento reg = new PrestadorServicoAgendamento
            {
                PessoaAg = new PessoaAgendamento
                {
                    CPF = Global.GerarCpf(true),
                    Nome = "Jose",
                    Apelido = "Joslito",
                    Email = "rubem@globalsys.com.br",
                    Sexo = Dominio.Pessoas.Sexo.Masculino,
                    DataNascimento = new DateTime(1980, 01, 02),
                    TelefoneNumEmergencial = "27997959272",
                    NomeMae = "Nome Mae 1",
                    NomePai = "Nome Pai 1",
                    EstadoCivil = EstadoCivil.Casado,
                    Escolaridade = Escolaridade.Doutorado,
                    Naturalidade = EnderecoTeste.ObterEndereco(unidTrabalho).Logradouro.Bairro.Cidade,

                    RG = "3061250",
                    RGDataEmissao = DateTime.Now,
                    RGOrgaoEmissor = "SSP",
                    RGOrgaoEmissorUF = EnderecoTeste.ObterEndereco(unidTrabalho).Logradouro.Bairro.Cidade.Estado,

                    CTPS = 1,
                    CTPSSerie = "2",
                    CTPSEstado = EnderecoTeste.ObterEndereco(unidTrabalho).Logradouro.Bairro.Cidade.Estado,
                    CTPSData = new DateTime(2000, 1, 1),

                    Endereco = EnderecoTeste.ObterEndereco(unidTrabalho).Logradouro,
                    EnderecoNumero = "1"
                },

                Empresa = EmpresaTeste.ObterEmpresa(true),
                DataAdmissao = new DateTime(2001, 2, 3),
                Cargo = CargoTeste.ObterCargo(true),
                TipoPrestador = TipoPrestador.Temporario
                //GestorPO = gestor
            };

            return reg;
        }


        public static PrestadorServicoAgendamento ObterUmRegistro(IUnidadeTrabalho unidTrabalho, StatusAgendamento status)
        {
            var regs = buscarTodos(unidTrabalho);

            regs = regs.Where(x => x.Status.OrderByDescending(y => y.Codigo).Select(z => z.StatusAgendamento).FirstOrDefault() == status);

            return regs.FirstOrDefault();
        }

        public static PrestadorServicoAgendamento ObterUmRegistro(IUnidadeTrabalho unidTrabalho, StatusAgendamento status1, StatusAgendamento status2)
        {
            var regs = buscarTodos(unidTrabalho);

            regs = regs.Where(
                x => x.Status.OrderByDescending(y => y.Codigo).Select(z => z.StatusAgendamento).FirstOrDefault() == status1 ||
                x.Status.OrderByDescending(y => y.Codigo).Select(z => z.StatusAgendamento).FirstOrDefault() == status2
            );
            return regs.FirstOrDefault();

        }

        public static PrestadorServicoAgendamento ObterUmRegistro(IUnidadeTrabalho unidTrabalho, StatusAgendamento status1, StatusAgendamento status2, StatusAgendamento status3)
        {
            var regs = buscarTodos(unidTrabalho);

            regs = regs.Where(
                x => x.Status.OrderByDescending(y => y.Codigo).Select(z => z.StatusAgendamento).FirstOrDefault() == status1 ||
                x.Status.OrderByDescending(y => y.Codigo).Select(z => z.StatusAgendamento).FirstOrDefault() == status2 ||
                x.Status.OrderByDescending(y => y.Codigo).Select(z => z.StatusAgendamento).FirstOrDefault() == status3
            );
            return regs.FirstOrDefault();

        }

        private static IQueryable<PrestadorServicoAgendamento> buscarTodos(IUnidadeTrabalho unidTrabalho)
        {
            var rep = Fabrica.Instancia.ObterRepositorio<IRepositorioPreCadastroPrestador>(unidTrabalho);
            return rep.ObterTodos();
        }
    }
}
