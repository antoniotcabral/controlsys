﻿using Controlsys.Dominio.Parametros;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Controlsys.Persistencia;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Tests.Controller
{
    [TestClass]
    public class ModeloCredencialTeste
    {
        /// <summary>
        /// Pode incluir.
        /// </summary>
        [TestMethod]
        public void Pode_Incluir()
        {
            var data = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            var modeloCredencialAux = new ModeloCredencial();
            modeloCredencialAux.Nome = "Nome-" + data;
            modeloCredencialAux.TipoCredencial = Global.ObterEnumAleatorio<TipoCredencial>();
            modeloCredencialAux.HtmlFrente = "<div class='campo edt txt placa2' id='placa2Frente' style='position: absolute; right: auto; left: 48px; top: 112px;'>PLACA 2</div><div class='campo edt txt indicadorCorporativo' id='indicadorCorporativoFrente' style='position: absolute; right: auto; left: 43px; top: 206px;'>CORPORATIVO (S/N)</div>";
            modeloCredencialAux.HtmlVerso = "<div class='campo edt txt dataInspecao' id='dataInspecaoVerso' style='position: absolute; right: auto; left: 53px; top: 151px;'>DATA INSPEÇÃO</div><div class='campo edt txt empresaNome' id='empresaNomeVerso' style='position: absolute; right: auto; left: 136px; top: 187px;'>EMPRESA</div><div class='campo edt txt numero' id='numeroVerso' style='position: absolute; right: auto; left: 43px; top: 48px;'>NÚMERO REGISTRO</div>";
            modeloCredencialAux.Altura = new Random().Next(1, 999);
            modeloCredencialAux.Largura = new Random().Next(1, 999);
            var fundoFrente = "iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAIAAAACUFjqAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAC1JREFUeNpi/P//PwMMMDIyonFZ8MgBuVAhrHIgBkgJDjkQG4jxWT94nQYQYACFckf/FCnS7wAAAABJRU5ErkJggg==";
            var fundoVerso = "iVBORw0KGgoAAAANSUhEUgAAAA8AAAAPCAMAAAAMCGV4AAAADFBMVEUVZJ0VZZ4cebMde7VYjpkuAAAAHklEQVR4AWNgYmZkQCIYULlMDKhcZgYwCzcx2M0DAH+AAQ8e3xflAAAAAElFTkSuQmCC";

            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();

            var controller = new ModeloCredencialController(unidTrabalho);

            var result = controller.Incluir(modeloCredencialAux, fundoFrente, fundoVerso) as JsonResult;
            Assert.IsNotNull(result);

            var modeloCredencial = result.Data as ModeloCredencialModelView;
            Assert.IsNotNull(modeloCredencial);
            Assert.AreEqual(modeloCredencialAux.Nome.ToLower(), modeloCredencial.Nome.ToLower());
            Assert.AreEqual(modeloCredencialAux.Altura, modeloCredencial.Altura);
            Assert.AreEqual(modeloCredencialAux.Largura, modeloCredencial.Largura);
            Assert.AreEqual(modeloCredencialAux.TipoCredencial.ObterDescricaoEnum().ToLower(), modeloCredencial.TipoCredencialNome.ToLower());
            Assert.IsTrue(modeloCredencial.Ativo);

            unidTrabalho.Rollback();
        }

        /// <summary>
        /// Pode alterar.
        /// </summary>
        [TestMethod]
        public void Pode_Alterar()
        {
            ModeloCredencial modeloCredencialAux = ObterModeloCredencial(true);
            Assert.IsNotNull(modeloCredencialAux);

            var data = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            modeloCredencialAux.Nome = "Nome-" + data;
            var fundoFrente = "iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAIAAAACUFjqAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAC1JREFUeNpi/P//PwMMMDIyonFZ8MgBuVAhrHIgBkgJDjkQG4jxWT94nQYQYACFckf/FCnS7wAAAABJRU5ErkJggg==";
            var fundoVerso = "iVBORw0KGgoAAAANSUhEUgAAAA8AAAAPCAMAAAAMCGV4AAAADFBMVEUVZJ0VZZ4cebMde7VYjpkuAAAAHklEQVR4AWNgYmZkQCIYULlMDKhcZgYwCzcx2M0DAH+AAQ8e3xflAAAAAElFTkSuQmCC";

            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new ModeloCredencialController(unidTrabalho);

            var result = controller.Alterar(modeloCredencialAux, fundoFrente, fundoVerso) as JsonResult;
            Assert.IsNotNull(result);

            var modeloCredencial = result.Data as ModeloCredencialModelView;
            Assert.IsNotNull(modeloCredencial);
            Assert.AreEqual(modeloCredencialAux.Codigo, modeloCredencial.Codigo);
            Assert.AreEqual(modeloCredencialAux.Nome.ToLower(), modeloCredencial.Nome.ToLower());
            Assert.AreEqual(modeloCredencialAux.TipoCredencial.ObterDescricaoEnum().ToLower(), modeloCredencial.TipoCredencialNome.ToLower());
            Assert.AreEqual(modeloCredencialAux.Ativo, modeloCredencial.Ativo);
        }

        /// <summary>
        /// Pode selecionar.
        /// </summary>
        [TestMethod]
        public void Pode_Selecionar()
        {
            ModeloCredencial modeloCredencialAux = ObterModeloCredencial(true);
            Assert.IsNotNull(modeloCredencialAux);

            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new ModeloCredencialController(unidTrabalho);

            var result = controller.Selecionar(modeloCredencialAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var modeloCredencial = result.Data as ModeloCredencialModelView;
            Assert.IsNotNull(modeloCredencial);
            Assert.AreEqual(modeloCredencialAux.Codigo, modeloCredencial.Codigo);
            Assert.AreEqual(modeloCredencialAux.Nome.ToLower(), modeloCredencial.Nome.ToLower());
            Assert.AreEqual(modeloCredencialAux.TipoCredencial.ObterDescricaoEnum().ToLower(), modeloCredencial.TipoCredencialNome.ToLower());
            Assert.AreEqual(modeloCredencialAux.Ativo, modeloCredencial.Ativo);
        }

        /// <summary>
        /// Pode ativar.
        /// </summary>
        [TestMethod]
        public void Pode_Ativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new ModeloCredencialController(unidTrabalho);
            var lista = ObterListaModeloCredencial();
            Assert.IsTrue(lista.Any());

            lista = lista.Where(x => x.Ativo == false).ToList();
            Assert.IsTrue(lista.Any());

            var modeloCredencialAux = lista[0];
            Assert.IsFalse(modeloCredencialAux.Ativo);

            var result = controller.AtivarInativar(modeloCredencialAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var modeloCredencial = result.Data as ModeloCredencialModelView;
            Assert.IsNotNull(modeloCredencial);
            Assert.AreEqual(modeloCredencialAux.Codigo, modeloCredencial.Codigo);
            Assert.IsTrue(modeloCredencial.Ativo);
        }

        /// <summary>
        /// Pode inativar.
        /// </summary>
        [TestMethod]
        public void Pode_Inativar()
        {
            ModeloCredencial modeloCredencialAux = ObterModeloCredencial(true);
            Assert.IsNotNull(modeloCredencialAux);
            Assert.IsTrue(modeloCredencialAux.Ativo);

            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new ModeloCredencialController(unidTrabalho);

            var result = controller.AtivarInativar(modeloCredencialAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var modeloCredencial = result.Data as ModeloCredencialModelView;
            Assert.IsNotNull(modeloCredencial);
            Assert.AreEqual(modeloCredencialAux.Codigo, modeloCredencial.Codigo);
            Assert.IsFalse(modeloCredencial.Ativo);
        }

        /// <summary>
        /// Busca aleatóriamente um Modelo Credencial cadastrado na base de dados.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// Filtrar Modelo Credencial ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ModeloCredencial.
        /// </returns>
        public static ModeloCredencial ObterModeloCredencial(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<ModeloCredencial>(unidTrabalho);
            var listaModeloCredencial = repositorio.ObterTodos().ToList();
            Assert.IsNotNull(listaModeloCredencial);
            Assert.IsTrue(listaModeloCredencial.Any());

            var rand = new Random();
            listaModeloCredencial = listaModeloCredencial.OrderBy(x => rand.Next()).ToList();

            return listaModeloCredencial.FirstOrDefault(x => !somenteAtivos || x.Ativo);
        }

        /// <summary>
        /// Obtem lista com ordém aleatória de um Modelo Credencial cadastrado na base de dados.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// Filtrar Modelo Credencial ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;ModeloCredencial&gt;
        /// </returns>
        public static List<ModeloCredencial> ObterListaModeloCredencial(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<ModeloCredencial>(unidTrabalho);
            var listaModeloCredencial = repositorio.ObterTodos().ToList();
            Assert.IsNotNull(listaModeloCredencial);
            Assert.IsTrue(listaModeloCredencial.Any());

            var rand = new Random();
            listaModeloCredencial = listaModeloCredencial.OrderBy(x => rand.Next()).ToList();

            return somenteAtivos ? listaModeloCredencial.Where(x => x.Ativo == true).ToList() : listaModeloCredencial;
        }
    }
}
