﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Controlsys.Dominio.Acesso;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Controlsys.Repositorio.Acesso;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Controlsys.Infra;
using Controlsys.Persistencia;
using Controlsys.Dominio.Pessoas;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) GrupoAcessoTeste.
    /// </summary>
    [TestClass]
    public class GrupoAcessoTeste
    {
        /// <summary>
        /// Pode incluir.
        /// </summary>
        [TestMethod]
        public void Pode_Incluir()
        {
            IUnidadeTrabalho unidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new GrupoAcessoController(unidadeTrabalho);

            var data = DateTime.Now.ToString("MM/dd HH:mm:ss.fff");
            string nome = "Nome-" + data;
            var grupoAcessoAux = new GrupoAcesso() { Nome = nome };

            #region [Papel/Pessoa]
            var listaPessoas = PessoaTeste.ObterListaPessoasFisica();
            Assert.IsNotNull(listaPessoas);
            Assert.IsTrue(listaPessoas.Any());

            JsonResult result = null;
            var pessoaController = new PessoaController(unidadeTrabalho);
            foreach (var pessoa in listaPessoas)
            {
                result = pessoaController.ObterListaPessoa("", null, pessoa.Nome, true) as JsonResult;
                if (result != null)
                    break;
            }

            Assert.IsNotNull(result);
            List<PessoaModelView> listaPessoasNewAux = JsonHelper.GetJsonObjectRepresentation<List<PessoaModelView>>(result);
            Assert.IsNotNull(listaPessoasNewAux);
            Assert.IsTrue(listaPessoasNewAux.Any());

            var rand = new Random();
            listaPessoasNewAux = listaPessoasNewAux.OrderBy(x => rand.Next()).ToList();
            var countAux = listaPessoasNewAux.Count >= 3 ? 3 : (listaPessoasNewAux.Count >= 2 ? 2 : 1);
            var pessoasNew = new string[countAux];
            for (int i = 0; i < countAux; i++)
            {
                pessoasNew[i] = listaPessoasNewAux[i].Codigo;
            }
            #endregion

            #region [GrupoLeitora]
            GrupoLeitoraController grupoLeitoraController = new GrupoLeitoraController(unidadeTrabalho);
            result = grupoLeitoraController.ObterTodos(true) as JsonResult;
            Assert.IsNotNull(result);
            List<GrupoLeitoraModelView> listaGrupoLeitoraNewAux = JsonHelper.GetJsonObjectRepresentation<List<GrupoLeitoraModelView>>(result);
            Assert.IsNotNull(listaGrupoLeitoraNewAux);
            Assert.IsTrue(listaGrupoLeitoraNewAux.Any());

            rand = new Random();
            listaGrupoLeitoraNewAux = listaGrupoLeitoraNewAux.OrderBy(x => rand.Next()).ToList();
            countAux = listaGrupoLeitoraNewAux.Count >= 3 ? 3 : (listaGrupoLeitoraNewAux.Count >= 2 ? 2 : 1);
            var gruposLeitoraNew = new int[countAux];
            for (int i = 0; i < countAux; i++)
            {
                gruposLeitoraNew[i] = listaGrupoLeitoraNewAux[i].Codigo;
            }
            #endregion

            result = controller.Incluir(grupoAcessoAux, pessoasNew, gruposLeitoraNew) as JsonResult;
            Assert.IsNotNull(result);

            var grupoAcesso = JsonHelper.GetJsonObjectRepresentation<GrupoAcessoModelView>(result);
            Assert.IsNotNull(grupoAcesso);
            Assert.AreEqual(nome, grupoAcesso.Nome);
            Assert.IsTrue(grupoAcesso.Ativo);

            //TODO: Testar lista GrupoAcessoPapel
            //foreach (var grupoAcessoPapel in grupoAcesso.Pessoas)
            //{
            //    CollectionAssert.Contains(pessoasNew, grupoAcessoPapel.Papel.PessoaFisica.Codigo);
            //}

            //TODO: Testar lista GrupoLeitoraAcesso
            //foreach (var grupoLeitoraAcesso in grupoAcesso.GruposLeitora)
            //{
            //    CollectionAssert.Contains(gruposLeitoraNew, grupoLeitoraAcesso.GrupoLeitora.Codigo);
            //}
        }

        /// <summary>
        /// Pode alterar.
        /// </summary>
        [TestMethod]
        public void Pode_Alterar()
        {
            var unidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            GrupoAcessoController controller = new GrupoAcessoController(unidadeTrabalho);
            GrupoAcesso grupoAcessoAux = ObterGrupoAcesso(true);
            Assert.IsNotNull(grupoAcessoAux);
            Assert.IsTrue(grupoAcessoAux.Ativo);

            var data = DateTime.Now.ToString("MM/dd HH:mm:ss.fff");
            var nome = "Nome-" + data;
            grupoAcessoAux.Nome = nome;
            var propModificadas = new string[] { "Nome" };

            #region [Papel/Pessoa]
            var listaPessoas = PessoaTeste.ObterListaPessoasFisica();
            Assert.IsNotNull(listaPessoas);
            Assert.IsTrue(listaPessoas.Any());

            JsonResult result = null;
            var pessoaController = new PessoaController(unidadeTrabalho);
            foreach (var pessoa in listaPessoas)
            {
                result = pessoaController.ObterListaPessoa("", null, pessoa.Nome, true) as JsonResult;
                if (result != null)
                    break;
            }

            Assert.IsNotNull(result);
            List<PessoaModelView> listaPessoasNewAux = JsonHelper.GetJsonObjectRepresentation<List<PessoaModelView>>(result);
            Assert.IsNotNull(listaPessoasNewAux);
            Assert.IsTrue(listaPessoasNewAux.Any());

            var rand = new Random();
            listaPessoasNewAux = listaPessoasNewAux.OrderBy(x => rand.Next()).ToList();
            var countAux = listaPessoasNewAux.Count >= 3 ? 3 : (listaPessoasNewAux.Count >= 2 ? 2 : 1);
            var pessoasNew = new string[countAux];
            for (int i = 0; i < countAux; i++)
            {
                pessoasNew[i] = listaPessoasNewAux[i].Codigo;
            }

            int[] papeisDel = grupoAcessoAux.Pessoas.Select(x => x.Papel.Codigo).ToArray();
            #endregion

            #region [GrupoLeitora]
            GrupoLeitoraController grupoLeitoraController = new GrupoLeitoraController(unidadeTrabalho);
            result = grupoLeitoraController.ObterTodos(true) as JsonResult;
            Assert.IsNotNull(result);
            List<GrupoLeitoraModelView> listaGrupoLeitoraNewAux = JsonHelper.GetJsonObjectRepresentation<List<GrupoLeitoraModelView>>(result);
            Assert.IsNotNull(listaGrupoLeitoraNewAux);
            Assert.IsTrue(listaGrupoLeitoraNewAux.Any());

            rand = new Random();
            listaGrupoLeitoraNewAux = listaGrupoLeitoraNewAux.OrderBy(x => rand.Next()).ToList();
            countAux = listaGrupoLeitoraNewAux.Count >= 3 ? 3 : (listaGrupoLeitoraNewAux.Count >= 2 ? 2 : 1);
            var gruposLeitoraNew = new int[countAux];
            for (int i = 0; i < countAux; i++)
            {
                gruposLeitoraNew[i] = listaGrupoLeitoraNewAux[i].Codigo;
            }

            int[] gruposLeitoraDel = grupoAcessoAux.GruposLeitora.Select(x => x.GrupoLeitora.Codigo).ToArray();
            #endregion

            result = controller.Alterar(grupoAcessoAux, propModificadas, pessoasNew, papeisDel, gruposLeitoraNew, gruposLeitoraDel) as JsonResult;
            Assert.IsNotNull(result);

            var grupoAcesso = JsonHelper.GetJsonObjectRepresentation<GrupoAcessoModelView>(result);
            Assert.IsNotNull(grupoAcesso);
            Assert.AreEqual(grupoAcessoAux.Codigo, grupoAcesso.Codigo);
            Assert.AreEqual(nome, grupoAcesso.Nome);
            Assert.IsTrue(grupoAcesso.Ativo);

            //TODO: Testar lista GrupoAcessoPapel
            //foreach (var grupoAcessoPapel in grupoAcesso.Pessoas)
            //{
            //    CollectionAssert.Contains(pessoasNew, grupoAcessoPapel.Papel.PessoaFisica.Codigo);
            //}

            //TODO: Testar lista GrupoLeitoraAcesso
            //foreach (var grupoLeitoraAcesso in grupoAcesso.GruposLeitora)
            //{
            //    CollectionAssert.Contains(gruposLeitoraNew, grupoLeitoraAcesso.GrupoLeitora.Codigo);
            //}
        }

        /// <summary>
        /// Pode inativar.
        /// </summary>
        [TestMethod]
        public void Pode_Inativar()
        {
            var unidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new GrupoAcessoController(unidadeTrabalho);
            var grupoAcessoAux = ObterGrupoAcesso(true);
            Assert.IsNotNull(grupoAcessoAux);
            Assert.IsTrue(grupoAcessoAux.Ativo);

            var result = controller.AtivarInativar(grupoAcessoAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var grupoAcesso = result.Data as GrupoAcessoModelView;
            Assert.IsNotNull(grupoAcesso);
            Assert.AreEqual(grupoAcesso.Codigo, grupoAcessoAux.Codigo);
            Assert.IsFalse(grupoAcesso.Ativo);
        }

        /// <summary>
        /// Pode ativar.
        /// </summary>
        [TestMethod]
        public void Pode_Ativar()
        {
            var unidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new GrupoAcessoController(unidadeTrabalho);

            var listaGrupoAcesso = ObterListaGrupoAcesso();
            Assert.IsTrue(listaGrupoAcesso.Any());

            listaGrupoAcesso = listaGrupoAcesso.Where(x => x.Ativo == false).ToList();
            Assert.IsTrue(listaGrupoAcesso.Any());

            var grupoAcessoAux = listaGrupoAcesso.FirstOrDefault();
            Assert.IsNotNull(grupoAcessoAux);
            Assert.IsFalse(grupoAcessoAux.Ativo);

            var result = controller.AtivarInativar(grupoAcessoAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var grupoAcesso = result.Data as GrupoAcessoModelView;
            Assert.IsNotNull(grupoAcesso);
            Assert.AreEqual(grupoAcesso.Codigo, grupoAcessoAux.Codigo);
            Assert.IsTrue(grupoAcesso.Ativo);
        }

        /// <summary>
        /// Busca aleatóriamente um GrupoAcesso cadastrado na base de dados.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// Filtrar GruposAcesso ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) GrupoAcesso.
        /// </returns>
        public static GrupoAcesso ObterGrupoAcesso(bool somenteAtivos = false)
        {
            var unidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<GrupoAcesso>(unidadeTrabalho);
            var listaGrupoAcesso = repositorio.ObterTodos().ToList();
            Assert.IsTrue(listaGrupoAcesso.Any());

            Random rand = new Random();
            listaGrupoAcesso = listaGrupoAcesso.OrderBy(x => rand.Next()).ToList();

            return listaGrupoAcesso.FirstOrDefault(x => somenteAtivos ? x.Ativo : 1 == 1);
        }

        /// <summary>
        /// Obter lista grupo acesso.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// Filtrar GruposAcesso ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;GrupoAcessoModelView&gt;
        /// </returns>
        public static List<GrupoAcessoModelView> ObterListaGrupoAcesso(bool somenteAtivos = false)
        {
            var unidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new GrupoAcessoController(unidadeTrabalho);
            var result = controller.ObterTodos() as JsonResult;
            Assert.IsNotNull(result);

            var listaGrupoAcesso = JsonHelper.GetJsonObjectRepresentation<List<GrupoAcessoModelView>>(result);
            Assert.IsNotNull(listaGrupoAcesso);

            var rand = new Random();
            listaGrupoAcesso = listaGrupoAcesso.OrderBy(x => rand.Next()).ToList();
            return listaGrupoAcesso.Where(x => somenteAtivos ? x.Ativo : 1 == 1).ToList();
        }


        #region Testes específicos


        [TestMethod]
        public void PermissaoAcessoNoGrupoLeitora()
        {
            bool permissaoEsperada = false;
            string cpf = "28293961899";
            string nomeGrupoLeitora = "ACESSO_PORTO";

            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repVisitante = new Repositorio<Visitante>(unidTrabalho);
            var listaVisitante = repVisitante.ObterTodos().Where(v => v.PessoaFisica.CPF == cpf).ToList();
            var visitante = listaVisitante.Where(v => v.ObterLogPapel().Status == StatusPapelLog.Ativo).FirstOrDefault();

            var repGrupoAcesso = new Repositorio<GrupoAcesso>(unidTrabalho);
            var possuiPermissao = repGrupoAcesso.ObterTodos().Any(ga => ga.Ativo == true &&
                ga.GruposLeitora.Any(gl => gl.Ativo == true && gl.GrupoAcesso.Codigo == ga.Codigo && gl.GrupoLeitora.Nome == nomeGrupoLeitora) &&
                ga.Pessoas.Any(gap => gap.Ativo == true && gap.Papel.Codigo == visitante.Codigo)
            );

            if (permissaoEsperada) Assert.IsTrue(possuiPermissao);
            else Assert.IsFalse(possuiPermissao);
        }


        #endregion
    }
}
