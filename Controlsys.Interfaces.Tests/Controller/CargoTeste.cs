﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Parametros;
using Controlsys.Infra;
using Controlsys.Persistencia;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// Representa um(a) CargoTeste.
    /// </summary>
    public class CargoTeste
    {
        /// <summary>
        /// Busca aleatóriamente um cargo cadastrado na base de dados.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// Filtrar listaCargo ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Cargo.
        /// </returns>
        public static Cargo ObterCargo(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<Cargo>(unidTrabalho);
            var listaCargo = repositorio.ObterTodos().ToList();
            Assert.IsTrue(listaCargo.Any());

            var rand = new Random();
            listaCargo = listaCargo.OrderBy(x => rand.Next()).ToList();

            return listaCargo.First(x => !somenteAtivos || x.Ativo);
        }

    }
}
