﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Controlsys.Dominio.Seguranca;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Controlsys.Persistencia;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) AcaoTeste.
    /// </summary>
    [TestClass]
    public class AcaoTeste
    {
        /// <summary>
        /// Busca aleatóriamente uma Acao cadastrada na base de dados.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// Filtrar Acoes ativas.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Acao.
        /// </returns>
        public static Acao ObterAcao(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<Acao>(unidTrabalho);
            var acoes = repositorio.ObterTodos().ToList();
            Assert.IsTrue(acoes.Any());

            var rand = new Random();
            acoes = acoes.OrderBy(x => rand.Next()).ToList();

            Acao acao = null;
            foreach (var item in acoes)
            {
                acao = item;

                if (acao == null)
                    continue;
                if (somenteAtivos)
                {
                    if (acao.Ativo)
                        break;
                    acao = null;
                }
                else
                    break;
            }

            return acao;
        }

        /// <summary>
        /// Obter lista acao.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// Filtrar Acoes ativas.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;Acao&gt;
        /// </returns>
        public static List<Acao> ObterListaAcao(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<Acao>(unidTrabalho);
            var listaAcao = repositorio.ObterTodos().ToList();
            Assert.IsTrue(listaAcao.Any());

            var rand = new Random();
            listaAcao = listaAcao.OrderBy(x => rand.Next()).ToList();

            return somenteAtivos ? listaAcao.Where(x => x.Ativo).ToList() : listaAcao;
        }
    }
}
