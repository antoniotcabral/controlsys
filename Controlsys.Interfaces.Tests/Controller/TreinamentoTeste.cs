﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Controlsys.Dominio.Parametros;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Tests.Properties;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Controlsys.Persistencia;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) TreinamentoTeste.
    /// </summary>
    [TestClass]
    public class TreinamentoTeste
    {
        /// <summary>
        /// Valida incluir um novo Treinamento, informando Nome, Descricao, DiasValide.
        /// </summary>
        [TestMethod]
        public void Pode_Incluir()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new TreinamentoController(unidTrabalho);

            var rand = new Random();
            var dt = DateTime.Now;
            var data = dt.ToString("MM/dd HH:mm:ss.fff");
            var nome = "Nome-" + data;
            var descricao = "Descricao-" + data;
            var diasValid = rand.Next(1, 365);

            var treinamentoAux = new Treinamento
            {
                Nome = nome,
                Descricao = descricao,
                DiasValidade = diasValid
            };

            var result = controller.Incluir(treinamentoAux) as JsonResult;
            Assert.IsNotNull(result.Data);

            var treinamento = JsonHelper.GetJsonObjectRepresentation<Treinamento>(result);
            Assert.AreEqual(nome, treinamento.Nome);
            Assert.AreEqual(descricao, treinamento.Descricao);
            Assert.AreEqual(diasValid, treinamento.DiasValidade);
        }

        /// <summary>
        /// Valida alterar um Treinamento, modificando Nome, Descricao e DiasValidade.
        /// </summary>
        [TestMethod]
        public void Pode_Alterar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new TreinamentoController(unidTrabalho);
            var treinamentoAux = ObterTreinamento(true);
            Assert.IsNotNull(treinamentoAux);
            Assert.IsTrue(treinamentoAux.Ativo);

            var rand = new Random();
            var dt = DateTime.Now;
            var data = dt.ToString("MM/dd HH:mm:ss.fff");
            var nome = "Nome-" + data;
            var descricao = "Descricao-" + data;
            var diasValid = rand.Next(1, 365);

            treinamentoAux.Nome = nome;
            treinamentoAux.Descricao = descricao;
            treinamentoAux.DiasValidade = diasValid;

            var result = controller.Alterar(treinamentoAux) as JsonResult;
            Assert.IsNotNull(result.Data);

            var treinamento = JsonHelper.GetJsonObjectRepresentation<Treinamento>(result);
            Assert.AreEqual(nome, treinamento.Nome);
            Assert.AreEqual(descricao, treinamento.Descricao);
            Assert.AreEqual(diasValid, treinamento.DiasValidade);
        }

        /// <summary>
        /// Valida Ativar Treinamento.
        /// </summary>
        [TestMethod]
        public void Pode_Ativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new TreinamentoController(unidTrabalho);
            var lista = ObterListaTreinamento();
            Assert.IsTrue(lista.Any(), Resources.Lista_vazia);

            var listaAtivo = lista.Where(x => x.Ativo).ToList();
            var listaInativo = lista.Where(x => x.Ativo == false).ToList();
            Assert.IsTrue(listaInativo.Any(), Resources.Lista_vazia);

            Treinamento treinamentoAux = null;
            for (int i = 0; i < listaInativo.Count; i++)
            {
                if (listaAtivo.Any())
                    if (listaAtivo.Exists(x => x.Nome == listaInativo[i].Nome)) continue;
                treinamentoAux = listaInativo[i];
                break;
            }
            Assert.IsNotNull(treinamentoAux);
            Assert.IsFalse(treinamentoAux.Ativo);

            var result = controller.AtivarInativar(treinamentoAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var treinamento = result.Data as Treinamento;
            Assert.IsNotNull(treinamento);
            Assert.AreEqual(treinamentoAux.Codigo, treinamento.Codigo);
            Assert.IsTrue(treinamento.Ativo);
        }

        /// <summary>
        /// Valida Inativar Treinamento.
        /// </summary>
        [TestMethod]
        public void Pode_Inativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new TreinamentoController(unidTrabalho);
            var treinamentoAux = ObterTreinamento(true);
            Assert.IsTrue(treinamentoAux.Ativo);

            var result = controller.AtivarInativar(treinamentoAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var treinamento = result.Data as Treinamento;
            Assert.IsNotNull(treinamento);
            Assert.AreEqual(treinamentoAux.Codigo, treinamento.Codigo);
            Assert.IsFalse(treinamento.Ativo);
        }

        /// <summary>
        /// Busca aleatóriamente um Treinamento cadastrado na base de dados.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// Filtrar Treinamentos ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Treinamento.
        /// </returns>
        public static Treinamento ObterTreinamento(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<Treinamento>(unidTrabalho);
            var listaTreinamento = repositorio.ObterTodos().ToList();
            Assert.IsTrue(listaTreinamento.Any());

            var rand = new Random();
            listaTreinamento = listaTreinamento.OrderBy(x => rand.Next()).ToList();

            return listaTreinamento.First(x => !somenteAtivos || x.Ativo);
        }

        /// <summary>
        /// Obter lista treinamento.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// Filtrar Treinamentos ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;Treinamento&gt;
        /// </returns>
        public static List<Treinamento> ObterListaTreinamento(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<Treinamento>(unidTrabalho);
            var listaTreinamento = repositorio.ObterTodos().ToList();
            Assert.IsTrue(listaTreinamento.Any());

            var rand = new Random();
            listaTreinamento = listaTreinamento.OrderBy(x => rand.Next()).ToList();

            return somenteAtivos ? listaTreinamento.Where(x => x.Ativo).ToList() : listaTreinamento;
        }
    }
}
