﻿using System;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Collections.Generic;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Controlsys.Interfaces.Controllers;
using Controlsys.Infra;
using Globalsys;
using Controlsys.Dominio.Parametros;
using System.Web.Mvc;
using System.Collections.Generic;
using Controlsys.Persistencia;
using Controlsys.Interfaces.Models;

namespace Controlsys.Interfaces.Tests.Controller
{
    [TestClass]
    public class InfracaoTeste
    {
        /// <summary>
        /// Pode incluir.
        /// </summary>
        [TestMethod]
        public void Pode_Incluir()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            InfracaoController controller = new InfracaoController(unidTrabalho);
            string data = DateTime.Now.ToString("MM/dd HH:mm:ss.fff");
            Infracao infracaoNovo = new Infracao()
            {
                Nome = "Nome-" + data,
                Sigla = ("Sigla-" + data).Substring(0,20),
                Grau = (GrauInfracao)Enum.ToObject(typeof(GrauInfracao), new Random().Next(1, 4))
            };

            var result = controller.Incluir(infracaoNovo) as JsonResult;
            var infracao = JsonHelper.GetJsonObjectRepresentation<InfracaoModelView>(result);
            Assert.IsNotNull(infracao);

            Assert.AreEqual("Nome-" + data, infracao.Nome);
            Assert.AreEqual(("Sigla-" + data).Substring(0, 20), infracao.Sigla);
            
        }

        /// <summary>
        /// Pode alterar.
        /// </summary>
        [TestMethod]
        public void Pode_Alterar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            InfracaoController controller = new InfracaoController(unidTrabalho);
            JsonResult result = controller.ObterTodos() as JsonResult;
            var infracaos = JsonHelper.GetJsonObjectRepresentation<List<InfracaoModelView>>(result);
            Assert.IsNotNull(infracaos);

            Random rand = new Random();
            infracaos = infracaos.OrderBy(x => rand.Next()).ToList();

            Infracao infracaoAux = null;
            foreach (var item in infracaos)
            {
                result = controller.Selecionar(item.Codigo) as JsonResult;
                infracaoAux = JsonHelper.GetJsonObjectRepresentation<Infracao>(result);
                if (infracaoAux == null)
                    continue;
                if (infracaoAux.Ativo)
                    break;
                infracaoAux = null;
            }
            Assert.IsNotNull(infracaoAux);

            Assert.IsTrue(infracaoAux.Ativo);
            string data = DateTime.Now.ToString("MM/dd HH:mm:ss.fff");
            infracaoAux.Nome = "Nome-" + data;
            infracaoAux.Sigla = ("Sigla-" + data).Substring(0, 20);
            //var infracao = (controller.Alterar(infracaoAux) as JsonResult).Data as infracao;
            result = controller.Alterar(infracaoAux) as JsonResult;
            var infracao = JsonHelper.GetJsonObjectRepresentation<InfracaoModelView>(result);
            Assert.AreEqual("Nome-" + data, infracao.Nome);
            Assert.AreEqual(("Sigla-" + data).Substring(0, 20), infracao.Sigla);

        }

        /// <summary>
        /// Pode ativar.
        /// </summary>
        [TestMethod]
        public void Pode_Ativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            InfracaoController controller = new InfracaoController(unidTrabalho);

            var repositorio = new Repositorio<Infracao>(unidTrabalho);
            var infracaos = repositorio.ObterTodos().Where(x => x.Ativo == false).ToList();
            Assert.IsNotNull(infracaos);
            Assert.IsTrue(infracaos.Any());

            var rand = new Random();
            infracaos = new List<Infracao>(infracaos.OrderBy(x => rand.Next()));

            Infracao infracaoAux = infracaos.ToList()[0];
            Assert.IsNotNull(infracaoAux);
            Assert.IsFalse(infracaoAux.Ativo);

            var result = controller.AtivarInativar(infracaoAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            //var infracao = result.Data as infracao;
            var infracao = JsonHelper.GetJsonObjectRepresentation<Infracao>(result);
            Assert.IsNotNull(infracao);
            Assert.AreEqual(infracaoAux.Codigo, infracao.Codigo);
            Assert.IsTrue(infracao.Ativo);
        }

        /// <summary>
        /// Pode inativar.
        /// </summary>
        [TestMethod]
        public void Pode_Inativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            InfracaoController controller = new InfracaoController(unidTrabalho);

            InfracaoModelView infracaoAux = ObterInfracao(true);
            Assert.IsNotNull(infracaoAux);
            Assert.IsTrue(infracaoAux.Ativo);

            var result = controller.AtivarInativar(infracaoAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            //var infracao = result.Data as infracao;
            var infracao = JsonHelper.GetJsonObjectRepresentation<InfracaoModelView>(result);
            Assert.IsNotNull(infracao);
            Assert.AreEqual(infracaoAux.Codigo, infracao.Codigo);
            Assert.IsFalse(infracao.Ativo);
        }

        /// <summary>
        /// Obter infracao.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// true to somente ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) infracao.
        /// </returns>
        public static InfracaoModelView ObterInfracao(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            InfracaoController controller = new InfracaoController(unidTrabalho);
            JsonResult result = controller.ObterTodos(somenteAtivos) as JsonResult;
            var infracaos = JsonHelper.GetJsonObjectRepresentation<List<InfracaoModelView>>(result);
            Assert.IsNotNull(infracaos);

            Random rand = new Random();
            infracaos = infracaos.OrderBy(x => rand.Next()).ToList();

            InfracaoModelView infracaoAux = null;
            foreach (var item in infracaos)
            {
                result = controller.Selecionar(item.Codigo) as JsonResult;
                Assert.IsNotNull(result.Data);
                infracaoAux = JsonHelper.GetJsonObjectRepresentation<InfracaoModelView>(result);

                if (somenteAtivos)
                {
                    if (infracaoAux.Ativo)
                        break;
                    infracaoAux = null;
                }
                else
                    break;
            }

            return infracaoAux;
        }

        /// <summary>
        /// Obter lista infracao.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// true to somente ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;infracao&gt;
        /// </returns>
        public static List<Infracao> ObterListaInfracao(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<Infracao>(unidTrabalho);
            var infracaos = repositorio.ObterTodos().ToList();
            Assert.IsTrue(infracaos.Any());

            var rand = new Random();
            infracaos = infracaos.OrderBy(x => rand.Next()).ToList();

            return somenteAtivos ? infracaos.Where(x => x.Ativo).ToList() : infracaos;
        }
    }
}
