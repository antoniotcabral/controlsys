﻿using System;
using System.Linq;
using System.Web.Mvc;
using Controlsys.Dominio.Audit;
using Controlsys.Dominio.Seguranca;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Controlsys.Persistencia.Repositorios.Seguranca;
using Controlsys.Repositorios.Seguranca;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks.Constraints;
using System.Collections.Generic;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) RelatorioAuditoriaTeste.
    /// </summary>
    [TestClass]
    public class RelatorioAuditoriaTeste
    {
        /// <summary>
        /// Pode exportar PDF.
        /// </summary>
        [TestMethod]
        public void Pode_ExportarPDF()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RelatorioAuditoriaController controller = new RelatorioAuditoriaController(unidTrabalho);

            Usuario usuario = UsuarioTeste.ObterUsuario(true);
            Assert.IsNotNull(usuario);

            Pagina pagina = PaginaTeste.ObterPagina(true);
            Assert.IsNotNull(pagina);

            //List<string> selectedFields = new List<string>()
            //{
            //    "Codigo",
            //    "Nome",
            //    "Documento",
            //    "Pagina",
            //    "Acao",
            //    "Descricao",
            //    "DataHora"
            //};

            List<string> selectedFields = typeof(AuditoriaModelView).GetProperties().Select(pi => pi.Name).ToList();

            var result = controller.Exportar("PDF", selectedFields, usuario.Codigo, null, pagina.Codigo, null, null) as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/pdf", result.ContentType);
            Assert.AreEqual("download", result.FileDownloadName);
        }

        /// <summary>
        /// Pode exportar excel.
        /// </summary>
        [TestMethod]
        public void Pode_ExportarExcel()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RelatorioAuditoriaController controller = new RelatorioAuditoriaController(unidTrabalho);

            Usuario usuario = UsuarioTeste.ObterUsuario(true);
            Assert.IsNotNull(usuario);

            Pagina pagina = PaginaTeste.ObterPagina(true);
            Assert.IsNotNull(pagina);

            //List<string> selectedFields = new List<string>()
            //{
            //    "Codigo",
            //    "Nome",
            //    "Documento",
            //    "Pagina",
            //    "Acao",
            //    "Descricao",
            //    "DataHora"
            //};

            List<string> selectedFields = typeof(AuditoriaModelView).GetProperties().Select(pi => pi.Name).ToList();

            var result = controller.Exportar("Excel", selectedFields, usuario.Codigo, null, pagina.Codigo, null, null) as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/vnd.ms-excel", result.ContentType);
            Assert.AreEqual("download", result.FileDownloadName);
        }

        /// <summary>
        /// Pode exportar word.
        /// </summary>
        [TestMethod]
        public void Pode_ExportarWord()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RelatorioAuditoriaController controller = new RelatorioAuditoriaController(unidTrabalho);

            Usuario usuario = UsuarioTeste.ObterUsuario(true);
            Assert.IsNotNull(usuario);

            Pagina pagina = PaginaTeste.ObterPagina(true);
            Assert.IsNotNull(pagina);

            //List<string> selectedFields = new List<string>()
            //{
            //    "Codigo",
            //    "Nome",
            //    "Documento",
            //    "Pagina",
            //    "Acao",
            //    "Descricao",
            //    "DataHora"
            //};

            List<string> selectedFields = typeof(AuditoriaModelView).GetProperties().Select(pi => pi.Name).ToList();

            var result = controller.Exportar("Word", selectedFields, usuario.Codigo, null, pagina.Codigo, null, null) as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/msword", result.ContentType);
            Assert.AreEqual("download", result.FileDownloadName);
        }
    }
}
