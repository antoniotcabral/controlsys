﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Controlsys.Persistencia;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) ModeloCrachaTeste.
    /// </summary>
    [TestClass]
    public class ModeloCrachaTeste
    {
        /// <summary>
        /// Pode incluir.
        /// </summary>
        [TestMethod]
        public void Pode_Incluir()
        {
            var data = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            var modeloCrachaAux = new ModeloCracha();
            modeloCrachaAux.Nome = "Nome-" + data;
            modeloCrachaAux.TipoPapel = Global.ObterEnumAleatorio<TipoPapel>();
            modeloCrachaAux.Html = "<div class=\"logo\" id=\"ext-gen1165\" style=\"position: absolute; right: auto; left: 25px; top: 250px; background-color: rgb(255, 255, 236);\"><img src=\"/Content/img/bg/logotk.fw.png\" width=\"160px\" height=\"40px\"></div>";

            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new ModeloCrachaController(unidTrabalho);

            var result = controller.Incluir(modeloCrachaAux) as JsonResult;
            Assert.IsNotNull(result);

            var modeloCracha = result.Data as ModeloCrachaModelView;
            Assert.IsNotNull(modeloCracha);
            Assert.AreEqual(modeloCrachaAux.Nome.ToLower(), modeloCracha.Nome.ToLower());
            Assert.AreEqual(modeloCrachaAux.TipoPapel.ObterDescricaoEnum().ToLower(), modeloCracha.TipoPapel.ToLower());
            Assert.IsTrue(modeloCracha.Ativo);
        }

        /// <summary>
        /// Pode alterar.
        /// </summary>
        [TestMethod]
        public void Pode_Alterar()
        {
            ModeloCracha modeloCrachaAux = ObterModeloCracha(true);
            Assert.IsNotNull(modeloCrachaAux);

            var data = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            modeloCrachaAux.Nome = "Nome-" + data;

            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new ModeloCrachaController(unidTrabalho);

            var result = controller.Alterar(modeloCrachaAux) as JsonResult;
            Assert.IsNotNull(result);

            var modeloCracha = result.Data as ModeloCrachaModelView;
            Assert.IsNotNull(modeloCracha);
            Assert.AreEqual(modeloCrachaAux.Codigo, modeloCracha.Codigo);
            Assert.AreEqual(modeloCrachaAux.Nome.ToLower(), modeloCracha.Nome.ToLower());
            Assert.AreEqual(modeloCrachaAux.TipoPapel.ObterDescricaoEnum().ToLower(), modeloCracha.TipoPapel.ToLower());
            Assert.AreEqual(modeloCrachaAux.Ativo, modeloCracha.Ativo);
        }

        /// <summary>
        /// Pode selecionar.
        /// </summary>
        [TestMethod]
        public void Pode_Selecionar()
        {
            ModeloCracha modeloCrachaAux = ObterModeloCracha(true);
            Assert.IsNotNull(modeloCrachaAux);

            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new ModeloCrachaController(unidTrabalho);

            var result = controller.Selecionar(modeloCrachaAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var modeloCracha = result.Data as ModeloCrachaModelView;
            Assert.IsNotNull(modeloCracha);
            Assert.AreEqual(modeloCrachaAux.Codigo, modeloCracha.Codigo);
            Assert.AreEqual(modeloCrachaAux.Nome.ToLower(), modeloCracha.Nome.ToLower());
            Assert.AreEqual(modeloCrachaAux.TipoPapel.ObterDescricaoEnum().ToLower(), modeloCracha.TipoPapel.ToLower());
            Assert.AreEqual(modeloCrachaAux.Ativo, modeloCracha.Ativo);
        }

        /// <summary>
        /// Pode ativar.
        /// </summary>
        [TestMethod]
        public void Pode_Ativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new ModeloCrachaController(unidTrabalho);
            var lista = ObterListaModeloCracha();
            Assert.IsTrue(lista.Any());

            lista = lista.Where(x => x.Ativo == false).ToList();
            Assert.IsTrue(lista.Any());

            var modeloCrachaAux = lista[0];
            Assert.IsFalse(modeloCrachaAux.Ativo);

            var result = controller.AtivarInativar(modeloCrachaAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var modeloCracha = result.Data as ModeloCrachaModelView;
            Assert.IsNotNull(modeloCracha);
            Assert.AreEqual(modeloCrachaAux.Codigo, modeloCracha.Codigo);
            Assert.IsTrue(modeloCracha.Ativo);
        }

        /// <summary>
        /// Pode inativar.
        /// </summary>
        [TestMethod]
        public void Pode_Inativar()
        {
            ModeloCracha modeloCrachaAux = ObterModeloCracha(true);
            Assert.IsNotNull(modeloCrachaAux);
            Assert.IsTrue(modeloCrachaAux.Ativo);

            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new ModeloCrachaController(unidTrabalho);

            var result = controller.AtivarInativar(modeloCrachaAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var modeloCracha = result.Data as ModeloCrachaModelView;
            Assert.IsNotNull(modeloCracha);
            Assert.AreEqual(modeloCrachaAux.Codigo, modeloCracha.Codigo);
            Assert.IsFalse(modeloCracha.Ativo);
        }

        /// <summary>
        /// Busca aleatóriamente um Modelo Cracha cadastrado na base de dados.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// Filtrar Modelo Cracha ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ModeloCracha.
        /// </returns>
        public static ModeloCracha ObterModeloCracha(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<ModeloCracha>(unidTrabalho);
            var listaModeloCracha = repositorio.ObterTodos().ToList();
            Assert.IsNotNull(listaModeloCracha);
            Assert.IsTrue(listaModeloCracha.Any());

            var rand = new Random();
            listaModeloCracha = listaModeloCracha.OrderBy(x => rand.Next()).ToList();

            return listaModeloCracha.FirstOrDefault(x => !somenteAtivos || x.Ativo);
        }

        /// <summary>
        /// Obtem lista com ordém aleatória de um Modelo Cracha cadastrado na base de dados.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// Filtrar Modelo Cracha ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;ModeloCracha&gt;
        /// </returns>
        public static List<ModeloCracha> ObterListaModeloCracha(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<ModeloCracha>(unidTrabalho);
            var listaModeloCracha = repositorio.ObterTodos().ToList();
            Assert.IsNotNull(listaModeloCracha);
            Assert.IsTrue(listaModeloCracha.Any());

            var rand = new Random();
            listaModeloCracha = listaModeloCracha.OrderBy(x => rand.Next()).ToList();

            return somenteAtivos ? listaModeloCracha.Where(x => x.Ativo == true).ToList() : listaModeloCracha;
        }
    }
}
