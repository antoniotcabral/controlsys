﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Globalsys;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using System.Web.Mvc;
using System.Collections.Generic;
using Ext.Net;
using Ext.Net.MVC;
using System.Data.SqlTypes;
using System.Linq;

namespace Controlsys.Interfaces.Tests.Controller
{
    [TestClass]
    public class RelatorioVisitantePresenteTeste
    {
        [TestMethod]
        public void Pode_Pesquisar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RelatorioVisitantePresenteController controller = new RelatorioVisitantePresenteController(unidTrabalho);

            var x = new Dictionary<string, object>() { };
            x.Add("start", (int)0);
            x.Add("limit", (int)25);


            var result = controller.Pesquisar(new StoreRequestParameters(x), new DateTime(2015, 03, 03), new TimeSpan(15, 00, 00)) as StoreResult;
            Assert.IsNotNull(result);

            var visitantesPresentes = result.Data as List<RelatorioVisitantePresenteModelView>;
            Assert.IsNotNull(visitantesPresentes);

        }

        [TestMethod]
        public void Pode_ExportarPDF()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RelatorioVisitantePresenteController controller = new RelatorioVisitantePresenteController(unidTrabalho);

            List<string> selectedFields = typeof(RelatorioVisitantePresenteModelView).GetProperties().Select(pi => pi.Name).ToList();

            var result = controller.Exportar(selectedFields, "PDF", new DateTime(2015, 03, 03), new TimeSpan(15, 00, 00)) as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/pdf", result.ContentType);
            Assert.AreEqual("download.pdf", result.FileDownloadName);
        }

        [TestMethod]
        public void Pode_ExportarExcel()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RelatorioVisitantePresenteController controller = new RelatorioVisitantePresenteController(unidTrabalho);

            List<string> selectedFields = typeof(RelatorioVisitantePresenteModelView).GetProperties().Select(pi => pi.Name).ToList();

            var result = controller.Exportar(selectedFields, "Excel", new DateTime(2015, 03, 03), new TimeSpan(15, 00, 00)) as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/vnd.ms-excel", result.ContentType);
            Assert.AreEqual("download.xls", result.FileDownloadName);
        }

        [TestMethod]
        public void Pode_ExportarWord()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RelatorioVisitantePresenteController controller = new RelatorioVisitantePresenteController(unidTrabalho);

            List<string> selectedFields = typeof(RelatorioVisitantePresenteModelView).GetProperties().Select(pi => pi.Name).ToList();

            var result = controller.Exportar(selectedFields, "Word", new DateTime(2015, 03, 03), new TimeSpan(15, 00, 00)) as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/msword", result.ContentType);
            Assert.AreEqual("download.doc", result.FileDownloadName);
        }
    }
}
