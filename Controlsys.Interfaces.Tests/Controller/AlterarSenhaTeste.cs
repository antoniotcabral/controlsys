﻿using System;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using Controlsys.Dominio.Seguranca;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Repositorios.Seguranca;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Controlsys.Persistencia;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) AlterarSenhaTeste.
    /// </summary>
    [TestClass]
    public class AlterarSenhaTeste
    {
        /// <summary>
        /// Altera um registro de alterarSenha.
        /// </summary>
        ///
        /// <exception cref="AssertFailedException">
        /// Lançada quando uma condição de erro Assert Failed ocorre.
        /// </exception>
        [TestMethod]
        public void Alterar()
        {
            string cpf = UsuarioTeste.ObterUsuario().PessoaFisica.CPF; // 385.700.316-29
            const string senha1 = "Abcd1234@";
            const string senha2 = "@4321dcbA";
            string senhaAtual = null;
            string novaSenha = null;

            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(unidTrabalho);
            Usuario usuarioAux = repUsuario.ObterPorLogin(cpf, Globalsys.Util.Tools.CriptografarMD5(senha1));
            if (usuarioAux != null)
            {
                senhaAtual = senha1;
                novaSenha = senha2;
            }
            else
            {
                senhaAtual = senha2;
                novaSenha = senha1;
                usuarioAux = repUsuario.ObterPorLogin(cpf, Globalsys.Util.Tools.CriptografarMD5(senha2));
            }
            Assert.IsNotNull(usuarioAux);


            var fakeHttpContext = new Mock<HttpContextBase>();
            var fakeIdentity = new GenericIdentity(usuarioAux.Login);
            var principal = new GenericPrincipal(fakeIdentity, null);

            fakeHttpContext.Setup(t => t.User).Returns(principal);
            var controllerContext = new Mock<ControllerContext>();
            controllerContext.Setup(t => t.HttpContext).Returns(fakeHttpContext.Object);


            var controller = new AlterarSenhaController(unidTrabalho);
            //Set your controller ControllerContext with fake context
            controller.ControllerContext = controllerContext.Object;
            try
            {
                controller.Alterar(novaSenha, novaSenha, senhaAtual);
                //var usuario = repUsuario.ObterPorLogin(cpf, Globalsys.Util.Tools.CriptografarMD5(novaSenha));
                //Assert.IsNotNull(usuario);
                //Assert.AreEqual(cpf, usuario.Login);
            }
            catch (NullReferenceException)
            {

            }
            catch (Exception ex)
            {
                throw new AssertFailedException(ex.Message);
            }
        }
    }
}
