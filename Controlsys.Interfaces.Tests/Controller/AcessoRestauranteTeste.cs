﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Controlsys.Interfaces.Tests.Controller
{
    [TestClass]
    public class AcessoRestauranteTeste
    {
        [TestMethod]
        public void Incluir()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();


            #region [Pessoa]
            var pessoaController = new PessoaController(unidTrabalho);
            var pessoaResult = pessoaController.ObterTodos(true) as JsonResult;
            var pessoaList = JsonHelper.GetJsonObjectRepresentation<List<PessoaModelView>>(pessoaResult);
            Assert.IsNotNull(pessoaList);
            Assert.IsTrue(pessoaList.Count > 0);

            var pessoaModelView = pessoaList.OrderBy(x => new Random().Next()).FirstOrDefault();
            Assert.IsNotNull(pessoaModelView);
            Assert.IsNotNull(pessoaModelView.Codigo);
            
            var pessoaFisicaResult = pessoaController.Selecionar(pessoaModelView.Codigo) as JsonResult;
            var pessoaFisica = JsonHelper.GetJsonObjectRepresentation<PessoaFisicaModelView>(pessoaFisicaResult);
            Assert.IsNotNull(pessoaFisica);
            Assert.IsTrue(pessoaFisica.PapelModelView.CodigoPapel > 0);
            #endregion


            #region [Restaurante]
            var restauranteController = new RestauranteController(unidTrabalho);
            var restauranteResult = restauranteController.ObterTodos(true) as JsonResult;
            var restauranteList = JsonHelper.GetJsonObjectRepresentation<List<RestauranteModelView>>(restauranteResult);
            Assert.IsNotNull(restauranteList);
            Assert.IsTrue(restauranteList.Count > 0);

            var restauranteModelView = restauranteList.OrderBy(x => new Random().Next()).FirstOrDefault();
            Assert.IsNotNull(restauranteModelView);
            #endregion


            #region [Tipo Alimentacao]
            var tipoAlimentacaoController = new TipoAlimentacaoController(unidTrabalho);
            var tipoAlimentacaoResult = tipoAlimentacaoController.ObterTodos(true) as JsonResult;
            var tipoAlimentacaoList = JsonHelper.GetJsonObjectRepresentation<List<TipoAlimentacaoRestModelView>>(tipoAlimentacaoResult);
            Assert.IsNotNull(tipoAlimentacaoList);
            Assert.IsTrue(tipoAlimentacaoList.Count > 0);

            var tipoAlimentacaoModelView = tipoAlimentacaoList.Where(
                x => restauranteModelView.TiposAlimentacao.Select(ta => ta.Codigo)
                .Contains(x.Codigo)).OrderBy(x => new Random().Next()).ToList();
            Assert.IsNotNull(tipoAlimentacaoModelView);
            Assert.IsTrue(tipoAlimentacaoModelView.Count > 0);

            var tipoAlimentacaoRestaurantes = new List<TipoAlimentacaoRestaurante>();
            for (int i = 0; i < new Random().Next(1, tipoAlimentacaoModelView.Count); i++)
            {
                var reg = new TipoAlimentacaoRestaurante
                {
                    Restaurante = new Restaurante
                    {
                        Codigo = restauranteModelView.Codigo,
                        Nome = restauranteModelView.Nome
                    },
                    TipoAlimentacao = new TipoAlimentacao
                    {
                        Codigo = tipoAlimentacaoModelView[i].Codigo,
                        Nome = tipoAlimentacaoModelView[i].Nome
                    }
                };
                tipoAlimentacaoRestaurantes.Add(reg);
            }
            #endregion
            

            var acessoRestauranteController = new AcessoRestauranteController(unidTrabalho);
            var acessoRestaurante = new AcessoRestaurante();
            bool crachaMestre = new Random().Next(0, 1) != 0;
            bool validarAH = new Random().Next(0, 1) != 0;
            var result = acessoRestauranteController.Incluir(
                acessoRestaurante, tipoAlimentacaoRestaurantes.ToArray(), pessoaFisica.PapelModelView.CodigoPapel, crachaMestre, validarAH) as JsonResult;
            var registroGerado = JsonHelper.GetJsonObjectRepresentation<AcessoRestauranteModelView>(result);

            Assert.IsNotNull(registroGerado);
            Assert.AreEqual(pessoaModelView.Nome, registroGerado.Nome);
            Assert.AreEqual(crachaMestre, registroGerado.CrachaMestre);
            Assert.IsTrue(registroGerado.Ativo);
            //TODO: Validar o Restaurante
            //Assert.AreEqual(restauranteModelView.Codigo, registroGerado.TipoAlimentacaoRest.Select(ta => ta.CodRestaurante).FirstOrDefault());
            //TODO: Validar os TipoAlimentacao
            //CollectionAssert.Contains(
            //    new[] { tipoAlimentacaoRestaurantes.Select(ta => ta.TipoAlimentacao.Codigo) },
            //    registroGerado.TipoAlimentacaoRest.Select(ta => ta.CodTipoAlimento)
            //);

            unidTrabalho.Rollback();
        }

        [TestMethod]
        public void Alterar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();


            #region [AcessoRestaurante]
            var acessoRestauranteController = new AcessoRestauranteController(unidTrabalho);
            var acessoRestauranteResult = acessoRestauranteController.ObterTodos(true) as JsonResult;
            var acessoRestauranteList = JsonHelper.GetJsonObjectRepresentation<List<AcessoRestauranteModelView>>(acessoRestauranteResult);
            Assert.IsNotNull(acessoRestauranteList);
            Assert.IsTrue(acessoRestauranteList.Count > 0);

            var acessoRestauranteModelView = acessoRestauranteList.OrderBy(x => new Random().Next()).FirstOrDefault();
            Assert.IsNotNull(acessoRestauranteModelView);
            Assert.IsNotNull(acessoRestauranteModelView.Codigo);
            Assert.IsTrue(acessoRestauranteModelView.Ativo);
            #endregion


            #region [Restaurante]
            var restauranteController = new RestauranteController(unidTrabalho);
            var restauranteResult = restauranteController.ObterTodos(true) as JsonResult;
            var restauranteList = JsonHelper.GetJsonObjectRepresentation<List<RestauranteModelView>>(restauranteResult);
            Assert.IsNotNull(restauranteList);
            Assert.IsTrue(restauranteList.Count > 0);

            var restauranteModelView = restauranteList.OrderBy(x => new Random().Next()).FirstOrDefault();
            Assert.IsNotNull(restauranteModelView);
            #endregion


            #region [Tipo Alimentacao]
            var tipoAlimentacaoController = new TipoAlimentacaoController(unidTrabalho);
            var tipoAlimentacaoResult = tipoAlimentacaoController.ObterTodos(true) as JsonResult;
            var tipoAlimentacaoList = JsonHelper.GetJsonObjectRepresentation<List<TipoAlimentacaoRestModelView>>(tipoAlimentacaoResult);
            Assert.IsNotNull(tipoAlimentacaoList);
            Assert.IsTrue(tipoAlimentacaoList.Count > 0);

            var tipoAlimentacaoModelView = tipoAlimentacaoList.Where(
                x => restauranteModelView.TiposAlimentacao.Select(ta => ta.Codigo)
                .Contains(x.Codigo)).OrderBy(x => new Random().Next()).ToList();
            Assert.IsNotNull(tipoAlimentacaoModelView);
            Assert.IsTrue(tipoAlimentacaoModelView.Count > 0);

            var tipoAlimentacaoRestaurantes = new List<TipoAlimentacaoRestaurante>();
            for (int i = 0; i < new Random().Next(1, tipoAlimentacaoModelView.Count); i++)
            {
                var reg = new TipoAlimentacaoRestaurante
            {
                    Restaurante = new Restaurante
                {
                        Codigo = restauranteModelView.Codigo,
                        Nome = restauranteModelView.Nome
                    },
                    TipoAlimentacao = new TipoAlimentacao
                    {
                        Codigo = tipoAlimentacaoModelView[i].Codigo,
                        Nome = tipoAlimentacaoModelView[i].Nome
                    }
                };
                tipoAlimentacaoRestaurantes.Add(reg);
            }
            #endregion


            var acessoRestaurante = new AcessoRestaurante();
            acessoRestaurante.Codigo = acessoRestauranteModelView.Codigo;
            bool crachaMestre = new Random().Next(0, 1) != 0;
            bool validarAH = new Random().Next(0, 1) != 0;
            var result = acessoRestauranteController.Alterar(
                acessoRestaurante, tipoAlimentacaoRestaurantes.ToArray(), tipoAlimentacaoRestaurantes.ToArray(), crachaMestre, validarAH) as JsonResult;
            var registroGerado = JsonHelper.GetJsonObjectRepresentation<AcessoRestauranteModelView>(result);

            Assert.IsNotNull(registroGerado);
            Assert.AreEqual(acessoRestauranteModelView.Codigo, registroGerado.Codigo);
            Assert.AreEqual(crachaMestre, registroGerado.CrachaMestre);
            Assert.IsTrue(registroGerado.Ativo);
            //TODO: Validar o Restaurante
            //Assert.AreEqual(restauranteModelView.Codigo, registroGerado.TipoAlimentacaoRest.Select(ta => ta.CodRestaurante).FirstOrDefault());
            //TODO: Validar os TipoAlimentacao
            //CollectionAssert.Contains(
            //    new[] { tipoAlimentacaoRestaurantes.Select(ta => ta.TipoAlimentacao.Codigo) },
            //    registroGerado.TipoAlimentacaoRest.Select(ta => ta.CodTipoAlimento)
            //);

            unidTrabalho.Rollback();
        }

        [TestMethod]
        public void AtivarInativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();

            var controller = new AcessoRestauranteController(unidTrabalho);

            #region [ObterTodos]
            var result1 = controller.ObterTodos() as JsonResult;
            var lista = JsonHelper.GetJsonObjectRepresentation<List<AcessoRestauranteModelView>>(result1);

            Assert.IsNotNull(lista);
            Assert.IsTrue(lista.Count > 0);
            #endregion

            var registro = lista.OrderBy(x => new Random().Next()).FirstOrDefault();
            Assert.IsNotNull(registro);

            var result = controller.AtivarInativar(registro.Codigo) as JsonResult;
            var registroAlterado = JsonHelper.GetJsonObjectRepresentation<AcessoRestauranteModelView>(result);

            Assert.IsNotNull(registro);
            Assert.AreEqual(registro.Codigo, registroAlterado.Codigo);
            Assert.AreNotEqual(registro.Ativo, registroAlterado.Ativo);

            unidTrabalho.Rollback();
        }

        [TestMethod]
        public void Selecionar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new AcessoRestauranteController(unidTrabalho);

            #region [ObterTodos]
            var result1 = controller.ObterTodos() as JsonResult;
            var lista = JsonHelper.GetJsonObjectRepresentation<List<AcessoRestauranteModelView>>(result1);

            Assert.IsNotNull(lista);
            Assert.IsTrue(lista.Count > 0);
            #endregion

            var registro = lista.OrderBy(x => new Random().Next()).FirstOrDefault();
            Assert.IsNotNull(registro, "Nenhum registro encontrado para ser utilizado como referência de teste no Selecionar");

            var result2 = controller.Selecionar(registro.Codigo) as JsonResult;
            var registroSelecionado = JsonHelper.GetJsonObjectRepresentation<AcessoRestauranteModelView>(result2);

            Assert.IsNotNull(registroSelecionado, "Selecionar retornando nulo");
            Assert.AreEqual(registro.Codigo, registroSelecionado.Codigo, "Codigo diferente do valor utilizado como referência");
            Assert.AreEqual(registro.Nome, registroSelecionado.Nome, "Nome diferente do valor utilizado como referência");
        }

        [TestMethod]
        public void ObterTodos()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();

            var controller = new AcessoRestauranteController(unidTrabalho);
            var result = controller.ObterTodos() as JsonResult;
            var lista = JsonHelper.GetJsonObjectRepresentation<List<AcessoRestauranteModelView>>(result);

            Assert.IsNotNull(lista);
            Assert.IsTrue(lista.Count > 0);
        }
    }
}
