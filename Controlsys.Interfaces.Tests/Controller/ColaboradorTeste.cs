﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Controlsys.Dominio.Enderecos;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Controlsys.Persistencia;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Rhino.Mocks;
using MockRepository = Rhino.Mocks.MockRepository;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) ColaboradorTeste.
    /// </summary>
    [TestClass]
    public class ColaboradorTeste
    {
        /// <summary>
        /// Valida incluir um novo Colaborador.
        /// </summary>
        [TestMethod]
        public void Pode_Incluir()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new ColaboradorController(unidTrabalho);

            var dt = DateTime.Now;
            var rand = new Random();
            var randValue = rand.Next(1, 9999);

            #region [PessoaFisica]
            #region [Estado]
            var repEstado = new Repositorio<Estado>(unidTrabalho);
            var estados = repEstado.ObterTodos().ToList();
            Assert.IsTrue(estados.Any());
            estados = estados.OrderBy(x => rand.Next()).ToList();
            var estado = estados[0];
            #endregion
            #region [Cidade]
            var repCidade = new Repositorio<Cidade>(unidTrabalho);
            var cidades = repCidade.ObterTodos().ToList();
            Assert.IsTrue(cidades.Any());
            cidades = cidades.Where(x => x.Estado.Codigo == estado.Codigo).OrderBy(x => rand.Next()).ToList();
            Assert.IsTrue(cidades.Any());
            var cidade = cidades[0];
            #endregion
            #region [Telefones]
            var telefones = new List<Telefone>()
            {
                new Telefone()
                {
                    TipoTelefone = TipoTelefone.Residencial,
                    TelefoneNum = Global.GerarTelefone(TipoTelefone.Residencial)
                },
                new Telefone()
                {
                    TipoTelefone = TipoTelefone.Emergencial,
                    TelefoneNum = Global.GerarTelefone(TipoTelefone.Emergencial)
                },
                new Telefone()
                {
                    TipoTelefone = TipoTelefone.Celular,
                    TelefoneNum = Global.GerarTelefone(TipoTelefone.Residencial)
                }

            };
            #endregion
            #region [Emails]
            var emails = new List<Email>();
            for (int i = 0; i < rand.Next(1, 3); i++)
            {
                emails.Add(new Email()
                {
                    EmailDesc = Global.GerarEmail()
                });
            }
            #endregion
            #region [Treinamento]
            var countAux = rand.Next(1, 3);
            var listaTreinamento = new List<TreinamentoPessoa>();
            for (int i = 0; i < countAux; i++)
            {
                var treinamento = TreinamentoTeste.ObterTreinamento(true);
                if (listaTreinamento.Exists(x => x.Treinamento == treinamento))
                {
                    countAux++;
                    continue;
                }
                var dtRealizacao = dt.AddDays(rand.Next(-365, -1));
                listaTreinamento.Add(new TreinamentoPessoa()
                {
                    Treinamento = treinamento,
                    DataRealizacao = dtRealizacao,
                    DataValidade = dtRealizacao.AddYears(1)
                });
            }
            #endregion
            #region [Documentações]
            var repCheckList = new Repositorio<ChecklistDocumento>(unidTrabalho);
            var listaCheckListDocumento = repCheckList.ObterTodos().ToList();
            listaCheckListDocumento = listaCheckListDocumento.Where(x => x.TipoCadastro == TipoCadastro.Colaborador).ToList();
            Assert.IsTrue(listaCheckListDocumento.Any());

            var listaDocumento = new List<DocumentoMidiaModelView>();
            var listaTipoDocumento = new List<TipoDocumento>();
            foreach (var checkListDocumento in listaCheckListDocumento)
            {
                foreach (var checkListTipoDocumento in checkListDocumento.TiposDocumento)
                {
                    var tipoDocumento = checkListTipoDocumento.TipoDocumento;
                    if (listaTipoDocumento.Contains(tipoDocumento))
                        continue;
                    listaTipoDocumento.Add(tipoDocumento);
                    listaDocumento.Add(new DocumentoMidiaModelView()
                    {
                        Documento = new Documento
                        {
                            TipoDocumento = tipoDocumento,
                            Descricao = "Descrição-" + randValue,
                            Status = Global.ObterEnumAleatorio<StatusDocumento>()
                        },
                        Midias = null
                    });
                }
            }
            Assert.IsTrue(listaDocumento.Any());
            #endregion

            var pessoaFisica = new PessoaFisica()
            {
                CPF = Global.GerarCpf(true),
                Passaporte = "Passaport" + randValue,
                Nome = "Nome" + randValue,
                Apelido = "Apelido" + randValue,
                Email = Global.GerarEmail(),
                DataNascimento = dt.AddDays(rand.Next(-21900, -7300)),
                DataVisto = dt.AddDays(rand.Next(-365, -1)),
                DataValidadeVisto = dt.AddDays(rand.Next(1, 365)),
                Sexo = Global.ObterEnumAleatorio<Sexo>(),

                NomeMae = "Mae" + randValue,
                NomePai = "Pai" + randValue,
                RNE = "RNE" + randValue,
                DataExpedicaoRNE = dt.AddDays(rand.Next(1, 365)),
                EstadoCivil = Global.ObterEnumAleatorio<EstadoCivil>(),
                Escolaridade = Global.ObterEnumAleatorio<Escolaridade>(),
                TipoSanguineo = Global.ObterEnumAleatorio<TipoSanguineo>(),
                Nacionalidade = "Brasileiro",
                Naturalidade = cidade,

                RG = randValue.ToString(),
                RGOrgaoEmissor = "SSP",
                RGOrgaoEmissorUF = estado,
                RGDataEmissao = dt.AddDays(rand.Next(-5475, -365)),

                TituloEleitor = rand.Next(100, 999999),
                TituloEleitorSecao = randValue,
                TituloEleitorZona = randValue,
                TituloEleitorCidade = cidade,

                CTPS = randValue,
                CTPSData = dt.AddDays(rand.Next(-999, -365)),
                CTPSSerie = "CTPS" + randValue,
                CTPSEstado = estado,

                CNH = "cnh-" + randValue,
                CNHCategoria = Global.ObterBoolean() ? CategoriaCNH.A : CategoriaCNH.AB,
                CNHDataValidade = dt.AddDays(rand.Next(365, 999)),

                CertificadoReservista = randValue.ToString(),
                CertificadoReservistaCat = "CAT-" + randValue,

                Endereco = EnderecoTeste.ObterEnderecoNovo(),                
                //Documentos = 
                Telefones = telefones,
                Emails = emails
            }; 
            #endregion

            #region [Colaborador]
            #region [Cargo]
            var cargo = new Cargo()
            {
                Descricao = "Descrição-" + randValue,
                Nome = "Nome-" + randValue
            };
            #endregion
            #region [ASO]
            var listaASO = new List<ASO>();
            for (int i = 1; i <= rand.Next(1, 3); i++)
            {
                var data = dt.AddYears(-(i * 2));
                listaASO.Add(new ASO()
                {
                    DataRealizacao = data,
                    DataVencimento = data.AddYears(1)
                });
            }
            #endregion

            var colaboradorAux = new Colaborador()
            {
                PIS = randValue.ToString(),
                AntiPassBack = Global.ObterBoolean(),
                PessoaJuridica = Global.ObterBoolean(),
                ConselhoProfissional = "ConProf-" + randValue,
                RegistroProfissional = "RegProm-" + randValue,
                Observacao = "Observação-" + randValue,
                Empresa = EmpresaTeste.ObterEmpresa(true),
                DataAdmissao = dt.AddDays(rand.Next(-7, 7)),
                HoristaMensalista = Global.ObterEnumAleatorio<HoristaMensalista>(),
                Cargo = cargo,
                Salario = rand.Next(724, 3620),
                ASOs = listaASO,
                PessoaFisica = pessoaFisica
            }; 
            #endregion

            //TODO: Fazer upload da foto

            var result = controller.Incluir(colaboradorAux, null, listaTreinamento, listaDocumento, null, null,null) as JsonResult;
            Assert.IsNotNull(result);

            var colaborador = result.Data as ColaboradorModelView;
            Assert.IsNotNull(colaborador);

            Assert.AreEqual(pessoaFisica.CPF, colaborador.CPF.Replace(".", "").Replace("-", ""));
            Assert.AreEqual(pessoaFisica.TipoSanguineo, colaborador.TipoSanguineo);
            Assert.AreEqual(pessoaFisica.Sexo.ToString(), colaborador.Sexo);
            Assert.AreEqual(pessoaFisica.EstadoCivil, colaborador.EstadoCivil);
            //Assert.AreEqual(pessoaFisica.DataExpedicaoRNE, colaborador.DataExpedicaoRNE);
            //Assert.AreEqual(pessoaFisica.DataNascimento, colaborador.DataNascimento);
            Assert.AreEqual(pessoaFisica.NomePai, colaborador.NomePai);
            Assert.AreEqual(pessoaFisica.NomeMae, colaborador.NomeMae);
            Assert.AreEqual(pessoaFisica.RG, colaborador.RG);
            Assert.AreEqual(pessoaFisica.RGOrgaoEmissor, colaborador.RGOrgaoEmissor);
            Assert.AreEqual(pessoaFisica.RGOrgaoEmissorUF.Codigo, colaborador.RGOrgaoEmissorUF);
            //Assert.AreEqual(pessoaFisica.RGDataEmissao, colaborador.RGDataEmissao);
            Assert.AreEqual(pessoaFisica.Passaporte, colaborador.Passaporte);
            Assert.AreEqual(pessoaFisica.Naturalidade.Codigo, colaborador.Naturalidade);
            Assert.AreEqual(pessoaFisica.CTPS, colaborador.CTPS);
            Assert.AreEqual(pessoaFisica.CTPSSerie, colaborador.CTPSSerie);
            Assert.AreEqual(pessoaFisica.CTPSEstado.Codigo, colaborador.CTPSEstado);
            Assert.AreEqual(pessoaFisica.CNH, colaborador.CNH);
            Assert.AreEqual(pessoaFisica.CNHCategoria, colaborador.CNHCategoria);
            Assert.AreEqual(pessoaFisica.TituloEleitor, colaborador.TituloEleitor);
            Assert.AreEqual(pessoaFisica.TituloEleitorCidade.Codigo, colaborador.TituloEleitorCidade);
            Assert.AreEqual(pessoaFisica.CertificadoReservista, colaborador.CertificadoReservista);
            Assert.AreEqual(pessoaFisica.CertificadoReservistaCat, colaborador.CertificadoReservistaCat);
            Assert.AreEqual(pessoaFisica.RNE, colaborador.RNE);
            Assert.AreEqual(pessoaFisica.Escolaridade, colaborador.Escolaridade);

            // TODO: Validar lista de Emails
            // TODO: Validar lista de Papeis
        }

        /// <summary>
        /// Valida alterar um Colaborador.
        /// </summary>
        [TestMethod]
        public void Pode_Alterar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new ColaboradorController(unidTrabalho);

            var dt = DateTime.Now;
            var rand = new Random();
            var randValue = rand.Next(1, 9999);

            #region [Estado]
            var repEstado = new Repositorio<Estado>(unidTrabalho);
            var estados = repEstado.ObterTodos().ToList();
            Assert.IsTrue(estados.Any());
            estados = estados.OrderBy(x => rand.Next()).ToList();
            var estado = estados[0];
            #endregion
            #region [Cidade]
            var repCidade = new Repositorio<Cidade>(unidTrabalho);
            var cidades = repCidade.ObterTodos().ToList();
            Assert.IsTrue(cidades.Any());
            cidades = cidades.Where(x => x.Estado.Codigo == estado.Codigo).OrderBy(x => rand.Next()).ToList();
            Assert.IsTrue(cidades.Any());
            var cidade = cidades[0];
            #endregion
            #region [Telefones]
            var telefones = new List<Telefone>()
            {
                new Telefone()
                {
                    TipoTelefone = TipoTelefone.Celular,
                    TelefoneNum = Global.GerarTelefone(TipoTelefone.Celular)
                },
                new Telefone()
                {
                    TipoTelefone = TipoTelefone.Emergencial,
                    TelefoneNum = Global.GerarTelefone(TipoTelefone.Emergencial)
                },
                new Telefone()
                {
                    TipoTelefone = TipoTelefone.Celular,
                    TelefoneNum = Global.GerarTelefone(TipoTelefone.Residencial)
                },
                new Telefone()
                {
                    TipoTelefone = TipoTelefone.Empresarial,
                    TelefoneNum = Global.GerarTelefone(TipoTelefone.Empresarial)
                }

            };
            #endregion
            #region [Emails]
            var emails = new List<Email>();
            for (int i = 0; i < rand.Next(1, 3); i++)
            {
                emails.Add(new Email()
                {
                    EmailDesc = Global.GerarEmail()
                });
            }
            #endregion
            /*
            var colaboradorAux = ObterColaborador(true);
            Assert.IsNotNull(colaboradorAux);

            var repPF = new Repositorio<Colaborador>(unidTrabalho);
            var pessoaFisica = repPF.ObterTodos().FirstOrDefault(x => x.Codigo == colaboradorAux.Codigo);
            Assert.IsNotNull(pessoaFisica);

            pessoaFisica.CPF = Global.GerarCpf(true);
            pessoaFisica.Passaporte = "Passaport" + randValue;
            pessoaFisica.Nome = "Nome" + randValue;
            pessoaFisica.Apelido = "Apelido" + randValue;
            pessoaFisica.Email = Global.GerarEmail();
            pessoaFisica.DataNascimento = dt.AddYears(rand.Next(-60, -20));
            pessoaFisica.DataVisto = dt.AddDays(rand.Next(-365, -1));
            pessoaFisica.DataValidadeVisto = dt.AddDays(rand.Next(1, 365));
            pessoaFisica.Sexo = Global.ObterEnumAleatorio<Sexo>();
            pessoaFisica.NomeMae = "Mae" + randValue;
            pessoaFisica.NomePai = "Pai" + randValue;
            pessoaFisica.RNE = "RNE" + randValue;
            pessoaFisica.DataExpedicaoRNE = dt.AddDays(rand.Next(1, 365));
            pessoaFisica.EstadoCivil = Global.ObterEnumAleatorio<EstadoCivil>();
            pessoaFisica.Escolaridade = Global.ObterEnumAleatorio<Escolaridade>();
            pessoaFisica.TipoSanguineo = Global.ObterEnumAleatorio<TipoSanguineo>();
            pessoaFisica.Naturalidade = cidade;

            pessoaFisica.RG = randValue.ToString();
            pessoaFisica.RGOrgaoEmissor = "SSP";
            pessoaFisica.RGOrgaoEmissorUF = estado;
            pessoaFisica.RGDataEmissao = dt.AddDays(rand.Next(-730, -30));

            pessoaFisica.TituloEleitor = rand.Next(987654, 987654321);
            pessoaFisica.TituloEleitorSecao = randValue;
            pessoaFisica.TituloEleitorZona = randValue;
            pessoaFisica.TituloEleitorCidade = cidade;
            pessoaFisica.CTPS = randValue;
            pessoaFisica.CTPSData = dt.AddDays(rand.Next(-999, -365));
            pessoaFisica.CTPSSerie = "CTPS" + randValue;
            pessoaFisica.CTPSEstado = estado;
            pessoaFisica.CNH = "cnh-" + randValue;
            pessoaFisica.CNHCategoria = Global.ObterBoolean() ? "A" : "B";
            pessoaFisica.CNHDataValidade = dt.AddDays(rand.Next(365, 999));
            pessoaFisica.CertificadoReservista = "R-" + randValue;
            pessoaFisica.CertificadoReservistaCat = Global.ObterBoolean() ? "A" : "B";

            pessoaFisica.Telefones = telefones;
            pessoaFisica.Emails = emails;
            pessoaFisica.Endereco = EnderecoTeste.ObterEnderecoNovo();

            colaboradorAux.Colaborador = pessoaFisica;

            var result = controller.Alterar(colaboradorAux, new string[] { }) as JsonResult;
            Assert.IsNotNull(result);

            var colaborador = JsonHelper.GetJsonObjectRepresentation<ColaboradorModelView>(result);
            Assert.IsNotNull(colaborador);
            Assert.AreEqual(pessoaFisica.CPF, colaborador.CPF.Replace(".", "").Replace("-", ""));
            Assert.AreEqual(pessoaFisica.TipoSanguineo, colaborador.TipoSanguineo);
            Assert.AreEqual(pessoaFisica.Sexo, colaborador.Sexo);
            Assert.AreEqual(pessoaFisica.EstadoCivil, colaborador.EstadoCivil);
            //Assert.AreEqual(pessoaFisica.DataExpedicaoRNE, colaborador.DataExpedicaoRNE);
            //Assert.AreEqual(pessoaFisica.DataNascimento, colaborador.DataNascimento);
            Assert.AreEqual(pessoaFisica.NomePai, colaborador.NomePai);
            Assert.AreEqual(pessoaFisica.NomeMae, colaborador.NomeMae);
            Assert.AreEqual(pessoaFisica.RG, colaborador.RG);
            Assert.AreEqual(pessoaFisica.RGOrgaoEmissor, colaborador.RGOrgaoEmissor);
            Assert.AreEqual(pessoaFisica.RGOrgaoEmissorUF.Codigo, colaborador.RGOrgaoEmissorUF);
            //Assert.AreEqual(pessoaFisica.RGDataEmissao, colaborador.RGDataEmissao);
            Assert.AreEqual(pessoaFisica.Passaporte, colaborador.Passaporte);
            Assert.AreEqual(pessoaFisica.Naturalidade.Codigo, colaborador.Naturalidade);
            Assert.AreEqual(pessoaFisica.CTPS, colaborador.CTPS);
            Assert.AreEqual(pessoaFisica.CTPSSerie, colaborador.CTPSSerie);
            Assert.AreEqual(pessoaFisica.CTPSEstado.Codigo, colaborador.CTPSEstado);
            Assert.AreEqual(pessoaFisica.CNH, colaborador.CNH);
            Assert.AreEqual(pessoaFisica.CNHCategoria, colaborador.CNHCategoria);
            Assert.AreEqual(pessoaFisica.TituloEleitor, colaborador.TituloEleitor);
            Assert.AreEqual(pessoaFisica.TituloEleitorCidade.Codigo, colaborador.TituloEleitorCidade);
            Assert.AreEqual(pessoaFisica.CertificadoReservista, colaborador.CertificadoReservista);
            Assert.AreEqual(pessoaFisica.CertificadoReservistaCat, colaborador.CertificadoReservistaCat);
            Assert.AreEqual(pessoaFisica.RNE, colaborador.RNE);
            Assert.AreEqual(pessoaFisica.Escolaridade, colaborador.Escolaridade);
            */

            // TODO: Validar lista de Emails
            // TODO: Validar lista de Papeis
        }

        /*
        /// <summary>
        /// Valida Ativar Colaborador
        /// </summary>
        [TestMethod]
        public void Pode_Ativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new ColaboradorController(unidTrabalho);
            var lista = ObterListaColaborador();
            Assert.IsTrue(lista.Any(), Resources.Lista_vazia);

            lista = lista.Where(x => x.Ativo == false).ToList();
            Assert.IsTrue(lista.Any(), Resources.Lista_vazia);

            var colaboradorAux = lista[0];
            Assert.IsFalse(colaboradorAux.Ativo);

            var result = controller.AtivarInativar(colaboradorAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var colaborador = JsonHelper.GetJsonObjectRepresentation<ColaboradorModelView>(result);
            Assert.IsNotNull(colaborador);
            Assert.IsTrue(colaborador.Ativo);
        }

        /// <summary>
        /// Valida Inativar Colaborador
        /// </summary>
        [TestMethod]
        public void Pode_Inativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new ColaboradorController(unidTrabalho);
            var colaboradorAux = ObterColaborador(true);
            Assert.IsTrue(colaboradorAux.Ativo);

            var result = controller.AtivarInativar(colaboradorAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var colaborador = JsonHelper.GetJsonObjectRepresentation<ColaboradorModelView>(result);
            Assert.IsNotNull(colaborador);
            Assert.AreEqual(colaboradorAux.Codigo, colaborador.Codigo);
            Assert.IsFalse(colaborador.Ativo);
        }
        */

        /// <summary>
        /// Busca aleatóriamente um colaborador cadastrado na base de dados.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) Colaborador.
        /// </returns>
        public static Colaborador ObterColaborador()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<Colaborador>(unidTrabalho);
            var listaColaborador = repositorio.ObterTodos().ToList();
            Assert.IsTrue(listaColaborador.Any());

            var rand = new Random();
            listaColaborador = listaColaborador.OrderBy(x => rand.Next()).ToList();
            return listaColaborador.First();
        }

        /// <summary>
        /// Obter lista colaborador.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) List&lt;Colaborador&gt;
        /// </returns>
        public static List<Colaborador> ObterListaColaborador()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<Colaborador>(unidTrabalho);
            var listaColaborador = repositorio.ObterTodos().ToList();
            Assert.IsTrue(listaColaborador.Any());

            var rand = new Random();
            listaColaborador = listaColaborador.OrderBy(x => rand.Next()).ToList();
            return listaColaborador;
        }
    }
}
