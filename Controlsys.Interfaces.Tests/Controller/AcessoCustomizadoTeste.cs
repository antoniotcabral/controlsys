﻿using System;
using System.Linq;
using Controlsys.Interfaces.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Controlsys.Infra;
using Globalsys;
using System.Web.Mvc;
using Controlsys.Interfaces.Controllers;
using Controlsys.Dominio.Pessoas;
using Controlsys.Repositorios.Pessoas;
using Controlsys.Persistencia;
using System.Collections.Generic;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) AcessoCustomizadoTeste.
    /// </summary>
    [TestClass]
    public class AcessoCustomizadoTeste
    {
        /// <summary>
        /// Pode alterar.
        /// </summary>
        ///
        /// <exception cref="AssertFailedException">
        /// Lançada quando uma condição de erro Assert Failed ocorre.
        /// </exception>
        [TestMethod]
        public void Pode_Alterar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            IRepositorioPessoaFisica rep = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaFisica>(unidTrabalho);

            List<PessoaFisica> listaPessoa = PessoaTeste.ObterListaPessoasFisica(true);
            Assert.IsNotNull(listaPessoa);
            Assert.IsTrue(listaPessoa.Any());

            Random ran = new Random();
            listaPessoa = listaPessoa.OrderBy(x => ran.Next()).ToList();
            PessoaFisica pessoaFiAux = listaPessoa.First();
            Papel papelAux = pessoaFiAux.ObterPapel();
            Assert.IsNotNull(papelAux);

            PapelModelView papelModelAux = new PapelModelView();
            papelModelAux.Codigo = papelAux.Codigo;
            papelModelAux.SobAnalise = !papelAux.SobAnalise;
            papelModelAux.Supervisionado = !papelAux.Supervisionado;
            papelModelAux.Supervisor = !papelAux.Supervisor;
            papelModelAux.SenhaLeitora = Global.GeradorAlfanumerico(5);

            PapelLog papelLogAux = papelAux.ObterLogPapel();
            Assert.IsNotNull(papelLogAux);
            papelLogAux.Status = Global.ObterEnumAleatorio<StatusPapelLog>();
            papelLogAux.DataInicio = DateTime.Now.AddDays(ran.Next(1,5));
            papelLogAux.DataFim = DateTime.Now.AddDays(ran.Next(5,15));
            papelLogAux.Motivo = DateTime.Now.ToString();
            if (papelLogAux.Status == StatusPapelLog.Afastamento || papelLogAux.Status == StatusPapelLog.BaixaDefinitiva)
            {
                papelLogAux.DataFim = null;
            }

            AcessoCustomizadoController controller = new AcessoCustomizadoController(unidTrabalho);
            try
            {
                controller.Alterar(papelModelAux, papelLogAux);
            }
            catch (Exception ex)
            {
                throw new AssertFailedException(ex.Message);
            }

        }
        
    }
}
