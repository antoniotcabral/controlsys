﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using System.Collections.Generic;
using Ext.Net;
using Controlsys.Interfaces.Models;
using Ext.Net.MVC;
using Globalsys;
using System.Web.Mvc;
using Controlsys.Dominio.Empresas;
using System.Linq;
using Controlsys.Persistencia;

namespace Controlsys.Interfaces.Tests.Controller
{
    [TestClass]
    public class ArquivoEmpresaTeste
    {
        [TestMethod]
        public void Pode_Pesquisar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            ArquivoEmpresaController controller = new ArquivoEmpresaController(unidTrabalho);

            var x = new Dictionary<string, object>() { };
            x.Add("start", (int)0);
            x.Add("limit", (int)25);

            var result = controller.Pesquisar(new StoreRequestParameters(x), null, null, null, 0) as StoreResult;

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Data as ArquivoEmpresaModelView[]);
        }

        [TestMethod]
        public void Pode_Exportar_Pesquisar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            ArquivoEmpresaController controller = new ArquivoEmpresaController(unidTrabalho);

            var result = controller.ExportarPesquisa(null, null, null, 0) as FileResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Pode_Exportar_Selecao()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<Empresa>(unidTrabalho);
            ArquivoEmpresaController controller = new ArquivoEmpresaController(unidTrabalho);

            IQueryable<Empresa> empresas = repositorio.ObterTodos().Where(p => p.Documentos.Select(d => d.Documento).Any(dm => dm.Midias.Any()));

            var result = controller.ExportarSelecao(empresas.Select(p => p.Codigo).ToArray(), null) as FileResult;

            Assert.IsNotNull(result);
        }
    }
}
