﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Tests.App_Start;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Controlsys.Interfaces.Tests.Controller
{
    [TestClass]
    public class GraficoOcorrenciaSegurancaTeste
    {
        [TestMethod]
        public void ExportarPorTipo()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new GraficoOcorrenciaSegurancaController(unidTrabalho);
            Mock.ServerMapPath(controller, "GraficoOcorrenciaSeguranca.rdlc");

            var dtInicio = (DateTime)SqlDateTime.MinValue;
            var dtfim = DateTime.Now;

            var result = controller.Exportar("PDF", "TIPO", dtInicio, dtfim) as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/pdf", result.ContentType);
            Assert.AreEqual("download.pdf", result.FileDownloadName);
        }

        [TestMethod]
        public void ExportarPorGravidade()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var controller = new GraficoOcorrenciaSegurancaController(unidTrabalho);
            Mock.ServerMapPath(controller, "GraficoOcorrenciaSeguranca.rdlc");

            var dtInicio = (DateTime)SqlDateTime.MinValue;
            var dtfim = DateTime.Now;

            var result = controller.Exportar("PDF", "GRAVIDADE", dtInicio, dtfim) as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/pdf", result.ContentType);
            Assert.AreEqual("download.pdf", result.FileDownloadName);
        }
    }
}
