﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Controlsys.Dominio.Empresas;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) RelatorioJournalTeste.
    /// </summary>
    [TestClass]
    public class RelatorioJournalTeste
    {
        /// <summary>
        /// Pode exportar PDF.
        /// </summary>
        [TestMethod]
        public void Pode_ExportarPDF()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RelatorioJournalController controller = new RelatorioJournalController(unidTrabalho);

            List<string> selectedFields = typeof(ControleAcessoModelView).GetProperties().Select(pi => pi.Name).ToList();

            Empresa empresa = EmpresaTeste.ObterEmpresa();
            Assert.IsNotNull(empresa);

            var result = controller.Exportar(null, null, selectedFields, "PDF") as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/pdf", result.ContentType);
            Assert.AreEqual("download", result.FileDownloadName);
        }

        /// <summary>
        /// Pode exportar excel.
        /// </summary>
        [TestMethod]
        public void Pode_ExportarExcel()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RelatorioJournalController controller = new RelatorioJournalController(unidTrabalho);

            List<string> selectedFields = typeof(ControleAcessoModelView).GetProperties().Select(pi => pi.Name).ToList();

            Empresa empresa = EmpresaTeste.ObterEmpresa();
            Assert.IsNotNull(empresa);

            var result = controller.Exportar(null, null, selectedFields, "Excel") as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/vnd.ms-excel", result.ContentType);
            Assert.AreEqual("download", result.FileDownloadName);
        }

        /// <summary>
        /// Pode exportar word.
        /// </summary>
        [TestMethod]
        public void Pode_ExportarWord()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RelatorioJournalController controller = new RelatorioJournalController(unidTrabalho);

            List<string> selectedFields = typeof(ControleAcessoModelView).GetProperties().Select(pi => pi.Name).ToList();

            Empresa empresa = EmpresaTeste.ObterEmpresa();
            Assert.IsNotNull(empresa);

            var result = controller.Exportar(null, null, selectedFields, "Word", null, null, empresa.Codigo) as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/msword", result.ContentType);
            Assert.AreEqual("download", result.FileDownloadName);
        }
    }
}
