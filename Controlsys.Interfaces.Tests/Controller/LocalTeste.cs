﻿using Controlsys.Dominio.Parametros;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Persistencia;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// Summary description for LocalTeste.
    /// </summary>
    [TestClass]
    public class LocalTeste
    {
        /// <summary>
        /// Pode incluir.
        /// </summary>
        [TestMethod]
        public void Pode_Incluir()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();

            LocalController controller = new LocalController(unidTrabalho);
            Local registro = new Local
            {
                Nome = "Teste",
                Sigla = "T"
            };
            var result = controller.Incluir(registro) as JsonResult;
            var registroGerado = JsonHelper.GetJsonObjectRepresentation<Local>(result);

            Assert.IsNotNull(registro);
            Assert.AreEqual(registro.Nome, registroGerado.Nome);
            Assert.AreEqual(registro.Sigla, registroGerado.Sigla);

            unidTrabalho.Rollback();
        }

        /// <summary>
        /// Pode alterar.
        /// </summary>
        [TestMethod]
        public void Pode_Alterar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();

            LocalController controller = new LocalController(unidTrabalho);

            #region [ObterTodos]
            var result1 = controller.ObterTodos() as JsonResult;
            var lista = JsonHelper.GetJsonObjectRepresentation<List<Local>>(result1);

            Assert.IsNotNull(lista);
            Assert.IsTrue(lista.Count > 0);
            #endregion

            var registro = lista.Where(x => x.Ativo).OrderBy(x => new Random().Next()).FirstOrDefault();

            Assert.IsNotNull(registro, "Nenhum registro encontrado para ser utilizado como referência de teste no Alterar");

            registro.Nome = new Random().Next().ToString();
            registro.Sigla = new Random().Next().ToString();

            var result = controller.Alterar(registro) as JsonResult;
            var registroAlterado = JsonHelper.GetJsonObjectRepresentation<Local>(result);

            Assert.IsNotNull(registro);
            Assert.AreEqual(registro.Nome, registroAlterado.Nome);
            Assert.AreEqual(registro.Sigla, registroAlterado.Sigla);

            unidTrabalho.Rollback();
        }

        /// <summary>
        /// 
        /// </summary>
        [TestMethod]
        public void AtivarInativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            unidTrabalho.BeginTransaction();

            LocalController controller = new LocalController(unidTrabalho);

            #region [ObterTodos]
            var result1 = controller.ObterTodos() as JsonResult;
            var lista = JsonHelper.GetJsonObjectRepresentation<List<Local>>(result1);

            Assert.IsNotNull(lista);
            Assert.IsTrue(lista.Count > 0);
            #endregion

            var registro = lista.OrderBy(x => new Random().Next()).FirstOrDefault();

            Assert.IsNotNull(registro, "Nenhum registro encontrado para ser utilizado como referência de teste no AtivarInativar");

            var result = controller.AtivarInativar(registro.Codigo) as JsonResult;
            var registroAlterado = JsonHelper.GetJsonObjectRepresentation<Local>(result);

            Assert.IsNotNull(registro, "Retornando registro nulo ao AtivarInativar");
            Assert.AreNotEqual(registro.Ativo, registroAlterado.Ativo, "Valor diferente do esperado.");

            unidTrabalho.Rollback();
        }

        /// <summary>
        /// 
        /// </summary>
        [TestMethod]
        public void Selecionar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            LocalController controller = new LocalController(unidTrabalho);

            #region [ObterTodos]
            var result1 = controller.ObterTodos() as JsonResult;
            var lista = JsonHelper.GetJsonObjectRepresentation<List<Local>>(result1);

            Assert.IsNotNull(lista);
            Assert.IsTrue(lista.Count > 0);
            #endregion

            var registro = lista.OrderBy(x => new Random().Next()).FirstOrDefault();

            Assert.IsNotNull(registro, "Nenhum registro encontrado para ser utilizado como referência de teste no Selecionar");

            var result2 = controller.Selecionar(registro.Codigo) as JsonResult;
            var registroSelecionado = JsonHelper.GetJsonObjectRepresentation<Local>(result2);

            Assert.IsNotNull(registroSelecionado, "Selecionar retornando nulo");
            Assert.AreEqual(registro.Nome, registroSelecionado.Nome, "Nome difere do valor utilizado como referência");
            Assert.AreEqual(registro.Sigla, registroSelecionado.Sigla, "Nome difere do valor utilizado como referência");
        }

        /// <summary>
        /// 
        /// </summary>
        [TestMethod]
        public void ObterTodos()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();

            LocalController controller = new LocalController(unidTrabalho);
            var result = controller.ObterTodos() as JsonResult;
            var lista = JsonHelper.GetJsonObjectRepresentation<List<Local>>(result);

            Assert.IsNotNull(lista);
            Assert.IsTrue(lista.Count > 0);
        }

        /// <summary>
        /// Obter Local.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// true to somente ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Local.
        /// </returns>
        public static Local ObterLocal(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            LocalController controller = new LocalController(unidTrabalho);
            JsonResult result = controller.ObterTodos(somenteAtivos) as JsonResult;
            var Locais = JsonHelper.GetJsonObjectRepresentation<List<Local>>(result);
            Assert.IsNotNull(Locais);

            Random rand = new Random();
            Locais = Locais.OrderBy(x => rand.Next()).ToList();

            Local LocalAux = null;
            foreach (var item in Locais)
            {
                result = controller.Selecionar(item.Codigo) as JsonResult;
                Assert.IsNotNull(result.Data);
                LocalAux = JsonHelper.GetJsonObjectRepresentation<Local>(result);

                //LocalAux = (controller.Selecionar(item.Codigo) as JsonResult).Data as Local;
                //Assert.IsNotNull(LocalAux);

                if (somenteAtivos)
                {
                    if (LocalAux.Ativo)
                        break;
                    LocalAux = null;
                }
                else
                    break;
            }

            return LocalAux;
        }

        /// <summary>
        /// Obter lista Local.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// true to somente ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;Local&gt;
        /// </returns>
        public static List<Local> ObterListaLocal(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<Local>(unidTrabalho);
            var Locais = repositorio.ObterTodos().ToList();
            Assert.IsTrue(Locais.Any());

            var rand = new Random();
            Locais = Locais.OrderBy(x => rand.Next()).ToList();

            return somenteAtivos ? Locais.Where(x => x.Ativo).ToList() : Locais;
        }
    }
}
