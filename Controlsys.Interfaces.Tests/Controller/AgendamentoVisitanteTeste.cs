﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Controlsys.Dominio.AgendamentosPessoa;
using Controlsys.Dominio.Pessoas;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Controlsys.Interfaces.Tests.Controller
{
    [TestClass]
    public class AgendamentoVisitanteTeste
    {
        [TestMethod]
        public void Pode_Incluir()
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();

            var fakeHttpContext = new Mock<HttpContextBase>();
            var fakeIdentity = new GenericIdentity("theo.ladislau");
            var principal = new GenericPrincipal(fakeIdentity, null);

            fakeHttpContext.Setup(t => t.User).Returns(principal);
            var controllerContext = new Mock<ControllerContext>();
            controllerContext.Setup(t => t.HttpContext).Returns(fakeHttpContext.Object);

            AgendamentoVisitanteController controller = new AgendamentoVisitanteController(unidTrabalho);
            controller.ControllerContext = controllerContext.Object;

            unidTrabalho.BeginTransaction();

            VisitanteAgendamentoModelView visMV = incluir(unidTrabalho, controller);

            Assert.IsNotNull(visMV);
            Assert.AreEqual(visMV.Empresa, "META");
            Assert.IsNotNull(visMV.Solicitante);

            unidTrabalho.Rollback();
        }

        [TestMethod]
        public void Pode_Alterar()
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();

            var fakeHttpContext = new Mock<HttpContextBase>();
            var fakeIdentity = new GenericIdentity("theo.ladislau");
            var principal = new GenericPrincipal(fakeIdentity, null);

            fakeHttpContext.Setup(t => t.User).Returns(principal);
            var controllerContext = new Mock<ControllerContext>();
            controllerContext.Setup(t => t.HttpContext).Returns(fakeHttpContext.Object);

            AgendamentoVisitanteController controller = new AgendamentoVisitanteController(unidTrabalho);
            controller.ControllerContext = controllerContext.Object;

            unidTrabalho.BeginTransaction();

            VisitanteAgendamentoModelView visMV = incluir(unidTrabalho, controller);

            VisitanteAgendamento visAgend = new VisitanteAgendamento
            {
                Codigo = visMV.Codigo,
                Motivo = "Não tem motivo!",
                ResponsavelVisita = ColaboradorTeste.ObterColaborador(),
                PessoaAg = new PessoaAgendamento
                {
                    Codigo = visMV.CodigoPessoa,
                    Nome = "Joaquim"
                }
            };

            VisitanteAgendamentoModelView visMV2 = (controller.Alterar(visAgend, new string[] { "Motivo", "Nome" }) as JsonResult).Data as VisitanteAgendamentoModelView;

            Assert.IsNotNull(visAgend);
            Assert.AreEqual(visMV2.Nome, "Joaquim");
            Assert.AreEqual(visMV2.Motivo, "Não tem motivo!");

            unidTrabalho.Rollback();
        }

        [TestMethod]
        public void Pode_Aprovar()
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();

            unidTrabalho.BeginTransaction();

            var fakeHttpContext = new Mock<HttpContextBase>();
            var fakeIdentity = new GenericIdentity("theo.ladislau");
            var principal = new GenericPrincipal(fakeIdentity, null);

            fakeHttpContext.Setup(t => t.User).Returns(principal);
            var controllerContext = new Mock<ControllerContext>();
            controllerContext.Setup(t => t.HttpContext).Returns(fakeHttpContext.Object);

            AgendamentoVisitanteController controllerAg = new AgendamentoVisitanteController(unidTrabalho);
            controllerAg.ControllerContext = controllerContext.Object;

            VisitanteAgendamentoModelView visMV = incluir(unidTrabalho, controllerAg);

            VisitanteController controllerV = new VisitanteController(unidTrabalho);

            Visitante visitante = new Visitante
            {
                PessoaFisica = new PessoaFisica
                {
                    Nome = visMV.Nome,
                    Apelido = visMV.Apelido,
                    CPF = visMV.CPF,
                    RG = visMV.RG,
                    RGDataEmissao = visMV.RGDataEmissao,
                    RGOrgaoEmissor = visMV.RGOrgaoEmissor,
                    RGOrgaoEmissorUF = EnderecoTeste.ObterEndereco(unidTrabalho).Logradouro.Bairro.Cidade.Estado,
                    Sexo = Sexo.Masculino
                },
                DataRegistro = DateTime.Now,
                Motivo = visMV.Motivo,
                DataInicio = visMV.DataInicio,
                DataFim = visMV.DataFim,
                Funcao = visMV.Funcao,
                ResponsavelVisita = ColaboradorTeste.ObterColaborador(),
                Solicitante = ColaboradorTeste.ObterColaborador(),
                Empresa = visMV.Empresa,
                ModeloCracha = ModeloCrachaTeste.ObterModeloCracha(),
                TipoVisitante = TipoVisitante.TipoVisitante1
            };

            VisitanteModelView visitanteMV = (controllerV.Incluir(visitante, new string[] { "TipoVisitante", "ModeloCracha" }, null, null, null, new string[] { }, visMV.Codigo) as JsonResult).Data as VisitanteModelView;

            Assert.IsNotNull(visitanteMV);
        }

        [TestMethod]
        public void Pode_Reprovar()
        {
            IUnidadeTrabalho unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();

            unidTrabalho.BeginTransaction();

            var fakeHttpContext = new Mock<HttpContextBase>();
            var fakeIdentity = new GenericIdentity("theo.ladislau");
            var principal = new GenericPrincipal(fakeIdentity, null);

            fakeHttpContext.Setup(t => t.User).Returns(principal);
            var controllerContext = new Mock<ControllerContext>();
            controllerContext.Setup(t => t.HttpContext).Returns(fakeHttpContext.Object);

            AgendamentoVisitanteController controller = new AgendamentoVisitanteController(unidTrabalho);
            controller.ControllerContext = controllerContext.Object;

            VisitanteAgendamentoModelView visMV = incluir(unidTrabalho, controller);

            VisitanteAgendamentoModelView visMV2 = ((controller.Reprovar(visMV.Codigo, "Reprovado!") as JsonResult).Data as VisitanteAgendamentoModelView);

            Assert.IsNotNull(visMV2);
            unidTrabalho.Rollback();
        }

        private VisitanteAgendamentoModelView incluir(IUnidadeTrabalho unidTrabalho, AgendamentoVisitanteController controller)
        {
            Colaborador colaborador = ColaboradorTeste.ObterColaborador();

            VisitanteAgendamento visitante = new VisitanteAgendamento
            {
                PessoaAg = new PessoaAgendamento
                {
                    CPF = Global.GerarCpf(true),
                    Nome = "João do Anzol",
                    Apelido = "Zé",
                    Email = "jose@meta.com.br",
                    Sexo = Dominio.Pessoas.Sexo.Masculino,
                    TelefoneNumEmergencial = "27997959272",
                    RG = "3061250",
                    RGDataEmissao = DateTime.Now,
                    RGOrgaoEmissor = "SSP",
                    RGOrgaoEmissorUF = EnderecoTeste.ObterEndereco(unidTrabalho).Logradouro.Bairro.Cidade.Estado
                },

                Empresa = "META",
                Motivo = "Teste",
                Funcao = "Função",
                DataInicio = DateTime.Now,
                ResponsavelVisita = colaborador,
                DataFim = DateTime.Now.AddDays(1)
            };

            VisitanteAgendamentoModelView visMV = (controller.Incluir(visitante, new string[] { }) as JsonResult).Data as VisitanteAgendamentoModelView;

            return visMV;
        }
    }
}
