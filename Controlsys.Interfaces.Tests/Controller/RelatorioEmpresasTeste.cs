﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Controlsys.Dominio.Empresas;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Controlsys.Interfaces.Tests.Controller;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) RelatorioEmpresasTeste.
    /// </summary>
    [TestClass]
    public class RelatorioEmpresasTeste
    {
        /// <summary>
        /// Pode exportar PDF.
        /// </summary>
        [TestMethod]
        public void Pode_ExportarPDF()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RelatorioEmpresasController controller = new RelatorioEmpresasController(unidTrabalho);

            Empresa empresa = EmpresaTeste.ObterEmpresa(true);
            Assert.IsNotNull(empresa);

            List<string> selectedFields = typeof(EmpresaModelView).GetProperties().Select(pi => pi.Name).ToList();

            var result = controller.Exportar("PDF", selectedFields, empresa.Nome, "", "", true, 0, 0) as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/pdf", result.ContentType);
            Assert.AreEqual("download", result.FileDownloadName);
        }

        /// <summary>
        /// Pode exportar excel.
        /// </summary>
        [TestMethod]
        public void Pode_ExportarExcel()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RelatorioEmpresasController controller = new RelatorioEmpresasController(unidTrabalho);

            Empresa empresa = EmpresaTeste.ObterEmpresa(true);
            Assert.IsNotNull(empresa);

            List<string> selectedFields = typeof(EmpresaModelView).GetProperties().Select(pi => pi.Name).ToList();

            var result = controller.Exportar("Excel", selectedFields, empresa.Nome, "", "", true, 0, 0) as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/vnd.ms-excel", result.ContentType);
            Assert.AreEqual("download", result.FileDownloadName);
        }

        /// <summary>
        /// Pode exportar word.
        /// </summary>
        [TestMethod]
        public void Pode_ExportarWord()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RelatorioEmpresasController controller = new RelatorioEmpresasController(unidTrabalho);

            Empresa empresa = EmpresaTeste.ObterEmpresa(true);
            Assert.IsNotNull(empresa);

            List<string> selectedFields = typeof(EmpresaModelView).GetProperties().Select(pi => pi.Name).ToList();

            var result = controller.Exportar("Word", selectedFields, empresa.Nome, "", "", true, 0, 0) as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/msword", result.ContentType);
            Assert.AreEqual("download", result.FileDownloadName);
        }

        /// <summary>
        /// Pode pesquisar.
        /// </summary>
        [TestMethod]
        public void Pode_Pesquisar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RelatorioEmpresasController controller = new RelatorioEmpresasController(unidTrabalho);

            Empresa empresa = EmpresaTeste.ObterEmpresa();
            Assert.IsNotNull(empresa);

            var result = controller.Pesquisar(null, null, null, null, null, 0, 0) as JsonResult;
            Assert.IsNotNull(result);

            var empresas = result.Data as List<EmpresaModelView>;
            Assert.IsNotNull(empresas);
        }
    }
}
