﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Controlsys.Dominio.Parametros;
using Controlsys.Interfaces.Controllers;
using Controlsys.Persistencia;
using Globalsys;
using Globalsys.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Controlsys.Infra;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) FeriadoTeste.
    /// </summary>
    [TestClass]
    public class FeriadoTeste
    {
        /// <summary>
        /// Pode incluir.
        /// </summary>
        [TestMethod]
        public void Pode_Incluir()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            FeriadoController controller = new FeriadoController(unidTrabalho);

            var now = DateTime.Now;
            var dt = new DateTime(now.Year, now.Month, now.Day);
            var data = dt.ToString("MM/dd HH:mm:ss.fff");
            var nome = "Nome-" + data;
            var descricao = "Descrição-" + data;
            Feriado feriadoAux = new Feriado()
            {
                DataFeriado = dt,
                Nome = nome,
                Descricao = descricao
            };

            try
            {
                var result = controller.Incluir(feriadoAux) as JsonResult;
                Assert.IsNotNull(result);

                var feriado = JsonHelper.GetJsonObjectRepresentation<Feriado>(result);
                Assert.IsNotNull(feriado);
                Assert.AreEqual(dt, feriado.DataFeriado.AddHours(-3));
                Assert.AreEqual(nome, feriado.Nome);
                Assert.AreEqual(descricao, feriado.Descricao);
            }
            catch (CoreException ex)
            {
                StringAssert.Contains(ex.Message.ToLower(), "já existe um feriado");
            }
        }

        /// <summary>
        /// Pode alterar.
        /// </summary>
        [TestMethod]
        public void Pode_Alterar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            FeriadoController controller = new FeriadoController(unidTrabalho);
            Feriado feriadoAux = ObterFeriado(true);
            Assert.IsNotNull(feriadoAux);
            Assert.IsTrue(feriadoAux.Ativo);

            var now = DateTime.Now;
            var dt = new DateTime(now.Year, now.Month, now.Day);
            var data = now.ToString("MM/dd HH:mm:ss.fff");
            var nome = "Nome-" + data;
            var descricao = "Descrição-" + data;

            feriadoAux.DataFeriado = dt;
            feriadoAux.Nome = nome;
            feriadoAux.Descricao = descricao;

            var result = controller.Alterar(feriadoAux) as JsonResult;
            Assert.IsNotNull(result.Data);

            var feriado = JsonHelper.GetJsonObjectRepresentation<Feriado>(result);
            Assert.IsTrue(feriado.Codigo > 0);
            Assert.AreEqual(dt, feriado.DataFeriado.AddHours(-3));
            Assert.AreEqual(nome, feriado.Nome);
            Assert.AreEqual(descricao, feriado.Descricao);
        }

        /// <summary>
        /// Pode ativar.
        /// </summary>
        [TestMethod]
        public void Pode_Ativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            FeriadoController controller = new FeriadoController(unidTrabalho);

            var repositorio = new Repositorio<Feriado>(unidTrabalho);
            var feriados = repositorio.ObterTodos().Where(x => x.Ativo == false).ToList();
            Assert.IsTrue(feriados.Any());

            var rand = new Random();
            feriados = new List<Feriado>(feriados.OrderBy(x => rand.Next()));

            Feriado feriadoAux = feriados.ToList()[0];
            Assert.IsNotNull(feriadoAux);
            Assert.IsFalse(feriadoAux.Ativo);

            var result = controller.AtivarInativar(feriadoAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var feriado = JsonHelper.GetJsonObjectRepresentation<Feriado>(result);
            Assert.IsNotNull(feriado);
            Assert.AreEqual(feriadoAux.Codigo, feriado.Codigo);
            Assert.IsTrue(feriado.Ativo);
        }

        /// <summary>
        /// Pode inativar.
        /// </summary>
        [TestMethod]
        public void Pode_Inativar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            FeriadoController controller = new FeriadoController(unidTrabalho);

            Feriado feriadoAux = ObterFeriado(true);
            Assert.IsNotNull(feriadoAux);
            Assert.IsTrue(feriadoAux.Ativo);

            var result = controller.AtivarInativar(feriadoAux.Codigo) as JsonResult;
            Assert.IsNotNull(result);

            var feriado = JsonHelper.GetJsonObjectRepresentation<Feriado>(result);
            Assert.IsNotNull(feriado);
            Assert.AreEqual(feriadoAux.Codigo, feriado.Codigo);
            Assert.IsFalse(feriado.Ativo);
        }

        /// <summary>
        /// Busca aleatóriamente um feriado cadastrado na base de dados.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// Filtrar feriados ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Feriado.
        /// </returns>
        public static Feriado ObterFeriado(bool somenteAtivos = false)
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            var repositorio = new Repositorio<Feriado>(unidTrabalho);
            var listaFeriado = repositorio.ObterTodos().ToList();
            Assert.IsTrue(listaFeriado.Any());

            var rand = new Random();
            listaFeriado = listaFeriado.OrderBy(x => rand.Next()).ToList();

            return listaFeriado.First(x => !somenteAtivos || x.Ativo);
        }

        /// <summary>
        /// Obter feriado turno.
        /// </summary>
        ///
        /// <param name="turno">
        /// O(a) turno.
        /// </param>
        ///
        /// <returns>
        /// Um(a) FeriadoTurno.
        /// </returns>
        public static FeriadoTurno ObterFeriadoTurno(Turno turno)
        {
            return new FeriadoTurno()
            {
                Feriado = ObterFeriado(true),
                Turno = turno
            };
        }
    }
}
