﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Controlsys.Dominio.Pessoas;
using Controlsys.Infra;
using Controlsys.Interfaces.Controllers;
using Controlsys.Interfaces.Models;
using Globalsys;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Controlsys.Dominio.Empresas;

namespace Controlsys.Interfaces.Tests.Controller
{
    /// <summary>
    /// (classe de teste de unidade) representa um(a) RelatorioPessoasAcessoTeste.
    /// </summary>
    [TestClass]
    public class RelatorioPessoasAcessoTeste
    {
        /// <summary>
        /// Pode exportar PDF.
        /// </summary>
        [TestMethod]
        public void Pode_ExportarPDF()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RelatorioPessoasAcessoController controller = new RelatorioPessoasAcessoController(unidTrabalho);

            List<string> selectedFields = typeof(RelatorioPessoaModelView).GetProperties().Select(pi => pi.Name).ToList();

            PessoaFisica pessoa = PessoaTeste.ObterListaPessoasFisica(true).FirstOrDefault();
            Assert.IsNotNull(pessoa);

            var result = controller.Exportar(selectedFields, "PDF", pessoa.Nome) as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/pdf", result.ContentType);
            Assert.AreEqual("download", result.FileDownloadName);
        }

        /// <summary>
        /// Pode exportar excel.
        /// </summary>
        [TestMethod]
        public void Pode_ExportarExcel()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RelatorioPessoasAcessoController controller = new RelatorioPessoasAcessoController(unidTrabalho);

            List<string> selectedFields = typeof(RelatorioPessoaModelView).GetProperties().Select(pi => pi.Name).ToList();

            PessoaFisica pessoa = PessoaTeste.ObterListaPessoasFisica(true).FirstOrDefault();
            Assert.IsNotNull(pessoa);

            var result = controller.Exportar(selectedFields, "Excel", pessoa.Nome) as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/vnd.ms-excel", result.ContentType);
            Assert.AreEqual("download", result.FileDownloadName);
        }

        /// <summary>
        /// Pode exportar word.
        /// </summary>
        [TestMethod]
        public void Pode_ExportarWord()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RelatorioPessoasAcessoController controller = new RelatorioPessoasAcessoController(unidTrabalho);

            List<string> selectedFields = typeof(RelatorioPessoaModelView).GetProperties().Select(pi => pi.Name).ToList();

            PessoaFisica pessoa = PessoaTeste.ObterListaPessoasFisica(true).FirstOrDefault();
            Assert.IsNotNull(pessoa);

            var result = controller.Exportar(selectedFields, "Word", pessoa.Nome) as FileContentResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("application/msword", result.ContentType);
            Assert.AreEqual("download", result.FileDownloadName);
        }

        /// <summary>
        /// Pode pesquisar.
        /// </summary>
        [TestMethod]
        public void Pode_Pesquisar()
        {
            var unidTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RelatorioPessoasAcessoController controller = new RelatorioPessoasAcessoController(unidTrabalho);

            PessoaFisica pessoa = PessoaTeste.ObterListaPessoasFisica(true).FirstOrDefault();
            Assert.IsNotNull(pessoa);

            var result = controller.Pesquisar(null, pessoa.Nome) as JsonResult;
            Assert.IsNotNull(result);

            var registros = result.Data as List<RelatorioPessoaModelView>;
            Assert.IsNotNull(registros);
        }
    }
}
