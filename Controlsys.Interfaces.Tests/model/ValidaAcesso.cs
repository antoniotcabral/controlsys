﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Interfaces.Tests.model
{
    public class ValidaAcesso
    {
        public int StatusAcesso { get; set; }
        public byte Permissao { get; set; }
        public string Direcao { get; set; }
    }
}
