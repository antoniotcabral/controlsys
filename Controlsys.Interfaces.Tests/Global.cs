﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Pessoas;

namespace Controlsys.Interfaces.Tests
{
    /// <summary>
    /// Representa um(a) Global.
    /// </summary>
    public class Global
    {
        /// <summary>
        /// Busca valor enum aleatório do tipo especificado.
        /// </summary>
        ///
        /// <typeparam name="T">
        /// enum.
        /// </typeparam>
        ///
        /// <returns>
        /// Um(a) T.
        /// </returns>
        public static T ObterEnumAleatorio<T>()
        {
            Array values = Enum.GetValues(typeof(T));
            Random random = new Random();
            return (T)values.GetValue(random.Next(values.Length));
        }

        /// <summary>
        /// Get value from description.
        /// </summary>
        ///
        /// <exception cref="InvalidOperationException">
        /// Lançada quando an operação requerida é inválida.
        /// </exception>
        /// <exception cref="ArgumentException">
        /// Lançada quando um ou mais argumentos tem valores não suportados ou ilegais.
        /// </exception>
        ///
        /// <typeparam name="T">
        /// Generic type parameter.
        /// </typeparam>
        /// <param name="description">
        /// The description.
        /// </param>
        ///
        /// <returns>
        /// Um(a) T.
        /// </returns>
        public static T GetValueFromDescription<T>(string description)
        {
            var type = typeof(T);
            if (!type.IsEnum) throw new InvalidOperationException();
            foreach (var field in type.GetFields())
            {
                var attribute = Attribute.GetCustomAttribute(field,
                    typeof(DescriptionAttribute)) as DescriptionAttribute;
                if (attribute != null)
                {
                    if (attribute.Description == description)
                        return (T)field.GetValue(null);
                }
                else
                {
                    if (field.Name == description)
                        return (T)field.GetValue(null);
                }
            }
            throw new ArgumentException("Not found.", "description");
            // or return default(T);
        }

        /*
        public static String GerarCPF()
        {
            Random r = new Random();
            int a1 = r.Next(10), a2 = r.Next(10), a3 = r.Next(10),
                a4 = r.Next(10), a5 = r.Next(10), a6 = r.Next(10),
                a7 = r.Next(10), a8 = r.Next(10), a9 = r.Next(10);
            int b1 = a1 * 10,
                b2 = a2 * 9,
                b3 = a3 * 8,
                b4 = a4 * 7,
                b5 = a5 * 6,
                b6 = a6 * 5,
                b7 = a7 * 4,
                b8 = a8 * 3,
                b9 = a9 * 2;
            int DV1 = b1 + b2 + b3 + b4 + b5 + b6 + b7 + b8 + b9;
            int T1 = DV1 / 11;
            int T2 = DV1 % 11;// Cálculo do resto da divisão
            int Primeiro_Digito_Verificador;
            if (T2 <= 2)
                Primeiro_Digito_Verificador = 0;
            else
                Primeiro_Digito_Verificador = 11 - T2;

            int c1 = a1 * 11,
                c2 = a2 * 10,
                c3 = a3 * 9,
                c4 = a4 * 8,
                c5 = a5 * 7,
                c6 = a6 * 6,
                c7 = a7 * 5,
                c8 = a8 * 4,
                c9 = a9 * 3,
                c10 = Primeiro_Digito_Verificador * 2;
            int DV2 = c1 + c2 + c3 + c4 + c5 + c6 + c7 + c8 + c9 + c10;
            int T3 = DV2 / 11;
            int T4 = DV2 % 11; // Cálculo do resto da divisão
            int Segundo_Digito_Verificador;
            if (T4 <= 2)
                Segundo_Digito_Verificador = 0;
            else
                Segundo_Digito_Verificador = 11 - T4;

            return a1.ToString() + a2.ToString() + a3.ToString() + "." +
                a4.ToString() + a5.ToString() + a6.ToString() + "." + a7.ToString() + a8.ToString() + a9.ToString() + "-"
                + Primeiro_Digito_Verificador.ToString() + Segundo_Digito_Verificador.ToString();
        }
        */

        /// <summary>
        /// Metodo que gera um Cpf para testes.
        /// </summary>
        ///
        /// <param name="somenteNumero">
        /// true to somente numero.
        /// </param>
        ///
        /// <returns>
        /// Um(a) String.
        /// </returns>
        public static String GerarCpf(bool somenteNumero = false)
        {
            Random r = new Random();
            int a1 = r.Next(10), a2 = r.Next(10), a3 = r.Next(10),
                a4 = r.Next(10), a5 = r.Next(10), a6 = r.Next(10),
                a7 = r.Next(10), a8 = r.Next(10), a9 = r.Next(10);
            int b1 = a1 * 10,
                b2 = a2 * 9,
                b3 = a3 * 8,
                b4 = a4 * 7,
                b5 = a5 * 6,
                b6 = a6 * 5,
                b7 = a7 * 4,
                b8 = a8 * 3,
                b9 = a9 * 2;
            int DV1 = b1 + b2 + b3 + b4 + b5 + b6 + b7 + b8 + b9;
            int T1 = DV1 / 11;
            int T2 = DV1 % 11;// Cálculo do resto da divisão
            int dv1;
            if (T2 <= 2)
            {
                dv1 = 0;
            }
            else
            {
                dv1 = 11 - T2;
            }

            int c1 = a1 * 11,
                c2 = a2 * 10,
                c3 = a3 * 9,
                c4 = a4 * 8,
                c5 = a5 * 7,
                c6 = a6 * 6,
                c7 = a7 * 5,
                c8 = a8 * 4,
                c9 = a9 * 3,
                c10 = dv1 * 2;
            int DV2 = c1 + c2 + c3 + c4 + c5 + c6 + c7 + c8 + c9 + c10;
            int T3 = DV2 / 11;
            int T4 = DV2 % 11; // Cálculo do resto da divisão
            int dv2;
            if (T4 <= 2)
            {
                dv2 = 0;
            }
            else
            {
                dv2 = 11 - T4;
            }
            
            return string.Format((somenteNumero
                ? "{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}"
                : "{0}{1}{2}.{3}{4}{5}.{6}{7}{8}-{9}{10}"),
                a1, a2, a3, a4, a5, a6, a7, a8, a9, dv1, dv2);

        }

        /// <summary>
        /// Metodo que gera um cnpj para teste.
        /// </summary>
        ///
        /// <param name="somenteNumeros">
        /// true to somente numeros.
        /// </param>
        ///
        /// <returns>
        /// Um(a) String.
        /// </returns>
        public static String GerarCnpj(bool somenteNumeros = false)
        {
            Random r = new Random();

            var n1 = r.Next(10);
            var n2 = r.Next(10);
            var n3 = r.Next(10);
            var n4 = r.Next(10);
            var n5 = r.Next(10);
            var n6 = r.Next(10);
            var n7 = r.Next(10);
            var n8 = r.Next(10);
            var n9 = 0;//gera_random(n);
            var n10 = 0;//gera_random(n);
            var n11 = 0;//gera_random(n);
            var n12 = 1;//gera_random(n);

            int d1 = n12 * 2 + n11 * 3 + n10 * 4 + n9 * 5 + n8 * 6 + n7 * 7 + n6 * 8 + n5 * 9 + n4 * 2 + n3 * 3 + n2 * 4 + n1 * 5;
            d1 = 11 - (d1 % 11);

            if (d1 >= 10)
            {
                d1 = 0;
            }

            int d2 = d1 * 2 + n12 * 3 + n11 * 4 + n10 * 5 + n9 * 6 + n8 * 7 + n7 * 8 + n6 * 9 + n5 * 2 + n4 * 3 + n3 * 4 + n2 * 5 + n1 * 6;
            d2 = 11 - (d2 % 11);

            if (d2 >= 10)
            {
                d2 = 0;
            }

            return string.Format(somenteNumeros ?
                "{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}{11}{12}{13}" :
                "{0}{1}.{2}{3}{4}.{5}{6}{7}/{8}{9}{10}{11}-{12}{13}",
                n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, d1, d2);
        }

        /// <summary>
        /// Gerar um CEP aleatório.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) String.
        /// </returns>
        public static String GerarCep()
        {
            Random rand = new Random();
            return string.Format("{0}-{1}", rand.Next(10000, 99999), rand.Next(100, 999));
        }

        /// <summary>
        /// Gerar um telefone aleatório.
        /// </summary>
        ///
        /// <param name="tipoTelefone">
        /// O(a) tipo telefone.
        /// </param>
        ///
        /// <returns>
        /// Um(a) String.
        /// </returns>
        public static String GerarTelefone(TipoTelefone tipoTelefone)
        {
            Random rand = new Random();
            switch (tipoTelefone)
            {
                case TipoTelefone.Celular:
                    return string.Format("({0}) {1}-{2}", rand.Next(10, 99), rand.Next(8000, 9999), rand.Next(1000, 9999));

                default:
                    return string.Format("({0}) {1}-{2}", rand.Next(10, 99), rand.Next(2000, 5000), rand.Next(1000, 9999));
            }
        }

        /// <summary>
        /// Gerar email.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) String.
        /// </returns>
        public static String GerarEmail()
        {
            Random rand = new Random();
            return String.Format("{0}@dominio.com.br", rand.Next(1000, int.MaxValue));
        }

        /// <summary>
        /// Obter boolean.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) Boolean.
        /// </returns>
        public static Boolean ObterBoolean()
        {
            return new Random().Next() % 2 == 0 ? true : false;
        }

        /// <summary>
        /// Gerador alfanumerico.
        /// </summary>
        ///
        /// <param name="qtdCaracteres">
        /// O(a) qtd caracteres.
        /// </param>
        ///
        /// <returns>
        /// Um(a) string.
        /// </returns>
        public static string GeradorAlfanumerico(int qtdCaracteres)
        {
            var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            var result = new string(
                Enumerable.Repeat(chars, qtdCaracteres)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
            return result;
        }
    }
}
