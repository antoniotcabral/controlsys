﻿using System.IO;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Rhino.Mocks;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Controlsys.Interfaces.Tests
{
    /// <summary>
    /// Representa um(a) JsonHelper.
    /// </summary>
    public class JsonHelper
    {
        /// <summary>
        /// Unit Testing The MVC JsonResult Ref: http://blogs.msdn.com/b/miah/archive/2009/02/25/unit-
        /// testing-the-mvc-jsonresult.aspx.
        /// </summary>
        ///
        /// <typeparam name="T">
        /// Generic type parameter.
        /// </typeparam>
        /// <param name="jsonResult">
        /// O(a) JSON result.
        /// </param>
        ///
        /// <returns>
        /// Um(a) T.
        /// </returns>
        public static T GetJsonObjectRepresentation<T>(JsonResult jsonResult)
        {
            jsonResult.JsonRequestBehavior = JsonRequestBehavior.AllowGet;

            var controllerContextMock = MockRepository.GenerateStub<ControllerContext>();
            var httpContextMock = MockRepository.GenerateStub<HttpContextBase>();
            var httpResponseMock = MockRepository.GenerateStub<HttpResponseBase>();

            httpContextMock.Stub(x => x.Response).Return(httpResponseMock);
            controllerContextMock.HttpContext = httpContextMock;

            jsonResult.ExecuteResult(controllerContextMock);

            var args = httpResponseMock.GetArgumentsForCallsMadeOn(x => x.Write(null));

            JsonSerializer json = new JsonSerializer();

            json.NullValueHandling = NullValueHandling.Ignore;
            json.ObjectCreationHandling = ObjectCreationHandling.Replace;
            json.MissingMemberHandling = MissingMemberHandling.Ignore;
            json.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

            StringReader sr = new StringReader(args[0][0] as string);
            JsonTextReader reader = new JsonTextReader(sr);
            var result = json.Deserialize<T>(reader);
            reader.Close();

            return result;

            //return JsonConvert.DeserializeObject<T>(args[0][0] as string, json);

            //JavaScriptSerializer serializer = new JavaScriptSerializer();
            //return serializer.Deserialize<T>(args[0][0] as string);
        }

    }
}
