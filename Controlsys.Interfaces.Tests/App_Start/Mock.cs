﻿using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Moq;
using Rhino.Mocks;
using MockRepository = Rhino.Mocks.MockRepository;

namespace Controlsys.Interfaces.Tests.App_Start
{
    public class Mock
    {
        public static void ServerMapPath(ControllerBase controller, string fileName)
        {
            var server = MockRepository.GeneratePartialMock<HttpServerUtilityBase>();
            var context = MockRepository.GeneratePartialMock<HttpContextBase>();
            context.Expect(x => x.Server).Return(server);
            var expected = @"D:\Projetos\Controlsys\develop-fase2\Controlsys.Interfaces\Reports\" + fileName;
            server.Expect(x => x.MapPath("~/Reports/" + fileName)).Return(expected);
            var requestContext = new RequestContext(context, new RouteData());
            controller.ControllerContext = new ControllerContext(requestContext, controller);
        }

        public static GenericIdentity UserHttpContext(ControllerBase controller, string username)
        {
            var fakeHttpContext = new Mock<HttpContextBase>();
            var fakeIdentity = new GenericIdentity(username);
            var principal = new GenericPrincipal(fakeIdentity, null);

            fakeHttpContext.Setup(t => t.User).Returns(principal);
            var controllerContext = new Mock<ControllerContext>();
            controllerContext.Setup(t => t.HttpContext).Returns(fakeHttpContext.Object);

            controller.ControllerContext = controllerContext.Object;

            return fakeIdentity;
        }
    }
}
