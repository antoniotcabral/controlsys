﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Rhino.Mocks;

namespace Controlsys.Interfaces.Tests
{
    /// <summary>
    /// Representa um(a) MockContext.
    /// </summary>
    public class MockContext
    {
        //public Mock<RequestContext> RoutingRequestContext { get; private set; }
        //public Mock<HttpContextBase> Http { get; private set; }
        //public Mock<HttpServerUtilityBase> Server { get; private set; }
        //public Mock<HttpResponseBase> Response { get; private set; }
        //public Mock<HttpRequestBase> Request { get; private set; }
        //public Mock<HttpSessionStateBase> Session { get; private set; }
        //public Mock<ActionExecutingContext> ActionExecuting { get; private set; }
        //public HttpCookieCollection Cookies { get; private set; }

        //public MockContext()
        //{
        //    this.RoutingRequestContext = new Mock<RequestContext>(MockBehavior.Loose);
        //    this.ActionExecuting = new Mock<ActionExecutingContext>(MockBehavior.Loose);
        //    this.Http = new Mock<HttpContextBase>(MockBehavior.Loose);
        //    this.Server = new Mock<HttpServerUtilityBase>(MockBehavior.Loose);
        //    this.Response = new Mock<HttpResponseBase>(MockBehavior.Loose);
        //    this.Request = new Mock<HttpRequestBase>(MockBehavior.Loose);
        //    this.Session = new Mock<HttpSessionStateBase>(MockBehavior.Loose);
        //    this.Cookies = new HttpCookieCollection();

        //    this.RoutingRequestContext.SetupGet(c => c.HttpContext).Returns(this.Http.Object);
        //    this.ActionExecuting.SetupGet(c => c.HttpContext).Returns(this.Http.Object);
        //    this.Http.SetupGet(c => c.Request).Returns(this.Request.Object);
        //    this.Http.SetupGet(c => c.Response).Returns(this.Response.Object);
        //    this.Http.SetupGet(c => c.Server).Returns(this.Server.Object);
        //    this.Http.SetupGet(c => c.Session).Returns(this.Session.Object);
        //    this.Request.Setup(c => c.Cookies).Returns(Cookies);
        //}

        //public static HttpContextBase FakeHttpContext()
        //{
        //    var httpContext = new Mock<HttpContextBase>();
        //    var request = new Mock<HttpRequestBase>();
        //    var response = new Mock<HttpResponseBase>();
        //    var session = new Mock<HttpSessionStateBase>();
        //    var server = new Mock<HttpServerUtilityBase>();
        //    var cookies = new HttpCookieCollection();
        //    httpContext.Setup(x => x.Server).Returns(server.Object);
        //    httpContext.Setup(x => x.Session).Returns(session.Object);
        //    httpContext.Setup(x => x.Request).Returns(request.Object);
        //    httpContext.Setup(x => x.Response).Returns(response.Object);
        //    response.Setup(x => x.Cookies).Returns(cookies);
        //    httpContext.SetupGet(x => x.Request.Url).Returns(new Uri("http://www.csuci.edu"));
        //    httpContext.SetupGet(x => x.Request.UrlReferrer).Returns(new Uri("http://www.csuci.edu"));
        //    //var writer = new StringWriter();
        //    //var wr = new SimpleWorkerRequest("", "", "", "", writer);
        //    //HttpContext.Current = new HttpContext(wr);
        //    return httpContext.Object;
        //}
    }

    /// <summary>
    /// Representa um(a) MockHttpContext.
    /// </summary>
    public class MockHttpContext : HttpContextBase
    {
        /// <summary>
        /// O(a) key.
        /// </summary>
        private readonly string _key;

        /// <summary>
        /// O(a) value.
        /// </summary>
        private readonly string _val;
        //private IPrincipal _user;

        /// <summary>
        /// O(a) server.
        /// </summary>
        private readonly HttpServerUtilityBase _server;

        /// <summary>
        /// Construtor para Controlsys.Interfaces.Tests.MockHttpContext.
        /// </summary>
        ///
        /// <param name="key">
        /// O(a) key.
        /// </param>
        /// <param name="val">
        /// O(a) value.
        /// </param>
        public MockHttpContext(string key, string val)
        {
            _key = key;
            _val = val;
            _server = MockRepository.GenerateStub<HttpServerUtilityBase>();
        }
    }
}
