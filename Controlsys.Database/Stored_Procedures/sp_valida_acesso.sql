﻿
CREATE PROCEDURE [dbo].[sp_valida_acesso] 
	@numeroCracha VARCHAR(40),
	@numLeitora INT,
	@senha VARCHAR(100),
	@ipControladora VARCHAR(20)
AS
BEGIN
	-- VARIAVEIS VINDAS DO BANCO
	DECLARE 
		@codAcessoTemp INT,
		@cdPapel INT,
		@cdPessoaFisica VARCHAR(10),
		@txCpf VARCHAR(10),
		@dtValidadeVisto DATETIME,
		@vencimentoCNH DATETIME,
		@blSupervisionado BIT,
		@txStatus VARCHAR(100),
		@dtInicioStatus DATETIME,
		@dtFimStatus DATETIME,
		@cdVisitante INT,
		@tipoVisitante VARCHAR(20),
		@dtInicioVisitante DATETIME,
		@dtFimVisitante DATETIME,
		@dtAso DATETIME,
		@senhaPapel VARCHAR(100),
		@cdPrestador INT,
		@dtValidadePrestador DATETIME,
		@tpPrestador VARCHAR(50),
		@cdColaborador INT,
		@dtStatusPedido DATETIME,
		@txStatusPedido VARCHAR(30),
		@dtInicioPedido DATETIME,
		@dtFimPedido DATETIME,
		@numCrachaRFID VARCHAR(40),
		@dtRegistroCracha DATETIME,
		@blAtivo BIT,
		@cdCracha INT,
		@cdGrupoTrabalho INT,
		@cdTurno INT,
		@blObedeceEscala BIT,
		@nuDiaSemana INT,
		@hrInicio TIME,
		@hrFim TIME,
		-- Query alocação colaborador
		@cdAlocacao INT,
		@dtVencimentoPedido DATETIME,
		@statusPedido VARCHAR(40),
		-- Consultar permissões de acesso
		@cdControladora INT,
		@cdLeitora INT,
		@direcaoLeitora VARCHAR(20),
		@txStatusControladora VARCHAR(50),
		-- Consultar acesso customizado
		@ultimoAcessoDirecao VARCHAR(20),
		@statusUltimoAcesso VARCHAR(30),
		-- Parametro antipassback
		@resultGrupos INT,
		@usaAntipassback VARCHAR(4),
		@resultAcessTemp INT,
		@antipassbackGrupo INT,
		@antipassbackIndividual INT,
		@cdGrupoLeitora INT,
		-- Tolerância entrada
		@nutolerancia INT,
		@BLEXTERNA BIT,
		@BLSUPERVISAO BIT;
	-- VARIAVEIS AUXILIARES
	DECLARE @dtAtual DATETIME, -- Usado para comparações de data e hora
		@hrAtual TIME, -- Usado para comparações de hora
		@dAtual DATETIME;-- Usado para comparações apenas de data, ignorando as horas
		
	SET @dtAtual = GETDATE();
	SET @hrAtual = CONVERT(TIME, @dtAtual);
	SET @dAtual = CONVERT(DATETIME, CONVERT(DATE, @dtAtual));
	SET @resultAcessTemp = 0;
	SET @resultGrupos = 0;

	SET @cdGrupoLeitora = 0;

	DECLARE @rtDescricao VARCHAR(50),
		@rtTemPermissao BIT;

	-- Validações de Pessoa
	SELECT TOP 1 -- Obtém dados pessoais Cracha, Papel(Colaborador/Visitante/Prestador)
		@cdPapel = p.CD_PAPEL,
		@blSupervisionado = p.BL_SUPERVISIONADO,
		@cdCracha = c.CD_CRACHA,
		@cdPessoaFisica = pf.CD_PESSOA_FISICA,
		@txCpf = pf.TX_CPF,
		@dtValidadeVisto = pf.DT_VALIDADE_VISTO,
		@vencimentoCNH = pf.DT_CNH_VALIDADE,
		@cdVisitante = vis.CD_PAPEL,
		@tipoVisitante = vis.NU_VISITA,
		@dtInicioVisitante = vis.DT_INICIO,
		@dtFimVisitante = vis.DT_FIM,
		@cdPrestador = pres.CD_PAPEL,
		@dtValidadePrestador = pres.DT_VALIDADE,
		@tpPrestador = pres.TX_PRESTADOR,
		@senhaPapel = p.TX_SENHA,
		@cdColaborador = col.CD_PAPEL,
		@numCrachaRFID = c.TX_RFID,
		@dtRegistroCracha = c.DT_REGISTRO,
		@blAtivo = c.BL_ATIVO,
		@antipassbackIndividual = ISNULL(P.BL_PASSBACK,0)
	FROM CRACHA c
	INNER JOIN PAPEL p ON p.CD_PAPEL = c.CD_PAPEL
	INNER JOIN PESSOA_FISICA pf ON pf.CD_PESSOA_FISICA = p.CD_PESSOA_FISICA
	LEFT JOIN VISITANTE vis ON vis.CD_PAPEL = p.CD_PAPEL
	LEFT JOIN COLABORADOR col ON col.CD_PAPEL = p.CD_PAPEL
	LEFT JOIN PRESTADOR pres ON pres.CD_PAPEL = p.CD_PAPEL
	--WHERE (REPLICATE('0', 14 - LEN(c.TX_RFID)) + c.TX_RFID) = @numeroCracha
	WHERE SUBSTRING(c.TX_RFID, PATINDEX('%[^0]%',c.TX_RFID),len(c.TX_RFID)) = SUBSTRING(@numeroCracha, PATINDEX('%[^0]%',@numeroCracha),len(@numeroCracha))
		AND c.BL_ATIVO = 1
	ORDER BY c.DT_REGISTRO DESC

	SELECT TOP 1 -- Obtém o status atual da pessoa
		@txStatus = s.TX_STATUS,
		@dtInicioStatus = s.DT_INICIO,
		@dtFimStatus = s.DT_FIM
	FROM PAPEL_LOG s
	WHERE s.CD_PAPEL = @cdPapel
	ORDER BY s.DT_REGISTRO DESC

	SELECT TOP 1 -- Obtém a validade da ASO	   	   
		@dtAso = a.DT_VALIDADE
	FROM ASO a
	WHERE a.CD_PAPEL = @cdPapel
	ORDER BY a.DT_VALIDADE DESC

	SELECT TOP 1 -- Obtém informações da leitora/controladora
		@cdControladora = C.CD_CONTROLADORA,
		@cdLeitora = L.CD_LEITORA,
		@direcaoLeitora = L.TX_DIRECAO,
		@txStatusControladora = C.TX_STATUS
	FROM CONTROLADORA C
	INNER JOIN LEITORA L ON L.CD_CONTROLADORA = C.CD_CONTROLADORA
	WHERE TX_IP = @ipControladora AND L.NU_ORDEM = @numLeitora AND L.BL_ATIVO = 1
	
	SELECT TOP 1 @BLEXTERNA = BL_EXTERNA, @BLSUPERVISAO = BL_SUPERVISAO, @antipassbackGrupo = ISNULL(GL.BL_ANTIPASSBACK,0)
	FROM LEITORA_GRUPO_LEITOR LGL
	INNER JOIN GRUPO_LEITORA GL ON LGL.CD_GRUPO = GL.CD_GRUPO_LEITORA
	WHERE LGL.CD_LEITORA = @cdLeitora and lgl.BL_ATIVO = 1 and gl.BL_ATIVO = 1
	ORDER BY LGL.CD_LEITORA_GRUPO_LEITORA DESC

	SET @rtDescricao = '1';
	SET @rtTemPermissao = 1;

	BEGIN TRY

		-- Interdição, liberação direta
		IF @txStatusControladora = 'Interditado'
			RAISERROR ('99',16,1);-- OCO_UR_CONTROLADORA_INTERDITADA

		IF @txStatusControladora <> 'Liberado'
		BEGIN
			BEGIN -- Autenticar Pessoa
				IF @cdPapel IS NULL
					RAISERROR ('2',16,1);-- OCO_UR_NAO_CADASTRADO
					
				IF @blAtivo = 0
					RAISERROR ('2',16,1);-- OCO_UR_NAO_CADASTRADO

				IF NOT EXISTS (SELECT TOP 1 1 FROM PAPEL_EXCECAO_ACESSO pea WHERE pea.CD_PAPEL = @cdPapel AND pea.BL_ATIVO = 1 AND pea.TX_CRITERIO_BLOQUEIO = 'ExameMedico')
				BEGIN
					IF (@cdColaborador IS NOT NULL OR (@cdPrestador IS NOT NULL AND @tpPrestador = 'Temporario')) AND @dtAso < @dAtual -- verifica aso
						RAISERROR ('93',16,1);-- OCO_UR_ASO_VENCIDO
				END

				IF @txStatus <> 'Ativo' AND (@dtInicioStatus <= @dAtual AND (@dtFimStatus IS NULL OR @dtFimStatus >= @dAtual))
				BEGIN
					IF @txStatus = 'BaixaDefinitiva'
						RAISERROR ('81',16,1);-- OCO_UR_DEMITIDO
					ELSE IF @txStatus = 'Ferias'
					BEGIN
						IF NOT EXISTS (
								SELECT TOP 1 1
								FROM PAPEL_EXCECAO_ACESSO pea
								WHERE pea.CD_PAPEL = @cdPapel
									AND pea.BL_ATIVO = 1
									AND pea.TX_CRITERIO_BLOQUEIO = 'Ferias'
								)
							RAISERROR ('95',16,1);-- OCO_UR_FERIAS
					END
					ELSE IF @txStatus = 'Afastamento'
						RAISERROR ('94',16,1);-- OCO_UR_AFASTADO
					ELSE IF @txStatus = 'LicencaMedica'
						RAISERROR ('96',16,1);-- OCO_UR_LICENCA_MEDICA
					ELSE
						RAISERROR ('82',16,1);-- OCO_UR_INATIVO
				END

				IF @txCpf IS NULL AND @dtValidadeVisto < @dAtual
					RAISERROR ('83',16,1);-- OCO_UR_VISTO_EXPIRADO

				IF @cdVisitante IS NOT NULL
				BEGIN
					IF ((@dtInicioVisitante > @dAtual OR @dtFimVisitante < @dAtual))
						RAISERROR ('84',16,1);-- OCO_UR_VISITANTE_EXPIRADO
					
				END

				IF @cdPrestador IS NOT NULL AND (@dtValidadePrestador < @dAtual)
					RAISERROR ('85',16,1);-- OCO_UR_PRESTADOR_EXPIRADO
					
				IF @cdColaborador IS NOT NULL
				BEGIN
					SELECT TOP 1 -- Obtém dados da alocação ao Pedido
						@cdAlocacao = AC.CD_ALOCACAO_COLABORADOR,
						@statusPedido = P.TX_STATUS
					FROM ALOCACAO_COLABORADOR AC
					INNER JOIN PEDIDO P ON P.CD_PEDIDO = AC.CD_PEDIDO
					INNER JOIN EMPRESA E ON E.CD_PESSOA = P.CD_EMPRESA
					WHERE AC.CD_PAPEL = @cdPapel
					ORDER BY AC.CD_ALOCACAO_COLABORADOR DESC

					IF (@cdAlocacao IS NOT NULL AND @statusPedido LIKE 'Inativo')
						RAISERROR ('86',16,1);-- OCO_PED_EMPRESA_VENCIDO
						
					-- VALIDA TURNO, GRUPO_TRABALHO
					IF NOT EXISTS (SELECT TOP 1 1 FROM PAPEL_EXCECAO_ACESSO pea WHERE (pea.CD_PAPEL = @cdPapel AND pea.BL_ATIVO = 1 AND pea.TX_CRITERIO_BLOQUEIO = 'Turno'))
					BEGIN	
						-- ACESSO TEMPORARIO
						SELECT TOP 1 @cdGrupoTrabalho = GT.CD_GRUPO_TRABALHO,
							@cdTurno = T.CD_TURNO,
							@blObedeceEscala = T.BL_ESCALA
						FROM GRUPO_TRABALHO GT
						INNER JOIN GRUPO_TRABALHO_TURNO GTT ON GTT.CD_GRUPO_TRABALHO = GT.CD_GRUPO_TRABALHO
						INNER JOIN TURNO T ON T.CD_TURNO = GTT.CD_TURNO
						INNER JOIN GRUPO_TRAB_COLAB GTC ON GTC.CD_GRUPO = GT.CD_GRUPO_TRABALHO
						WHERE CD_PAPEL = @cdPapel
							AND GT.BL_ATIVO = 1
							AND GTC.BL_ATIVO = 1
							AND GTT.BL_ATIVO = 1
							AND NOT EXISTS (
								SELECT 1
								FROM FERIADO_TURNO ft
								LEFT JOIN FERIADO f ON ft.CD_FERIADO = f.CD_FERIADO
								WHERE ft.CD_TURNO = t.CD_TURNO
									AND ft.BL_ATIVO = 1
									AND f.BL_ATIVO = 1
									AND convert(DATE, f.DT_FERIADO) = @dAtual
								)

						IF (@cdGrupoTrabalho IS NULL)
							RAISERROR ('5',16,1);-- OCO_UR_HORARIO_INVALIDO

						IF @blObedeceEscala = 0
						BEGIN
							SET @nuDiaSemana = DATEPART(DW, @dtAtual);
							SET @nutolerancia = (
									SELECT NU_TOLERANCIA
									FROM TURNO
									WHERE CD_TURNO = @cdTurno
									);

							IF (@nutolerancia IS NULL)
								SET @nutolerancia = 0;

							IF NOT EXISTS (
									SELECT TOP 1 1
									FROM HORA_TURNO
									WHERE 
										CD_TURNO = @cdTurno
										AND NU_DIA_SEMANA = @nuDiaSemana
										AND BL_ATIVO = 1
										AND (
											(
											  --dateadd(mi, - 1 * @nutolerancia, HR_INICIO) > dateadd(mi, @nutolerancia, HR_FIM)
												dateadd(mi, - 1 * @nutolerancia, dateadd(mi, DATEDIFF(MINUTE, 0, HR_INICIO), @dAtual)) > dateadd(mi, @nutolerancia, dateadd(mi, DATEDIFF(MINUTE, 0, HR_FIM), @dAtual))
												AND (
													dateadd(mi, DATEDIFF(MINUTE, 0, @hratual), @dAtual) >= dateadd(mi, - 1 * @nutolerancia, dateadd(mi, DATEDIFF(MINUTE, 0, HR_INICIO), @dAtual))
													AND dateadd(mi, DATEDIFF(MINUTE, 0, @hratual), @dAtual) < dateadd(mi, DATEDIFF(MINUTE, 0, convert(TIME, '23:59:59.9999999')), @dAtual)
													OR dateadd(mi, DATEDIFF(MINUTE, 0, @hratual), @dAtual) BETWEEN dateadd(mi, DATEDIFF(MINUTE, 0, convert(TIME, '00:00')), @dAtual)
														AND dateadd(mi, @nutolerancia, dateadd(mi, DATEDIFF(MINUTE, 0, HR_FIM), @dAtual))
													)
												)
											OR (
												dateadd(mi, - 1 * @nutolerancia, dateadd(mi, DATEDIFF(MINUTE, 0, HR_INICIO), @dAtual)) < dateadd(mi, @nutolerancia, dateadd(mi, DATEDIFF(MINUTE, 0, HR_FIM), @dAtual))
												AND (
													dateadd(mi, DATEDIFF(MINUTE, 0, @hratual), @dAtual) BETWEEN dateadd(mi, - 1 * @nutolerancia, dateadd(mi, DATEDIFF(MINUTE, 0, HR_INICIO), @dAtual))
														AND dateadd(mi, @nutolerancia, dateadd(mi, DATEDIFF(MINUTE, 0, HR_FIM), @dAtual))
													)
												)
											OR (dateadd(mi, - 1 * @nutolerancia, dateadd(mi, DATEDIFF(MINUTE, 0, HR_INICIO), @dAtual)) = dateadd(mi, @nutolerancia, dateadd(mi, DATEDIFF(MINUTE, 0, HR_FIM), @dAtual)))
											)
										
									)
								RAISERROR ('5',16,1);-- OCO_UR_HORARIO_INVALIDO
						END
						ELSE
						BEGIN
							IF NOT EXISTS (
									SELECT TOP 1 1
									FROM ESCALA E
									INNER JOIN HORA_TURNO HT ON HT.CD_HORA_TURNO = E.CD_HORA_TURNO
									INNER JOIN TURNO TU ON HT.CD_TURNO = TU.CD_TURNO --AND HT.BL_ATIVO = 1
									WHERE 
										(
											(
												HT.BL_24H = 0
												AND (
													CONVERT(DATE, E.DT_ESCALA) = CONVERT(DATE, DATEADD(MI, - 1 * TU.NU_TOLERANCIA, @dtAtual))
													OR CONVERT(DATE, E.DT_ESCALA) = CONVERT(DATE, DATEADD(MI, TU.NU_TOLERANCIA, @dtAtual))
													)
												)
											OR (
												(
													HT.BL_24H = 1
													AND (
														CONVERT(DATE, E.DT_ESCALA) = CONVERT(DATE, DATEADD(MI, - 1 * TU.NU_TOLERANCIA, @dtAtual))
														OR CONVERT(DATE, E.DT_ESCALA) = CONVERT(DATE, DATEADD(MI, TU.NU_TOLERANCIA, @dtAtual))
														)
													)
												OR (
													HT.BL_24H = 1
													AND (
														CONVERT(DATE, DATEADD(D, 1, E.DT_ESCALA)) = CONVERT(DATE, DATEADD(MI, - 1 * TU.NU_TOLERANCIA, @dtAtual))
														OR CONVERT(DATE, DATEADD(D, 1, E.DT_ESCALA)) = CONVERT(DATE, DATEADD(MI, TU.NU_TOLERANCIA, @dtAtual))
														)
													)
												)
											)
										AND E.CD_GRUPO_TRABALHO = @cdGrupoTrabalho
										AND (E.DT_ESCALA + HT.HR_INICIO) <= DATEADD(MI, TU.NU_TOLERANCIA, @dtAtual)
										AND (
											(
												HT.BL_24H = 1
												AND (DATEADD(DAY, 1, E.DT_ESCALA) + HT.HR_FIM) >= DATEADD(MI, - 1 * TU.NU_TOLERANCIA, @dtAtual)
												)
											OR (
												HT.BL_24H = 0
												AND (E.DT_ESCALA + HT.HR_FIM) >= DATEADD(MI, - 1 * TU.NU_TOLERANCIA, @dtAtual)
												)
											)
										
									)
								RAISERROR ('5',16,1);-- OCO_UR_HORARIO_INVALIDO				  
						END
					END
					--ELSE
					--BEGIN
					--	IF (@resultAcessTemp = 0)
					--		RAISERROR (
					--				'5',
					--				16,
					--				1
					--				);-- OCO_UR_VISITANTE_EXPIRADO
					--		
					--END

					
					-- VALIDA TURNO, GRUPO_TRABALHO

					IF EXISTS (SELECT TOP 1 1 FROM PAPEL_EXCECAO_ACESSO pea 
						WHERE pea.CD_PAPEL = @cdPapel AND pea.BL_ATIVO = 1 AND pea.TX_CRITERIO_BLOQUEIO = 'Antipassback')
						SET @antipassbackIndividual = 0;
					ELSE
						SET @antipassbackIndividual = 1;
					
				END		
			END

			--RESTAURANTE
			IF EXISTS (SELECT TOP 1 1 FROM dbo.fn_get_restaurantes(@cdLeitora))
			BEGIN
				IF NOT EXISTS (SELECT TOP 1 1 FROM dbo.fn_get_acessosrestaurante(@cdPapel, @cdLeitora, @dAtual, @dtAtual))
					RAISERROR ('70',16,1);--OCO_UR_RESTAURANTE_ACESSO_REST_HORARIO_INVALIDO

				IF NOT EXISTS (
						SELECT TOP 1 1
						FROM PAPEL_EXCECAO_ACESSO pea
						WHERE pea.CD_PAPEL = @cdPapel
							AND pea.BL_ATIVO = 1
							AND pea.TX_CRITERIO_BLOQUEIO = 'MultiplosAcessosRestaurante'
						)
					AND EXISTS (
						(
							SELECT TOP 1 1
							FROM dbo.fn_get_acessosrestaurante(@cdPapel, NULL, @dAtual, @dtAtual) AR
							WHERE AR.BL_CRACHA_MESTRE = 0
								AND EXISTS (
									SELECT TOP 1 1
									FROM ACESSO A
									WHERE TX_DIRECAO = @direcaoLeitora
										AND A.CD_PAPEL = @cdPapel
										AND A.TX_STATUS IN (1, 10)
										AND (
											(@dAtual + AR.HR_INICIO) < A.DT_LEITORA
											AND (@dAtual + AR.HR_FIM) > A.DT_LEITORA
											)
										AND EXISTS (
											SELECT TOP 1 1
											FROM LEITORA L
											INNER JOIN LEITORA_GRUPO_LEITOR LGL ON LGL.CD_LEITORA = L.CD_LEITORA
											INNER JOIN GRUPO_LEITORA GL ON GL.CD_GRUPO_LEITORA = LGL.CD_GRUPO
											INNER JOIN RESTAURANTE R ON R.CD_GRUPO_LEITORA = GL.CD_GRUPO_LEITORA
											WHERE A.CD_LEITORA = L.CD_LEITORA
												AND LGL.BL_ATIVO = 1
												AND l.BL_ATIVO = 1
												AND R.CD_RESTAURANTE = AR.CD_RESTAURANTE
											)
									)
							)
						)
					RAISERROR ('98',16,1);--OCO_UR_RESTAURANTE_COTA_DIARIA
						-- Verifica se é para validar acesso hierárquico (edit: antigo apenas para restaurante)
						--IF EXISTS (
						--		SELECT TOP 1 1
						--		FROM dbo.fn_get_acessosrestaurante(@cdPapel, NULL, @dAtual, @dtAtual) AR
						--		WHERE AR.BL_CRACHA_MESTRE = 0
						--			AND EXISTS (
						--				SELECT TOP 1 1
						--				FROM dbo.LOG_ACESSO_RESTAURANTE lar
						--				INNER JOIN dbo.ACESSO_RESTAURANTE ar2 ON ar2.CD_ACESSO_RESTAURANTE = lar.CD_ACESSO_RESTAURANTE
						--				INNER JOIN dbo.ACESSOREST_RESTTIPOALIMENT ar3 ON ar3.CD_ACESSO_RESTAURANTE = ar2.CD_ACESSO_RESTAURANTE
						--				WHERE ar3.CD_RESTAURANTE_TIPOALIMENTACAO = AR.CD_RESTAURANTE_TIPOALIMENTACAO
						--					AND lar.BL_NAO_VALIDA_ACESSO_HIERARQUICO = 0
						--					AND ar2.CD_PAPEL = @cdPapel
						--					AND ar2.BL_ATIVO = 1
						--				ORDER BY lar.CD_LOG_ACESSO_RESTAURANTE DESC
						--				)
						--		)
						--BEGIN
						--	-- Caso tenha de validar acesso hirárquico, entao ele verifica a ultima 
						--	IF NOT EXISTS (
						--			SELECT TOP 1 1
						--			FROM acesso a
						--			INNER JOIN (
						--				SELECT max_papel.CD_PAPEL,
						--					max(max_papel.cd_acesso) cd_acesso_max
						--				FROM acesso max_papel
						--				INNER JOIN dbo.PAPEL p ON p.CD_PAPEL = max_papel.CD_PAPEL
						--				INNER JOIN dbo.LEITORA l ON l.CD_LEITORA = max_papel.CD_LEITORA
						--				INNER JOIN dbo.LEITORA_GRUPO_LEITOR lgl ON lgl.CD_LEITORA = l.CD_LEITORA
						--				INNER JOIN dbo.GRUPO_LEITORA gl ON gl.CD_GRUPO_LEITORA = lgl.CD_GRUPO
						--				WHERE gl.BL_EXTERNA = 1
						--				GROUP BY max_papel.cd_papel
						--				) a_max ON a.CD_ACESSO = a_max.cd_acesso_max
						--			WHERE a.TX_DIRECAO LIKE 'Entrada'
						--				AND a.CD_PAPEL = @cdPapel
						--				AND a.TX_STATUS IN (1, 10)
						--			)
						--		RAISERROR (
						--				'80',
						--				16,
						--				1
						--				)
						--END;
			END
			ELSE
			BEGIN -- Consultar permissões de acesso
				-- Valida se o usuário possui permissão a alguma leitora
				--IF NOT EXISTS(SELECT TOP 1 1
				--			  FROM PAPEL_EXCECAO_ACESSO pea
				--			  WHERE pea.CD_PAPEL = @cdPapel
				--			  AND pea.BL_ATIVO = 1
				--			  AND pea.TX_CRITERIO_BLOQUEIO = 'AcessoHierarquico')
				-- BEGIN
				WITH HGrupo
				AS (
					SELECT GL.CD_GRUPO_LEITORA,
						GH.CD_GRUPO_LEITORA_PAI,
						GH.CD_GRUPO_LEITORA_FILHO,
						0 AS NIVEL
					FROM GRUPO_LEITORA GL
					LEFT JOIN GRUPO_LEITORA_HIERARQ GH ON GL.CD_GRUPO_LEITORA = GH.CD_GRUPO_LEITORA_PAI
					WHERE GL.BL_ATIVO = 1
						AND (
							GH.BL_ATIVO IS NULL
							OR GH.BL_ATIVO = 1
							)
					
					UNION ALL
					
					SELECT HG.CD_GRUPO_LEITORA,
						GH.CD_GRUPO_LEITORA_PAI,
						GH.CD_GRUPO_LEITORA_FILHO,
						HG.NIVEL + 1 AS NIVEL
					FROM HGrupo HG
					INNER JOIN GRUPO_LEITORA_HIERARQ GH ON HG.CD_GRUPO_LEITORA_FILHO = GH.CD_GRUPO_LEITORA_PAI
					WHERE GH.BL_ATIVO = 1
					)
				SELECT TOP 1 @resultGrupos = 1
				FROM HGrupo HG
				INNER JOIN LEITORA_GRUPO_LEITOR LL ON HG.CD_GRUPO_LEITORA = LL.CD_GRUPO
					OR HG.CD_GRUPO_LEITORA_PAI = LL.CD_GRUPO
					OR HG.CD_GRUPO_LEITORA_FILHO = LL.CD_GRUPO
				INNER JOIN GRUPO_LEITORA GL ON GL.CD_GRUPO_LEITORA = LL.CD_GRUPO
				INNER JOIN LEITORA L ON L.CD_LEITORA = LL.CD_LEITORA
				INNER JOIN CONTROLADORA C ON C.CD_CONTROLADORA = L.CD_CONTROLADORA
				WHERE EXISTS (
						SELECT TOP 1 1
						FROM GRUPO_LEITORA_ACESSO GLA
						INNER JOIN GRUPO_ACESSO_PAPEL GAP ON GAP.CD_GRUPO_ACESSO = GLA.CD_GRUPO_ACESSO
						INNER JOIN GRUPO_ACESSO GA ON GA.CD_GRUPO_ACESSO = GAP.CD_GRUPO_ACESSO
						WHERE GAP.CD_PAPEL = @cdPapel
							AND GA.BL_ATIVO = 1
							AND GAP.BL_ATIVO = 1
							AND GLA.BL_ATIVO = 1
							AND (
								GLA.CD_GRUPO_LEITORA = HG.CD_GRUPO_LEITORA
								OR GLA.CD_GRUPO_LEITORA = HG.CD_GRUPO_LEITORA_PAI
								OR GLA.CD_GRUPO_LEITORA = HG.CD_GRUPO_LEITORA_FILHO
								)
						)
					AND GL.BL_ATIVO = 1
					AND LL.BL_ATIVO = 1
					AND L.BL_ATIVO = 1
					AND C.BL_ATIVO = 1
					AND L.CD_LEITORA = @cdLeitora
					
				IF EXISTS (
					SELECT TOP 1 1
					FROM ACESSO_TEMPORARIO AT
					INNER JOIN LEITORA_GRUPO_LEITOR LGL ON AT.CD_GRUPO_LEITORA = LGL.CD_GRUPO
					INNER JOIN LEITORA L ON LGL.CD_LEITORA = L.CD_LEITORA
					WHERE @dtAtual >= AT.DT_HR_VALIDADE_INICIO 
						AND @dtAtual < AT.DT_HR_VALIDADE_FIM
						AND AT.BL_ATIVO = 1
						AND L.BL_ATIVO = 1
						AND LGL.BL_ATIVO = 1
						AND L.CD_CONTROLADORA = @CDCONTROLADORA
						AND AT.CD_PAPEL = @cdPapel)
					SET @resultAcessTemp = 1;-- CHECA ACESSO_TEMPORARIO
					
				if(@resultGrupos = 0 AND @resultAcessTemp = 0)
					RAISERROR ('7',16,1);-- OCO_UR_HORARIO_INVALIDO

				--Verifica se existem grupos de leitora que validam CNH
				IF EXISTS (
						SELECT TOP 1 1
						FROM GRUPO_LEITORA GL
						INNER JOIN LEITORA_GRUPO_LEITOR LGL ON LGL.CD_GRUPO = GL.CD_GRUPO_LEITORA
						INNER JOIN LEITORA L ON L.CD_LEITORA = LGL.CD_LEITORA
						INNER JOIN CONTROLADORA C ON C.CD_CONTROLADORA = L.CD_CONTROLADORA
						WHERE L.CD_LEITORA = @cdLeitora
							AND GL.BL_CNH = 1
							AND LGL.BL_ATIVO = 1
							AND GL.BL_ATIVO = 1
							AND C.NU_TIPO_CNTRL = 3
							AND EXISTS (
								SELECT TOP 1 1
								FROM GRUPO_LEITORA_ACESSO GLA
								INNER JOIN GRUPO_ACESSO_PAPEL GAP ON GAP.CD_GRUPO_ACESSO = GLA.CD_GRUPO_ACESSO
								INNER JOIN GRUPO_ACESSO GA ON GA.CD_GRUPO_ACESSO = GAP.CD_GRUPO_ACESSO
								WHERE GAP.CD_PAPEL = @cdPapel
									AND GA.BL_ATIVO = 1
									AND GAP.BL_ATIVO = 1
									AND GLA.BL_ATIVO = 1
									AND GL.CD_GRUPO_LEITORA = GLA.CD_GRUPO_LEITORA
								)
						)
					AND (
						@vencimentoCNH IS NULL
						OR @vencimentoCNH < @dAtual
						)
					RAISERROR ('87',16,1);-- OCO_UR_CNH_VENCIDA

				-- Verifica se existem grupos de leitora que possuem treinamentos e se a pessoa tem algum treinamento necessário não feito
				IF NOT EXISTS (SELECT TOP 1 1 FROM PAPEL_EXCECAO_ACESSO pea WHERE pea.CD_PAPEL = @cdPapel AND pea.BL_ATIVO = 1 AND pea.TX_CRITERIO_BLOQUEIO = 'Treinamento')
				BEGIN
					IF EXISTS (
							SELECT TOP 1 1
							FROM GRUPO_LEITORA GL
							INNER JOIN GRUPO_LEITORA_TREIN GLT ON GLT.CD_GRUPO_LEITORA = GL.CD_GRUPO_LEITORA
							INNER JOIN LEITORA_GRUPO_LEITOR LGL ON LGL.CD_GRUPO = GL.CD_GRUPO_LEITORA
							WHERE CD_LEITORA = @cdLeitora
								AND GL.BL_ATIVO = 1
								AND GLT.BL_ATIVO = 1
								AND LGL.BL_ATIVO = 1
								AND (
									CD_TREINAMENTO NOT IN (
										SELECT CD_TREINAMENTO
										FROM TREINAMENTO_PESSOA
										WHERE CD_PAPEL = @cdPapel
											AND BL_ATIVO = 1
										)
									OR CD_TREINAMENTO IN (
										SELECT CD_TREINAMENTO
										FROM TREINAMENTO_PESSOA
										WHERE CD_PAPEL = @cdPapel
											AND DT_VALIDADE < @dAtual
											AND BL_ATIVO = 1
										)
									)
								AND EXISTS (
									SELECT TOP 1 1
									FROM GRUPO_LEITORA_ACESSO GLA
									INNER JOIN GRUPO_ACESSO_PAPEL GAP ON GAP.CD_GRUPO_ACESSO = GLA.CD_GRUPO_ACESSO
									INNER JOIN GRUPO_ACESSO GA ON GA.CD_GRUPO_ACESSO = GAP.CD_GRUPO_ACESSO
									WHERE GAP.CD_PAPEL = @cdPapel
										AND GA.BL_ATIVO = 1
										AND GAP.BL_ATIVO = 1
										AND GLA.BL_ATIVO = 1
										AND GL.CD_GRUPO_LEITORA = GLA.CD_GRUPO_LEITORA
									)
							)
						RAISERROR ('88',16,1);-- OCO_UR_TREINAMENTO
				END
			END
			
			--VALIDA ACESSO HIERARQUICO NOVO
			IF NOT EXISTS (SELECT TOP 1 1
						   FROM PAPEL_EXCECAO_ACESSO pea
						   WHERE pea.CD_PAPEL = @cdPapel
							 AND pea.BL_ATIVO = 1
							 AND pea.TX_CRITERIO_BLOQUEIO = 'AcessoHierarquico')
				AND NOT EXISTS (SELECT TOP 1 1
								FROM RESTAURANTE R
								INNER JOIN GRUPO_LEITORA GL ON R.CD_GRUPO_LEITORA = GL.CD_GRUPO_LEITORA
								INNER JOIN LEITORA_GRUPO_LEITOR LGL ON GL.CD_GRUPO_LEITORA = LGL.CD_GRUPO
								INNER JOIN RESTAURANTE_TIPOALIMENTACAO RTA ON R.CD_RESTAURANTE = RTA.CD_RESTAURANTE
								INNER JOIN ACESSOREST_RESTTIPOALIMENT ARRTA ON RTA.CD_RESTAURANTE_TIPOALIMENTACAO = ARRTA.CD_RESTAURANTE_TIPOALIMENTACAO
								INNER JOIN ACESSO_RESTAURANTE AR ON ARRTA.CD_ACESSO_RESTAURANTE = AR.CD_ACESSO_RESTAURANTE
								INNER JOIN LOG_ACESSO_RESTAURANTE LAR ON AR.CD_ACESSO_RESTAURANTE = LAR.CD_ACESSO_RESTAURANTE
								INNER JOIN (
									SELECT CD_ACESSO_RESTAURANTE
										,MAX(CD_LOG_ACESSO_RESTAURANTE) CD_LOG_ACESSO_RESTAURANTE
									FROM LOG_ACESSO_RESTAURANTE
									GROUP BY CD_ACESSO_RESTAURANTE
									) MAX_LAR ON LAR.CD_LOG_ACESSO_RESTAURANTE = MAX_LAR.CD_LOG_ACESSO_RESTAURANTE
								WHERE AR.CD_PAPEL = @CDPAPEL
									AND LGL.CD_LEITORA = @CDLEITORA
									AND R.BL_ATIVO = 1
									AND GL.BL_ATIVO = 1
									AND LGL.BL_ATIVO = 1
									AND RTA.BL_ATIVO = 1
									AND ARRTA.BL_ATIVO = 1
									AND AR.BL_ATIVO = 1
									AND LAR.BL_NAO_VALIDA_ACESSO_HIERARQUICO = 1)
				AND @BLEXTERNA = 0				
				AND @direcaoLeitora = 'Entrada'
				AND NOT EXISTS (
					SELECT TOP 1 1
					FROM acesso a
					INNER JOIN (
						SELECT max_papel.CD_PAPEL,
							max(max_papel.cd_acesso) cd_acesso_max
						FROM acesso max_papel
						INNER JOIN LEITORA L ON max_papel.CD_LEITORA = L.CD_LEITORA
						INNER JOIN LEITORA_GRUPO_LEITOR LGL ON L.CD_LEITORA = LGL.CD_LEITORA
						INNER JOIN GRUPO_LEITORA GL ON GL.CD_GRUPO_LEITORA = lgl.CD_GRUPO
						WHERE gl.BL_EXTERNA = 1
							AND max_papel.CD_PAPEL = @cdPapel
						GROUP BY max_papel.cd_papel
						) a_max ON a.CD_ACESSO = a_max.cd_acesso_max
					WHERE a.TX_DIRECAO LIKE 'Entrada'
						AND a.TX_STATUS IN (1, 10)
					)
				RAISERROR ('80',16,1)

			--VALIDA ACESSO SUPERVISIONADO
			IF @BLSUPERVISAO = 1 AND @direcaoLeitora LIKE 'Entrada' AND @blSupervisionado = 1
			BEGIN
				IF NOT EXISTS (
						SELECT TOP 1 1
						FROM acesso a
						INNER JOIN (
							SELECT max_papel.CD_PAPEL,
								max(max_papel.cd_acesso) cd_acesso_max
							FROM acesso max_papel
							INNER JOIN papel p ON max_papel.CD_PAPEL = p.CD_PAPEL
							WHERE max_papel.CD_LEITORA = @cdLeitora
								AND max_papel.CD_PAPEL <> @cdPapel
								AND p.BL_SUPERVISOR = 1
							GROUP BY max_papel.cd_papel
							) a_max ON a.CD_ACESSO = a_max.cd_acesso_max
						WHERE a.TX_DIRECAO LIKE 'Entrada'
							AND a.TX_STATUS IN (1, 10)
						)
					RAISERROR ('91',16,1);-- OCO_UR_SUPERVISIONADO
			END;

			BEGIN -- Consultar acesso customizado		
				IF @senha IS NOT NULL AND @senha <> @senhaPapel
					RAISERROR ('89',16,1);-- OCO_UR_SENHA_INVALIDA
				
				SET @usaAntipassback = (
						SELECT TX_VALOR
						FROM PARAMETRO
						WHERE TX_NOME = 'Antipassback'
						);
					
				IF (@usaAntipassback = 'Sim' OR (
					(@antipassbackGrupo = 1) AND (
						(@cdColaborador IS NOT NULL OR @cdPrestador IS NOT NULL OR @cdVisitante IS NOT NULL) AND @antipassbackIndividual = 1
					) AND NOT EXISTS (SELECT TOP 1 1 FROM PAPEL_EXCECAO_ACESSO pea WHERE pea.CD_PAPEL = @cdPapel AND 
					pea.BL_ATIVO = 1 AND pea.TX_CRITERIO_BLOQUEIO = 'Antipassback'))
				) --AND (@cdGrupoLeitora > 0)
                BEGIN
					
					
                    SELECT TOP 1 @ultimoAcessoDirecao = a.TX_DIRECAO,
                        @statusUltimoAcesso = a.TX_STATUS,
						@codAcessoTemp = a.CD_ACESSO
                    FROM ACESSO a
					INNER JOIN dbo.LEITORA l ON a.CD_LEITORA = l.CD_LEITORA
					INNER JOIN dbo.LEITORA_GRUPO_LEITOR lgl ON l.CD_LEITORA = lgl.CD_LEITORA
					INNER JOIN dbo.GRUPO_LEITORA gl ON lgl.CD_GRUPO = gl.CD_GRUPO_LEITORA
                    WHERE a.CD_PAPEL = @cdPapel
                        AND a.CD_CRACHA = @cdCracha
                        AND a.TX_STATUS IN (1, 10, 11)
						AND gl.CD_GRUPO_LEITORA IN (
							SELECT TOP 1 ISNULL(GL.CD_GRUPO_LEITORA,0)
							FROM GRUPO_LEITORA GL 
							INNER JOIN LEITORA_GRUPO_LEITOR LGL ON LGL.CD_GRUPO = GL.CD_GRUPO_LEITORA
							INNER JOIN LEITORA L ON L.CD_LEITORA = LGL.CD_LEITORA
							INNER JOIN CONTROLADORA C ON C.CD_CONTROLADORA = L.CD_CONTROLADORA
							WHERE L.CD_LEITORA = @cdLeitora
								AND L.BL_ATIVO = 1
								AND (
									(@usaAntipassback = 'Sim' AND GL.BL_EXTERNA = 1) OR
									(GL.BL_ANTIPASSBACK = 1)
									--(@antipassbackGrupo = 1)
								)
								AND GL.BL_ATIVO = 1
								AND LGL.BL_ATIVO = 1
								AND C.BL_ATIVO = 1
						)
					ORDER BY a.DT_LEITORA DESC;

                    -- Verifica se o ultimo status do papel é igual a Acesso Liberado (1), Entrada Completa (10) ou Saída Completa (11)
                    -- e se a direção é diferente de saída. Se for, antipassback.       
                    IF @direcaoLeitora = 'Entrada'
                    BEGIN
                        IF (@ultimoAcessoDirecao <> 'Saida')
                            RAISERROR (
                                    '90',
                                    16,
                                    1
                                    );-- OCO_UR_ANTIPASSBACK        
                    END
                END

			END
		END
	    ----Fim do Liberado
	END TRY

	BEGIN CATCH
		SET @rtDescricao = ERROR_MESSAGE();
		SET @rtTemPermissao = 0;
	END CATCH

	SET @direcaoLeitora = SUBSTRING(@direcaoLeitora, 0, 2)

	SELECT CONVERT(INT, @rtDescricao), @rtTemPermissao, @direcaoLeitora
	--,@antipassbackIndividual AS 'AntiPassIndi', @cdColaborador AS 'cd_colab', @antipassbackGrupo AS 'AntiPassGrp'
END
GO
