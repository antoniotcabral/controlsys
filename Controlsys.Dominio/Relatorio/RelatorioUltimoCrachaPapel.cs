﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Relatorio
{
    public class RelatorioUltimoCrachaPapel
    {

        public virtual int CodigoPapel { get; set; }

        public virtual string TipoPapel { get; set; }

        public virtual string NomePessoa { get; set; }

        public virtual string Cargo { get; set; }

        public virtual string CPF { get; set; }

        public virtual string Passaporte { get; set; }

        public virtual string Status { get; set; }

        public virtual string CNPJEmpresa { get; set; }

        public virtual string ApelidoEmpresa { get; set; }

        public virtual string NomeEmpresa { get; set; }

        public virtual string CNPJEmpresaAlocacao { get; set; }

        public virtual string ApelidoEmpresaAlocacao { get; set; }

        public virtual string NomeEmpresaAlocacao { get; set; }

        public virtual string CNPJContratada { get; set; }

        public virtual string ApelidoContratada { get; set; }

        public virtual string NomeContratada { get; set; }

        public virtual int CodigoCracha { get; set; }

        public virtual string RFID { get; set; }

        public virtual Boolean StatusCracha { get; set; }

        public virtual DateTime DataRegistroCracha { get; set; }

        public virtual DateTime DataDesativacaoCracha { get; set; }

        public virtual DateTime DataDescarteCracha { get; set; }

        public virtual DateTime DataImpressaoCracha { get; set; }

        public virtual string Motivo { get; set; }

    }
}
