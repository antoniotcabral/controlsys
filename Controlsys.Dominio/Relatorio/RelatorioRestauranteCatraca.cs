﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Relatorio
{
    public class RelatorioRestauranteCatraca
    {
        public virtual int CodigoAcesso { get; set; }

        public virtual int CodigoPapel { get; set; }

        public virtual string CodigoEmpresa { get; set; }

        public virtual int CodigoTipoAlimentacao { get; set; }

        public virtual int CodigoSetorCusto { get; set; }

        public virtual int CodigoRestaurante { get; set; }

        public virtual string NumeroPO { get; set; }

        public virtual string Empresa { get; set; }

        public virtual string Restaurante { get; set; }

        public virtual string TipoAlimentacao { get; set; }

        public virtual DateTime DataLeitora { get; set; }
    }
}
