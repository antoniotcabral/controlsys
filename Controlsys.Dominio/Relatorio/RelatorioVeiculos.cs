﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Relatorio
{
    public class RelatorioVeiculos
    {
        public virtual string Codigo { get; set; }
        public virtual int CodigoPapel { get; set; }
        public virtual string CodigoPessoaFisica { get; set; }
        public virtual string CPF { get; set; }
        public virtual string Passaporte { get; set; }
        public virtual string RG { get; set; }
        public virtual string Nome { get; set; }
        public virtual string Cargo { get; set; }
        public virtual string TipoPapel { get; set; }
        public virtual DateTime DataRegistroPessoa { get; set; }
        public virtual DateTime DataRegistroPapel { get; set; }
        public virtual string StatusPapel { get; set; }
        public virtual DateTime DataRegistroStatusPapel { get; set; }
        public virtual DateTime? DataValidadeASO { get; set; }
        public virtual string MotivoPapelLog { get; set; }

        public virtual string Cracha { get; set; }
        public virtual bool StatusCracha { get; set; }
        public virtual DateTime DataRegistroCracha { get; set; }

        public virtual string CNPJ { get; set; }
        public virtual string RazaoSocial { get; set; }
        public virtual string NomeFantasia { get; set; }
    }
}