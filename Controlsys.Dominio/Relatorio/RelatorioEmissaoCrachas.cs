﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Relatorio
{
    public class RelatorioEmissaoCrachas
    {

        public virtual int QtdCrachas { get; set; }

        public virtual string CNPJEmpresa { get; set; }

        public virtual string NomeEmpresa { get; set; }

        public virtual string CNPJContratada { get; set; }

        public virtual string NomeContratada { get; set; }

        //public virtual string CNPJEmpresa { get; set; }

        //public virtual string NomeEmpresa { get; set; }

        //public virtual string CNPJContratada { get; set; }

        //public virtual string NomeContratada { get; set; }

        //public virtual int QtdCrachas { get; set; }
    }
}
