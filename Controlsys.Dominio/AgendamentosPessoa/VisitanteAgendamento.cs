﻿using Controlsys.Dominio.Enderecos;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Controlsys.Dominio.Seguranca;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.AgendamentosPessoa
{
    public class VisitanteAgendamento : PapelAgendamento
    {
        //
        public virtual bool AcessoPorto { get; set; }

        public virtual string Empresa { get; set; }

        public virtual Colaborador ResponsavelVisita { get; set; }

        public virtual string Motivo { get; set; }

        public virtual string Funcao { get; set; }

        public virtual DateTime DataInicio { get; set; }

        public virtual DateTime DataFim { get; set; }                
    }
}
