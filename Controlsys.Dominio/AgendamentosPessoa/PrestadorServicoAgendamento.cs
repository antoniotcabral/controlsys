﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Pessoas;

namespace Controlsys.Dominio.AgendamentosPessoa
{
    public class PrestadorServicoAgendamento : PapelAgendamento
    {                
        /// <summary>
        /// Gets/Sets valor para Fornecedor.
        /// </summary>
        ///
        /// <value>
        /// Informa caso o tipo do prestador seja emergencial
        /// Retorna o(a) Fornecedor.
        /// </value>
        public virtual Fornecedor Fornecedor { get; set; }

        public virtual Empresa Empresa { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataAdmissao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataAdmissao.
        /// </value>
        public virtual DateTime? DataAdmissao { get; set; }

        /// <summary>
        /// Gets/Sets valor para TipoPrestador.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TipoPrestador.
        /// </value>
        public virtual TipoPrestador TipoPrestador { get; set; }

        /// <summary>
        /// Gets/Sets valor para TipoPrestador.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TipoPrestador.
        /// </value>
        public virtual Colaborador GestorResponsavel { get; set; }

        /// <summary>
        /// Gets/Sets valor para Observacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Observacao.
        /// </value>
        public virtual string Observacao { get; set; }
    }
}
