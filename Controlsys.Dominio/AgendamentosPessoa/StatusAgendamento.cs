﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Controlsys.Dominio.AgendamentosPessoa
{
    public enum StatusAgendamento
    {
        [Description("Cadastro Aprovado")]
        CadastroAprovado,

        [Description("Cadastro Não Aprovado")]
        CadastroNaoAprovado,

        [Description("Aguardando Aprovação")]
        AguardandoAprovacao,

        [Description("Aguardando Autorização")]
        AguardandoAutorizacao,

        [Description("Contratação Autorizada")]
        ContratacaoAutorizada,

        [Description("Contratação Não Autorizada")]
        ContratacaoNaoAutorizada,

        [Description("Cadastro Pré-Aprovado")]
        CadastroPreAprovado,
    }
}
