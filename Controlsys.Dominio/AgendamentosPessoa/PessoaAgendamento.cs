﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Enderecos;
using Controlsys.Dominio.Pessoas;

namespace Controlsys.Dominio.AgendamentosPessoa
{
    public class PessoaAgendamento
    {
        public virtual int Codigo { get; set; }

        public virtual Logradouro Endereco { get; set; }

        public virtual string EnderecoNumero { get; set; }

        public virtual string EnderecoComplemento { get; set; }

        public virtual string CPF { get; set; }

        public virtual string Passaporte { get; set; }

        public virtual string Nome { get; set; }

        public virtual string Apelido { get; set; }

        public virtual DateTime DataRegistro { get; set; }

        public virtual string Email { get; set; }

        public virtual Sexo Sexo { get; set; }

        public virtual string TelefoneNumResidencial { get; set; }

        public virtual string TelefoneNumCelular { get; set; }

        public virtual string TelefoneNumEmergencial { get; set; }

        public virtual string RG { get; set; }

        public virtual string RGOrgaoEmissor { get; set; }

        public virtual Estado RGOrgaoEmissorUF { get; set; }

        public virtual DateTime? RGDataEmissao { get; set; }

        public virtual byte[] Foto { get; set; }

        public virtual TipoSanguineo? TipoSanguineo { get; set; }

        /// <summary>
        /// Gets/Sets valor para EstadoCivil.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) EstadoCivil.
        /// </value>
        public virtual EstadoCivil? EstadoCivil { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataExpedicaoRNE.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataExpedicaoRNE.
        /// </value>
        public virtual DateTime? DataExpedicaoRNE { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataNascimento.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataNascimento.
        /// </value>
        public virtual DateTime? DataNascimento { get; set; }

        /// <summary>
        /// Gets/Sets valor para NomePai.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) NomePai.
        /// </value>
        public virtual string NomePai { get; set; }

        /// <summary>
        /// Gets/Sets valor para NomeMae.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) NomeMae.
        /// </value>
        public virtual string NomeMae { get; set; }
        /// <summary>
        /// Gets/Sets valor para Nacionalidade.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Nacionalidade.
        /// </value>
        public virtual string Nacionalidade { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataVisto.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataVisto.
        /// </value>
        public virtual DateTime? DataVisto { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataValidadeVisto.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataValidadeVisto.
        /// </value>
        public virtual DateTime? DataValidadeVisto { get; set; }        

        /// <summary>
        /// Gets/Sets valor para Naturalidade.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Naturalidade.
        /// </value>
        public virtual Cidade Naturalidade { get; set; }

        /// <summary>
        /// Gets/Sets valor para CTPSSerie.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CTPSSerie.
        /// </value>
        public virtual string CTPSSerie { get; set; }

        /// <summary>
        /// Gets/Sets valor para CTPS.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CTPS.
        /// </value>
        public virtual long? CTPS { get; set; }

        /// <summary>
        /// Gets/Sets valor para CTPSEstado.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CTPSEstado.
        /// </value>
        public virtual Estado CTPSEstado { get; set; }

        /// <summary>
        /// Gets/Sets valor para CTPSData.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CTPSData.
        /// </value>
        public virtual DateTime? CTPSData { get; set; }

        /// <summary>
        /// Gets/Sets valor para CNH.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CNH.
        /// </value>
        public virtual string CNH { get; set; }

        /// <summary>
        /// Gets/Sets valor para CNHCategoria.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CNHCategoria.
        /// </value>
        public virtual CategoriaCNH? CNHCategoria { get; set; }

        /// <summary>
        /// Gets/Sets valor para CNHDataValidade.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CNHDataValidade.
        /// </value>
        public virtual DateTime? CNHDataValidade { get; set; }

        /// <summary>
        /// Gets/Sets valor para TituloEleitor.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TituloEleitor.
        /// </value>
        public virtual long? TituloEleitor { get; set; }

        /// <summary>
        /// Gets/Sets valor para TituloEleitorZona.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TituloEleitorZona.
        /// </value>
        public virtual int? TituloEleitorZona { get; set; }

        /// <summary>
        /// Gets/Sets valor para TituloEleitorSecao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TituloEleitorSecao.
        /// </value>
        public virtual int? TituloEleitorSecao { get; set; }

        /// <summary>
        /// Gets/Sets valor para TituloEleitorCidade.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TituloEleitorCidade.
        /// </value>
        public virtual Cidade TituloEleitorCidade { get; set; }

        /// <summary>
        /// Gets/Sets valor para CertificadoReservista.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CertificadoReservista.
        /// </value>
        public virtual string CertificadoReservista { get; set; }

        /// <summary>
        /// Gets/Sets valor para CertificadoReservistaCat.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CertificadoReservistaCat.
        /// </value>
        public virtual string CertificadoReservistaCat { get; set; }

        /// <summary>
        /// Gets/Sets valor para RNE.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) RNE.
        /// </value>
        public virtual string RNE { get; set; }

        /// <summary>
        /// Gets/Sets valor para Escolaridade.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Escolaridade.
        /// </value>
        public virtual Escolaridade? Escolaridade { get; set; }

        /// <summary>
        /// Gets/Sets valor para Emails.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Emails.
        /// </value>
        //public virtual IList<Email> Emails { get; set; }

        public virtual string CPFFormatado()
        {
            return CPF != null ? string.Format(@"{0:000\.000\.000\-00}", Convert.ToInt64(CPF)) : null;
        } 
    }
}
