﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;

namespace Controlsys.Dominio.AgendamentosPessoa
{
    public class ColaboradorAgendamento : PapelAgendamento
    {        
        /// <summary>
        /// Gets/Sets valor para PIS.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) PIS.
        /// </value>
        public virtual string PIS { get; set; }

        /// <summary>
        /// Gets/Sets valor para Observacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Observacao.
        /// </value>
        public virtual string Observacao { get; set; }

        /// <summary>
        /// Gets/Sets valor para HoristaMensalista.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) HoristaMensalista.
        /// </value>
        public virtual HoristaMensalista HoristaMensalista { get; set; }

        /// <summary>
        /// Gets/Sets valor para Salario.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Salario.
        /// </value>
        public virtual decimal Salario { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataAdmissao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataAdmissao.
        /// </value>
        public virtual DateTime? DataAdmissao { get; set; }

        /// <summary>
        /// Gets/Sets valor para ConselhoProfissional.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) ConselhoProfissional.
        /// </value>
        public virtual string ConselhoProfissional { get; set; }

        /// <summary>
        /// Gets/Sets valor para RegistroProfissional.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) RegistroProfissional.
        /// </value>
        public virtual string RegistroProfissional { get; set; }

        /// <summary>
        /// Gets/Sets valor para GruposTrabalho.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) GruposTrabalho.
        /// </value>
        public virtual GrupoTrabalho GrupoTrabalho { get; set; }

        public virtual Empresa Empresa { get; set; }
    
        
        
    
    }
}
