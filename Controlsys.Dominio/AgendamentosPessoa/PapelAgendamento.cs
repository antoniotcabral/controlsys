﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Controlsys.Dominio.Seguranca;

namespace Controlsys.Dominio.AgendamentosPessoa
{
    public abstract class PapelAgendamento
    {

        public PapelAgendamento()
        {
            Status = new List<StatusPapelAgendamento>();
            Documentos = new List<DocumentoPapel>();
            Treinamentos = new List<Treinamento>();
        }

        public virtual int Codigo { get; set; }

        public virtual Area Area { get; set; }

        public virtual bool PessoaJuridica { get; set; }

        public virtual DateTime DataRegistro { get; set; }

        public virtual Usuario Solicitante { get; set; }

        public virtual Cargo Cargo { get; set; }

        public virtual PessoaAgendamento PessoaAg { get; set; }

        public virtual Papel Papel { get; set; }

        public virtual EmpregadoSAP EmpregadoSap { get; set; }

        public virtual SetorCusto SetorCusto { get; set; }

        public virtual IList<DocumentoPapel> Documentos { get; set; }

        public virtual IList<Treinamento> Treinamentos { get; set; }

        public virtual ColaboradorPedido GestorPO { get; set; }

        public virtual IList<StatusPapelAgendamento> Status { get; set; }

        public virtual StatusPapelAgendamento ObterStatusAtual()
        {
            return Status.OrderByDescending(spa => spa.Codigo).FirstOrDefault();
        }

        public virtual StatusPapelAgendamento InserirPapelAg(StatusAgendamento statusAgendamento, string observacao = null)
        {
            StatusPapelAgendamento statusPapelAg = new StatusPapelAgendamento();
            statusPapelAg.StatusAgendamento = statusAgendamento;
            statusPapelAg.PapelAgendamento = this;
            statusPapelAg.Observacao = observacao;
            statusPapelAg.DataRegistro = DateTime.Now;

            Status.Add(statusPapelAg);
            return statusPapelAg;

        }
    }
}
