﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controlsys.Dominio.AgendamentosPessoa
{
    public class StatusPapelAgendamento
    {
        public virtual int Codigo { get; set; }

        public virtual StatusAgendamento StatusAgendamento { get; set; }

        public virtual PapelAgendamento PapelAgendamento { get; set; }

        public virtual string Observacao { get; set; }

        public virtual DateTime DataRegistro { get; set; }

    }
}
