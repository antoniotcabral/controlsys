﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Seguranca;

namespace Controlsys.Dominio.Audit
{
    /// <summary>
    /// Representa um(a) Auditoria.
    /// </summary>
    public class Auditoria
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public virtual int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para URL.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) URL.
        /// </value>
        public virtual string Url { get; set; }

        /// <summary>
        /// Gets/Sets valor para Acao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Acao.
        /// </value>
        public virtual Acao Acao { get; set; }

        /// <summary>
        /// Gets/Sets valor para Usuario.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Usuario.
        /// </value>
        public virtual Usuario Usuario { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataRegistro.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataRegistro.
        /// </value>
        public virtual DateTime DataRegistro { get; set; }

        /// <summary>
        /// Gets/Sets valor para Descricao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Descricao.
        /// </value>
        public virtual string Descricao { get; set; }

    }
}
