﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Audit
{
    /// <summary>
    /// Representa um(a) IntegracaoSAP.
    /// </summary>
    public class IntegracaoSAP
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public virtual int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataIntegracao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataIntegracao.
        /// </value>
        public virtual DateTime? DataIntegracao { get; set; }

        /// <summary>
        /// Gets/Sets valor para Status.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Status.
        /// </value>
        public virtual int Status { get; set; }

        /// <summary>
        /// Gets/Sets valor para Mensagem.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Mensagem.
        /// </value>
        public virtual string Mensagem { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataAtualizacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataAtualizacao.
        /// </value>
        public virtual DateTime DataAtualizacao { get; set; }

        /// <summary>
        /// Gets/Sets valor para TipoAtualizacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TipoAtualizacao.
        /// </value>
        public virtual int TipoAtualizacao { get; set; }
    }
}
