﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Audit
{
    /// <summary>
    /// Valores que representam StatusIntegracao.
    /// </summary>
    public enum StatusIntegracao
    {
        /// <summary>
        /// An enum constant representing the Pendente option.
        /// </summary>
        [Description("Pendente")]
        Pendente = 1,
        /// <summary>
        /// An enum constant representing the Sucesso option.
        /// </summary>
        [Description("Sucesso")]
        Sucesso = 2,
        /// <summary>
        /// An enum constant representing the licenca Falha option.
        /// </summary>
        [Description("Falha")]
        Falha = 3
    }
}
