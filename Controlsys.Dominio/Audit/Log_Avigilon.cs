﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Audit
{
   public  class Log_Avigilon
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public virtual int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Message.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Message.
        /// </value>
        public virtual string Message { get; set; }

        /// <summary>
        /// Gets/Sets valor para StackTrace.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) StackTrace.
        /// </value>
        public virtual string StackTrace { get; set; }

        /// <summary>
        /// Gets/Sets valor para URL.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) URL.
        /// </value>
        public virtual string Url { get; set; }

        /// <summary>
        /// Gets/Sets valor para UserName.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UserName.
        /// </value>
        public virtual string UserName { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataRegistro.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataRegistro.
        /// </value>
        public virtual DateTime DataRegistro { get; set; }
    }

    
}
