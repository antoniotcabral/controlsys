﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Audit
{
    /// <summary>
    /// Valores que representam TipoIntegracao.
    /// </summary>
    public enum TipoIntegracao
    {
        /// <summary>
        /// An enum constant representing the Atualizacao_Enviada option.
        /// </summary>
        [Description("Atualizacao_Enviada")]
        Atualizacao_Enviada = 1,
        /// <summary>
        /// An enum constant representing the Atualizacao_Recebida option.
        /// </summary>
        [Description("Atualizacao_Recebida")]
        Atualizacao_Recebida = 2,
    }
}
