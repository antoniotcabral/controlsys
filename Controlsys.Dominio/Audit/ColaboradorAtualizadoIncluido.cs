﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Audit
{
    /// <summary>
    /// Representa o controle para o ColaboradorAtualizadoIncluido.
    /// </summary>
    public class ColaboradorAtualizadoIncluido 
    {
        /// <summary>
        /// Gets/Sets valor para CDColaborador
        /// </summary>
        /// 
        /// <value>
        /// Retorna o CDColaborador
        /// </value>
        public virtual int CDColaborador { get; set; }

        /// <summary>
        /// Gets/Sets valor para CDPapel
        /// </summary>
        /// 
        /// <value>
        /// Retorna o CDPapel
        /// </value>
        public virtual int CDPapel { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataRegistro
        /// </summary>
        /// 
        /// <value>
        /// Retorna a DataRegistro
        /// </value>
        /// 
        public virtual DateTime DataRegistro { get; set; }

        /// <summary>
        /// Gets/Sets valor para MensagemRecebida
        /// </summary>
        /// 
        /// <value>
        /// Retorna a MensagemRecebida
        /// </value>
        public virtual string MensagemRecebida { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataEnvio
        /// </summary>
        /// 
        /// <value>
        /// Retorna a DataEnvio
        /// </value>
        public virtual DateTime? DataEnvio { get; set; }

        /// <summary>
        /// Gets/Sets valor para MensagemLog
        /// </summary>
        /// 
        /// <value>
        /// Retorna a MensagemLog
        /// </value>
        public virtual string MensagemLog { get; set; }
    }
}
