﻿using Controlsys.Dominio.Acesso;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Audit
{
    /// <summary>
    /// Representa um(a) IntegracaoSAPAcesso.
    /// </summary>
    public class IntegracaoSAPAcesso
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public virtual int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Acesso.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Acesso.
        /// </value>
        public virtual ControleAcesso ControleAcesso { get; set; }

        /// <summary>
        /// Gets/Sets valor para IntegracaoSAP.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) IntegracaoSAP.
        /// </value>
        public virtual IntegracaoSAP IntegracaoSAP { get; set; }
    }
}
