﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;

namespace Controlsys.Dominio.Ocorrencias
{
    public class OcorrenciaTransito
    {
        public virtual int Codigo { get; set; }

        public virtual Papel Papel { get; set; }

        public virtual PessoaFisica PessoaFisica { get; set; }        

        public virtual Infracao Infracao { get; set; }

        public virtual Penalidade Penalidade { get; set; }

        public virtual Local Local { get; set; }

        public virtual DateTime DataOcorrencia { get; set; }

        public virtual string NumeroCracha { get; set; }

        public virtual bool Anulada { get; set; }

        public virtual DateTime DataRegistro { get; set; }

        public virtual DateTime? DataDesativacao { get; set; }

        public virtual bool Ativo { get; set; }

        public virtual IList<TreinamentoOcorrenciaTransito> Treinamentos { get; set; }        

        public virtual TreinamentoOcorrenciaTransito ObterTreinamento()
        {
            return Treinamentos.Where(tot => !tot.CompareceuTreinamento.HasValue).OrderByDescending(t => t.Codigo).FirstOrDefault();
        }

        public virtual void Ativar()
        {
            Ativo = true;
            DataDesativacao = null;
        }

        public virtual void Inativar()
        {
            Ativo = false;
            DataDesativacao = DateTime.Now;
        }
    }
}
