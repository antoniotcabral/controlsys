﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Ocorrencias
{
    public enum StatusTreinamentoOcorrencia
    {
        AguardandoComparecimento,
        NaoCompareceu,
        Compareceu
    }
}
