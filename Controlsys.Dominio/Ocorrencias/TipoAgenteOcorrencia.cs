﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controlsys.Dominio.Ocorrencias
{
    public enum TipoAgenteOcorrencia
    {
        Passivo = 1,
        Ativo = 2
    }
}
