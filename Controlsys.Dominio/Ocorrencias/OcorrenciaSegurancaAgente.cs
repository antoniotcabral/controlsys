﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Pessoas;

namespace Controlsys.Dominio.Ocorrencias
{
    public class OcorrenciaSegurancaAgente
    {
        public virtual int Codigo { get; set; }

        public virtual OcorrenciaSeguranca Ocorrencia { get; set; }

        public virtual PessoaFisica PessoaFisica { get; set; }

        public virtual string Cracha { get; set; }

        public virtual TipoAgenteOcorrencia TipoAgente { get; set; }

        public virtual bool Ativo { get; protected set; }

        public virtual DateTime DataRegistro { get; set; }

        public virtual DateTime? DataDesativacao { get; set; }

        public virtual void Ativar()
        {
            Ativo = true;
            DataDesativacao = null;
        }

        public virtual void Inativar()
        {
            Ativo = false;
            DataDesativacao = DateTime.Now;
        }
    }
}
