﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Ocorrencias
{
    public enum GravidadeOcorrencia
    {
        [Description("Leve")]
        Leve = 1,
        [Description("Média")]
        Media = 2,
        [Description("Grave")]
        Grave = 3,
        [Description("Gravíssima")]
        Gravissima = 4
    }
}
