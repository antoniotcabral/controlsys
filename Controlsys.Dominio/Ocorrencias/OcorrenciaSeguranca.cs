﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.CredenciamentoVeiculos;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Seguranca;

namespace Controlsys.Dominio.Ocorrencias
{
    public class OcorrenciaSeguranca
    {
        public OcorrenciaSeguranca()
        {
            this.AgentesOcorrencia = new List<OcorrenciaSegurancaAgente>();
            this.Midias = new List<OcorrenciaSegurancaMidia>();
            this.Veiculos = new List<OcorrenciaSegurancaVeiculo>();
        }

        public virtual int Codigo { get; set; }

        public virtual string Descricao { get; set; }

        public virtual string Procedimentos { get; set; }

        public virtual string AcoesRecomendadas { get; set; }

        public virtual DateTime DataRegistro { get; set; }

        public virtual DateTime DataOcorrencia { get; set; }

        public virtual TimeSpan HoraOcorrencia { get; set; }
        
        public virtual TipoOcorrencia TipoOcorrencia { get; set; }

        public virtual bool Vulnerabilidade { get; set; }

        public virtual bool Tentativa { get; set; }

        public virtual bool Efetiva { get; set; }

        public virtual GravidadeOcorrencia Gravidade { get; set; }

        public virtual Usuario Supervisor { get; set; }

        public virtual Local Local { get; set; }

        public virtual IList<OcorrenciaSegurancaAgente> AgentesOcorrencia { get; set; }

        public virtual IList<OcorrenciaSegurancaMidia> Midias { get; set; }

        public virtual IList<OcorrenciaSegurancaVeiculo> Veiculos { get; set; }

        public virtual bool Ativo { get; set; }

        public virtual DateTime? DataDesativacao { get; set; }

        public virtual void Ativar()
        {
            Ativo = true;
            DataDesativacao = null;
        }

        public virtual void Inativar()
        {
            Ativo = false;
            DataDesativacao = DateTime.Now;
        }

        public virtual void Graduacoes(GraduacaoOcorrencia[] graduacoes) {
            
            this.Vulnerabilidade = false;
            this.Efetiva = false;
            this.Tentativa = false;

            foreach (GraduacaoOcorrencia item in graduacoes)
            {
                switch (item)
                {
                    case GraduacaoOcorrencia.Vulnerabilidade:
                        this.Vulnerabilidade = true;
                        break;
                    case GraduacaoOcorrencia.Efetiva:
                        this.Efetiva = true;
                        break;
                    case GraduacaoOcorrencia.Tentativa:
                        this.Tentativa = true;
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
