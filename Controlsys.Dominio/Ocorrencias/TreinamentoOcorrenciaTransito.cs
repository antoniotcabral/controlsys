﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Ocorrencias
{
    public class TreinamentoOcorrenciaTransito
    {
        public virtual int Codigo { get; set; }

        public virtual OcorrenciaTransito OcorrenciaTransito { get; set; }

        public virtual bool CompareceuNotificacao { get; set; }

        public virtual DateTime? DataAgendamento { get; set; }

        public virtual bool? CompareceuTreinamento { get; set; }

        public virtual bool? Apto { get; set; }

        public virtual DateTime DataRegistro { get; set; }

        public virtual DateTime? DataDesativacao { get; set; }

        public virtual bool Ativo { get; set; }

        public virtual void Ativar()
        {
            Ativo = true;
            DataDesativacao = null;
        }

        public virtual void Inativar()
        {
            Ativo = false;
            DataDesativacao = DateTime.Now;
        }

    }
}
