﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Ocorrencias
{
    public enum GraduacaoOcorrencia
    {
        [Description("Vulnerabilidade (V)")]
        Vulnerabilidade = 1,
        [Description("Tentativa (T)")]
        Tentativa = 2,
        [Description("Efetiva (E)")]
        Efetiva = 3
    }
}
