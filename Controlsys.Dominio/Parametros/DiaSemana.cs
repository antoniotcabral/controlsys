﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Controlsys.Dominio.Parametros
{
    /// <summary>
    /// Valores que representam DiaSemana.
    /// </summary>
    public enum DiaSemana
    {
        /// <summary>
        /// An enum constant representing the domingo option.
        /// </summary>
        Domingo = 1,
        /// <summary>
        /// An enum constant representing the segunda option.
        /// </summary>
        Segunda = 2,
        /// <summary>
        /// An enum constant representing the terca option.
        /// </summary>
        [Description("Terça")]
        Terca = 3,
        /// <summary>
        /// An enum constant representing the quarta option.
        /// </summary>
        Quarta = 4,
        /// <summary>
        /// An enum constant representing the quinta option.
        /// </summary>
        Quinta = 5,
        /// <summary>
        /// An enum constant representing the sexta option.
        /// </summary>
        Sexta = 6,
        /// <summary>
        /// An enum constant representing the sabado option.
        /// </summary>
        [Description("Sábado")]
        Sabado = 7
        
    }
}
