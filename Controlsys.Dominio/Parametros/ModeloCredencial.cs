﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Parametros
{
    public class ModeloCredencial
    {
        public virtual int Codigo { get; set; }

        public virtual string Nome { get; set; }

        public virtual TipoCredencial TipoCredencial { get; set; }

        public virtual byte[] FundoFrente { get; set; }

        public virtual byte[] FundoVerso { get; set; }

        public virtual string HtmlFrente { get; set; }

        public virtual string HtmlVerso { get; set; }

        public virtual decimal Altura { get; set; }

        public virtual decimal Largura { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataRegistro.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataRegistro.
        /// </value>
        public virtual DateTime DataRegistro { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataDesativacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataDesativacao.
        /// </value>
        public virtual DateTime? DataDesativacao { get; set; }

        /// <summary>
        /// Gets/Sets a situação do objetivo do tipo ExcecaoAcesso.
        /// </summary>
        ///
        /// <value>
        /// <see cref="DataDesativacao"/> nula retorna true, <see cref="DataDesativacao"/> com valor
        /// retorna false.
        /// </value>
        public virtual bool Ativo { get; set; }

        /// <summary>
        /// Ativa o objeto, definindo <see cref="Ativo"/> como true e <see cref="DataDesativacao"/> como
        /// nulo.
        /// </summary>
        public virtual void Ativar()
        {
            Ativo = true;
            DataDesativacao = null;
        }

        /// <summary>
        /// Inativa o objeto, definindo <see cref="Ativo"/> como falso e <see cref="DataDesativacao"/>
        /// como a data atual.
        /// </summary>
        public virtual void Inativar()
        {
            Ativo = false;
            DataDesativacao = DateTime.Now;
        }

        //private decimal defaultHeightCm { get { return 8.45M; } }
        //private decimal defaultWidthCm { get { return 5.32M; } }
        //private decimal defaultHeightPx { get { return 451.27937007897M; } }
        //private decimal defaultWidthPx { get { return 284.21669291355M; } }

        //public virtual decimal AlturaPx()
        //{
        //    return (Altura * defaultHeightPx) / defaultHeightCm;
        //}
        //
        //public virtual decimal LarguraPx()
        //{
        //    return (Largura * defaultWidthPx) / defaultWidthCm;
        //}
    }
}
