﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Controlsys.Dominio.Parametros
{
    public enum GrauInfracao
    {
        /// <summary>
        /// An enum constant representing the em andamento option.
        /// </summary>
        Leve,
        /// <summary>
        /// An enum constant representing the em andamento option.
        /// </summary>
        [Description("Média")]
        Media,
        /// <summary>
        /// An enum constant representing the em andamento option.
        /// </summary>
        Grave,
        /// <summary>
        /// An enum constant representing the em andamento option.
        /// </summary>
        [Description("Gravíssima")]
        Gravissima
    }
}
