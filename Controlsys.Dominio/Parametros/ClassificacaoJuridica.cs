﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Parametros
{
    public class ClassificacaoJuridica
    {
        public virtual int Codigo { get; set; }

        public virtual ItemClassificacaoJuridica ItemClassificacao { get; set; }

        public virtual string Letra { get; set; }

        public virtual string Formula { get; set; }

        public virtual int? Peso { get; set; }

        public virtual bool Pontua { get; set; }

        public virtual bool Ativo { get; set; }

        public virtual DateTime DataRegistro { get; set; }

        public virtual DateTime? DataDesativacao { get; set; }

        public virtual void Ativar()
        {
            Ativo = true;
            DataDesativacao = null;
        }

        public virtual void Inativar()
        {
            Ativo = false;
            DataDesativacao = DateTime.Now;
        }
    }
}
