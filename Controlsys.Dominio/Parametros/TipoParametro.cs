﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Parametros
{
    /// <summary>
    /// Valores que representam TipoParametro.
    /// </summary>
    public enum TipoParametro
    {
        /// <summary>
        /// An enum constant representing the lista option.
        /// </summary>
        Lista = 1,
        /// <summary>
        /// An enum constant representing the numero option.
        /// </summary>
        Numero = 2,
        /// <summary>
        /// An enum constant representing the texto option.
        /// </summary>
        Texto = 3,
        /// <summary>
        /// An enum constant representing the simulation nao option.
        /// </summary>
        SimNao = 4,
        /// <summary>
        /// An enum constant representing the data option.
        /// </summary>
        Data = 5,
        /// <summary>
        /// An enum constant representing the data option.
        /// </summary>
        MesAno = 6
    }
}
