﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace Controlsys.Dominio.Parametros
{
    public class Midia
    {
        public virtual int Codigo { get; set; }

        public virtual string Url
        {
            get
            {
                string path = ConfigurationManager.AppSettings.Get("pathUploads");
                path = (path.EndsWith(@"\") ? path : path + @"\");
                return path + NomeArquivo;
            }
        }

        public virtual DateTime DataRegistro { get; set; }

        public virtual string NomeArquivo { get; set; }

        public virtual string Extensao { get; set; }

        public virtual string MimeType { get; set; }

    }
}
