﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controlsys.Dominio.Parametros
{
    /// <summary>
    /// Representa um(a) Parametro.
    /// </summary>
    public class Parametro
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public virtual int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Nome.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Nome.
        /// </value>
        public virtual ParametroSistema Nome { get; set; }

        /// <summary>
        /// Gets/Sets valor para Valor.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Valor.
        /// </value>
        public virtual string Valor { get; set; }

        /// <summary>
        /// Gets/Sets valor para URL.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) URL.
        /// </value>
        public virtual string Url { get; set; }

        /// <summary>
        /// Gets/Sets valor para TipoParametro.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TipoParametro.
        /// </value>
        public virtual TipoParametro TipoParametro { get; set; }

        /// <summary>
        /// Gets/Sets valor para ExecutaProcedimento.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) ExecutaProcedimento.
        /// </value>
        public virtual bool ExecutaProcedimento { get; set; }
    }
}
