﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controlsys.Dominio.Parametros
{
    /// <summary>
    /// Representa um(a) EscalaTrabalho.
    /// </summary>
    public class EscalaTrabalho
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public virtual int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataEscala.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataEscala.
        /// </value>
        public virtual DateTime DataEscala { get; set; }

        /// <summary>
        /// Gets/Sets valor para GrupoTrabalho.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) GrupoTrabalho.
        /// </value>
        public virtual GrupoTrabalho GrupoTrabalho { get; set; }

        /// <summary>
        /// Gets/Sets valor para HoraTurno.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) HoraTurno.
        /// </value>
        public virtual HoraTurno HoraTurno { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataRegistro.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataRegistro.
        /// </value>
        public virtual DateTime DataRegistro { get; set; }
    }
}
