﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Parametros
{
    public class Sindicato
    {
        public virtual int Codigo { get; set; }
        public virtual string Nome { get; set; }
        public virtual string Razao { get; set; } ////Razão Social
        public virtual string Cnpj { get; set; }
        public virtual string Email { get; set; }
        public virtual string Telefone { get; set; }
        public virtual string TelEmergencia { get; set; }
        public virtual string Celular { get; set; }
        public virtual DateTime DataRegistro { get; set; }
        public virtual DateTime? DataDesativacao { get; set; }
        public virtual bool Ativo { get; set; }
        public virtual void Ativar()
        {
            Ativo = true;
            DataDesativacao = null;
        }
        public virtual void Inativar()
        {
            Ativo = false;
            DataDesativacao = DateTime.Now;
        }

        /// <summary>
        /// Cnpj formatado.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) string.
        /// </returns>
        public virtual string CNPJFormatado()
        {
            Regex apenasDigitos = new Regex(@"[^\d]");
            return string.Format(@"{0:00\.000\.000\/0000\-00}", Convert.ToInt64(apenasDigitos.Replace(Cnpj, "")));
        }
    }
}
