﻿using System;
using Controlsys.Dominio.Pessoas;
using Controlsys.Dominio.Seguranca;

namespace Controlsys.Dominio.Parametros
{
    public class AtendimentoReceptivo
    {
        public virtual int Codigo { get; set; }

        public virtual Midia Midia { get; set; }

        public virtual Papel Papel { get; set; }

        public virtual Usuario Usuario { get; set; }

        public virtual string Descricao { get; set; }

        public virtual DateTime DataRegistro { get; set; }

        public virtual DateTime? DataDesativacao { get; set; }

        public virtual bool Ativo { get; protected set; }

        public virtual void Ativar()
        {
            Ativo = true;
            DataDesativacao = null;
        }

        public virtual void Inativar()
        {
            Ativo = false;
            DataDesativacao = DateTime.Now;
        }
    }
}
