﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Controlsys.Dominio.Parametros
{
    /// <summary>
    /// Valores que representam StatusDocumento.
    /// </summary>
    public enum StatusDocumento
    {
        /// <summary>
        /// An enum constant representing the completo option.
        /// </summary>
        Completo = 1,
        /// <summary>
        /// An enum constant representing the incompleto option.
        /// </summary>
        Incompleto = 2,
        /// <summary>
        /// An enum constant representing the nao entregue option.
        /// </summary>
        [Description("Não Entregue")]
        NaoEntregue = 3,
        /// <summary>
        /// An enum constant representing the nao entregue option.
        /// </summary>
        [Description("Pré Cadastro")]
        PreCadastro = 4
    }
}
