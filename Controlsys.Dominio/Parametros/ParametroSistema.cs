﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Parametros
{
    /// <summary>
    /// Valores que representam ParametroSistema.
    /// </summary>
    public enum ParametroSistema
    {
        /// <summary>
        /// An enum constant representing the mensagem cadastro pessoa fisica option.
        /// </summary>
        [Description("Mensagem no cadastro de pessoa que estiver cadastrada como pessoa crítica")]
        MensagemCadastroPessoaFisica,

        /// <summary>
        /// An enum constant representing the mensagem email notifica grupo cadastro pessoa critica
        /// option.
        /// </summary>
        [Description("Mensagem de corpo de e-mail que notifica a um grupo de usuários a tentativa de cadastro de uma pessoa crítica")]
        MensagemEmailNotificaGrupoCadastroPessoaCritica,

        /// <summary>
        /// An enum constant representing the mensagem email notifica grupo cadastro pessoa critica
        /// option.
        /// </summary>
        [Description("Mensagem de corpo de e-mail que notifica sobre a validade de documentos dos colaboradores")]
        MensagemEmailNotificaValidadeDocumentos,

        /// <summary>
        /// An enum constant representing the grupo usuario responsavel pessoa critica option.
        /// </summary>
        [Description("Grupo de usuários responsável por pessoas críticas")]
        GrupoUsuarioResponsavelPessoaCritica,

        /// <summary>
        /// An enum constant representing the antipassback option.
        /// </summary>
        [Description("Usa antipassback")]
        Antipassback,

        /// <summary>
        /// An enum constant representing the configurar dias inatividade colaborador option.
        /// </summary>
        [Description("Configurar dias para inatividade do colaborador")]
        ConfigurarDiasInatividadeColaborador,
        
        /// <summary>
        /// An enum constant representing the configurar dias inatividade prestador option.
        /// </summary>
        [Description("Configurar dias para inatividade do prestador")]
        ConfigurarDiasInatividadePrestador,

        /// <summary>
        /// An enum constant representing the configurar dias inatividade prestador option.
        /// </summary>
        [Description("Configurar dias para não inativar colaborador/prestador após ativação")]
        ConfigurarDiasNInativAposAtivColabPrest,

        /// <summary>
        /// An enum constant representing the tempo espera input 2 option.
        /// </summary>
        [Description("Tempo de espera para o segundo input")]
        TempoEsperaInput2,

        /// <summary>
        /// An enum constant representing the tempo repete input 1 option.
        /// </summary>
        [Description("Tempo para repetir primeiro input")]
        TempoRepeteInput1,

        /// <summary>
        /// An enum constant representing the intervalo keep on option.
        /// </summary>
        [Description("Intervalo KeepOn")]
        IntervaloKeepOn,

        /// <summary>
        /// An enum constant representing the intervalo keep off option.
        /// </summary>
        [Description("Intervalo KeepOff")]
        IntervaloKeepOff,

        /// <summary>
        /// An enum constant representing the tempo giro option.
        /// </summary>
        [Description("Tempo limite para giro na catraca")]
        TempoGiro,

        /// <summary>
        /// An enum constant representing the porta entrada option.
        /// </summary>
        [Description("Porta de entrada para controladoras")]
        PortaEntrada,

        /// <summary>
        /// An enum constant representing the intervalo repete input 1 option.
        /// </summary>
        [Description("Intervalo para repetir primeiro input")]
        IntervaloRepeteInput1,

        /// <summary>
        /// An enum constant representing the catracas atualizadas option.
        /// </summary>
        [Description("Catracas atualizadas")]
        CatracasAtualizadas,

        /// <summary>
        /// An enum constant representing the data ultima atualizacao escala option.
        /// </summary>
        [Description("Data de ultima atualização escala")]
        DataUltimaAtualizacaoEscala,

        /// <summary>
        /// An enum constant representing the frequencia carga escala option.
        /// </summary>
        [Description("Frequência para carregar escalas")]
        FrequenciaCargaEscala,

        /// <summary>
        /// An enum constant representing the grupo sobanalise option.
        /// </summary>
        [Description("Grupo de Usuários responsável por receber alerta de pessoas sob análise.")]
        GrupoSobanalise,

        /// <summary>
        /// An enum constant representing the grupo sobanalise option.
        /// </summary>
        [Description("Tempo (dias) para notificação de vencimento de documentos para empresa.")]
        TempoNotificaValidadeDocumentos,

        /// <summary>
        /// An enum constant representing the grupo controladora offline option.
        /// </summary>
        [Description("Grupo de usuários reponsável por receber alerta caso uma controladora fique offline.")]
        GrupoControladoraOffline,

        /// <summary>
        /// An enum constant representing the grupo controladora offline option.
        /// </summary>
        [Description("Tempo (horas) para alertar caso uma controladora fique offline.")]
        TempoNotificaControladoraOffline,

        /// <summary>
        /// An enum constant representing the grupo controladora offline option.
        /// </summary>
        [Description("Tempo (dias) a mais que o atual para verificar a data de validade de documentos")]
        NumeroDiasVerificarValidadeDocumentos,

        [Description("Valor mínimo para classificação jurídica ruim")]
        ClassificacaoJuridicaRuimMinimo,

        [Description("Valor máximo para classificação jurídica ruim")]
        ClassificacaoJuridicaRuimMaximo,

        [Description("Valor mínimo para classificação jurídica médio")]
        ClassificacaoJuridicaMedioMinimo,

        [Description("Valor máximo para classificação jurídica médio")]
        ClassificacaoJuridicaMedioMaximo,

        [Description("Valor mínimo para classificação jurídica bom")]
        ClassificacaoJuridicaBomMinimo,

        [Description("Valor máximo para classificação jurídica bom")]
        ClassificacaoJuridicaBomMaximo,

        [Description("Código de Projeto Padrão")]
        CodigoProjetoPadrao,

       /// <summary>
        /// An enum constant representing the number off days after insert request to visit.
        /// </summary>
        [Description("Número de dias limite para agendamento de visitante.")]
        NumeroDiaInicioVisita,

        [Description("Dia para execução da carga dos dados do relatório FTE.")]
        DataExecuçãoRelatorioFTE,
                
        [Description("Dia para execução da carga dos dados do relatório FTE Prestador.")]
        DataExecuçãoRelatorioFTEPrestador,

        [Description("Exportar FTE e HHT para Visual Flash.")]
        DataAgendamentoRelatorioHHT,
        // 27	CargoUsuario	500	Lista	/Cargo/ObterTodos

        /// <summary>
        /// Description Aciona manualmente API : ListaColaboradoresAtualizadosControlsys,
        /// </summary>
        [Description("Acionar API ListaColaboradoresAtualizadosControlsys, para envio ao SAP.")]
        ListaColaboradoresAtualizadosControlsys,

        /// <summary>
        /// An enum constant representing the mensagem cadastro pessoa fisica option.
        /// </summary>
        [Description("Ip do Servidor Avigilon")]
        IP_AVIGILON,

        /// <summary>
        /// An enum constant representing the mensagem cadastro pessoa fisica option.
        /// </summary>
        [Description("Login do Servidor Avigilon")]
        LOGIN_AVIGILON,



        /// <summary>
        /// An enum constant representing the mensagem cadastro pessoa fisica option.
        /// </summary>
        [Description("Senha do Servidor Avigilon")]
        PASSW_AVIGILON,


        /// <summary>
        /// An enum constant representing the mensagem cadastro pessoa fisica option.
        /// </summary>
        [Description("Caminho Virtual da Imagem")]
        CAMINHO_IMAGEM,

        /// <summary>
        /// An enum constant representing the mensagem cadastro pessoa fisica option.
        /// </summary>
        [Description("Caminho Físico da Imagem")]
        CAMINHO_IMAGEM_FISICO,

        /// <summary>
        /// An enum constant representing the mensagem cadastro pessoa fisica option.
        /// </summary>
        [Description("PORTA_AVIGILON")]
        PORTA_AVIGILON,
        /// <summary>
        /// An enum constant representing the mensagem cadastro pessoa fisica option.
        /// </summary>
        [Description("Tempo Limite")]
        ESPERA_AVIGILON,

            /// <summary>
            /// An enum constant representing the mensagem cadastro pessoa fisica option.
            /// </summary>
        [Description("Ip do Integrador Avigilon")]
        IP_INTEGRADOR,
        //   [Description("IP_INTEGRADOR")]

        /// <summary>
        /// An enum constant representing the mensagem cadastro pessoa fisica option.
        /// </summary>
        [Description("Porta do Integrador Avigilon")]
        PORTA_INTEGRADOR_AVIGILON,
        //[Description("PORTA_INTEGRADOR_AVIGILON")]

        /// <summary>
        /// An enum constant representing the mensagem cadastro pessoa fisica option.
        /// </summary>
        [Description("Validação do Integrador Avigilon")]
        ATIVACAO_CONSULTA_INTEGRADOR,


        /// <summary>
        /// An enum constant representing the mensagem cadastro pessoa fisica option.
        /// </summary>
        [Description("Imagem Avigilon Altura")]
        IMAGEM_AVIGILON_ALTURA,
        //[Description("IMAGEM_AVIGILON_ALTURA")]

        /// <summary>
        /// An enum constant representing the mensagem cadastro pessoa fisica option.
        /// </summary>
        [Description("Imagem Avigilon Largura")]
        IMAGEM_AVIGILON_LARGURA,

        /// <summary>
        /// An enum constant representing the mensagem Tempo valido leitua LPR Avigilon.
        /// </summary>
        [Description("Tempo (ms) valido para uma leitua LPR Avigilon")]
        TEMPO_VALIDO_LEITURA_LPR_AVIGILON
        /*
        /*
        /// <summary>
        /// An enum constant representing the grupo controladora offline option.
        /// </summary>
        [Description("Está sendo realizado um pré cadastro de $$Papel$$, e requer autorização de um gestor da PO $$PO$$, para submeter a aprovação.")]
        MensagemEmailNotificaGestorPreCadastro
        */
    }
}
