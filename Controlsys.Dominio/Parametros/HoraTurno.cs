﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controlsys.Dominio.Parametros
{
    /// <summary>
    /// Representa um(a) HoraTurno.
    /// </summary>
    public class HoraTurno
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public virtual int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Turno.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Turno.
        /// </value>
        public virtual Turno Turno { get; set; }

        /// <summary>
        /// Gets/Sets valor para HoraInicio.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) HoraInicio.
        /// </value>
        public virtual TimeSpan HoraInicio { get; set; }

        /// <summary>
        /// Gets/Sets valor para HoraFim.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) HoraFim.
        /// </value>
        public virtual TimeSpan HoraFim { get; set; }

        /// <summary>
        /// Gets/Sets valor para Ordem.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Ordem.
        /// </value>
        public virtual int Ordem { get; set; }

        /// <summary>
        /// Gets/Sets valor para DiaSemana.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DiaSemana.
        /// </value>
        public virtual DiaSemana? DiaSemana { get; set; }

        /// <summary>
        /// Gets/Sets valor para Trabalha.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Trabalha.
        /// </value>
        public virtual bool Trabalha { get; set; }

        /// <summary>
        /// Gets/Sets valor para Maior24h.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Maior24h.
        /// </value>
        public virtual bool Maior24h { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataDesativacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataDesativacao.
        /// </value>
        public virtual DateTime? DataDesativacao { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataRegistro.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataRegistro.
        /// </value>
        public virtual DateTime DataRegistro { get; set; }

        /// <summary>
        /// Gets/Sets a situação do objetivo do tipo HoraTurno.
        /// </summary>
        ///
        /// <value>
        /// <see cref="DataDesativacao"/> nula retorna true, <see cref="DataDesativacao"/> com valor
        /// retorna false.
        /// </value>
        public virtual bool Ativo { get; protected set; }

        /// <summary>
        /// Ativa o objeto, definindo <see cref="Ativo"/> como true e <see cref="DataDesativacao"/> como
        /// nulo.
        /// </summary>
        public virtual void Ativar()
        {
            Ativo = true;
            DataDesativacao = null;
        }

        /// <summary>
        /// Inativa o objeto, definindo <see cref="Ativo"/> como falso e <see cref="DataDesativacao"/>
        /// como a data atual.
        /// </summary>
        public virtual void Inativar()
        {
            Ativo = false;
            DataDesativacao = DateTime.Now;
        }

    }
}
