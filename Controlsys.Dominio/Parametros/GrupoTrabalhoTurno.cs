﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controlsys.Dominio.Parametros
{
    /// <summary>
    /// Representa um(a) GrupoTrabalhoTurno.
    /// </summary>
    public class GrupoTrabalhoTurno
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public virtual int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataInicio.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataInicio.
        /// </value>
        public virtual DateTime DataInicio { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataFim.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataFim.
        /// </value>
        public virtual DateTime? DataFim { get; set; }

        /// <summary>
        /// Gets/Sets valor para GrupoTrabalho.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) GrupoTrabalho.
        /// </value>
        public virtual GrupoTrabalho GrupoTrabalho { get; set; }

        /// <summary>
        /// Gets/Sets valor para Turno.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Turno.
        /// </value>
        public virtual Turno Turno { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataRegistro.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataRegistro.
        /// </value>
        public virtual DateTime DataRegistro { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataDesativacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataDesativacao.
        /// </value>
        public virtual DateTime? DataDesativacao { get; set; }

        /// <summary>
        /// Gets/Sets a situação do objetivo do tipo GrupoTrabalhoTurno.
        /// </summary>
        ///
        /// <value>
        /// <see cref="DataDesativacao"/> nula retorna true, <see cref="DataDesativacao"/> com valor
        /// retorna false.
        /// </value>
        public virtual bool Ativo { get; /*protected*/ set; }

        /// <summary>
        /// Ativa o objeto, definindo <see cref="Ativo"/> como true e <see cref="DataDesativacao"/> como
        /// nulo.
        /// </summary>
        public virtual void Ativar()
        {
            Ativo = true;
            DataDesativacao = null;
        }

        /// <summary>
        /// Inativa o objeto, definindo <see cref="Ativo"/> como falso e <see cref="DataDesativacao"/>
        /// como a data atual.
        /// </summary>
        public virtual void Inativar()
        {
            Ativo = false;
            DataDesativacao = DateTime.Now;
        }
    }
}
