﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Parametros
{
    public class TipoAlimentacao
    {
        public virtual int Codigo { get; set; }

        public virtual string Nome { get; set; }

        public virtual DateTime DataRegistro { get; set; }

        public virtual DateTime? DataDesativacao { get; set; }

        public virtual bool Ativo { get; set; }

        public virtual void Ativar()
        {
            DataDesativacao = null;
            Ativo = true;
        }

        public virtual void Inativar()
        {
            DataDesativacao = DateTime.Now;
            Ativo = false;
        }
    }
}
