﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Controlsys.Dominio.Parametros
{
    /// <summary>
    /// Valores que representam TipoCadastro.
    /// </summary>
    public enum TipoCadastro
    {
        /// <summary>
        /// An enum constant representing the empresa option.
        /// </summary>
        [Description("Empresa")]
        Empresa = 1,
        /// <summary>
        /// An enum constant representing the colaborador option.
        /// </summary>
        [Description("Colaborador")]
        Colaborador = 2,
        /// <summary>
        /// An enum constant representing the visitante option.
        /// </summary>
        [Description("Visitante")]
        Visitante = 3,
        /// <summary>
        /// An enum constant representing the prestador servico temporary option.
        /// </summary>
        [Description("Prestador Serviço Temporário")]
        PrestadorServicoTemp = 4,
        /// <summary>
        /// An enum constant representing the prestador servico emergencial option.
        /// </summary>
        [Description("Prestador Serviço Emergencial")]
        PrestadorServicoEmergencial = 5
    }
}
