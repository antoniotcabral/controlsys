﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Acesso;

namespace Controlsys.Dominio.Parametros
{
    public class Restaurante
    {
        public Restaurante()
        {
            TiposAlimentacao = new List<TipoAlimentacaoRestaurante>();
        }

        public virtual int Codigo { get; set; }

        public virtual string Nome { get; set; }

        public virtual GrupoLeitora GrupoLeitora { get; set; }

        public virtual IList<TipoAlimentacaoRestaurante> TiposAlimentacao { get; set; }

        public virtual bool Ativo { get; set; }

        public virtual DateTime DataRegistro { get; set; }

        public virtual DateTime? DataDesativacao { get; set; }

        public virtual void Ativar()
        {
            Ativo = true;
            DataDesativacao = null;
        }

        public virtual void Inativar()
        {
            Ativo = false;
            DataDesativacao = DateTime.Now;
        }
    }
}
