﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Controlsys.Dominio.Pessoas;

namespace Controlsys.Dominio.Parametros
{
    /// <summary>
    /// Representa um(a) GrupoTrabalho.
    /// </summary>
    public class GrupoTrabalho
    {
        /// <summary>
        /// Construtor para Controlsys.Dominio.Parametros.GrupoTrabalho.
        /// </summary>
        public GrupoTrabalho()
        {
            GrupoTrabalhoTurno = new List<GrupoTrabalhoTurno>();
            GrupoTrabalhoColaboradores = new List<GrupoTrabalhoColab>();
        }

        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public virtual int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para CodigoPHT.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CodigoPHT.
        /// </value>
        public virtual string CodigoPHT { get; set; }

        /// <summary>
        /// Gets/Sets valor para Nome.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Nome.
        /// </value>
        public virtual string Nome { get; set; }

        /// <summary>
        /// Gets/Sets a situação do objetivo do tipo GrupoTrabalho.
        /// </summary>
        ///
        /// <value>
        /// <see cref="DataDesativacao"/> nula retorna true, <see cref="DataDesativacao"/> com valor
        /// retorna false.
        /// </value>
        public virtual bool Ativo { get; /*protected*/ set; }

        /// <summary>
        /// Gets/Sets valor para DataRegistro.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataRegistro.
        /// </value>
        public virtual DateTime DataRegistro { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataDesativacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataDesativacao.
        /// </value>
        public virtual DateTime? DataDesativacao { get; set; }

        /// <summary>
        /// Gets/Sets valor para GrupoTrabalhoTurno.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) GrupoTrabalhoTurno.
        /// </value>
        public virtual IList<GrupoTrabalhoTurno> GrupoTrabalhoTurno { get; set; }

        /// <summary>
        /// Gets/Sets valor para GrupoTrabalhoColaboradores.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) GrupoTrabalhoColaboradores.
        /// </value>
        public virtual IList<GrupoTrabalhoColab> GrupoTrabalhoColaboradores { get; set; }

        /// <summary>
        /// Ativa o objeto, definindo <see cref="Ativo"/> como true e <see cref="DataDesativacao"/> como
        /// nulo.
        /// </summary>
        public virtual void Ativar()
        {
            Ativo = true;
            DataDesativacao = null;
        }

        /// <summary>
        /// Inativa o objeto, definindo <see cref="Ativo"/> como falso e <see cref="DataDesativacao"/>
        /// como a data atual.
        /// </summary>
        public virtual void Inativar()
        {
            Ativo = false;
            DataDesativacao = DateTime.Now;
        }

        /// <summary>
        /// Obter turno trabalho.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) GrupoTrabalhoTurno.
        /// </returns>
        public virtual GrupoTrabalhoTurno ObterTurnoTrabalho()
        {
            return GrupoTrabalhoTurno.OrderByDescending(pl => pl.DataRegistro).FirstOrDefault();
        }
    }
}
