﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Parametros
{
    public enum TipoCredencial
    {
        [Description("Provisória")]
        Provisoria,

        [Description("Definitiva")]
        Definitiva,

        [Description("Visitante")]
        Visitante,

        [Description("Estacionamento Externo")]
        EstacionamentoExterno
    }
}
