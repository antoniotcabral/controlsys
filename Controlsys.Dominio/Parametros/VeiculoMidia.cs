﻿using Controlsys.Dominio.CredenciamentoVeiculos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Parametros
{
    public class VeiculoMidia
    {

        public virtual int Codigo { get; set; }
        
        public virtual Veiculo Veiculo { get; set; }
        
        public virtual Midia Midia { get; set; }
        
        public virtual DateTime DataRegistro { get; set; }
        
        public virtual DateTime? DataDesativacao { get; set; }

        public virtual bool Ativo { get; protected set; }

        public virtual void Ativar()
        {
            Ativo = true;
            DataDesativacao = null;
        }

        public virtual void Inativar()
        {
            Ativo = false;
            DataDesativacao = DateTime.Now;
        }

    }
}
