﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controlsys.Dominio.Parametros
{
    /// <summary>
    /// 
    /// </summary>
    public class TipoAlimentacaoRestaurante
    {
        public virtual int Codigo { get; set; }        

        public virtual TipoAlimentacao TipoAlimentacao { get; set; }

        public virtual Restaurante Restaurante { get; set; }

        public virtual TimeSpan HoraInicio { get; set; }

        public virtual TimeSpan HoraFim { get; set; }

        public virtual bool Ativo { get; set; }

        public virtual DateTime DataRegistro { get; set; }

        public virtual DateTime? DataDesativacao { get; set; }

        public virtual void Ativar()
        {
            Ativo = true;
            DataDesativacao = null;
        }

        public virtual void Inativar()
        {
            Ativo = false;
            DataDesativacao = DateTime.Now;
        }
    }
}
