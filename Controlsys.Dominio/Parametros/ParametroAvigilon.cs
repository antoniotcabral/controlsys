﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Parametros
{
    /// <summary>
    /// Paramentros usados no integrador Avigilon
    /// </summary>
    public enum ParametroAvigilon
    {
        /// <summary>
        /// An enum constant representing the mensagem cadastro pessoa fisica option.
        /// </summary>
        [Description("IP_AVIGILON")]
        IP_AVIGILON,


        /// <summary>
        /// An enum constant representing the mensagem cadastro pessoa fisica option.
        /// </summary>
        [Description("LOGIN_AVIGILON")]
        LOGIN_AVIGILON,



        /// <summary>
        /// An enum constant representing the mensagem cadastro pessoa fisica option.
        /// </summary>
        [Description("PASSW_AVIGILON")]
        PASSW_AVIGILON,


        /// <summary>
        /// An enum constant representing the mensagem cadastro pessoa fisica option.
        /// </summary>
        [Description("CAMINHO_IMAGEM")]
        CAMINHO_IMAGEM,

        /// <summary>
        /// An enum constant representing the mensagem cadastro pessoa fisica option.
        /// </summary>
        [Description("CAMINHO_IMAGEM_FISICO")]
        CAMINHO_IMAGEM_FISICO,

        /// <summary>
        /// An enum constant representing the mensagem cadastro pessoa fisica option.
        /// </summary>
        [Description("PORTA_AVIGILON")]
        PORTA_AVIGILON,
        /// <summary>
        /// An enum constant representing the mensagem cadastro pessoa fisica option.
        /// </summary>
        [Description("ESPERA_AVIGILON")]
        ESPERA_AVIGILON



    }
}
