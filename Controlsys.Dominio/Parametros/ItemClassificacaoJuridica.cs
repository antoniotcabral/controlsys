﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Parametros
{
    public enum ItemClassificacaoJuridica
    {
        [Description("CNPJ ativos")]
        CNPJAtivos,

        [Description("Pedidos de compra ativos")]
        PedidosDeCompraAtivos,

        [Description("Colaboradores")]
        Colaboradores,

        [Description("Crachás de colaboradores inativos não devolvidos")]
        CrachasColaboradoresInativosNaoDevolvidos,

        [Description("Prestadores")]
        Prestadores,

        [Description("Crachás de prestadores inativos não devolvidos")]
        CrachasPrestadoresInativosNaoDevolvidos,

        [Description("Visitantes")]
        Visitantes,

        [Description("Crachás de visitantes inativos não devolvidos")]
        CrachasVisitantesInativosNaoDevolvidos,

        [Description("Credenciais veicular emitadas")]
        CredencialVeicularEmitidas,

        [Description("Credenciais vencidas")]
        CredenciaisVencidas,

        [Description("Pessoas críticas")]
        PessoasCriticas,

        [Description("Ocorrência de segurança leve")]
        OcorrenciaSegLeve,

        [Description("Ocorrência de segurança media")]
        OcorrenciaSegMedia,

        [Description("Ocorrência de segurança grave")]
        OcorrenciaSegGrave,

        [Description("Ocorrência de segurança gravíssima")]
        OcorrenciaSegGravissima,

        [Description("Ocorrência de trânsito leve")]
        OcorrenciaTransLeve,

        [Description("Ocorrência de trânsito media")]
        OcorrenciaTransMedia,

        [Description("Ocorrência de trânsito grave")]
        OcorrenciaTransGrave,

        [Description("Ocorrência de trânsito gravíssima")]
        OcorrenciaTransGravissima        
    }
}
