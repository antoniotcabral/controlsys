﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Controlsys.Dominio.Enderecos;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Empresas;

namespace Controlsys.Dominio.Pessoas
{
    /// <summary>
    /// Representa um(a) Pessoa.
    /// </summary>
    public abstract class Pessoa
    {
        /// <summary>
        /// Construtor para Controlsys.Dominio.Pessoas.Pessoa.
        /// </summary>
        public Pessoa()
        {
            
        }

        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public virtual string Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Endereco.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Endereco.
        /// </value>
        public virtual Endereco Endereco { get; set; }
        
        //public virtual IList<DocumentoPessoa> DocumentosPessoa { get; set; }

        /// <summary>
        /// Gets/Sets valor para Telefones.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Telefones.
        /// </value>
        public virtual IList<Telefone> Telefones { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataRegistro.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataRegistro.
        /// </value>
        public virtual DateTime DataRegistro { get; set; }

        /// <summary>
        /// Gets/Sets valor para Nome.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Nome.
        /// </value>
        public virtual string Nome { get; set; }

        /// <summary>
        /// Gets/Sets valor para Apelido.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Apelido.
        /// </value>
        public virtual string Apelido { get; set; }

        /// <summary>
        /// Gets/Sets valor para Apelido.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Apelido.
        /// </value>
        
        public virtual string ApelidoLast { get; set; }

        /// <summary>
        /// Gets/Sets valor para Email.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Email.
        /// </value>
        public virtual string Email { get; set; }

        /// <summary>
        /// Gerar codigo pessoa.
        /// </summary>
        ///
        /// <param name="pessoas">
        /// O(a) pessoas.
        /// </param>
        public virtual void GerarCodigoPessoa(IQueryable<Pessoa> pessoas)
        {
            string codigo = "",
                   sufixo = ""; 

            if (this is Empresa)
            {
                IQueryable<Empresa> empresas = pessoas.OfType<Empresa>();
                codigo = empresas.OrderByDescending(p => p.Codigo).FirstOrDefault().Codigo.Substring(2, 8);
                sufixo = "EM";                
            }
            if (this is PessoaFisica)
            {
                IQueryable<PessoaFisica> pessoasFisicas = pessoas.OfType<PessoaFisica>();
                codigo = pessoasFisicas.OrderByDescending(p => p.Codigo).Where(pf => pf.Codigo != "US00000001").FirstOrDefault().Codigo.Substring(2, 8);
                sufixo = "PF";                
            }
            
            int numCodigo = Convert.ToInt32(codigo) + 1;

            int totalInt = numCodigo;
            int casas = 1;

            while ((totalInt = totalInt / 10) > 0)
                casas++;

            string zeros = "";
            for (int i = 0; i < (codigo.Length - casas); i++)
                zeros += "0";

            string final = sufixo + zeros + numCodigo;

            Codigo = final;
        }
        
        public virtual bool EmailValido()
        {
            return EmailValido(Email);
        }

        public static bool EmailValido(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
                throw;
            }
        }
    }
}
