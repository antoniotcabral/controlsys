﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Controlsys.Dominio.Empresas;

namespace Controlsys.Dominio.Pessoas
{
    /// <summary>
    /// Representa um(a) PrestadorServico.
    /// </summary>
    public class PrestadorServico : Papel
    {
        /// <summary>
        /// Gets/Sets valor para Solicitante.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Solicitante.
        /// </value>
        public virtual Colaborador Solicitante { get; set; }

        /// <summary>
        /// Gets/Sets valor para Empresa.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Empresa.
        /// </value>
        public virtual Empresa Empresa { get; set; }

        /// <summary>
        /// Gets/Sets valor para Fornecedor.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Fornecedor.
        /// </value>
        public virtual Fornecedor Fornecedor { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataAdmissao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataAdmissao.
        /// </value>
        public virtual DateTime? DataAdmissao { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataValidade.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataValidade.
        /// </value>
        public virtual DateTime DataValidade { get; set; }

        /// <summary>
        /// Gets/Sets valor para TipoPrestador.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TipoPrestador.
        /// </value>
        public virtual TipoPrestador TipoPrestador { get; set; }

        /// <summary>
        /// Gets/Sets valor para Observacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Observacao.
        /// </value>
        public virtual string Observacao { get; set; }

        /// <summary>
        /// Crachá do prestador de serviço.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) cracha.
        /// </returns>
        /// 

        public virtual string CodigoCracha { get; set; }

        /// <summary>
        /// Gets/Sets valor para Mão de Obra.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Mão de Obra.
        /// </value>
        public virtual MaoDeObra MaoDeObra { get; set; }

        /// <summary>
        /// Gets/Sets valor para Mão de Obra.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Mão de Obra.
        /// </value>
        public virtual GrupoPessoa GrupoPessoa { get; set; }        

        /// <summary>
        /// Gets/Sets valor para Pedido de Compra.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Pedido de Compra.
        /// </value>
        public virtual PedidoCompra PedidoCompra { get; set; }

        /// <summary>
        /// Gets/Sets valor para Setor de Custo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Setor de Custo.
        /// </value>
        public virtual SetorCusto SetorCusto { get; set; }

        /// <summary>
        /// Gets/Sets valor para AcessoPorto.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) AcessoPorto.
        /// </value>
        public virtual bool AcessoPorto { get; set; }
    }
}
