﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Empresas;

namespace Controlsys.Dominio.Pessoas
{
    /// <summary>
    /// Representa um(a) Papel.
    /// </summary>
    public abstract class Papel
    {
        /// <summary>
        /// Construtor para Controlsys.Dominio.Pessoas.Papel.
        /// </summary>
        public Papel()
        {
            PapelLogs = new List<PapelLog>();
            ASOs = new List<ASO>();
            ExcecaoAcesso = new List<ExcecaoAcesso>();
            Documentos = new List<DocumentoPapel>();
            AtendimentosReceptivo = new List<AtendimentoReceptivo>();
            Treinamentos = new List<TreinamentoPessoa>();
        }

        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public virtual int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para PessoaFisica.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) PessoaFisica.
        /// </value>
        public virtual PessoaFisica PessoaFisica { get; set; }

        /// <summary>
        /// Gets/Sets valor para Documentos.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Documentos.
        /// </value>
        public virtual IList<DocumentoPapel> Documentos { get; set; }

        /// <summary>
        /// Gets/Sets valor para Cargo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Cargo.
        /// </value>
        public virtual Cargo Cargo { get; set; }

        /// <summary>
        /// Gets/Sets valor para ModeloCracha.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) ModeloCracha.
        /// </value>
        public virtual ModeloCracha ModeloCracha { get; set; }

        public virtual EmpregadoSAP EmpregadoSAP { get; set; }

        /// <summary>
        /// Gets/Sets valor para ASOs.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) ASOs.
        /// </value>
        public virtual IList<ASO> ASOs { get; set; }

        /// <summary>
        /// Gets/Sets valor para AntiPassBack.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) AntiPassBack.
        /// </value>
        public virtual bool AntiPassBack { get; set; }

        /// <summary>
        /// Gets/Sets valor para SenhaLeitora.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) SenhaLeitora.
        /// </value>
        public virtual string SenhaLeitora { get; set; }

        /// <summary>
        /// Gets/Sets valor para Supervisor.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Supervisor.
        /// </value>
        public virtual bool Supervisor { get; set; }

        /// <summary>
        /// Gets/Sets valor para Supervisionado.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Supervisionado.
        /// </value>
        public virtual bool Supervisionado { get; set; }

        /// <summary>
        /// Gets/Sets valor para SobAnalise.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) SobAnalise.
        /// </value>
        public virtual bool SobAnalise { get; set; }

        /// <summary>
        /// Gets/Sets valor para PessoaJuridica.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) PessoaJuridica.
        /// </value>
        public virtual bool PessoaJuridica { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataRegistro.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataRegistro.
        /// </value>
        public virtual DateTime DataRegistro { get; set; }

        /// <summary>
        /// Gets/Sets valor para IdUsuario.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) IdUsuario.
        /// </value>
        public virtual string IdUsuario { get; set; }

        /// <summary>
        /// Gets/Sets valor para PapelLogs.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) PapelLogs.
        /// </value>
        public virtual IList<PapelLog> PapelLogs { get; set; }

        /// <summary>
        /// Gets/Sets valor para ExcecaoAcesso.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) ExcecaoAcesso.
        /// </value>
        public virtual IList<ExcecaoAcesso> ExcecaoAcesso { get; set; }

        /// <summary>
        /// Gets/Sets valor para Treinamentos.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Treinamentos.
        /// </value>
        public virtual IList<TreinamentoPessoa> Treinamentos { get; set; }
        public virtual IList<AtendimentoReceptivo> AtendimentosReceptivo { get; set; }

        /// <summary>
        /// Get/Sets valor para Area.
        /// </summary>
        ///<value>
        /// Retorna a área onde o colaborador está vinculado.
        /// </value> 

        public virtual Area Area { get; set; }

        /// <summary>
        /// Obter log papel.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) PapelLog.
        /// </returns>
        public virtual PapelLog ObterLogPapel()
        {
            PapelLog papelLog = PapelLogs.OrderByDescending(pl => pl.Codigo).FirstOrDefault(
                pl => pl.DataInicio <= DateTime.Now && (pl.DataFim == null || pl.DataFim >= DateTime.Now.Date)
            );

            return papelLog;
        }

        /// <summary>
        /// Inserir papel log.
        /// </summary>
        ///
        /// <param name="dataInicio">
        /// The data inicio Date/Time.
        /// </param>
        /// <param name="dataFim">
        /// O(a) data fim.
        /// </param>
        /// <param name="statusPapelLog">
        /// O(a) status papel log.
        /// </param>
        ///
        /// <returns>
        /// Um(a) PapelLog.
        /// </returns>
        public virtual PapelLog InserirPapelLog(DateTime dataInicio, DateTime? dataFim, StatusPapelLog statusPapelLog)
        {
            PapelLog papelLog = new PapelLog();
            papelLog.DataInicio = dataInicio;
            papelLog.DataFim = dataFim;
            papelLog.Status = statusPapelLog;
            papelLog.DataRegistro = DateTime.Now;
            papelLog.Papel = this;
            PapelLogs.Add(papelLog);
            return papelLog;

        }
        
        public virtual TipoPapel ObterTipoPapel()
        {
            if (this is Colaborador) return TipoPapel.Colaborador;
            if (this is PrestadorServico) return TipoPapel.PrestadorServico;
            if (this is Visitante) return TipoPapel.Visitante;
            return TipoPapel.SemPapel;
        }
    }
}
