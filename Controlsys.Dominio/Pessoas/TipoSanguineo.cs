﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Pessoas
{
    /// <summary>
    /// Valores que representam TipoSanguineo.
    /// </summary>
    public enum TipoSanguineo
    {
        /// <summary>
        /// An enum constant representing the opos option.
        /// </summary>
        [Description("O+")]
        Opos = 1,
        /// <summary>
        /// An enum constant representing the oneg option.
        /// </summary>
        [Description("O-")]
        Oneg = 2,
        /// <summary>
        /// An enum constant representing the apos option.
        /// </summary>
        [Description("A+")]
        Apos = 3,
        /// <summary>
        /// An enum constant representing the aneg option.
        /// </summary>
        [Description("A-")]
        Aneg = 4,
        /// <summary>
        /// An enum constant representing the bpos option.
        /// </summary>
        [Description("B+")]
        Bpos = 5,
        /// <summary>
        /// An enum constant representing the bneg option.
        /// </summary>
        [Description("B-")]
        Bneg = 6,
        /// <summary>
        /// An enum constant representing the bpos option.
        /// </summary>
        [Description("AB+")]
        ABpos = 7,
        /// <summary>
        /// An enum constant representing the bneg option.
        /// </summary>
        [Description("AB-")]
        ABneg = 8
    }
}
