﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controlsys.Dominio.Pessoas
{
    /// <summary>
    /// Valores que representam HoristaMensalista.
    /// </summary>
    public enum HoristaMensalista
    {
        /// <summary>
        /// An enum constant representing the horista option.
        /// </summary>
        Horista = 1,
        /// <summary>
        /// An enum constant representing the mensalista option.
        /// </summary>
        Mensalista = 2
    }
}
