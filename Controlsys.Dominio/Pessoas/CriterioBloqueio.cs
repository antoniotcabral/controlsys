﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Pessoas
{
    /// <summary>
    /// Valores que representam CriterioBloqueio.
    /// </summary>
    public enum CriterioBloqueio
    {
        /// <summary>
        /// An enum constant representing the antipassback option.
        /// </summary>
        Antipassback = 1,
        /// <summary>
        /// An enum constant representing the acesso hierarquico option.
        /// </summary>
        [Description("Acesso Hierarquico")]
        AcessoHierarquico = 2,
        /// <summary>
        /// An enum constant representing the exame medico option.
        /// </summary>
        [Description("Exame Medico (ASO) vencido")]
        ExameMedico = 3,
        /// <summary>
        /// An enum constant representing the treinamento option.
        /// </summary>
        [Description("Treinamento")]
        Treinamento = 4,
        /// <summary>
        /// An enum constant representing the turno option.
        /// </summary>
        [Description("Turno (acesso fora do horário de trabalho)")]
        Turno = 5,
        /// <summary>
        /// An enum constant representing the ferias option.
        /// </summary>
        [Description("Férias")]
        Ferias = 6,
        /// <summary>
        /// Permite múltiplos acessos no mesmo dia para o mesmo papel
        /// </summary>
        [Description("Múltiplos acessos no mesmo dia para o mesmo papel")]
        MultiplosAcessosRestaurante = 7    
    }
}
