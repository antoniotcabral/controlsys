﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Controlsys.Dominio.Pessoas
{
    /// <summary>
    /// Valores que representam TipoPapel.
    /// </summary>
    public enum TipoPapel
    {
        [Description("Sem Papel")]
        SemPapel = 0,
        /// <summary>
        /// An enum constant representing the visitante option.
        /// </summary>
        Visitante = 1,
        /// <summary>
        /// An enum constant representing the colaborador option.
        /// </summary>
        Colaborador = 2,
        /// <summary>
        /// An enum constant representing the prestador servico option.
        /// </summary>
        [Description("Prestador Serviço")]
        PrestadorServico = 3
    }
}
