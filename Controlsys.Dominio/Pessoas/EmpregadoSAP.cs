﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Pessoas
{
    public class EmpregadoSAP
    {
        public virtual long Codigo { get; set; }

        public virtual EmpregadoSAP GestorPonto { get; set; }

        public virtual EmpregadoSAP SuperiorImediato { get; set; }
    }
}
