﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Pessoas
{
    /// <summary>
    /// Valores que representam StatusAcesso.
    /// </summary>    
    public enum TipoAcesso
    {
        /// <summary>
        /// An enum constant representing the TipoAcesso valor Pedestre.
        /// </summary>
        [Description("Pedestre")]
        P = 1,
        /// <summary>
        /// An enum constant representing the TipoAcesso valor Veiculo.
        /// </summary>
        [Description("Veículo")]
        V = 2,
        /// <summary>
        /// An enum constant representing the TipoAcesso valor Sem Acesso.
        /// </summary>
        [Description("Sem Acesso")]
        S = 3,
    }
}
