﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Pessoas
{
    /// <summary>
    /// Valores que representam StatusAcesso.
    /// </summary>    
    public enum GrupoPessoa
    {
        /// <summary>
        /// An enum constant representing the GrupoPessoa valor Próprio.
        /// </summary>
        [Description("Próprio")]
        Proprio = 1,
        /// <summary>
        /// An enum constant representing the GrupoPessoa valor Contratista.
        /// </summary>
        [Description("Contratista")]
        Contratista = 2,
    }
}
