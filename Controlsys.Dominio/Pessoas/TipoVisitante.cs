﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Controlsys.Dominio.Pessoas
{
    /// <summary>
    /// Valores que representam TipoVisitante.
    /// </summary>
    public enum TipoVisitante
    {
        /// <summary>
        /// An enum constant representing the tipo visitante 1 option.
        /// </summary>
        [Description ("Tipo Visitante 1")]
        TipoVisitante1 = 1,
        /// <summary>
        /// An enum constant representing the tipo visitante 2 option.
        /// </summary>
        [Description("Tipo Visitante 2")]
        TipoVisitante2 = 2
    }
}
