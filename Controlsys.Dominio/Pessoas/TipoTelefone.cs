﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controlsys.Dominio.Pessoas
{
    /// <summary>
    /// Valores que representam TipoTelefone.
    /// </summary>
    public enum TipoTelefone
    {
        /// <summary>
        /// An enum constant representing the residencial option.
        /// </summary>
        Residencial = 1,
        /// <summary>
        /// An enum constant representing the emergencial option.
        /// </summary>
        Emergencial = 2,
        /// <summary>
        /// An enum constant representing the empresarial option.
        /// </summary>
        Empresarial = 3,
        /// <summary>
        /// An enum constant representing the celular option.
        /// </summary>
        Celular = 4
    }
}
