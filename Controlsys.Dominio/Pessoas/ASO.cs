﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controlsys.Dominio.Pessoas
{
    /// <summary>
    /// Representa um(a) ASO.
    /// </summary>
    public class ASO
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public virtual int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Papel.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Papel.
        /// </value>
        public virtual Papel Papel { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataVencimento.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataVencimento.
        /// </value>
        public virtual DateTime DataVencimento { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataRealizacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataRealizacao.
        /// </value>
        public virtual DateTime DataRealizacao { get; set; }
    }
}
