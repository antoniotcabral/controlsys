﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Pessoas
{
    /// <summary>
    /// Valores que representam MaoDeObra.
    /// </summary>
    public enum MaoDeObra
    {
        /// <summary>
        /// An enum constant representing the Direta option.
        /// </summary>
        [Description("Direta")]
        Direta = 1,
        /// <summary>
        /// An enum constant representing the Indireta option.
        /// </summary>
        [Description("Indireta")]
        Indireta = 2,
        /// <summary>
        /// An enum constant representing the licenca Offsite option.
        /// </summary>
        [Description("Não Operativo")]
        NaoOperativo = 3
    }
}