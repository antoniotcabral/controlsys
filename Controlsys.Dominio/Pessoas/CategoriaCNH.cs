﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Pessoas
{
    /// <summary>
    /// Valores que representam CategoriaCNH.
    /// </summary>
    public enum CategoriaCNH
    {
        /// <summary>
        /// An enum constant representing a option.
        /// </summary>
        [Description("A")]
        A = 1,
        /// <summary>
        /// An enum constant representing the b option.
        /// </summary>
        [Description("B")]
        B = 2,
        /// <summary>
        /// An enum constant representing the c option.
        /// </summary>
        [Description("C")]
        C = 3,
        /// <summary>
        /// An enum constant representing the d option.
        /// </summary>
        [Description("D")]
        D = 4,
        /// <summary>
        /// An enum constant representing the e option.
        /// </summary>
        [Description("E")]
        E = 5,
        /// <summary>
        /// An enum constant representing the ab option.
        /// </summary>
        [Description("AB")]
        AB = 6,
        /// <summary>
        /// An enum constant representing the AC option.
        /// </summary>
        [Description("AC")]
        AC = 7,
        /// <summary>
        /// An enum constant representing the ad option.
        /// </summary>
        [Description("AD")]
        AD = 8,
        /// <summary>
        /// An enum constant representing the ae option.
        /// </summary>
        [Description("AE")]
        AE = 9
    }
}