﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Controlsys.Dominio.Pessoas
{
    /// <summary>
    /// Valores que representam TipoPrestador.
    /// </summary>
    public enum TipoPrestador
    {
        /// <summary>
        /// An enum constant representing the temporario option.
        /// </summary>
        [Description("Temporário")]
        Temporario = 1,
        /// <summary>
        /// An enum constant representing the emergencial option.
        /// </summary>
        Emergencial = 2
    }
}
