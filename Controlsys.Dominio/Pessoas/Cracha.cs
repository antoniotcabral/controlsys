﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Controlsys.Dominio.Parametros;

namespace Controlsys.Dominio.Pessoas
{
    /// <summary>
    /// Representa um(a) Cracha.
    /// </summary>
    public class Cracha
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public virtual int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para ModeloCracha.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) ModeloCracha.
        /// </value>
        public virtual ModeloCracha ModeloCracha { get; set; }

        /// <summary>
        /// Gets/Sets valor para Papel.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Papel.
        /// </value>
        public virtual Papel Papel { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataDesativacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataDesativacao.
        /// </value>
        public virtual DateTime? DataDesativacao { get; set; }

        /// <summary>
        /// Gets/Sets a situação do objetivo do tipo Cracha.
        /// </summary>
        ///
        /// <value>
        /// <see cref="DataDesativacao"/> nula retorna true, <see cref="DataDesativacao"/> com valor
        /// retorna false.
        /// </value>
        public virtual bool Ativo { get; protected set; }

        /// <summary>
        /// Gets/Sets valor para DataDescarte.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataDescarte.
        /// </value>
        public virtual DateTime? DataDescarte { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataImpressao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataImpressao.
        /// </value>
        public virtual DateTime? DataImpressao { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataRegistro.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataRegistro.
        /// </value>
        public virtual DateTime DataRegistro { get; set; }

        /// <summary>
        /// Gets/Sets valor para MotivoDesativacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) MotivoDesativacao.
        /// </value>
        public virtual string MotivoDesativacao { get; set; }

        /// <summary>
        /// Gets/Sets valor para Numero.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Numero.
        /// </value>
        public virtual string Numero { get; set; }

        /// <summary>
        /// Ativa o objeto, definindo <see cref="Ativo"/> como true e <see cref="DataDesativacao"/> como
        /// nulo.
        /// </summary>
        public virtual void Ativar()
        {
            Ativo = true;
            DataDesativacao = null;
        }

        /// <summary>
        /// Inativa o objeto, definindo <see cref="Ativo"/> como falso e <see cref="DataDesativacao"/>
        /// como a data atual.
        /// </summary>
        public virtual void Inativar()
        {
            Ativo = false;
            DataDescarte = DateTime.Now;
            DataDesativacao = DateTime.Now;
        }               
    }
}
