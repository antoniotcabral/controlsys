﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Controlsys.Dominio.Enderecos;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Seguranca;
using Globalsys.Exceptions;

namespace Controlsys.Dominio.Pessoas
{
    /// <summary>
    /// Representa um(a) PessoaFisica.
    /// </summary>
    public class PessoaFisica : Pessoa
    {
        /// <summary>
        /// Construtor para Controlsys.Dominio.Pessoas.PessoaFisica.
        /// </summary>
        public PessoaFisica()
        {
            Papeis = new List<Papel>();
            Telefones = new List<Telefone>();
            Emails = new List<Email>();
        }

        /// <summary>
        /// Gets/Sets valor para CPF.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CPF.
        /// </value>
        public virtual string CPF { get; set; }

        /// <summary>
        /// Gets/Sets valor para TipoSanguineo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TipoSanguineo.
        /// </value>
        public virtual TipoSanguineo? TipoSanguineo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Sexo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Sexo.
        /// </value>
        public virtual Sexo Sexo { get; set; }

        /// <summary>
        /// Gets/Sets valor para EstadoCivil.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) EstadoCivil.
        /// </value>
        public virtual EstadoCivil? EstadoCivil { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataExpedicaoRNE.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataExpedicaoRNE.
        /// </value>
        public virtual DateTime? DataExpedicaoRNE { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataNascimento.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataNascimento.
        /// </value>
        public virtual DateTime? DataNascimento { get; set; }

        /// <summary>
        /// Gets/Sets valor para NomePai.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) NomePai.
        /// </value>
        public virtual string NomePai { get; set; }

        /// <summary>
        /// Gets/Sets valor para NomeMae.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) NomeMae.
        /// </value>
        public virtual string NomeMae { get; set; }

        /// <summary>
        /// Gets/Sets valor para RG.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) RG.
        /// </value>
        public virtual string RG { get; set; }

        /// <summary>
        /// Gets/Sets valor para RGOrgaoEmissor.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) RGOrgaoEmissor.
        /// </value>
        public virtual string RGOrgaoEmissor { get; set; }

        /// <summary>
        /// Gets/Sets valor para RGDataEmissao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) RGDataEmissao.
        /// </value>
        public virtual DateTime? RGDataEmissao { get; set; }

        /// <summary>
        /// Gets/Sets valor para Passaporte.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Passaporte.
        /// </value>
        public virtual string Passaporte { get; set; }

        /// <summary>
        /// Gets/Sets valor para Nacionalidade.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Nacionalidade.
        /// </value>
        public virtual string Nacionalidade { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataVisto.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataVisto.
        /// </value>
        public virtual DateTime? DataVisto { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataValidadeVisto.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataValidadeVisto.
        /// </value>
        public virtual DateTime? DataValidadeVisto { get; set; }

        /// <summary>
        /// Gets/Sets valor para Foto.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Foto.
        /// </value>
        public virtual byte[] Foto { get; set; }

        /// <summary>
        /// Gets/Sets valor para RGOrgaoEmissorUF.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) RGOrgaoEmissorUF.
        /// </value>
        public virtual Estado RGOrgaoEmissorUF { get; set; }

        /// <summary>
        /// Gets/Sets valor para Naturalidade.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Naturalidade.
        /// </value>
        public virtual Cidade Naturalidade { get; set; }

        /// <summary>
        /// Gets/Sets valor para CTPSSerie.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CTPSSerie.
        /// </value>
        public virtual string CTPSSerie { get; set; }

        /// <summary>
        /// Gets/Sets valor para CTPS.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CTPS.
        /// </value>
        public virtual long? CTPS { get; set; }

        /// <summary>
        /// Gets/Sets valor para CTPSEstado.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CTPSEstado.
        /// </value>
        public virtual Estado CTPSEstado { get; set; }

        /// <summary>
        /// Gets/Sets valor para CTPSData.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CTPSData.
        /// </value>
        public virtual DateTime? CTPSData { get; set; }

        /// <summary>
        /// Gets/Sets valor para CNH.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CNH.
        /// </value>
        public virtual string CNH { get; set; }

        /// <summary>
        /// Gets/Sets valor para CNHCategoria.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CNHCategoria.
        /// </value>
        public virtual CategoriaCNH? CNHCategoria { get; set; }

        /// <summary>
        /// Gets/Sets valor para CNHDataValidade.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CNHDataValidade.
        /// </value>
        public virtual DateTime? CNHDataValidade { get; set; }

        /// <summary>
        /// Gets/Sets valor para TituloEleitor.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TituloEleitor.
        /// </value>
        public virtual long? TituloEleitor { get; set; }

        /// <summary>
        /// Gets/Sets valor para TituloEleitorZona.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TituloEleitorZona.
        /// </value>
        public virtual int? TituloEleitorZona { get; set; }

        /// <summary>
        /// Gets/Sets valor para TituloEleitorSecao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TituloEleitorSecao.
        /// </value>
        public virtual int? TituloEleitorSecao { get; set; }

        /// <summary>
        /// Gets/Sets valor para TituloEleitorCidade.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TituloEleitorCidade.
        /// </value>
        public virtual Cidade TituloEleitorCidade { get; set; }

        /// <summary>
        /// Gets/Sets valor para CertificadoReservista.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CertificadoReservista.
        /// </value>
        public virtual string CertificadoReservista { get; set; }

        /// <summary>
        /// Gets/Sets valor para CertificadoReservistaCat.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CertificadoReservistaCat.
        /// </value>
        public virtual string CertificadoReservistaCat { get; set; }

        /// <summary>
        /// Gets/Sets valor para RNE.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) RNE.
        /// </value>
        public virtual string RNE { get; set; }


        public virtual Usuario Usuario { get; set; }

        /// <summary>
        /// Gets/Sets valor para Escolaridade.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Escolaridade.
        /// </value>
        public virtual Escolaridade? Escolaridade { get; set; }

        /// <summary>
        /// Gets/Sets valor para Emails.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Emails.
        /// </value>
        public virtual IList<Email> Emails { get; set; }

        /// <summary>
        /// Gets/Sets valor para Papeis.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Papeis.
        /// </value>
        public virtual IList<Papel> Papeis { get; set; }

        /// <summary>
        /// Obter papel.
        /// </summary>
        ///
        /// <param name="somenteAtivos">
        /// true to somente ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Papel.
        /// </returns>
        public virtual Papel ObterPapel(bool somenteAtivos = false, bool buscarColab = true, bool buscarPrest = true, bool buscarVisit = true, bool ExcetoStatusBaixaDefinitiva = false)
        {
            IEnumerable<Papel> papeis = Papeis;

            papeis = papeis.OrderByDescending(p => p.ObterLogPapel().Codigo).AsEnumerable();

            if (!buscarColab)
                papeis = papeis.Where(p => !(p is Colaborador)).AsEnumerable();

            if (!buscarPrest)
                papeis = papeis.Where(p => !(p is PrestadorServico)).AsEnumerable();

            if (!buscarVisit)
                papeis = papeis.Where(p => !(p is Visitante)).AsEnumerable();

            if (somenteAtivos)
                papeis = papeis.Where(p => p.ObterLogPapel().Status == StatusPapelLog.Ativo).AsEnumerable();

            if (ExcetoStatusBaixaDefinitiva)
                papeis = papeis.Where(p => p.ObterLogPapel().Status != StatusPapelLog.BaixaDefinitiva).AsEnumerable();

            return papeis.FirstOrDefault();
        }

        /// <summary>
        /// Cpf formatado.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) string.
        /// </returns>
        public virtual string CPFFormatado()
        {
            return !string.IsNullOrEmpty(CPF) ? string.Format(@"{0:000\.000\.000\-00}", Convert.ToInt64(CPF)) : string.Empty;
        }

        public virtual bool CPFValido()
        {
            return CPFValido(CPF);
        }

        public static bool CPFValido(string cpf)
        {
            int[] multiplicador1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            string tempCpf;
            string digito;
            int soma;
            int resto;
            cpf = cpf.Trim();
            cpf = cpf.Replace(".", "").Replace("-", "");
            if (cpf.Length != 11)
                return false;
            tempCpf = cpf.Substring(0, 9);
            soma = 0;

            for (int i = 0; i < 9; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador1[i];
            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = resto.ToString();
            tempCpf = tempCpf + digito;
            soma = 0;
            for (int i = 0; i < 10; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador2[i];
            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = digito + resto.ToString();
            return cpf.EndsWith(digito);

        }
    }
}
