﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controlsys.Dominio.Pessoas
{
    /// <summary>
    /// Representa um(a) Visitante.
    /// </summary>
    public class Visitante : Papel
    {
        /// <summary>
        /// Gets/Sets valor para TipoVisitante.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TipoVisitante.
        /// </value>
        public virtual TipoVisitante TipoVisitante { get; set; }

        /// <summary>
        /// Gets/Sets valor para AcessoPorto.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) AcessoPorto.
        /// </value>
        public virtual bool AcessoPorto { get; set; }

        /// <summary>
        /// Gets/Sets valor para Empresa.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Empresa.
        /// </value>
        public virtual string Empresa { get; set; }

        /// <summary>
        /// Gets/Sets valor para Funcao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Funcao.
        /// </value>
        public virtual string Funcao { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataInicio.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataInicio.
        /// </value>
        public virtual DateTime DataInicio { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataFim.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataFim.
        /// </value>
        public virtual DateTime DataFim { get; set; }

        /// <summary>
        /// Gets/Sets valor para Solicitante.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Solicitante.
        /// </value>
        public virtual Colaborador Solicitante { get; set; }

        /// <summary>
        /// Gets/Sets valor para ResponsavelVisita.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) ResponsavelVisita.
        /// </value>
        public virtual Colaborador ResponsavelVisita { get; set; }

        /// <summary>
        /// Gets/Sets valor para Motivo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Motivo.
        /// </value>
        public virtual string Motivo { get; set; }

        public virtual string CodigoCracha { get; set; }

        /// <summary>
        /// Crachá do visitante.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) cracha.
        /// </returns>
        /// 
    }
}
