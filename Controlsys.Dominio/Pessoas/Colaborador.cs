﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Enderecos;
using Controlsys.Dominio.Parametros;

namespace Controlsys.Dominio.Pessoas
{
    /// <summary>
    /// Representa um(a) Colaborador.
    /// </summary>
    public class Colaborador : Papel
    {
        /// <summary>
        /// Construtor para Controlsys.Dominio.Pessoas.Colaborador.
        /// </summary>
        public Colaborador()
        {
            GruposTrabalho = new List<GrupoTrabalhoColab>();
        }

        /// <summary>
        /// Gets/Sets valor para PIS.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) PIS.
        /// </value>
        public virtual string PIS { get; set; }
        
        /// <summary>
        /// Gets/Sets valor para Observacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Observacao.
        /// </value>
        public virtual string Observacao { get; set; }

        /// <summary>
        /// Gets/Sets valor para Empresa.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Empresa.
        /// </value>
        public virtual Empresa Empresa { get; set; }

        //Alterado por Antonio Cabral 15/04/20, atendimento chamado-16828-tx1362323-item-289-mudança-webservice-novidadespessoasap
        public virtual string SubContratista { get; set; }

        public virtual string Cnpj_SubContratista { get; set; }
        //------------------------------------------------------------------------------------------------
        /// <summary>
        /// Gets/Sets valor para Area.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Area.
        /// </value>
        public virtual Area Area { get; set; }

        /// <summary>
        /// Gets/Sets valor para HoristaMensalista.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) HoristaMensalista.
        /// </value>
        public virtual HoristaMensalista HoristaMensalista { get; set; }

        /// <summary>
        /// Gets/Sets valor para Salario.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Salario.
        /// </value>
        public virtual decimal Salario { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataAdmissao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataAdmissao.
        /// </value>
        public virtual DateTime? DataAdmissao { get; set; }


        /// <summary>
        /// Gets/Sets valor para DataValidade.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataValidade.
        /// </value>
        public virtual DateTime? DataValidade { get; set; }

        /// <summary>
        /// Gets/Sets valor para ConselhoProfissional.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) ConselhoProfissional.
        /// </value>
        public virtual string ConselhoProfissional { get; set; }

        /// <summary>
        /// Gets/Sets valor para RegistroProfissional.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) RegistroProfissional.
        /// </value>
        public virtual string RegistroProfissional { get; set; }

        /// <summary>
        /// Gets/Sets valor para GruposTrabalho.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) GruposTrabalho.
        /// </value>
        public virtual IList<GrupoTrabalhoColab> GruposTrabalho { get; set; }

        /// <summary>
        /// Obter grupo trabalho.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) GrupoTrabalhoColab.
        /// </returns>
        /// 

        public virtual string CrachaColaborador { get; set; }

        /// <summary>
        /// Obter grupo trabalho.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) GrupoTrabalhoColab.
        /// </returns>
        /// 

        public virtual GrupoTrabalhoColab ObterGrupoTrabalho()
        {
            return GruposTrabalho.Where(gt => gt.Ativo).OrderByDescending(gt => gt.DataRegistro).FirstOrDefault();
        }

        /// <summary>
        /// Gets/Sets valor para Mão de Obra.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Mão de Obra.
        /// </value>
        public virtual MaoDeObra MaoDeObra { get; set; }

        /// <summary>
        /// Gets/Sets valor para Grupo de Pessoa.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Grupo de Pessoa.
        /// </value>
        public virtual GrupoPessoa GrupoPessoa { get; set; }

        /// <summary>
        /// Gets/Sets valor para SetorCusto.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) SetorCusto.
        /// </value>
        public virtual SetorCusto SetorCusto { get; set; }

        /// <summary>
        /// Gets/Sets valor para AcessoPorto.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) AcessoPorto.
        /// </value>
        public virtual bool AcessoPorto { get; set; }

        /// <summary>
        /// Gets/Sets valor para IdMotivo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) IdMotivo.
        /// </value>
        public virtual string IdMotivo { get; set; }

        /// <summary>
        /// Gets/Sets valor para DescMotivo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DescMotivo.
        /// </value>
        public virtual string DescMotivo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Processado.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Processado.
        /// </value>
        public virtual DateTime? Processado { get; set; }

        /// <summary>
        /// Gets/Sets valor para TipoAcesso.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TipoAcesso.
        /// </value>
        public virtual TipoAcesso TipoAcesso { get; set; }

    }
}
