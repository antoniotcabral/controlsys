﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Pessoas
{
    /// <summary>
    /// Valores que representam Escolaridade.
    /// </summary>
    public enum Escolaridade
    {
        /// <summary>
        /// An enum constant representing the analfabeto option.
        /// </summary>
        [Description("Analfabeto")]
        Analfabeto = 1,

        /// <summary>
        /// An enum constant representing the ensino fundamental incompleto option.
        /// </summary>
        [Description("Ensino fundamental incompleto")]
        EnsinoFundamentalIncompleto = 2,

        /// <summary>
        /// An enum constant representing the ensino fundamental completo option.
        /// </summary>
        [Description("Ensino fundamental completo")]
        EnsinoFundamentalCompleto = 3,

        /// <summary>
        /// An enum constant representing the ensino medio incompleto option.
        /// </summary>
        [Description("Ensino médio incompleto")]
        EnsinoMedioIncompleto = 4,

        /// <summary>
        /// An enum constant representing the ensino medio completo option.
        /// </summary>
        [Description("Ensino médio completo")]
        EnsinoMedioCompleto = 5,

        /// <summary>
        /// An enum constant representing the ensino position medio completo option.
        /// </summary>
        [Description("Ensino pós-médio completo")]
        EnsinoPosMedioCompleto = 6,

        /// <summary>
        /// An enum constant representing the ensino superior incompleto option.
        /// </summary>
        [Description("Ensino superior incompleto")]
        EnsinoSuperiorIncompleto = 7,

        /// <summary>
        /// An enum constant representing the ensino superior completo option.
        /// </summary>
        [Description("Ensino superior completo")]
        EnsinoSuperiorCompleto = 8,
        
        /// <summary>
        /// An enum constant representing the position graduado incompleto option.
        /// </summary>
        [Description("Pós-graduação incompleta")]
        PosGraduadoIncompleto = 9,

        /// <summary>
        /// An enum constant representing the position graduado option.
        /// </summary>
        [Description("Pós-graduação completa")]
        PosGraduado = 10,

        /// <summary>
        /// An enum constant representing the mestrado option.
        /// </summary>
        [Description("Mestrado")]
        Mestrado = 11,

        /// <summary>
        /// An enum constant representing the doutorado option.
        /// </summary>
        [Description("Doutorado")]
        Doutorado = 12,

        /// <summary>
        /// An enum constant representing the outros option.
        /// </summary>
        [Description("Outros")]
        Outros = 13

    }
}
