﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Controlsys.Dominio.Pessoas
{
    /// <summary>
    /// Valores que representam StatusPapelLog.
    /// </summary>
    public enum StatusPapelLog
    {
        /// <summary>
        /// An enum constant representing the ativo option.
        /// </summary>
        [Description("Ativo")]
        Ativo = 1,
        /// <summary>
        /// An enum constant representing the afastamento option.
        /// </summary>
        [Description("Afastamento")]
        Afastamento = 2,
        /// <summary>
        /// An enum constant representing the licenca medica option.
        /// </summary>
        [Description("Licença Médica")]
        LicencaMedica = 3,
        /// <summary>
        /// An enum constant representing the ferias option.
        /// </summary>
        [Description("Férias")]
        Ferias = 4,
        /// <summary>
        /// An enum constant representing the inativacao option.
        /// </summary>
        [Description("Inativação")]
        Inativacao = 5,
        /// <summary>
        /// An enum constant representing the baixa definitiva option.
        /// </summary>
        [Description("Baixa Definitiva")]
        BaixaDefinitiva = 6
    }
}
