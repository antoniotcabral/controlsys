﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Pessoas
{
    /// <summary>
    /// Valores que representam EstadoCivil.
    /// </summary>
    public enum EstadoCivil
    {
        /// <summary>
        /// An enum constant representing the solteiro option.
        /// </summary>
        Solteiro = 1,
        /// <summary>
        /// An enum constant representing the uniao estavel option.
        /// </summary>
        [Description("União Estável")]
        UniaoEstavel = 2,
        /// <summary>
        /// An enum constant representing the casado option.
        /// </summary>
        Casado = 3,
        /// <summary>
        /// An enum constant representing the separado option.
        /// </summary>
        Separado = 4,
        /// <summary>
        /// An enum constant representing the divorciado option.
        /// </summary>
        Divorciado = 5,
        /// <summary>
        /// An enum constant representing the viuvo option.
        /// </summary>
        [Description("Viúvo")]
        Viuvo = 6
    }
}
