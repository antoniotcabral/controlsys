﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controlsys.Dominio.Pessoas
{
    /// <summary>
    /// Valores que representam Sexo.
    /// </summary>
    public enum Sexo
    {
        /// <summary>
        /// An enum constant representing the feminino option.
        /// </summary>
        Feminino = 1,
        /// <summary>
        /// An enum constant representing the masculino option.
        /// </summary>
        Masculino = 2
    }
}
