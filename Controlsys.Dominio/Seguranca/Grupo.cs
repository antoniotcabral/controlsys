﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Controlsys.Dominio.Acesso;

namespace Controlsys.Dominio.Seguranca
{
    /// <summary>
    /// Representa um(a) Grupo.
    /// </summary>
    public class Grupo
    {
        /// <summary>
        /// Construtor para Controlsys.Dominio.Seguranca.Grupo.
        /// </summary>
        public Grupo()
        {
            GruposLeitoras = new List<GrupoLeitoraGrupo>();
            GruposUsuarios = new List<GrupoUsuario>();
            Permissoes = new List<Permissao>();
            Acoes = new List<Acao>();
        }

        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public virtual int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Nome.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Nome.
        /// </value>
        public virtual string Nome { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataDesativacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataDesativacao.
        /// </value>
        public virtual DateTime? DataDesativacao { get; set; }

        /// <summary>
        /// Gets/Sets a situação do objetivo do tipo Grupo.
        /// </summary>
        ///
        /// <value>
        /// <see cref="DataDesativacao"/> nula retorna true, <see cref="DataDesativacao"/> com valor
        /// retorna false.
        /// </value>
        public virtual bool Ativo { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataRegistro.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataRegistro.
        /// </value>
        public virtual DateTime DataRegistro { get; set; }

        /// <summary>
        /// Gets/Sets valor para GruposLeitoras.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) GruposLeitoras.
        /// </value>
        public virtual IList<GrupoLeitoraGrupo> GruposLeitoras { get; set; }

        /// <summary>
        /// Gets/Sets valor para GruposUsuarios.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) GruposUsuarios.
        /// </value>
        public virtual IList<GrupoUsuario> GruposUsuarios { get; set; }

        /// <summary>
        /// Gets/Sets valor para Permissoes.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Permissoes.
        /// </value>
        public virtual IList<Permissao> Permissoes { get; set; }

        /// <summary>
        /// Gets/Sets valor para Acoes.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Acoes.
        /// </value>
        public virtual IList<Acao> Acoes { get; set; }

        /// <summary>
        /// Gets/Sets valor para Descricao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Descricao.
        /// </value>
        public virtual string Descricao { get; set; }

        /// <summary>
        /// Ativa o objeto, definindo <see cref="Ativo"/> como true e <see cref="DataDesativacao"/> como
        /// nulo.
        /// </summary>
        public virtual void Ativar()
        {
            Ativo = true;
            DataDesativacao = null;
        }

        /// <summary>
        /// Inativa o objeto, definindo <see cref="Ativo"/> como falso e <see cref="DataDesativacao"/>
        /// como a data atual.
        /// </summary>
        public virtual void Inativar()
        {
            Ativo = false;
            DataDesativacao = DateTime.Now;
        }

        public virtual bool IsAdmin()
        {
            return this.Nome == "Administradores";
        }
    }
}
