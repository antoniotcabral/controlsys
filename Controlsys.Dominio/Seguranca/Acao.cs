﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controlsys.Dominio.Seguranca
{
    /// <summary>
    /// Representa um(a) Acao.
    /// </summary>
    public class Acao
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public virtual int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Nome.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Nome.
        /// </value>
        public virtual string Nome { get; set; }

        /// <summary>
        /// Gets/Sets valor para Descricao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Descricao.
        /// </value>
        public virtual string Descricao { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataRegistro.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataRegistro.
        /// </value>
        public virtual DateTime DataRegistro { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataDesativacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataDesativacao.
        /// </value>
        public virtual DateTime? DataDesativacao { get; set; }

        /// <summary>
        /// Gets/Sets a situação do objetivo do tipo Acao.
        /// </summary>
        ///
        /// <value>
        /// <see cref="DataDesativacao"/> nula retorna true, <see cref="DataDesativacao"/> com valor
        /// retorna false.
        /// </value>
        public virtual bool Ativo { get; /*protected*/ set; }

        /// <summary>
        /// Gets/Sets valor para URL.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) URL.
        /// </value>
        public virtual string Url { get; set; }

        /// <summary>
        /// Gets/Sets valor para Pagina.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Pagina.
        /// </value>
        public virtual Pagina Pagina { get; set; }

        /// <summary>
        /// Gets/Sets valor para Auditavel.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Auditavel.
        /// </value>
        public virtual bool Auditavel { get; set; }

    }
}
