﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controlsys.Dominio.Seguranca
{
    /// <summary>
    /// Representa um(a) Pagina.
    /// </summary>
    public class Pagina
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public virtual int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Nome.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Nome.
        /// </value>
        public virtual string Nome { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataRegistro.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataRegistro.
        /// </value>
        public virtual DateTime DataRegistro { get; set; }

        /// <summary>
        /// Gets/Sets valor para URL.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) URL.
        /// </value>
        public virtual string Url { get; set; }

        /// <summary>
        /// Gets/Sets a situação do objetivo do tipo Pagina.
        /// </summary>
        ///
        /// <value>
        /// <see cref="DataDesativacao"/> nula retorna true, <see cref="DataDesativacao"/> com valor
        /// retorna false.
        /// </value>
        public virtual bool Ativo
        {
            get { return !DataDesativacao.HasValue; }
            set { DataDesativacao = value ? (DateTime?)DateTime.Now : null; }
        }

        /// <summary>
        /// Gets/Sets valor para DataDesativacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataDesativacao.
        /// </value>
        public virtual DateTime? DataDesativacao { get; set; }

        /// <summary>
        /// Gets/Sets valor para Ordem.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Ordem.
        /// </value>
        public virtual int Ordem { get; set; }

        /// <summary>
        /// Gets/Sets valor para TipoPagina.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TipoPagina.
        /// </value>
        public virtual TipoPagina TipoPagina { get; set; }

        /// <summary>
        /// Gets/Sets valor para PaginaPai.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) PaginaPai.
        /// </value>
        public virtual Pagina PaginaPai { get; set; }

        /// <summary>
        /// Gets/Sets valor para PaginasFilhas.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) PaginasFilhas.
        /// </value>
        public virtual IList<Pagina> PaginasFilhas { get; set; }

        /// <summary>
        /// Gets/Sets valor para Acoes.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Acoes.
        /// </value>
        public virtual List<Acao> Acoes { get; set; }

        /// <summary>
        /// Gets/Sets valor para Icone.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Icone.
        /// </value>
        public virtual string Icone { get; set; }
    }
}
