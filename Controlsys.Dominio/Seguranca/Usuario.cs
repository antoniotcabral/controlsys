﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Controlsys.Dominio.Pessoas;
using Globalsys.Util;

namespace Controlsys.Dominio.Seguranca
{
    /// <summary>
    /// Representa um(a) Usuario.
    /// </summary>
    public class Usuario
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public virtual string Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Login.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Login.
        /// </value>
        public virtual string Login { get; set; }

        /// <summary>
        /// Gets/Sets valor para Senha.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Senha.
        /// </value>
        public virtual string Senha { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataRegistro.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataRegistro.
        /// </value>
        public virtual DateTime DataRegistro { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataDesativacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataDesativacao.
        /// </value>
        public virtual DateTime? DataDesativacao { get; set; }

        /// <summary>
        /// Gets/Sets a situação do objetivo do tipo Usuario.
        /// </summary>
        ///
        /// <value>
        /// <see cref="DataDesativacao"/> nula retorna true, <see cref="DataDesativacao"/> com valor
        /// retorna false.
        /// </value>
        public virtual bool Ativo { get; /*protected*/ set; }

        /// <summary>
        /// Gets/Sets valor para AD.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) AD.
        /// </value>
        public virtual bool AD { get; set; }

        /// <summary>
        /// Gets/Sets valor para PessoaFisica.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) PessoaFisica.
        /// </value>
        public virtual PessoaFisica PessoaFisica { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataLogin.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataLogin.
        /// </value>
        public virtual DateTime? DataLogin { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataLogoff.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataLogoff.
        /// </value>
        public virtual DateTime? DataLogoff { get; set; }

        /// <summary>
        /// Gets/Sets valor para Logado.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Logado.
        /// </value>
        public virtual bool Logado { get; set; }

        /// <summary>
        /// Gets/Sets valor para GruposUsuarios.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) GruposUsuarios.
        /// </value>
        public virtual IList<GrupoUsuario> GruposUsuarios { get; set; }

        /// <summary>
        /// Ativa o objeto, definindo <see cref="Ativo"/> como true e <see cref="DataDesativacao"/> como
        /// nulo.
        /// </summary>
        public virtual void Ativar()
        {
            Ativo = true;
            DataDesativacao = null;
        }

        /// <summary>
        /// Inativa o objeto, definindo <see cref="Ativo"/> como falso e <see cref="DataDesativacao"/>
        /// como a data atual.
        /// </summary>
        public virtual void Inativar()
        {
            Ativo = false;
            DataDesativacao = DateTime.Now;
        }

        /// <summary>
        /// Gera senha alfanumerica e atribui ao usuário. Verifica se o usuário poderá ter a senha
        /// gerada. Criptografa a senha em MD5.
        /// </summary>
        ///
        /// <returns>
        /// Retorna se a senha gerada sem criptografia.
        /// </returns>
        public virtual string GerarSenha()
        {
            string senha = string.Empty;

            if (!AD)
            {
                senha = Tools.GerarSenhaAleatoria();
                Senha = Tools.CriptografarMD5(senha);
            }

            return senha;
        }        
    }
}
