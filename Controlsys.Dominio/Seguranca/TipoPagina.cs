﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Seguranca
{
    /// <summary>
    /// Valores que representam TipoPagina.
    /// </summary>
    public enum TipoPagina
    {
        /// <summary>
        /// An enum constant representing the modulo option.
        /// </summary>
        Modulo = 1,
        /// <summary>
        /// An enum constant representing the pagina option.
        /// </summary>
        Pagina = 2
    }
}
