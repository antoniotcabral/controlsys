﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Parametros;

namespace Controlsys.Dominio.CredenciamentoVeiculos
{
    public class Veiculo
    {
        public Veiculo()
        {
            Midias = new List<VeiculoMidia>();
        }

        public virtual int Codigo { get; set; }

        public virtual string Placa { get; set; }

        public virtual ModeloVeiculo Modelo { get; set; }

        public virtual int AnoFabricacao { get; set; }

        public virtual int AnoModelo { get; set; }

        public virtual string Cor { get; set; }

        public virtual DateTime DataRegistro { get; set; }

        public virtual IList<VeiculoMidia> Midias { get; set; }
    }
}
