﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Controlsys.Dominio.CredenciamentoVeiculos;

namespace Controlsys.Dominio.CredenciamentoVeiculos
{
    public class SituacaoCredencialVeiculo
    {
        public virtual int Codigo { get; set; }

        public virtual StatusCredencial Status { get; set; }

        public virtual string Observacao { get; set; }

        public virtual DateTime DataRegistro { get; set; }

        public virtual CredencialVeiculo Credencial { get; set; }

    }
}
