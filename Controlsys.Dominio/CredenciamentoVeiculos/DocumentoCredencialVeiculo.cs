﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Controlsys.Dominio.Parametros;

namespace Controlsys.Dominio.CredenciamentoVeiculos
{
    public class DocumentoCredencialVeiculo
    {
        public virtual int Codigo { get; set; }

        public virtual bool Ativo { get; set; }

        public virtual DateTime DataRegistro { get; set; }

        public virtual DateTime? DataDesativacao { get; set; }

        public virtual CredencialVeiculo CredencialVeiculo { get; set; }

        public virtual Documento Documento { get; set; }
    }
}
