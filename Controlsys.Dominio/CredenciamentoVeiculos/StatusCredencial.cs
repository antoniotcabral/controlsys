﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Controlsys.Dominio.CredenciamentoVeiculos
{
    public enum StatusCredencial
    {
        [Description("Recebida")]
        Recebida,

        [Description("Inspecionada")]
        Inspecionada,

        [Description("Aprovada")]
        Aprovada,

        [Description("Impressa")]
        Impressa,

        [Description("Devolvida")]
        Devolvida
    }
}
