﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Controlsys.Dominio.Seguranca;

namespace Controlsys.Dominio.CredenciamentoVeiculos
{
    public class CredencialVeiculo
    {
        public CredencialVeiculo()
        {
            Veiculos = new List<VeiculoCredencialVeiculo>();
            Situacoes = new List<SituacaoCredencialVeiculo>();
        }

        public virtual int Codigo { get; set; }

        public virtual DateTime DataRegistro { get; set; }

        public virtual Usuario Usuario { get; set; }

        public virtual DateTime? DataVencimento { get; set; }

        public virtual Empresa Empresa { get; set; }

        public virtual PedidoCompra PedidoCompra { get; set; }

        public virtual Area Area { get; set; }

        public virtual Empresa SubContratada { get; set; }

        public virtual string Fornecedor { get; set; }

        public virtual Papel Condutor { get; set; }

        public virtual ModeloCredencial ModeloCredencial { get; set; }

        public virtual TipoValidade TipoValidade { get; set; }

        public virtual int? ValidadeDiasCredencial { get; set; }

        public virtual DateTime? DataValidadeLit { get; set; }

        public virtual DateTime DataVencimentoSeguro { get; set; }

        public virtual bool Corporativo { get; set; }

        public virtual string Observacao { get; set; }

        public virtual IList<VeiculoCredencialVeiculo> Veiculos { get; set; }

        public virtual IList<SituacaoCredencialVeiculo> Situacoes { get; set; }

        public virtual SituacaoCredencialVeiculo ObterSituacao()
        { return Situacoes.OrderByDescending(s => s.Codigo).FirstOrDefault(); }

        public virtual bool ImprimirVerso()
        {
            if (ModeloCredencial == null) return false;
            return !string.IsNullOrEmpty(ModeloCredencial.HtmlVerso);
        }
    }
}
