﻿using Controlsys.Dominio.Pessoas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Acesso
{
    public class AcessoRestaurante
    {
        public AcessoRestaurante()
        {
            TiposAlimentacao = new List<AcessoRestauranteTipoAlimentacao>();
            LogAcessoRestaurante = new List<LogAcessoRestaurante>();
        }

        public virtual int Codigo { get; set; }

        public virtual Papel Papel { get; set; }

        public virtual IList<LogAcessoRestaurante> LogAcessoRestaurante { get; set; }

        public virtual IList<AcessoRestauranteTipoAlimentacao> TiposAlimentacao { get; set; }

        public virtual DateTime DataRegistro { get; set; }

        public virtual DateTime? DataDesativacao { get; set; }

        public virtual bool Ativo { get; protected set; }

        public virtual void Ativar()
        {
            Ativo = true;
            DataDesativacao = null;
        }

        public virtual void Inativar()
        {
            Ativo = false;
            DataDesativacao = DateTime.Now;
        }

    }
}
