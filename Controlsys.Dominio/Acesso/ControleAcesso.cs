﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Controlsys.Dominio.Pessoas;

namespace Controlsys.Dominio.Acesso
{
    /// <summary>
    /// Representa um(a) ControleAcesso.
    /// </summary>
    public class ControleAcesso
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public virtual int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Leitora.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Leitora.
        /// </value>
        public virtual Leitora Leitora { get; set; }

        /// <summary>
        /// Gets/Sets valor para NumeroSerie.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) NumeroSerie.
        /// </value>
        public virtual string NumeroSerie { get; set; }

        /// <summary>
        /// Gets/Sets valor para Cracha.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Cracha.
        /// </value>
        public virtual Cracha Cracha { get; set; }

        /// <summary>
        /// Gets/Sets valor para Papel.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Papel.
        /// </value>
        public virtual Papel Papel { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataRegistro.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataRegistro.
        /// </value>
        public virtual DateTime DataRegistro { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataAcesso.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataAcesso.
        /// </value>
        public virtual DateTime DataAcesso { get; set; }

        /// <summary>
        /// Gets/Sets valor para StatusAcesso.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) StatusAcesso.
        /// </value>
        public virtual StatusAcesso StatusAcesso { get; set; }

        /// <summary>
        /// Gets/Sets valor para Direcao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Direcao.
        /// </value>
        public virtual DirecaoLeitora Direcao { get; set; }                        

        /// <summary>
        /// Gets/Sets valor para Local.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Local.
        /// </value>
        public virtual string Local { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataIntegracao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataIntegracao.
        /// </value>
        public virtual DateTime? DataIntegracao { get; set; }
    }
}
