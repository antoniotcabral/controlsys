﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Acesso
{
    /// <summary>
    /// Representa um(a) Controladora.
    /// </summary>
    public class Controladora
    {
        /// <summary>
        /// Construtor para Controlsys.Dominio.Acesso.Controladora.
        /// </summary>
        public Controladora()
        {
            Leitoras = new List<Leitora>();
            Logs = new List<LogControladora>();
        }

        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public virtual int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Modelo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Modelo.
        /// </value>
        public virtual string Modelo { get; set; }

        /// <summary>
        /// Gets/Sets valor para IP.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) IP.
        /// </value>
        public virtual string IP { get; set; }

        /// <summary>
        /// Gets/Sets valor para Online.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Online.
        /// </value>
        public virtual bool Online { get; set; }

        /// <summary>
        /// Gets/Sets valor para HoraSincronizacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) HoraSincronizacao.
        /// </value>
        public virtual TimeSpan? HoraSincronizacao { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataRegistro.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataRegistro.
        /// </value>
        public virtual DateTime DataRegistro { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataDesativacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataDesativacao.
        /// </value>
        public virtual DateTime? DataDesativacao { get; set; }

        /// <summary>
        /// Gets/Sets a situação do objetivo do tipo Controladora.
        /// </summary>
        ///
        /// <value>
        /// <see cref="DataDesativacao"/> nula retorna true, <see cref="DataDesativacao"/> com valor
        /// retorna false.
        /// </value>
        public virtual bool Ativo { get; set; }

        /// <summary>
        /// Gets/Sets valor para TipoControladora.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TipoControladora.
        /// </value>
        public virtual TipoControladora TipoControladora { get; set; }

        /// <summary>
        /// Gets/Sets valor para Leitoras.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Leitoras.
        /// </value>
        public virtual IList<Leitora> Leitoras { get; set; }

        public virtual IList<LogControladora> Logs { get; set; }

        public virtual decimal Tensao { get; set; }

        public virtual int Temperatura { get; set; }

        public virtual decimal Corrente { get; set; }

        public virtual int QtdMaxListaOrdenada { get; set; }

        public virtual int QtdListaOrdenada { get; set; }

        public virtual int QtdMaxListaDesordenada { get; set; }

        public virtual int QtdListaDesordenada { get; set; }

        public virtual int NumeroSerial { get; set; }

        public virtual string VersaoFirmware { get; set; }

        public virtual DateTime? DataUltimaAlteracaoOnOff { get; set; }

        public virtual StatusControladora? Status { get; set; }

        public virtual DateTime? DataStatus { get; set; }        

        public virtual void AlterarStatus(StatusControladora status)
        {
            DateTime dtAtual = DateTime.Now;

            switch (status)
            {
                case StatusControladora.Ativo:
                    Ativo = true;
                    DataDesativacao = null;
                    break;
                case StatusControladora.Inativo:
                    Ativo = false;
                    DataDesativacao = dtAtual;
                    break;                
            }

            Status = status;
            DataStatus = dtAtual;
        }

        /// <summary>
        /// Gets/Sets Valida as câmeras da Controladora.
        /// </summary>
        ///
        /// <value>
        /// <see cref="ValidaOCR"/> nula retorna true, <see cref="ValidaOCR"/> com valor
        /// retorna false.
        /// </value>
        public virtual bool ValidaOCR { get; set; }

        /// <summary>
        /// Obter Log Atual
        /// </summary>
        /// <returns></returns>
        public virtual LogControladora ObterLog()
        {
            return Logs.OrderByDescending(l => l.Codigo).FirstOrDefault();
        }
    }
}
