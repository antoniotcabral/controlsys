﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Controlsys.Dominio.Seguranca;

namespace Controlsys.Dominio.Acesso
{
    /// <summary>
    /// Representa um(a) PessoaCritica.
    /// </summary>
    public class PessoaCritica
    {
        public PessoaCritica() 
        {
            this.Midias = new List<PessoaCriticaMidia>();
        }

        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public virtual int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para PessoaFisica.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) PessoaFisica.
        /// </value>
        public virtual PessoaFisica PessoaFisica { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataBloqueio.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataBloqueio.
        /// </value>
        public virtual DateTime DataBloqueio { get; set; }

        /// <summary>
        /// Gets/Sets valor para MotivoBloqueio.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) MotivoBloqueio.
        /// </value>
        public virtual MotivoBloqueio MotivoBloqueio { get; set; }

        /// <summary>
        /// Gets/Sets valor para Observacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Observacao.
        /// </value>
        public virtual string Observacao { get; set; }

        /// <summary>
        /// Gets/Sets valor para Responsavel.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Responsavel.
        /// </value>
        public virtual Usuario Responsavel { get; set; }

        /// <summary>
        /// Gets/Sets valor para Documentos.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Documentos.
        /// </value>
        public virtual IList<DocumentoPessoaCritica> Documentos { get; set; }

        /// <summary>
        /// Gets/Sets valor para Midias.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Midias.
        /// </value>
        public virtual IList<PessoaCriticaMidia> Midias { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataRegistro.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataRegistro.
        /// </value>
        public virtual DateTime DataRegistro { get; set; }

        /// <summary>
        /// Gets/Sets a situação do objetivo do tipo PessoaCritica.
        /// </summary>
        ///
        /// <value>
        /// <see cref="DataDesativacao"/> nula retorna true, <see cref="DataDesativacao"/> com valor
        /// retorna false.
        /// </value>
        public virtual bool Ativo { get; /*protected*/ set; }

        /// <summary>
        /// Gets/Sets valor para DataDesativacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataDesativacao.
        /// </value>
        public virtual DateTime? DataDesativacao { get; set; }

        /// <summary>
        /// Ativa o objeto, definindo <see cref="Ativo"/> como true e <see cref="DataDesativacao"/> como
        /// nulo.
        /// </summary>
        public virtual void Ativar()
        {
            Ativo = true;
            DataDesativacao = null;
        }

        /// <summary>
        /// Inativa o objeto, definindo <see cref="Ativo"/> como falso e <see cref="DataDesativacao"/>
        /// como a data atual.
        /// </summary>
        public virtual void Inativar()
        {
            Ativo = false;
            DataDesativacao = DateTime.Now;
        }
    }
}
