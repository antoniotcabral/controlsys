﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Acesso
{
    /// <summary>
    /// Representa a imagem em string
    /// </summary>
  public class ImagemPlaca
    {

        /// <summary>
        /// Representa a camera onde puxará a imagem
        /// </summary>
        public Camera Camera { get; set; }


        /// <summary>
        /// Imagem em string Base64
        /// </summary>
        public string Imagem { get; set; }
    }
}
