﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Controlsys.Dominio.Acesso
{
    /// <summary>
    /// Valores que representam DirecaoLeitora.
    /// </summary>
    public enum DirecaoLeitora
    {
        /// <summary>
        /// An enum constant representing the entrada option.
        /// </summary>
        [Description("Entrada")]
        Entrada = 1,
        /// <summary>
        /// An enum constant representing the saida option.
        /// </summary>
        [Description("Saída")]
        Saida = 2
    }
}
