﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controlsys.Dominio.Acesso
{
    /// <summary>
    /// Valores que representam TipoSincronizacaoPessoa.
    /// </summary>
    public enum TipoSincronizacaoPessoa
    {
        /// <summary>
        /// An enum constant representing the conceder option.
        /// </summary>
        Conceder,
        /// <summary>
        /// An enum constant representing the suspender option.
        /// </summary>
        Suspender
    }
}
