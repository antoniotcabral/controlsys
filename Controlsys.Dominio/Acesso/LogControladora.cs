﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Seguranca;

namespace Controlsys.Dominio.Acesso
{
    public class LogControladora
    {
        public virtual int Codigo { get; set; }

        public virtual Controladora Controladora { get; set; }

        public virtual StatusControladora Status { get; set; }

        public virtual string Observacoes { get; set; }

        public virtual DateTime DataHora { get; set; }

        public virtual Usuario Usuario { get; set; }
    }
}
