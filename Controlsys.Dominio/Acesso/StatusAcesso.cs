﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Controlsys.Dominio.Acesso
{
    /// <summary>
    /// Valores que representam StatusAcesso.
    /// </summary>
    public enum StatusAcesso
    {
        /// <summary>
        /// An enum constant representing the oco ur leitura cartao option.
        /// </summary>
        [Description("Leitura Cartão")]
        OCO_UR_LEITURA_CARTAO = 0,
        /// <summary>
        /// An enum constant representing the oco ur acesso liberado option.
        /// </summary>
        [Description("Acesso Liberado")]
        OCO_UR_ACESSO_LIBERADO = 1,
        /// <summary>
        /// An enum constant representing the oco ur nao cadastrado option.
        /// </summary>
        [Description("Não Cadastrado")]
        OCO_UR_NAO_CADASTRADO = 2,
        /// <summary>
        /// An enum constant representing the oco ur excluido option.
        /// </summary>
        [Description("Excluído")]
        OCO_UR_EXCLUIDO = 3,
        /// <summary>
        /// An enum constant representing the oco ur aguardando input 2 option.
        /// </summary>
        [Description("Aguardando Input")]
        OCO_UR_AGUARDANDO_INPUT2 = 4,
        /// <summary>
        /// An enum constant representing the oco ur horario invalido option.
        /// </summary>
        [Description("Horário Inválido")]
        OCO_UR_HORARIO_INVALIDO = 5,
        /// <summary>
        /// An enum constant representing the oco ur violacao de fluxo option.
        /// </summary>
        [Description("Violação de Fluxo")]
        OCO_UR_VIOLACAO_DE_FLUXO = 6,
        /// <summary>
        /// An enum constant representing the oco ur leitora invalida option.
        /// </summary>
        [Description("Leitora Inválida")]
        OCO_UR_LEITORA_INVALIDA = 7,
        /// <summary>
        /// An enum constant representing the oco ur negado input 2 option.
        /// </summary>
        [Description("Negado Input")]
        OCO_UR_NEGADO_INPUT2 = 8,
        /// <summary>
        /// An enum constant representing the oco ur negado lista negra option.
        /// </summary>
        [Description("Lista Negra")]
        OCO_UR_NEGADO_LISTA_NEGRA = 9,
        /// <summary>
        /// An enum constant representing the oco ur entrada completa option.
        /// </summary>
        [Description("Entrada Completa")]
        OCO_UR_ENTRADA_COMPLETA = 10,
        /// <summary>
        /// An enum constant representing the oco ur saida completa option.
        /// </summary>
        [Description("Saída Completa")]
        OCO_UR_SAIDA_COMPLETA = 11,
        /// <summary>
        /// An enum constant representing the oco ur acesso nao concluido option.
        /// </summary>
        [Description("Não Concluído")]
        OCO_UR_ACESSO_NAO_CONCLUIDO = 12,
        /// <summary>
        /// An enum constant representing the oco ur timeout input 2 option.
        /// </summary>
        [Description("Timeout Input")]
        OCO_UR_TIMEOUT_INPUT2 = 13,
        /// <summary>
        /// An enum constant representing the oco ur aci invalido option.
        /// </summary>
        [Description("Acionamento inválido")]
        OCO_UR_ACI_INVALIDO = 14,
        /// <summary>
        /// An enum constant representing the oco ur modifier library invalido option.
        /// </summary>
        [Description("Modo de liberação inválido")]
        OCO_UR_MOD_LIB_INVALIDO = 15,
        /// <summary>
        /// An enum constant representing the oco ur ponto registrado option.
        /// </summary>
        [Description("Ponto Registrado")]
        OCO_UR_PONTO_REGISTRADO = 20,
        /// <summary>
        /// An enum constant representing the oco ur timeout comunicacao hk option.
        /// </summary>
        [Description("Timeout Comunicaçao HK")]
        OCO_UR_TIMEOUT_COMUNICACAO_HK = 21,
        /// <summary>
        /// An enum constant representing the oco ur timeout leitura bio option.
        /// </summary>
        [Description("Leitura Bio")]
        OCO_UR_TIMEOUT_LEITURA_BIO = 22,
        /// <summary>
        /// An enum constant representing the oco ur mao invalida option.
        /// </summary>
        [Description("Mão Inválida")]
        OCO_UR_MAO_INVALIDA = 23,
        /// <summary>
        /// An enum constant representing the oco ur digital invalida option.
        /// </summary>
        [Description("Digital Inválida")]
        OCO_UR_DIGITAL_INVALIDA = 24,


        /// <summary>
        /// An enum constant representing the oco ur digital invalida option.
        /// </summary>
        [Description("Condutor autorizado, Veículo não autorizado")]
        OCO_UR_CONDUTOR_AUTORIZADO_VEICULO_NAO_AUTORIZADO = 25,


        /// <summary>
        /// An enum constant representing the oco ur digital invalida option.
        /// </summary>
        [Description("Condutor não autorizado, Veículo não autorizado")]
        OCO_UR__CONDUTOR_NAO_AUTORIZADO_VEICULO_AUTORIZADO = 26,

        /// <summary>
        /// An enum constant representing the oco ur digital invalida option.
        /// </summary>
        [Description("Condutor não autorizado, Veículo não autorizado")]
        OCO_UR__CONDUTOR_NAO_AUTORIZADO_VEICULO_NAO_AUTORIZADO = 27,

        /// <summary>
        /// An enum constant representing the oco ur alarme ativado option.
        /// </summary>
        [Description("Alarme Ativado")]
        OCO_UR_ALARME_ATIVADO = 30,
        /// <summary>
        /// An enum constant representing the oco ur alarme desativado option.
        /// </summary>
        [Description("Alarme Desativado ")]
        OCO_UR_ALARME_DESATIVADO = 31,
        /// <summary>
        /// An enum constant representing the oco ur alarme porta aberta option.
        /// </summary>
        [Description("Porta Aberta")]
        OCO_UR_ALARME_PORTA_ABERTA = 32,
        /// <summary>
        /// An enum constant representing the oco ur alarme intrusao option.
        /// </summary>
        [Description("Alarme Intrusão")]
        OCO_UR_ALARME_INTRUSAO = 33,
        /// <summary>
        /// An enum constant representing the oco ur reset 1 option.
        /// </summary>
        [Description("Reset 1")]
        OCO_UR_RESET1 = 40,
        /// <summary>
        /// An enum constant representing the oco ur reset 2 option.
        /// </summary>
        [Description("Reset 2")]
        OCO_UR_RESET2 = 41,
        /// <summary>
        /// An enum constant representing the oco ur reset 3 option.
        /// </summary>
        [Description("Reset 3")]
        OCO_UR_RESET3 = 42,
        /// <summary>
        /// An enum constant representing the oco ur reset 4 option.
        /// </summary>
        [Description("Reset 4")]
        OCO_UR_RESET4 = 43,
        /// <summary>
        /// An enum constant representing the oco ur reset 5 option.
        /// </summary>
        [Description("Reset 5")]
        OCO_UR_RESET5 = 44,
        /// <summary>
        /// An enum constant representing the oco ur reset 6 option.
        /// </summary>
        [Description("Reset 6")]
        OCO_UR_RESET6 = 45,
        /// <summary>
        /// An enum constant representing the oco ur reset 7 option.
        /// </summary>
        [Description("Reset 7")]
        OCO_UR_RESET7 = 46,
        /// <summary>
        /// An enum constant representing the oco ur reset 7 option.
        /// </summary>
        [Description("Braço cai")]
        OCO_UR_BRACO_CAI = 47,
        /// <summary>
        /// An enum constant representing the oco ur reset 7 option.
        /// </summary>
        [Description("Reset 8")]
        OCO_UR_RESET8 = 48,
        /// <summary>
        /// An enum constant representing the oco ur reset 7 option.
        /// </summary>
        [Description("Reset 9")]
        OCO_UR_RESET9 = 49,
        /// <summary>
        /// An enum constant representing the oco ur maximum tensao option.
        /// </summary>
        [Description("Max. Tensão")]
        OCO_UR_MAX_TENSAO = 50,
        /// <summary>
        /// An enum constant representing the oco ur maximum corrente option.
        /// </summary>
        [Description("Max. Corrente")]
        OCO_UR_MAX_CORRENTE = 51,
        /// <summary>
        /// An enum constant representing the oco ur maximum temperatura option.
        /// </summary>
        [Description("Max. Temperatura")]
        OCO_UR_MAX_TEMPERATURA = 52,
        /// <summary>
        /// An enum constant representing the oco ur nrm tensao option.
        /// </summary>
        [Description("Número Tensão")]
        OCO_UR_NRM_TENSAO = 53,
        /// <summary>
        /// An enum constant representing the oco ur nrm corrente option.
        /// </summary>
        [Description("Número Corrente")]
        OCO_UR_NRM_CORRENTE = 54,
        /// <summary>
        /// An enum constant representing the oco ur nrm temperatura option.
        /// </summary>
        [Description("Número Temperatura")]
        OCO_UR_NRM_TEMPERATURA = 55,
        /// <summary>
        /// An enum constant representing the oco ur minimum tensao option.
        /// </summary>
        [Description("Min. Tensão")]
        OCO_UR_MIN_TENSAO = 56,
        /// <summary>
        /// An enum constant representing the oco ur minimum temperatura option.
        /// </summary>
        [Description("Min. Temperatura")]
        OCO_UR_MIN_TEMPERATURA = 57,
        /// <summary>
        /// An enum constant representing the oco ur online option.
        /// </summary>
        [Description("Online")]
        OCO_UR_ONLINE = 300,
        /// <summary>
        /// An enum constant representing the oco ur offline option.
        /// </summary>
        [Description("Offline")]
        OCO_UR_OFFLINE = 301,
        /// <summary>
        /// An enum constant representing the oco ur demitido option.
        /// </summary>
        [Description("Demitido")]
        OCO_UR_DEMITIDO = 81,
        /// <summary>
        /// An enum constant representing the oco ur inativo option.
        /// </summary>
        [Description("Inativo")]
        OCO_UR_INATIVO = 82,
        /// <summary>
        /// An enum constant representing the oco ur visto expirado option.
        /// </summary>
        [Description("Visto Expirado")]
        OCO_UR_VISTO_EXPIRADO = 83,
        /// <summary>
        /// An enum constant representing the oco ur visitante expirado option.
        /// </summary>
        [Description("Visitante Expirado")]
        OCO_UR_VISITANTE_EXPIRADO = 84,
        /// <summary>
        /// An enum constant representing the oco ur prestador expirado option.
        /// </summary>
        [Description("Prestador Expirado")]
        OCO_UR_PRESTADOR_EXPIRADO = 85,
        /// <summary>
        /// An enum constant representing the oco ped empresa vencido option.
        /// </summary>
        [Description("PED Empresa Vencido/Inativo")]
        OCO_PED_EMPRESA_VENCIDO = 86,
        /// <summary>
        /// An enum constant representing the oco ur cnh vencida option.
        /// </summary>
        [Description("CNH Vencida")]
        OCO_UR_CNH_VENCIDA = 87,
        /// <summary>
        /// An enum constant representing the oco ur treinamento option.
        /// </summary>
        [Description("Treinamento")]
        OCO_UR_TREINAMENTO = 88,
        /// <summary>
        /// An enum constant representing the oco ur senha invalida option.
        /// </summary>
        [Description("Senha inválida")]
        OCO_UR_SENHA_INVALIDA = 89,
        /// <summary>
        /// An enum constant representing the oco ur antipassback option.
        /// </summary>
        [Description("Antipassback")]
        OCO_UR_ANTIPASSBACK = 90,
        /// <summary>
        /// An enum constant representing the oco ur supervisionado option.
        /// </summary>
        [Description("Supervisionado")]
        OCO_UR_SUPERVISIONADO = 91,
        /// <summary>
        /// An enum constant representing the oco ur baixa cracha option.
        /// </summary>
        [Description("Baixa definitiva emitida automaticamente")]
        OCO_UR_BAIXA_CRACHA = 92,
        /// <summary>
        /// An enum constant representing the oco ur aso vencida option.
        /// </summary>
        [Description("ASO Vencido")]
        OCO_UR_ASO_VENCIDA = 93,
        /// <summary>
        /// An enum constant representing the oco ur afastado option.
        /// </summary>
        //ToDo: Tratar acesso com os status abaixo:
        [Description("Afastado")]
        OCO_UR_AFASTADO = 94,
        /// <summary>
        /// An enum constant representing the oco ur ferias option.
        /// </summary>
        [Description("Férias")]
        OCO_UR_FERIAS = 95,
        /// <summary>
        /// An enum constant representing the oco ur licenca medica option.
        /// </summary>
        [Description("Licença médica")]
        OCO_UR_LICENCA_MEDICA = 96,

        /// <summary>
        /// Ocorrência que nega o acesso ao restaurante.
        /// </summary>
        [Description("Restaurante não autorizado")]
        OCO_UR_RESTAURANTE_N_AUTORIZADO = 97,

        /// <summary>
        /// Ocorrência para o caso do acesso ao restaurante já ter sido utilizado
        /// </summary>
        [Description("Restaurante não autorizado. Cota ultrapassada.")]
        OCO_UR_RESTAURANTE_COTA_DIARIA = 98,

        /// <summary>
        /// Ocorrência para caso o acesso a leitora esteja interditado.
        /// </summary>
        [Description("Controladora interditada")]
        OCO_UR_CONTROLADORA_INTERDITADA = 99,


        /// <summary>
        /// Ocorrência para caso o acesso de não ter validado acesso hierárquico
        /// (Não ter acessado primeiro uma leitora externa)
        /// </summary>
        [Description("Acesso não autorizado. Acesso hierárquico.")]
        OCO_UR_RESTAURANTE_ACESSO_HIERARQUICO = 80,


        /// <summary>
        /// Ocorrência que nega o acesso ao restaurante,
        /// Ou está fora do horário
        /// </summary>
        [Description("Restaurante não autorizado ou horário inválido.")]
        OCO_UR_RESTAURANTE_ACESSO_REST_HORARIO_INVALIDO = 70,

        /// <summary>
        /// Ocorrência que india a necessidade de controle de revista
        /// </summary>
        [Description("Controle de Revista")]
        OCO_UR_CONTROLE_REVISTA = 58,

        /// <summary>
        /// An enum constant representing the oco bloqueio em empresa para colaborador sem pedido alocado.
        /// </summary>
        [Description("Bloqueio Empresa para Colaborador sem Pedido Alocado.")]
        OCO_BLOQ_COLAB_SEM_PEDIDO = 61,

        /// <summary>
        /// Ocorrência que indica a autorização do veículo caso a precisão do LPR seja maior do que a nos parâmetros do sistema
        /// </summary>
        [Description("Veículo autorizado, condutor autorizado, precisão acima do LPR")]
        OCO_UR_CONTROLE_ACESSO_AUTORIZADO_LPR_ATIVO_ACIMA = 100,

        /// <summary>
        ///  Ocorrência que indica a não autorização do veículo caso a precisão do LPR seja maior do que a nos parâmetros do sistema
        /// </summary>
        [Description("Veículo autorizado, condutor não autorizado, precisão acima do LPR")]
        OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_LPR_ATIVO_ACIMA = 101,

        /// <summary>
        ///  Ocorrência que indica a não autoriação do veículo caso a precisão do LPR seja menor do que a nos parâmetros do sistema
        /// </summary>
        [Description("Veículo não autorizado, precisão abaixo do LPR, veículo não identificado")]
        OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_LPR_ATIVO_ABAIXO = 102,

        /// <summary>
        ///  Ocorrência que indica a não autoriação do veículo caso a precisão do LPR seja menor do que a nos parâmetros do sistema
        /// </summary>
        [Description("Veículo autorizado,LPR inativo, veículo não identificado")]
        OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_LPR_INATIVO = 103,

        /// <summary>
        ///  Ocorrência que indica a não autoriação do veículo no GrupoLeitora caso a LPR esteja ativo na controladora
        /// </summary>
        [Description("Veículo não autorizado, LPR Ativo")]
        OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_GRUPO_LEITORA = 104,

        /// <summary>
        /// Ocorrência que indica a  autoriação do veículo no GrupoLeitora caso o LPR esteja Inativo na controladora e a câmera esteja ativa
        /// </summary>
        [Description("Veículo autorizado, LPR Ativo, Câmera Inativa")]
        OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_INATIVA = 105,

       

        /// <summary>
        /// Ocorrência que indica a  autoriação do veículo no GrupoLeitora caso o LPR esteja Inativo na controladora e a câmera esteja ativa
        /// </summary>
        [Description("Veículo não autorizado, LPR Inativo, Câmera Inativa")]
        OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_CAMERA_INATIVA = 106,

        /// <summary>
        /// Ocorrência que indica a  autoriação do veículo no GrupoLeitora caso o LPR esteja Inativo na controladora e a câmera esteja ativa
        /// </summary>
        [Description("Veículo autorizado, LPR Inativo, Câmera Ativa")]
        OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_ATIVA_LPR_INATIVO = 107,


        ///// <summary>
        ///// Ocorrência que indica a  autoriação do veículo no GrupoLeitora caso o LPR esteja Inativo na controladora e a câmera esteja ativa
        ///// </summary>
        //[Description("Veículo autorizado, LPR Inativo, Câmera Ativa")]
        //OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_ATIVA_LPR_INATIVO = 107,


        /// <summary>
        ///  Ocorrência que indica a não autorização do veículo por conta da câmera estar offline
        /// </summary>
        [Description("Veículo autorizado, LPR Inativo, Câmera/Câmeras Offline")]
        OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_OFFLINE = 108,


        /// <summary>
        ///  Ocorrência que indica a não autorização do veículo por conta da câmera estar offline
        /// </summary>
        [Description("Veículo autorizado, LPR Inativo, Câmera/Câmeras Offline")]
        OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_CAMERA_OFFLINE = 109,
        /// <summary>
        ///  Ocorrência que indica a não autorização do veículo por conta da câmera estar offline
        /// </summary>
        [Description("Veículo não autorizado, Câmera/Câmeras não vinculadas")]
        OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERAS_NAO_VINCULADAS = 110,


        /// <summary>
        ///  Ocorrência que indica a não autorização do veículo por conta da câmera estar offline
        /// </summary>
        [Description("Veículo não autorizado, Câmera/Câmeras não vinculadas")]
        OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_CAMERAS_NAO_VINCULADAS = 111,
        /// <summary>
        /// Ocorrência que indica a não autorização do veículo por conta do sistema da Avigilin estar offline
        /// </summary>
        [Description("Veículo não autorizado, Sistema Avigilon Inativo")]
        OCO_UR_CONTROLE_ACESSO_VEICULO_ACC_INATIVA_NEGADO = 112,

        /// <summary>
        /// Ocorrência que indica a não autorização do veículo por conta do sua permissão de acesso temporário ao local ter expirado
        /// </summary>
        [Description("Veículo não autorizado, Permissão de acesso temporário expirado")]
        OCO_UR_CONTROLE_ACESSO_VEICULO_DATA_ACESSO_TEMPORARIO_EXPIRADO = 113,

        /// <summary>
        /// Ocorrência que indica a não autorização do veículo por conta do sua permissão de acesso temporário ao local ter expirado
        /// </summary>
        [Description("Veículo não autorizado, Controladora não possui câmeras vinculadas")]
        OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_CONTROLADORA_SEM_CAMERAS = 114,

        /// <summary>
        /// Ocorrência que indica a não autorização do veículo por conta do sua permissão de acesso temporário ao local ter expirado
        /// </summary>
        [Description("Papel não está autorizado e consequentemente seu Veículo")]
        OCO_UR_CONTROLE_ACESSO_PAPEL_NAO_AUTORIZADO_CANCELA = 115,

        /// <summary>
        /// Ocorrência que indica a não autorização do veículo por conta do sua permissão de acesso temporário ao local ter expirado
        /// </summary>
        [Description("Papel não está autorizado e consequentemente seu Veículo")]
        OCO_UR_CONTROLE_ACESSO_PAPEL_AUTORIZADO_VEICULO_NAO_AUTORIZADO = 116,


        /// <summary>
        /// Ocorrência que indica a não autorização do veículo por conta do sua permissão de acesso temporário ao local ter expirado
        /// </summary>
        [Description("Papel não está autorizado e consequentemente seu Veículo")]
        OCO_UR_CONTROLE_ACESSO_PAPEL_NAO_AUTORIZADO_VEICULO_NAO_AUTORIZADO = 117,

        /// <summary>
        /// Ocorrência que indica a não autorização do veículo por conta do sua permissão de acesso temporário ao local ter expirado
        /// </summary>
        [Description("Credencial do Veículo inexistente")]
        OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_CREDENCIAL_INEXISTENTE = 118,

        /// <summary>
        /// Ocorrência que indica a não autorização do veículo por conta do sua permissão de acesso temporário ao local ter expirado
        /// </summary>
        [Description("Veículo não identificado no banco de dados")]
        OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_ENCONTRADO_BASE_DADOS = 119,

        /// <summary>
        /// Ocorrência que indica a não autorização do veículo por conta do sua permissão de acesso temporário ao local ter expirado
        /// </summary>
        [Description("Credencial não impressa")]
        OCO_UR_CONTROLE_ACESSO_VEICULO_CREDENCIAL_NAO_IMPRESSA = 120,

        /// <summary>
        /// Ocorrência que indica a não autorização do veículo por conta do sua permissão de acesso temporário ao local ter expirado
        /// </summary>
        [Description("Veículo não autorizado, LPR Inativo, Câmera Ativa")]
        OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_CAMERA_ATIVA_LPR_INATIVO = 121,

        /// <summary>
        /// Ocorrência que indica a não autorização do veículo por conta do sua permissão de acesso temporário ao local ter expirado
        /// </summary>
        [Description("Veículo autorizado, LPR Inativo na câmera por parâmetro do sistema ")]
        OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_ATIVA_LPR_INATIVO_PARAMETRO = 122,

        /// <summary>
        /// Ocorrência que indica a houve erro na aplicação
        /// </summary>
        [Description("Erro na aplicação de validação de veículos")]
        OCO_UR_CONTROLE_ACESSO_VEICULO_ERRO = 123,

        /// <summary>
        /// Ocorrência que indica que a validação foi desativada no sistema por um parâmetro geral (Inteligência para desativar e ativar a validação de veículos)
        /// </summary>
        [Description("Validação de acesso do veículo desativada")]
        OCO_UR_CONTROLE_ACESSO_VEICULO_VALIDACAO_DESATIVADA = 124,

        /// <summary>
        /// Ocorrência que indica que a validação foi desativada no sistema por um parâmetro geral (Inteligência para desativar e ativar a validação de veículos)
        /// </summary>
        [Description("Data de credencial fora do intervalo")]
        OCO_UR_CONTROLE_ACESSO_VEICULO_CREDENCIAL_FORA_INTERVALO = 125,

        /// <summary>
        /// Ocorrência que indica a  autoriação do veículo no GrupoLeitora caso o LPR esteja Inativo na controladora e a câmera esteja ativa
        /// </summary>
        [Description("Veículo não autorizado,Tempo de resposta Excedido")]
        OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_TEMPO_RESPOSTA_EXCEDIDO = 126

    }
}
