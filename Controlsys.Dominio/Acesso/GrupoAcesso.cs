﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Acesso
{
    /// <summary>
    /// Representa um(a) GrupoAcesso.
    /// </summary>
    public class GrupoAcesso
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public virtual int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Nome.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Nome.
        /// </value>
        public virtual string Nome { get; set; }

        /// <summary>
        /// Gets/Sets valor para Pessoas.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Pessoas.
        /// </value>
        public virtual IList<GrupoAcessoPapel> Pessoas { get; set; }

        /// <summary>
        /// Gets/Sets valor para GruposLeitora.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) GruposLeitora.
        /// </value>
        public virtual IList<GrupoLeitoraAcesso> GruposLeitora { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataRegistro.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataRegistro.
        /// </value>
        public virtual DateTime DataRegistro { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataDesativacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataDesativacao.
        /// </value>
        public virtual DateTime? DataDesativacao { get; set; }

        /// <summary>
        /// Gets/Sets a situação do objetivo do tipo GrupoAcesso.
        /// </summary>
        ///
        /// <value>
        /// <see cref="DataDesativacao"/> nula retorna true, <see cref="DataDesativacao"/> com valor
        /// retorna false.
        /// </value>
        public virtual bool Ativo { get; protected set; }

        /// <summary>
        /// Ativa o objeto, definindo <see cref="Ativo"/> como true e <see cref="DataDesativacao"/> como
        /// nulo.
        /// </summary>
        public virtual void Ativar()
        {
            Ativo = true;
            DataDesativacao = null;
        }

        /// <summary>
        /// Inativa o objeto, definindo <see cref="Ativo"/> como falso e <see cref="DataDesativacao"/>
        /// como a data atual.
        /// </summary>
        public virtual void Inativar()
        {
            Ativo = false;
            DataDesativacao = DateTime.Now;
        }

        /// <summary>
        /// Construtor para Controlsys.Dominio.Acesso.GrupoAcesso.
        /// </summary>
        public GrupoAcesso()
        {
            Pessoas = new List<GrupoAcessoPapel>();
            GruposLeitora = new List<GrupoLeitoraAcesso>();
        }
    }
}
