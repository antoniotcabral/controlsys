﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.CredenciamentoVeiculos;
using Globalsys;
using System.Drawing;
using System.Web;
using System.IO;
using System.Diagnostics;
using System.Net;
using System.Threading;


namespace Controlsys.Dominio.Acesso
{
    /// <summary>
    /// Representa um(a) Acesso de veículo.
    /// </summary>
    public class AcessoVeiculo
    {
        /// <summary>
        /// ID chave primária do Acesso do Veículo
        /// </summary>
        public virtual int Codigo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public AcessoVeiculo()
        {
            CamerasImagens = new List<CameraImagem>();

        }
        ///// <summary>
        ///// Representa a tentativa de acesso do Papel
        ///// </summary>
        //public virtual ControleAcesso? AcessoPapel { get; set; }

        /// <summary>
        /// Representa a tentativa de acesso do Papel
        /// </summary>
        public virtual Controlsys.Dominio.Pessoas.Cracha Cracha { get; set; }

        /// <summary>
        /// Representa a tentativa de acesso do Papel
        /// </summary>
        public virtual Controlsys.Dominio.Pessoas.Papel Papel { get; set; }
        /// <summary>
        /// Representa o veículo que efetuou a tentativa de acesso
        /// </summary>
        public virtual VeiculoCredencialVeiculo VeiculoCredencialVeiculo { get; set; }

        /// <summary>
        /// Representa a controladora  que foi acessada pelo papel
        /// </summary>
        public virtual Controladora Controladora { get; set; }

        /// <summary>
        /// Leitora onde o papel passou o crachá
        /// </summary>
        public virtual Leitora Leitora { get; set; }


        /// <summary>
        /// Lista de cameras com o caminho das imagens no servidor, placa e precisão do LPR da imagem
        /// </summary>
        public virtual IList<CameraImagem> CamerasImagens { get; set; }

        /// Data da tentativa de acesso
        /// </summary>
        public virtual DateTime DataRegistro { get; set; }


        /// <summary>
        ///  Representa o status do acesso do veículo Autorizado ou não
        /// </summary>
        public virtual bool StatusAutorizacao { get; set; }

        /// <summary>
        /// Gets/Sets valor para StatusAcesso.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) StatusAcesso.
        /// </value>
        public virtual StatusAcesso StatusAcesso { get; set; }

        /// <summary>
        /// Motivo do bloqueio ou liberação
        /// </summary>
        public virtual string Observacoes { get; set; }
        //Acesso - Acesso


        /// <summary>
        /// Motivo do bloqueio ou liberação
        /// </summary>
        public virtual ControleAcesso AcessoPapel { get; set; }
        //Acesso - Acesso


    }
}
