﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controlsys.Dominio.Acesso
{
    /// <summary>
    /// Representa um(a) MotivoBloqueio.
    /// </summary>
    public class MotivoBloqueio
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public virtual int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Nome.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Nome.
        /// </value>
        public virtual string Nome { get; set; }

        /// <summary>
        /// Gets/Sets valor para Descricao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Descricao.
        /// </value>
        public virtual string Descricao { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataCadastro.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataCadastro.
        /// </value>
        public virtual DateTime DataCadastro { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataDesativacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataDesativacao.
        /// </value>
        public virtual DateTime? DataDesativacao { get; set; }

        /// <summary>
        /// Gets/Sets a situação do objetivo do tipo MotivoBloqueio.
        /// </summary>
        ///
        /// <value>
        /// <see cref="DataDesativacao"/> nula retorna true, <see cref="DataDesativacao"/> com valor
        /// retorna false.
        /// </value>
        public virtual bool Ativo { get; /*protected*/ set; }

        /// <summary>
        /// Ativa o objeto, definindo <see cref="Ativo"/> como true e <see cref="DataDesativacao"/> como
        /// nulo.
        /// </summary>
        public virtual void Ativar()
        {
            Ativo = true;
            DataDesativacao = null;
        }

        /// <summary>
        /// Inativa o objeto, definindo <see cref="Ativo"/> como falso e <see cref="DataDesativacao"/>
        /// como a data atual.
        /// </summary>
        public virtual void Inativar()
        {
            Ativo = false;
            DataDesativacao = DateTime.Now;
        }
    }
}
