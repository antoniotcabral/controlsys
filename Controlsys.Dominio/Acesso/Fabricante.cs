﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Controlsys.Dominio.Acesso
{
    /// <summary>
    /// Valores que representam Fabricante.
    /// </summary>
    public enum Fabricante
    {
        /// <summary>
        /// An enum constant representing the wiegand option.
        /// </summary>
        Wiegand = 1,

        /// <summary>
        /// An enum constant representing the clock data magstripe abatrak option.
        /// </summary>
        [Description("CLOCK&DATA | MAGSTRIPE | ABATRAK")]
        ClockDataMagstripeAbatrak = 2,

        /// <summary>
        /// An enum constant representing the mifare acura option.
        /// </summary>
        [Description("Mifare-Acura")]
        MifareAcura = 3,

        /// <summary>
        /// An enum constant representing the mifare ineltec option.
        /// </summary>
        [Description("Mifare-Ineltec")]
        MifareIneltec = 4,

        /// <summary>
        /// An enum constant representing the codigo barras compex option.
        /// </summary>
        [Description("Código de barras Compex")]
        CodigoBarrasCompex = 5,


        /// <summary>
        /// An enum constant representing the codigo barras compex option.
        /// </summary>
        [Description("Trielo Mifare")]
        TM10 = 24

    }
}
