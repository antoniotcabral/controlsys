﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Controlsys.Dominio.Acesso
{
    /// <summary>
    /// Valores que representam TipoLeitora.
    /// </summary>
    public enum TipoLeitora
    {
        /// <summary>
        /// An enum constant representing the leitora option.
        /// </summary>
        Leitora = 1,
        /// <summary>
        /// An enum constant representing the leitora teclado option.
        /// </summary>
        [Description("Leitora com teclado")]
        LeitoraTeclado = 2,
        /// <summary>
        /// An enum constant representing the catraca option.
        /// </summary>
        Catraca = 3,
        /// <summary>
        /// An enum constant representing the porta com teclado option.
        /// </summary>
        [Description("Porta com teclado")]
        PortaComTeclado = 4
    }
}
