﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Acesso
{
    /// <summary>
    /// 
    /// </summary>
    public class CameraImagem 
    {

        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public virtual int Codigo { get; set; }
        /// <summary>
        /// Representa o acesso do Veículo ao qual esta atrelada
        /// </summary>
        public virtual AcessoVeiculo AcessoVeiculo { get; set; }

        /// <summary>
        /// Representa a camera que fotografou a placa do veículo
        /// </summary>
        public virtual Camera Camera { get; set; }

        /// <summary>
        /// Imagem da Câmera 1
        /// </summary>
        public virtual string CaminhoImagem { get; set; }

        /// <summary>
        /// Placa detectada pela câmera
        /// </summary>
        public virtual string PlacaVeiculo { get; set; }

        /// <summary>
        /// Precisão da imagem fotografada pela câmera
        /// </summary>
        public virtual float PrecisaoImage { get; set; }

    }
}
