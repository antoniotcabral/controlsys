﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Pessoas;

namespace Controlsys.Dominio.Acesso
{
    /// <summary>
    /// Representa um(a) SincronizaPessoaAcesso.
    /// </summary>
    public class SincronizaPessoaAcesso
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public virtual int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Cracha.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Cracha.
        /// </value>
        public virtual Cracha Cracha { get; set; }

        /// <summary>
        /// Gets/Sets valor para Controladora.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Controladora.
        /// </value>
        public virtual Controladora Controladora { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataRegistro.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataRegistro.
        /// </value>
        public virtual DateTime DataRegistro { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataInicio.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataInicio.
        /// </value>
        public virtual DateTime? DataInicio { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataSincronizacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataSincronizacao.
        /// </value>
        public virtual DateTime? DataSincronizacao { get; set; }

        /// <summary>
        /// Gets/Sets valor para TipoSincronizacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TipoSincronizacao.
        /// </value>
        public virtual TipoSincronizacaoPessoa TipoSincronizacao { get; set; }

        /// <summary>
        /// Gets/Sets valor para Tarefa.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Tarefa.
        /// </value>
        public virtual int? Tarefa { get; set; }

        /// <summary>
        /// Gets/Sets valor para Status.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Status.
        /// </value>
        public virtual StatusSincronizacao Status { get; set; }

        /// <summary>
        /// Gets/Sets valor para Porcentagem.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Porcentagem.
        /// </value>
        public virtual int Porcentagem { get; set; }

        /// <summary>
        /// Gets/Sets valor para Observacoes.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Observacoes.
        /// </value>
        public virtual string Observacoes { get; set; }
    }
}
