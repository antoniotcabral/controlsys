﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Acesso
{
    public class Camera
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public virtual int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Controladora.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Controladora.
        /// </value>
        public virtual Controladora Controladora { get; set; }

        /// <summary>
        /// Gets/Sets valor para Nome.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Nome.
        /// </value>
        public virtual string Nome { get; set; }

        /// <summary>
        /// Gets/Sets valor para Modelo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Modelo.
        /// </value>
        public virtual string Modelo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Descricao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Descricao.
        /// </value>
        public virtual string Descricao { get; set; }

        /// <summary>
        /// Gets/Sets valor para PrecisaoIMG.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) PrecisaoIMG.
        /// </value>
        public virtual string Precisao { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataRegistro.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataRegistro.
        /// </value>
        public virtual DateTime DataRegistro { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataStatus.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataStatus.
        /// </value>
        public virtual DateTime? DataStatus { get; set; }

        /// <summary>
        /// Gets/Sets valor para Ativo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Ativo.
        /// </value>
        public virtual bool Ativo { get; set; }

        /// <summary>
        /// Ativa o objeto, definindo <see cref="Ativo"/> como true e <see cref="DataStatus"/> como
        /// nulo.
        /// </summary>
        public virtual void Ativar()
        {
            Ativo = true;
            DataStatus = null;
        }

        /// <summary>
        /// Inativa o objeto, definindo <see cref="Ativo"/> como falso e <see cref="DataStatus"/>
        /// como a data atual.
        /// </summary>
        public virtual void Inativar()
        {
            Ativo = false;
            DataStatus = DateTime.Now;
        }
    }
}
