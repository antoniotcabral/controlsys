﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controlsys.Dominio.Acesso
{
    /// <summary>
    /// Representa um(a) Leitora.
    /// </summary>
    public class Leitora
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public virtual int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para IDLeitora.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) IDLeitora.
        /// </value>
        public virtual string IDLeitora { get; set; }

        /// <summary>
        /// Gets/Sets valor para Direcao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Direcao.
        /// </value>
        public virtual DirecaoLeitora Direcao { get; set; }

        //public virtual TipoLeitora TipoLeitora { get; set; }

        /// <summary>
        /// Gets/Sets valor para Local.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Local.
        /// </value>
        public virtual string Local { get; set; }

        /// <summary>
        /// Gets/Sets valor para Descricao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Descricao.
        /// </value>
        public virtual string Descricao { get; set; }

        /// <summary>
        /// Gets/Sets valor para Fabricante.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Fabricante.
        /// </value>
        public virtual Fabricante Fabricante { get; set; }

        /// <summary>
        /// Gets/Sets valor para Controladora.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Controladora.
        /// </value>
        public virtual Controladora Controladora { get; set; }

        /// <summary>
        /// Gets/Sets valor para Senha.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Senha.
        /// </value>
        public virtual bool Senha { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataRegistro.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataRegistro.
        /// </value>
        public virtual DateTime DataRegistro { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataDesativacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataDesativacao.
        /// </value>
        public virtual DateTime? DataDesativacao { get; set; }

        /// <summary>
        /// Gets/Sets valor para ExigeSupervisao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) ExigeSupervisao.
        /// </value>
        public virtual bool ExigeSupervisao { get; set; }

        /// <summary>
        /// Gets/Sets a situação do objetivo do tipo Leitora.
        /// </summary>
        ///
        /// <value>
        /// <see cref="DataDesativacao"/> nula retorna true, <see cref="DataDesativacao"/> com valor
        /// retorna false.
        /// </value>
        public virtual bool Ativo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Ordem.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Ordem.
        /// </value>
        public virtual int Ordem { get; set; }

        /// <summary>
        /// Gets/Sets valor para EmiteBaixaCracha.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) EmiteBaixaCracha.
        /// </value>
        public virtual bool EmiteBaixaCracha { get; set; }

        /// <summary>
        /// Ativa o objeto, definindo <see cref="Ativo"/> como true e <see cref="DataDesativacao"/> como
        /// nulo.
        /// </summary>
        public virtual void Ativar()
        {
            Ativo = true;
            DataDesativacao = null;
        }

        /// <summary>
        /// Inativa o objeto, definindo <see cref="Ativo"/> como falso e <see cref="DataDesativacao"/>
        /// como a data atual.
        /// </summary>
        public virtual void Inativar()
        {
            Ativo = false;
            DataDesativacao = DateTime.Now;
        }        
    }
}
