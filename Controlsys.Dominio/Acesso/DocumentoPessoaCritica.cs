﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Parametros;

namespace Controlsys.Dominio.Acesso
{
    /// <summary>
    /// Representa um(a) DocumentoPessoaCritica.
    /// </summary>
    public class DocumentoPessoaCritica
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public virtual int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Documento.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Documento.
        /// </value>
        public virtual Documento Documento { get; set; }

        /// <summary>
        /// Gets/Sets valor para PessoaCritica.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) PessoaCritica.
        /// </value>
        public virtual PessoaCritica PessoaCritica { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataRegistro.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataRegistro.
        /// </value>
        public virtual DateTime DataRegistro { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataDesativacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataDesativacao.
        /// </value>
        public virtual DateTime? DataDesativacao { get; set; }

        /// <summary>
        /// Gets/Sets a situação do objetivo do tipo DocumentoPessoaCritica.
        /// </summary>
        ///
        /// <value>
        /// <see cref="DataDesativacao"/> nula retorna true, <see cref="DataDesativacao"/> com valor
        /// retorna false.
        /// </value>
        public virtual bool Ativo
        {
            get { return !DataDesativacao.HasValue; }
            set { DataDesativacao = value ? (DateTime?)DateTime.Now : null; }
        }
    }
}
