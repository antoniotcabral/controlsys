﻿using Controlsys.Dominio.Pessoas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Parametros;

namespace Controlsys.Dominio.Acesso
{
    public class AcessoRestauranteTipoAlimentacao
    {
        public virtual int Codigo { get; set; }

        public virtual AcessoRestaurante AcessoRestaurante { get; set; }

        public virtual TipoAlimentacaoRestaurante TipoAlimentacao { get; set; }

        public virtual DateTime DataRegistro { get; set; }

        public virtual DateTime? DataDesativacao { get; set; }

        public virtual bool Ativo { get; protected set; }

        public virtual void Ativar()
        {
            Ativo = true;
            DataDesativacao = null;
        }

        public virtual void Inativar()
        {
            Ativo = false;
            DataDesativacao = DateTime.Now;
        }
    }
}
