﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Acesso
{
    public class LogAcessoRestaurante
    {
        public virtual int Codigo { get; set; }

        public virtual AcessoRestaurante AcessoRestaurante { get; set; }

        public virtual bool CrachaMestre { get; set; }

        public virtual bool NaoValidaAcessoHierarquico { get; set; }

        public virtual DateTime DataRegistro { get; set; }
    }
}
