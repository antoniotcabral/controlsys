﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Controlsys.Dominio.Acesso
{
    /// <summary>
    /// Valores que representam StatusSincronizacao.
    /// </summary>
    public enum StatusSincronizacao
    {
        /// <summary>
        /// An enum constant representing the em andamento option.
        /// </summary>
        [Description("Em andamento")]
        EmAndamento = 1,
        /// <summary>
        /// An enum constant representing the aguardando option.
        /// </summary>
        [Description("Aguardando")]
        Aguardando = 2,
        /// <summary>
        /// An enum constant representing the concluido option.
        /// </summary>
        [Description("Concluído")]
        Concluido = 3,
        /// <summary>
        /// An enum constant representing the nao concluido option.
        /// </summary>
        [Description("Não Concluído")]
        NaoConcluido = 4
    }
}
