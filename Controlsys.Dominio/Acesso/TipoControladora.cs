﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Acesso
{
    /// <summary>
    /// Valores que representam TipoControladora.
    /// </summary>
    public enum TipoControladora
    {
        /// <summary>
        /// An enum constant representing the porta option.
        /// </summary>
        Porta = 1,
        /// <summary>
        /// An enum constant representing the catraca option.
        /// </summary>
        Catraca = 2,
        /// <summary>
        /// An enum constant representing the cancela option.
        /// </summary>
        Cancela = 3,
        /// <summary>
        /// An enum constant representing the tourniquete option.
        /// </summary>
        Tourniquete = 4,
        /// <summary>
        /// An enum constant representing the ponto option.
        /// </summary>
        Ponto = 5
    }
}
