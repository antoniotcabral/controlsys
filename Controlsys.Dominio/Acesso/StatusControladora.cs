﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Acesso
{
    public enum StatusControladora
    {
        Liberado,
        Ativo,
        Inativo,
        Interditado,
        Online,
        Offline
    }
}
