﻿using Controlsys.Dominio.Pessoas;
using System;
namespace Controlsys.Dominio.Acesso
{
    public class AcessoTemporario
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public virtual int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Papel.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Papel.
        /// </value>
        public virtual Papel Papel { get; set; }

        /// <summary>
        /// Gets/Sets valor para GrupoLeitora.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) GrupoAcesso.
        /// </value>
        public virtual GrupoLeitora GrupoLeitora { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataHoraValidadeInicio.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataRegistro.
        /// </value>
        public virtual DateTime DataHoraValidadeInicio { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataHoraValidadeFim.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataRegistro.
        /// </value>
        public virtual DateTime DataHoraValidadeFim { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataRegistro.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataRegistro.
        /// </value>
        public virtual DateTime DataRegistro { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataRegistro.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataRegistro.
        /// </value>
        public virtual DateTime? DataDesativacao { get; set; }

        /// <summary>
        /// Gets/Sets a situação do objetivo do tipo GrupoAcessoPapel.
        /// </summary>
        ///
        /// <value>
        /// 1 para ativo, 0 para inativo.
        /// </value>
        public virtual bool Ativo { get; protected set; }

        public virtual string Nome { get; set; }
        public virtual string GrupoLeitoraNome { get; set; }
        public virtual string DtHrValidadeInicio { get; set; }
        public virtual string DtHrValidadeFim { get; set; }
        public virtual bool AtivoTemp { get; set; }

        /// <summary>
        /// Ativa o objeto, definindo <see cref="Ativo"/> como true e <see cref="DataDesativacao"/> como
        /// nulo.
        /// </summary>
        public virtual void Ativar()
        {
            Ativo = true;
            DataDesativacao = null;
        }

        /// <summary>
        /// Inativa o objeto, definindo <see cref="Ativo"/> como falso e <see cref="DataDesativacao"/>
        /// como a data atual.
        /// </summary>
        public virtual void Inativar()
        {
            Ativo = false;
            DataDesativacao = DateTime.Now;
        }
    }
}
