﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Parametros;

namespace Controlsys.Dominio.SolicitacoesMaterial
{
    public class ItemSolicitacaoMaterial
    {
        public virtual int Codigo { get; set; }

        public virtual SolicitacaoMaterial SolicitacaoMaterial { get; set; }

        public virtual int Quantidade { get; set; }

        public virtual string NotaServico { get; set; }

        public virtual string NotaFiscalEntrada { get; set; }

        public virtual string NotaFiscalSaida { get; set; }

        public virtual string Descricao { get; set; }

        public virtual string NumeroPatrimonio { get; set; }

        public virtual bool? AutorizacaoVigilante { get; set; }

        public virtual bool Ativo { get; set; }

        public virtual DateTime DataRegistro { get; set; }

        public virtual DateTime? DataDesativacao { get; set; }

        /// <summary>
        /// Ativa o objeto, definindo <see cref="Ativo"/> como true e <see cref="DataDesativacao"/> como
        /// nulo.
        /// </summary>
        public virtual void Ativar()
        {
            Ativo = true;
            DataDesativacao = null;
        }

        /// <summary>
        /// Inativa o objeto, definindo <see cref="Ativo"/> como falso e <see cref="DataDesativacao"/>
        /// como a data atual.
        /// </summary>
        public virtual void Inativar()
        {
            Ativo = false;
            DataDesativacao = DateTime.Now;
        }

    }
}
