﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Seguranca;

namespace Controlsys.Dominio.SolicitacoesMaterial
{
    public class StatusSolicitacaoMaterial
    {
        public virtual int Codigo { get; set; }

        public virtual SolicitacaoMaterial SolicitacaoMaterial { get; set; }        

        public virtual SituacaoSolicitacao Situacao { get; set; }

        public virtual string Observacoes { get; set; }

        public virtual DateTime DataRegistro { get; set; }
        /// <summary>
        /// Aqui informa quem foi o usuário que inseriu cada status, solicitado pelo Sup&Mnt_Controlsys_-_Requisição_de_Mudança_-_2017-01-23_.xlsx ref. ao item 88 - em 30/01/2017.
        /// </summary>
        public virtual Usuario UsuarioStatus { get; set; }
    }
}