﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.SolicitacoesMaterial
{
    public enum SituacaoSolicitacao
    {
        [Description("Aguardando Aprovação")]
        AguardandoAprovacao,
        Aprovada,
        Reprovada,        
        Impressa,
        Registrada,
        [Description("Concluída")]
        Concluida,
        Recebida,
        [Description("Não Conforme")]
        NaoConforme
    }
}
