﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.SolicitacoesMaterial
{
    public enum Modalidade
    {
        [Description("Desmobilização")]
        Desmobilizacao,
        [Description("Devolução")]
        Devolucao,
        [Description("Manutenção")]
        Manutencao
    }
}
