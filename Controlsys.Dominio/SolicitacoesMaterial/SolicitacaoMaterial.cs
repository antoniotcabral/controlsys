﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Pessoas;
using Controlsys.Dominio.Seguranca;
using Controlsys.Dominio.Parametros;

namespace Controlsys.Dominio.SolicitacoesMaterial
{
    public class SolicitacaoMaterial
    {
        public virtual int Codigo { get; set; }

        public virtual Usuario Solicitante { get; set; }

        public virtual TipoSolicitacao TipoSolicitacao { get; set; }

        public virtual Modalidade? Modalidade { get; set; }

        public virtual DateTime? DataPrevisaoRetorno { get; set; }

        public virtual SetorCusto SetorCusto { get; set; }

        public virtual DateTime DataEntradaSaida { get; set; }

        public virtual IList<StatusSolicitacaoMaterial> Status { get; set; }

        public virtual Papel RecebidoPor { get; set; }

        public virtual Empresa Contratada { get; set; }

        public virtual Empresa SubContratada { get; set; }

        public virtual PedidoCompra PedidoCompra { get; set; }

        public virtual ColaboradorPedido GestorResponsavel { get; set; }

        public virtual Colaborador ColaboradorResponsavel { get; set; }

        public virtual string Motivo { get; set; }

        public virtual string PlacaVeiculo { get; set; }

        public virtual string Portador { get; set; }

        public virtual string Destino { get; set; }

        public virtual IList<ItemSolicitacaoMaterial> Itens { get; set; }

        public virtual IList<SolicitacaoMaterialMidia> Midias { get; set; }

        public virtual bool Ativo { get; set; }

        public virtual DateTime DataRegistro { get; set; }

        public virtual DateTime? DataDesativacao { get; set; }

        public virtual DateTime? DataAprovacaoReprovacao
        {
            get
            {
                StatusSolicitacaoMaterial status = this.Status.Where(st => st.Situacao == SituacaoSolicitacao.Aprovada || st.Situacao == SituacaoSolicitacao.Reprovada).FirstOrDefault();
                return status != null ? status.DataRegistro : new DateTime?();
            }
            set { }
        }

        public virtual DateTime? DataNaoConforme
        {
            get
            {
                StatusSolicitacaoMaterial status = this.Status.Where(st => st.Situacao == SituacaoSolicitacao.NaoConforme).FirstOrDefault();
                return status != null ? status.DataRegistro : new DateTime?();
            }
            set { }
        }

        public virtual DateTime? DataRegistrado
        {
            get
            {
                StatusSolicitacaoMaterial status = this.Status.Where(st => st.Situacao == SituacaoSolicitacao.Registrada).FirstOrDefault();
                return status != null ? status.DataRegistro : new DateTime?();
            }
            set { }
        }

        public virtual DateTime? DataAguardandoAprovacao
        {
            get
            {
                StatusSolicitacaoMaterial status = this.Status.Where(st => st.Situacao == SituacaoSolicitacao.AguardandoAprovacao).FirstOrDefault();
                return status != null ? status.DataRegistro : new DateTime?();
            }
            set { }
        }

        /// <summary>
        /// Ativa o objeto, definindo <see cref="Ativo"/> como true e <see cref="DataDesativacao"/> como
        /// nulo.
        /// </summary>
        public virtual void Ativar()
        {
            Ativo = true;
            DataDesativacao = null;
        }

        /// <summary>
        /// Inativa o objeto, definindo <see cref="Ativo"/> como falso e <see cref="DataDesativacao"/>
        /// como a data atual.
        /// </summary>
        public virtual void Inativar()
        {
            Ativo = false;
            DataDesativacao = DateTime.Now;
        }

        public virtual StatusSolicitacaoMaterial ObterStatusAtual()
        {
            return Status.OrderByDescending(s => s.Codigo).FirstOrDefault();
        }
    }
}
