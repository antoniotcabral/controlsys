﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Controlsys.Dominio.Empresas
{
    /// <summary>
    /// Representa um(a) Fornecedor.
    /// </summary>
    public class Fornecedor
    {
        /// <summary>
        /// Construtor para Controlsys.Dominio.Empresas.Fornecedor.
        /// </summary>
        public Fornecedor()
        {
            Empresas = new List<FornecedorEmpresa>();
        }

        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public virtual int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para CNPJ.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CNPJ.
        /// </value>
        public virtual string CNPJ { get; set; }

        /// <summary>
        /// Gets/Sets valor para RazaoSocial.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) RazaoSocial.
        /// </value>
        public virtual string RazaoSocial { get; set; }

        /// <summary>
        /// Gets/Sets valor para NomeFantasia.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) NomeFantasia.
        /// </value>
        public virtual string NomeFantasia { get; set; }

        /// <summary>
        /// Gets/Sets valor para NomeContato.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) NomeContato.
        /// </value>
        public virtual string NomeContato { get; set; }

        /// <summary>
        /// Gets/Sets valor para TelefoneContato.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) TelefoneContato.
        /// </value>
        public virtual string TelefoneContato { get; set; }

        /// <summary>
        /// Gets/Sets valor para EmailContato.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) EmailContato.
        /// </value>
        public virtual string EmailContato { get; set; }

        /// <summary>
        /// Gets/Sets valor para Empresas.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Empresas.
        /// </value>
        public virtual IList<FornecedorEmpresa> Empresas { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataDesativacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataDesativacao.
        /// </value>
        public virtual DateTime? DataDesativacao { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataRegistro.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataRegistro.
        /// </value>
        public virtual DateTime DataRegistro { get; set; }

        /// <summary>
        /// Gets/Sets a situação do objetivo do tipo Fornecedor.
        /// </summary>
        ///
        /// <value>
        /// <see cref="DataDesativacao"/> nula retorna true, <see cref="DataDesativacao"/> com valor
        /// retorna false.
        /// </value>
        public virtual bool Ativo { get; /*protected*/ set; }

        /// <summary>
        /// Ativa o objeto, definindo <see cref="Ativo"/> como true e <see cref="DataDesativacao"/> como
        /// nulo.
        /// </summary>
        public virtual void Ativar()
        {
            Ativo = true;
            DataDesativacao = null;
        }

        /// <summary>
        /// Inativa o objeto, definindo <see cref="Ativo"/> como falso e <see cref="DataDesativacao"/>
        /// como a data atual.
        /// </summary>
        public virtual void Inativar()
        {
            Ativo = false;
            DataDesativacao = DateTime.Now;
        }

        /// <summary>
        /// Cnpj formatado.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) string.
        /// </returns>
        public virtual string CNPJFormatado()
        {
            return string.Format(@"{0:00\.000\.000\/0000\-00}", Convert.ToInt64(CNPJ));
        }
    }
}
