﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Controlsys.Dominio.Empresas
{
    /// <summary>
    /// Valores que representam TipoSetorCusto.
    /// </summary>
    public enum TipoSetorCusto
    {
        /// <summary>
        /// An enum constant representing the pep option.
        /// </summary>
        [Description("PEP")]
        PEP = 1,
        /// <summary>
        /// An enum constant representing the centro custo option.
        /// </summary>
        [Description("Centro Custo")]
        CentroCusto = 2
    }
}
