﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Dominio.Empresas
{
    /// <summary>
    /// Representa um(a) AreaProjeto.
    /// </summary>
    public class AreaProjeto
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public virtual int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Area.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Area.
        /// </value>
        public virtual Area Area { get; set; }

        /// <summary>
        /// Gets/Sets valor para Projeto.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Projeto.
        /// </value>
        public virtual Projeto Projeto { get; set; }

        /// <summary>
        /// Gets/Sets a situação do objetivo do tipo AreaProjeto.
        /// </summary>
        ///
        /// <value>
        /// <see cref="DataDesativacao"/> nula retorna true, <see cref="DataDesativacao"/> com valor
        /// retorna false.
        /// </value>
        public virtual bool Ativo { get; protected set; }

        /// <summary>
        /// Gets/Sets valor para DataRegistro.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataRegistro.
        /// </value>
        public virtual DateTime DataRegistro { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataDesativacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataDesativacao.
        /// </value>
        public virtual DateTime? DataDesativacao { get; set; }

        /// <summary>
        /// Ativa o objeto, definindo <see cref="Ativo"/> como true e <see cref="DataDesativacao"/> como
        /// nulo.
        /// </summary>
        public virtual void Ativar()
        {
            Ativo = true;
            DataDesativacao = null;
        }

        /// <summary>
        /// Inativa o objeto, definindo <see cref="Ativo"/> como falso e <see cref="DataDesativacao"/>
        /// como a data atual.
        /// </summary>
        public virtual void Inativar()
        {
            Ativo = false;
            DataDesativacao = DateTime.Now;
        }
    }
}
