﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;

namespace Controlsys.Dominio.Empresas
{
    /// <summary>
    /// Representa um(a) AlocacaoColaborador.
    /// </summary>
    public class AlocacaoColaborador
    {
        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public virtual int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataRegistro.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataRegistro.
        /// </value>
        public virtual DateTime DataRegistro { get; set; }

        /// <summary>
        /// Gets/Sets valor para Papel.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Papel.
        /// </value>
        public virtual Papel Papel { get; set; }

        /// <summary>
        /// Gets/Sets valor para PedidoCompra.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) PedidoCompra.
        /// </value>
        public virtual PedidoCompra PedidoCompra { get; set; }

        /// <summary>
        /// Gets/Sets valor para SetorCusto.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) SetorCusto.
        /// </value>
        public virtual SetorCusto SetorCusto { get; set; }        

        /// <summary>
        /// Gets/Sets valor para DataDesativacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataDesativacao.
        /// </value>
        public virtual DateTime? DataDesativacao { get; set; }

        /// <summary>
        /// Gets/Sets a situação do objetivo do tipo AlocacaoColaborador.
        /// </summary>
        ///
        /// <value>
        /// <see cref="DataDesativacao"/> nula retorna true, <see cref="DataDesativacao"/> com valor
        /// retorna false.
        /// </value>
        public virtual bool Ativo { get; protected set; }

        /// <summary>
        /// Ativa o objeto, definindo <see cref="Ativo"/> como true e <see cref="DataDesativacao"/> como
        /// nulo.
        /// </summary>
        public virtual void Ativar()
        {
            Ativo = true;
            DataDesativacao = null;
        }

        /// <summary>
        /// Inativa o objeto, definindo <see cref="Ativo"/> como falso e <see cref="DataDesativacao"/>
        /// como a data atual.
        /// </summary>
        public virtual void Inativar()
        {
            Ativo = false;
            DataDesativacao = DateTime.Now;
        }

        /// <summary>
        /// Obter contratada.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) PedidoCompra.
        /// </returns>
        public virtual PedidoCompra ObterContratada()
        {
            PedidoCompra pedidoCompra = PedidoCompra;

            if (pedidoCompra.PedidoCompraPai != null)
            {
                while (pedidoCompra.PedidoCompraPai.PedidoCompraPai != null)
                    pedidoCompra = pedidoCompra.PedidoCompraPai;
            }

            return pedidoCompra;
        }
    }
}
