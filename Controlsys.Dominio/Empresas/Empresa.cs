﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Controlsys.Dominio.Pessoas;
using Controlsys.Dominio.Parametros;

namespace Controlsys.Dominio.Empresas
{
    /// <summary>
    /// Representa um(a) Empresa.
    /// </summary>
    public class Empresa : Pessoa
    {
        /// <summary>
        /// Construtor para Controlsys.Dominio.Empresas.Empresa.
        /// </summary>
        public Empresa()
        {
            CNAEs = new List<EmpresaCNAE>();
            Documentos = new List<DocumentoEmpresa>();
            Sindicatos = new List<EmpresaSindicato>();
        }

        /// <summary>
        /// Gets/Sets valor para CNPJ.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CNPJ.
        /// </value>
        public virtual string CNPJ { get; set; }

        /// <summary>
        /// Gets/Sets valor para InscEstadual.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) InscEstadual.
        /// </value>
        public virtual string InscEstadual { get; set; }

        /// <summary>
        /// Gets/Sets valor para InscMunicipal.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) InscMunicipal.
        /// </value>
        public virtual string InscMunicipal { get; set; }

        /// <summary>
        /// Gets/Sets valor para CNAEs.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CNAEs.
        /// </value>
        public virtual IList<EmpresaCNAE> CNAEs { get; set; }

        /// <summary>
        /// Gets/Sets valor para Documentos.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Documentos.
        /// </value>
        public virtual IList<DocumentoEmpresa> Documentos { get; set; }


        public virtual IList<EmpresaSindicato> Sindicatos { get; set; }

        //public virtual string NaturezaJuridica { get; set; }

        /// <summary>
        /// Gets/Sets a situação do objetivo do tipo Empresa.
        /// </summary>
        ///
        /// <value>
        /// <see cref="DataDesativacao"/> nula retorna true, <see cref="DataDesativacao"/> com valor
        /// retorna false.
        /// </value>
        public virtual bool Ativo { get; /*protected*/ set; }
        /// <summary>
        /// 
        /// </summary>
        public virtual bool CalculoFte { get; /*protected*/ set; }
        
        /// <summary>
        /// Indica se a empresa deve ou não ser exibida no Relatório FTE
        /// </summary>
        public virtual bool ExibirRelatorioFTE { get; set; }

        /// <summary>
        /// Indica se colaborador sem pedido da empresa deve ou não ser bloqueado acesso
        /// </summary>
        public virtual bool BloqColabSemPedido { get; set; }

        /// <summary>
        /// Gets/Sets valor para EmpresaSAP.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) EmpresaSAP.
        /// </value>
        public virtual long? EmpresaSAP { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataDesativacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataDesativacao.
        /// </value>
        public virtual DateTime? DataDesativacao { get; set; }

        /// <summary>
        /// Ativa o objeto, definindo <see cref="Ativo"/> como true e <see cref="DataDesativacao"/> como
        /// nulo.
        /// </summary>
        public virtual void Ativar()
        {
            Ativo = true;
            DataDesativacao = null;
        }

        /// <summary>
        /// Inativa o objeto, definindo <see cref="Ativo"/> como falso e <see cref="DataDesativacao"/>
        /// como a data atual.
        /// </summary>
        public virtual void Inativar()
        {
            Ativo = false;
            DataDesativacao = DateTime.Now;
        }

        /// <summary>
        /// Cnpj formatado.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) string.
        /// </returns>
        public virtual string CNPJFormatado()
        {
            return string.Format(@"{0:00\.000\.000\/0000\-00}", Convert.ToInt64(CNPJ));
        }

        /// <summary>
        /// O metodo retorna apenas o nome da empresa, ele retira a concatenação do ( cnpj ) + nomeCNPJ
        /// </summary>
        /// <param name="empresa"></param>
        /// <returns></returns>
        public static string RetornaApenasNomeEmpresa(string empresa)
        {
            var splEmpresa = empresa.Split('(');
            string empresaNome = splEmpresa[0];
            int x = 0;
            if (splEmpresa[splEmpresa.Count() - 1].Where(c => char.IsNumber(c)).Count() > 0)
            {
                x = 1;
            }
            for (int i = 1; i < (splEmpresa.Count() - x); i++)
            {
                empresaNome = empresaNome + '(' + splEmpresa[i];
            }
            return empresaNome.TrimEnd();
        }
        
        public static string RetornaCNPJSemMascara(string empresa)
        {
            var splEmpresa = empresa.Split('(');
            string empresaNome = splEmpresa[0];
            int x = 0;
            if (splEmpresa[splEmpresa.Count() - 1].Where(c => char.IsNumber(c)).Count() > 0)
            {
                x = 1;
            }
            for (int i = 1; i < (splEmpresa.Count() - x); i++)
            {
                empresaNome = empresaNome + '(' + splEmpresa[i];
            }
            return empresaNome.TrimEnd();
        }       
    }
}
