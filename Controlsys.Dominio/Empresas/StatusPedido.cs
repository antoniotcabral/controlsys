﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controlsys.Dominio.Empresas
{
    /// <summary>
    /// Valores que representam StatusPedido.
    /// </summary>
    public enum StatusPedido
    {
        /// <summary>
        /// An enum constant representing the ativo option.
        /// </summary>
        Ativo,
        /// <summary>
        /// An enum constant representing the inativo option.
        /// </summary>
        Inativo,
        /// <summary>
        /// An enum constant representing the removido option.
        /// </summary>
        Removido
    }
}
