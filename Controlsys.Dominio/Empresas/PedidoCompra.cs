﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Controlsys.Dominio.Pessoas;

namespace Controlsys.Dominio.Empresas
{
    /// <summary>
    /// Representa um(a) PedidoCompra.
    /// </summary>
    public class PedidoCompra
    {
        /// <summary>
        /// Construtor para Controlsys.Dominio.Empresas.PedidoCompra.
        /// </summary>
        public PedidoCompra()
        {
            SubPedidos = new List<PedidoCompra>();
            Setores = new List<SetorCustoPedido>();
            GestoresResponsaveis = new List<ColaboradorPedido>();
            Prazos = new List<Prazo>();
            ColaboradoresAlocados = new List<AlocacaoColaborador>();
        }

        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public virtual int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataRegistro.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataRegistro.
        /// </value>
        public virtual DateTime DataRegistro { get; set; }

        /// <summary>
        /// Gets/Sets valor para Status.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Status.
        /// </value>
        public virtual StatusPedido Status { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataStatus.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataStatus.
        /// </value>
        public virtual DateTime? DataStatus { get; set; }

        /// <summary>
        /// Gets/Sets valor para Numero.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Numero.
        /// </value>
        public virtual string Numero { get; set; }

        /// <summary>
        /// Gets/Sets valor para Projeto.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Projeto.
        /// </value>
        public virtual Projeto Projeto { get; set; }

        /// <summary>
        /// Gets/Sets valor para Empresa.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Empresa.
        /// </value>
        public virtual Empresa Empresa { get; set; }

        /// <summary>
        /// Gets/Sets valor para Nome.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Nome.
        /// </value>
        public virtual string Nome { get; set; }

        /// <summary>
        /// Gets/Sets valor para Observacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Observacao.
        /// </value>
        public virtual string Observacao { get; set; }

        /// <summary>
        /// Gets/Sets valor para PedidoCompraPai.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) PedidoCompraPai.
        /// </value>
        public virtual PedidoCompra PedidoCompraPai { get; set; }

        /// <summary>
        /// Gets/Sets valor para SubPedidos.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) SubPedidos.
        /// </value>
        public virtual IList<PedidoCompra> SubPedidos { get; set; }

        /// <summary>
        /// Gets/Sets valor para Setores.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Setores.
        /// </value>
        public virtual IList<SetorCustoPedido> Setores { get; set; }

        /// <summary>
        /// Gets/Sets valor para GestoresResponsaveis.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) GestoresResponsaveis.
        /// </value>
        public virtual IList<ColaboradorPedido> GestoresResponsaveis { get; set; }

        /// <summary>
        /// Gets/Sets valor para Prazos.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Prazos.
        /// </value>
        public virtual IList<Prazo> Prazos { get; set; }

        /// <summary>
        /// Gets/Sets valor para ColaboradoresAlocados.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) ColaboradoresAlocados.
        /// </value>
        public virtual IList<AlocacaoColaborador> ColaboradoresAlocados { get; set; }

        /// <summary>
        /// Gets/Sets valor para CNAE.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) CNAE.
        /// </value>
        public virtual CNAE CNAE { get; set; }

        /// <summary>
        /// Obter contratada.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) PedidoCompra.
        /// </returns>
        public virtual PedidoCompra ObterContratada()
        {
            PedidoCompra pedido = this;

            if (pedido.PedidoCompraPai != null)
            {
                while (pedido.PedidoCompraPai.PedidoCompraPai != null)
                    pedido.PedidoCompraPai = pedido;
            }

            return pedido;
        }
    }
}
