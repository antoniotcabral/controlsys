﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controlsys.Dominio.Empresas
{
    /// <summary>
    /// Representa um(a) Projeto.
    /// </summary>
    public class Projeto
    {
        /// <summary>
        /// Construtor para Controlsys.Dominio.Empresas.Projeto.
        /// </summary>
        public Projeto()
        {
            Areas = new List<AreaProjeto>();
            Treinamentos = new List<TreinamentoProjeto>();
            GruposTrabalho = new List<GrupoTrabalhoProjeto>();
        }

        /// <summary>
        /// Gets/Sets valor para Codigo.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Codigo.
        /// </value>
        public virtual int Codigo { get; set; }

        /// <summary>
        /// Gets/Sets valor para Nome.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Nome.
        /// </value>
        public virtual string Nome { get; set; }

        /// <summary>
        /// Gets/Sets valor para Descricao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Descricao.
        /// </value>
        public virtual string Descricao { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataInicio.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataInicio.
        /// </value>
        public virtual DateTime DataInicio { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataFim.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataFim.
        /// </value>
        public virtual DateTime DataFim { get; set; }

        /// <summary>
        /// Gets/Sets a situação do objetivo do tipo Projeto.
        /// </summary>
        ///
        /// <value>
        /// <see cref="DataDesativacao"/> nula retorna true, <see cref="DataDesativacao"/> com valor
        /// retorna false.
        /// </value>
        public virtual bool Ativo { get; /*protected*/ set; }

        /// <summary>
        /// Gets/Sets valor para DataRegistro.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataRegistro.
        /// </value>
        public virtual DateTime DataRegistro { get; set; }

        /// <summary>
        /// Gets/Sets valor para DataDesativacao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) DataDesativacao.
        /// </value>
        public virtual DateTime? DataDesativacao { get; set; }

        /// <summary>
        /// Gets/Sets valor para Areas.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Areas.
        /// </value>
        public virtual IList<AreaProjeto> Areas { get; set; }

        /// <summary>
        /// Gets/Sets valor para Treinamentos.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Treinamentos.
        /// </value>
        public virtual IList<TreinamentoProjeto> Treinamentos { get; set; }

        /// <summary>
        /// Gets/Sets valor para GruposTrabalho.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) GruposTrabalho.
        /// </value>
        public virtual IList<GrupoTrabalhoProjeto> GruposTrabalho { get; set; }

        /// <summary>
        /// Ativa o objeto, definindo <see cref="Ativo"/> como true e <see cref="DataDesativacao"/> como
        /// nulo.
        /// </summary>
        public virtual void Ativar()
        {
            Ativo = true;
            DataDesativacao = null;
        }

        /// <summary>
        /// Inativa o objeto, definindo <see cref="Ativo"/> como falso e <see cref="DataDesativacao"/>
        /// como a data atual.
        /// </summary>
        public virtual void Inativar()
        {
            Ativo = false;
            DataDesativacao = DateTime.Now;
        }
    }
}
