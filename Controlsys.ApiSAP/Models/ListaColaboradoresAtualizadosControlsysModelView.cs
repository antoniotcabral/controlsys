﻿using Controlsys.Dominio.Pessoas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Globlasys.Controlsys.ApiSAP.Models
{
    public class ListaColaboradoresAtualizadosControlsysModelView
    {
        public virtual int CD_Papel { get; set; }
        public virtual long ID_Empregado_Sap { get; set; }
        public virtual string Nome { get; set; }
        public virtual string Email { get; set; }
        public virtual string Cracha { get; set; }
        public virtual string Cargo { get; set; }
        public virtual long? CD_Superior { get; set; }
        public virtual string Area { get; set; }
        public virtual string Status { get; set; }
        public virtual string Status_Processo { get; set; }
        public virtual string MSG_Processo { get; set; }
    }
}