﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Globalsys;
using Controlsys.Infra;
using Controlsys.Repositorio.Acesso;
using Controlsys.Repositorio.Parametros;
using Controlsys.Dominio.Acesso;
using System.Configuration;
using Controlsys.ApiSAP.Globalsys.ApiSAPEnviaAcesso;
using Controlsys.Repositorio.Audit;
using Controlsys.Dominio.Audit;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using System.Text;
using System.Net.Http;
using Globalsys.Exceptions;
using Controlsys.Dominio.Pessoas;
using Controlsys.Repositorios.Acesso;
using NHibernate;
using Controlsys.Repositorios.Pessoas;

namespace Controlsys.ApiSAP.Models
{

    class ControleAcessoModel
    {        
        public int Codigo { get; set; }
        public int Leitora { get; set; }
        public string NumeroSerie { get; set; }
        public int Cracha { get; set; }
        public int Papel { get; set; }
        public DateTime DataRegistro { get; set; }
        public DateTime DataAcesso { get; set; }
        public int StatusAcesso { get; set; }
        public string Direcao { get; set; }
        public string Local { get; set; }
        public DateTime? DataIntegracao { get; set; }
    }


    public class EnviarAcesso : ZCN_ENVIAR_MARCAJES
    {
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        public ZCN_F00129_NTEResponse ZCN_F00129_NTE()
        {
            UnidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            
            Globalsys.ApiSAPEnviaAcesso.ZCN_ENVIAR_MARCAJES w = new Globalsys.ApiSAPEnviaAcesso.ZCN_ENVIAR_MARCAJES();
            ZCN_F00129_NTEResponse retorno = new ZCN_F00129_NTEResponse();

            string usuarioApiSAP = System.Configuration.ConfigurationSettings.AppSettings["usuarioApiSAP"];
            DateTime ultimoEnvioLogErro = DateTime.Now.AddMinutes(-180);
            try
            {
                //IRepositorioLogError repLogPassou = Fabrica.Instancia.ObterRepositorio<IRepositorioLogError>(UnidadeTrabalho);

                IRepositorioControleAcesso repControleAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioControleAcesso>(UnidadeTrabalho);
                IRepositorioCracha repCracha = Fabrica.Instancia.ObterRepositorio<IRepositorioCracha>(UnidadeTrabalho);
                IRepositorioGrupoLeitora repGrupoLeitora = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoLeitora>(UnidadeTrabalho);

                IRepositorioIntegracaoSAP repIntegracaoSAP = Fabrica.Instancia.ObterRepositorio<IRepositorioIntegracaoSAP>(UnidadeTrabalho);
                IRepositorioIntegracaoSAPAcesso repIntegracaoSAPAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioIntegracaoSAPAcesso>(UnidadeTrabalho);
                
                IRepositorioLeitora repLeitora = Fabrica.Instancia.ObterRepositorio<IRepositorioLeitora>(UnidadeTrabalho);
                IRepositorioPapel repPapel = Fabrica.Instancia.ObterRepositorio<IRepositorioPapel>(UnidadeTrabalho);                                

                DateTime dataInicioIntegracaoAcesso = Convert.ToDateTime("2000-01-01 00:00:00");

                if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["dataInicioIntegracaoAcesso"]))
                {
                    dataInicioIntegracaoAcesso = Convert.ToDateTime(ConfigurationManager.AppSettings["dataInicioIntegracaoAcesso"]);
                }

                string qtdRegIntegracaoAcesso = ConfigurationManager.AppSettings["qtdRegIntegracaoAcesso"];
                int qtdRegIntAcesso = 500;
                if (!String.IsNullOrEmpty(qtdRegIntegracaoAcesso))
                {
                    if (Convert.ToInt32(qtdRegIntegracaoAcesso) > 2000)
                    {
                        qtdRegIntAcesso = Convert.ToInt32(2000);
                    }
                    else
                    {
                        qtdRegIntAcesso = Convert.ToInt32(qtdRegIntegracaoAcesso);
                    }
                }

                string sql = $@" Select TOP({qtdRegIntAcesso}) a.CD_ACESSO as Codigo,
                                a.CD_LEITORA as Leitora,
                                a.TX_SERIE as NumeroSerie,
                                a.CD_CRACHA as Cracha,
                                a.CD_PAPEL as Papel,
                                a.DT_REGISTRO as DataRegistro,
                                a.DT_LEITORA as DataAcesso,
                                a.TX_STATUS as StatusAcesso,
                                a.TX_DIRECAO as Direcao,
                                a.TX_LOCAL as Local,
                                a.DT_INTEGRACAO as DataIntegracao
                        From Acesso a
                        Where a.DT_INTEGRACAO is null
                            And a.CD_PAPEL is not null
                            And a.DT_LEITORA >= '{dataInicioIntegracaoAcesso.ToString("yyyy-MM-dd")}'
                            And a.TX_STATUS In(1, 10, 11)
                            And Exists(Select *
                                        From Leitora l Join Leitora_Grupo_Leitor lgl On(l.CD_LEITORA = lgl.CD_LEITORA)
                                                    Join Grupo_Leitora gl On(lgl.CD_GRUPO = gl.CD_GRUPO_LEITORA)
                                        Where l.BL_ATIVO = 1
                                        And lgl.BL_ATIVO = 1
                                        And gl.BL_ATIVO = 1
                                        And gl.BL_EXTERNA = 1
                                        And a.CD_LEITORA = l.CD_LEITORA)
                            And Not exists(Select *
                                            From INTEGRACAO_SAP_ACESSO T50 Join INTEGRACAO_SAP T51 On(T50.CD_INTEGRACAO_SAP = T51.CD_INTEGRACAO_SAP)
                                            Where(T51.TX_STATUS = 3 Or T51.DT_INTEGRACAO_SAP is null)
                                            And T51.DT_ATUALIZACAO >= CAST(GETDATE() AS DATE)
                                            And a.CD_ACESSO = T50.CD_ACESSO)
                            Order By CD_ACESSO desc
                ";

                var lista = UnidadeTrabalho.ExecuteSql<ControleAcessoModel>(sql, new Dictionary<string, object>()).AsQueryable();

                IEnumerable<ControleAcesso> listaControleAcesso = lista.Select(d => new ControleAcesso()
                {
                    Codigo = d.Codigo,
                    Leitora = repLeitora.ObterPorId(d.Leitora),
                    NumeroSerie = d.NumeroSerie,
                    Cracha = repCracha.ObterPorId(d.Cracha),
                    Papel = repPapel.ObterPorId(d.Papel),
                    DataRegistro = d.DataRegistro,
                    DataAcesso = d.DataAcesso,
                    StatusAcesso = (StatusAcesso)d.StatusAcesso,
                    Direcao = (DirecaoLeitora)Enum.Parse(typeof(DirecaoLeitora) ,d.Direcao),                 
                    Local = d.Local,
                    DataIntegracao = d.DataIntegracao
                }).OrderByDescending(z => z.DataAcesso).Take(qtdRegIntAcesso).ToList();

                //var listaGrupoLeitora = repGrupoLeitora.ObterTodos().Where(g => g.Externa == true).Select(lgl => lgl.Leitoras);

                //List<ControleAcesso> listaControleAcesso =
                //    repControleAcesso.ObterTodos().Where(m => m.DataIntegracao == null
                //                                         && m.Papel != null
                //                                         && m.DataAcesso >= dataInicioIntegracaoAcesso                                                         
                //                                         && (m.StatusAcesso == StatusAcesso.OCO_UR_ACESSO_LIBERADO || m.StatusAcesso == StatusAcesso.OCO_UR_ENTRADA_COMPLETA || m.StatusAcesso == StatusAcesso.OCO_UR_SAIDA_COMPLETA)
                //                                         && listaGrupoLeitora.Any(lgl => lgl.Any(l => l.Leitora.Codigo == m.Leitora.Codigo))                                                         
                //                                         ).OrderBy(z => z.DataAcesso).Take(qtdRegIntAcesso).ToList();

                w.Url = ConfigurationManager.AppSettings["EnderecoApiSAP"];               

                ZECN0195[] zECN0195 = listaControleAcesso.Select(lca => new ZECN0195
                {
                    ID_FIC = Convert.ToString(lca.Codigo),
                    LEGAJO = lca?.Papel?.EmpregadoSAP?.Codigo.ToString() ?? lca?.Papel?.Codigo.ToString() ?? null,
                    FECHA = lca.DataAcesso.ToString("yyyy-MM-dd"),
                    HORA = lca.DataAcesso.ToString("HH:mm:ss"),
                    TIPO_F = lca.Direcao.ToString() == "Entrada"? "P10": "P20",
                    PUERTA = lca?.Leitora?.Codigo.ToString(),
                    PROCESADO = "",
                    CVE_PRO = "",                    
                    USER_MOD = usuarioApiSAP,
                    FEC_MOD = lca.DataRegistro.ToString("yyyy-MM-dd"),
                    HOR_MOD = lca.DataRegistro.ToString("HH:mm:ss"),                  
                    CARD_NUM = lca.Papel != null ? repCracha.ObterTodos().FirstOrDefault(x => x.Papel.Codigo == lca.Papel.Codigo)?.Numero.ToString() : null,
                }).ToArray();

                ZECN0196[] zECN0196 = listaControleAcesso.Select(lca => new ZECN0196
                {
                    ID_FIC = Convert.ToString(lca.Codigo),
                    //LEGAJO = lca?.Papel?.EmpregadoSAP?.Codigo.ToString() ?? null,
                    LEGAJO = lca?.Papel?.EmpregadoSAP?.Codigo.ToString() ?? lca.Papel?.Codigo.ToString() ?? null,
                    FECHA = lca.DataAcesso.ToString("yyyy-MM-dd"),
                    HORA = lca.DataAcesso.ToString("HH:mm:ss"),
                    TIPO_F = lca.Direcao.ToString() == "Entrada" ? "P10" : "P20",
                    PUERTA = lca?.Leitora?.Codigo.ToString(),
                    PROCESADO = "",
                    CVE_PRO = "",                    
                    USER_MOD = usuarioApiSAP,
                    FEC_MOD = lca.DataRegistro.ToString("yyyy-MM-dd"),
                    HOR_MOD = lca.DataRegistro.ToString("HH:mm:ss"),
                    CARD_NUM = lca.Papel != null ? repCracha.ObterTodos().FirstOrDefault(x => x.Papel.Codigo == lca.Papel.Codigo)?.Numero.ToString() : null,
                    ESTATUS_PROCESO = "",
                    MSJ = ""
                }).ToArray();
                
                StringWriter stringWriter = new StringWriter();
                XmlTextWriter xmlTextWriter = new XmlTextWriter(stringWriter);
                XmlSerializer serializer = new XmlSerializer(zECN0195.GetType());
                serializer.Serialize(xmlTextWriter, zECN0195);
                var verxml = stringWriter.ToString();
                
                retorno = w.ZCN_F00129_NTE(new ZCN_F00129_NTE()
                {
                    I_DATOS_FIC = zECN0195,
                    O_DATOS_FIC = zECN0196
                });                

                var listaRetorno = retorno.O_DATOS_FIC.ToList();

                foreach (var item in listaRetorno)
                {
                    var controleAcesso = repControleAcesso.ObterTodos().Where(x => x.Codigo == Convert.ToInt32(item.ID_FIC)).FirstOrDefault();

                    var papel = controleAcesso.Papel;

                    IntegracaoSAP integracaoSAP = new IntegracaoSAP();

                    var integracaoSAPAcesso = repIntegracaoSAPAcesso.ObterTodos().Where(x => x.ControleAcesso.Codigo == controleAcesso.Codigo && (x.IntegracaoSAP.DataIntegracao == null || x.IntegracaoSAP.Status == (Int32)StatusIntegracao.Falha)).FirstOrDefault();
                    if (integracaoSAPAcesso != null)
                    {
                        integracaoSAP = repIntegracaoSAP.ObterTodos().Where(x => x.Codigo == integracaoSAPAcesso.IntegracaoSAP.Codigo).FirstOrDefault();
                    }

                    if (item.ESTATUS_PROCESO == "S")
                    {
                        controleAcesso.DataIntegracao = DateTime.Now;
                        try
                        {
                            UnidadeTrabalho.BeginTransaction();
                            repControleAcesso.Atualizar(controleAcesso);
                            UnidadeTrabalho.Commit();
                        }
                        catch (Exception ex)
                        {
                            UnidadeTrabalho.Rollback();
                            throw ex;
                        }
                        

                        //Atualiza o log de erro.
                        if (integracaoSAPAcesso != null)
                        {
                            integracaoSAP.DataIntegracao = DateTime.Now;
                            integracaoSAP.Status = (int)StatusIntegracao.Sucesso;
                            integracaoSAP.Mensagem = "Sucesso";
                            integracaoSAP.DataAtualizacao = DateTime.Now;
                            integracaoSAP.TipoAtualizacao = (int)TipoIntegracao.Atualizacao_Recebida;
                            try
                            {
                                UnidadeTrabalho.BeginTransaction();
                                repIntegracaoSAP.Atualizar(integracaoSAP);
                                UnidadeTrabalho.Commit();
                            }
                            catch (Exception ex)
                            {
                                UnidadeTrabalho.Rollback();
                                throw ex;
                            }
                            
                        }
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(item.ESTATUS_PROCESO))
                        {
                            // Grava o log de erro.
                            if (integracaoSAPAcesso != null)
                            {
                                integracaoSAP.Status = (int)StatusIntegracao.Falha;
                                integracaoSAP.Mensagem = item.MSJ;
                                integracaoSAP.DataAtualizacao = DateTime.Now;
                                integracaoSAP.TipoAtualizacao = (int)TipoIntegracao.Atualizacao_Recebida;
                                try
                                {
                                    UnidadeTrabalho.BeginTransaction();
                                    repIntegracaoSAP.Atualizar(integracaoSAP);
                                    UnidadeTrabalho.Commit();
                                }
                                catch (Exception ex)
                                {
                                    UnidadeTrabalho.Rollback();
                                    throw ex;
                                }
                                
                            }
                            else
                            {
                                integracaoSAP.Status = (int)StatusIntegracao.Falha;
                                integracaoSAP.Mensagem = item.MSJ;
                                integracaoSAP.DataAtualizacao = DateTime.Now;
                                integracaoSAP.TipoAtualizacao = (int)TipoIntegracao.Atualizacao_Recebida;
                                repIntegracaoSAP.Salvar(integracaoSAP);

                                integracaoSAPAcesso = new IntegracaoSAPAcesso();

                                integracaoSAPAcesso.ControleAcesso = controleAcesso;
                                integracaoSAPAcesso.IntegracaoSAP = integracaoSAP;
                                repIntegracaoSAPAcesso.Salvar(integracaoSAPAcesso);
                            }
                        }

                    }
                }

                /*
                LogError logPassou = new LogError();
                logPassou = new LogError();
                logPassou.Message = "Passo 3 - Depois Gravar xml";
                logPassou.StackTrace = "Passo 3 - Depois Gravar xml";
                logPassou.Url = "Controlsys.ApiSAP\\RegistraAcesso";
                logPassou.DataRegistro = DateTime.Now;
                logPassou.UserName = System.Configuration.ConfigurationSettings.AppSettings["usuarioApiSAP"];
                repLogPassou.Salvar(logPassou);
                */
                                
                return retorno;                
            }
            catch (Exception ex)
            {
                IRepositorioLogError repLogError = Fabrica.Instancia.ObterRepositorio<IRepositorioLogError>(UnidadeTrabalho);
                //System.Diagnostics.Debug.WriteLine("Log: " + Guid.NewGuid().ToString());                        
                if (ultimoEnvioLogErro.AddMinutes(5) <= DateTime.Now)
                {
                    LogError logError = new LogError();
                    logError.Message = ex.Message;
                    logError.StackTrace = ex.StackTrace;
                    logError.Url = "Controlsys.ApiSAP\\RegistraAcesso";
                    logError.DataRegistro = DateTime.Now;
                    logError.UserName = usuarioApiSAP;

                    repLogError.Salvar(logError);

                    ultimoEnvioLogErro = DateTime.Now;
                }
                Console.WriteLine(ex.Message);

                return retorno;
            }      
        }

        public Task<ZCN_F00129_NTEResponse> ZCN_F00129_NTEAsync()
        {
            throw new NotImplementedException();
        }
    }
}