﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Controlsys.ApiSAP.Models
{
    [Serializable]
    public class RegistraAcessoModelView
    {
        [System.Xml.Serialization.XmlElement("ID_unico_de_fichada")]
        public virtual string ID_unico_de_fichada { get; set; }

        [System.Xml.Serialization.XmlElement("Matricula_de_Personal_SAP")]
        public virtual long Matricula_de_Personal_SAP { get; set; }

        [System.Xml.Serialization.XmlElement("Fecha_de_marcaje_leitora")]
        public virtual string Fecha_de_marcaje_leitora { get; set; }

        [System.Xml.Serialization.XmlElement("Hora_de_marcaje_leitora")]
        public virtual string Hora_de_marcaje_leitora { get; set; }

        [System.Xml.Serialization.XmlElement("Clase_hecho_temporal")]        
        public virtual string Clase_hecho_temporal { get; set; }

        [System.Xml.Serialization.XmlElement("ID_Terminal")]
        public virtual string ID_Terminal { get; set; }

        [System.Xml.Serialization.XmlElement("Indicador_general")]
        public virtual string Indicador_general { get; set; }

        [System.Xml.Serialization.XmlElement("Clave_Proceso")]
        public virtual string Clave_Proceso { get; set; }

        [System.Xml.Serialization.XmlElement("Nombre_de_usuario")]
        public virtual string Nombre_de_usuario { get; set; }

        [System.Xml.Serialization.XmlElement("Fecha_de_marcaje_registro")]
        public virtual string Fecha_de_marcaje_registro { get; set; }

        [System.Xml.Serialization.XmlElement("Hora_de_marcaje_registro")]
        public virtual string Hora_de_marcaje_registro { get; set; }

        [System.Xml.Serialization.XmlElement("Comunicacion_id_numero")]
        public virtual string Comunicacion_id_numero { get; set; }
    }

    [Serializable]
    [XmlRoot("ColecaoRegistraAcessoModelView")]
    [XmlInclude(typeof(RegistraAcessoModelView))] // include type class Person
    public class ColecaoRegistraAcessoModelView
    {
        [XmlArray("RegistraAcessoModelView")]
        [XmlArrayItem("RegistraAcessoModelView", typeof(RegistraAcessoModelView))]
        public List<RegistraAcessoModelView> colRegistraAcessoModelView { get; set; }
    }

}