﻿using Controlsys.ApiSAP.Globalsys.ApiSAPEnviaAcesso;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Controlsys.ApiSAP.Models
{
    // Models returned by AccountController actions.

    public class ExternalLoginViewModel : ZCN_ENVIAR_MARCAJES
    {
        public string Name { get; set; }

        public string Url { get; set; }

        public string State { get; set; }

        //public ZCN_F00129_NTEResponse ZCN_F00129_NTE(ZCN_F00129_NTERequest request)
        //{
        //    Globalsys.ApiSAPEnviaAcesso.ZCN_ENVIAR_MARCAJES w = new Globalsys.ApiSAPEnviaAcesso.ZCN_ENVIAR_MARCAJES();
        //    var r = w.ZCN_F00129_NTE(new Globalsys.ApiSAPEnviaAcesso.ZCN_F00129_NTE() {
                
        //    });

        //    //r.
        //    throw new NotImplementedException();
        //}

        //public Task<ZCN_F00129_NTEResponse1> ZCN_F00129_NTEAsync(ZCN_F00129_NTERequest request)
        //{
        //    throw new NotImplementedException();
        //}
    }

    public class ManageInfoViewModel
    {
        public string LocalLoginProvider { get; set; }

        public string Email { get; set; }

        public IEnumerable<UserLoginInfoViewModel> Logins { get; set; }

        public IEnumerable<ExternalLoginViewModel> ExternalLoginProviders { get; set; }
    }

    public class UserInfoViewModel
    {
        public string Email { get; set; }

        public bool HasRegistered { get; set; }

        public string LoginProvider { get; set; }
    }

    public class UserLoginInfoViewModel
    {
        public string LoginProvider { get; set; }

        public string ProviderKey { get; set; }
    }
}
