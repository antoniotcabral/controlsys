﻿using Controlsys.Dominio.Audit;
using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Pessoas;
using Controlsys.Infra;
using Controlsys.Persistencia;
using Controlsys.Repositorio.Audit;
using Controlsys.Repositorio.Parametros;
using Controlsys.Repositorios.Parametros;
using Controlsys.Repositorios.Pessoas;
using Globalsys;
using Globlasys.Controlsys.ApiSAP.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.EnterpriseServices;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

namespace Controlsys.ApiSAP.Models
{

    //public class ListaColaboradoresAtualizadosControlsys
    //{
    //    public IUnidadeTrabalho UnidadeTrabalho { get; set; }
    //    /// <summary>
    //    /// O método envia colaboradores atualizados ou incluídos
    //    /// </summary>
    //    /// <returns>Retorna a lista de colaboradores da TERNIUM enviados ao SAP</returns>
    //    public List<ListaColaboradoresAtualizadosControlsysModelView> EnviaColaboradoresAtualizadosControlsys(DateTime? periodoColaborador = null)
    //    {
    //        UnidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
    //        List<ListaColaboradoresAtualizadosControlsysModelView> retornoListaAlterInclusao = new List<ListaColaboradoresAtualizadosControlsysModelView>();
    //        string usuarioApiSAP = System.Configuration.ConfigurationSettings.AppSettings["usuarioApiSAP"];

    //        IRepositorioPapel repPapel = Fabrica.Instancia.ObterRepositorio<IRepositorioPapel>(UnidadeTrabalho);
    //        IRepositorioPessoa repPessoa = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoa>(UnidadeTrabalho);
    //        IRepositorioColaborador repColaborador = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);
    //        IRepositorioCracha repCracha = Fabrica.Instancia.ObterRepositorio<IRepositorioCracha>(UnidadeTrabalho);
    //        IRepositorioCargo repCargo = Fabrica.Instancia.ObterRepositorio<IRepositorioCargo>(UnidadeTrabalho);
    //        IRepositorioArea repArea = Fabrica.Instancia.ObterRepositorio<IRepositorioArea>(UnidadeTrabalho);
    //        IRepositorio<EmpregadoSAP> repEmpregadoSap = Fabrica.Instancia.ObterRepositorio<IRepositorio<EmpregadoSAP>>(UnidadeTrabalho);
    //        IRepositorio<PapelLog> repPapelLog = Fabrica.Instancia.ObterRepositorio<IRepositorio<PapelLog>>(UnidadeTrabalho);
    //        IRepositorioColaboradorAtualizadoIncluido repAtualizadoIncluido_ = Fabrica.Instancia.ObterRepositorio<IRepositorioColaboradorAtualizadoIncluido>(UnidadeTrabalho);
    //        ListaColaboradoresAtualizadosControlsysModelView dadosColaborador = new ListaColaboradoresAtualizadosControlsysModelView();
    //        List<ColaboradorAtualizadoIncluido> listaColaboradores = new List<ColaboradorAtualizadoIncluido>();
    //        int qtdRegIntAcesso = 1000;

    //        if (periodoColaborador != null)
    //        {
    //            listaColaboradores = repAtualizadoIncluido_.ObterTodos().Where(x => x.DataRegistro == periodoColaborador).Take(qtdRegIntAcesso).ToList();
    //        }
    //        else
    //        {
    //            listaColaboradores = repAtualizadoIncluido_.ObterTodos().Where(x => x.DataEnvio == null).Take(qtdRegIntAcesso).ToList();
    //        }
    //        foreach (var item in listaColaboradores)
    //        {
    //            List<Colaborador> listaColaborador = repColaborador.ObterTodos().Where(x => x.Codigo == item.CDPapel).ToList();
    //            foreach (var itemColaboradores in listaColaborador)
    //            {
    //                EmpregadoSAP empregadoSap;
    //                Area area;
    //                Pessoa pessoa = repPessoa.ObterPorId(itemColaboradores.PessoaFisica.Codigo);
    //                Cracha cracha = repCracha.ObterTodos().Where(x => x.Papel.Codigo == item.CDPapel && x.DataDescarte == null).SingleOrDefault();
    //                Papel papel = repPapel.ObterPorId(item.CDPapel);
    //                if (papel.EmpregadoSAP == null) { empregadoSap = null; }
    //                else { empregadoSap = repEmpregadoSap.ObterPorId(papel.EmpregadoSAP.Codigo); }
    //                if (itemColaboradores.Area == null) { area = null; }
    //                else { area = repArea.ObterPorId(itemColaboradores.Area.Codigo); }

    //                PapelLog papelLog = repPapelLog.ObterTodos().Where(x => x.Papel.Codigo == item.CDPapel).OrderBy(x => x.DataInicio).First();

    //                dadosColaborador.CD_Papel = item.CDPapel;
    //                dadosColaborador.Nome = pessoa.Nome;
    //                dadosColaborador.Email = pessoa.Email;
    //                dadosColaborador.Cargo = papel.Cargo.Nome;
    //                dadosColaborador.Status = papelLog.Status.ToString();
    //                if (empregadoSap != null)
    //                {
    //                    dadosColaborador.ID_Empregado_Sap = empregadoSap.Codigo;
    //                    if (empregadoSap.SuperiorImediato != null)
    //                    { dadosColaborador.CD_Superior = empregadoSap.SuperiorImediato.Codigo; }
    //                }
    //                if (cracha != null) { dadosColaborador.Cracha = cracha.Numero; }
    //                if (area != null) { dadosColaborador.Area = area.Nome; }
    //                retornoListaAlterInclusao.Add(dadosColaborador);
    //            }
    //        }
    //        try
    //        {
    //            #region [Teste - SoapUI]
    //            var task = Task.Run<List<ListaColaboradoresAtualizadosControlsysModelView>>(async () => await EnviaListaSap(retornoListaAlterInclusao));
    //            var resultadoIntegracaoSap = task.Result.ToList();
    //            #endregion
    //            bool resultado = AdicionaResultadoAPISAP(resultadoIntegracaoSap);
    //        }
    //        catch (Exception ex)
    //        {
    //            IRepositorioLogError repLogError = Fabrica.Instancia.ObterRepositorio<IRepositorioLogError>(UnidadeTrabalho);

    //            LogError logError = new LogError();
    //            logError.Message = ex.Message;
    //            logError.StackTrace = ex.StackTrace;
    //            logError.Url = "Controlsys.ApiSAP\\ListaColaboradoresAtualizadosControlsys";
    //            logError.DataRegistro = DateTime.Now;
    //            logError.UserName = usuarioApiSAP;
    //            repLogError.Salvar(logError);
    //            Console.WriteLine(ex.Message);
    //            return retornoListaAlterInclusao;
    //            throw;
    //        }

    //        return retornoListaAlterInclusao;
    //    }

    //    private bool AdicionaResultadoAPISAP(List<ListaColaboradoresAtualizadosControlsysModelView> retornoSap, DateTime? periodoParametro = null)
    //    {
    //        UnidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
    //        IRepositorioColaboradorAtualizadoIncluido repAtualizadoIncluido_ = Fabrica.Instancia.ObterRepositorio<IRepositorioColaboradorAtualizadoIncluido>(UnidadeTrabalho);
    //        List<ColaboradorAtualizadoIncluido> colaboradorAtualizado_ = new List<ColaboradorAtualizadoIncluido>();
    //        foreach (var item in retornoSap)
    //        {
    //            if (periodoParametro != null)
    //            {
    //                colaboradorAtualizado_ = repAtualizadoIncluido_.ObterTodos().Where(x => x.CDPapel.Equals(item.CD_Papel) && x.DataRegistro == periodoParametro).ToList();
    //                if (colaboradorAtualizado_.Count().Equals(0))
    //                { break; }
    //            }
    //            else
    //            {
    //                colaboradorAtualizado_ = repAtualizadoIncluido_.ObterTodos().Where(x => x.CDPapel.Equals(item.CD_Papel) && x.DataEnvio == null).ToList();
    //                if (colaboradorAtualizado_.Count().Equals(0))
    //                { break; }
    //            }

    //            foreach (var itemColaboradores in colaboradorAtualizado_)
    //            {
    //                if (item.Status_Processo.Equals("S"))
    //                {
    //                    itemColaboradores.MensagemRecebida = "Sucesso";
    //                    itemColaboradores.DataEnvio = DateTime.Now;
    //                    itemColaboradores.MensagemLog = null;
    //                }
    //                else if (item.Status_Processo.Equals("I"))
    //                {
    //                    itemColaboradores.MensagemRecebida = "Inválido";
    //                    itemColaboradores.MensagemLog = item.MSG_Processo;
    //                }
    //                try
    //                {
    //                    UnidadeTrabalho.BeginTransaction();
    //                    repAtualizadoIncluido_.Atualizar(itemColaboradores);
    //                    UnidadeTrabalho.Commit();
    //                }
    //                catch (Exception e)
    //                {
    //                    UnidadeTrabalho.Rollback();
    //                    throw e;
    //                }
    //            }
    //        }
    //        return true;
    //    }

    //    private async Task<List<ListaColaboradoresAtualizadosControlsysModelView>> EnviaListaSap(List<ListaColaboradoresAtualizadosControlsysModelView> retornoListaAlterInclusao)
    //    {
    //        string usuarioApiSAPUrl = System.Configuration.ConfigurationSettings.AppSettings["EnderecoUrlSapApi"];

    //        HttpClient client = new HttpClient();
    //        var jsonContent = JsonConvert.SerializeObject(retornoListaAlterInclusao);
    //        var contentString = new StringContent(jsonContent, Encoding.UTF8, "application/json");
    //        client.BaseAddress = new Uri(usuarioApiSAPUrl);
    //        HttpResponseMessage response = await client.PostAsync("/Api/ListaColaboradoresAtualizadosControlsys", new StringContent("", Encoding.UTF8, "application/xml"));
    //        var result = response.Content.ReadAsAsync<List<ListaColaboradoresAtualizadosControlsysModelView>>();
    //        return result.Result.ToList();
    //    }
    //}
}