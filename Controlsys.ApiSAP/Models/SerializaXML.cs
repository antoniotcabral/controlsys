﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

namespace Globlasys.Controlsys.ApiSAP.Models
{
    public class SerializaXML
    {
        public static string GetXMLString<T>(T objeto) where T : class
        {
            StringWriter stringWriter = new StringWriter();
            XmlTextWriter xmlTextWriter = new XmlTextWriter(stringWriter);
            XmlSerializer serializer = new XmlSerializer(objeto.GetType());
            serializer.Serialize(xmlTextWriter, objeto);
            return stringWriter.ToString();
        }

        public static T GetObjectXML<T>(string xml) where T : class
        {
            StringReader strReader = new StringReader(xml);
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            XmlTextReader xmlReader = new XmlTextReader(strReader);
            return (T)serializer.Deserialize(xmlReader);
        }

    }
}