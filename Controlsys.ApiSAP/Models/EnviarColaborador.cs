﻿using Controlsys.ApiSAP.Globalsys.ApiSAPEnviarColaborador;
using Controlsys.Dominio.Audit;
using Controlsys.Infra;
using Controlsys.Repositorio.Audit;
using Controlsys.Repositorio.Parametros;
using Controlsys.Repositorios.Pessoas;
using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace Controlsys.ApiSAP.Models
{
    public class EnviarColaborador : Sincronia
    {
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }        

        public ColaboradorResult[] WebServiceEnviarColaborador(DateTime? periodoColaborador = null)
        {
            ColaboradorResult[] retorno = new ColaboradorResult[0];

            string usuarioApiSAP = System.Configuration.ConfigurationSettings.AppSettings["usuarioApiSAP"];

            try
            {
                UnidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();

                Sincronia sincronia = new Sincronia();
                        
                IRepositorioColaborador repColaborador = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);
                IRepositorioCracha repCracha = Fabrica.Instancia.ObterRepositorio<IRepositorioCracha>(UnidadeTrabalho);
                                
                IRepositorioColaboradorAtualizadoIncluido repAtualizadoIncluido_ = Fabrica.Instancia.ObterRepositorio<IRepositorioColaboradorAtualizadoIncluido>(UnidadeTrabalho);

                List<ColaboradorAtualizadoIncluido> listaColaboradores = new List<ColaboradorAtualizadoIncluido>();
                int qtdRegIntAcesso = 30;

                if (periodoColaborador != null)
                {
                    listaColaboradores = repAtualizadoIncluido_.ObterTodos().Where(x => x.DataRegistro == periodoColaborador).Take(qtdRegIntAcesso).ToList();
                }
                else
                {
                    listaColaboradores = repAtualizadoIncluido_.ObterTodos().Where(x => x.DataEnvio == null).ToList();
                }

                var listaColaboradoresCodigo = listaColaboradores.Select(x => x.CDPapel).ToList();




                List<Dominio.Pessoas.Colaborador> lista = repColaborador.ObterTodos().Where(m => listaColaboradoresCodigo.Contains(m.Codigo)).ToList();

                

                var listaColaborador = lista.Select(lc => new Globalsys.ApiSAPEnviarColaborador.Colaborador
                {
                    Area = lc.Area?.Nome,
                    CD_Papel = lc.Codigo.ToString(),
                    CD_Superior = lc.EmpregadoSAP != null ? lc.EmpregadoSAP.SuperiorImediato?.Codigo.ToString() : null ,
                    Cargo = lc.Cargo?.Nome,
                    Cracha = lc.Codigo > 0 ? repCracha.ObterTodos().FirstOrDefault(x => x.Papel.Codigo == lc.Codigo && x.Ativo)?.Numero.ToString() : null,
                    //Cracha = lc.Codigo > 0 ? repCracha.ObterTodos().FirstOrDefault(x => x.Papel.Codigo == lc.Codigo)?.Numero.ToString() : null,
                    Email = lc.PessoaFisica.Email,
                    ID_Empregado_Sap = lc.EmpregadoSAP?.Codigo.ToString() != null ? lc.EmpregadoSAP?.Codigo.ToString() : "" ,
                    Nome = lc.PessoaFisica.Nome,
                    Status = lc.ObterLogPapel().Status.ToString(),
                    Foto = lc.PessoaFisica.Foto != null ? Convert.ToBase64String(lc.PessoaFisica.Foto) : null
                }).ToArray();

                sincronia.Url = System.Configuration.ConfigurationSettings.AppSettings["EnderecoUrlSapApi"];

                string Chave_EnviarColaborador = System.Configuration.ConfigurationSettings.AppSettings["Chave_EnviarColaborador"];
                Chave_EnviarColaborador = Chave_EnviarColaborador != null ? Chave_EnviarColaborador : "";

                string UserName = System.Configuration.ConfigurationSettings.AppSettings["UserName"];
                string PassWord = System.Configuration.ConfigurationSettings.AppSettings["PassWord"];

                sincronia.Credentials = new NetworkCredential(UserName, PassWord);

                retorno = sincronia.EnviarColaborador(Chave_EnviarColaborador, listaColaborador.ToArray());

                bool resultado = AdicionaResultadoAPISAP(retorno, periodoColaborador);

                return retorno;
            }
            catch (Exception ex)
            {
                IRepositorioLogError repLogError = Fabrica.Instancia.ObterRepositorio<IRepositorioLogError>(UnidadeTrabalho);

                LogError logError = new LogError();
                logError.Message = ex.Message;
                logError.StackTrace = ex.StackTrace;
                logError.Url = "Controlsys.ApiSAP\\ListaColaboradoresAtualizadosControlsys";
                logError.DataRegistro = DateTime.Now;
                logError.UserName = usuarioApiSAP;
                repLogError.Salvar(logError);
                Console.WriteLine(ex.Message);
                return retorno;
                throw;
            }
        }
        
        private bool AdicionaResultadoAPISAP(ColaboradorResult[] retornoSap, DateTime? periodoParametro = null)
        {
            UnidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            IRepositorioColaboradorAtualizadoIncluido repAtualizadoIncluido_ = Fabrica.Instancia.ObterRepositorio<IRepositorioColaboradorAtualizadoIncluido>(UnidadeTrabalho);
            List<ColaboradorAtualizadoIncluido> colaboradorAtualizado_ = new List<ColaboradorAtualizadoIncluido>();

            var listaRetornoSap = retornoSap.ToList();

            foreach (var item in listaRetornoSap)
            {
                if (periodoParametro != null)
                {
                    colaboradorAtualizado_ = repAtualizadoIncluido_.ObterTodos().Where(x => x.CDPapel.Equals(Convert.ToInt32(item.CD_Papel)) && x.DataRegistro == periodoParametro).ToList();
                }
                else
                {
                    colaboradorAtualizado_ = repAtualizadoIncluido_.ObterTodos().Where(x => x.CDPapel.Equals(Convert.ToInt32(item.CD_Papel)) && x.DataEnvio == null).ToList();
                }

                foreach (var itemColaboradores in colaboradorAtualizado_)
                {
                    if (item.Status_Processo.Equals("S"))
                    {
                        itemColaboradores.MensagemRecebida = "Sucesso";
                        itemColaboradores.DataEnvio = DateTime.Now;
                        itemColaboradores.MensagemLog = null;
                    }
                    else if (item.Status_Processo.Equals("I"))
                    {
                        itemColaboradores.MensagemRecebida = "Inválido";
                        itemColaboradores.MensagemLog = item.MSG_Processo;
                    }
                    try
                    {
                        UnidadeTrabalho.BeginTransaction();
                        repAtualizadoIncluido_.Atualizar(itemColaboradores);
                        UnidadeTrabalho.Commit();
                    }
                    catch (Exception e)
                    {
                        UnidadeTrabalho.Rollback();
                        throw e;
                    }
                }
            }
            return true;
        }        
    }
}