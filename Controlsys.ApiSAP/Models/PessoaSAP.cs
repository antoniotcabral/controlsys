﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Controlsys.ApiSAP.Models
{
    public class PessoaSAP
    {
        public virtual long? CodigoSap { get; set; }
        public virtual string CPF { get; set; }
        public virtual string Passaporte { get; set; }
        public virtual string Nome { get; set; }
        public virtual string Apelido { get; set; }
        public virtual long? PIS { get; set; }
        public virtual int GrupoPessoal { get; set; }
        public virtual long? CodigoFornecedor { get; set; }
        public virtual string CNPJFornecedor { get; set; }
        public virtual string NomeFornecedor { get; set; }
        public virtual string Estado { get; set; }
        public virtual string CNH { get; set; }
        public virtual string TipoCNH { get; set; }
        public virtual DateTime? Validade { get; set; }
        public virtual string CodigoSetorCusto { get; set; }
        public virtual string NumeroUnidadeOrganizacional { get; set; }
        public virtual string NomeUnidadeOrganizacional { get; set; }
        public virtual string CodigoPosicao { get; set; }
        public virtual string NomePosicao { get; set; }
        public virtual string Email { get; set; }
        public virtual long? CodigoSuperiorImediato { get; set; }
        public virtual long? CodigoGestorPonto { get; set; }
        public virtual string StatusMensagem { get; set; }
        public virtual string Mensagem { get; set; }
    }
}