﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Globlasys.Controlsys.ApiSAP.Models
{
    [Serializable()]
    public class RetornoRegistraAcessoModelView
    {
        [System.Xml.Serialization.XmlElement("ID_unico_de_fichada")]
        public virtual string ID_unico_de_fichada { get; set; }

        [System.Xml.Serialization.XmlElement("Status_Retorno")]
        public virtual string Status_Retorno { get; set; }

        [System.Xml.Serialization.XmlElement("Mensagem_Retorno")]
        public virtual string Mensagem_Retorno { get; set; }
    }
    
    [Serializable]
    [XmlRoot("ColecaoRetornoRegistraAcessoModelView")]
    [XmlInclude(typeof(RetornoRegistraAcessoModelView))] // include type class Person
    public class ColecaoRetornoRegistraAcessoModelView
    {
        [XmlArray("RetornoRegistraAcessoModelView")]
        [XmlArrayItem("RetornoRegistraAcessoModelView", typeof(RetornoRegistraAcessoModelView))]
        public List<RetornoRegistraAcessoModelView> colRetornoRegistraAcessoModelView { get; set; }

    }
}