﻿using Controlsys.ApiSAP.Globalsys.ApiSAPEnviaAcesso;
using Controlsys.ApiSAP.Models;
using Controlsys.Dominio.Acesso;
using Controlsys.Infra;
using Controlsys.Persistencia;
using Controlsys.Repositorio.Acesso;
using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace Controlsys.ApiSAP.Controllers
{
    public class IntegracaoSAPController : ApiController
    {        
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Mvc.ActionName("RegistraAcesso")]
        public ZCN_F00129_NTEResponse RegistraAcesso()
        {            
            EnviarAcesso enviarAcesso = new EnviarAcesso();          

            return enviarAcesso.ZCN_F00129_NTE();            
        }
    }
}
