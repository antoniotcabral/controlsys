﻿using Controlsys.ApiSAP.Models;
using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Enderecos;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Controlsys.Dominio.Seguranca;
using Controlsys.Infra;
using Controlsys.Repositorio.Acesso;
using Controlsys.Repositorio.Parametros;
using Controlsys.Repositorios.Acesso;
using Controlsys.Repositorios.Empresas;
using Controlsys.Repositorios.Parametros;
using Controlsys.Repositorios.Pessoas;
using Controlsys.Repositorios.Seguranca;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Controlsys.ApiSAP.Controllers
{
    public class NovidadesPessoaSAPController : ApiController
    {
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }
        /*
        // GET: api/NovidadesPessoaSAP/5
        public List<PessoaSAP> Get(int id)
        {
            try
            {
                UnidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();

                IRepositorioColaborador repColaborador = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);
                IRepositorioAlocacaoColaborador repAlocacao = Fabrica.Instancia.ObterRepositorio<IRepositorioAlocacaoColaborador>(UnidadeTrabalho);

                List<Colaborador> listaColaborador = repColaborador.ObterTodos().Where(x => x.Codigo == id).ToList();

                List<PessoaSAP> listaPessoaSAP = listaColaborador.Select(c => new PessoaSAP
                {
                    CodigoSap = c.PapelLogs?.OrderByDescending(pls => pls.Codigo).FirstOrDefault().Papel?.EmpregadoSAP?.Codigo?? (long?)null,
                    CPF = c.PessoaFisica.CPF,
                    Passaporte = c.PessoaFisica.Passaporte?? " ",
                    Nome = c.PessoaFisica.Nome,
                    Apelido = c.PessoaFisica.Apelido,
                    PIS = c.PIS != null ? Convert.ToInt64(c.PIS.Trim()): 0,
                    GrupoPessoal = (Int32)c.GrupoPessoa,
                    CodigoFornecedor = c.Empresa != null ? Convert.ToInt64(c.Empresa.EmpresaSAP) : 0,
                    CNPJFornecedor = c.Empresa.CNPJ,
                    NomeFornecedor = c.Empresa.Nome,
                    Estado = c.PapelLogs.OrderByDescending(pls => pls.Codigo).FirstOrDefault().Status.ObterDescricaoEnum(),
                    CNH = c.PessoaFisica.CNH != null ? c.PessoaFisica.CNH : null,
                    TipoCNH = c.PessoaFisica.CNHCategoria != null ? c.PessoaFisica.CNHCategoria.ObterDescricaoEnum() : null,
                    Validade = c.PessoaFisica.CNHDataValidade != null ? c.PessoaFisica.CNHDataValidade : (DateTime?)null,
                    CodigoSetorCusto = repAlocacao.ObterAlocacao(c.Codigo,true).ObterContratada().Numero,
                    NumeroUnidadeOrganizacional = c.Area != null ? c.Area.Numero : "",
                    NomeUnidadeOrganizacional = c.Area != null ? c.Area.Nome.ToString() : "",
                    CodigoPosicao = c.Cargo.CargoSAP.ToString(),
                    NomePosicao = c.Cargo.Nome,
                    Email = c.PessoaFisica.Email,
                    CodigoSuperiorImediato = c.EmpregadoSAP?.SuperiorImediato != null ? c.EmpregadoSAP?.SuperiorImediato.Codigo : (long?)null,
                    CodigoGestorPonto = c.EmpregadoSAP?.GestorPonto != null ? c.EmpregadoSAP?.GestorPonto.Codigo : (long?)null,
                    StatusMensagem = "S",
                    Mensagem = " "
                }).ToList();
              
                return listaPessoaSAP;
            }
            catch (KeyNotFoundException)
            {
                return null;
            }
        }
        */

        /*
        // POST: api/NovidadesPessoaSAP
        //[System.Web.Http.AcceptVerbs("POST")]
        [System.Web.Http.HttpPost]
        [System.Web.Http.AllowAnonymous]
        [System.Web.Mvc.ActionName("NovidadesPessoaSAP")]
        [System.Web.Http.Route("api/NovidadesPessoaSAP")]
        public List<PessoaSAP> Post([FromBody] List<PessoaSAP> pessoaSAP)//)
        {          
            // List < PessoaSAP > pessoaSAP = new List<PessoaSAP>();
            //HttpRequestMessage request
            string usuarioApiSAP = System.Configuration.ConfigurationSettings.AppSettings["usuarioApiSAP"];

            try
            {
                pessoaSAP = ValidaCamposPessoaSAP(pessoaSAP);

                for (int i = 0; i < pessoaSAP.Count; i++)
                {
                    if (pessoaSAP[i].StatusMensagem == "S")
                    {
                        pessoaSAP[i] = GravarPessoaSAP(pessoaSAP[i]);
                    }                   

                    if (pessoaSAP[i].StatusMensagem == "E")
                    {
                        IRepositorioLogErrorIntegracaoSAP repLogErrorIntegracaoSAP = Fabrica.Instancia.ObterRepositorio<IRepositorioLogErrorIntegracaoSAP>(UnidadeTrabalho);
                        LogErrorIntegracaoSAP logErrorIntegracaoSAP = new LogErrorIntegracaoSAP();
                        logErrorIntegracaoSAP.Message = "CodigoSAP:" + pessoaSAP[i].CodigoSap + " | Nome:" + pessoaSAP[i].Nome + " | cpf:" + pessoaSAP[i].CPF + " | Mensagem:" + pessoaSAP[i].Mensagem;
                        logErrorIntegracaoSAP.Url = "Controlsys.ApiSAP\\NovidadesPessoaSAP";
                        logErrorIntegracaoSAP.DataRegistro = DateTime.Now;
                        logErrorIntegracaoSAP.UserName = usuarioApiSAP;
                        repLogErrorIntegracaoSAP.Salvar(logErrorIntegracaoSAP);
                    }
                }
               
               
                return pessoaSAP;

            }
            catch (Exception ex)
            {
                IRepositorioLogError repLogError = Fabrica.Instancia.ObterRepositorio<IRepositorioLogError>(UnidadeTrabalho);
                LogError logError = new LogError();
                logError.Message = ex.Message;
                logError.StackTrace = ex.StackTrace;
                logError.Url = "Controlsys.ApiSAP\\NovidadesPessoaSAP";
                logError.DataRegistro = DateTime.Now;
                logError.UserName = usuarioApiSAP;
                repLogError.Salvar(logError);
                return pessoaSAP;
            }
        }
        */
        private PessoaSAP GravarPessoaSAP(PessoaSAP pessoaSAP)
        {
            UnidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();

            // Validação - retorna mensagem de falha - validação referencial.            
            List<string> errosNaLinha = new List<string>();

            try
            {
                Colaborador colaborador = RetornarColaborador(pessoaSAP, out errosNaLinha);
                if (colaborador == null) throw new Exception("O colaborador não pode ser nulo.");

                RetornarStatusMensagem(pessoaSAP, errosNaLinha);

                if (pessoaSAP.StatusMensagem == "S")
                {
                    string[] propModificadasColab = new string[3];
                    propModificadasColab[0] = "colaborador";
                    propModificadasColab[1] = "pessoaFisica";
                    propModificadasColab[2] = "CNHDataValidade";

                    string[] propModificadasDadosColab = new string[3];
                    propModificadasDadosColab[0] = "colaborador";
                    propModificadasDadosColab[1] = "DataValidade";
                    propModificadasDadosColab[2] = "CodigoGrupoTrabalho";

                    if (colaborador.Codigo == null || colaborador.Codigo == 0)
                    {
                        Incluir(colaborador, propModificadasColab, pessoaSAP, out errosNaLinha);
                    }
                    else
                    {
                        try
                        {
                            UnidadeTrabalho.BeginTransaction();
                            Alterar(colaborador, propModificadasColab, propModificadasDadosColab, pessoaSAP, out errosNaLinha);
                            UnidadeTrabalho.Commit();
                        }
                        catch (Exception ex)
                        {
                            errosNaLinha.Add("Falha ao gravar PessoaSAP. " + ex);
                            UnidadeTrabalho.Rollback();
                        }
                    }
                }
                RetornarStatusMensagem(pessoaSAP, errosNaLinha);
            }
            catch (Exception ex)
            {
                errosNaLinha.Add("Falha ao gravar PessoaSAP. " + ex);
                RetornarStatusMensagem(pessoaSAP, errosNaLinha);
            }
            RetornarStatusMensagem(pessoaSAP, errosNaLinha);
            return pessoaSAP;
        }

        private Colaborador RetornarColaborador(PessoaSAP pessoaSAP, out List<string> errosNaLinha)
        {
            IRepositorioColaborador repColaborador = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);
            IRepositorio<EmpregadoSAP> repEmpregadoSap = Fabrica.Instancia.ObterRepositorio<IRepositorio<EmpregadoSAP>>(UnidadeTrabalho);
            IRepositorioSetorCusto repSetorCusto = Fabrica.Instancia.ObterRepositorio<IRepositorioSetorCusto>(UnidadeTrabalho);

            errosNaLinha = new List<string>();

            Colaborador colaborador = null;

            try
            {
                Cargo cargo = GravarCargo(pessoaSAP, out errosNaLinha);
                if (cargo == null) throw new Exception("O cargo não pode ser nulo.");

                Area area = GravarArea(pessoaSAP, out errosNaLinha);
                if (area == null) throw new Exception("O área não pode ser nula.");

                Empresa fornecedor = RecupeararEmpresa(pessoaSAP, out errosNaLinha);
                if (fornecedor == null) throw new Exception("O fornecedor não foi localizado.");

                SetorCusto setorCusto = repSetorCusto.ObterTodos().Where(x => x.Ativo == true && x.Numero == pessoaSAP.CodigoSetorCusto).FirstOrDefault();
                if (setorCusto == null) throw new Exception("O setor de custo não foi localizado.");

                colaborador = repColaborador.ObterTodos().Where(x => x.EmpregadoSAP.Codigo == pessoaSAP.CodigoSap && x.PessoaFisica.CPF == pessoaSAP.CPF).OrderByDescending(x => x.Codigo).FirstOrDefault();
                //bool ultimoStatusAtivo = colaborador.ObterLogPapel().Status == StatusPapelLog.Ativo;
                //if (!ultimoStatusAtivo)
                //    colaborador = null;         

                if (colaborador == null)
                {
                    colaborador = new Colaborador();
                    colaborador.PessoaFisica = new PessoaFisica();

                    colaborador.PessoaFisica.Sexo = Sexo.Masculino;
                    colaborador.DataValidade = Convert.ToDateTime("2099-01-01 00:00:00");
                    colaborador.HoristaMensalista = HoristaMensalista.Mensalista;
                    colaborador.MaoDeObra = MaoDeObra.Direta;
                }
                else
                {
                    if (colaborador.EmpregadoSAP.Codigo != pessoaSAP.CodigoSap) throw new Exception("O CodigoSap não pode ser alterado.");

                    if (colaborador.PessoaFisica.CPF != pessoaSAP.CPF) throw new Exception("O CPF é diferente não pode ser alterado. ("
                        + "cod Papel banco:" + colaborador.Codigo.ToString()
                        + " -cpf banco:" + colaborador.PessoaFisica.CPF.ToString()
                        + " -cpf xml:" + pessoaSAP.CPF.ToString()
                        + " -Cod_SAP banco:" + colaborador.EmpregadoSAP.Codigo.ToString()
                        + " -Cod_SAP xml:" + pessoaSAP.CodigoSap + ")");
                }

                /* Passar os valores apenas quando for inserir. */
                if (!(colaborador.Codigo > 0))
                {
                    //CPF
                    colaborador.PessoaFisica.CPF = pessoaSAP.CPF;
                }

                //Passaporte
                colaborador.PessoaFisica.Passaporte = pessoaSAP.Passaporte;
                //Nome
                colaborador.PessoaFisica.Nome = pessoaSAP.Nome;
                //Apelido
                colaborador.PessoaFisica.Apelido = pessoaSAP.Apelido;
                //PIS
                colaborador.PIS = pessoaSAP.PIS.ToString();
                //Grupo de Pessoal
                colaborador.GrupoPessoa = pessoaSAP.GrupoPessoal == 1 ? GrupoPessoa.Proprio : GrupoPessoa.Contratista;
                //CNPJ
                //colaborador.Empresa.CNPJ = pessoaSAP.CNPJFornecedor;
                colaborador.Empresa = fornecedor;
                //Codigo Fornecedor SAP
                colaborador.Empresa.EmpresaSAP = pessoaSAP.CodigoFornecedor;
                //Nome do Fornecedor
                colaborador.Empresa.Nome = pessoaSAP.NomeFornecedor;
                //Estado
                //**** Papel papel = repPapel.SalvarPapelLog(colaborador.InserirPapelLog(DateTime.Now, null, StatusPapelLog.Ativo));
                //CNH
                colaborador.PessoaFisica.CNH = pessoaSAP.CNH;
                //Tipo CNH
                colaborador.PessoaFisica.CNHCategoria = CategoriaCNH.AE;
                //Validade CNH
                colaborador.PessoaFisica.CNHDataValidade = pessoaSAP.Validade;
                //Código do Setor de Custo
                colaborador.SetorCusto = setorCusto;
                //Número da Unidade Organizacional
                colaborador.Area = area;
                //Código da Posição
                //Nome da Unidade Organizacional
                colaborador.Cargo = cargo;
                //Email
                colaborador.PessoaFisica.Email = pessoaSAP.Email;

                //CodigoSap
                colaborador.EmpregadoSAP = new EmpregadoSAP() { Codigo = pessoaSAP.CodigoSap.HasValue ? pessoaSAP.CodigoSap.Value : 0 };

                //CodigoGestorPonto
                colaborador.EmpregadoSAP.GestorPonto = pessoaSAP.CodigoSuperiorImediato > 0 ? repEmpregadoSap.ObterPorId(pessoaSAP.CodigoGestorPonto) : null;
                if (colaborador.EmpregadoSAP.GestorPonto == null)
                    throw new CoreException("Não foi possível encontrar o código do Gestor de Ponto no Controlsys.");
                //CodigoSuperiorImediato
                colaborador.EmpregadoSAP.SuperiorImediato = pessoaSAP.CodigoSuperiorImediato > 0 ? repEmpregadoSap.ObterPorId(pessoaSAP.CodigoSuperiorImediato) : null;
                if (colaborador.EmpregadoSAP.SuperiorImediato == null)
                    throw new CoreException("Não foi possível encontrar o código do Supervisor Imediato no Controlsys.");

                AplicarRegraEmpregadoSap(colaborador);

                RetornarStatusMensagem(pessoaSAP, errosNaLinha);
            }
            catch (Exception ex)
            {
                errosNaLinha.Add("Falha ao gravar Colaborador. " + ex);
                RetornarStatusMensagem(pessoaSAP, errosNaLinha);
            }

            RetornarStatusMensagem(pessoaSAP, errosNaLinha);
            return colaborador;
        }

        private Empresa RecupeararEmpresa(PessoaSAP pessoaSAP, out List<string> errosNaLinha)
        {
            IRepositorioEmpresa repEmpresa = Fabrica.Instancia.ObterRepositorio<IRepositorioEmpresa>(UnidadeTrabalho);
            Empresa fornecedor = repEmpresa.ObterTodos().Where(f => f.CNPJ == pessoaSAP.CNPJFornecedor).FirstOrDefault(); // f.EmpresaSAP == pessoaSAP.CodigoFornecedor &&
            errosNaLinha = new List<string>();
            try
            {
                if (fornecedor == null) throw new CoreException("O Código do fornecedor SAP:" + pessoaSAP.CodigoFornecedor.ToString()
                                                              + " cpf:" + pessoaSAP.CPF.ToString()
                                                              + " não foi localizado. ");
                else
                {
                    if (fornecedor.CNPJ != pessoaSAP.CNPJFornecedor) throw new CoreException("O CNPJ do fornecedor é diferente não pode ser alterado. ("
                                + "cod Fornecedor banco:" + fornecedor.Codigo.ToString()
                                + "CNPJ banco:" + fornecedor.CNPJ
                                + " -CNPJ xml:" + pessoaSAP.CNPJFornecedor
                                + " -Cod_Fornecedor_SAP banco:" + fornecedor.Codigo.ToString()
                                + " -Cod_Fornecedor_SAP xml:" + pessoaSAP.CodigoFornecedor.ToString() + ")");
                }
            }
            catch (Exception ex)
            {
                errosNaLinha.Add("Falha ao recuperar Fornecedor. " + ex);
                RetornarStatusMensagem(pessoaSAP, errosNaLinha);
            }

            return fornecedor;
        }

        private List<PessoaSAP> ValidaCamposPessoaSAP(List<PessoaSAP> pessoaSAP)
        {
            // Valida conteúdo dos campos.            
            List<PessoaSAP> ListaValidadaPessoaSAP = new List<PessoaSAP>();
            List<string> errosNaLinha = new List<string>();
            foreach (var item in pessoaSAP)
            {
                //CodigoSap
                //item.CodigoSap;
                //CPF
                //Passaporte                
                if (string.IsNullOrEmpty(item.CPF) && string.IsNullOrEmpty(item.Passaporte)) errosNaLinha.Add("O CPF e Passaporte não podem ser nulos.");
                //Nome                
                if (string.IsNullOrEmpty(item.Nome)) errosNaLinha.Add("O nome do colaborador não podem ser nulo.");
                //Apelido                
                if (string.IsNullOrEmpty(item.Apelido)) errosNaLinha.Add("O apelido do colaborador não podem ser nulo.");
                //PIS                
                if (string.IsNullOrEmpty(item.PIS.ToString())) errosNaLinha.Add("O PIS não podem ser nulo. Informe o PIS.");
                //Grupo de Pessoal
                if (!(item.GrupoPessoal == 1 || item.GrupoPessoal == 2)) errosNaLinha.Add("O Grupo de Pessoal não podem ser diferete de 1-Proprio ou 2-Contratista. Informe o Grupo de Pessoal 1-Proprio ou 2-Contratista.");
                //Codigo Fornecedor                
                if (string.IsNullOrEmpty(item.CodigoFornecedor.ToString())) errosNaLinha.Add("O código do fornecedor não podem ser nulo.");
                //CNPJ Fornecedor                
                if (string.IsNullOrEmpty(item.CNPJFornecedor)) errosNaLinha.Add("O CNPJ do fornecedor não podem ser nulo.");
                //Nome do Fornecedor                
                if (string.IsNullOrEmpty(item.NomeFornecedor)) errosNaLinha.Add("O nome do fornecedor não podem ser nulo.");
                //Estado
                if (!(item.Estado == "0" || item.Estado == "1")) errosNaLinha.Add("O esta do papel é diferente de: 1 = Ativo e 0 = Inativo.");
                //CNH                
                if (string.IsNullOrEmpty(item.CNH)) errosNaLinha.Add("O CNH não podem ser nulo. Informe CNH.");
                //Tipo CNH                
                if (string.IsNullOrEmpty(CategoriaCNH.AE.ToString())) errosNaLinha.Add("A categoria do CNH não podem ser nulo. Informe a categoria do CNH.");
                //Validade CNH                
                if (item.Validade == null) errosNaLinha.Add("A data de validade do CNH não podem ser nulo. Informe a data de validade do CNH.");
                //Código do Setor de Custo
                //Número da Unidade Organizacional                
                if (string.IsNullOrEmpty(item.NumeroUnidadeOrganizacional)) errosNaLinha.Add("O código da Unidade Organizacional não podem ser nulo.");
                if (string.IsNullOrEmpty(item.NomeUnidadeOrganizacional)) errosNaLinha.Add("O nome da Unidade Organizacional não podem ser nulo.");
                //Código da Posição
                //Nome da Posição                
                if (string.IsNullOrEmpty(item.CodigoPosicao)) errosNaLinha.Add("O código da posição não podem ser nulo.");
                if (string.IsNullOrEmpty(item.NomePosicao)) errosNaLinha.Add("O nome da posição não podem ser nulo.");
                //Email                
                if (string.IsNullOrEmpty(item.Email)) errosNaLinha.Add("O email não podem ser nulo.");
                //CodigoSuperiorImediato                
                if (string.IsNullOrEmpty(item.CodigoSuperiorImediato.ToString())) errosNaLinha.Add("O código do superior imediato não podem ser nulo.");
                //CodigoGestorPonto                
                if (string.IsNullOrEmpty(item.CodigoGestorPonto.ToString())) errosNaLinha.Add("O código do gestor de ponto não podem ser nulo.");

                ListaValidadaPessoaSAP.Add(RetornarStatusMensagem(item, errosNaLinha));
                errosNaLinha = new List<string>();
            }
            return ListaValidadaPessoaSAP;
        }

        private PessoaSAP RetornarStatusMensagem(PessoaSAP pessoaSAP, List<string> errosNaLinha)
        {
            if (errosNaLinha.Any())
            {
                pessoaSAP.Mensagem = string.Join("|", errosNaLinha.ToArray());
                pessoaSAP.StatusMensagem = "E";
                errosNaLinha = new List<string>();
            }
            else
            {
                pessoaSAP.StatusMensagem = "S";
            }
            return pessoaSAP;
        }

        private SetorCusto GravarAlocacaoColaboradorAoSetorCusto(PessoaSAP pessoaSAP, Colaborador colaborador, out List<string> errosNaLinha)
        {
            IRepositorioAlocacaoColaborador repAlocacaoColaborador = Fabrica.Instancia.ObterRepositorio<IRepositorioAlocacaoColaborador>(UnidadeTrabalho);
            IRepositorioSetorCusto repSetorCusto = Fabrica.Instancia.ObterRepositorio<IRepositorioSetorCusto>(UnidadeTrabalho);
            IRepositorioPedidoCompra repPedidoCompra = Fabrica.Instancia.ObterRepositorio<IRepositorioPedidoCompra>(UnidadeTrabalho);
            errosNaLinha = new List<string>();

            AlocacaoColaborador alocCol = repAlocacaoColaborador.ObterAlocacao(colaborador.Codigo);
            SetorCusto setorCusto = repSetorCusto.ObterTodos().Where(x => x.Ativo == true && x.Numero == pessoaSAP.CodigoSetorCusto).FirstOrDefault();

            PedidoCompra pedidoCompra = repPedidoCompra.ObterTodos().Where(x => x.Codigo == 1).FirstOrDefault();

            try
            {
                IQueryable<AlocacaoColaborador> alocColDifEmp = repAlocacaoColaborador.ObterTodos().Where(ac => ac.Ativo && (ac.Papel.Codigo == colaborador.Codigo) && (ac.PedidoCompra.Empresa.Codigo != colaborador.Empresa.Codigo));

                if (alocColDifEmp.Any())
                {
                    throw new CoreException("Não é possível salvar. Empresa selecionada para o Colaborador é diferente da empresa em que o mesmo está alocado.");
                }

                //SetorCusto            
                if (setorCusto == null) throw new Exception("O setor de custo não foi localizado.");

                if (pedidoCompra == null) throw new Exception("O pedido de compra não foi localizado.");

                if (setorCusto != null && (alocCol == null || alocCol.SetorCusto.Numero != setorCusto.Numero))
                {
                    if (alocCol != null)
                    {
                        alocCol.Inativar();
                        UnidadeTrabalho.BeginTransaction();
                        repAlocacaoColaborador.Atualizar(alocCol);
                        UnidadeTrabalho.Commit();
                    }

                    if (setorCusto.Codigo > 0 && setorCusto != null)
                    {
                        alocCol = new AlocacaoColaborador();
                        alocCol.PedidoCompra = pedidoCompra;
                        alocCol.Papel = colaborador;
                        alocCol.SetorCusto = setorCusto;
                        alocCol.Ativar();
                        alocCol.DataRegistro = DateTime.Now;
                        repAlocacaoColaborador.Validar(alocCol, EstadoObjeto.Novo);
                        repAlocacaoColaborador.Salvar(alocCol);
                    }
                }
            }
            catch (Exception ex)
            {
                errosNaLinha.Add("Falha ao gravar Unidade Organizacional. " + ex);
                RetornarStatusMensagem(pessoaSAP, errosNaLinha);
            }
            return setorCusto;
        }

        private PapelLog GravarStatusPapelAlterar(Papel papel, PapelLog papelLog, string opcao, out List<string> errosNaLinha)
        {
            IRepositorioPapel repPapel = Fabrica.Instancia.ObterRepositorio<IRepositorioPapel>(UnidadeTrabalho);
            IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
            IRepositorioSincronizaPessoaAcesso repSincronizaPessoaAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioSincronizaPessoaAcesso>(UnidadeTrabalho);

            IRepositorioGrupoUsuario repGrupoUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoUsuario>(UnidadeTrabalho);

            errosNaLinha = new List<string>();

            try
            {
                Papel papelA = repPapel.ObterPorId(papel.Codigo);
                PapelLog papelLogA = papelA.ObterLogPapel();

                papelA.SobAnalise = papel.SobAnalise;
                papelA.Supervisionado = papel.Supervisionado;
                papelA.Supervisor = papel.Supervisor;

                if (!string.IsNullOrEmpty(papel.SenhaLeitora) && papelA.SenhaLeitora != papel.SenhaLeitora)
                    papelA.SenhaLeitora = papel.SenhaLeitora;

                if (papelLog.Status.ToString() == "BaixaDefinitiva")
                {
                    string userLogado = System.Configuration.ConfigurationSettings.AppSettings["usuarioApiSAP"];
                    Usuario usuarioLogado = repUsuario.ObterPorLogin(userLogado, somenteAtivos: true);
                    if (usuarioLogado != null)
                    {
                        var grupos = usuarioLogado.GruposUsuarios.Select(g => g.Grupo);
                        var acoes = grupos.SelectMany(g => g.Acoes);
                        var ok = false;
                        switch (opcao)
                        {
                            case "v":
                                if (!(acoes.Where(a => a.Url == "/Visitante/ExecutarBDvisitante")).Any()) ok = true; break;
                            case "c":
                                if (!(acoes.Where(a => a.Url == "/Colaborador/ExecutarBDcolaborador")).Any()) ok = true; break;
                            case "p":
                                if (!(acoes.Where(a => a.Url == "/PrestadorServico/ExecutarBDprestador")).Any()) ok = true; break;
                        }
                        if (ok) throw new CoreException("Você não tem permissão para alterar o status para baixa definitiva.");

                        IQueryable<GrupoUsuario> grupousuario = repGrupoUsuario.ObterTodos().Where(p => (p.Usuario.PessoaFisica.Codigo == papelA.PessoaFisica.Codigo));
                        foreach (var item in grupousuario)
                        {
                            item.Inativar();
                        }
                    }
                }

                if (papelLogA.Status != papelLog.Status)
                {
                    papelLog.Papel = papelA;
                    papelLog.DataRegistro = DateTime.Now;
                    repPapel.ValidarPapelLog(papelLogA, papelLog);
                    repPapel.SalvarPapelLog(papelLog);
                    papelA.PapelLogs.Add(papelLog);

                    DateTime? dataInicio = null;

                    if (papelLog.DataInicio != DateTime.Today)
                        dataInicio = papelLog.DataInicio;

                    if (papelLog.Status != StatusPapelLog.Ativo)
                    {
                        Usuario usuario = repUsuario.ObterPorPessoa(papelLog.Papel.PessoaFisica.Codigo);
                        if (usuario != null && papelLog.Status != StatusPapelLog.Ferias)
                        {
                            usuario.Inativar();
                            repUsuario.Atualizar(usuario);
                        }

                        repSincronizaPessoaAcesso.Sincronizar(papelA, TipoSincronizacaoPessoa.Suspender, dataInicio);

                        if (papelLog.Status == StatusPapelLog.BaixaDefinitiva)
                        {
                            repPapel.DarBaixaDefinitiva(papelLog);
                            return null;
                        }
                    }
                    else
                    {
                        repSincronizaPessoaAcesso.Sincronizar(papelA, TipoSincronizacaoPessoa.Conceder, dataInicio);
                    }
                }
                else
                {
                    papelLogA.DataInicio = papelLog.DataInicio;
                    papelLogA.DataFim = papelLog.DataFim;
                    papelLogA.Motivo = papelLog.Motivo;
                }
            }
            catch (Exception ex)
            {
                errosNaLinha.Add("Falha ao gravar Status do Papel. " + ex);
            }

            return papelLog;
        }

        private Area GravarArea(PessoaSAP pessoaSAP, out List<string> errosNaLinha)
        {
            IRepositorioArea repArea = Fabrica.Instancia.ObterRepositorio<IRepositorioArea>(UnidadeTrabalho);
            Area area = repArea.ObterTodos().Where(x => x.Numero == pessoaSAP.NumeroUnidadeOrganizacional).FirstOrDefault();
            errosNaLinha = new List<string>();

            try
            {
                if (area == null)
                {
                    area = new Area();
                    area.Nome = pessoaSAP.NomeUnidadeOrganizacional;
                    area.Numero = pessoaSAP.NumeroUnidadeOrganizacional;
                    area.DataRegistro = DateTime.Now;
                    area.Ativar();
                    repArea.Validar(area, EstadoObjeto.Novo);
                    repArea.Salvar(area);
                }
                else
                {
                    Area areaA = repArea.ObterPorId(area.Codigo);
                    areaA.Numero = pessoaSAP.NumeroUnidadeOrganizacional;
                    areaA.Nome = pessoaSAP.NomeUnidadeOrganizacional;
                    repArea.Validar(areaA, EstadoObjeto.Alterado);
                    repArea.Atualizar(areaA);
                    area = areaA;
                }
            }
            catch (Exception ex)
            {
                errosNaLinha.Add("Falha ao gravar Unidade Organizacional. " + ex);
                RetornarStatusMensagem(pessoaSAP, errosNaLinha);
            }

            return area;
        }

        private Cargo GravarCargo(PessoaSAP pessoaSAP, out List<string> errosNaLinha)
        {
            IRepositorioCargo repCargo = Fabrica.Instancia.ObterRepositorio<IRepositorioCargo>(UnidadeTrabalho);
            Cargo cargo = repCargo.ObterTodos().Where(x => x.CargoSAP == Convert.ToInt64(pessoaSAP.CodigoPosicao)).FirstOrDefault();
            errosNaLinha = new List<string>();

            try
            {
                if (cargo == null)
                {
                    cargo = new Cargo();
                    cargo.Nome = pessoaSAP.NomePosicao;
                    cargo.CargoSAP = Convert.ToInt32(pessoaSAP.CodigoPosicao);
                    cargo.DataRegistro = DateTime.Now;
                    cargo.Ativar();
                    repCargo.Validar(cargo, EstadoObjeto.Novo);
                    repCargo.Salvar(cargo);
                }
                else
                {
                    Cargo cargoA = repCargo.ObterPorId(cargo.Codigo);
                    //cargoA.Descricao = cargo.Descricao;
                    cargoA.Nome = pessoaSAP.NomePosicao;
                    repCargo.Validar(cargoA, EstadoObjeto.Alterado);
                    repCargo.Atualizar(cargoA);
                    cargo = cargoA;
                }
            }
            catch (Exception ex)
            {
                errosNaLinha.Add("Falha ao gravar Posição do colaborador. " + ex);
                RetornarStatusMensagem(pessoaSAP, errosNaLinha);
            }
            return cargo;
        }

        private void AplicarRegraEmpregadoSap(Colaborador colaborador)
        {
            IRepositorio<EmpregadoSAP> repEmpregadoSap = Fabrica.Instancia.ObterRepositorio<IRepositorio<EmpregadoSAP>>(UnidadeTrabalho);

            var empregadoSap = repEmpregadoSap.ObterPorId(colaborador.EmpregadoSAP.Codigo);

            if (colaborador.EmpregadoSAP.GestorPonto != null)
            {
                colaborador.EmpregadoSAP.GestorPonto = colaborador.EmpregadoSAP.GestorPonto != null ? repEmpregadoSap.ObterPorId(colaborador.EmpregadoSAP.GestorPonto.Codigo) : null;
                if (colaborador.EmpregadoSAP.GestorPonto == null)
                    throw new CoreException("Não foi possível encontrar o código do Gestor de Ponto no Controlsys.");
            }

            if (colaborador.EmpregadoSAP.SuperiorImediato != null)
            {
                colaborador.EmpregadoSAP.SuperiorImediato = colaborador.EmpregadoSAP.SuperiorImediato != null ? repEmpregadoSap.ObterPorId(colaborador.EmpregadoSAP.SuperiorImediato.Codigo) : null;
                if (colaborador.EmpregadoSAP.SuperiorImediato == null)
                    throw new CoreException("Não foi possível encontrar o código do Supervisor Imediato no Controlsys.");
            }

            try
            {
                if (empregadoSap == null)
                {
                    empregadoSap = new EmpregadoSAP
                    {
                        Codigo = colaborador.EmpregadoSAP.Codigo,
                        GestorPonto = colaborador.EmpregadoSAP.GestorPonto,
                        SuperiorImediato = colaborador.EmpregadoSAP.SuperiorImediato
                    };

                    repEmpregadoSap.Salvar(empregadoSap);
                }
                else
                {
                    empregadoSap.GestorPonto = colaborador.EmpregadoSAP.GestorPonto;
                    empregadoSap.SuperiorImediato = colaborador.EmpregadoSAP.SuperiorImediato;
                    repEmpregadoSap.Atualizar(empregadoSap);
                }

                colaborador.EmpregadoSAP = empregadoSap;
            }
            catch (Exception ex)
            {
                throw new CoreException("Erro ao tentar aplicar as regras do Empregado SAP: " + ex.Message);
            }
        }

        private Colaborador Alterar(Colaborador colaborador, string[] propModificadasColab, string[] propModificadasDadosColab, PessoaSAP pessoaSAP, out List<string> errosNaLinha)
        {
            errosNaLinha = new List<string>();

            try
            {
                colaborador.Empresa.Nome = Empresa.RetornaApenasNomeEmpresa(colaborador.Empresa.Nome);

                IRepositorioColaborador repColab = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);
                IRepositorioPessoaFisica repPessoaFisica = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaFisica>(UnidadeTrabalho);
                IRepositorioPapel repPapel = Fabrica.Instancia.ObterRepositorio<IRepositorioPapel>(UnidadeTrabalho);
                IRepositorioSincronizaPessoaAcesso repSincronizaPessoaAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioSincronizaPessoaAcesso>(UnidadeTrabalho);
                IRepositorioGrupoAcesso repGrupoAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupoAcesso>(UnidadeTrabalho);
                IRepositorioArea repArea = Fabrica.Instancia.ObterRepositorio<IRepositorioArea>(UnidadeTrabalho);

                //PESSOAFISCA
                PessoaFisica pessoaFisica = colaborador.PessoaFisica;

                pessoaFisica = repPessoaFisica.ObterPorId(pessoaFisica, propModificadasColab);

                if (colaborador.EmpregadoSAP != null && colaborador.EmpregadoSAP.Codigo > 0)
                {
                    AplicarRegraEmpregadoSap(colaborador);
                }

                if (colaborador.Area != null)
                    colaborador.Area = repArea.ObterPorId(colaborador.Area.Codigo);

                repPessoaFisica.Validar(EstadoObjeto.Alterado, pessoaFisica);
                repPessoaFisica.Atualizar(pessoaFisica);

                //COLABORADOR
                colaborador = repColab.ObterPorId(colaborador, propModificadasDadosColab);

                if (propModificadasDadosColab == null)
                    propModificadasDadosColab = new string[1];

                //DATA DE VALIDADE COLABORADOR            
                //if (propModificadasDadosColab.Contains("DataValidade"))
                //{
                //    colaborador = repColab.ObterPorId(colaborador, propModificadasDadosColab);
                //    if (colaborador.DataValidade >= DateTime.Today)
                //        repSincronizaPessoaAcesso.Sincronizar(colaborador, TipoSincronizacaoPessoa.Conceder, null);
                //    else
                //        repSincronizaPessoaAcesso.Sincronizar(colaborador, TipoSincronizacaoPessoa.Suspender, null);
                //}

                repColab.Validar(EstadoObjeto.Alterado, colaborador);

                if (propModificadasColab != null && propModificadasColab.Any(a => a == "CNHDataValidade"))
                {
                    List<Controladora> controladorasCNH = repGrupoAcesso.ObterControladorasCNH(colaborador);

                    if (colaborador.PessoaFisica.CNHDataValidade >= DateTime.Today)
                        repSincronizaPessoaAcesso.Sincronizar(colaborador, TipoSincronizacaoPessoa.Conceder, null, controladorasCNH);
                    else
                        repSincronizaPessoaAcesso.Sincronizar(colaborador, TipoSincronizacaoPessoa.Suspender, null, controladorasCNH);
                }

                RetornarStatusMensagem(pessoaSAP, errosNaLinha);

                PapelLog papelLog = new PapelLog();
                //papelLog.Codigo = 0;
                papelLog.Papel = colaborador.PapelLogs.OrderByDescending(pls => pls.Codigo).FirstOrDefault().Papel;
                papelLog.Status = pessoaSAP.Estado == "1" ? StatusPapelLog.Ativo : StatusPapelLog.Inativacao;
                papelLog.DataInicio = DateTime.Today;
                if (papelLog.Status == StatusPapelLog.Inativacao)
                {
                    papelLog.DataFim = DateTime.Today;
                }

                papelLog = GravarStatusPapelAlterar(papelLog.Papel, papelLog, "c", out errosNaLinha);
                RetornarStatusMensagem(pessoaSAP, errosNaLinha);
            }
            catch (Exception ex)
            {
                errosNaLinha.Add("Falha ao alterar Colaborador. " + ex);
                RetornarStatusMensagem(pessoaSAP, errosNaLinha);
            }

            return colaborador;
        }

        private Colaborador Incluir(Colaborador colaborador, string[] propModificadasPessoa, PessoaSAP pessoaSAP, out List<string> errosNaLinha)
        {
            errosNaLinha = new List<string>();

            colaborador.Empresa.Nome = Empresa.RetornaApenasNomeEmpresa(colaborador.Empresa.Nome);
            IRepositorioGrupo repGrupo = null;
            IRepositorioParametro repParam = null;
            try
            {
                UnidadeTrabalho.BeginTransaction();

                //repositorios
                IRepositorioColaborador repColab = Fabrica.Instancia.ObterRepositorio<IRepositorioColaborador>(UnidadeTrabalho);
                IRepositorioPessoaFisica repPessoaFisica = Fabrica.Instancia.ObterRepositorio<IRepositorioPessoaFisica>(UnidadeTrabalho);
                IRepositorioPapel repPapel = Fabrica.Instancia.ObterRepositorio<IRepositorioPapel>(UnidadeTrabalho);
                IRepositorioSincronizaPessoaAcesso repSincronizaPessoaAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioSincronizaPessoaAcesso>(UnidadeTrabalho);
                IRepositorioEmpresa repEmpresa = Fabrica.Instancia.ObterRepositorio<IRepositorioEmpresa>(UnidadeTrabalho);
                IRepositorioArea repArea = Fabrica.Instancia.ObterRepositorio<IRepositorioArea>(UnidadeTrabalho);
                IRepositorio<EmpregadoSAP> repEmpSap = Fabrica.Instancia.ObterRepositorio<IRepositorio<EmpregadoSAP>>(UnidadeTrabalho);
                repGrupo = Fabrica.Instancia.ObterRepositorio<IRepositorioGrupo>(UnidadeTrabalho);
                repParam = Fabrica.Instancia.ObterRepositorio<IRepositorioParametro>(UnidadeTrabalho);

                if (colaborador.Area != null)
                    colaborador.Area = repArea.ObterPorId(colaborador.Area.Codigo);
                PessoaFisica pessoaFisica = colaborador.PessoaFisica;


                if (colaborador.EmpregadoSAP != null && colaborador.EmpregadoSAP.Codigo > 0)
                {
                    AplicarRegraEmpregadoSap(colaborador);
                }

                colaborador.Empresa = repEmpresa.ObterTodos().FirstOrDefault(e => e.Codigo.Equals(colaborador.Empresa.Codigo) || e.Nome.Equals(colaborador.Empresa.Nome));
                Endereco end = colaborador.PessoaFisica.Endereco;

                if (pessoaFisica.CPF != null)
                    pessoaFisica.CPF = pessoaFisica.CPF.Replace(".", "").Replace("-", "");

                if (!string.IsNullOrEmpty(pessoaFisica.Codigo))
                {
                    pessoaFisica = repPessoaFisica.ObterPorId(pessoaFisica, propModificadasPessoa);
                    repPessoaFisica.Atualizar(pessoaFisica);
                }

                //validações referentes à pessoa
                if (string.IsNullOrEmpty(pessoaFisica.Codigo))
                {
                    repPessoaFisica.Salvar(pessoaFisica, null);
                }

                repColab.Validar(EstadoObjeto.Novo, colaborador);

                //cadastrar papel
                Papel papel = repPapel.SalvarPapelLog(colaborador.InserirPapelLog(DateTime.Now, null, StatusPapelLog.Ativo));

                repColab.Salvar(colaborador);

                UnidadeTrabalho.Commit();

                RetornarStatusMensagem(pessoaSAP, errosNaLinha);
                return colaborador;
            }
            catch (CoreException msg)
            {
                try
                {
                    errosNaLinha.Add("Falha ao incluir Colaborador. " + msg);
                    RetornarStatusMensagem(pessoaSAP, errosNaLinha);

                }
                catch (Exception ex)
                {
                    throw new CoreException(msg.Message, ex);
                }
                finally
                {
                    UnidadeTrabalho.Rollback();
                }
                throw msg;
            }
            finally
            {
                UnidadeTrabalho.Dispose();
            }
        }

    }
}
