﻿using Controlsys.ApiSAP.Globalsys.ApiSAPEnviarColaborador;
using Controlsys.ApiSAP.Models;
using Globalsys;
using Globlasys.Controlsys.ApiSAP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Controlsys.ApiSAP.Controllers
{
    public class ListaColaboradoresAtualizadosControlsysController : ApiController
    {

        #region [COMENTARIO]
        //public IUnidadeTrabalho UnidadeTrabalho { get; set; }
        //[System.Web.Http.AcceptVerbs("GET", "POST")]
        //[System.Web.Http.HttpGet]
        ////[System.Web.Mvc.ActionName("ListaColaboradoresAtualizadosControlsys")]
        //[Route("api/ListaColaboradoresAtualizadosControlsys")]
        //public List<ListaColaboradoresAtualizadosControlsysModelView> ListaColaboradoresAtualizadosControlsys(DateTime? periodo = null)
        //{
        //    ListaColaboradoresAtualizadosControlsys colaboradores = new ListaColaboradoresAtualizadosControlsys();
        //    List<ListaColaboradoresAtualizadosControlsysModelView> listaColaboradoresControlsys = new List<ListaColaboradoresAtualizadosControlsysModelView>();
        //    listaColaboradoresControlsys = colaboradores.EnviaColaboradoresAtualizadosControlsys(periodo);

        //    return listaColaboradoresControlsys;
        //}
        #endregion

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        //[System.Web.Mvc.ActionName("ListaColaboradoresAtualizadosControlsys")]
        [Route("api/ListaColaboradoresAtualizadosControlsys")]
        public ColaboradorResult[] ListaColaboradoresAtualizadosControlsys(string periodo = null)        
        {
            DateTime? periodo_data = null;

            if (periodo != null)
            {
                periodo_data = DateTime.Parse(periodo);
            }

            EnviarColaborador enviarColaborador = new EnviarColaborador();
            

            return enviarColaborador.WebServiceEnviarColaborador(periodo_data);
        }

    }
}
