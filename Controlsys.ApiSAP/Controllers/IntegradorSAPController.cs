﻿using Controlsys.ApiSAP.Models;
using Controlsys.Dominio.Acesso;
using Controlsys.Infra;
using Controlsys.Repositorio.Acesso;
using Controlsys.Repositorio.Parametros;
using Globalsys;
using Globlasys.Controlsys.ApiSAP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Controlsys.ApiSAP.Controllers
{
    public class IntegradorSAPController : ApiController
    {

        public IUnidadeTrabalho UnidadeTrabalho { get; set; }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Mvc.ActionName("RegistraAcesso")]
        public string RegistraAcesso()
        {
            UnidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            IRepositorioControleAcesso repControleAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioControleAcesso>(UnidadeTrabalho);
            IRepositorioCracha repCracha = Fabrica.Instancia.ObterRepositorio<IRepositorioCracha>(UnidadeTrabalho);

            List<ControleAcesso> listaControleAcesso = repControleAcesso.ObterTodos().Where(w => w.DataIntegracao == null && w.Codigo >= 16942 && w.Codigo <= 16942).OrderBy(z => z.DataAcesso).Take(10).ToList();

            var listaRegistraAcessoModelView = listaControleAcesso.Select(a => new RegistraAcessoModelView()
            {
                ID_unico_de_fichada = Convert.ToString(a.Codigo),
                Matricula_de_Personal_SAP = a.Papel != null && a.Papel.EmpregadoSAP != null ? a.Papel.EmpregadoSAP.Codigo : 0,
                Fecha_de_marcaje_leitora = a.DataAcesso.ToString("dd.MM.yyyy"),
                Hora_de_marcaje_leitora = a.DataAcesso.ToString("HH.mm.ss"),
                Clase_hecho_temporal = a.Direcao.ToString(),
                ID_Terminal = " ",
                Indicador_general = " ",
                Clave_Proceso = " ",
                Nombre_de_usuario = a.Papel.IdUsuario,
                Fecha_de_marcaje_registro = a.DataRegistro.ToString("dd.MM.yyyy"),
                Hora_de_marcaje_registro = a.DataRegistro.ToString("HH.mm.ss"),
                Comunicacion_id_numero = a.Papel != null ? repCracha.ObterTodos().Where(x => x.Papel.Codigo == a.Papel.Codigo).FirstOrDefault().Numero : " ",
            }).ToList();

            var colecaoRegistraAcessoModelView = new ColecaoRegistraAcessoModelView { colRegistraAcessoModelView = listaRegistraAcessoModelView };
                       
            var xml = SerializaXML.GetXMLString(colecaoRegistraAcessoModelView);

            var objeto = SerializaXML.GetObjectXML<ColecaoRegistraAcessoModelView>(xml);

            return xml;


            //public void Serialize(XmlWriter xmlWriter, object o, XmlSerializerNamespaces namespaces, string encodingStyle);]

            //var ms = new MemoryStream();

            //xw.Flush();
            //ms.Seek(0, SeekOrigin.Begin);
            //var sr = new StreamReader(ms, Encoding.UTF8);
            //string retorno = "";
            //retorno = sr.ReadToEnd();
            //ms.Dispose();
            ////xw.Dispose();
            //sr.Dispose();
            //HttpContext.Current.Response.BinaryWrite(ms.ToArray());
            //return retorno;

            //MemoryStream ms = new MemoryStream(byteArray);
            // XmlSerializer serialize = new XmlSerializer(ms.GetType());            
            // var retorno = new StringWriter();


            // serialize.Serialize(retorno, listaRegistraAcessoModelView);

            //var str = retorno;

            // return retorno.ToString();                
        }
        /*
        [System.Web.Http.HttpGet]
        public void RetornoRegistraAcessoGet ()
        {
            RetornoRegistraAcesso();
        }

        [System.Web.Http.HttpPost]
        public string RetornoRegistraAcessoPost()
        {
            return RetornoRegistraAcesso();
        }
        */

    }
}
