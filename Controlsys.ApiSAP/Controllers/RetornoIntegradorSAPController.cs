﻿using Controlsys.Dominio.Acesso;
using Controlsys.Infra;
using Controlsys.Repositorio.Acesso;
using Controlsys.Repositorio.Parametros;
using Globalsys;
using Globlasys.Controlsys.ApiSAP.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Xml.Serialization;

namespace Globlasys.Controlsys.ApiSAP.Controllers
{
    public class RetornoIntegradorSAPController : ApiController
    {
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }
        
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [System.Web.Mvc.ActionName("RetornoRegistraAcesso")]
        public String RetornoRegistraAcesso()
        {
            UnidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            IRepositorioControleAcesso repControleAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioControleAcesso>(UnidadeTrabalho);
            IRepositorioCracha repCracha = Fabrica.Instancia.ObterRepositorio<IRepositorioCracha>(UnidadeTrabalho);

            List<ControleAcesso> listaControleAcesso = repControleAcesso.ObterTodos().Where(w => w.DataIntegracao == null && w.Codigo == 24420983).OrderBy(z => z.DataAcesso).Take(10).ToList();

            var listaRegistraAcessoModelView = listaControleAcesso.Select(a => new RetornoRegistraAcessoModelView()
            {
                ID_unico_de_fichada = Convert.ToString(a.Codigo),
                Status_Retorno = "Sucesso",
                Mensagem_Retorno = "Mensagem_Retorno"
            }).ToList();

            var colecaoRetornoRegistraAcessoModelView = new ColecaoRetornoRegistraAcessoModelView { colRetornoRegistraAcessoModelView = listaRegistraAcessoModelView };

            var xml = SerializaXML.GetXMLString(colecaoRetornoRegistraAcessoModelView);

            var objeto = SerializaXML.GetObjectXML<ColecaoRetornoRegistraAcessoModelView>(xml);

            return xml;

            /*
            XmlSerializer serialize = new XmlSerializer(listaRegistraAcessoModelView.GetType());
            var retorno = new StringWriter();

            serialize.Serialize(retorno, listaRegistraAcessoModelView);

            return retorno.ToString();
            */
        }

    }
}
