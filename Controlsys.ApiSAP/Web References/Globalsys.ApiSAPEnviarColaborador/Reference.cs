﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by Microsoft.VSDesigner, Version 4.0.30319.42000.
// 
#pragma warning disable 1591

namespace Controlsys.ApiSAP.Globalsys.ApiSAPEnviarColaborador {
    using System;
    using System.Web.Services;
    using System.Diagnostics;
    using System.Web.Services.Protocols;
    using System.Xml.Serialization;
    using System.ComponentModel;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3761.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="SincroniaSoap", Namespace="http://tempuri.org/")]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(object[]))]
    public partial class Sincronia : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback EnviarObjetoOperationCompleted;
        
        private System.Threading.SendOrPostCallback EnviarColaboradorOperationCompleted;
        
        private System.Threading.SendOrPostCallback validaAlocacoesOperationCompleted;
        
        private System.Threading.SendOrPostCallback enviarAlocacoesOperationCompleted;
        
        private System.Threading.SendOrPostCallback recebeAlocacaoOperationCompleted;
        
        private System.Threading.SendOrPostCallback validaDevolucoesOperationCompleted;
        
        private System.Threading.SendOrPostCallback enviarDevolucoesOperationCompleted;
        
        private System.Threading.SendOrPostCallback recebeDevolucaoOperationCompleted;
        
        private bool useDefaultCredentialsSetExplicitly;
        
        /// <remarks/>
        public Sincronia() {
            this.Url = global::Controlsys.ApiSAP.Properties.Settings.Default.Controlsys_ApiSAP_Globalsys_ApiSAPEnviarColaborador_Sincronia;
            if ((this.IsLocalFileSystemWebService(this.Url) == true)) {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        public new string Url {
            get {
                return base.Url;
            }
            set {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true) 
                            && (this.useDefaultCredentialsSetExplicitly == false)) 
                            && (this.IsLocalFileSystemWebService(value) == false))) {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }
        
        public new bool UseDefaultCredentials {
            get {
                return base.UseDefaultCredentials;
            }
            set {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        /// <remarks/>
        public event EnviarObjetoCompletedEventHandler EnviarObjetoCompleted;
        
        /// <remarks/>
        public event EnviarColaboradorCompletedEventHandler EnviarColaboradorCompleted;
        
        /// <remarks/>
        public event validaAlocacoesCompletedEventHandler validaAlocacoesCompleted;
        
        /// <remarks/>
        public event enviarAlocacoesCompletedEventHandler enviarAlocacoesCompleted;
        
        /// <remarks/>
        public event recebeAlocacaoCompletedEventHandler recebeAlocacaoCompleted;
        
        /// <remarks/>
        public event validaDevolucoesCompletedEventHandler validaDevolucoesCompleted;
        
        /// <remarks/>
        public event enviarDevolucoesCompletedEventHandler enviarDevolucoesCompleted;
        
        /// <remarks/>
        public event recebeDevolucaoCompletedEventHandler recebeDevolucaoCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/EnviarObjeto", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public string EnviarObjeto(string chave, string objeto) {
            object[] results = this.Invoke("EnviarObjeto", new object[] {
                        chave,
                        objeto});
            return ((string)(results[0]));
        }
        
        /// <remarks/>
        public void EnviarObjetoAsync(string chave, string objeto) {
            this.EnviarObjetoAsync(chave, objeto, null);
        }
        
        /// <remarks/>
        public void EnviarObjetoAsync(string chave, string objeto, object userState) {
            if ((this.EnviarObjetoOperationCompleted == null)) {
                this.EnviarObjetoOperationCompleted = new System.Threading.SendOrPostCallback(this.OnEnviarObjetoOperationCompleted);
            }
            this.InvokeAsync("EnviarObjeto", new object[] {
                        chave,
                        objeto}, this.EnviarObjetoOperationCompleted, userState);
        }
        
        private void OnEnviarObjetoOperationCompleted(object arg) {
            if ((this.EnviarObjetoCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.EnviarObjetoCompleted(this, new EnviarObjetoCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/EnviarColaborador", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlArrayItemAttribute(IsNullable=false)]
        public ColaboradorResult[] EnviarColaborador(string Chave, [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable=false)] Colaborador[] Colaboradores) {
            object[] results = this.Invoke("EnviarColaborador", new object[] {
                        Chave,
                        Colaboradores});
            return ((ColaboradorResult[])(results[0]));
        }
        
        /// <remarks/>
        public void EnviarColaboradorAsync(string Chave, Colaborador[] Colaboradores) {
            this.EnviarColaboradorAsync(Chave, Colaboradores, null);
        }
        
        /// <remarks/>
        public void EnviarColaboradorAsync(string Chave, Colaborador[] Colaboradores, object userState) {
            if ((this.EnviarColaboradorOperationCompleted == null)) {
                this.EnviarColaboradorOperationCompleted = new System.Threading.SendOrPostCallback(this.OnEnviarColaboradorOperationCompleted);
            }
            this.InvokeAsync("EnviarColaborador", new object[] {
                        Chave,
                        Colaboradores}, this.EnviarColaboradorOperationCompleted, userState);
        }
        
        private void OnEnviarColaboradorOperationCompleted(object arg) {
            if ((this.EnviarColaboradorCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.EnviarColaboradorCompleted(this, new EnviarColaboradorCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/validaAlocacoes", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public string[] validaAlocacoes(string chave, string[] idEmp, string data, long idCentroLogistico) {
            object[] results = this.Invoke("validaAlocacoes", new object[] {
                        chave,
                        idEmp,
                        data,
                        idCentroLogistico});
            return ((string[])(results[0]));
        }
        
        /// <remarks/>
        public void validaAlocacoesAsync(string chave, string[] idEmp, string data, long idCentroLogistico) {
            this.validaAlocacoesAsync(chave, idEmp, data, idCentroLogistico, null);
        }
        
        /// <remarks/>
        public void validaAlocacoesAsync(string chave, string[] idEmp, string data, long idCentroLogistico, object userState) {
            if ((this.validaAlocacoesOperationCompleted == null)) {
                this.validaAlocacoesOperationCompleted = new System.Threading.SendOrPostCallback(this.OnvalidaAlocacoesOperationCompleted);
            }
            this.InvokeAsync("validaAlocacoes", new object[] {
                        chave,
                        idEmp,
                        data,
                        idCentroLogistico}, this.validaAlocacoesOperationCompleted, userState);
        }
        
        private void OnvalidaAlocacoesOperationCompleted(object arg) {
            if ((this.validaAlocacoesCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.validaAlocacoesCompleted(this, new validaAlocacoesCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/enviarAlocacoes", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public string enviarAlocacoes(string chave, object[] row) {
            object[] results = this.Invoke("enviarAlocacoes", new object[] {
                        chave,
                        row});
            return ((string)(results[0]));
        }
        
        /// <remarks/>
        public void enviarAlocacoesAsync(string chave, object[] row) {
            this.enviarAlocacoesAsync(chave, row, null);
        }
        
        /// <remarks/>
        public void enviarAlocacoesAsync(string chave, object[] row, object userState) {
            if ((this.enviarAlocacoesOperationCompleted == null)) {
                this.enviarAlocacoesOperationCompleted = new System.Threading.SendOrPostCallback(this.OnenviarAlocacoesOperationCompleted);
            }
            this.InvokeAsync("enviarAlocacoes", new object[] {
                        chave,
                        row}, this.enviarAlocacoesOperationCompleted, userState);
        }
        
        private void OnenviarAlocacoesOperationCompleted(object arg) {
            if ((this.enviarAlocacoesCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.enviarAlocacoesCompleted(this, new enviarAlocacoesCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/recebeAlocacao", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public object[] recebeAlocacao(string chave, string idAloc) {
            object[] results = this.Invoke("recebeAlocacao", new object[] {
                        chave,
                        idAloc});
            return ((object[])(results[0]));
        }
        
        /// <remarks/>
        public void recebeAlocacaoAsync(string chave, string idAloc) {
            this.recebeAlocacaoAsync(chave, idAloc, null);
        }
        
        /// <remarks/>
        public void recebeAlocacaoAsync(string chave, string idAloc, object userState) {
            if ((this.recebeAlocacaoOperationCompleted == null)) {
                this.recebeAlocacaoOperationCompleted = new System.Threading.SendOrPostCallback(this.OnrecebeAlocacaoOperationCompleted);
            }
            this.InvokeAsync("recebeAlocacao", new object[] {
                        chave,
                        idAloc}, this.recebeAlocacaoOperationCompleted, userState);
        }
        
        private void OnrecebeAlocacaoOperationCompleted(object arg) {
            if ((this.recebeAlocacaoCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.recebeAlocacaoCompleted(this, new recebeAlocacaoCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/validaDevolucoes", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public string[] validaDevolucoes(string chave, string[] idEmp, string data, long idCentroLogistico) {
            object[] results = this.Invoke("validaDevolucoes", new object[] {
                        chave,
                        idEmp,
                        data,
                        idCentroLogistico});
            return ((string[])(results[0]));
        }
        
        /// <remarks/>
        public void validaDevolucoesAsync(string chave, string[] idEmp, string data, long idCentroLogistico) {
            this.validaDevolucoesAsync(chave, idEmp, data, idCentroLogistico, null);
        }
        
        /// <remarks/>
        public void validaDevolucoesAsync(string chave, string[] idEmp, string data, long idCentroLogistico, object userState) {
            if ((this.validaDevolucoesOperationCompleted == null)) {
                this.validaDevolucoesOperationCompleted = new System.Threading.SendOrPostCallback(this.OnvalidaDevolucoesOperationCompleted);
            }
            this.InvokeAsync("validaDevolucoes", new object[] {
                        chave,
                        idEmp,
                        data,
                        idCentroLogistico}, this.validaDevolucoesOperationCompleted, userState);
        }
        
        private void OnvalidaDevolucoesOperationCompleted(object arg) {
            if ((this.validaDevolucoesCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.validaDevolucoesCompleted(this, new validaDevolucoesCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/enviarDevolucoes", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public string enviarDevolucoes(string chave, Devolucao devolucao) {
            object[] results = this.Invoke("enviarDevolucoes", new object[] {
                        chave,
                        devolucao});
            return ((string)(results[0]));
        }
        
        /// <remarks/>
        public void enviarDevolucoesAsync(string chave, Devolucao devolucao) {
            this.enviarDevolucoesAsync(chave, devolucao, null);
        }
        
        /// <remarks/>
        public void enviarDevolucoesAsync(string chave, Devolucao devolucao, object userState) {
            if ((this.enviarDevolucoesOperationCompleted == null)) {
                this.enviarDevolucoesOperationCompleted = new System.Threading.SendOrPostCallback(this.OnenviarDevolucoesOperationCompleted);
            }
            this.InvokeAsync("enviarDevolucoes", new object[] {
                        chave,
                        devolucao}, this.enviarDevolucoesOperationCompleted, userState);
        }
        
        private void OnenviarDevolucoesOperationCompleted(object arg) {
            if ((this.enviarDevolucoesCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.enviarDevolucoesCompleted(this, new enviarDevolucoesCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/recebeDevolucao", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public Devolucao recebeDevolucao(string chave, string idDev) {
            object[] results = this.Invoke("recebeDevolucao", new object[] {
                        chave,
                        idDev});
            return ((Devolucao)(results[0]));
        }
        
        /// <remarks/>
        public void recebeDevolucaoAsync(string chave, string idDev) {
            this.recebeDevolucaoAsync(chave, idDev, null);
        }
        
        /// <remarks/>
        public void recebeDevolucaoAsync(string chave, string idDev, object userState) {
            if ((this.recebeDevolucaoOperationCompleted == null)) {
                this.recebeDevolucaoOperationCompleted = new System.Threading.SendOrPostCallback(this.OnrecebeDevolucaoOperationCompleted);
            }
            this.InvokeAsync("recebeDevolucao", new object[] {
                        chave,
                        idDev}, this.recebeDevolucaoOperationCompleted, userState);
        }
        
        private void OnrecebeDevolucaoOperationCompleted(object arg) {
            if ((this.recebeDevolucaoCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.recebeDevolucaoCompleted(this, new recebeDevolucaoCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
        
        private bool IsLocalFileSystemWebService(string url) {
            if (((url == null) 
                        || (url == string.Empty))) {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024) 
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0))) {
                return true;
            }
            return false;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3761.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class Colaborador {
        
        private string areaField;
        
        private string cD_PapelField;
        
        private string cD_SuperiorField;
        
        private string cargoField;
        
        private string crachaField;
        
        private string emailField;
        
        private string iD_Empregado_SapField;
        
        private string nomeField;
        
        private string statusField;
        
        private string fotoField;
        
        /// <remarks/>
        public string Area {
            get {
                return this.areaField;
            }
            set {
                this.areaField = value;
            }
        }
        
        /// <remarks/>
        public string CD_Papel {
            get {
                return this.cD_PapelField;
            }
            set {
                this.cD_PapelField = value;
            }
        }
        
        /// <remarks/>
        public string CD_Superior {
            get {
                return this.cD_SuperiorField;
            }
            set {
                this.cD_SuperiorField = value;
            }
        }
        
        /// <remarks/>
        public string Cargo {
            get {
                return this.cargoField;
            }
            set {
                this.cargoField = value;
            }
        }
        
        /// <remarks/>
        public string Cracha {
            get {
                return this.crachaField;
            }
            set {
                this.crachaField = value;
            }
        }
        
        /// <remarks/>
        public string Email {
            get {
                return this.emailField;
            }
            set {
                this.emailField = value;
            }
        }
        
        /// <remarks/>
        public string ID_Empregado_Sap {
            get {
                return this.iD_Empregado_SapField;
            }
            set {
                this.iD_Empregado_SapField = value;
            }
        }
        
        /// <remarks/>
        public string Nome {
            get {
                return this.nomeField;
            }
            set {
                this.nomeField = value;
            }
        }
        
        /// <remarks/>
        public string Status {
            get {
                return this.statusField;
            }
            set {
                this.statusField = value;
            }
        }
        
        /// <remarks/>
        public string Foto {
            get {
                return this.fotoField;
            }
            set {
                this.fotoField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3761.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class Devolucao {
        
        private string idField;
        
        private string dataField;
        
        private string statusEntradaField;
        
        private string estoquistaEntradaField;
        
        private string comentarioField;
        
        /// <remarks/>
        public string id {
            get {
                return this.idField;
            }
            set {
                this.idField = value;
            }
        }
        
        /// <remarks/>
        public string data {
            get {
                return this.dataField;
            }
            set {
                this.dataField = value;
            }
        }
        
        /// <remarks/>
        public string statusEntrada {
            get {
                return this.statusEntradaField;
            }
            set {
                this.statusEntradaField = value;
            }
        }
        
        /// <remarks/>
        public string estoquistaEntrada {
            get {
                return this.estoquistaEntradaField;
            }
            set {
                this.estoquistaEntradaField = value;
            }
        }
        
        /// <remarks/>
        public string comentario {
            get {
                return this.comentarioField;
            }
            set {
                this.comentarioField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3761.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class ColaboradorResult {
        
        private string cD_PapelField;
        
        private string iD_Empregado_SapField;
        
        private string status_ProcessoField;
        
        private string mSG_ProcessoField;
        
        /// <remarks/>
        public string CD_Papel {
            get {
                return this.cD_PapelField;
            }
            set {
                this.cD_PapelField = value;
            }
        }
        
        /// <remarks/>
        public string ID_Empregado_Sap {
            get {
                return this.iD_Empregado_SapField;
            }
            set {
                this.iD_Empregado_SapField = value;
            }
        }
        
        /// <remarks/>
        public string Status_Processo {
            get {
                return this.status_ProcessoField;
            }
            set {
                this.status_ProcessoField = value;
            }
        }
        
        /// <remarks/>
        public string MSG_Processo {
            get {
                return this.mSG_ProcessoField;
            }
            set {
                this.mSG_ProcessoField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3761.0")]
    public delegate void EnviarObjetoCompletedEventHandler(object sender, EnviarObjetoCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3761.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class EnviarObjetoCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal EnviarObjetoCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public string Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3761.0")]
    public delegate void EnviarColaboradorCompletedEventHandler(object sender, EnviarColaboradorCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3761.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class EnviarColaboradorCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal EnviarColaboradorCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public ColaboradorResult[] Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((ColaboradorResult[])(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3761.0")]
    public delegate void validaAlocacoesCompletedEventHandler(object sender, validaAlocacoesCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3761.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class validaAlocacoesCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal validaAlocacoesCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public string[] Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string[])(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3761.0")]
    public delegate void enviarAlocacoesCompletedEventHandler(object sender, enviarAlocacoesCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3761.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class enviarAlocacoesCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal enviarAlocacoesCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public string Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3761.0")]
    public delegate void recebeAlocacaoCompletedEventHandler(object sender, recebeAlocacaoCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3761.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class recebeAlocacaoCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal recebeAlocacaoCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public object[] Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((object[])(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3761.0")]
    public delegate void validaDevolucoesCompletedEventHandler(object sender, validaDevolucoesCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3761.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class validaDevolucoesCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal validaDevolucoesCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public string[] Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string[])(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3761.0")]
    public delegate void enviarDevolucoesCompletedEventHandler(object sender, enviarDevolucoesCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3761.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class enviarDevolucoesCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal enviarDevolucoesCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public string Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3761.0")]
    public delegate void recebeDevolucaoCompletedEventHandler(object sender, recebeDevolucaoCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3761.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class recebeDevolucaoCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal recebeDevolucaoCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public Devolucao Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((Devolucao)(this.results[0]));
            }
        }
    }
}

#pragma warning restore 1591