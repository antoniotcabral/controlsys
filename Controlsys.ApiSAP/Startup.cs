﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;
using Globalsys;
using System.Configuration;
using Controlsys.Dominio.Acesso;
using Controlsys.Repositorio.Audit;
using Hangfire;
using Controlsys.Dominio.Audit;
using Controlsys.Infra;
using Controlsys.Repositorio.Acesso;
using Globlasys.Controlsys.ApiSAP.Models;
using System.Net;
using Controlsys.ApiSAP.Models;
using Hangfire.Storage;

[assembly: OwinStartup(typeof(Controlsys.ApiSAP.Startup))]

namespace Controlsys.ApiSAP
{
    public partial class Startup
    {
        public IUnidadeTrabalho UnidadeTrabalho { get; set; }
        
        private static bool processing = false;
        private static bool processing_IntegraçãoListaColaborador = false;

        public void Configuration(IAppBuilder app)
        {
            GlobalConfiguration.Configuration.UseSqlServerStorage(ConfigurationManager.AppSettings["hangfiredb"]);

            string limpaAgendamentohangfiredb = ConfigurationManager.AppSettings["limpaAgendamentohangfiredb"];

            if (!String.IsNullOrEmpty(limpaAgendamentohangfiredb) && limpaAgendamentohangfiredb == "true")
            {
                using (var connection = JobStorage.Current.GetConnection())
                {
                    foreach (var recurringJob in connection.GetRecurringJobs())
                    {
                        RecurringJob.RemoveIfExists(recurringJob.Id);
                    }
                }
            }

            string hangfire_IntervaloTempo = ConfigurationManager.AppSettings["hangfire_IntervaloTempo"];
            if (String.IsNullOrEmpty(hangfire_IntervaloTempo))
            {
                hangfire_IntervaloTempo = "*/5 * * * *";
            }

            RecurringJob.AddOrUpdate("Integração EnviarAcesso", () => Integracao_EnviarAcesso(), hangfire_IntervaloTempo); // Minuto a minuto

            string hangfire_IntervaloTempoApiSap = ConfigurationManager.AppSettings["hangfire_IntervaloTempoApiSap"];
            if (String.IsNullOrEmpty(hangfire_IntervaloTempoApiSap))
            {
                hangfire_IntervaloTempoApiSap = "0 9 * * *";
            }

            //RecurringJob.AddOrUpdate("Integração EnviarAcesso_Teste", () => Integracao(), "* * * * *"); // Minuto a minuto
            RecurringJob.AddOrUpdate("IntegraçãoListaColaborador ListaColaboradoresAtualizadosControlsys", () => IntegracaoSapColaboradoresAtualizados(), hangfire_IntervaloTempoApiSap);

            app.UseHangfireServer();
            app.UseHangfireDashboard();

             ConfigureAuth(app);
        }

        public void Integracao_EnviarAcesso()
        {
            UnidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();

            DateTime ultimoEnvioLogErro = DateTime.Now.AddMinutes(-180);

            System.Threading.Tasks.Task.Factory.StartNew(() =>
            {                
                try
                {
                    if (!Startup.processing)
                    {
                        Startup.processing = true;
                        try
                        {
                            EnviarAcesso enviarAcesso = new EnviarAcesso();
                            enviarAcesso.ZCN_F00129_NTE();
                        }
                        catch (Exception ex)
                        {
                            IRepositorioLogError repLogError = Fabrica.Instancia.ObterRepositorio<IRepositorioLogError>(UnidadeTrabalho);
                            //System.Diagnostics.Debug.WriteLine("Log: " + Guid.NewGuid().ToString());                        
                            if (ultimoEnvioLogErro.AddMinutes(5) <= DateTime.Now)
                            {
                                LogError logError = new LogError();
                                logError.Message = ex.Message;
                                logError.StackTrace = ex.StackTrace;
                                logError.Url = "Controlsys.ApiSAP\\RegistraAcesso";
                                logError.DataRegistro = DateTime.Now;
                                logError.UserName = System.Configuration.ConfigurationSettings.AppSettings["usuarioApiSAP"];

                                repLogError.Salvar(logError);

                                ultimoEnvioLogErro = DateTime.Now;
                            }
                            Console.WriteLine(ex.Message);
                        }
                    }
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    Startup.processing = false;
                }
            });
        }

        public void IntegracaoSapColaboradoresAtualizados()
        {
            UnidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();

            DateTime ultimoEnvioLogErro = DateTime.Now.AddMinutes(-180);

            System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                try
                {
                    if (!Startup.processing_IntegraçãoListaColaborador)
                    {
                        Startup.processing_IntegraçãoListaColaborador = true;
                        try
                        {
                            EnviarColaborador enviarColaborador = new EnviarColaborador();
                            enviarColaborador.WebServiceEnviarColaborador();
                        }
                        catch (Exception ex)
                        {
                            IRepositorioLogError repLogError = Fabrica.Instancia.ObterRepositorio<IRepositorioLogError>(UnidadeTrabalho);
                            //System.Diagnostics.Debug.WriteLine("Log: " + Guid.NewGuid().ToString());                        
                            if (ultimoEnvioLogErro.AddMinutes(5) <= DateTime.Now)
                            {
                                LogError logError = new LogError();
                                logError.Message = ex.Message;
                                logError.StackTrace = ex.StackTrace;
                                logError.Url = "Controlsys.ApiSAP\\ListaColaboradoresAtualizadosControlsys";
                                logError.DataRegistro = DateTime.Now;
                                logError.UserName = System.Configuration.ConfigurationSettings.AppSettings["usuarioApiSAP"];

                                repLogError.Salvar(logError);

                                ultimoEnvioLogErro = DateTime.Now;
                            }
                            Console.WriteLine(ex.Message);
                        }
                    }
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    Startup.processing_IntegraçãoListaColaborador = false;
                }
            });
        }




        //public void Integracao()
        //{
        //    UnidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
        //    IRepositorioControleAcesso repControleAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioControleAcesso>(UnidadeTrabalho);

        //    ControleAcesso controleAcesso = new ControleAcesso();

        //    controleAcesso = repControleAcesso.ObterTodos().Where(x => x.Codigo == 24420983).FirstOrDefault();
        //    controleAcesso.DataIntegracao = DateTime.Now;
        //    UnidadeTrabalho.BeginTransaction();
        //    repControleAcesso.Atualizar(controleAcesso);
        //    UnidadeTrabalho.Commit();


        //    UnidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();

        //    System.Threading.Tasks.Task.Factory.StartNew(() =>
        //    {
        //        try
        //        {
        //            if (!Startup.processing)
        //            {
        //                Startup.processing = true;

        //                string ENDPOINT_CONTROLSYS = ConfigurationManager.AppSettings["EnderecoWebservice"];

        //                int seq = 0;
        //                DateTime dataHora = DateTime.Now;
        //                DateTime ultimoEnvioLogErro = DateTime.Now;
        //                try
        //                {
        //                    //UnidadeTrabalho.BeginTransaction();

        //                    string URI = ENDPOINT_CONTROLSYS;

        //                    IRepositorioControleAcesso repControleAcessox = Fabrica.Instancia.ObterRepositorio<IRepositorioControleAcesso>(UnidadeTrabalho);

        //                    List<RetornoRegistraAcessoModelView> listaRetorno = new List<RetornoRegistraAcessoModelView>();

        //                    using (WebClient wc = new WebClient())
        //                    {
        //                        string myParameters = "";
        //                        wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
        //                        string HtmlResult = wc.UploadString(URI, myParameters);

        //                        var novoXML = HtmlResult.Replace("\\", "").Replace(@"\\r\\n", "").Replace(@"\<", "<").Replace(@"\>", "");
        //                        novoXML = novoXML.TrimStart('"').TrimEnd('"');

        //                        //var objeto = SerializaXML.GetObjectXML<ColecaoRegistraAcessoModelView>(novoXML);

        //                        var objeto = SerializaXML.GetObjectXML<ColecaoRetornoRegistraAcessoModelView>(novoXML);

        //                        listaRetorno = objeto.colRetornoRegistraAcessoModelView;
        //                    }

        //                    foreach (var item in listaRetorno)
        //                    {
        //                        if (item.Status_Retorno == "Sucesso")
        //                        {
        //                            ControleAcesso controleAcessox = new ControleAcesso();

        //                            controleAcessox = repControleAcesso.ObterTodos().Where(x => x.Codigo == Convert.ToInt32(item.ID_unico_de_fichada)).FirstOrDefault();
        //                            controleAcessox.DataIntegracao = DateTime.Now;
        //                            repControleAcesso.Atualizar(controleAcessox);
        //                        }
        //                        else
        //                        {
        //                            // Grava na tabela de log de erro.
        //                            /**/
        //                            IRepositorioIntegracaoSAP repIntegracaoSAP = Fabrica.Instancia.ObterRepositorio<IRepositorioIntegracaoSAP>(UnidadeTrabalho);
        //                            IntegracaoSAP integracaoSAP = new IntegracaoSAP();
        //                            integracaoSAP.DataIntegracao = DateTime.Now;
        //                            integracaoSAP.Status = 2;
        //                            integracaoSAP.Mensagem = "Teste - Erro RegistraAcesso com a integração SAP";
        //                            integracaoSAP.DataAtualizacao = DateTime.Now;
        //                            integracaoSAP.TipoAtualizacao = 2;
        //                            repIntegracaoSAP.Salvar(integracaoSAP);
        //                            /**/
        //                        }
        //                    }

        //                    //UnidadeTrabalho.Commit();
        //                }
        //                catch (Exception ex)
        //                {
        //                    //UnidadeTrabalho.Rollback();

        //                    IRepositorioLogError repLogError = Fabrica.Instancia.ObterRepositorio<IRepositorioLogError>(UnidadeTrabalho);
        //                    //System.Diagnostics.Debug.WriteLine("Log: " + Guid.NewGuid().ToString());                        
        //                    if (ultimoEnvioLogErro.AddMinutes(10) <= DateTime.Now)
        //                    {
        //                        LogError logError = new LogError();
        //                        logError.Message = ex.Message;
        //                        logError.StackTrace = ex.StackTrace;
        //                        logError.Url = "Controlsys.ApiSAP\\RegistraAcesso";
        //                        logError.DataRegistro = DateTime.Now;
        //                        logError.UserName = System.Configuration.ConfigurationSettings.AppSettings["usuarioWSMI"];

        //                        repLogError.Salvar(logError);

        //                        ultimoEnvioLogErro = DateTime.Now;
        //                    }
        //                    Console.WriteLine(ex.Message);
        //                }
        //            }
        //        }

        //        catch (Exception ex)
        //        {
        //            throw ex;
        //        }
        //        finally
        //        {
        //            Startup.processing = false;
        //        }
        //    });

        //}
    }
}
