﻿using AvigilonDotNet;
using Controlsys.Dominio.Acesso;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Web;

namespace IntegradorAvigilonCC.Models
{
    public class Integrador
    {
        IAvigilonControlCenter controlCenter = null;

        /// <summary>
        /// Logar no AvigilonControl Center e retorn o Network Video Recorder A.K.A. Nvr
        /// </summary>
        /// <returns></returns>
        //public INvr LoginNvr(string urlBase)
        //{
        //    String hostname = "10.10.2.37";
        //    int port = 38880; //38888
        //    String username = "administrator";
        //    String password = "123@Mudar";
        //    //String channelId = "0a14312e3030313838353063376335642e63616d30301a10f355e06e8ced443d908e9fc462336a12221098b315b7044c438a917f8f83c5d19591380a";
        //    const int paramentroEspera = 1000;
        //    const int nvrConnectWait = paramentroEspera; // (seconds)

        //    IDataStore connectedDataStore = null;
        //    // var retorno = "";
        //    //-----Init Control Center

        //    if (!IPAddress.TryParse(hostname, out IPAddress addr))
        //    {
        //        return null;
        //        throw new Exception("IP Address is invalid.");

        //    }
        //    AvigilonSdk sdk = new AvigilonSdk();

        //    SdkInitParams initParams = new SdkInitParams(
        //        AvigilonSdk.MajorVersion,

        //        AvigilonSdk.MinorVersion)
        //    {
        //        //Era False
        //        ServiceMode = true,
        //        AutoDiscoverNvrs = true
        //    };
        //    initParams.SdkPath = urlBase + "packages\\AvigilonSdkDotNet.dll";
        //    controlCenter = sdk.CreateInstance(initParams);

        //    if (controlCenter == null)
        //    {
        //        return null;
        //    }
        //    controlCenter.NvrListAdd += new AvigilonDotNet.UuidHandler﻿(m_sharedAcc_NvrListAdd﻿﻿﻿);

        //    //CameraIP
        //    IPEndPoint endpoint = new IPEndPoint(addr, port);
        //    DateTime waitEnd = DateTime.Now + new TimeSpan(0, 0, nvrConnectWait);

        //    //controlCenter.NvrListAdd += new AvigilonDotN﻿e﻿t.UuidHandler﻿﻿(m_sharedAcc_NvrListAd﻿d﻿﻿﻿﻿﻿﻿);
        //    AvigilonDotNet.AvgError error = controlCenter.AddNvr(endpoint);

        //    //Stopwatch tempo = new Stopwatch();
        //    //tempo.Start();
        //    while (nvr == null)
        //    {
        //        nvr = controlCenter.GetNvr(endpoint.Address);
        //        //if (tempo.ElapsedMilliseconds > paramentroEspera * 10000) return null;
        //    }
        //    //tempo.Stop();
        //    var result = nvr.Login(username, password);
        //    nvr.DeviceListAdd += new AvigilonDotNet.UuidHandler(NVR_DeviceLi﻿stAdd﻿);
        //    return nvr;
        //}

        public static  Bitmap MakeBitmapFromImageRaw(AvigilonDotNet.IFrameImageRaw rawFrame)
        {
            Rectangle imageRect = new Rectangle(0, 0, rawFrame.Size.Width, rawFrame.Size.Height);

            switch (rawFrame.DefaultPixelFormat)
            {
                case AvigilonDotNet.PixelFormat.Gray8:
                    {
                        Bitmap retVal = new Bitmap(
                            rawFrame.Size.Width,
                            rawFrame.Size.Height,
                            System.Drawing.Imaging.PixelFormat.Format8bppIndexed);

                        // Define palette for 8bit grayscale
                        System.Drawing.Imaging.ColorPalette pal = retVal.Palette;
                        for (int ix = 0; ix < pal.Entries.Length; ++ix)
                        {
                            pal.Entries[ix] = System.Drawing.Color.FromArgb(ix, ix, ix);
                        }
                        retVal.Palette = pal;

                        System.Drawing.Imaging.BitmapData bitmapData = retVal.LockBits(
                            imageRect,
                            System.Drawing.Imaging.ImageLockMode.WriteOnly,
                            System.Drawing.Imaging.PixelFormat.Format8bppIndexed);

                        byte[] byteArray = rawFrame.GetAsArray(
                            AvigilonDotNet.PixelFormat.Gray8,
                            (ushort)bitmapData.Stride);

                        System.Runtime.InteropServices.Marshal.Copy(
                            byteArray,
                            0,
                            bitmapData.Scan0,
                            byteArray.Length);
                        retVal.UnlockBits(bitmapData);

                        return retVal;
                    }

                case AvigilonDotNet.PixelFormat.RGB24:
                    {
                        Bitmap retVal = new Bitmap(
                            rawFrame.Size.Width,
                            rawFrame.Size.Height,
                            System.Drawing.Imaging.PixelFormat.Format24bppRgb);

                        System.Drawing.Imaging.BitmapData bitmapData = retVal.LockBits(
                            imageRect,
                            System.Drawing.Imaging.ImageLockMode.WriteOnly,
                            System.Drawing.Imaging.PixelFormat.Format24bppRgb);

                        byte[] byteArray = rawFrame.GetAsArray(
                            AvigilonDotNet.PixelFormat.RGB24,
                            (ushort)bitmapData.Stride);

                        System.Runtime.InteropServices.Marshal.Copy(
                            byteArray,
                            0,
                            bitmapData.Scan0,
                            byteArray.Length);
                        retVal.UnlockBits(bitmapData);

                        return retVal;
                    }

                case AvigilonDotNet.PixelFormat.RGB32:
                    {
                        Bitmap retVal = new Bitmap(
                            rawFrame.Size.Width,
                            rawFrame.Size.Height,
                            System.Drawing.Imaging.PixelFormat.Format32bppRgb);

                        System.Drawing.Imaging.BitmapData bitmapData = retVal.LockBits(
                            imageRect,
                            System.Drawing.Imaging.ImageLockMode.WriteOnly,
                            System.Drawing.Imaging.PixelFormat.Format32bppRgb);

                        byte[] byteArray = rawFrame.GetAsArray(
                            AvigilonDotNet.PixelFormat.RGB32,
                            (ushort)bitmapData.Stride);

                        System.Runtime.InteropServices.Marshal.Copy(
                            byteArray,
                            0,
                            bitmapData.Scan0,
                            byteArray.Length);
                        retVal.UnlockBits(bitmapData);

                        return retVal;
                    }

                default:
                    return null;
            }
        }

        /// <summary>
        /// Valida as placas existentes e compara com a placa do veículo Credenciado
        /// </summary>
        /// <param name="acessoVeiculo"></param>
        private bool ValidaPlaca(AcessoVeiculo acessoVeiculo)
        {
            float precisaoAnterior = 0f;
            float precisaoAtual = 0f;

            for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
            {
                //Verificar precisão das placas
                if (float.Parse(acessoVeiculo.CamerasImagens[i].Camera.Precisao) >= acessoVeiculo.CamerasImagens[i].PrecisaoImage)
                {
                    //Validação da 
                }
                else
                {

                }

                //Verificar se alguma placa passa do 
            }

            return true;
        }

    }
}