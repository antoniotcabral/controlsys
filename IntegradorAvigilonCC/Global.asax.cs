﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Threading;
using Controlsys.Infra;
using Globalsys;
using IntegradorAvigilonCC;
using IntegradorAvigilonCC.Controllers;

//namespace Controlsys.Interfaces
namespace IntegradorAvigilonCC
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            //AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            //WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //AtivadorController ativa = new AtivadorController(Fabrica.Instancia.Obter<IUnidadeTrabalho>());
            //ativa.RegistraLogAvigilon("Iniciando ativar e desativar de controladora automático", DateTime.Now);
            ////Thread th = new Thread(() =>
            ////{
            ////    ativa.AtivarInativaControladora();
            ////});
            ////th.Start();

            //AreaRegistration.RegisterAllAreas();
            //GlobalConfiguration.Configure(WebApiConfig.Register);
            //WebApiConfig.Register(GlobalConfiguration.Configuration);
            //FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            //RouteConfig.RegisterRoutes(RouteTable.Routes);


            //Fabrica.Instancia.Kernel.Bind<IFilterProvider>().To<NinjectFilterAttribute>().InSingletonScope();

            //GlobalConfiguration.Configuration.DependencyResolver = new NinjectDependencyResolver(Fabrica.Instancia.Kernel);

            DependencyResolver.SetResolver(new NinjectDependencyResolver(Fabrica.Instancia.Kernel));


        }
    }
}
