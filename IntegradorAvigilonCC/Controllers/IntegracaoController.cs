﻿using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Pessoas;
using Controlsys.Dominio.CredenciamentoVeiculos;
using Controlsys.Infra;
using Controlsys.Repositorio.Acesso;
using Globalsys;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Controlsys.Repositorio.Parametros;
using System.IO;
using System.Diagnostics;
using Controlsys.Repositorios.Acesso;
using Controlsys.Repositorios.Parametros;
using Controlsys.Repositorio.Audit;
using Controlsys.Dominio.Audit;
using AvigilonDotNet;
using System.Net;
using System.Threading;
using System.Text;
using System.Globalization;
using System.Web.Hosting;
using Controlsys.Dominio.Parametros;
using Controlsys.Repositorios.Pessoas;
using Controlsys.Interfaces.Controllers;
using Globalsys.Exceptions;

namespace IntegradorAvigilonCC.Controllers
{
    public class IntegracaoController :  ApiController
    {
        IUnidadeTrabalho unidade;
        private Bitmap m_bitmap;
        //public static bool SuccessTentativa { get; set; }
        //public static int InfoControladora { get; set; }
        static readonly object GravaTentativa = new object();
        static readonly object LockInfoControladora = new object();
        List<ImagemPlaca> imagemPlacas = new List<ImagemPlaca>();
        DateTime dataRegistro = new DateTime();
        string paramentroUrlCaminhoImg = "";
        List<Camera> cameras = null;
        Bitmap imagem = null;
        string placa = "";
        string placaMaiorPrecisao = "";
        AcessoVeiculo acesso = new AcessoVeiculo();
        IEventDeviceLprPlateFound lpr = null;
        CameraImagem cameraImagem = new CameraImagem();
        List<CameraImagem> LPR = new List<CameraImagem>();
        List<CameraImagem> Detectadas = new List<CameraImagem>();
        //List<CameraBitmap> cameraBitmaps = new List<CameraBitmap>();
        List<bool> statusCameras = null;
        List<string> ImagensGravadas = new List<string>();
        float precisao = 0f;
        float maiorPrecisao = 0f;
        float parametroExpirar = float.MaxValue;
        List<Bitmap> bitmaps = new List<Bitmap>();
        IAvigilonControlCenter controlCenter = null;
        RequestParams m_params = new RequestParams();
        INvr nvr = null;
        string nomeDevice = null;
        ManualResetEvent oSignalEvent = new ManualResetEvent(false);
        int MaxPlateFound = 10;
        int MaxImageFound = int.MinValue;
        int TotalDevices = int.MinValue;
        float parametroPrecisao = 80;
        Camera camera = null;
        string urlBase = null;
        string erro;
        int LprIndex = 0;
        DateTime dateTime = DateTime.UtcNow.AddHours(-3);
        Stopwatch tempo = new Stopwatch();
        int imagemAvigilonAltura = 0;
        int imagemAvigilonLargura = 0;
        int indexLPR = 0;

        /// <summary>
        /// GET: /GrupoAcesso/.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        private IUnidadeTrabalho UnidadeTrabalho { get; set; }

        ///// <summary>
        ///// Contrutor padrão do Integrador
        ///// </summary>
        //public IntegracaoController() 
        //{
          
        //}

        /// <summary>
        /// Retorna o tempo de espera especificado nos paramentros do sistema
        /// </summary>
        /// <param name="tempoEspera"></param>
        /// <returns></returns>
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        private string ObterTempoEspera(int tempoEspera)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(tempoEspera);
        }

        /// <summary>
        /// Verificação da obtenção de acesso do veículo
        /// </summary>
        /// <param name="IdLeitora"></param>
        /// <param name="IpControladora"></param>
        /// <param name="numCracha"></param>
        /// /// <param name="papelPermissao"></param>
        /// <returns></returns>
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        //[System.Web.Http.HttpGet]     
        //[System.Web.Http.Route("/api/ObterPermissaoVeiculo")]
        public async Task<string> ObterPermissaoVeiculo(int IdLeitora, int IpControladora, string numCracha, bool papelPermissao)
        {
            UnidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();

            IRepositorioParametro repositorioParametro = Fabrica.Instancia.ObterRepositorio<IRepositorioParametro>(UnidadeTrabalho);
            string json = null;
            String crachaTrimado = "";

            IRepositorioControladora repControladora = Fabrica.Instancia.ObterRepositorio<IRepositorioControladora>(UnidadeTrabalho);
            Controladora controladora = repControladora.ObterTodos().Where(x => x.Codigo == IpControladora).SingleOrDefault();


            if (controladora.TipoControladora != TipoControladora.Cancela)
            {               
                json = Newtonsoft.Json.JsonConvert.SerializeObject(true);
                return json;
            }
            if (!String.IsNullOrEmpty(numCracha)){
                crachaTrimado =  numCracha.TrimStart(new Char[] { '0' });
            }
            
            try
            {
                //Int32.Parse( ) * 1000
                var tempoValor = repositorioParametro.RecuperarParametro(ParametroSistema.ESPERA_AVIGILON).Valor;
                var less = tempoValor.Replace(",", ".").ToString();
                float tempoFloat = float.Parse(less);
                if (less.Contains("0,") || less.Contains("0.")){
                    parametroExpirar = float.Parse(tempoValor.Replace(",", ".")) * 100;
                }
                else
                {
                    parametroExpirar = float.Parse(tempoValor.Replace(",", ".")) * 1000;
                }
                
                    
                tempo.Start();
                                            

                IRepositorioAcessoVeiculo repositorio = Fabrica.Instancia.ObterRepositorio<IRepositorioAcessoVeiculo>(UnidadeTrabalho); ;

                RegistraLogAvigilon("Inicio do processo na controladora - IdLeitora: " + IdLeitora + ", IdControladora " + IpControladora + ", Numero do Crachá: " + numCracha + "  Permissão do Papel: " + papelPermissao, dateTime);

             

                statusCameras = new List<bool>();
               
            }
            catch (Exception erroInicio)
            {

            }
                //IRepositorioParametro repositorioParametro = Fabrica.Instancia.ObterRepositorio<IRepositorioParametro>(UnidadeTrabalho);
                try
            {
                var altura = int.Parse(repositorioParametro.ObterTodos().Where(p => p.Nome == ParametroSistema.IMAGEM_AVIGILON_ALTURA).SingleOrDefault().Valor);
                var largura = int.Parse(repositorioParametro.ObterTodos().Where(p => p.Nome == ParametroSistema.IMAGEM_AVIGILON_LARGURA).SingleOrDefault().Valor);
                if (largura > 0)
                {
                    imagemAvigilonLargura = largura;
                }

                if (altura > 0)
                {
                    imagemAvigilonAltura = altura;
                }
            }
            catch (Exception eParam)
            {
                RegistraLogAvigilon("Parâmetro de Altura ou Largura da imagem consultada da Avigilon não criado no banco de dados", dateTime);
                imagemAvigilonLargura = 1280;
                imagemAvigilonAltura = 720;
            }
            //Buscar no banco de dados o caminho onde a imagem será gravada no servidor de arquivos
            var path = "";
            //paramentroUrlCaminhoImg = 
            try
            {
               
                RegistraLogAvigilon("Inicio do processo no Integrador", dateTime);               

                //Inteligência para desativar e ativar a validação de veículos
                //Desativação de verificação por parâmetro geral
                IRepositorioParametro repParametro = Fabrica.Instancia.ObterRepositorio<IRepositorioParametro>(UnidadeTrabalho);

                //paramentroUrlCaminhoImg = repParametro.ObterTodos().Where(p => p.Nome == Dominio.Parametros.ParametroSistema.CaminhoBaseImagem).SingleOrDefault().Url;
                //var parametro = repParametro.ObterTodos().Where(p => p.Nome == Dominio.Parametros.ParametroSistema.AtivacaoValidarVeiculo).SingleOrDefault();

                ////Validação ativa == "1"
                ////Validação inativa == "0"
                //if (parametro.Valor != "1")
                //{
                //    //e valor for diferente de 1
                //    json = Newtonsoft.Json.JsonConvert.SerializeObject(true);
                //    return json;
                //}

                //urlBase = repParametro.ObterTodos().Where(p => p.Nome == Dominio.Parametros.ParametroSistema.CaminhoBaseImagem).SingleOrDefault().Url;

                //Tempo de expiração
                //Stopwatch expiracao = new Stopwatch();
                //expiracao.Start();

                

                //IRepositorioControleAcesso repAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioControleAcesso>(UnidadeTrabalho);
                //var acessoPapel = repAcesso.ObterPorId(idAcesso);

               
                
               
                acesso.Controladora = controladora;
                               
                RegistraLogAvigilon("Controladora not null", dateTime);


                IRepositorioLeitora repLeitora = Fabrica.Instancia.ObterRepositorio<IRepositorioLeitora>(UnidadeTrabalho);
                Leitora leitora = repLeitora.ObterTodos().Where(x => x.Codigo == IdLeitora).SingleOrDefault();

                IRepositorioCamera repCamera = Fabrica.Instancia.ObterRepositorio<IRepositorioCamera>(UnidadeTrabalho);

                List<Camera> cameras = repCamera.ObterTodos().Where(c => c.Controladora.Codigo == IpControladora).ToList();

                IRepositorioCracha repositorioCracha = Fabrica.Instancia.ObterRepositorio<IRepositorioCracha>(UnidadeTrabalho);
                var cracha = repositorioCracha.ObterTodos().Where(c => c.Numero == crachaTrimado && c.Ativo == true).SingleOrDefault();

                IRepositorioAcessoVeiculo repAcessoVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioAcessoVeiculo>(UnidadeTrabalho);
               


                
                if (cracha == null)
                {
                    RegistraLogAvigilon("Crachar de número " + numCracha + "tentou acesso estando inativo", dateTime);
                    acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_PAPEL_NAO_AUTORIZADO_CANCELA;
                    LogAcessoVeiculo(acesso, acesso.Controladora, false);
                    json = Newtonsoft.Json.JsonConvert.SerializeObject("False");
                    return json;
                }


                var papel = repositorioCracha.ObterTodos().Where(p => p.Codigo == cracha.Codigo && cracha.Ativo == true).ToList().OrderBy(c => c.DataImpressao).LastOrDefault().Papel;

                acesso.Cracha = cracha;
                acesso.Papel = papel;
                acesso.DataRegistro = DateTime.Now;
                acesso.Leitora = leitora;


                //cameras = null;

                //Não há câmeras vinculadas a controladora
                if (cameras == null || cameras.Count == 0)
                {
                  
                    //RegistraLogAvigilon("Não há câmera vinculada", dateTime);
                    //json = Newtonsoft.Json.JsonConvert.SerializeObject(false);
                    //VerificarCamerasNaoVinculadas(acesso, papelPermissao);
                    ////Não registra imagens
                    //repAcessoVeiculo.Salvar(acesso);
                    //return json;
                    if (papelPermissao)
                    {
                        RegistraLogAvigilon("Não há câmera vinculada", dateTime);
                        json = Newtonsoft.Json.JsonConvert.SerializeObject(true);
                        VerificarCamerasNaoVinculadas(acesso, papelPermissao);
                        //Não registra imagens
                        repAcessoVeiculo.Salvar(acesso);
                        return json;
                    }
                    else
                    {
                        RegistraLogAvigilon("Não há câmera vinculada", dateTime);
                        json = Newtonsoft.Json.JsonConvert.SerializeObject("Câmera(s) Não vinculada(s)");
                        VerificarCamerasNaoVinculadas(acesso, papelPermissao);
                        //Não registra imagens
                        repAcessoVeiculo.Salvar(acesso);
                        return json;
                    }
                }

                //Tem câmera(s) vinculada(s) a controladora porém ela/elas NÃO está/estão ativa(s) no sistema
                if (cameras.Count(c => c.Ativo == false) == cameras.Count())
                {
                    if (papelPermissao)
                    {
                        acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_INATIVA;
                        json = Newtonsoft.Json.JsonConvert.SerializeObject(true);
                    }
                    else
                    {
                        acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_CAMERA_INATIVA;
                        json = Newtonsoft.Json.JsonConvert.SerializeObject("Câmeras não vinculadas");
                    }
                    
                    RegistraLogAvigilon("Câmeras não vinculadas", dateTime);
                    VerificarPermissaoSemLPR(ref acesso, acesso.Controladora, cameras, true, papelPermissao);
                    return json;
                }

                //if (tempo.ElapsedMilliseconds > parametroExpirar)
                //{
                //    acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_TEMPO_RESPOSTA_EXCEDIDO;
                //    LogAcessoVeiculo(acesso, acesso.Controladora, false);
                //    json = Newtonsoft.Json.JsonConvert.SerializeObject(false);
                //    return serializaObjeto(false);
                //}

                //acesso.AcessoPapel = acessoPapel;
                acesso.Leitora = leitora;
                acesso.StatusAcesso = StatusAcesso.OCO_UR_ACESSO_LIBERADO;
                acesso.CamerasImagens = new List<CameraImagem>();

                //if (tempo.ElapsedMilliseconds > parametroExpirar)
                //{
                //    acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_TEMPO_RESPOSTA_EXCEDIDO;
                //    LogAcessoVeiculo(acesso, acesso.Controladora, false);
                //    json = Newtonsoft.Json.JsonConvert.SerializeObject(false);
                //    return serializaObjeto(false);
                //}

                INvr nvr = LoginNvr();
                


                if (nvr == null)
                {
                    RegistraLogAvigilon("sistema Avigilon Off-line", dateTime);
                    acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_ACC_INATIVA_NEGADO;
                    LogAcessoVeiculo(acesso, acesso.Controladora, false);
                    return Newtonsoft.Json.JsonConvert.SerializeObject(false);
                }


                //A partir daqui existem 1 ou mais câmeras vinculadas à controladora
                //if (tempo.ElapsedMilliseconds > parametroExpirar)
                //{
                //    acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_TEMPO_RESPOSTA_EXCEDIDO;
                //    LogAcessoVeiculo(acesso, acesso.Controladora, false);
                //    json = Newtonsoft.Json.JsonConvert.SerializeObject(false);
                //    return serializaObjeto(false);
                //}


                RegistraLogAvigilon("Início consulta imagens", dateTime);                       
                acesso = await ObterImagensAvigilonACC(acesso, cameras, nvr);

               


                //if (acesso == null)
                //{
                //    //RegistraLogAvigilon("acesso não nulo", dateTime);
                //    acesso = new AcessoVeiculo();
                //    acesso.Cracha = cracha;
                //    acesso.Papel = papel;
                //    acesso.DataRegistro = DateTime.Now;
                //    acesso.Controladora = controladora;
                //    acesso.Leitora = leitora;
                //    acesso.StatusAcesso = StatusAcesso.OCO_UR_ACESSO_LIBERADO;
                //    acesso.CamerasImagens = Detectadas;
                //    //RegistraLogAvigilon("Gerou acesso ", dateTime);
                //}

                if (TotalDevices == 0)
                {
                    RegistraLogAvigilon("total devices", dateTime);
                    if (papelPermissao == true)
                    {
                        acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_CAMERA_OFFLINE;
                    }
                    else
                    {
                        acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_CAMERA_OFFLINE;
                    }

                    if (tempo.ElapsedMilliseconds > parametroExpirar)
                    {
                        acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_TEMPO_RESPOSTA_EXCEDIDO;
                        LogAcessoVeiculo(acesso, acesso.Controladora, false);
                        json = Newtonsoft.Json.JsonConvert.SerializeObject(false);
                        return serializaObjeto(false);
                    }
                    RegistraLogAvigilon("Registrando dispositivos inativos", dateTime);
                    var autorizacaoVeiculo = LogCamerasOffline(ref acesso);
                    json = Newtonsoft.Json.JsonConvert.SerializeObject(false);
                    return json;
                }

                if (acesso.Controladora.ValidaOCR == false)
                {
                    if (papelPermissao)
                    {
                        acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_ATIVA_LPR_INATIVO;

                        LogAcessoVeiculo(acesso, acesso.Controladora, true);
                        json = Newtonsoft.Json.JsonConvert.SerializeObject(true);
                        RegistraLogAvigilon("Registrando  acessos, não detectou placas - OCR Inativo", dateTime);
                        return json;
                    }
                    else
                    {
                        acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_CAMERA_ATIVA_LPR_INATIVO;
                        LogAcessoVeiculo(acesso, acesso.Controladora, false);
                        json = Newtonsoft.Json.JsonConvert.SerializeObject(false);
                        RegistraLogAvigilon("Registrando  acessos, não detectou placas", dateTime);
                        return json;
                    }
                }

                if (tempo.ElapsedMilliseconds > parametroExpirar)
                {
                    acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_TEMPO_RESPOSTA_EXCEDIDO;
                    LogAcessoVeiculo(acesso, acesso.Controladora, false);
                    json = Newtonsoft.Json.JsonConvert.SerializeObject(false);
                    return serializaObjeto(false);
                }


                if (acesso.Controladora.ValidaOCR)
                {
                    //Câmeras off-line
                   


                    

                    List<CameraImagem> acimaLPR = new List<CameraImagem>();
                    List<CameraImagem> abaixoLPR = new List<CameraImagem>();
                    var autorizacaoVeiculoGrupoLeitora = false;

                    if (LPR == null || (LPR.Count == 0 && acesso.Controladora.ValidaOCR == true))
                    {
                        //if (tempo.ElapsedMilliseconds > parametroExpirar)
                        //{
                        //    acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_ENCONTRADO_BASE_DADOS;
                        //    LogAcessoVeiculo(acesso, acesso.Controladora, false);
                        //    json = Newtonsoft.Json.JsonConvert.SerializeObject(false);
                        //    return serializaObjeto(false);
                        //}

                        acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_ENCONTRADO_BASE_DADOS;
                        
                        LogAcessoVeiculo(acesso, acesso.Controladora, false);
                        json = Newtonsoft.Json.JsonConvert.SerializeObject(false);
                        RegistraLogAvigilon("Registrando  acessos, não detectou placas", dateTime);
                        return json;
                    }

                   
                   
                    if (!String.IsNullOrEmpty(erro))
                    {
                        if (tempo.ElapsedMilliseconds > parametroExpirar)
                        {
                            acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_TEMPO_RESPOSTA_EXCEDIDO;
                            LogAcessoVeiculo(acesso, acesso.Controladora, false);
                            json = Newtonsoft.Json.JsonConvert.SerializeObject(false);
                            return serializaObjeto(false);
                        }

                        RegistraLogAvigilon("Registrando  acessos veículo erro", dateTime);
                        acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_ERRO;
                        LogAcessoVeiculo(acesso, acesso.Controladora, false);

                        return erro;
                    }

                    IRepositorioVeiculo repVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioVeiculo>(UnidadeTrabalho);
                    var veiculo = repVeiculo.ObterTodos().Where(v => v.Placa.Replace("-", "") == placa).ToList().LastOrDefault();


                    VeiculoCredencialVeiculo veiculoCredencialVeiculo = null;
                    //Veículo não cadastrado
                    if (veiculo == null)
                    {

                        if (tempo.ElapsedMilliseconds > parametroExpirar)
                        {
                            acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_TEMPO_RESPOSTA_EXCEDIDO;
                            LogAcessoVeiculo(acesso, acesso.Controladora, false);
                            json = Newtonsoft.Json.JsonConvert.SerializeObject(false);
                            return serializaObjeto(false);
                        }
                        //Veículo não encontrado no banco de dados
                        acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_ENCONTRADO_BASE_DADOS;
                        LogAcessoVeiculo(acesso, acesso.Controladora, false);
                        json = Newtonsoft.Json.JsonConvert.SerializeObject(false);
                        return json;
                    }
                    else
                    {
                        
                        IRepositorioVeiculoCredencialVeiculo repVeiculoCredencialVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioVeiculoCredencialVeiculo>(UnidadeTrabalho);
                        veiculoCredencialVeiculo = repVeiculoCredencialVeiculo.ObterTodos().Where(vcv => vcv.Veiculo.Codigo == veiculo.Codigo).ToList().LastOrDefault();
                        if (veiculoCredencialVeiculo == null)
                        {
                            //Veículo sem credencial
                            acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_CREDENCIAL_INEXISTENTE;
                            LogAcessoVeiculo(acesso, acesso.Controladora, false);
                            json = Newtonsoft.Json.JsonConvert.SerializeObject(false);
                            return json;
                        }

                        acesso.VeiculoCredencialVeiculo = veiculoCredencialVeiculo;


                        
                        if (veiculoCredencialVeiculo.CredencialVeiculo.Situacoes.LastOrDefault().Status != StatusCredencial.Impressa)
                        {
                            //Credencial não impressa
                            acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_CREDENCIAL_NAO_IMPRESSA;
                            LogAcessoVeiculo(acesso, acesso.Controladora, false);
                            json = Newtonsoft.Json.JsonConvert.SerializeObject(false);
                            return json;
                        }

                        if(veiculoCredencialVeiculo.CredencialVeiculo.DataRegistro > DateTime.Now)
                        {
                            //Credencial fora do período ---> acessando antes de ter permissão
                            acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_DATA_ACESSO_TEMPORARIO_EXPIRADO;
                            LogAcessoVeiculo(acesso, acesso.Controladora, false);
                            json = Newtonsoft.Json.JsonConvert.SerializeObject(false);
                            return json;
                        }
                    
                        if(veiculoCredencialVeiculo.CredencialVeiculo.DataVencimento != null && veiculoCredencialVeiculo.CredencialVeiculo.DataVencimento <= DateTime.Now)
                            {
                                //Credencial Expirada
                                acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_CREDENCIAL_FORA_INTERVALO;
                                LogAcessoVeiculo(acesso, acesso.Controladora, false);
                                json = Newtonsoft.Json.JsonConvert.SerializeObject(false);
                                return json;
                            }
                        

                        
                        

                        IRepositorioAcessoTemporarioVeiculo repAcessotemporarioVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioAcessoTemporarioVeiculo>(UnidadeTrabalho);
                        //IRepositorioLeitora repLeitora = Fabrica.Instancia.ObterRepositorio<IRepositorioLeitora>(UnidadeTrabalho);

                        var acessoTemporarioVeiculo = repAcessotemporarioVeiculo.ObterTodos().Where(atv => atv.VeiculoCredencialVeiculo.Codigo == veiculoCredencialVeiculo.Codigo).ToList();
                        var PermissaoLeitora = false;
                        if(acessoTemporarioVeiculo == null || acessoTemporarioVeiculo.Count == 0)
                        {
                            acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_DATA_ACESSO_TEMPORARIO_EXPIRADO;
                            await Task.Run(() =>
                            {
                                LogAcessoVeiculo(acesso, acesso.Controladora, false);
                            }).ConfigureAwait(false);

                            json = Newtonsoft.Json.JsonConvert.SerializeObject(false);
                            return json;
                        }

                        AcessoTemporarioVeiculo acessoTemporario = null;
                        for (int i = 0; i < acessoTemporarioVeiculo.Count; i++)
                        {
                            if (acessoTemporarioVeiculo[i].Ativo)
                            {
                                for (int ix = 0; ix < acessoTemporarioVeiculo[i].GrupoLeitora.Leitoras.Count; ix++)
                                { 
                                    if(acessoTemporarioVeiculo[i].GrupoLeitora.Leitoras[ix].Leitora.Codigo == leitora.Codigo)
                                    {
                                        PermissaoLeitora = true;
                                        acessoTemporario = acessoTemporarioVeiculo[i];
                                    }
                                }
                            }
                            
                        }    
                        
                        if(acessoTemporario != null)
                        {
                            //Acesso Temporário Expirado?
                            if (acessoTemporario.DataHoraValidadeInicio > DateTime.Now || acessoTemporario.DataHoraValidadeFim < DateTime.Now)
                            {
                                acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_DATA_ACESSO_TEMPORARIO_EXPIRADO;
                                await Task.Run(() =>
                                {
                                    LogAcessoVeiculo(acesso, acesso.Controladora, false);
                                }).ConfigureAwait(false);

                                json = Newtonsoft.Json.JsonConvert.SerializeObject(false);
                                return json;
                            }
                        }

                      
                        if (PermissaoLeitora == false)
                        {
                            //Papel pode ou não ter acesso ao grupo de leitoras, mas veículo não tema cesso
                            acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_GRUPO_LEITORA;
                            await Task.Run(() =>
                            {
                                LogAcessoVeiculo(acesso, acesso.Controladora, false);
                            }).ConfigureAwait(false);

                            json = Newtonsoft.Json.JsonConvert.SerializeObject(false);
                            return json;
                        }

                        
                        //Verificar se a placa está acima do parâmetro do LPR
                        parametroPrecisao = 0f;

                        if (tempo.ElapsedMilliseconds > parametroExpirar)
                        {
                            acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_TEMPO_RESPOSTA_EXCEDIDO;
                            LogAcessoVeiculo(acesso, acesso.Controladora, false);
                            json = Newtonsoft.Json.JsonConvert.SerializeObject(false);
                            return serializaObjeto(false);
                        }
                        //Calculo da maior precisão aferida entre as duas câmeras do sistema para colocar na mensagem do log
                        //Se pelo menos uma placa aferida estiver acima do parametro
                        for (int i = 0; i < LPR.Count; i++)
                        {
                            var precisao = float.Parse(acesso.CamerasImagens.Where(c => c.Camera.Nome == LPR[i].Camera.Nome).Single().Camera.Precisao, CultureInfo.InvariantCulture);
                            if (LPR[i].PrecisaoImage > precisao)
                            {
                                acimaLPR.Add(LPR[i]);
                            }
                            else
                            {
                                abaixoLPR.Add(LPR[i]);
                            }
                        }

                        if (acimaLPR.Count > 0)
                        {
                            var precisaoAnterior = 0f;
                            for (int i = 0; i < acimaLPR.Count; i++)
                            {
                                if (acimaLPR[i].PrecisaoImage > precisaoAnterior)
                                {
                                    precisaoAnterior = acimaLPR[i].PrecisaoImage;
                                    maiorPrecisao = acimaLPR[i].PrecisaoImage;
                                    placaMaiorPrecisao = acimaLPR[i].PlacaVeiculo;
                                    parametroPrecisao = float.Parse(acesso.CamerasImagens.Where(c => c.Camera.Nome == acimaLPR[i].Camera.Nome).Single().Camera.Precisao, CultureInfo.InvariantCulture);
                                }
                            }
                            ControladoraController controladoraController = new ControladoraController(UnidadeTrabalho);
                            if (papelPermissao)
                            {
                                acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_AUTORIZADO_LPR_ATIVO_ACIMA;
                                
                                LogAcessoVeiculo(acesso, acesso.Controladora, true);
                                RegistraLogAvigilon("Registrando  acessos, veículo autorizado", dateTime);
                                json = Newtonsoft.Json.JsonConvert.SerializeObject(true);
                                //controladoraController.AtivarInativarIntegradorAvigilon(acesso.Controladora.Codigo, false);
                                return json;
                            }
                            else
                            {
                                acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_LPR_ATIVO_ACIMA;
                                //controladoraController.AtivarInativarIntegradorAvigilon(acesso.Controladora.Codigo, false);
                                LogAcessoVeiculo(acesso, acesso.Controladora, false);
                                RegistraLogAvigilon("Registrando  acessos, veículo não  autorizado", dateTime);
                                json = Newtonsoft.Json.JsonConvert.SerializeObject(false);
                                //controladoraController.AtivarInativarIntegradorAvigilon(acesso.Controladora.Codigo, false);
                                return json;
                            }                          
                        }
                        else
                        {
                            //Verificar a maior precisão entre as câmeras 
                            var precisaoAnterior = 0f;
                            for (int ix = 0; ix < abaixoLPR.Count; ix++)
                            {
                                if (abaixoLPR[ix].PrecisaoImage > precisaoAnterior)
                                {
                                    precisaoAnterior = abaixoLPR[ix].PrecisaoImage;
                                    maiorPrecisao = abaixoLPR[ix].PrecisaoImage;
                                    placaMaiorPrecisao = abaixoLPR[ix].PlacaVeiculo;
                                    parametroPrecisao = float.Parse(acesso.CamerasImagens.Where(c => c.Camera.Nome == abaixoLPR[ix].Camera.Nome).Single().Camera.Precisao, CultureInfo.InvariantCulture);
                                }
                            }
                            //Pegar a maior precisao e setar no Precisao e Precisao
                            acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_LPR_ATIVO_ABAIXO;
                            LogAcessoVeiculo(acesso, acesso.Controladora, false);
                            json = Newtonsoft.Json.JsonConvert.SerializeObject(false);
                            return json;
                        }
                        //Quando não há câmeras online ou o sistema da Avigilon estejam offline
                    }
                    //Não identificou placas
                }
                else
                {
                    RegistraLogAvigilon("Registrando  acessos, veículo  autorizado, LPR inativo", dateTime);
                    acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_LPR_INATIVO;
                        LogAcessoVeiculo(acesso, acesso.Controladora, true);
                        json = Newtonsoft.Json.JsonConvert.SerializeObject(true);
                        return json;
                }
            }
            catch (Exception e)
            {
                //Log de erro
                //Controladora controladora = await Task.Run(() =>
                //{
                //    IRepositorioControladora repControladora = Fabrica.Instancia.ObterRepositorio<IRepositorioControladora>(UnidadeTrabalho);
                //    return repControladora.ObterTodos().Where(x => x.Codigo == IpControladora).SingleOrDefault();
                //});
                AcessoVeiculo acesso = new AcessoVeiculo();
                acesso.Controladora = controladora;


                IRepositorioCracha repositorioCracha = Fabrica.Instancia.ObterRepositorio<IRepositorioCracha>(UnidadeTrabalho);

                var cracha = repositorioCracha.ObterTodos().Where(c => c.Numero == numCracha && c.Ativo == true).SingleOrDefault();
                acesso.Cracha = cracha;

                acesso.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_ERRO;


                LogAcessoVeiculo(acesso, acesso.Controladora, false);
                if (String.IsNullOrEmpty(erro)){
                    erro = e.Message;
                    RegistraLogAvigilon("Registro Erro Avigilon: " + erro, dateTime);
                   
                    return "erro:" + erro;
                }        

               

              

                LogAcessoVeiculo(acesso, acesso.Controladora, false);
                RegistraLogAvigilon("Registro Erro Avigilon: " + e.Message, dateTime);
                //json = Newtonsoft.Json.JsonConvert.SerializeObject( ;
                //json = Newtonsoft.Json.JsonConvert.SerializeObject(false);
                return "erro:" + erro;
            }
        }

     
        [System.Web.Http.AcceptVerbs("GET", "POST")]        
        public string AtualizarACessoVeiculo(int idAcesso, int numeroCracha)
        {
            UnidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();
            RegistraLogAvigilon("Registro Atualiza, idAcesso: " + idAcesso + " , Cracha " + numeroCracha, DateTime.Now);
            
            
            //RegistraLogAvigilon("Registro Atualiza, idAcesso: " + idAcesso + " , Cracha " + numeroCracha, DateTime.Now);
            ControleAcesso acessoPapel = null;
            List<ControleAcesso> ListAcessoPapel = null;
            if (idAcesso == 0)
            {
                IRepositorioControleAcesso
               repositorioControleAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioControleAcesso>(UnidadeTrabalho);
                var periodo = DateTime.Now.AddSeconds(-40);

               ListAcessoPapel = repositorioControleAcesso.ObterTodos().Where(x => x.Cracha.Numero == numeroCracha.ToString() && x.DataRegistro >= periodo).ToList();

                RegistraLogAvigilon("Registro Acesso " + DateTime.Now.AddSeconds(-30), DateTime.Now);
                if (ListAcessoPapel != null)
                {
                    if(ListAcessoPapel.Count > 0)
                    {
                        acessoPapel = ListAcessoPapel.Last();
                        RegistraLogAvigilon("Registro Acesso " + acessoPapel.Codigo, DateTime.Now);
                    }
                    
                   
                }
                else
                {
                    RegistraLogAvigilon("Acesso Papel null", DateTime.Now);
                }
            }
            else
            {
                IRepositorioControleAcesso
               repositorioControleAcesso = Fabrica.Instancia.ObterRepositorio<IRepositorioControleAcesso>(UnidadeTrabalho);
                acessoPapel = repositorioControleAcesso.ObterTodos().Where(x => x.Codigo == idAcesso).SingleOrDefault();
            }
            
            if(ListAcessoPapel != null)
            {
                try
                {
                    RegistraLogAvigilon("Registro Acesso not null " + acessoPapel != null ? acessoPapel.Codigo.ToString() : "" + "Numero crachá" + numeroCracha, DateTime.Now);

                    //if (!String.IsNullOrEmpty(numeroCracha))
                    //{
                    //    crachaTrimado = numeroCracha.TrimStart(new Char[] { '0' });
                    //}

                    var periodo = DateTime.Now.AddMinutes(-1);

                    IRepositorioAcessoVeiculo
                    repositorioAcessoVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioAcessoVeiculo>(UnidadeTrabalho);
                    List<AcessoVeiculo> listAcessoVeiculos = null;
                    AcessoVeiculo acessoVeiculol = null;
                    listAcessoVeiculos = repositorioAcessoVeiculo.ObterTodos().Where(x => x.Cracha.Numero == numeroCracha.ToString() && x.DataRegistro >= periodo).ToList();

                    if (listAcessoVeiculos == null)
                    {
                        RegistraLogAvigilon("Registro Acesso Veículo Nulo ", DateTime.Now);
                    }
                    else
                    {
                        if (listAcessoVeiculos.Count > 0)
                        {
                            acessoVeiculol = listAcessoVeiculos.Last();
                            RegistraLogAvigilon("Registro Acesso Veículo not null " + acessoPapel.Codigo, DateTime.Now);
                            acessoVeiculol.AcessoPapel = acessoPapel;
                            //&& x.DataRegistro >= DateTime.Now.AddSeconds(-30)
                            //RegistraLogAvigilon("Registro Atualiza, acesso Veículo: "+ acessoVeiculol.Codigo + ", idAcesso: " + idAcesso + " , Cracha " + crachaTrimado, DateTime.Now);

                            repositorioAcessoVeiculo.Atualizar(acessoVeiculol);

                            UnidadeTrabalho.BeginTransaction();
                            UnidadeTrabalho.Commit();
                            UnidadeTrabalho.Dispose();

                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new CoreException("Erro: " + ex.Message);                    
                }
                
            }         
          
            return "ok";

        }

        /// <summary>
        /// Verificação da obtenção de acesso do veículo
        /// </summary>
        /// <param name="IdLeitora"></param>
        /// <param name="IpControladora"></param>
        /// <param name="numCracha"></param>
        /// /// <param name="papelPermissao"></param>
        /// <returns></returns>
        //[System.Web.Http.HttpGet]        
        public void AtivarInativaControladora()
        {
            UnidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();


            while (true)
            {
                IRepositorioControladora repControladora = Fabrica.Instancia.ObterRepositorio<IRepositorioControladora>(UnidadeTrabalho);


                IRepositorioCamera repCamera = Fabrica.Instancia.ObterRepositorio<IRepositorioCamera>(UnidadeTrabalho);

                IRepositorioParametro repositorioParametro = Fabrica.Instancia.ObterRepositorio<IRepositorioParametro>(UnidadeTrabalho);

                List<Controladora> controladoras = repControladora.ObterTodos().Where(x => x.TipoControladora == TipoControladora.Cancela).ToList();

                for (int i = 0; i < controladoras.Count; i++)
                {

                    var controladora = repControladora.ObterTodos().Where(x => x.Codigo == controladoras[i].Codigo).Single();
                    //controladoras[i]
                    var altura = int.Parse(repositorioParametro.ObterTodos().Where(p => p.Nome == ParametroSistema.IMAGEM_AVIGILON_ALTURA).SingleOrDefault().Valor);
                    var largura = int.Parse(repositorioParametro.ObterTodos().Where(p => p.Nome == ParametroSistema.IMAGEM_AVIGILON_LARGURA).SingleOrDefault().Valor);
                    if (largura > 0)
                    {
                        imagemAvigilonLargura = largura;
                    }

                    if (altura > 0)
                    {
                        imagemAvigilonAltura = altura;
                    }


                    List<Camera> cameras = repCamera.ObterTodos().Where(c => c.Controladora.Codigo == controladoras[i].Codigo).ToList();
                    if (cameras == null || cameras.Count == 0)
                    {

                        //Inativa Controladora
                        controladora.Ativo = false;
                        repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                        repControladora.Atualizar(controladora);
                    }


                    if (cameras.Count(c => c.Ativo == false) == cameras.Count())
                    {

                        //Inativa Controladora
                        controladora.Ativo = false;
                        repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                        repControladora.Atualizar(controladora);
                    }


                    INvr nvr = LoginNvr();
                    if (nvr == null)
                    {
                        //Inativa Controladora
                        controladora.Ativo = false;
                        repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                        repControladora.Atualizar(controladora);
                    }
                    acesso = new AcessoVeiculo();
                    acesso.Controladora = controladora;
                    


                    acesso = ObterPlacasAvigilonACC(acesso, cameras, nvr).Result;

                    if (TotalDevices == 0)
                    {
                        //Inativa Controladora
                        controladora.Ativo = false;
                        repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                        repControladora.Atualizar(controladora);
                    }

                    if (controladoras[i].ValidaOCR == false)
                    {
                        //Ativa controladora
                        controladora.Ativo = false;
                        repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                        repControladora.Atualizar(controladora);
                    }

                    if (acesso.Controladora.ValidaOCR)
                    {
                        List<CameraImagem> acimaLPR = new List<CameraImagem>();
                        List<CameraImagem> abaixoLPR = new List<CameraImagem>();
                        var autorizacaoVeiculoGrupoLeitora = false;

                        if (LPR == null || (LPR.Count == 0 && acesso.Controladora.ValidaOCR == true))
                        {
                            // Inativa controladora
                            controladora.Ativo = false;
                            repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                            repControladora.Atualizar(controladora);
                        }



                        if (!String.IsNullOrEmpty(erro))
                        {
                            // Inativa controladora
                            controladora.Ativo = false;
                            repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                            repControladora.Atualizar(controladora);

                        }

                        IRepositorioVeiculo repVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioVeiculo>(UnidadeTrabalho);
                        var veiculo = repVeiculo.ObterTodos().Where(v => v.Placa.Replace("-", "") == placa).FirstOrDefault();


                        VeiculoCredencialVeiculo veiculoCredencialVeiculo = null;
                        //Veículo não cadastrado
                        if (veiculo == null)
                        {
                            // Inativa controladora
                            controladora.Ativo = false;
                            repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                            repControladora.Atualizar(controladora);
                        }
                        else
                        {

                            IRepositorioVeiculoCredencialVeiculo repVeiculoCredencialVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioVeiculoCredencialVeiculo>(UnidadeTrabalho);
                            veiculoCredencialVeiculo = repVeiculoCredencialVeiculo.ObterTodos().Where(vcv => vcv.Veiculo.Codigo == veiculo.Codigo).SingleOrDefault();
                            if (veiculoCredencialVeiculo == null)
                            {
                                //Veículo sem credencial
                                // Inativa controladora
                                controladora.Ativo = false;
                                repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                                repControladora.Atualizar(controladora);
                            }

                            if (veiculoCredencialVeiculo.CredencialVeiculo.Situacoes.LastOrDefault().Status != StatusCredencial.Impressa)
                            {
                                //Credencial não impressa
                                // Inativa controladora
                                controladora.Ativo = false;
                                repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                                repControladora.Atualizar(controladora);
                            }

                            if (veiculoCredencialVeiculo.CredencialVeiculo.DataRegistro > DateTime.Now)
                            {
                                // Inativa controladora                       
                                controladora.Ativo = false;
                                repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                                repControladora.Atualizar(controladora);
                            }

                            if (veiculoCredencialVeiculo.CredencialVeiculo.DataVencimento != null && veiculoCredencialVeiculo.CredencialVeiculo.DataVencimento <= DateTime.Now)
                            {
                                // Inativa controladora
                                controladora.Ativo = false;
                                repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                                repControladora.Atualizar(controladora);
                            }

                            IRepositorioAcessoTemporarioVeiculo repAcessotemporarioVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioAcessoTemporarioVeiculo>(UnidadeTrabalho);
                            //IRepositorioLeitora repLeitora = Fabrica.Instancia.ObterRepositorio<IRepositorioLeitora>(UnidadeTrabalho);

                            var acessoTemporarioVeiculo = repAcessotemporarioVeiculo.ObterTodos().Where(atv => atv.VeiculoCredencialVeiculo.Codigo == veiculoCredencialVeiculo.Codigo).ToList();
                            var PermissaoLeitora = false;
                            if (acessoTemporarioVeiculo == null || acessoTemporarioVeiculo.Count == 0)
                            {
                                // Inativa controladora
                                controladora.Ativo = false;
                                repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                                repControladora.Atualizar(controladora);
                            }

                            IRepositorioLeitora repLeitoras = Fabrica.Instancia.ObterRepositorio<IRepositorioLeitora>(UnidadeTrabalho);
                            List<Leitora> leitoras = repLeitoras.ObterTodos().ToList();

                            AcessoTemporarioVeiculo acessoTemporario = null;
                            for (int ix = 0; ix < acessoTemporarioVeiculo.Count; ix++)
                            {
                                if (acessoTemporarioVeiculo[ix].Ativo)
                                {
                                    for (int ii = 0; ii < acessoTemporarioVeiculo[i].GrupoLeitora.Leitoras.Count; ii++)
                                    {
                                        for (int iix = 0; iix < leitoras.Count; iix++)
                                        {
                                            if (acessoTemporarioVeiculo[ix].GrupoLeitora.Leitoras[ii].Leitora.Codigo == leitoras[iix].Codigo)
                                            {
                                                //Ativa Controladora
                                                controladora.Ativo = true;
                                                repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                                                repControladora.Atualizar(controladora);

                                            }

                                        }

                                    }
                                }
                                else
                                {
                                    //Inativa Controladora
                                    controladora.Ativo = false;
                                    repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                                    repControladora.Atualizar(controladora);
                                }

                            }

                            if (acessoTemporario != null)
                            {
                                //Acesso Temporário Expirado?
                                if (acessoTemporario.DataHoraValidadeInicio > DateTime.Now || acessoTemporario.DataHoraValidadeFim < DateTime.Now)
                                {
                                    //Inativa Acesso
                                    controladora.Ativo = false;
                                    repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                                    repControladora.Atualizar(controladora);
                                }
                            }


                            if (PermissaoLeitora == false)
                            {
                                //Papel pode ou não ter acesso ao grupo de leitoras, mas veículo não tema cesso
                                //Inativa acesso
                                controladora.Ativo = false;
                                repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                                repControladora.Atualizar(controladora);
                            }


                            //Verificar se a placa está acima do parâmetro do LPR
                            parametroPrecisao = 0f;


                            //Calculo da maior precisão aferida entre as duas câmeras do sistema para colocar na mensagem do log
                            //Se pelo menos uma placa aferida estiver acima do parametro
                            for (int xi = 0; xi < LPR.Count; xi++)
                            {
                                var precisao = float.Parse(acesso.CamerasImagens.Where(c => c.Camera.Nome == LPR[xi].Camera.Nome).Single().Camera.Precisao, CultureInfo.InvariantCulture);
                                if (LPR[i].PrecisaoImage > precisao)
                                {
                                    acimaLPR.Add(LPR[xi]);
                                }
                                else
                                {
                                    abaixoLPR.Add(LPR[xi]);
                                }
                            }

                            if (acimaLPR.Count > 0)
                            {
                                controladora.Ativo = true;
                                repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                                repControladora.Atualizar(controladora);
                            }
                            else
                            {
                                //Verificar a maior precisão entre as câmeras 
                                controladora.Ativo = false;
                                repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                                repControladora.Atualizar(controladora);
                            }


                        }
                        //Quando não há câmeras online ou o sistema da Avigilon estejam offline

                        //Não identificou placas
                    }
                    else
                    {

                    }
                }
}
           
            
        }


        /// <summary>
        /// Solicita informações de Cameras, placas e imagens da controladora
        /// </summary>
        /// <param name="acessoVeiculo"></param>
        /// <param name="cameras"></param>
        /// <returns></returns>
        private async Task<AcessoVeiculo> ObterImagensAvigilonACC(AcessoVeiculo acessoVeiculo, List<Camera> cameras, INvr nvr)
        {
            try
            {
                for (int i = 0; i < cameras.Count; i++)
                {
                    camera = cameras[i];
                    // cameras[i].Controladora.ValidaOCR == true
                    if (cameras[i].Ativo)
                    {
                       
                        CameraImagem PlacaPrecisao = null;
                        var acessoImage =  ObterImagemACC(cameras[i], acessoVeiculo, nvr);
                       
                        if (acessoImage.Controladora.ValidaOCR)
                        {
                            
                                CameraImagem PlacaEPrecisao = new CameraImagem(); ;
                                acessoImage = ObterLprAcc(cameras[i], ref acessoVeiculo, ref cameraImagem, nvr);
                                                       
                            ///RegistraLogAvigilon("Placas result", DateTime.Now);
                            Detectadas.Add(new CameraImagem()
                            {
                                AcessoVeiculo = acessoImage,
                                Camera = cameraImagem.Camera,
                                PlacaVeiculo = cameraImagem.PlacaVeiculo,
                                PrecisaoImage = cameraImagem.PrecisaoImage
                            });
                            //RegistraLogAvigilon("Placas result 2", DateTime.Now);
                        }
                        acessoVeiculo = acessoImage;
                    }
                }
                return acessoVeiculo;
                //if (acessoVeiculo.CamerasImagens.Count > 0 || TotalDevices == 0)
                //{

                //    return acessoVeiculo;

                //}
                //else
                //{
                //    //RegistraLogAvigilon("Retorno nulo de método deimagem", DateTime.Now);
                //    return null;
                //}
            }

            catch (Exception exAvigilon)
            {
                Console.WriteLine(exAvigilon.Message);
                RegistraLogAvigilon(exAvigilon.Message, DateTime.Now);
                return null;
            }

        }

        /// <summary>
        /// Solicita informações de Cameras, placas e imagens da controladora
        /// </summary>
        /// <param name="acessoVeiculo"></param>
        /// <param name="cameras"></param>
        /// <returns></returns>
        private async Task<AcessoVeiculo> ObterPlacasAvigilonACC(AcessoVeiculo acessoVeiculo, List<Camera> cameras, INvr nvr)
        {
            try
            {
                for (int i = 0; i < cameras.Count; i++)
                {
                    camera = cameras[i];

                    if (cameras[i].Ativo && cameras[i].Controladora.ValidaOCR == true)
                    {

                        if (acessoVeiculo.Controladora.ValidaOCR)
                        {

                            CameraImagem PlacaEPrecisao = new CameraImagem(); ;
                            acessoVeiculo = ObterLprAcc(cameras[i], ref acessoVeiculo, ref cameraImagem, nvr);

                            if (tempo.ElapsedMilliseconds > parametroExpirar)
                            {
                                return acessoVeiculo;
                            }
                            ///RegistraLogAvigilon("Placas result", DateTime.Now);
                            Detectadas.Add(new CameraImagem()
                            {
                                AcessoVeiculo = acessoVeiculo,
                                Camera = cameraImagem.Camera,
                                PlacaVeiculo = cameraImagem.PlacaVeiculo,
                                PrecisaoImage = cameraImagem.PrecisaoImage
                            });
                            //RegistraLogAvigilon("Placas result 2", DateTime.Now);
                        }
                    }
                }
                return acessoVeiculo;
            }

            catch (Exception exAvigilon)
            {
                Console.WriteLine(exAvigilon.Message);
                RegistraLogAvigilon(exAvigilon.Message, DateTime.Now);
                return null;
            }

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="controladora"></param>
        /// <param name="papel"></param>
        private void ObterLprAvigilonACC(Controladora controladora, Papel papel, ref CameraImagem cameraImagem, Camera camera)
        {
            //      ObterLprAcc(camera, ref acesso, controladora, "", cameraImagem);

        }

        /// <summary>
        /// Seta a  acesso baseado no grupo de leitoras
        /// </summary>
        /// <param name="acessoVeiculo"></param>
        /// <returns></returns>
        private bool SetAcessoVeiculo(AcessoVeiculo acessoVeiculo)
        {

            //Log de acesso


            //Valida acesso
            return true;
        }

        /// <summary>
        /// //Verifica a permissão baseado no LPR e faz o cadastro da tentativa de acesso
        /// </summary>
        /// <param name="acessoVeiculo"></param>
        /// /// <param name="indexCameraImagem"></param>
        /// <param name="controladora"></param>
        /// <returns></returns>
        private bool LogPermissaoLPR(
            AcessoVeiculo acessoVeiculo, int indexCameraImagem,
            Controladora controladora)
        {
            var parametroPrecisao = 100;

            //Verifica se o Veículo tem acesso ao GrupoLeitora que está tentando acessar
            //-------------------------------------------------------------------------// 

            var autorizacaoGrupoLeitora = ValidaPlaca(acessoVeiculo);

            if (!autorizacaoGrupoLeitora)
            {
                acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_GRUPO_LEITORA;
                acessoVeiculo.StatusAutorizacao = false;
            }
            else if (acessoVeiculo.CamerasImagens[indexCameraImagem].PrecisaoImage >= parametroPrecisao)
            {
                if (autorizacaoGrupoLeitora)
                {
                    //Permitido o acesso ao local, LPR ativo e acima do parametro do sistema
                    acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_AUTORIZADO_LPR_ATIVO_ACIMA;
                    acessoVeiculo.StatusAutorizacao = true;
                }
                else
                {
                    //Permitido o acesso ao local, LPR ativo e acima do parametro do sistema
                    acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_LPR_ATIVO_ACIMA;
                    acessoVeiculo.StatusAutorizacao = false;
                }
            }
            else
            {
                acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_LPR_ATIVO_ABAIXO;
                acessoVeiculo.StatusAutorizacao = false;
            }

            Task.Run(() =>
            {
                LogAcessoVeiculo(acessoVeiculo, controladora, autorizacaoGrupoLeitora);
            }).ConfigureAwait(false);


            return autorizacaoGrupoLeitora;
        }

        /// <summary>
        /// //Verifica a permissão baseado em câmera Inativas e faz o cadastro da tentativa de acesso
        /// </summary>
        /// <param name="acessoVeiculo"></param>
        ///<param name="papelPermissao"></param>
        /// <returns></returns>
        private bool VerificarCamerasNaoVinculadas(AcessoVeiculo acessoVeiculo, bool papelPermissao)
        {
            //Câmeras não vinculadas a controladora
            if (papelPermissao)
            {
                var permissaoAcesso = true;
                acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERAS_NAO_VINCULADAS;

                LogAcessoVeiculo(acessoVeiculo, acessoVeiculo.Controladora, permissaoAcesso);
                return false;
            }
            else
            {
                var permissaoAcesso = false;
                acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_CAMERAS_NAO_VINCULADAS;

                LogAcessoVeiculo(acessoVeiculo, acessoVeiculo.Controladora, permissaoAcesso);
                return false;
            }
        }

        /// <summary>
        /// //Verifica a permissão baseado em câmeras Vinculadas Ativas ou Inativas e faz o cadastro da tentativa de acesso
        /// </summary>
        /// <param name="acessoVeiculo"></param>
        /// <param name="controladora"></param>
        ///<param name="cameras"></param>
        ///<param name="lpr"></param>
        /// <returns></returns>
        private bool VerificarPermissaoSemLPR(
            ref AcessoVeiculo acessoVeiculo,
            Controladora controladora, List<Camera> cameras, bool lpr, bool permissaoAcesso)
        {

            //Somente câmeras inativas
            if (cameras != null && cameras.Count > 0 && cameras.Count(c => c.Ativo == false) == cameras.Count)
            {
                
                if (permissaoAcesso)
                {
                    acessoVeiculo.StatusAutorizacao = true;
                    acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_INATIVA;
                    LogAcessoVeiculo(acessoVeiculo, controladora, permissaoAcesso);
                    return true;
                }
                else
                {
                    acessoVeiculo.StatusAutorizacao = false;
                    acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_CAMERA_INATIVA;
                    LogAcessoVeiculo(acessoVeiculo, controladora, permissaoAcesso);
                    return false;
                }
            }

            //Há uma ou mais câmeras ativa(s)
            if (cameras != null && cameras.Count > 0)
            {
                if (acessoVeiculo.Controladora.ValidaOCR)
                {
                    //LPR Ativo, cameras ativas
                    if (cameras != null && cameras.Count(c => c.Ativo) > 0)
                    {
                        acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_INATIVA;
                        acessoVeiculo.StatusAutorizacao = true;
                        LogAcessoVeiculo(acessoVeiculo, acessoVeiculo.Controladora, true);
                        return true;
                    }
                    //LPR Ativo, camneras Inativas
                    else
                    {
                        acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_CAMERA_INATIVA;
                        acessoVeiculo.StatusAutorizacao = false;
                        LogAcessoVeiculo(acessoVeiculo, acessoVeiculo.Controladora, false);
                        return false;
                    }

                }
                else
                {
                    //LPR Inativo, cameras Ativas
                    if (permissaoAcesso)
                    {
                        acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_ATIVA_LPR_INATIVO;
                        acessoVeiculo.StatusAutorizacao = true;
                        LogAcessoVeiculo(acessoVeiculo, controladora, true);
                        return true;
                    }
                    //LPR InAtivo, cameras Ativas
                    else
                    {
                        acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_CAMERA_ATIVA_LPR_INATIVO;
                        acessoVeiculo.StatusAutorizacao = false;
                        LogAcessoVeiculo(acessoVeiculo, controladora, false);
                        return false;

                    }
                }
            }
            else
            {
                //camerasOff
                if (permissaoAcesso)
                {
                    acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_OFFLINE;
                    acessoVeiculo.StatusAutorizacao = true;
                    LogAcessoVeiculo(acessoVeiculo, controladora, true);
                    return true;
                }
                else
                {
                    acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_CAMERA_OFFLINE;
                    acessoVeiculo.StatusAutorizacao = true;
                    LogAcessoVeiculo(acessoVeiculo, controladora, true);
                    return false;
                }
            }
        }

        /// <summary>
        /// Log de tentativa de acesso onde as câmeras estão offline
        /// </summary>
        /// <param name="acessoVeiculo"></param>
        /// <param name="controladora"></param>
        ///<param name="cameras"></param>
        ///<param name="lpr"></param>
        /// <returns></returns>
        private bool LogCamerasOffline(
            ref AcessoVeiculo acessoVeiculo)
        {
            var permissaoAcesso = false;

        

            if (acessoVeiculo.StatusAcesso == StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_OFFLINE)
            {
                permissaoAcesso = true;
                LogAcessoVeiculo(acessoVeiculo, acessoVeiculo.Controladora, permissaoAcesso);
                return true;
            }
            else
            {
                acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_CAMERA_OFFLINE;
                LogAcessoVeiculo(acessoVeiculo, acessoVeiculo.Controladora, permissaoAcesso);
                return false;
            }
        }
                     
        /// <summary>
        /// 
        /// </summary>
        /// <param name="camera"></param>
        /// <param name="acessoVeiculo"></param>
        /// <param name="controladora"></param>
        /// <param name="channelIdParam"></param>
        /// <param name="imagem"></param>
        /// <returns></returns>
        private AcessoVeiculo ObterImagemACC(Camera camera, AcessoVeiculo acessoVeiculo)
        {
            try
            {
                String hostname = "10.10.2.37";
                int port = 38880; //38888
                String username = "administrator";
                String password = "123@Mudar";
                String channelId = "0a14312e3030313838353063376335642e63616d30301a10f355e06e8ced443d908e9fc462336a12221098b315b7044c438a917f8f83c5d19591380a";
                const int paramentroEspera = 1000;
                const int nvrConnectWait = paramentroEspera; // (seconds)
                nomeDevice = camera.Nome;
                IDataStore connectedDataStore = null;
                // var retorno = "";
                //-----Init Control Center

                IPAddress addr = null;
                try
                {
                    addr = IPAddress.Parse(hostname);
                }
                catch (Exception exIp)
                {
                    return null;
                    throw new Exception("IP Address is invalid.");
                }

                Debug.WriteLine("Init Control Center");

                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();
                AvigilonSdk sdk = new AvigilonSdk();

                SdkInitParams initParams = new SdkInitParams(
                    AvigilonSdk.MajorVersion,

                    AvigilonSdk.MinorVersion)
                {
                    //Era False
                    ServiceMode = true,
                    AutoDiscoverNvrs = true
                };
                initParams.SdkPath = urlBase + "packages\\AvigilonSdkDotNet.dll";
                controlCenter = sdk.CreateInstance(initParams);


                if (controlCenter == null)
                {
                    return null;

                }
                controlCenter.NvrListAdd += new AvigilonDotNet.UuidHandler﻿(m_sharedAcc_NvrListAdd﻿﻿﻿);

                //CameraIP
                IPEndPoint endpoint = new IPEndPoint(addr, port);
                DateTime waitEnd = DateTime.Now + new TimeSpan(0, 0, nvrConnectWait);


                //controlCenter.NvrListAdd += new AvigilonDotN﻿e﻿t.UuidHandler﻿﻿(m_sharedAcc_NvrListAd﻿d﻿﻿﻿﻿﻿﻿);
                AvigilonDotNet.AvgError error = controlCenter.AddNvr(endpoint);

                Stopwatch tempo = new Stopwatch();
                tempo.Start();
                while (nvr == null)
                {
                    nvr = controlCenter.GetNvr(endpoint.Address);

                    if (tempo.ElapsedMilliseconds > paramentroEspera * 1000) return null;
                }
                tempo.Stop();

                var result = nvr.Login(username, password);
                nvr.DeviceListAdd += new AvigilonDotNet.UuidHandler(NVR_DeviceLi﻿stAdd﻿);
                var tentativaDevices = 10;

                while (nvr.Devices.Count == 0 || tentativaDevices > 0)
                {

                    tentativaDevices--;

                    var camerasLigadas = 0;
                    if (nvr.Devices.Count > 0)
                    {
                        TotalDevices = nvr.Devices.Count;
                        camerasLigadas = nvr.Devices.Count(d => d.Connected);
                    }

                    if (camerasLigadas > 0)
                    {
                        var device = nvr.Devices.Where(d => d.DisplayName == camera.Nome).SingleOrDefault().Entities.Where(T => T.DisplayName == nomeDevice && T.Type == EntityType.Camera).SingleOrDefault();
                        if (device == null)
                        {
                            return null;
                        }
                        IRequestor m_requestor = controlCenter.CreateRequestor();
                        m_params.DeviceId = device.DeviceId;
                        m_params.NvrUuid = device.ReportingNvrUuid;
                        m_params.Timestamp = DateTime.Now.AddSeconds(-10);
                        m_params.DataStoreUuid = device.ReportingDataStoreUuid;
                        m_requestor.ResultFrameReceived += ImageRequestResultFrameHandler_;

                        //var eventTypes = new List<EventType>();
                        //eventTypes.Add(AvigilonDotNet.EventType.DeviceLprPlateFound);
                        Stopwatch stopImg0 = new Stopwatch();
                        stopImg0.Start();
                        m_requestor.Query(
                               1,
                               m_params);

                        // m_requestor.Query(1, m_params);
                        try

                        {
                            sdk.Shutdown();

                            Stopwatch stopImg = new Stopwatch();
                            stopImg.Start();
                            var tempoPassdo = stopImg.ElapsedMilliseconds;

                            while (MaxImageFound < 0)
                            {
                                if (stopImg.ElapsedMilliseconds > 15000) break;

                            }

                            MaxImageFound = int.MinValue;
                            stopImg0.Stop();
                            stopImg.Stop();
                            cameraImagem.AcessoVeiculo = acessoVeiculo;
                            cameraImagem.Camera = camera;
                            acessoVeiculo.CamerasImagens.Add(cameraImagem);
                            statusCameras.Add(true);
                            //if(acessoVeiculo.Controladora != null)
                            //{
                            //    if (acessoVeiculo.Controladora.ValidaLPR)
                            //    {
                            //        acessoVeiculo = ObterLprAcc(camera, ref acessoVeiculo, ref cameraImagem, "");
                            //        cameraImagem.PrecisaoImage = precisao;
                            //        cameraImagem.PlacaVeiculo = placa;
                            //    }
                            //}


                            acessoVeiculo.DataRegistro = DateTime.Now;

                            return acessoVeiculo;
                        }
                        catch
                        {
                            return null;
                        }
                    }

                }
                if (nvr.Devices == null || nvr.Devices.Count(c => c.Connected) == 0 || nvr.Devices.Count == 0)
                {
                    TotalDevices = 0;
                    return acessoVeiculo;
                }
                else
                {
                    return null;
                }
                //return acessoVeiculo;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }

        }
        
        /// <summary>
        /// Já logado, solcita a imagem capturada pela Avigilon Control Center
        /// </summary>
        /// <param name="camera"></param>
        /// <param name="acessoVeiculo"></param>
        /// <param name="nvr"></param>
        /// <returns></returns>
        private AcessoVeiculo ObterImagemACC(Camera camera, AcessoVeiculo acessoVeiculo, INvr nvr)
        {
            try
            {
                nomeDevice = camera.Nome;
                var tentativaDevices = 1000;
                Stopwatch stop = new Stopwatch();
                stop.Start();

                while (nvr.Devices.Count > 0 && tentativaDevices > 0 && stop.ElapsedMilliseconds < 6000)
                {

                    tentativaDevices--;

                    var camerasLigadas = 0;
                    if (nvr.Devices.Count > 0)
                    {
                        TotalDevices = nvr.Devices.Count;
                        camerasLigadas = nvr.Devices.Count(d => d.Connected);
                        
                    }


                    if (camerasLigadas > 0)
                    {
                        var con = nvr.Devices.Where(d => d.DisplayName == camera.Nome).SingleOrDefault();
                        
                        
                        var device = nvr.Devices.Where(d => d.DisplayName == camera.Nome).SingleOrDefault().Entities.Where(T => T.DisplayName == nomeDevice && T.Type == EntityType.Camera).SingleOrDefault();
                        tentativaDevices = 1;
                        if (device == null)
                        {
                            return acessoVeiculo;
                        }

                        if (!con.Connected)
                        {
                            return acessoVeiculo;
                        }

                        IRequestor m_requestor = controlCenter.CreateRequestor();
                        m_params.DeviceId = device.DeviceId;
                        m_params.NvrUuid = device.ReportingNvrUuid;
                        m_params.Timestamp = DateTime.Now;

                        //Setando a resolução esperada da imagem consultada no sistema Avigilon
                        m_params.Resolution = new Size
                        {
                            Height = imagemAvigilonAltura,
                            Width = imagemAvigilonLargura
                        };
                        m_params.DataStoreUuid = device.ReportingDataStoreUuid;

                        MaxImageFound = 0;
                        cameraImagem.AcessoVeiculo = acessoVeiculo;
                        cameraImagem.Camera = camera;
                        acessoVeiculo.DataRegistro = DateTime.Now;
                        m_requestor.ResultFrameReceived += ImageRequestResultFrameHandler_;

                        m_requestor.Query(
                               1,
                               m_params);

                        try

                        {
                            Stopwatch stopwatch = new Stopwatch();
                            stopwatch.Start();
                            while (MaxImageFound == 0 || stopwatch.ElapsedMilliseconds < 3000)
                            {
                                if (MaxImageFound > 0 || stopwatch.ElapsedMilliseconds > 3000)
                                {
                                    break;
                                }
                            }
                            stopwatch.Stop();
                            MaxImageFound = 0;
                            stopwatch.Start();

                            acessoVeiculo.CamerasImagens.Add(cameraImagem);
                            
                            stopwatch.Stop();
                            statusCameras.Add(true);

                            return acessoVeiculo;
                        }
                        catch
                        {
                            return acessoVeiculo;
                        }
                    }
                }
                stop.Stop();
                if (nvr.Devices == null || nvr.Devices.Count(c => c.Connected) == 0 || nvr.Devices.Count == 0)
                {
                    TotalDevices = 0;
                    return acessoVeiculo;
                }
                else
                {
                    return acessoVeiculo;
                }
                //return acessoVeiculo;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return acessoVeiculo;
            }

        }

        /// <summary>
        /// Logar no AvigilonControl Center e retorn o Network Video Recorder A.K.A. Nvr
        /// </summary>
        /// <returns></returns>
        private INvr LoginNvr()
        {
            IRepositorioParametro repositorioParametro = Fabrica.Instancia.ObterRepositorio<IRepositorioParametro>(UnidadeTrabalho);
            //var parametro = repositorioParametro.ObterTodos().Where(p => p.Nome.ObterDescricaoEnum() == ParametroAvigilon.IP_AVIGILON.ObterDescricaoEnum());
            var host = repositorioParametro.ObterTodos().Where(p => p.Nome == ParametroSistema.LOGIN_AVIGILON).Single().Valor;
            //var hostParam = ParametroSistema.IP_AVIGILON.ObterDescricaoEnum();
            String hostname = repositorioParametro.ObterTodos().Where(p => p.Nome == ParametroSistema.IP_AVIGILON).SingleOrDefault().Valor;
            int port = Int32.Parse(repositorioParametro.ObterTodos().Where(p => p.Nome == ParametroSistema.PORTA_AVIGILON).SingleOrDefault().Valor);

            //38880; //55080

            String username = repositorioParametro.ObterTodos().Where(p => p.Nome == ParametroSistema.LOGIN_AVIGILON).SingleOrDefault().Valor;
            //"administrator";
            String password = repositorioParametro.ObterTodos().Where(p => p.Nome == ParametroSistema.PASSW_AVIGILON).SingleOrDefault().Valor;
            //"123@Mudar";
            //String channelId = "0a14312e3030313838353063376335642e63616d30301a10f355e06e8ced443d908e9fc462336a12221098b315b7044c438a917f8f83c5d19591380a";
            //int paramentroEspera = Int32.Parse(repositorioParametro.ObterTodos().Where(p => p.Nome == ParametroSistema.ESPERA_AVIGILON).SingleOrDefault().Valor);
            int nvrConnectWait = 10000; // (seconds)
           
            IDataStore connectedDataStore = null;
            // var retorno = "";
            //-----Init Control Center

            IPAddress addr = null;
            try
            {
                addr = IPAddress.Parse(hostname);
            }
            catch (Exception exIp)
            {
                return null;
                throw new Exception("IP Address is invalid.");
            }

            AvigilonSdk sdk = new AvigilonSdk();

            SdkInitParams initParams = new SdkInitParams(
                AvigilonSdk.MajorVersion,

                AvigilonSdk.MinorVersion)
            {
                //Era False
                ServiceMode = true,
                AutoDiscoverNvrs = true
            };
            initParams.SdkPath = urlBase + "bin\\AvigilonSdkDotNet.dll";
            RegistraLogAvigilon(urlBase + "bin\\AvigilonSdkDotNet.dll", dateTime);
            controlCenter = sdk.CreateInstance(initParams);
            
            if (controlCenter == null)
            {
                RegistraLogAvigilon("controlCenter nulo", dateTime);
                return null;
            }

            RegistraLogAvigilon(controlCenter.ToString(), dateTime);
            controlCenter.NvrListAdd += new AvigilonDotNet.UuidHandler﻿(m_sharedAcc_NvrListAdd﻿﻿﻿);

            //CameraIP
            IPEndPoint endpoint = new IPEndPoint(addr, port);
            DateTime waitEnd = DateTime.Now + new TimeSpan(0, 0, nvrConnectWait);

            //controlCenter.NvrListAdd += new AvigilonDotN﻿e﻿t.UuidHandler﻿﻿(m_sharedAcc_NvrListAd﻿d﻿﻿﻿﻿﻿﻿);
            AvigilonDotNet.AvgError error = controlCenter.AddNvr(endpoint);

            Stopwatch tempo = new Stopwatch();
            tempo.Start();
            while (nvr == null)
            {
                nvr = controlCenter.GetNvr(endpoint.Address);
                if (tempo.ElapsedMilliseconds > 300){
                    RegistraLogAvigilon("Nvr nulo", dateTime);
                    break;
                }
            }
           
            tempo.Stop();
            if(nvr == null)
            {
                RegistraLogAvigilon("Não foi possível logar no servidor Avigilon", dateTime);
                return nvr;
            }
            else
            {
                //RegistraLogAvigilon("Nvr nulo: " + nvr.Valid.ToString(), dateTime);
                RegistraLogAvigilon("Logou no servidor Avigilon", dateTime);
                var result = nvr.Login(username, password);
                nvr.DeviceListAdd += new AvigilonDotNet.UuidHandler(NVR_DeviceLi﻿stAdd﻿);

                return nvr;
            }
            
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="camera"></param>
        /// <param name="acessoVeiculo"></param>
        /// <param name="cameraImagem"></param>
        /// <param name="channelIdParam"></param>
        /// <returns></returns>
        private AcessoVeiculo ObterLprAcc(Camera camera, ref AcessoVeiculo acessoVeiculo,
        ref CameraImagem cameraImagem,
          string channelIdParam)
        {
            String hostname = "10.10.2.37";
            int port = 38880; //38888
            String username = "administrator";
            String password = "123@Mudar";
            String channelId = "0a14312e3030313838353063376335642e63616d30301a10f355e06e8ced443d908e9fc462336a12221098b315b7044c438a917f8f83c5d19591380a";
            const int paramentroEspera = 1000;
            const int nvrConnectWait = paramentroEspera; // (seconds)
            nomeDevice = camera.Nome;
            IDataStore connectedDataStore = null;
            // var retorno = "";
            //-----Init Control Center

            IPAddress addr = null;
            try
            {
                addr = IPAddress.Parse(hostname);
            }
            catch (Exception exIp)
            {
                return null;
                throw new Exception("IP Address is invalid.");
            }

            Debug.WriteLine("Init Control Center");

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            AvigilonSdk sdk = new AvigilonSdk();

            SdkInitParams initParams = new SdkInitParams(
                AvigilonSdk.MajorVersion,

                AvigilonSdk.MinorVersion)
            {
                //Era False
                ServiceMode = true,
                AutoDiscoverNvrs = true
            };
            initParams.SdkPath = urlBase + "\\packages\\AvigilonSdkDotNet.dll";
            controlCenter = sdk.CreateInstance(initParams);


            if (controlCenter == null)
            {

                return null;
            }

            //CameraIP
            IPEndPoint endpoint = new IPEndPoint(addr, port);
            DateTime waitEnd = DateTime.Now + new TimeSpan(0, 0, nvrConnectWait);


            controlCenter.NvrListAdd += new AvigilonDotN﻿e﻿t.UuidHandler﻿﻿(m_sharedAcc_NvrListAd﻿d﻿﻿﻿﻿﻿﻿);
            AvigilonDotNet.AvgError error = controlCenter.AddNvr(endpoint);


            //nvr.DeviceListAdd += new AvigilonDotNet.UuidHandler(NVR_DeviceLi﻿stAdd﻿);
            Stopwatch tempo = new Stopwatch();
            tempo.Start();
            while (nvr == null)
            {
                nvr = controlCenter.GetNvr(endpoint.Address);
                if (tempo.ElapsedMilliseconds > paramentroEspera * 500) return null;
            }
            tempo.Stop();


            var result = nvr.Login(username, password);

            while (nvr.Devices != null)
            {
                if (nvr.Devices.Count(d => d.Connected == false) == nvr.Devices.Count)
                {
                    return null;
                }
                TotalDevices = nvr.Devices.Count(c => c.Connected);

                if (TotalDevices > 0)
                {
                    try

                    {
                        var device = nvr.Devices.Where(d => d.DisplayName == camera.Nome).SingleOrDefault().Entities.Where(T => T.DisplayName == camera.Nome).SingleOrDefault();
                        var devices = nvr.Devices.Where(d => d.DisplayName == camera.Nome).ToList();
                        List<string> ids = new List<string>();
                        for (int i = 0; i < devices.Count; i++)
                        {
                            ids.Add(devices[i].Entities.Where(e => e.Type == EntityType.Camera).SingleOrDefault().DeviceId);
                        }
                        var eventTypes = new List<EventType>();

                        eventTypes.Add(AvigilonDotNet.EventType.DeviceLprPlateFound);
                        eventTypes.Add(AvigilonDotNet.EventType.DeviceLprMatch);
                        eventTypes.Add(AvigilonDotNet.EventType.DeviceLprPlateLost);
                        //-------------GET PLR------------------------------ 
                        AvigilonDotNet.IQuery query;

                        query = nvr.QueryEventsDeviceId(
                              DateTime.Now.AddMinutes(-1),
                              DateTime.Now,
                              AvigilonDotNet.EventLinkMode.Closed | AvigilonDotNet.EventLinkMode.Opened |
                              AvigilonDotNet.EventLinkMode.Single | AvigilonDotNet.EventLinkMode.Undefined,
                              false, ids);

                        //-------------Pegar Placa ------------------------------
                        query.ResultReceived += ImageLPRResultFrameHandler_;

                        var vgError = query.Execute();
                        Stopwatch stopwatchLpr = new Stopwatch();
                        // m_requestor.Query(1, m_params);
                        while (MaxPlateFound > 0)
                        {
                            if(stopwatchLpr.ElapsedMilliseconds > 500)
                            {
                                break;
                            }
                        }

                        var indexLPR = 0;
                        if (LPR.Count > 0 && acessoVeiculo.CamerasImagens.Count > 0)
                        {
                            var precisaoAnterior = 0f;

                            for (int ix = 0; ix < LPR.Count; ix++)
                            {
                                if (LPR[ix].PrecisaoImage > precisaoAnterior)
                                {
                                    acessoVeiculo.CamerasImagens.Where(c => c.Camera.Nome == camera.Nome).Single().PlacaVeiculo = LPR[indexLPR].PlacaVeiculo;
                                    acessoVeiculo.CamerasImagens.Where(c => c.Camera.Nome == camera.Nome).Single().PrecisaoImage = LPR[indexLPR].PrecisaoImage;
                                }
                            }
                        }


                        sdk.Shutdown();

                        //cameraImagem.AcessoVeiculo = acessoVeiculo;
                        //cameraImagem.Camera = camera;
                        //cameraImagem.CaminhoImagem = paramentroUrlCaminhoImg + "_CAM_" + camera.Nome + "_" + DateTime.Now.ToString() + ".jpg";

                        //statusCameras.Add(true);
                        //if (acessoVeiculo.Controladora != null)
                        //{
                        //    if (acessoVeiculo.Controladora.ValidaLPR)
                        //    {
                        //        var placaResult = ObterLprAcc(camera, ref acesso, ref cameraImagem, "");
                        //       
                        //    }
                        //}

                        //acessoVeiculo.CamerasImagens.Add(cameraImagem);
                        return acessoVeiculo;
                    }
                    catch (Exception exLPR)
                    {
                        Console.WriteLine(exLPR.Message);
                        return null;
                    }
                }

            }
            stopwatch.Stop();

            if (stopwatch.ElapsedMilliseconds / 1000 > nvrConnectWait)
            {
                return null;
            }


            switch (result)
            {
                case LoginResult.Successful:
                    Debug.WriteLine("Login  - successfuly");
                    //Log Conexão con sucesso
                    connectedDataStore = nvr;

                    break;
                case LoginResult.ErrAlreadyLoggedIn:
                    Debug.WriteLine("Login - already logged in");
                    //Log já logado
                    connectedDataStore = nvr;
                    break;
                case LoginResult.ErrBadPassword:
                    Debug.WriteLine("The given password is not valid.");
                    //Log erro de senha
                    throw new Exception("The given password is not valid.");
                default:
                    //Log Erro inexperado ao logar no NVR
                    Debug.WriteLine("Unexpected error occurred while logging in to the NVR.");
                    throw new Exception("Unexpected error occurred while logging in to the NVR.");
            }

            return null;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="camera"></param>
        /// <param name="acessoVeiculo"></param>
        /// <param name="cameraImagem"></param>
        /// <param name="nvr"></param>
        /// <returns></returns>
        private AcessoVeiculo ObterLprAcc(Camera camera, ref AcessoVeiculo acessoVeiculo,
        ref CameraImagem cameraImagem, INvr nvr)
        {
            while (nvr.Devices != null)
            {
                if (nvr.Devices.Count(d => d.Connected == false) == nvr.Devices.Count)
                {
                    return acessoVeiculo;
                }
                TotalDevices = nvr.Devices.Count(c => c.Connected);

                if (TotalDevices > 0)
                {
                    try
                    {
                        var device = nvr.Devices.Where(d => d.DisplayName == camera.Nome).SingleOrDefault().Entities.Where(T => T.DisplayName == camera.Nome).SingleOrDefault();
                        var devices = nvr.Devices.Where(d => d.DisplayName == camera.Nome).ToList();
                        List<string> ids = new List<string>();
                        for (int i = 0; i < devices.Count; i++)
                        {
                            ids.Add(devices[i].Entities.Where(e => e.Type == EntityType.Camera).SingleOrDefault().DeviceId);
                        }
                        var eventTypes = new List<EventType>();

                        eventTypes.Add(AvigilonDotNet.EventType.DeviceLprPlateFound);
                        eventTypes.Add(AvigilonDotNet.EventType.DeviceLprMatch);
                        eventTypes.Add(AvigilonDotNet.EventType.DeviceLprPlateLost);
                        //-------------GET PLR------------------------------ 
                        AvigilonDotNet.IQuery query;

                        query = nvr.QueryEventsDeviceId(
                              DateTime.Now.AddMinutes(-1),
                              DateTime.Now,
                              AvigilonDotNet.EventLinkMode.Closed | AvigilonDotNet.EventLinkMode.Opened |
                              AvigilonDotNet.EventLinkMode.Single | AvigilonDotNet.EventLinkMode.Undefined,
                              false, ids);
                        MaxPlateFound = 0;
                        //-------------Pegar Placa ------------------------------
                        query.ResultReceived += ImageLPRResultFrameHandler_;

                        var vgError = query.Execute();
                        Stopwatch stopwatch = new Stopwatch();
                        stopwatch.Start();
                        while (MaxPlateFound == 0 || stopwatch.ElapsedMilliseconds < 2000)
                        {
                            if (MaxPlateFound > 0 || stopwatch.ElapsedMilliseconds > 2000)
                            {
                                break;
                            }
                        }
                        //RegistraLogAvigilon("Lpr obt", DateTime.UtcNow.AddHours(-3));
                        stopwatch.Stop();
                       
                        if (LPR.Count > 0 && acessoVeiculo.CamerasImagens.Count > 0)
                        {
                            //RegistraLogAvigilon("Maior Placa", DateTime.UtcNow.AddHours(-3));
                            var precisaoAnterior = 0f;

                            for (int ix = LprIndex; ix < LPR.Count; ix++)
                            {
                              
                                    if (LPR[ix].PrecisaoImage > precisaoAnterior)
                                    {
                                        acessoVeiculo.CamerasImagens.Where(c => c.Camera.Nome == camera.Nome).Single().PlacaVeiculo = LPR[ix].PlacaVeiculo;
                                        acessoVeiculo.CamerasImagens.Where(c => c.Camera.Nome == camera.Nome).Single().PrecisaoImage = LPR[ix].PrecisaoImage;
                                        precisaoAnterior = LPR[ix].PrecisaoImage;
                                        placa = LPR[ix].PlacaVeiculo;
                                        precisao = LPR[ix].PrecisaoImage;
                                        LprIndex = ix + 1;
                                    }
                              
                            }
                            //RegistraLogAvigilon("Maior placa 2", DateTime.UtcNow.AddHours(-3));
                        }
                        else
                        {
//RegistraLogAvigilon("Sem placa", DateTime.UtcNow.AddHours(-3));
                            acessoVeiculo.CamerasImagens.Where(c => c.Camera.Nome == camera.Nome).Single().PlacaVeiculo = "-";
                            acessoVeiculo.CamerasImagens.Where(c => c.Camera.Nome == camera.Nome).Single().PrecisaoImage = 0f;
                            placa = "-";
                            precisao = 0f;
                            
                        }
                        //RegistraLogAvigilon("Imagems obt", DateTime.UtcNow.AddHours(-3));
                        return acessoVeiculo;
                    }
                    catch (Exception exLPR)
                    {
                        Console.WriteLine(exLPR.Message);
                        return acessoVeiculo;
                    }
                }

            }         
            return acessoVeiculo;
        }

        private void ImageRequestResultFrameHandler_(uint requestId, AvgError error, IFrame frame)
        {
            var fram = frame;
            RegistraLogAvigilon("solicitando imagem", dateTime);
            //Pegar todas as imagens
            if (frame != null)
            {

                RegistraLogAvigilon("obteve imagem", dateTime);
                IRepositorioParametro repositorioParametro = Fabrica.Instancia.ObterRepositorio<IRepositorioParametro>(UnidadeTrabalho);
                var caminhoFisicoImagem = repositorioParametro.ObterTodos().Where(p => p.Nome == ParametroSistema.CAMINHO_IMAGEM_FISICO).SingleOrDefault().Valor;
                //var imagemPath = paramentroUrlCaminhoImg + "\\Img_" + DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".jpg";
                var imagemPath = "Cam_" + cameraImagem.Camera.Nome + "_" + DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss") + ".jpg";
                cameraImagem.CaminhoImagem = imagemPath;
                ImagensGravadas.Add(imagemPath);

                MaxImageFound = 1;
                var caminhofisicocompleto = caminhoFisicoImagem + "\\" + imagemPath;
                var aa = "";
                try
                {
                    
                        IFrameImage m_frameImage = frame as AvigilonDotNet.IFrameImage;
                        AvigilonDotNet.IFrameImageRaw fir = m_frameImage as AvigilonDotNet.IFrameImageRaw;
                        imagem = MakeBitmapFromImageRaw(fir);
                        Task.Run(() =>
                        {
                            imagem.Save(caminhoFisicoImagem + "\\" + imagemPath);
                            RegistraLogAvigilon("salvou imagem", dateTime);
                        }).ConfigureAwait(false);
                      
                     
                }
                catch(Exception exImg)
                {
                  
                    erro = exImg.Message;
                }               
            }
            else
            {
                String errMsg = "Request " + requestId.ToString() + " has failed, err = " + error.ToString();
            }
        }

        private void ImageLPRResultFrameHandler_(IQueryResult result, QueryState state)
        {
            
            //RegistraLogAvigilon("solicitando LPR", dateTime);
            IQueryRe﻿sultEve﻿nts﻿﻿﻿﻿﻿﻿﻿﻿﻿ eventResult = (IQueryRe﻿sultEve﻿nts﻿﻿﻿﻿﻿﻿﻿﻿﻿)result;

            

            IRepositorioCameraImagem repCameraImagem = Fabrica.Instancia.ObterRepositorio<IRepositorioCameraImagem>(UnidadeTrabalho);
            int placasRecebidas = 0;
            for (int i = 0; i < eventResult.Results.Count; i++)
            {
                if (eventResult.Results[i].originalEvent != null)
                {                 
                    var devicename = ((AvigilonDotNet.IEventCategoryDevice)eventResult.Results[i].originalEvent).DeviceName;
                    var tipo = eventResult.Results[i].originalEvent.GetType().ToString();
                    if (tipo.Contains("DeviceLprPlateFound"))
                    {
                        placasRecebidas++;
                        lpr = (IEventDeviceLprPlateFound)eventResult.Results[i].originalEvent;
                        List<CameraImagem> cameraImagem =
                        repCameraImagem.ObterTodos().Where(p => p.PlacaVeiculo == lpr.LicensePlateString &&
                        p.AcessoVeiculo.DataRegistro >= DateTime.Now.AddMinutes(-1)).ToList();
                        //Placa ainda não tratada, ou seja, não houve tentativa de acesso com o veículo desta placa
                        if (cameraImagem == null || cameraImagem.Count == 0)
                        {
                            LPR.Add(new CameraImagem
                            {
                                PlacaVeiculo = lpr.LicensePlateString,
                                PrecisaoImage = lpr.Confidence * 100,
                                Camera = new Camera
                                {
                                    Nome = devicename
                                }
                            });
                            //placa = lpr.LicensePlateString;
                            //precisao = lpr.Confidence * 100;
                        }
                    }
                    else if (tipo.Contains("DeviceLprPlateMatch"))
                    {
                        var lpr = (IEventDeviceLprMatch)eventResult.Results[i].originalEvent;
                        placa = lpr.DetectedLicensePlate;

                        placasRecebidas++;

                        List<CameraImagem> cameraImagem =
                        repCameraImagem.ObterTodos().Where(p => p.PlacaVeiculo == lpr.DetectedLicensePlate &&
                        p.AcessoVeiculo.DataRegistro >= DateTime.Now.AddMinutes(-1)).ToList();
                        //Placa ainda não tratada, ou seja, não houve tentativa de acesso com o veículo desta placa
                        if (cameraImagem == null || cameraImagem.Count == 0)
                        {
                            LPR.Add(new CameraImagem
                            {
                                PlacaVeiculo = lpr.DetectedLicensePlate,
                                PrecisaoImage = lpr.Confidence * 100,
                                Camera = new Camera
                                {
                                    Nome = devicename
                                }
                            });
                            //placa = lpr.DetectedLicensePlate;
                            //precisao = lpr.Confidence * 100;
                        }
                    }
                    else if (tipo.Contains("DeviceLprPlateLost"))
                    {
                        var lpr = (IEventDeviceLprPlateLost)eventResult.Results[i].originalEvent;
                        List<CameraImagem> cameraImagem =
                        repCameraImagem.ObterTodos().Where(p => p.PlacaVeiculo == lpr.LicensePlateString &&
                        p.AcessoVeiculo.DataRegistro >= DateTime.Now.AddMinutes(-1)).ToList();
                        //Placa ainda não tratada, ou seja, não houve tentativa de acesso com o veículo desta placa

                        placasRecebidas++;
                        if (cameraImagem == null || cameraImagem.Count == 0)
                        {
                            LPR.Add(new CameraImagem
                            {
                                PlacaVeiculo = lpr.LicensePlateString,
                                PrecisaoImage = lpr.Confidence * 100,
                                Camera = new Camera
                                {
                                    Nome = devicename
                                }
                            });
                            //placa = lpr.LicensePlateString;
                            //precisao = lpr.Confidence * 100;
                        }
                    }
                }
               
            }
            if(eventResult.Results.Count == 0)
            {
                MaxImageFound = 1;
            }
            else
            {
                MaxPlateFound = eventResult.Results.Count;
            }
            
            //RegistraLogAvigilon("obteve " + placasRecebidas + " placa(s)", dateTime);
        }


        private Bitmap MakeBitmapFromImageRaw(AvigilonDotNet.IFrameImageRaw rawFrame)
        {
            Rectangle imageRect = new Rectangle(0, 0, rawFrame.Size.Width, rawFrame.Size.Height);

            switch (rawFrame.DefaultPixelFormat)
            {
                case AvigilonDotNet.PixelFormat.Gray8:
                    {
                        Bitmap retVal = new Bitmap(
                            rawFrame.Size.Width,
                            rawFrame.Size.Height,
                            System.Drawing.Imaging.PixelFormat.Format8bppIndexed);

                        // Define palette for 8bit grayscale
                        System.Drawing.Imaging.ColorPalette pal = retVal.Palette;
                        for (int ix = 0; ix < pal.Entries.Length; ++ix)
                        {
                            pal.Entries[ix] = System.Drawing.Color.FromArgb(ix, ix, ix);
                        }
                        retVal.Palette = pal;

                        System.Drawing.Imaging.BitmapData bitmapData = retVal.LockBits(
                            imageRect,
                            System.Drawing.Imaging.ImageLockMode.WriteOnly,
                            System.Drawing.Imaging.PixelFormat.Format8bppIndexed);

                        byte[] byteArray = rawFrame.GetAsArray(
                            AvigilonDotNet.PixelFormat.Gray8,
                            (ushort)bitmapData.Stride);

                        System.Runtime.InteropServices.Marshal.Copy(
                            byteArray,
                            0,
                            bitmapData.Scan0,
                            byteArray.Length);
                        retVal.UnlockBits(bitmapData);

                        return retVal;
                    }

                case AvigilonDotNet.PixelFormat.RGB24:
                    {
                        Bitmap retVal = new Bitmap(
                            rawFrame.Size.Width,
                            rawFrame.Size.Height,
                            System.Drawing.Imaging.PixelFormat.Format24bppRgb);

                        System.Drawing.Imaging.BitmapData bitmapData = retVal.LockBits(
                            imageRect,
                            System.Drawing.Imaging.ImageLockMode.WriteOnly,
                            System.Drawing.Imaging.PixelFormat.Format24bppRgb);

                        byte[] byteArray = rawFrame.GetAsArray(
                            AvigilonDotNet.PixelFormat.RGB24,
                            (ushort)bitmapData.Stride);

                        System.Runtime.InteropServices.Marshal.Copy(
                            byteArray,
                            0,
                            bitmapData.Scan0,
                            byteArray.Length);
                        retVal.UnlockBits(bitmapData);

                        return retVal;
                    }

                case AvigilonDotNet.PixelFormat.RGB32:
                    {
                        Bitmap retVal = new Bitmap(
                            rawFrame.Size.Width,
                            rawFrame.Size.Height,
                            System.Drawing.Imaging.PixelFormat.Format32bppRgb);

                        System.Drawing.Imaging.BitmapData bitmapData = retVal.LockBits(
                            imageRect,
                            System.Drawing.Imaging.ImageLockMode.WriteOnly,
                            System.Drawing.Imaging.PixelFormat.Format32bppRgb);

                        byte[] byteArray = rawFrame.GetAsArray(
                            AvigilonDotNet.PixelFormat.RGB32,
                            (ushort)bitmapData.Stride);

                        System.Runtime.InteropServices.Marshal.Copy(
                            byteArray,
                            0,
                            bitmapData.Scan0,
                            byteArray.Length);
                        retVal.UnlockBits(bitmapData);

                        return retVal;
                    }

                default:
                    return null;
            }
        }

        /// <summary>
        /// Log de Acesso do Veículo
        /// </summary>
        /// <param name="acessoVeiculo"></param>
        /// <param name="controladora"></param>
        /// <param name="acesso"></param>
        private void LogAcessoVeiculo(
            AcessoVeiculo acessoVeiculo, Controladora controladora, bool acesso = false)
        {
            try
            {
                IRepositorioAcessoVeiculo repAcessoVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioAcessoVeiculo>(UnidadeTrabalho);
                IRepositorioCameraImagem repCameraImagem = Fabrica.Instancia.ObterRepositorio<IRepositorioCameraImagem>(UnidadeTrabalho);

                int parametro = 100;

                StringBuilder sb = new StringBuilder();
                sb.Append(acessoVeiculo.Cracha.Papel.PessoaFisica.Nome + " teve acesso ");
                switch (acessoVeiculo.StatusAcesso)
                {
                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_AUTORIZADO_LPR_ATIVO_ACIMA:

                        sb.Append("liberado às " + acessoVeiculo.DataRegistro + " por possuir permissão de acesso ao local da leitora " + acessoVeiculo.Leitora.IDLeitora + " que estava com LPR ativo, ");
                        if(placa == null)
                        {
                            RegistraLogAvigilon("Placa null", dateTime);
                        }
                        
                        {
                            RegistraLogAvigilon(parametroPrecisao.ToString(), dateTime);
                        }
                        

                        acessoVeiculo.StatusAutorizacao = true;
                        sb.Append("a placa aferida do veículo foi registrada como " + placa + " com a precisão aferida da placa de " + precisao + "%, ");
                        if (precisao > parametroPrecisao)
                        {
                            sb.Append("precisão registrada estava acima do nível de aceitável pelo sistema de Leitura de Placa que é de " + parametroPrecisao + "%.");

                        }
                        else if (precisao == parametroPrecisao)
                        {
                            sb.Append("precisão registrada estava igual ao nível de aceitável pelo sistema de Leitura de Placa que é de " + parametroPrecisao + "%.");
                        }

                        var observacaoLiberadoLPR = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoLiberadoLPR;
                        //UnidadeTrabalho.BeginTransaction();
                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        //UnidadeTrabalho.Commit();
                        for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                        {
                            repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                        }

                        break;

                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_LPR_ATIVO_ACIMA:

                        sb.Append("negado às " + acessoVeiculo.DataRegistro + "por não possuir permissão de acesso ao local da leitora" + acessoVeiculo.Leitora.IDLeitora + " que estava com LPR ativo , ");
                        acessoVeiculo.StatusAutorizacao = false;
                        sb.Append(" a placa aferida do veículo foi registrada como " + placa + " com a precisão aferida da placa de " + precisao + "%, ");
                        if (precisao > parametroPrecisao)
                        {
                            sb.Append("precisão registrada estava acima do nível de aceitável pelo sistema de Leitura de Placa que é de " + parametroPrecisao + "%.");

                        }
                        else if (precisao == parametroPrecisao)
                        {
                            sb.Append("precisão registrada estava igual ao nível de aceitável pelo sistema de Leitura de Placa que é de " + parametroPrecisao + "%.");
                        }
                        var observacaoNegadoLPR = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoNegadoLPR;
                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        //UnidadeTrabalho.Commit();
                        for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                        {
                            repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                        }
                        break;

                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_LPR_ATIVO_ABAIXO:
                        acessoVeiculo.StatusAutorizacao = false;
                        sb.Append("negado às " + acessoVeiculo.DataRegistro + "o que o veículo não foi identificado pelo sistema de Leitura de placas");
                        sb.Append(" a placa aferida do veículo foi registrada como " + placa + " com a precisão aferida da placa de " + precisao + "%, esta abaixo do parâmetro definido no sistema,");
                        sb.Append(" no local da leitora " + acessoVeiculo.Leitora.IDLeitora + " que estava com Sistema de leitura de placas ativo");

                        var observacaoVeiculoNaoIdentificado = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoVeiculoNaoIdentificado;
                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        //UnidadeTrabalho.Commit();
                        for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                        {
                            repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                        }

                        break;


                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_GRUPO_LEITORA:
                        acessoVeiculo.StatusAutorizacao = false;
                        sb.Append("negado às " + acessoVeiculo.DataRegistro + " por não possuir permissão de acesso ao local da leitora " + acessoVeiculo.Leitora.IDLeitora + " que estava com Sistema de leitura de placas ativo, ");

                        sb.Append(" a placa aferida do veículo foi registrada como " + placa + " com a precisão aferida da placa de " + precisao + "%, ");
                        if (precisao > parametroPrecisao)
                        {
                            sb.Append("precisão registrada estava acima do nível de aceitável pelo sistema de Leitura de Placa que é de " + parametroPrecisao + "%.");
                        }
                        else
                        {
                            sb.Append("precisão registrada estava abaixo do nível de aceitável pelo sistema de Leitura de Placa que é de " + parametroPrecisao + "%.");
                        }

                        var observacaoNegadoGrupoLeitora = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoNegadoGrupoLeitora;
                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        //UnidadeTrabalho.Commit();
                        for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                        {
                            repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                        }


                        break;


                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_INATIVA:

                        acessoVeiculo.StatusAutorizacao = true;
                        sb.Append("autorizado às " + acessoVeiculo.DataRegistro + " porque " + acessoVeiculo.Cracha.Papel.PessoaFisica.Nome +
                             " possuia permissão de acesso ao local da leitora " + acessoVeiculo.Leitora.IDLeitora +
                             ", não há registro fotográfico, porque no momento da passagem do crachá, não havia câmera ativa no local");

                        var observacaoLiberadoPapelVeiculoCamOffLine = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoLiberadoPapelVeiculoCamOffLine;
                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        break;

                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_CAMERA_INATIVA:

                        acessoVeiculo.StatusAutorizacao = false;
                        sb.Append("não autorizado às " + acessoVeiculo.DataRegistro + " porque " + acessoVeiculo.Cracha.Papel.PessoaFisica.Nome +
                            "não possuia permissão de acesso ao local da leitora " + acessoVeiculo.Leitora.IDLeitora +
                            ", não há registro fotográfico do momento porque no momento da passagem do crachá, não havia câmera ativa no local");

                        var observacaoCamInativa = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoCamInativa;

                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        break;


                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_ATIVA_LPR_INATIVO:
                        acessoVeiculo.StatusAutorizacao = true;

                        sb.Append("liberado às " + acessoVeiculo.DataRegistro + " no local da leitora " + acessoVeiculo.Leitora.IDLeitora +
                            ", sendo assim seu veículo não foi identificado, " +
                            "porém imagem foi tirada fotografica do carro no momento da passagem do crachá," +
                            " sistema de detecção de placas desligado");

                        var observacaoCamOnLineOCROff = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoCamOnLineOCROff;

                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        //UnidadeTrabalho.Commit();
                        for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                        {
                            repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                        }

                        break;


                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_OFFLINE:
                        acessoVeiculo.StatusAutorizacao = true;
                        sb.Append("autorizado às " + acessoVeiculo.DataRegistro +
                             " possuia permissão de acesso ao local da leitora " + acessoVeiculo.Leitora.IDLeitora +
                            " , a autorização levou em conta apenas a permissão do acesso da pessoa  porque a câmera(s) está/estão offline, " +
                            "sendo assim seu veículo não foi identificado, porém autorizado");

                        var observacaoCameraOffline = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoCameraOffline;
                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        break;

                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_CAMERA_OFFLINE:
                        acessoVeiculo.StatusAutorizacao = false;
                        sb.Append("não autorizado às " + acessoVeiculo.DataRegistro +
                            " não possuia permissão de acesso ao local da leitora " + acessoVeiculo.Leitora.IDLeitora +
                           " , a autorização levou em conta apenas a permissão do acesso da pessoa  porque a câmera(s) está/estão offline, " +
                            "sendo assim seu veículo não foi identificado e também não autorizado");


                        var observacaoNaoAutorizadoCameraOffline = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoNaoAutorizadoCameraOffline;
                       acessoVeiculo.CamerasImagens = null;

                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        break;


                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERAS_NAO_VINCULADAS:
                        sb.Append("autorizado ");

                        sb.Append("às " + acessoVeiculo.DataRegistro + ", pela validação de crachá," +
                            " não houve imagem fotografada no momento por não haver câmeras vinculadas a controladora "
                            + controladora.Modelo + ", sendo assim seu veículo não foi identificado e bloqueado o acesso");

                        var observacaoLiberadoPapelVeiculoBloqueado = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoLiberadoPapelVeiculoBloqueado;
                        acessoVeiculo.CamerasImagens = null;
                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        break;

                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_CAMERAS_NAO_VINCULADAS:
                        sb.Append("não autorizado ");

                        sb.Append("às " + acessoVeiculo.DataRegistro + ", pela validação de crachá," +
                            " não houve imagem fotografada no momento por não haver câmeras vinculadas a controladora "
                            + controladora.Modelo + ", sendo assim seu veículo não foi identificado");

                        var observacaoNaoPapelVeiculoBloqueado = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoNaoPapelVeiculoBloqueado;
                        acessoVeiculo.CamerasImagens = null;
                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        break;

                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_ACC_INATIVA_NEGADO:
                        acessoVeiculo.StatusAutorizacao = false;
                        sb.Append("negado às " + acessoVeiculo.DataRegistro + " porque o sistema Avigilon Control Center estava offline");
                        var observacaoAccOffline = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoAccOffline;
                        acessoVeiculo.CamerasImagens = null;
                        acessoVeiculo.DataRegistro = DateTime.Now;
                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        //Task.Run(() =>
                        //{
                           
                        //    //UnidadeTrabalho.Commit();
                        //}).ConfigureAwait(false);
                        break;

                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_DATA_ACESSO_TEMPORARIO_EXPIRADO:
                        acessoVeiculo.StatusAutorizacao = false;
                        sb.Append("negado às " + acessoVeiculo.DataRegistro + " porque a o veículo de placa " + placa +
                            " tentou acesso no local da leitora " + acessoVeiculo.Leitora.IDLeitora + " fora do seu período de permissão de acesso temporário, acesso expirado");

                        var observacaoAcessoExpirado = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoAcessoExpirado;

                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        //UnidadeTrabalho.Commit();
                        for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                        {
                            repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                        }

                        break;
                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_PAPEL_NAO_AUTORIZADO_CANCELA:

                        acessoVeiculo.StatusAutorizacao = false;
                        //Verificar se o veículo tem acesso
                        sb.Append("negado às " + acessoVeiculo.DataRegistro + " com o veículo de placa " + acessoVeiculo.VeiculoCredencialVeiculo.Veiculo.Placa +
                            ", o/a condutor(a) tentou acesso no local da leitora " + acessoVeiculo.Leitora.IDLeitora + ", porém não possui acesso ao local");

                        var observacaoAcessoNegadoPapelVeiculo = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoAcessoNegadoPapelVeiculo;

                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        //UnidadeTrabalho.Commit();
                        for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                        {
                            repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                        }


                        break;

                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_PAPEL_AUTORIZADO_VEICULO_NAO_AUTORIZADO:

                        acessoVeiculo.StatusAutorizacao = false;
                        //Verificar se o veículo tem acesso
                        sb.Append("negado às " + acessoVeiculo.DataRegistro + " porque o veículo de placa, " + acessoVeiculo.VeiculoCredencialVeiculo.Veiculo.Placa +
                            " tentou acesso no local da leitora " + acessoVeiculo.Leitora.IDLeitora + ", porém o veículo não possuia permissão de acesso ao local");

                        var observacaoAcessoAutorizadoPapelVeiculoNegado = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoAcessoAutorizadoPapelVeiculoNegado;

                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        //UnidadeTrabalho.Commit();
                        for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                        {
                            repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                        }

                        break;

                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_PAPEL_NAO_AUTORIZADO_VEICULO_NAO_AUTORIZADO:

                        acessoVeiculo.StatusAutorizacao = false;
                        //Verificar se o veículo tem acesso
                        sb.Append("negado às " + acessoVeiculo.DataRegistro + " porque o veículo de placa " + acessoVeiculo.VeiculoCredencialVeiculo.Veiculo.Placa +
                            ", tentou acesso no local da leitora " + acessoVeiculo.Leitora.IDLeitora + ", porém não possui permissão ao local");

                        var observacaoAcessoNegadoPapelAcessoNegadoVeiculo = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoAcessoNegadoPapelAcessoNegadoVeiculo;

                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        //UnidadeTrabalho.Commit();
                        for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                        {
                            repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                        }
                        break;

                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_CREDENCIAL_INEXISTENTE:
                        acessoVeiculo.StatusAutorizacao = false;
                        //Verificar se o veículo tem acesso
                        sb.Append("negado às " + acessoVeiculo.DataRegistro + " porque o veículo de placa " + placa +
                            " tentou acesso no local da leitora " + acessoVeiculo.Leitora.IDLeitora + ", porém o veículo não possuia credencial cadastrada, " +
                            " sendo assim o veículo teve seu acesso não autorizado");

                        var observacaoAcessoNegadoCredencialInexistente = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoAcessoNegadoCredencialInexistente;
                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        //UnidadeTrabalho.Commit();
                        for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                        {
                            repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                        }
                        //repAcessoVeiculo.Salvar(acessoVeiculo);

                        break;
                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_ENCONTRADO_BASE_DADOS:
                        acessoVeiculo.StatusAutorizacao = false;
                        sb.Append("negado às " + acessoVeiculo.DataRegistro + " porque o veículo de placa " + placa +
                            " tentou acesso no local da leitora " + acessoVeiculo.Leitora.IDLeitora + ", porém o veículo não foi identificado na base de dados, " +
                            " sendo assim o veículo teve seu acesso não autorizado");

                        var observacaoAcessoNegadoVeiculoInexistente = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoAcessoNegadoVeiculoInexistente;
                       
                            repAcessoVeiculo.Salvar(acessoVeiculo);
                        //UnidadeTrabalho.Commit();
                        for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                        {
                            if (String.IsNullOrEmpty(acessoVeiculo.CamerasImagens[i].PlacaVeiculo))
                            {
                                acessoVeiculo.CamerasImagens[i].PlacaVeiculo = "-";
                            }
                            repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                        }
                        //repAcessoVeiculo.Salvar(acessoVeiculo);

                        break;

                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_TEMPO_RESPOSTA_EXCEDIDO:
                        acessoVeiculo.StatusAutorizacao = false;
                        sb.Append("negado às " + acessoVeiculo.DataRegistro + " porque o veículo de placa " + placa +
                            " tentou acesso no local da leitora " + acessoVeiculo.Leitora.IDLeitora + ", porém o veículo não foi identificado na base de dados, " +
                            " sendo assim o veículo teve seu acesso não autorizado");

                        var observacaoAcessoNegadoTempoExcedido = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoAcessoNegadoTempoExcedido;

                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        //UnidadeTrabalho.Commit();
                        for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                        {
                            if (String.IsNullOrEmpty(acessoVeiculo.CamerasImagens[i].PlacaVeiculo))
                            {
                                acessoVeiculo.CamerasImagens[i].PlacaVeiculo = "-";
                            }
                            repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                        }
                        //repAcessoVeiculo.Salvar(acessoVeiculo);

                        break;


                        

                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_CREDENCIAL_NAO_IMPRESSA:
                        acessoVeiculo.StatusAutorizacao = false;
                        sb.Append("negado às " + acessoVeiculo.DataRegistro + " porque o veículo de placa " + placa +
                            " tentou acesso no local da leitora " + acessoVeiculo.Leitora.IDLeitora + ", porém o estado da credencial do veículo não era \"impressa\"" +
                            ", sendo assim o veículo teve seu acesso não autorizado");

                        var observacaoAcessoNegadoCredencialnaoImpressa = sb.ToString();

                        acessoVeiculo.Observacoes = observacaoAcessoNegadoCredencialnaoImpressa;
                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        //UnidadeTrabalho.Commit();
                        for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                        {
                            repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                        }
                        //repAcessoVeiculo.Salvar(acessoVeiculo);

                        break;

                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_ERRO:
                        acessoVeiculo.StatusAutorizacao = false;
                        sb.Append("negado às " + acessoVeiculo.DataRegistro + " houve erro na aplicação \n Erro: " + erro);

                        var observacaoAcessoErro = sb.ToString();

                        acessoVeiculo.Observacoes = observacaoAcessoErro;
                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        //UnidadeTrabalho.Commit();
                        if (acessoVeiculo.CamerasImagens != null)
                        {
                            for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                            {
                                repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                            }
                        }

                        

                        //repAcessoVeiculo.Salvar(acessoVeiculo);

                        break;


                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_CREDENCIAL_FORA_INTERVALO:
                        acessoVeiculo.StatusAutorizacao = false;
                        sb.Append("negado às " + acessoVeiculo.DataRegistro + ", porque a tentativa de acesso ocorreu fora do período de acesso temporário do veículo");

                        var observacaoAcessoTemporario = sb.ToString();

                        acessoVeiculo.Observacoes = observacaoAcessoTemporario;
                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        //UnidadeTrabalho.Commit();
                        if (acessoVeiculo.CamerasImagens != null)
                        {
                            for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                            {
                                repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                            }
                        }



                        //repAcessoVeiculo.Salvar(acessoVeiculo);

                        break;

                  
                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_CAMERA_ATIVA_LPR_INATIVO:
                        acessoVeiculo.StatusAutorizacao = false;

                        sb.Append("não autorizado às " + acessoVeiculo.DataRegistro + " no local da leitora " + acessoVeiculo.Leitora.IDLeitora +
                            ", sendo assim seu veículo não foi identificado, " +
                            "porém imagem foi tirada fotografica do carro no momento da passagem do crachá," +
                            " sistema de detecção de placas desligado");

                        var observacaoCamOnLineOCROffNaoAutorizado = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoCamOnLineOCROffNaoAutorizado;

                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        //UnidadeTrabalho.Commit();
                        if (acessoVeiculo.CamerasImagens != null)
                        {
                            for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                            {
                                repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                            }
                        }

                        break;

                }

            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.Message);
                erro = exc.Message;
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="json"></param>
        private ContentResult Json(string json)
        {
            return new ContentResult
            {
                Content = json,
                ContentType = "application/json"
            };

        }


        private bool UploadFTPSecure()
        {
            return true;

        }

        private void m_sharedAcc_NvrListAdd﻿(string uuid)
        {
            nvr = controlCenter.GetNvr(uuid);
            //nvr.DeviceListAdd += new AvigilonDotNet.UuidHandler(NVR_DeviceListAdd﻿);

            //String username = "administrator";
            //String password = "123@Mudar";

            //var result = nvr.Login(username, password);
            //if (result != AvigilonDotNet.LoginResult.Successful)
            //{
            //    return;
            //}

        }

        private DateTime LastDeviceAdded = DateTime.MinValue;
        private List<AvigilonDotNet.IEntity> dicTLCEntity = new List<AvigilonDotNet.IEntity>();
        private void NVR_DeviceListAdd(string deviceId)
        {
            //Log(LoggingLevel.INFO, "SDK NVR_DeviceListAdd START (Host {0})", Host);

            //LastDeviceAdded = DateTime.Now;

            AvigilonDotNet.IDevice device = nvr.GetDeviceById(deviceId);
            nvr.Devices.Add(device);
            if (device.DisplayName == nomeDevice)
            {
                for (int i = 0; i < device.Entities.Count; i++)
                {
                    if (device.Entities[i].Type == AvigilonDotNet.EntityType.Camera && device.Entities[i].DisplayName == nomeDevice)
                    {
                        dicTLCEntity.Add(device.Entities[i]);
                    }
                }
            }
        }


        /// <summary>
        /// Valida as placas existentes e compara com a placa do veículo Credenciado
        /// </summary>
        /// <param name="acessoVeiculo"></param>
        private bool ValidaPlaca(AcessoVeiculo acessoVeiculo)
        {
            float precisaoAnterior = 0f;
            float precisaoAtual = 0f;

            for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
            {
                //Verificar precisão das placas
                if (float.Parse(acessoVeiculo.CamerasImagens[i].Camera.Precisao) >= acessoVeiculo.CamerasImagens[i].PrecisaoImage)
                {
                    //Validação da 
                }
                else
                {

                }

                //Verificar se alguma placa passa do 
            }

            return true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// /// <param name="date"></param>
        private void RegistraLogAvigilon(string message, DateTime date)
        {
            IRepositorioLogAvigilon repLogAvigilon = Fabrica.Instancia.ObterRepositorio<IRepositorioLogAvigilon>(UnidadeTrabalho);
            Log_Avigilon log = new Log_Avigilon();
            log.DataRegistro = date;
            log.Message = message;
            log.Url = "";
            log.UserName = "";
            log.StackTrace = "";
            repLogAvigilon.Salvar(log);
            
        }

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="acessoVeiculo"></param>
        ///// <param name="placa"></param>
        ///// <returns></returns>
        //private bool ValidaAcessoGrupoLeitora(AcessoVeiculo acessoVeiculo)
        //{

        //}


        //private T serializaObjeto<T>(object obj)
        //{            
        //        return (T) obj;            
        //}

        private string serializaObjeto(object obj)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(obj);
        }


        private delegate string TempoHandler(object sender, ValidaTempoEventArgs args);

        private event TempoHandler tempoEvento;
        
        private string ValidaTempo()
        {
           return tempoEvento(this, new ValidaTempoEventArgs { Tempo = tempo.ElapsedMilliseconds });
        }

        private class ValidaTempoEventArgs : EventArgs
        {
            public long Tempo { get; set; }
        }

        async Task<string> ParametroTempo()
        {
            while (tempo.ElapsedMilliseconds < 1000)
            {

            }

            return "false";


        }

        public ActionResult Index()
        {
            return View();
        }

        private ActionResult View()
        {
            throw new NotImplementedException();
        }


    }
}
