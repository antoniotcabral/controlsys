﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Pessoas;
using Controlsys.Dominio.CredenciamentoVeiculos;
using Controlsys.Infra;
using Controlsys.Repositorio.Acesso;
using Globalsys;

using System.Drawing;

using System.Threading.Tasks;

using System.Web.Http;

using Controlsys.Repositorio.Parametros;
using System.IO;
using System.Diagnostics;
using Controlsys.Repositorios.Acesso;
using Controlsys.Repositorios.Parametros;
using Controlsys.Repositorio.Audit;
using Controlsys.Dominio.Audit;
using AvigilonDotNet;
using System.Net;
using System.Threading;
using System.Text;
using System.Globalization;
using System.Web.Hosting;
using Controlsys.Dominio.Parametros;
using Controlsys.Repositorios.Pessoas;
using Controlsys.Repositorios.Seguranca;
using Controlsys.Dominio.Seguranca;
using Controlsys.Interfaces.Controllers;

//namespace Controlsys.Interfaces.Controllers
namespace IntegradorAvigilonCC.Controllers
{
    [ExtendController]
    public class AtivadorController : Controller, IControllerBase
    {
        IUnidadeTrabalho unidade;
        private Bitmap m_bitmap;
        //public static bool SuccessTentativa { get; set; }
        //public static int InfoControladora { get; set; }
        static readonly object GravaTentativa = new object();
        static readonly object LockInfoControladora = new object();
        List<ImagemPlaca> imagemPlacas = new List<ImagemPlaca>();
        DateTime dataRegistro = new DateTime();
        string paramentroUrlCaminhoImg = "";
        List<Camera> cameras = null;
        Bitmap imagem = null;
        string placa = "";
        string placaMaiorPrecisao = "";
        AcessoVeiculo acesso = new AcessoVeiculo();
        IEventDeviceLprPlateFound lpr = null;
        CameraImagem cameraImagem = new CameraImagem();
        List<CameraImagem> LPR = new List<CameraImagem>();
        List<CameraImagem> Detectadas = new List<CameraImagem>();
        //List<CameraBitmap> cameraBitmaps = new List<CameraBitmap>();
        List<bool> statusCameras = null;
        List<string> ImagensGravadas = new List<string>();
        float precisao = 0f;
        float maiorPrecisao = 0f;
        int parametroExpirar = int.MaxValue;
        List<Bitmap> bitmaps = new List<Bitmap>();
        IAvigilonControlCenter controlCenter = null;
        RequestParams m_params = new RequestParams();
        INvr nvr = null;
        string nomeDevice = null;
        ManualResetEvent oSignalEvent = new ManualResetEvent(false);
        int MaxPlateFound = 10;
        int MaxImageFound = int.MinValue;
        int TotalDevices = int.MinValue;
        float parametroPrecisao = 80;
        Camera camera = null;
        string urlBase = null;
        string erro;
        int LprIndex = 0;
        DateTime dateTime = DateTime.UtcNow.AddHours(-3);
        Stopwatch tempo = new Stopwatch();
        int imagemAvigilonAltura = 0;
        int imagemAvigilonLargura = 0;
        int indexLPR = 0;
        bool validaVeiculo = true;

        private IUnidadeTrabalho UnidadeTrabalho { get; set; }
        IUnidadeTrabalho IControllerBase.UnidadeTrabalho { get; set; }

        public AtivadorController(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = unidadeTrabalho;
        }

        public AtivadorController()
        {
            
        }

        /// <summary>
        /// Verificação da obtenção de acesso do veículo
        /// </summary>
        /// <param name="IdLeitora"></param>
        /// <param name="IpControladora"></param>
        /// <param name="numCracha"></param>
        /// /// <param name="papelPermissao"></param>
        /// <returns></returns>
        //[System.Web.Http.HttpGet]        
        public void AtivarInativaControladora()
        {
            UnidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();

            var i = 0;
            while (true)
            {
                IRepositorioControladora repControladora = Fabrica.Instancia.ObterRepositorio<IRepositorioControladora>(UnidadeTrabalho);


                IRepositorioCamera repCamera = Fabrica.Instancia.ObterRepositorio<IRepositorioCamera>(UnidadeTrabalho);

                IRepositorioParametro repositorioParametro = Fabrica.Instancia.ObterRepositorio<IRepositorioParametro>(UnidadeTrabalho);

                List<Controladora> controladoras = repControladora.ObterTodos().Where(x => x.Codigo == 102).ToList();

                //List<Controladora> controladoras = repControladora.ObterTodos().Where(x => x.TipoControladora == TipoControladora.Cancela).ToList();
                ControladoraController controladoraController = new ControladoraController(UnidadeTrabalho);
              
                for (i = 0 ; i < controladoras.Count; i++)
                {
                    dateTime = DateTime.Now;
                    //RegistraLogAvigilon("Registrando  acessos veículo automático", dateTime);
                    placa = "";
                    placaMaiorPrecisao = "";
                    precisao = 0f;
                    maiorPrecisao = 0f;                    
                    parametroPrecisao = 0f;
                    LPR = null;
                    LPR = new List<CameraImagem>();
                    var controladora = repControladora.ObterTodos().Where(x => x.Codigo == controladoras[i].Codigo).Single();
                    var controladorCodigo = controladora.Codigo;

                    //controladoraController.AtivarInativarIntegradorAvigilon(controladora.Codigo, false);


                    //controladoras[i]
                    //IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
                    //Usuario usuario = repUsuario.ObterPorLogin("adm.desenv");
                    //controladora.Ativo = true;
                    //repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                    //repControladora.Atualizar(controladora);
                    //controladoraController.AtivarInativarIntegradorAvigilon(controladora.Codigo, true);

                    var altura = int.Parse(repositorioParametro.ObterTodos().Where(p => p.Nome == ParametroSistema.IMAGEM_AVIGILON_ALTURA).SingleOrDefault().Valor);
                    var largura = int.Parse(repositorioParametro.ObterTodos().Where(p => p.Nome == ParametroSistema.IMAGEM_AVIGILON_LARGURA).SingleOrDefault().Valor);
                    if (largura > 0)
                    {
                        imagemAvigilonLargura = largura;
                    }

                    if (altura > 0)
                    {
                        imagemAvigilonAltura = altura;
                    }


                    //Desativar caso controladora não tenha câmeras atreladas
                    List<Camera> cameras = repCamera.ObterTodos().Where(c => c.Controladora.Codigo == controladoras[i].Codigo).ToList();
                    if (cameras == null || cameras.Count == 0 && controladora.Ativo)
                    {

                        //Inativa Controladora
                        controladora.Ativo = false;

                        //repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                        //repControladora.Atualizar(controladora);
                        controladoraController.AtivarInativarIntegradorAvigilon(controladora.Codigo, false);
                    }

                    //Desativar caso controladora não tenha câmeras ativas
                    if (cameras.Count(c => c.Ativo == false) == cameras.Count() && controladora.Ativo)
                    {

                        //Inativa Controladora
                        controladora.Ativo = false;
                        repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                        repControladora.Atualizar(controladora);
                        controladoraController.AtivarInativarIntegradorAvigilon(controladora.Codigo, false);
                    }

                    //Faz login no sistema Avigilon
                    INvr nvr = LoginNvr();
                    if (nvr == null && controladora.Ativo)
                    {
                        //Inativa Controladora
                        controladora.Ativo = false;
                        
                        //repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                        //repControladora.Atualizar(controladora);
                        controladoraController.AtivarInativarIntegradorAvigilon(controladora.Codigo, false);
                    }

                   
                    acesso = new AcessoVeiculo();
                    acesso.Controladora = controladora;

                    //Busca placas no sistema Avigilon
                    if(nvr != null && cameras != null && cameras.Count > 0)
                    {

                        acesso = ObterPlacasAvigilonACC(acesso, cameras, nvr).Result;
                    }
                    
                              
                    if(LPR != null && LPR.Count > 0 && controladora.Ativo == false)
                    {
                        controladora.Ativo = true;

                        
                        //repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                        //repControladora.Atualizar(controladora);
                        controladoraController.AtivarInativarIntegradorAvigilon(controladora.Codigo, controladora.Ativo);
                    }         


                    if (TotalDevices == 0 && controladora.Ativo)
                    {
                        //Inativa Controladora
                        controladora.Ativo = false;
                        //repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                        //repControladora.Atualizar(controladora);
                        controladoraController.AtivarInativarIntegradorAvigilon(controladora.Codigo, false);
                    }

                    if (controladoras[i].ValidaOCR == false && controladora.Ativo == false)
                    {
                        //Ativa controladora
                        controladora.Ativo = true;
                        //repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                        //repControladora.Atualizar(controladora);
                        controladoraController.AtivarInativarIntegradorAvigilon(controladora.Codigo, controladora.Ativo);
                    }

                    if (LPR.Count == 0 && controladora.ValidaOCR == true && controladora.Ativo)
                    {
                        // Inativa controladora
                        controladora.Ativo = false;
                        //repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                        //repControladora.Atualizar(controladora);
                        controladoraController.AtivarInativarIntegradorAvigilon(controladora.Codigo, controladora.Ativo);
                    }


                    if (controladora.ValidaOCR && controladora.Ativo && LPR != null && LPR.Count > 0)
                    {
                        List<CameraImagem> acimaLPR = new List<CameraImagem>();
                        List<CameraImagem> abaixoLPR = new List<CameraImagem>();
                        var autorizacaoVeiculoGrupoLeitora = false;

                        



                        if (!String.IsNullOrEmpty(erro) && controladora.Ativo)
                        {
                            // Inativa controladora
                            controladora.Ativo = false;
                            //repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                            //repControladora.Atualizar(controladora);
                            controladoraController.AtivarInativarIntegradorAvigilon(controladora.Codigo, controladora.Ativo);

                        }

                        IRepositorioVeiculo repVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioVeiculo>(UnidadeTrabalho);
                        var veiculo = repVeiculo.ObterTodos().Where(v => v.Placa.Replace("-", "") == placa).FirstOrDefault();


                        VeiculoCredencialVeiculo veiculoCredencialVeiculo = null;
                        //Veículo não cadastrado
                        if (veiculo == null && controladora.Ativo)  
                        {
                            // Inativa controladora
                            controladora.Ativo = false;
                            //repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                            //repControladora.Atualizar(controladora);
                            controladoraController.AtivarInativarIntegradorAvigilon(controladora.Codigo, controladora.Ativo);
                        }
                        else
                        {

                            IRepositorioVeiculoCredencialVeiculo repVeiculoCredencialVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioVeiculoCredencialVeiculo>(UnidadeTrabalho);
                            veiculoCredencialVeiculo = repVeiculoCredencialVeiculo.ObterTodos().Where(vcv => vcv.Veiculo.Codigo == veiculo.Codigo).SingleOrDefault();
                            if (veiculoCredencialVeiculo == null && controladora.Ativo)
                            {
                                //Veículo sem credencial
                                // Inativa controladora
                                controladora.Ativo = false;
                                //repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                                //repControladora.Atualizar(controladora);
                                controladoraController.AtivarInativarIntegradorAvigilon(controladora.Codigo, controladora.Ativo);
                            }

                            if (veiculoCredencialVeiculo.CredencialVeiculo.Situacoes.LastOrDefault().Status != StatusCredencial.Impressa && controladora.Ativo)
                            {
                                //Credencial não impressa
                                // Inativa controladora
                                controladora.Ativo = false;
                                //repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                                //repControladora.Atualizar(controladora);
                                controladoraController.AtivarInativarIntegradorAvigilon(controladora.Codigo, controladora.Ativo);
                            }

                            if (veiculoCredencialVeiculo.CredencialVeiculo.DataRegistro > DateTime.Now && controladora.Ativo)
                            {
                                // Inativa controladora                       
                                controladora.Ativo = false;
                                //repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                                //repControladora.Atualizar(controladora);
                                controladoraController.AtivarInativarIntegradorAvigilon(controladora.Codigo, controladora.Ativo);
                            }

                            if (veiculoCredencialVeiculo.CredencialVeiculo.DataVencimento != null && veiculoCredencialVeiculo.CredencialVeiculo.DataVencimento <= DateTime.Now && controladora.Ativo)
                            {
                                // Inativa controladora
                                controladora.Ativo = false;
                                //repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                                //repControladora.Atualizar(controladora);
                                controladoraController.AtivarInativarIntegradorAvigilon(controladora.Codigo, controladora.Ativo);
                            }

                            IRepositorioAcessoTemporarioVeiculo repAcessotemporarioVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioAcessoTemporarioVeiculo>(UnidadeTrabalho);
                            //IRepositorioLeitora repLeitora = Fabrica.Instancia.ObterRepositorio<IRepositorioLeitora>(UnidadeTrabalho);

                            var acessoTemporarioVeiculo = repAcessotemporarioVeiculo.ObterTodos().Where(atv => atv.VeiculoCredencialVeiculo.Codigo == veiculoCredencialVeiculo.Codigo).ToList();
                            var PermissaoLeitora = false;
                            if (acessoTemporarioVeiculo == null || acessoTemporarioVeiculo.Count == 0 && controladora.Ativo)
                            {
                                // Inativa controladora
                                controladora.Ativo = false;
                                //repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                                //repControladora.Atualizar(controladora);
                                controladoraController.AtivarInativarIntegradorAvigilon(controladora.Codigo, controladora.Ativo);
                            }

                            IRepositorioLeitora repLeitoras = Fabrica.Instancia.ObterRepositorio<IRepositorioLeitora>(UnidadeTrabalho);
                            List<Leitora> leitoras = repLeitoras.ObterTodos().ToList();

                            AcessoTemporarioVeiculo acessoTemporario = null;
                            for (int ix = 0; ix < acessoTemporarioVeiculo.Count; ix++)
                            {
                                if (acessoTemporarioVeiculo[ix].Ativo && controladora.Ativo)
                                {
                                    for (int ii = 0; ii < acessoTemporarioVeiculo[ix].GrupoLeitora.Leitoras.Count; ii++)
                                    {
                                        for (int iix = 0; iix < leitoras.Count; iix++)
                                        {
                                            if (acessoTemporarioVeiculo[ix].GrupoLeitora.Leitoras[ii].Leitora.Codigo == leitoras[iix].Codigo)
                                            {
                                                //Ativa Controladora
                                                controladora.Ativo = true;
                                                //repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                                                //repControladora.Atualizar(controladora);
                                                controladoraController.AtivarInativarIntegradorAvigilon(controladora.Codigo, controladora.Ativo);
                                                acessoTemporario = acessoTemporarioVeiculo[ix];


                                            }

                                        }

                                    }
                                }

                            }

                            if (acessoTemporario != null && controladora.Ativo)
                            {
                                //Acesso Temporário Expirado?
                                if (acessoTemporario.DataHoraValidadeInicio > DateTime.Now || acessoTemporario.DataHoraValidadeFim < DateTime.Now)
                                {
                                    //Inativa Acesso
                                    controladora.Ativo = false;
                                    //repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                                    //repControladora.Atualizar(controladora);
                                    controladoraController.AtivarInativarIntegradorAvigilon(controladora.Codigo, controladora.Ativo);
                                }
                            }


                            //if (PermissaoLeitora == false && controladora.Ativo)
                            //{
                            //    //Papel pode ou não ter acesso ao grupo de leitoras, mas veículo não tema cesso
                            //    //Inativa acesso
                            //    controladora.Ativo = false;
                            //    repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                            //    repControladora.Atualizar(controladora);
                            //}


                            //Verificar se a placa está acima do parâmetro do LPR
                            parametroPrecisao = 0f;
                        
                           
                            //Calculo da maior precisão aferida entre as duas câmeras do sistema para colocar na mensagem do log
                            //Se pelo menos uma placa aferida estiver acima do parametro
                            //for (int xi = 0; xi < LPR.Count; xi++)
                            //{

                            //    if (LPR[xi].PrecisaoImage > LPR[xi].Camera.Precisao)
                            //    {
                            //        acimaLPR.Add(LPR[xi]);
                            //    }
                            //    else
                            //    {
                            //        abaixoLPR.Add(LPR[xi]);
                            //    }
                            //}

                            //if (acimaLPR.Count > 0 && controladora.Ativo)
                            //{
                            //    controladora.Ativo = true;
                            //    repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                            //    repControladora.Atualizar(controladora);
                            //}
                            //else
                            //{
                            //    //Verificar a maior precisão entre as câmeras 

                            //}


                        }
                        //Quando não há câmeras online ou o sistema da Avigilon estejam offline

                        //Não identificou placas
                    }
                }

                i = 0;
            }


        }


        /// <summary>
        /// Inicia a ativação e inativação de controladoras 
        /// </summary>
        /// <returns></returns>
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("/AtivarInativaControladoraAPi")]
        public void AtivarInativaControladoraAPi()
        {
            UnidadeTrabalho = Fabrica.Instancia.Obter<IUnidadeTrabalho>();



            try
            {
                IRepositorioLogAvigilon repLogAvigilon = Fabrica.Instancia.ObterRepositorio<IRepositorioLogAvigilon>(UnidadeTrabalho);
                Log_Avigilon log = repLogAvigilon.ObterTodos().Where(l => l.Message == "AtivacaoControladora").SingleOrDefault();
                AtivadorController ativa = new AtivadorController(Fabrica.Instancia.Obter<IUnidadeTrabalho>());
                ativa.RegistraLogAvigilon("Inicio da automatização da Controladora API", DateTime.Now);
                var dataInsercaoLog = DateTime.Now;
                if (log != null)
                {
                    //Inserir [LOG_AVIGILON] "AtivacaoControladora" com dataAtivacao da ativação
                    log.DataRegistro = dataInsercaoLog;
                    repLogAvigilon.Atualizar(log);
                }
                else
                {
                    ativa.RegistraLogAvigilon("AtivacaoControladora", dataInsercaoLog);
                }             
                

                Task.Run(() =>
                    {    
                       
                        //ativa.RegistraLogAvigilon("Thread veículo automático", DateTime.Now);
                        var i = 0;

                        IRepositorioParametro repParametro = Fabrica.Instancia.ObterRepositorio<IRepositorioParametro>(UnidadeTrabalho);
                        string validacao = repParametro.RecuperarParametro(ParametroSistema.ATIVACAO_CONSULTA_INTEGRADOR).Valor;
                        
                        ///Verificar status da aplicação

                        log = repLogAvigilon.ObterTodos().Where(l => l.Message == "AtivacaoControladora").SingleOrDefault();
                        if(log != null)
                        {
                            while (log.DataRegistro == dataInsercaoLog)
                            {
                               // ativa.RegistraLogAvigilon("AtivacaoControladora - " + validacao.ToLower(), dataInsercaoLog);
                              
                                if (validacao.ToLower() == "sim")
                                {


                                    IRepositorioControladora repControladora = Fabrica.Instancia.ObterRepositorio<IRepositorioControladora>(UnidadeTrabalho);

                                    IRepositorioCamera repCamera = Fabrica.Instancia.ObterRepositorio<IRepositorioCamera>(UnidadeTrabalho);

                                    IRepositorioParametro repositorioParametro = Fabrica.Instancia.ObterRepositorio<IRepositorioParametro>(UnidadeTrabalho);

                                    List<Controladora> controladoras = repControladora.ObterTodos().Where(x => x.Codigo == 102).ToList();

                                    //List<Controladora> controladoras = repControladora.ObterTodos().Where(x => x.TipoControladora == TipoControladora.Cancela).ToList();
                                    ControladoraController controladoraController = new ControladoraController(UnidadeTrabalho);

                                    for (i = 0; i < controladoras.Count; i++)
                                    {

                                        log = repLogAvigilon.ObterTodos().Where(l => l.Message == "AtivacaoControladora").ToList().Last();
                                        //ativa.RegistraLogAvigilon("Cont", dataInsercaoLog);
                                        if (log.DataRegistro != dataInsercaoLog)
                                        {
                                            ativa.RegistraLogAvigilon("Quebrando - Log:" + log.DataRegistro+ ", Data Inserção:  " + dataInsercaoLog, DateTime.Now);
                                            break;
                                        }

                                        validacao = repParametro.RecuperarParametro(ParametroSistema.ATIVACAO_CONSULTA_INTEGRADOR).Valor;

                                        if (validacao.ToLower() != "sim")
                                        {
                                            validaVeiculo = false;
                                            break;
                                        }

                                        dateTime = DateTime.Now;

                                        placa = "";
                                        placaMaiorPrecisao = "";
                                        precisao = 0f;
                                        maiorPrecisao = 0f;
                                        parametroPrecisao = 0f;
                                        LPR = null;
                                        LPR = new List<CameraImagem>();
                                        var controladora = controladoras[i];
                                        var controladorCodigo = controladora.Codigo;

                                        //controladoraController.AtivarInativarIntegradorAvigilon(controladora.Codigo, false);


                                        //controladoras[i]
                                        //IRepositorioUsuario repUsuario = Fabrica.Instancia.ObterRepositorio<IRepositorioUsuario>(UnidadeTrabalho);
                                        //Usuario usuario = repUsuario.ObterPorLogin("adm.desenv");
                                        //controladora.Ativo = true;
                                        //repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                                        //repControladora.Atualizar(controladora);
                                        //controladoraController.AtivarInativarIntegradorAvigilon(controladora.Codigo, true);

                                        var altura = int.Parse(repositorioParametro.ObterTodos().Where(p => p.Nome == ParametroSistema.IMAGEM_AVIGILON_ALTURA).SingleOrDefault().Valor);
                                        var largura = int.Parse(repositorioParametro.ObterTodos().Where(p => p.Nome == ParametroSistema.IMAGEM_AVIGILON_LARGURA).SingleOrDefault().Valor);
                                        if (largura > 0)
                                        {
                                            imagemAvigilonLargura = largura;
                                        }

                                        if (altura > 0)
                                        {
                                            imagemAvigilonAltura = altura;
                                        }


                                        //Desativar caso controladora não tenha câmeras atreladas
                                        List<Camera> cameras = repCamera.ObterTodos().Where(c => c.Controladora.Codigo == controladoras[i].Codigo).ToList();
                                        if (cameras == null || cameras.Count == 0 && controladora.Ativo)
                                        {

                                            //Inativa Controladora
                                            controladora.Ativo = false;

                                            //repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                                            //repControladora.Atualizar(controladora);
                                            controladoraController.AtivarInativarIntegradorAvigilon(controladora.Codigo, false);
                                        }

                                        //Desativar caso controladora não tenha câmeras ativas
                                        if (cameras.Count(c => c.Ativo == false) == cameras.Count() && controladora.Ativo)
                                        {

                                            //Inativa Controladora
                                            controladora.Ativo = false;
                                            repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                                            repControladora.Atualizar(controladora);
                                            controladoraController.AtivarInativarIntegradorAvigilon(controladora.Codigo, false);
                                        }

                                        //Faz login no sistema Avigilon
                                        INvr nvr = LoginNvr();
                                        if (nvr == null && controladora.Ativo)
                                        {
                                            ativa.RegistraLogAvigilon("Desativando", DateTime.Now);
                                            //Inativa Controladora
                                            controladora.Ativo = false;
                                            ativa.RegistraLogAvigilon("Desativando Sistema Avigilon Offline", DateTime.Now);
                                            //repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                                            //repControladora.Atualizar(controladora);
                                            controladoraController.AtivarInativarIntegradorAvigilon(controladora.Codigo, false);
                                        }


                                        acesso = new AcessoVeiculo();
                                        acesso.Controladora = controladora;

                                        //Busca placas no sistema Avigilon
                                        if (nvr != null && cameras != null && cameras.Count > 0)
                                        {

                                            acesso = ObterPlacasAvigilonACC(acesso, cameras, nvr).Result;
                                        }


                                        if (LPR != null && LPR.Count > 0 && controladora.Ativo == false)
                                        {
                                            controladora.Ativo = true;
                                            ativa.RegistraLogAvigilon("Ativando", DateTime.Now);
                                            //repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                                            //repControladora.Atualizar(controladora);

                                            //for (int iLPR = 0; iLPR < LPR.Count; iLPR++)
                                            //{
                                            //if (LPR[iLPR].AcessoVeiculo.DataRegistro <= DateTime.)
                                            //}
                                            
                                            //var logAtivo = repLogAvigilon.ObterTodos().Where(l => l.DataRegistro < DateTime.Now.AddSeconds(-10)).LastOrDefault();

                                           
                                            controladoraController.AtivarInativarIntegradorAvigilon(controladora.Codigo, controladora.Ativo);
                                        }


                                        if (TotalDevices == 0 && controladora.Ativo)
                                        {
                                            //Inativa Controladora
                                            controladora.Ativo = false;
                                            //repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                                            //repControladora.Atualizar(controladora);
                                            controladoraController.AtivarInativarIntegradorAvigilon(controladora.Codigo, false);
                                        }

                                        if (controladoras[i].ValidaOCR == false && controladora.Ativo == false)
                                        {
                                            //Ativa controladora
                                            controladora.Ativo = true;
                                            //repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                                            //repControladora.Atualizar(controladora);
                                            controladoraController.AtivarInativarIntegradorAvigilon(controladora.Codigo, controladora.Ativo);
                                        }

                                        if (LPR.Count == 0 && controladora.ValidaOCR == true && controladora.Ativo)
                                        {
                                            // Inativa controladora
                                            controladora.Ativo = false;
                                            //repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                                            //repControladora.Atualizar(controladora);
                                            controladoraController.AtivarInativarIntegradorAvigilon(controladora.Codigo, controladora.Ativo);
                                        }


                                        if (controladora.ValidaOCR && controladora.Ativo && LPR != null && LPR.Count > 0)
                                        {
                                            List<CameraImagem> acimaLPR = new List<CameraImagem>();
                                            List<CameraImagem> abaixoLPR = new List<CameraImagem>();
                                            var autorizacaoVeiculoGrupoLeitora = false;





                                            if (!String.IsNullOrEmpty(erro) && controladora.Ativo)
                                            {
                                                // Inativa controladora
                                                controladora.Ativo = false;
                                                //repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                                                //repControladora.Atualizar(controladora);
                                                controladoraController.AtivarInativarIntegradorAvigilon(controladora.Codigo, controladora.Ativo);

                                            }

                                            IRepositorioVeiculo repVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioVeiculo>(UnidadeTrabalho);
                                            var veiculo = repVeiculo.ObterTodos().Where(v => v.Placa.Replace("-", "") == placa).FirstOrDefault();


                                            VeiculoCredencialVeiculo veiculoCredencialVeiculo = null;
                                            //Veículo não cadastrado
                                            if (veiculo == null && controladora.Ativo)
                                            {
                                                // Inativa controladora
                                                controladora.Ativo = false;
                                                //repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                                                //repControladora.Atualizar(controladora);
                                                controladoraController.AtivarInativarIntegradorAvigilon(controladora.Codigo, controladora.Ativo);
                                            }
                                            else
                                            {

                                                IRepositorioVeiculoCredencialVeiculo repVeiculoCredencialVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioVeiculoCredencialVeiculo>(UnidadeTrabalho);
                                                veiculoCredencialVeiculo = repVeiculoCredencialVeiculo.ObterTodos().Where(vcv => vcv.Veiculo.Codigo == veiculo.Codigo).SingleOrDefault();
                                                if (veiculoCredencialVeiculo == null && controladora.Ativo)
                                                {
                                                    //Veículo sem credencial
                                                    // Inativa controladora
                                                    controladora.Ativo = false;
                                                    //repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                                                    //repControladora.Atualizar(controladora);
                                                    controladoraController.AtivarInativarIntegradorAvigilon(controladora.Codigo, controladora.Ativo);
                                                }

                                                if (veiculoCredencialVeiculo.CredencialVeiculo.Situacoes.LastOrDefault().Status != StatusCredencial.Impressa && controladora.Ativo)
                                                {
                                                    //Credencial não impressa
                                                    // Inativa controladora
                                                    controladora.Ativo = false;
                                                    //repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                                                    //repControladora.Atualizar(controladora);
                                                    controladoraController.AtivarInativarIntegradorAvigilon(controladora.Codigo, controladora.Ativo);
                                                }

                                                if (veiculoCredencialVeiculo.CredencialVeiculo.DataRegistro > DateTime.Now && controladora.Ativo)
                                                {
                                                    // Inativa controladora                       
                                                    controladora.Ativo = false;
                                                    //repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                                                    //repControladora.Atualizar(controladora);
                                                    controladoraController.AtivarInativarIntegradorAvigilon(controladora.Codigo, controladora.Ativo);
                                                }

                                                if (veiculoCredencialVeiculo.CredencialVeiculo.DataVencimento != null && veiculoCredencialVeiculo.CredencialVeiculo.DataVencimento <= DateTime.Now && controladora.Ativo)
                                                {
                                                    // Inativa controladora
                                                    controladora.Ativo = false;
                                                    //repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                                                    //repControladora.Atualizar(controladora);
                                                    controladoraController.AtivarInativarIntegradorAvigilon(controladora.Codigo, controladora.Ativo);
                                                }

                                                IRepositorioAcessoTemporarioVeiculo repAcessotemporarioVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioAcessoTemporarioVeiculo>(UnidadeTrabalho);
                                                //IRepositorioLeitora repLeitora = Fabrica.Instancia.ObterRepositorio<IRepositorioLeitora>(UnidadeTrabalho);

                                                var acessoTemporarioVeiculo = repAcessotemporarioVeiculo.ObterTodos().Where(atv => atv.VeiculoCredencialVeiculo.Codigo == veiculoCredencialVeiculo.Codigo).ToList();
                                                var PermissaoLeitora = false;
                                                if (acessoTemporarioVeiculo == null || acessoTemporarioVeiculo.Count == 0 && controladora.Ativo)
                                                {
                                                    // Inativa controladora
                                                    controladora.Ativo = false;
                                                    //repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                                                    //repControladora.Atualizar(controladora);
                                                    controladoraController.AtivarInativarIntegradorAvigilon(controladora.Codigo, controladora.Ativo);
                                                }

                                                IRepositorioLeitora repLeitoras = Fabrica.Instancia.ObterRepositorio<IRepositorioLeitora>(UnidadeTrabalho);
                                                List<Leitora> leitoras = repLeitoras.ObterTodos().ToList();

                                                AcessoTemporarioVeiculo acessoTemporario = null;
                                                for (int ix = 0; ix < acessoTemporarioVeiculo.Count; ix++)
                                                {
                                                    if (acessoTemporarioVeiculo[ix].Ativo && controladora.Ativo)
                                                    {
                                                        for (int ii = 0; ii < acessoTemporarioVeiculo[ix].GrupoLeitora.Leitoras.Count; ii++)
                                                        {
                                                            for (int iix = 0; iix < leitoras.Count; iix++)
                                                            {
                                                                if (acessoTemporarioVeiculo[ix].GrupoLeitora.Leitoras[ii].Leitora.Codigo == leitoras[iix].Codigo)
                                                                {
                                                                    //Ativa Controladora
                                                                    controladora.Ativo = true;
                                                                    //repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                                                                    //repControladora.Atualizar(controladora);
                                                                    controladoraController.AtivarInativarIntegradorAvigilon(controladora.Codigo, controladora.Ativo);
                                                                    acessoTemporario = acessoTemporarioVeiculo[ix];


                                                                }

                                                            }

                                                        }
                                                    }

                                                }

                                                if (acessoTemporario != null && controladora.Ativo)
                                                {
                                                    //Acesso Temporário Expirado?
                                                    if (acessoTemporario.DataHoraValidadeInicio > DateTime.Now || acessoTemporario.DataHoraValidadeFim < DateTime.Now)
                                                    {
                                                        //Inativa Acesso
                                                        controladora.Ativo = false;
                                                        //repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                                                        //repControladora.Atualizar(controladora);
                                                        controladoraController.AtivarInativarIntegradorAvigilon(controladora.Codigo, controladora.Ativo);
                                                    }
                                                }


                                                //if (PermissaoLeitora == false && controladora.Ativo)
                                                //{
                                                //    //Papel pode ou não ter acesso ao grupo de leitoras, mas veículo não tema cesso
                                                //    //Inativa acesso
                                                //    controladora.Ativo = false;
                                                //    repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                                                //    repControladora.Atualizar(controladora);
                                                //}


                                                //Verificar se a placa está acima do parâmetro do LPR
                                                parametroPrecisao = 0f;


                                                //Calculo da maior precisão aferida entre as duas câmeras do sistema para colocar na mensagem do log
                                                //Se pelo menos uma placa aferida estiver acima do parametro
                                                //for (int xi = 0; xi < LPR.Count; xi++)
                                                //{

                                                //    if (LPR[xi].PrecisaoImage > LPR[xi].Camera.Precisao)
                                                //    {
                                                //        acimaLPR.Add(LPR[xi]);
                                                //    }
                                                //    else
                                                //    {
                                                //        abaixoLPR.Add(LPR[xi]);
                                                //    }
                                                //}

                                                //if (acimaLPR.Count > 0 && controladora.Ativo)
                                                //{
                                                //    controladora.Ativo = true;
                                                //    repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                                                //    repControladora.Atualizar(controladora);
                                                //}
                                                //else
                                                //{
                                                //    //Verificar a maior precisão entre as câmeras 

                                                //}


                                            }
                                            //Quando não há câmeras online ou o sistema da Avigilon estejam offline

                                            //Não identificou placas
                                        }
                                    }

                                    i = 0;
                                }
                                else
                                {
                                   
                                    Thread.Sleep(1000);
                                    IRepositorioControladora repControladora = Fabrica.Instancia.ObterRepositorio<IRepositorioControladora>(UnidadeTrabalho);

                                    IRepositorioCamera repCamera = Fabrica.Instancia.ObterRepositorio<IRepositorioCamera>(UnidadeTrabalho);

                                    IRepositorioParametro repositorioParametro = Fabrica.Instancia.ObterRepositorio<IRepositorioParametro>(UnidadeTrabalho);

                                    List<Controladora> controladoras = repControladora.ObterTodos().Where(x => x.Codigo == 102).ToList();

                                    ControladoraController controladoraController = new ControladoraController(UnidadeTrabalho);

                                    for (int ii = 0; ii < controladoras.Count; ii++)
                                    {
                                        if (!controladoras[ii].Ativo)
                                        {
                                            var controladora = controladoras[i];
                                            var controladorCodigo = controladora.Codigo;
                                            controladora.Ativo = true;
                                            //repControladora.Validar(controladora, Globalsys.Validacao.EstadoObjeto.Alterado);
                                            //repControladora.Atualizar(controladora);
                                            controladoraController.AtivarInativarIntegradorAvigilon(controladora.Codigo, controladora.Ativo);
                                        }

                                    }
                                }
                            }
                        }
                      

                    }).ConfigureAwait(false);
               

            }
            catch (Exception ex)
            {

            }
           
            
            


         
           

        }



        /// <summary>
        /// Solicita informações de Cameras, placas e imagens da controladora
        /// </summary>
        /// <param name="acessoVeiculo"></param>
        /// <param name="cameras"></param>
        /// <returns></returns>
        private async Task<AcessoVeiculo> ObterImagensAvigilonACC(AcessoVeiculo acessoVeiculo, List<Camera> cameras, INvr nvr)
        {
            try
            {
                for (int i = 0; i < cameras.Count; i++)
                {
                    camera = cameras[i];

                    if (cameras[i].Ativo && cameras[i].Controladora.ValidaOCR == true)
                    {

                        CameraImagem PlacaPrecisao = null;
                        var acessoImage = ObterImagemACC(cameras[i], acessoVeiculo, nvr);
                        if (tempo.ElapsedMilliseconds > parametroExpirar)
                        {
                            return acessoImage;
                        }
                        if (acessoImage.Controladora.ValidaOCR)
                        {

                            CameraImagem PlacaEPrecisao = new CameraImagem(); ;
                            acessoImage = ObterLprAcc(cameras[i], ref acessoVeiculo, ref cameraImagem, nvr);

                            if (tempo.ElapsedMilliseconds > parametroExpirar)
                            {
                                return acessoImage;
                            }
                            /////RegistraLogAvigilon("Placas result", DateTime.Now);
                            Detectadas.Add(new CameraImagem()
                            {
                                AcessoVeiculo = acessoImage,
                                Camera = cameraImagem.Camera,
                                PlacaVeiculo = cameraImagem.PlacaVeiculo,
                                PrecisaoImage = cameraImagem.PrecisaoImage
                            });
                            ////RegistraLogAvigilon("Placas result 2", DateTime.Now);
                        }
                        acessoVeiculo = acessoImage;
                    }
                }
                return acessoVeiculo;
                //if (acessoVeiculo.CamerasImagens.Count > 0 || TotalDevices == 0)
                //{

                //    return acessoVeiculo;

                //}
                //else
                //{
                //    ////RegistraLogAvigilon("Retorno nulo de método deimagem", DateTime.Now);
                //    return null;
                //}
            }

            catch (Exception exAvigilon)
            {
                Console.WriteLine(exAvigilon.Message);
                //RegistraLogAvigilon(exAvigilon.Message, DateTime.Now);
                return null;
            }

        }

        /// <summary>
        /// Solicita informações de Cameras, placas e imagens da controladora
        /// </summary>
        /// <param name="acessoVeiculo"></param>
        /// <param name="cameras"></param>
        /// <returns></returns>
        private async Task<AcessoVeiculo> ObterPlacasAvigilonACC(AcessoVeiculo acessoVeiculo, List<Camera> cameras, INvr nvr)
        {
            try
            {
                LPR = null;
                LPR = new List<CameraImagem>();
                for (int i = 0; i < cameras.Count; i++)
                {
                    camera = cameras[i];

                    if (cameras[i].Ativo && cameras[i].Controladora.ValidaOCR == true)
                    {

                        if (acessoVeiculo.Controladora.ValidaOCR)
                        {

                            CameraImagem PlacaEPrecisao = new CameraImagem(); ;
                            acessoVeiculo = ObterLprAcc(cameras[i], ref acessoVeiculo, ref cameraImagem, nvr);

                            if (LPR.Count > 0)
                            {
                                if(LPR.Count == 1)
                                {
                                    placa = LPR[0].PlacaVeiculo;
                                    precisao = LPR[0].PrecisaoImage;
                                }
                                else
                                {
                                    var precisaoAnterior = 0f;

                                for (int ixxx = 0; ixxx < LPR.Count; ixxx++)
                                {

                                    if (LPR[ixxx].PrecisaoImage > precisaoAnterior)
                                    {
                                        precisaoAnterior = LPR[ixxx].PrecisaoImage;
                                        placa = LPR[ixxx].PlacaVeiculo;
                                        precisao = LPR[ixxx].PrecisaoImage;                                        
                                    }

                                }
                                }
                                ////RegistraLogAvigilon("Maior Placa", DateTime.UtcNow.AddHours(-3));
                                
                                ////RegistraLogAvigilon("Maior placa 2", DateTime.UtcNow.AddHours(-3));
                            }

                            if (tempo.ElapsedMilliseconds > parametroExpirar)
                            {
                                return acessoVeiculo;
                            }
                            /////RegistraLogAvigilon("Placas result", DateTime.Now);
                            Detectadas.Add(new CameraImagem()
                            {
                                AcessoVeiculo = acessoVeiculo,
                                Camera = cameraImagem.Camera,
                                PlacaVeiculo = cameraImagem.PlacaVeiculo,
                                PrecisaoImage = cameraImagem.PrecisaoImage
                            });
                            ////RegistraLogAvigilon("Placas result 2", DateTime.Now);
                        }
                    }
                }
                return acessoVeiculo;
            }

            catch (Exception exAvigilon)
            {
                Console.WriteLine(exAvigilon.Message);
                //RegistraLogAvigilon(exAvigilon.Message, DateTime.Now);
                return null;
            }

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="controladora"></param>
        /// <param name="papel"></param>
        private void ObterLprAvigilonACC(Controladora controladora, Papel papel, ref CameraImagem cameraImagem, Camera camera)
        {
            //      ObterLprAcc(camera, ref acesso, controladora, "", cameraImagem);

        }

        /// <summary>
        /// Seta a  acesso baseado no grupo de leitoras
        /// </summary>
        /// <param name="acessoVeiculo"></param>
        /// <returns></returns>
        private bool SetAcessoVeiculo(AcessoVeiculo acessoVeiculo)
        {

            //Log de acesso


            //Valida acesso
            return true;
        }

        /// <summary>
        /// //Verifica a permissão baseado no LPR e faz o cadastro da tentativa de acesso
        /// </summary>
        /// <param name="acessoVeiculo"></param>
        /// /// <param name="indexCameraImagem"></param>
        /// <param name="controladora"></param>
        /// <returns></returns>
        private bool LogPermissaoLPR(
            AcessoVeiculo acessoVeiculo, int indexCameraImagem,
            Controladora controladora)
        {
            var parametroPrecisao = 100;

            //Verifica se o Veículo tem acesso ao GrupoLeitora que está tentando acessar
            //-------------------------------------------------------------------------// 

            var autorizacaoGrupoLeitora = ValidaPlaca(acessoVeiculo);

            if (!autorizacaoGrupoLeitora)
            {
                acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_GRUPO_LEITORA;
                acessoVeiculo.StatusAutorizacao = false;
            }
            else if (acessoVeiculo.CamerasImagens[indexCameraImagem].PrecisaoImage >= parametroPrecisao)
            {
                if (autorizacaoGrupoLeitora)
                {
                    //Permitido o acesso ao local, LPR ativo e acima do parametro do sistema
                    acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_AUTORIZADO_LPR_ATIVO_ACIMA;
                    acessoVeiculo.StatusAutorizacao = true;
                }
                else
                {
                    //Permitido o acesso ao local, LPR ativo e acima do parametro do sistema
                    acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_LPR_ATIVO_ACIMA;
                    acessoVeiculo.StatusAutorizacao = false;
                }
            }
            else
            {
                acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_LPR_ATIVO_ABAIXO;
                acessoVeiculo.StatusAutorizacao = false;
            }

            Task.Run(() =>
            {
                LogAcessoVeiculo(acessoVeiculo, controladora, autorizacaoGrupoLeitora);
            }).ConfigureAwait(false);


            return autorizacaoGrupoLeitora;
        }

        /// <summary>
        /// //Verifica a permissão baseado em câmera Inativas e faz o cadastro da tentativa de acesso
        /// </summary>
        /// <param name="acessoVeiculo"></param>
        ///<param name="papelPermissao"></param>
        /// <returns></returns>
        private bool VerificarCamerasNaoVinculadas(AcessoVeiculo acessoVeiculo, bool papelPermissao)
        {
            //Câmeras não vinculadas a controladora
            if (papelPermissao)
            {
                var permissaoAcesso = true;
                acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERAS_NAO_VINCULADAS;

                LogAcessoVeiculo(acessoVeiculo, acessoVeiculo.Controladora, permissaoAcesso);
                return true;
            }
            else
            {
                var permissaoAcesso = false;
                acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_CAMERAS_NAO_VINCULADAS;

                LogAcessoVeiculo(acessoVeiculo, acessoVeiculo.Controladora, permissaoAcesso);
                return false;
            }
        }

        /// <summary>
        /// //Verifica a permissão baseado em câmeras Vinculadas Ativas ou Inativas e faz o cadastro da tentativa de acesso
        /// </summary>
        /// <param name="acessoVeiculo"></param>
        /// <param name="controladora"></param>
        ///<param name="cameras"></param>
        ///<param name="lpr"></param>
        /// <returns></returns>
        private bool VerificarPermissaoSemLPR(
            ref AcessoVeiculo acessoVeiculo,
            Controladora controladora, List<Camera> cameras, bool lpr, bool permissaoAcesso)
        {

            //Somente câmeras inativas
            if (cameras != null && cameras.Count > 0 && cameras.Count(c => c.Ativo == false) == cameras.Count)
            {

                if (permissaoAcesso)
                {
                    acessoVeiculo.StatusAutorizacao = true;
                    acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_INATIVA;
                    LogAcessoVeiculo(acessoVeiculo, controladora, permissaoAcesso);
                    return true;
                }
                else
                {
                    acessoVeiculo.StatusAutorizacao = false;
                    acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_CAMERA_INATIVA;
                    LogAcessoVeiculo(acessoVeiculo, controladora, permissaoAcesso);
                    return false;
                }
            }

            //Há uma ou mais câmeras ativa(s)
            if (cameras != null && cameras.Count > 0)
            {
                if (acessoVeiculo.Controladora.ValidaOCR)
                {
                    //LPR Ativo, cameras ativas
                    if (cameras != null && cameras.Count(c => c.Ativo) > 0)
                    {
                        acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_INATIVA;
                        acessoVeiculo.StatusAutorizacao = true;
                        LogAcessoVeiculo(acessoVeiculo, acessoVeiculo.Controladora, true);
                        return true;
                    }
                    //LPR Ativo, camneras Inativas
                    else
                    {
                        acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_CAMERA_INATIVA;
                        acessoVeiculo.StatusAutorizacao = false;
                        LogAcessoVeiculo(acessoVeiculo, acessoVeiculo.Controladora, false);
                        return false;
                    }

                }
                else
                {
                    //LPR Inativo, cameras Ativas
                    if (permissaoAcesso)
                    {
                        acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_ATIVA_LPR_INATIVO;
                        acessoVeiculo.StatusAutorizacao = true;
                        LogAcessoVeiculo(acessoVeiculo, controladora, true);
                        return true;
                    }
                    //LPR InAtivo, cameras Ativas
                    else
                    {
                        acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_CAMERA_ATIVA_LPR_INATIVO;
                        acessoVeiculo.StatusAutorizacao = false;
                        LogAcessoVeiculo(acessoVeiculo, controladora, false);
                        return false;

                    }
                }
            }
            else
            {
                //camerasOff
                if (permissaoAcesso)
                {
                    acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_OFFLINE;
                    acessoVeiculo.StatusAutorizacao = true;
                    LogAcessoVeiculo(acessoVeiculo, controladora, true);
                    return true;
                }
                else
                {
                    acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_CAMERA_OFFLINE;
                    acessoVeiculo.StatusAutorizacao = true;
                    LogAcessoVeiculo(acessoVeiculo, controladora, true);
                    return false;
                }
            }
        }

        /// <summary>
        /// Log de tentativa de acesso onde as câmeras estão offline
        /// </summary>
        /// <param name="acessoVeiculo"></param>
        /// <param name="controladora"></param>
        ///<param name="cameras"></param>
        ///<param name="lpr"></param>
        /// <returns></returns>
        private bool LogCamerasOffline(
            ref AcessoVeiculo acessoVeiculo)
        {
            var permissaoAcesso = false;



            if (acessoVeiculo.StatusAcesso == StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_OFFLINE)
            {
                permissaoAcesso = true;
                LogAcessoVeiculo(acessoVeiculo, acessoVeiculo.Controladora, permissaoAcesso);
                return true;
            }
            else
            {
                acessoVeiculo.StatusAcesso = StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_CAMERA_OFFLINE;
                LogAcessoVeiculo(acessoVeiculo, acessoVeiculo.Controladora, permissaoAcesso);
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="camera"></param>
        /// <param name="acessoVeiculo"></param>
        /// <param name="controladora"></param>
        /// <param name="channelIdParam"></param>
        /// <param name="imagem"></param>
        /// <returns></returns>
        private AcessoVeiculo ObterImagemACC(Camera camera, AcessoVeiculo acessoVeiculo)
        {
            try
            {
                String hostname = "10.10.2.37";
                int port = 38880; //38888
                String username = "administrator";
                String password = "123@Mudar";
                String channelId = "0a14312e3030313838353063376335642e63616d30301a10f355e06e8ced443d908e9fc462336a12221098b315b7044c438a917f8f83c5d19591380a";
                const int paramentroEspera = 1000;
                const int nvrConnectWait = paramentroEspera; // (seconds)
                nomeDevice = camera.Nome;
                IDataStore connectedDataStore = null;
                // var retorno = "";
                //-----Init Control Center

                IPAddress addr = null;
                try
                {
                    addr = IPAddress.Parse(hostname);
                }
                catch (Exception exIp)
                {
                    return null;
                    throw new Exception("IP Address is invalid.");
                }

                Debug.WriteLine("Init Control Center");

                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();
                AvigilonSdk sdk = new AvigilonSdk();

                SdkInitParams initParams = new SdkInitParams(
                    AvigilonSdk.MajorVersion,

                    AvigilonSdk.MinorVersion)
                {
                    //Era False
                    ServiceMode = true,
                    AutoDiscoverNvrs = true
                };
                initParams.SdkPath = urlBase + "packages\\AvigilonSdkDotNet.dll";
                controlCenter = sdk.CreateInstance(initParams);


                if (controlCenter == null)
                {
                    return null;

                }
                controlCenter.NvrListAdd += new AvigilonDotNet.UuidHandler﻿(m_sharedAcc_NvrListAdd﻿﻿﻿);

                //CameraIP
                IPEndPoint endpoint = new IPEndPoint(addr, port);
                DateTime waitEnd = DateTime.Now + new TimeSpan(0, 0, nvrConnectWait);


                //controlCenter.NvrListAdd += new AvigilonDotN﻿e﻿t.UuidHandler﻿﻿(m_sharedAcc_NvrListAd﻿d﻿﻿﻿﻿﻿﻿);
                AvigilonDotNet.AvgError error = controlCenter.AddNvr(endpoint);

                Stopwatch tempo = new Stopwatch();
                tempo.Start();
                while (nvr == null)
                {
                    nvr = controlCenter.GetNvr(endpoint.Address);

                    if (tempo.ElapsedMilliseconds > paramentroEspera * 1000) return null;
                }
                tempo.Stop();

                var result = nvr.Login(username, password);
                nvr.DeviceListAdd += new AvigilonDotNet.UuidHandler(NVR_DeviceLi﻿stAdd﻿);
                var tentativaDevices = 10;

                while (nvr.Devices.Count == 0 || tentativaDevices > 0)
                {

                    tentativaDevices--;

                    var camerasLigadas = 0;
                    if (nvr.Devices.Count > 0)
                    {
                        TotalDevices = nvr.Devices.Count;
                        camerasLigadas = nvr.Devices.Count(d => d.Connected);
                    }

                    if (camerasLigadas > 0)
                    {
                        var device = nvr.Devices.Where(d => d.DisplayName == camera.Nome).SingleOrDefault().Entities.Where(T => T.DisplayName == nomeDevice && T.Type == EntityType.Camera).SingleOrDefault();
                        if (device == null)
                        {
                            return null;
                        }
                        IRequestor m_requestor = controlCenter.CreateRequestor();
                        m_params.DeviceId = device.DeviceId;
                        m_params.NvrUuid = device.ReportingNvrUuid;
                        m_params.Timestamp = DateTime.Now.AddSeconds(-10);
                        m_params.DataStoreUuid = device.ReportingDataStoreUuid;
                        m_requestor.ResultFrameReceived += ImageRequestResultFrameHandler_;

                        //var eventTypes = new List<EventType>();
                        //eventTypes.Add(AvigilonDotNet.EventType.DeviceLprPlateFound);
                        Stopwatch stopImg0 = new Stopwatch();
                        stopImg0.Start();
                        m_requestor.Query(
                               1,
                               m_params);

                        // m_requestor.Query(1, m_params);
                        try

                        {
                            sdk.Shutdown();

                            Stopwatch stopImg = new Stopwatch();
                            stopImg.Start();
                            var tempoPassdo = stopImg.ElapsedMilliseconds;

                            while (MaxImageFound < 0)
                            {
                                if (stopImg.ElapsedMilliseconds > 15000) break;

                            }

                            MaxImageFound = int.MinValue;
                            stopImg0.Stop();
                            stopImg.Stop();
                            cameraImagem.AcessoVeiculo = acessoVeiculo;
                            cameraImagem.Camera = camera;
                            acessoVeiculo.CamerasImagens.Add(cameraImagem);
                            statusCameras.Add(true);
                            //if(acessoVeiculo.Controladora != null)
                            //{
                            //    if (acessoVeiculo.Controladora.ValidaLPR)
                            //    {
                            //        acessoVeiculo = ObterLprAcc(camera, ref acessoVeiculo, ref cameraImagem, "");
                            //        cameraImagem.PrecisaoImage = precisao;
                            //        cameraImagem.PlacaVeiculo = placa;
                            //    }
                            //}


                            acessoVeiculo.DataRegistro = DateTime.Now;

                            return acessoVeiculo;
                        }
                        catch
                        {
                            return null;
                        }
                    }

                }
                if (nvr.Devices == null || nvr.Devices.Count(c => c.Connected) == 0 || nvr.Devices.Count == 0)
                {
                    TotalDevices = 0;
                    return acessoVeiculo;
                }
                else
                {
                    return null;
                }
                //return acessoVeiculo;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }

        }

        /// <summary>
        /// Já logado, solcita a imagem capturada pela Avigilon Control Center
        /// </summary>
        /// <param name="camera"></param>
        /// <param name="acessoVeiculo"></param>
        /// <param name="nvr"></param>
        /// <returns></returns>
        private AcessoVeiculo ObterImagemACC(Camera camera, AcessoVeiculo acessoVeiculo, INvr nvr)
        {
            try
            {
                nomeDevice = camera.Nome;
                var tentativaDevices = 1000;
                Stopwatch stop = new Stopwatch();
                stop.Start();

                while (nvr.Devices.Count > 0 && tentativaDevices > 0 && stop.ElapsedMilliseconds < 6000)
                {

                    tentativaDevices--;

                    var camerasLigadas = 0;
                    if (nvr.Devices.Count > 0)
                    {
                        TotalDevices = nvr.Devices.Count;
                        camerasLigadas = nvr.Devices.Count(d => d.Connected);

                    }

                    if (camerasLigadas > 0)
                    {
                        var device = nvr.Devices.Where(d => d.DisplayName == camera.Nome).SingleOrDefault().Entities.Where(T => T.DisplayName == nomeDevice && T.Type == EntityType.Camera).SingleOrDefault();
                        tentativaDevices = 1;
                        if (device == null)
                        {
                            return acessoVeiculo;
                        }
                        IRequestor m_requestor = controlCenter.CreateRequestor();
                        m_params.DeviceId = device.DeviceId;
                        m_params.NvrUuid = device.ReportingNvrUuid;
                        m_params.Timestamp = DateTime.Now;

                        //Setando a resolução esperada da imagem consultada no sistema Avigilon
                        m_params.Resolution = new Size
                        {
                            Height = imagemAvigilonAltura,
                            Width = imagemAvigilonLargura
                        };
                        m_params.DataStoreUuid = device.ReportingDataStoreUuid;

                        MaxImageFound = 0;
                        cameraImagem.AcessoVeiculo = acessoVeiculo;
                        cameraImagem.Camera = camera;
                        acessoVeiculo.DataRegistro = DateTime.Now;
                        m_requestor.ResultFrameReceived += ImageRequestResultFrameHandler_;

                        m_requestor.Query(
                               1,
                               m_params);

                        try

                        {
                            Stopwatch stopwatch = new Stopwatch();
                            stopwatch.Start();
                            while (MaxImageFound == 0 || stopwatch.ElapsedMilliseconds < 5000)
                            {
                                if (MaxImageFound > 0 || stopwatch.ElapsedMilliseconds > 5000)
                                {
                                    break;
                                }
                            }
                            stopwatch.Stop();
                            MaxImageFound = 0;

                            acessoVeiculo.CamerasImagens.Add(cameraImagem);
                            statusCameras.Add(true);


                            return acessoVeiculo;
                        }
                        catch
                        {
                            return null;
                        }
                    }
                }
                stop.Stop();
                if (nvr.Devices == null || nvr.Devices.Count(c => c.Connected) == 0 || nvr.Devices.Count == 0)
                {
                    TotalDevices = 0;
                    return acessoVeiculo;
                }
                else
                {
                    return acessoVeiculo;
                }
                //return acessoVeiculo;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }

        }

        /// <summary>
        /// Logar no AvigilonControl Center e retorn o Network Video Recorder A.K.A. Nvr
        /// </summary>
        /// <returns></returns>
        private INvr LoginNvr()
        {
            nvr = null;
            IRepositorioParametro repositorioParametro = Fabrica.Instancia.ObterRepositorio<IRepositorioParametro>(UnidadeTrabalho);
            //var parametro = repositorioParametro.ObterTodos().Where(p => p.Nome.ObterDescricaoEnum() == ParametroAvigilon.IP_AVIGILON.ObterDescricaoEnum());
            var host = repositorioParametro.ObterTodos().Where(p => p.Nome == ParametroSistema.LOGIN_AVIGILON).Single().Valor;
            //var hostParam = ParametroSistema.IP_AVIGILON.ObterDescricaoEnum();
            String hostname = repositorioParametro.ObterTodos().Where(p => p.Nome == ParametroSistema.IP_AVIGILON).SingleOrDefault().Valor;
            int port = Int32.Parse(repositorioParametro.ObterTodos().Where(p => p.Nome == ParametroSistema.PORTA_AVIGILON).SingleOrDefault().Valor);

            //38880; //55080

            String username = repositorioParametro.ObterTodos().Where(p => p.Nome == ParametroSistema.LOGIN_AVIGILON).SingleOrDefault().Valor;
            //"administrator";
            String password = repositorioParametro.ObterTodos().Where(p => p.Nome == ParametroSistema.PASSW_AVIGILON).SingleOrDefault().Valor;
            //"123@Mudar";
            //String channelId = "0a14312e3030313838353063376335642e63616d30301a10f355e06e8ced443d908e9fc462336a12221098b315b7044c438a917f8f83c5d19591380a";
            int paramentroEspera = Int32.Parse(repositorioParametro.ObterTodos().Where(p => p.Nome == ParametroSistema.ESPERA_AVIGILON).SingleOrDefault().Valor);
            int nvrConnectWait = paramentroEspera; // (seconds)

            IDataStore connectedDataStore = null;
            // var retorno = "";
            //-----Init Control Center

            IPAddress addr = null;
            try
            {
                addr = IPAddress.Parse(hostname);
            }
            catch (Exception exIp)
            {
                return null;
                throw new Exception("IP Address is invalid.");
            }

            AvigilonSdk sdk = new AvigilonSdk();

            SdkInitParams initParams = new SdkInitParams(
                AvigilonSdk.MajorVersion,

                AvigilonSdk.MinorVersion)
            {
                //Era False
                ServiceMode = true,
                AutoDiscoverNvrs = true
            };
            initParams.SdkPath = urlBase + "bin\\AvigilonSdkDotNet.dll";
            //RegistraLogAvigilon(urlBase + "bin\\AvigilonSdkDotNet.dll", dateTime);
            controlCenter = sdk.CreateInstance(initParams);

            if (controlCenter == null)
            {
                //RegistraLogAvigilon("controlCenter nulo", dateTime);
                return null;
            }

            //RegistraLogAvigilon(controlCenter.ToString(), dateTime);
            controlCenter.NvrListAdd += new AvigilonDotNet.UuidHandler﻿(m_sharedAcc_NvrListAdd﻿﻿﻿);

            //CameraIP
            IPEndPoint endpoint = new IPEndPoint(addr, port);
            DateTime waitEnd = DateTime.Now + new TimeSpan(0, 0, nvrConnectWait);

            //controlCenter.NvrListAdd += new AvigilonDotN﻿e﻿t.UuidHandler﻿﻿(m_sharedAcc_NvrListAd﻿d﻿﻿﻿﻿﻿﻿);
            AvigilonDotNet.AvgError error = controlCenter.AddNvr(endpoint);
            System.Threading.Thread.Sleep(100);
            Stopwatch tempo = new Stopwatch();
            tempo.Start();
            
            while (nvr == null)
            {
                nvr = controlCenter.GetNvr(endpoint.Address);
                
                if (tempo.ElapsedMilliseconds > 10000)
                {
                    //RegistraLogAvigilon("Nvr nulo", dateTime);
                    break;
                }
            }

            tempo.Stop();
            if (nvr == null)
            {
                //RegistraLogAvigilon("Não foi possível logar no servidor Avigilon", dateTime);
                return nvr;
            }
            else
            {
                ////RegistraLogAvigilon("Nvr nulo: " + nvr.Valid.ToString(), dateTime);
                
                //RegistraLogAvigilon("Logou no servidor Avigilon", dateTime);
                var result = nvr.Login(username, password);
                nvr.DeviceListAdd += new AvigilonDotNet.UuidHandler(NVR_DeviceLi﻿stAdd﻿);
                System.Threading.Thread.Sleep(100);
                tempo.Start();
                while (!nvr.Ready || tempo.ElapsedMilliseconds < 500){
                    
                }
                tempo.Stop();
                return nvr;
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="camera"></param>
        /// <param name="acessoVeiculo"></param>
        /// <param name="cameraImagem"></param>
        /// <param name="channelIdParam"></param>
        /// <returns></returns>
        private AcessoVeiculo ObterLprAcc(Camera camera, ref AcessoVeiculo acessoVeiculo,
        ref CameraImagem cameraImagem,
          string channelIdParam)
        {
            String hostname = "10.10.2.37";
            int port = 38880; //38888
            String username = "administrator";
            String password = "123@Mudar";
            String channelId = "0a14312e3030313838353063376335642e63616d30301a10f355e06e8ced443d908e9fc462336a12221098b315b7044c438a917f8f83c5d19591380a";
            const int paramentroEspera = 1000;
            const int nvrConnectWait = paramentroEspera; // (seconds)
            nomeDevice = camera.Nome;
            IDataStore connectedDataStore = null;
            // var retorno = "";
            //-----Init Control Center

            IPAddress addr = null;
            try
            {
                addr = IPAddress.Parse(hostname);
            }
            catch (Exception exIp)
            {
                return null;
                throw new Exception("IP Address is invalid.");
            }

            Debug.WriteLine("Init Control Center");

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            AvigilonSdk sdk = new AvigilonSdk();

            SdkInitParams initParams = new SdkInitParams(
                AvigilonSdk.MajorVersion,

                AvigilonSdk.MinorVersion)
            {
                //Era False
                ServiceMode = true,
                AutoDiscoverNvrs = true
            };
            initParams.SdkPath = urlBase + "\\packages\\AvigilonSdkDotNet.dll";
            controlCenter = sdk.CreateInstance(initParams);


            if (controlCenter == null)
            {

                return null;
            }

            //CameraIP
            IPEndPoint endpoint = new IPEndPoint(addr, port);
            DateTime waitEnd = DateTime.Now + new TimeSpan(0, 0, nvrConnectWait);


            controlCenter.NvrListAdd += new AvigilonDotN﻿e﻿t.UuidHandler﻿﻿(m_sharedAcc_NvrListAd﻿d﻿﻿﻿﻿﻿﻿);
            AvigilonDotNet.AvgError error = controlCenter.AddNvr(endpoint);


            //nvr.DeviceListAdd += new AvigilonDotNet.UuidHandler(NVR_DeviceLi﻿stAdd﻿);
            Stopwatch tempo = new Stopwatch();
            tempo.Start();
            while (nvr == null)
            {
                nvr = controlCenter.GetNvr(endpoint.Address);
                if (tempo.ElapsedMilliseconds > paramentroEspera * 500) return null;
            }
            tempo.Stop();


            var result = nvr.Login(username, password);

            while (nvr.Devices != null)
            {
                if (nvr.Devices.Count(d => d.Connected == false) == nvr.Devices.Count)
                {
                    return null;
                }
                TotalDevices = nvr.Devices.Count(c => c.Connected);

                if (TotalDevices > 0)
                {
                    try

                    {
                        var device = nvr.Devices.Where(d => d.DisplayName == camera.Nome).SingleOrDefault().Entities.Where(T => T.DisplayName == camera.Nome).SingleOrDefault();
                        var devices = nvr.Devices.Where(d => d.DisplayName == camera.Nome).ToList();
                        List<string> ids = new List<string>();
                        for (int i = 0; i < devices.Count; i++)
                        {
                            ids.Add(devices[i].Entities.Where(e => e.Type == EntityType.Camera).SingleOrDefault().DeviceId);
                        }
                        var eventTypes = new List<EventType>();

                        eventTypes.Add(AvigilonDotNet.EventType.DeviceLprPlateFound);
                        eventTypes.Add(AvigilonDotNet.EventType.DeviceLprMatch);
                        eventTypes.Add(AvigilonDotNet.EventType.DeviceLprPlateLost);
                        //-------------GET PLR------------------------------ 
                        AvigilonDotNet.IQuery query;

                        query = nvr.QueryEventsDeviceId(
                              DateTime.Now.AddMinutes(-1),
                              DateTime.Now,
                              AvigilonDotNet.EventLinkMode.Closed | AvigilonDotNet.EventLinkMode.Opened |
                              AvigilonDotNet.EventLinkMode.Single | AvigilonDotNet.EventLinkMode.Undefined,
                              false, ids);

                        //-------------Pegar Placa ------------------------------
                        query.ResultReceived += ImageLPRResultFrameHandler_;

                        var vgError = query.Execute();
                        Stopwatch stopwatchLpr = new Stopwatch();
                        // m_requestor.Query(1, m_params);
                        while (MaxPlateFound > 0)
                        {
                            if (stopwatchLpr.ElapsedMilliseconds > 3000)
                            {
                                break;
                            }
                        }

                        var indexLPR = 0;
                        if (LPR.Count > 0 && acessoVeiculo.CamerasImagens.Count > 0)
                        {
                            var precisaoAnterior = 0f;

                            for (int ix = 0; ix < LPR.Count; ix++)
                            {
                                if (LPR[ix].PrecisaoImage > precisaoAnterior)
                                {
                                    acessoVeiculo.CamerasImagens.Where(c => c.Camera.Nome == camera.Nome).Single().PlacaVeiculo = LPR[indexLPR].PlacaVeiculo;
                                    acessoVeiculo.CamerasImagens.Where(c => c.Camera.Nome == camera.Nome).Single().PrecisaoImage = LPR[indexLPR].PrecisaoImage;
                                }
                            }
                        }


                        sdk.Shutdown();

                        //cameraImagem.AcessoVeiculo = acessoVeiculo;
                        //cameraImagem.Camera = camera;
                        //cameraImagem.CaminhoImagem = paramentroUrlCaminhoImg + "_CAM_" + camera.Nome + "_" + DateTime.Now.ToString() + ".jpg";

                        //statusCameras.Add(true);
                        //if (acessoVeiculo.Controladora != null)
                        //{
                        //    if (acessoVeiculo.Controladora.ValidaLPR)
                        //    {
                        //        var placaResult = ObterLprAcc(camera, ref acesso, ref cameraImagem, "");
                        //       
                        //    }
                        //}

                        //acessoVeiculo.CamerasImagens.Add(cameraImagem);
                        return acessoVeiculo;
                    }
                    catch (Exception exLPR)
                    {
                        Console.WriteLine(exLPR.Message);
                        return null;
                    }
                }

            }
            stopwatch.Stop();

            if (stopwatch.ElapsedMilliseconds / 1000 > nvrConnectWait)
            {
                return null;
            }


            switch (result)
            {
                case LoginResult.Successful:
                    Debug.WriteLine("Login  - successfuly");
                    //Log Conexão con sucesso
                    connectedDataStore = nvr;

                    break;
                case LoginResult.ErrAlreadyLoggedIn:
                    Debug.WriteLine("Login - already logged in");
                    //Log já logado
                    connectedDataStore = nvr;
                    break;
                case LoginResult.ErrBadPassword:
                    Debug.WriteLine("The given password is not valid.");
                    //Log erro de senha
                    throw new Exception("The given password is not valid.");
                default:
                    //Log Erro inexperado ao logar no NVR
                    Debug.WriteLine("Unexpected error occurred while logging in to the NVR.");
                    throw new Exception("Unexpected error occurred while logging in to the NVR.");
            }

            return null;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="camera"></param>
        /// <param name="acessoVeiculo"></param>
        /// <param name="cameraImagem"></param>
        /// <param name="nvr"></param>
        /// <returns></returns>
        private AcessoVeiculo ObterLprAcc(Camera camera, ref AcessoVeiculo acessoVeiculo,
        ref CameraImagem cameraImagem, INvr nvr)
        {
            while (nvr.Devices != null)
            {
                
                //TotalDevices = nvr.Devices.Count(c => c.Connected);
                TotalDevices = 1;
                if (TotalDevices >= 0)
                {
                    try
                    {
                        //var device = nvr.Devices.Where(d => d.DisplayName == camera.Nome).SingleOrDefault().Entities.Where(T => T.DisplayName == camera.Nome).SingleOrDefault();
                        var devices = nvr.Devices.Where(d => d.DisplayName == camera.Nome).ToList();
                        List<string> ids = new List<string>();
                        for (int i = 0; i < devices.Count; i++)
                        {
                            ids.Add(devices[i].Entities.Where(e => e.Type == EntityType.Camera).SingleOrDefault().DeviceId);
                        }
                        var eventTypes = new List<EventType>();

                        eventTypes.Add(AvigilonDotNet.EventType.DeviceLprPlateFound);
                        eventTypes.Add(AvigilonDotNet.EventType.DeviceLprMatch);
                        eventTypes.Add(AvigilonDotNet.EventType.DeviceLprPlateLost);
                        //-------------GET PLR------------------------------ 
                        AvigilonDotNet.IQuery query;

                        query = nvr.QueryEventsDeviceId(
                              DateTime.Now.AddMinutes(-1),
                              DateTime.Now,
                              AvigilonDotNet.EventLinkMode.Closed | AvigilonDotNet.EventLinkMode.Opened |
                              AvigilonDotNet.EventLinkMode.Single | AvigilonDotNet.EventLinkMode.Undefined,
                              false, ids);
                        MaxPlateFound = 0;
                        //-------------Pegar Placa ------------------------------
                        query.ResultReceived += ImageLPRResultFrameHandler_;

                        var vgError = query.Execute();
                        Stopwatch stopwatch = new Stopwatch();
                        stopwatch.Start();
                        while (MaxPlateFound == 0 || stopwatch.ElapsedMilliseconds < 1000)
                        {
                            if (MaxPlateFound > 0 || stopwatch.ElapsedMilliseconds > 1000)
                            {
                                break;
                            }
                        }
                        ////RegistraLogAvigilon("Lpr obt", DateTime.UtcNow.AddHours(-3));
                        stopwatch.Stop();

                        if (LPR.Count > 0)
                        {
                            ////RegistraLogAvigilon("Maior Placa", DateTime.UtcNow.AddHours(-3));
                            var precisaoAnterior = 0f;

                            for (int ix = LprIndex; ix < LPR.Count; ix++)
                            {

                                if (LPR[ix].PrecisaoImage > precisaoAnterior)
                                {                                   
                                    precisaoAnterior = LPR[ix].PrecisaoImage;
                                    placa = LPR[ix].PlacaVeiculo;
                                    precisao = LPR[ix].PrecisaoImage;
                                    LprIndex = ix + 1;
                                }

                            }
                            ////RegistraLogAvigilon("Maior placa 2", DateTime.UtcNow.AddHours(-3));
                        }
                        else
                        {
                            ////RegistraLogAvigilon("Sem placa", DateTime.UtcNow.AddHours(-3));
                            acessoVeiculo.CamerasImagens.Where(c => c.Camera.Nome == camera.Nome).Single().PlacaVeiculo = "-";
                            acessoVeiculo.CamerasImagens.Where(c => c.Camera.Nome == camera.Nome).Single().PrecisaoImage = 0f;
                            placa = "-";
                            precisao = 0f;

                        }
                        ////RegistraLogAvigilon("Imagems obt", DateTime.UtcNow.AddHours(-3));
                        return acessoVeiculo;
                    }
                    catch (Exception exLPR)
                    {
                        Console.WriteLine(exLPR.Message);
                        return acessoVeiculo;
                    }
                }

            }
            return acessoVeiculo;
        }

        private void ImageRequestResultFrameHandler_(uint requestId, AvgError error, IFrame frame)
        {
            var fram = frame;
            //RegistraLogAvigilon("solicitando imagem", dateTime);
            //Pegar todas as imagens
            if (frame != null)
            {

                //RegistraLogAvigilon("obteve imagem", dateTime);
                IRepositorioParametro repositorioParametro = Fabrica.Instancia.ObterRepositorio<IRepositorioParametro>(UnidadeTrabalho);
                var caminhoFisicoImagem = repositorioParametro.ObterTodos().Where(p => p.Nome == ParametroSistema.CAMINHO_IMAGEM_FISICO).SingleOrDefault().Valor;
                //var imagemPath = paramentroUrlCaminhoImg + "\\Img_" + DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".jpg";
                var imagemPath = "Cam_" + cameraImagem.Camera.Nome + "_" + DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss") + ".jpg";
                cameraImagem.CaminhoImagem = imagemPath;
                ImagensGravadas.Add(imagemPath);

                MaxImageFound = 1;
                var caminhofisicocompleto = caminhoFisicoImagem + "\\" + imagemPath;
                var aa = "";
                try
                {

                    IFrameImage m_frameImage = frame as AvigilonDotNet.IFrameImage;
                    AvigilonDotNet.IFrameImageRaw fir = m_frameImage as AvigilonDotNet.IFrameImageRaw;
                    imagem = MakeBitmapFromImageRaw(fir);
                    Task.Run(() =>
                    {
                        imagem.Save(caminhoFisicoImagem + "\\" + imagemPath);
                        //RegistraLogAvigilon("salvou imagem", dateTime);
                    }).ConfigureAwait(false);


                }
                catch (Exception exImg)
                {

                    erro = exImg.Message;
                }
            }
            else
            {
                String errMsg = "Request " + requestId.ToString() + " has failed, err = " + error.ToString();
            }
        }

        private void ImageLPRResultFrameHandler_(IQueryResult result, QueryState state)
        {

            ////RegistraLogAvigilon("solicitando LPR", dateTime);
            IQueryRe﻿sultEve﻿nts﻿﻿﻿﻿﻿﻿﻿﻿﻿ eventResult = (IQueryRe﻿sultEve﻿nts﻿﻿﻿﻿﻿﻿﻿﻿﻿)result;

            IRepositorioParametro repositorioParametro = Fabrica.Instancia.ObterRepositorio<IRepositorioParametro>(UnidadeTrabalho);
            var tempoValidoLeituraLPR_Avigilon = int.Parse(repositorioParametro.ObterTodos().Where(p => p.Nome == ParametroSistema.TEMPO_VALIDO_LEITURA_LPR_AVIGILON).SingleOrDefault().Valor);
            if (!(tempoValidoLeituraLPR_Avigilon > 0))
            {
                tempoValidoLeituraLPR_Avigilon = 1000;
            }

            IRepositorioCameraImagem repCameraImagem = Fabrica.Instancia.ObterRepositorio<IRepositorioCameraImagem>(UnidadeTrabalho);
            int placasRecebidas = 0;
            for (int i = 0; i < eventResult.Results.Count; i++)
            {
                if (eventResult.Results[i].originalEvent != null)
                {
                    var devicename = ((AvigilonDotNet.IEventCategoryDevice)eventResult.Results[i].originalEvent).DeviceName;
                    var tipo = eventResult.Results[i].originalEvent.GetType().ToString();
                    if (tipo.Contains("DeviceLprPlateFound"))
                    {
                        placasRecebidas++;
                        lpr = (IEventDeviceLprPlateFound)eventResult.Results[i].originalEvent;
                        List<CameraImagem> cameraImagem =
                        repCameraImagem.ObterTodos().Where(p => p.PlacaVeiculo == lpr.LicensePlateString &&
                        p.AcessoVeiculo.DataRegistro <= DateTime.Now.AddMilliseconds(-tempoValidoLeituraLPR_Avigilon)).ToList();
                        //Placa ainda não tratada, ou seja, não houve tentativa de acesso com o veículo desta placa
                        if (cameraImagem == null || cameraImagem.Count == 0)
                        {
                            
                            LPR.Add(new CameraImagem
                            {
                                PlacaVeiculo = lpr.LicensePlateString,
                                PrecisaoImage = lpr.Confidence * 100,
                                Camera = new Camera
                                {
                                    Nome = devicename
                                }
                            });
                            //placa = lpr.LicensePlateString;
                            //precisao = lpr.Confidence * 100;
                        }
                    }
                    else if (tipo.Contains("DeviceLprPlateMatch"))
                    {
                        var lpr = (IEventDeviceLprMatch)eventResult.Results[i].originalEvent;
                        placa = lpr.DetectedLicensePlate;

                        placasRecebidas++;

                        List<CameraImagem> cameraImagem =
                        repCameraImagem.ObterTodos().Where(p => p.PlacaVeiculo == lpr.DetectedLicensePlate &&
                        p.AcessoVeiculo.DataRegistro <= DateTime.Now.AddMilliseconds(-tempoValidoLeituraLPR_Avigilon)).ToList();
                        //Placa ainda não tratada, ou seja, não houve tentativa de acesso com o veículo desta placa
                        if (cameraImagem == null || cameraImagem.Count == 0)
                        {
                            LPR.Add(new CameraImagem
                            {
                                PlacaVeiculo = lpr.DetectedLicensePlate,
                                PrecisaoImage = lpr.Confidence * 100,
                                Camera = new Camera
                                {
                                    Nome = devicename
                                }
                            });
                            //placa = lpr.DetectedLicensePlate;
                            //precisao = lpr.Confidence * 100;
                        }
                    }
                    else if (tipo.Contains("DeviceLprPlateLost"))
                    {
                        var lpr = (IEventDeviceLprPlateLost)eventResult.Results[i].originalEvent;
                        List<CameraImagem> cameraImagem =
                       repCameraImagem.ObterTodos().Where(p => p.PlacaVeiculo == lpr.LicensePlateString &&
                       p.AcessoVeiculo.DataRegistro <= DateTime.Now.AddMilliseconds(-tempoValidoLeituraLPR_Avigilon)).ToList();
                        //Placa ainda não tratada, ou seja, não houve tentativa de acesso com o veículo desta placa

                        placasRecebidas++;
                        if (cameraImagem == null || cameraImagem.Count == 0)
                        {
                            LPR.Add(new CameraImagem
                            {
                                PlacaVeiculo = lpr.LicensePlateString,
                                PrecisaoImage = lpr.Confidence * 100,
                                Camera = new Camera
                                {
                                    Nome = devicename
                                }
                            });
                            //placa = lpr.LicensePlateString;
                            //precisao = lpr.Confidence * 100;
                        }
                    }
                }

            }
            if (eventResult.Results.Count == 0)
            {
                MaxImageFound = 1;
            }
            else
            {
                MaxPlateFound = eventResult.Results.Count;
            }

            ////RegistraLogAvigilon("obteve " + placasRecebidas + " placa(s)", dateTime);
        }


        private Bitmap MakeBitmapFromImageRaw(AvigilonDotNet.IFrameImageRaw rawFrame)
        {
            Rectangle imageRect = new Rectangle(0, 0, rawFrame.Size.Width, rawFrame.Size.Height);

            switch (rawFrame.DefaultPixelFormat)
            {
                case AvigilonDotNet.PixelFormat.Gray8:
                    {
                        Bitmap retVal = new Bitmap(
                            rawFrame.Size.Width,
                            rawFrame.Size.Height,
                            System.Drawing.Imaging.PixelFormat.Format8bppIndexed);

                        // Define palette for 8bit grayscale
                        System.Drawing.Imaging.ColorPalette pal = retVal.Palette;
                        for (int ix = 0; ix < pal.Entries.Length; ++ix)
                        {
                            pal.Entries[ix] = System.Drawing.Color.FromArgb(ix, ix, ix);
                        }
                        retVal.Palette = pal;

                        System.Drawing.Imaging.BitmapData bitmapData = retVal.LockBits(
                            imageRect,
                            System.Drawing.Imaging.ImageLockMode.WriteOnly,
                            System.Drawing.Imaging.PixelFormat.Format8bppIndexed);

                        byte[] byteArray = rawFrame.GetAsArray(
                            AvigilonDotNet.PixelFormat.Gray8,
                            (ushort)bitmapData.Stride);

                        System.Runtime.InteropServices.Marshal.Copy(
                            byteArray,
                            0,
                            bitmapData.Scan0,
                            byteArray.Length);
                        retVal.UnlockBits(bitmapData);

                        return retVal;
                    }

                case AvigilonDotNet.PixelFormat.RGB24:
                    {
                        Bitmap retVal = new Bitmap(
                            rawFrame.Size.Width,
                            rawFrame.Size.Height,
                            System.Drawing.Imaging.PixelFormat.Format24bppRgb);

                        System.Drawing.Imaging.BitmapData bitmapData = retVal.LockBits(
                            imageRect,
                            System.Drawing.Imaging.ImageLockMode.WriteOnly,
                            System.Drawing.Imaging.PixelFormat.Format24bppRgb);

                        byte[] byteArray = rawFrame.GetAsArray(
                            AvigilonDotNet.PixelFormat.RGB24,
                            (ushort)bitmapData.Stride);

                        System.Runtime.InteropServices.Marshal.Copy(
                            byteArray,
                            0,
                            bitmapData.Scan0,
                            byteArray.Length);
                        retVal.UnlockBits(bitmapData);

                        return retVal;
                    }

                case AvigilonDotNet.PixelFormat.RGB32:
                    {
                        Bitmap retVal = new Bitmap(
                            rawFrame.Size.Width,
                            rawFrame.Size.Height,
                            System.Drawing.Imaging.PixelFormat.Format32bppRgb);

                        System.Drawing.Imaging.BitmapData bitmapData = retVal.LockBits(
                            imageRect,
                            System.Drawing.Imaging.ImageLockMode.WriteOnly,
                            System.Drawing.Imaging.PixelFormat.Format32bppRgb);

                        byte[] byteArray = rawFrame.GetAsArray(
                            AvigilonDotNet.PixelFormat.RGB32,
                            (ushort)bitmapData.Stride);

                        System.Runtime.InteropServices.Marshal.Copy(
                            byteArray,
                            0,
                            bitmapData.Scan0,
                            byteArray.Length);
                        retVal.UnlockBits(bitmapData);

                        return retVal;
                    }

                default:
                    return null;
            }
        }

        /// <summary>
        /// Log de Acesso do Veículo
        /// </summary>
        /// <param name="acessoVeiculo"></param>
        /// <param name="controladora"></param>
        /// <param name="acesso"></param>
        private void LogAcessoVeiculo(
            AcessoVeiculo acessoVeiculo, Controladora controladora, bool acesso = false)
        {
            try
            {
                IRepositorioAcessoVeiculo repAcessoVeiculo = Fabrica.Instancia.ObterRepositorio<IRepositorioAcessoVeiculo>(UnidadeTrabalho);
                IRepositorioCameraImagem repCameraImagem = Fabrica.Instancia.ObterRepositorio<IRepositorioCameraImagem>(UnidadeTrabalho);

                int parametro = 100;

                StringBuilder sb = new StringBuilder();
                sb.Append(acessoVeiculo.Cracha.Papel.PessoaFisica.Nome + " teve acesso ");
                switch (acessoVeiculo.StatusAcesso)
                {
                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_AUTORIZADO_LPR_ATIVO_ACIMA:

                        sb.Append("liberado às " + acessoVeiculo.DataRegistro + " por possuir permissão de acesso ao local da leitora " + acessoVeiculo.Leitora.IDLeitora + " que estava com LPR ativo, ");
                        if (placa == null)
                        {
                            //RegistraLogAvigilon("Placa null", dateTime);
                        }

                        {
                            //RegistraLogAvigilon(parametroPrecisao.ToString(), dateTime);
                        }


                        acessoVeiculo.StatusAutorizacao = true;
                        sb.Append("a placa aferida do veículo foi registrada como " + placa + " com a precisão aferida da placa de " + precisao + "%, ");
                        if (precisao > parametroPrecisao)
                        {
                            sb.Append("precisão registrada estava acima do nível de aceitável pelo sistema de Leitura de Placa que é de " + parametroPrecisao + "%.");

                        }
                        else if (precisao == parametroPrecisao)
                        {
                            sb.Append("precisão registrada estava igual ao nível de aceitável pelo sistema de Leitura de Placa que é de " + parametroPrecisao + "%.");
                        }

                        var observacaoLiberadoLPR = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoLiberadoLPR;
                        //UnidadeTrabalho.BeginTransaction();
                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        //UnidadeTrabalho.Commit();
                        for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                        {
                            repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                        }

                        break;

                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_LPR_ATIVO_ACIMA:

                        sb.Append("negado às " + acessoVeiculo.DataRegistro + "por não possuir permissão de acesso ao local da leitora" + acessoVeiculo.Leitora.IDLeitora + " que estava com LPR ativo , ");
                        acessoVeiculo.StatusAutorizacao = false;
                        sb.Append(" a placa aferida do veículo foi registrada como " + placa + " com a precisão aferida da placa de " + precisao + "%, ");
                        if (precisao > parametroPrecisao)
                        {
                            sb.Append("precisão registrada estava acima do nível de aceitável pelo sistema de Leitura de Placa que é de " + parametroPrecisao + "%.");

                        }
                        else if (precisao == parametroPrecisao)
                        {
                            sb.Append("precisão registrada estava igual ao nível de aceitável pelo sistema de Leitura de Placa que é de " + parametroPrecisao + "%.");
                        }
                        var observacaoNegadoLPR = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoNegadoLPR;
                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        //UnidadeTrabalho.Commit();
                        for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                        {
                            repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                        }
                        break;

                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_LPR_ATIVO_ABAIXO:
                        acessoVeiculo.StatusAutorizacao = false;
                        sb.Append("negado às " + acessoVeiculo.DataRegistro + "o que o veículo não foi identificado pelo sistema de Leitura de placas");
                        sb.Append(" a placa aferida do veículo foi registrada como " + placa + " com a precisão aferida da placa de " + precisao + "%, esta abaixo do parâmetro definido no sistema,");
                        sb.Append(" no local da leitora " + acessoVeiculo.Leitora.IDLeitora + " que estava com Sistema de leitura de placas ativo");

                        var observacaoVeiculoNaoIdentificado = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoVeiculoNaoIdentificado;
                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        //UnidadeTrabalho.Commit();
                        for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                        {
                            repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                        }

                        break;


                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_GRUPO_LEITORA:
                        acessoVeiculo.StatusAutorizacao = false;
                        sb.Append("negado às " + acessoVeiculo.DataRegistro + " por não possuir permissão de acesso ao local da leitora " + acessoVeiculo.Leitora.IDLeitora + " que estava com Sistema de leitura de placas ativo, ");

                        sb.Append(" a placa aferida do veículo foi registrada como " + placa + " com a precisão aferida da placa de " + precisao + "%, ");
                        if (precisao > parametroPrecisao)
                        {
                            sb.Append("precisão registrada estava acima do nível de aceitável pelo sistema de Leitura de Placa que é de " + parametroPrecisao + "%.");
                        }
                        else
                        {
                            sb.Append("precisão registrada estava abaixo do nível de aceitável pelo sistema de Leitura de Placa que é de " + parametroPrecisao + "%.");
                        }

                        var observacaoNegadoGrupoLeitora = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoNegadoGrupoLeitora;
                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        //UnidadeTrabalho.Commit();
                        for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                        {
                            repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                        }


                        break;


                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_INATIVA:

                        acessoVeiculo.StatusAutorizacao = true;
                        sb.Append("autorizado às " + acessoVeiculo.DataRegistro + " porque " + acessoVeiculo.Cracha.Papel.PessoaFisica.Nome +
                             " possuia permissão de acesso ao local da leitora " + acessoVeiculo.Leitora.IDLeitora +
                             ", não há registro fotográfico, porque no momento da passagem do crachá, não havia câmera ativa no local");

                        var observacaoLiberadoPapelVeiculoCamOffLine = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoLiberadoPapelVeiculoCamOffLine;
                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        break;

                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_CAMERA_INATIVA:

                        acessoVeiculo.StatusAutorizacao = false;
                        sb.Append("não autorizado às " + acessoVeiculo.DataRegistro + " porque " + acessoVeiculo.Cracha.Papel.PessoaFisica.Nome +
                            "não possuia permissão de acesso ao local da leitora " + acessoVeiculo.Leitora.IDLeitora +
                            ", não há registro fotográfico do momento porque no momento da passagem do crachá, não havia câmera ativa no local");

                        var observacaoCamInativa = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoCamInativa;

                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        break;


                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_ATIVA_LPR_INATIVO:
                        acessoVeiculo.StatusAutorizacao = true;

                        sb.Append("liberado às " + acessoVeiculo.DataRegistro + " no local da leitora " + acessoVeiculo.Leitora.IDLeitora +
                            ", sendo assim seu veículo não foi identificado, " +
                            "porém imagem foi tirada fotografica do carro no momento da passagem do crachá," +
                            " sistema de detecção de placas desligado");

                        var observacaoCamOnLineOCROff = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoCamOnLineOCROff;

                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        //UnidadeTrabalho.Commit();
                        for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                        {
                            repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                        }

                        break;


                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERA_OFFLINE:
                        acessoVeiculo.StatusAutorizacao = true;
                        sb.Append("autorizado às " + acessoVeiculo.DataRegistro +
                             " possuia permissão de acesso ao local da leitora " + acessoVeiculo.Leitora.IDLeitora +
                            " , a autorização levou em conta apenas a permissão do acesso da pessoa  porque a câmera(s) está/estão offline, " +
                            "sendo assim seu veículo não foi identificado, porém autorizado");

                        var observacaoCameraOffline = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoCameraOffline;
                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        break;

                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_CAMERA_OFFLINE:
                        acessoVeiculo.StatusAutorizacao = false;
                        sb.Append("não autorizado às " + acessoVeiculo.DataRegistro +
                            " não possuia permissão de acesso ao local da leitora " + acessoVeiculo.Leitora.IDLeitora +
                           " , a autorização levou em conta apenas a permissão do acesso da pessoa  porque a câmera(s) está/estão offline, " +
                            "sendo assim seu veículo não foi identificado e também não autorizado");


                        var observacaoNaoAutorizadoCameraOffline = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoNaoAutorizadoCameraOffline;
                        acessoVeiculo.CamerasImagens = null;

                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        break;


                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_AUTORIZADO_CAMERAS_NAO_VINCULADAS:
                        if (acesso)
                        {
                            acessoVeiculo.StatusAutorizacao = true;
                            sb.Append("autorizado ");
                        }
                        else
                        {
                            acessoVeiculo.StatusAutorizacao = false;
                            sb.Append("não autorizado ");
                        }

                        sb.Append("às " + acessoVeiculo.DataRegistro + ", pela validação de crachá," +
                            " não houve imagem fotografada no momento por não haver câmeras vinculadas a controladora "
                            + controladora.Modelo + ", sendo assim seu veículo não foi identificado");

                        var observacaoLiberadoPapelVeiculoBloqueado = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoLiberadoPapelVeiculoBloqueado;

                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        break;

                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_ACC_INATIVA_NEGADO:
                        acessoVeiculo.StatusAutorizacao = false;
                        sb.Append("negado às " + acessoVeiculo.DataRegistro + " porque o sistema Avigilon Control Center estava offline");
                        var observacaoAccOffline = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoAccOffline;
                        acessoVeiculo.CamerasImagens = null;
                        acessoVeiculo.DataRegistro = DateTime.Now;
                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        //Task.Run(() =>
                        //{

                        //    //UnidadeTrabalho.Commit();
                        //}).ConfigureAwait(false);
                        break;

                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_DATA_ACESSO_TEMPORARIO_EXPIRADO:
                        acessoVeiculo.StatusAutorizacao = false;
                        sb.Append("negado às " + acessoVeiculo.DataRegistro + " porque a o veículo de placa " + placa +
                            " tentou acesso no local da leitora " + acessoVeiculo.Leitora.IDLeitora + " fora do seu período de permissão de acesso temporário, acesso expirado");

                        var observacaoAcessoExpirado = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoAcessoExpirado;

                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        //UnidadeTrabalho.Commit();
                        for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                        {
                            repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                        }

                        break;
                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_PAPEL_NAO_AUTORIZADO_CANCELA:

                        acessoVeiculo.StatusAutorizacao = false;
                        //Verificar se o veículo tem acesso
                        sb.Append("negado às " + acessoVeiculo.DataRegistro + " com o veículo de placa " + acessoVeiculo.VeiculoCredencialVeiculo.Veiculo.Placa +
                            ", o/a condutor(a) tentou acesso no local da leitora " + acessoVeiculo.Leitora.IDLeitora + ", porém não possui acesso ao local");

                        var observacaoAcessoNegadoPapelVeiculo = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoAcessoNegadoPapelVeiculo;

                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        //UnidadeTrabalho.Commit();
                        for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                        {
                            repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                        }


                        break;

                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_PAPEL_AUTORIZADO_VEICULO_NAO_AUTORIZADO:

                        acessoVeiculo.StatusAutorizacao = false;
                        //Verificar se o veículo tem acesso
                        sb.Append("negado às " + acessoVeiculo.DataRegistro + " porque o veículo de placa, " + acessoVeiculo.VeiculoCredencialVeiculo.Veiculo.Placa +
                            " tentou acesso no local da leitora " + acessoVeiculo.Leitora.IDLeitora + ", porém o veículo não possuia permissão de acesso ao local");

                        var observacaoAcessoAutorizadoPapelVeiculoNegado = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoAcessoAutorizadoPapelVeiculoNegado;

                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        //UnidadeTrabalho.Commit();
                        for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                        {
                            repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                        }

                        break;

                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_PAPEL_NAO_AUTORIZADO_VEICULO_NAO_AUTORIZADO:

                        acessoVeiculo.StatusAutorizacao = false;
                        //Verificar se o veículo tem acesso
                        sb.Append("negado às " + acessoVeiculo.DataRegistro + " porque o veículo de placa " + acessoVeiculo.VeiculoCredencialVeiculo.Veiculo.Placa +
                            ", tentou acesso no local da leitora " + acessoVeiculo.Leitora.IDLeitora + ", porém não possui permissão ao local");

                        var observacaoAcessoNegadoPapelAcessoNegadoVeiculo = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoAcessoNegadoPapelAcessoNegadoVeiculo;

                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        //UnidadeTrabalho.Commit();
                        for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                        {
                            repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                        }
                        break;

                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_CREDENCIAL_INEXISTENTE:
                        acessoVeiculo.StatusAutorizacao = false;
                        //Verificar se o veículo tem acesso
                        sb.Append("negado às " + acessoVeiculo.DataRegistro + " porque o veículo de placa " + placa +
                            " tentou acesso no local da leitora " + acessoVeiculo.Leitora.IDLeitora + ", porém o veículo não possuia credencial cadastrada, " +
                            " sendo assim o veículo teve seu acesso não autorizado");

                        var observacaoAcessoNegadoCredencialInexistente = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoAcessoNegadoCredencialInexistente;
                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        //UnidadeTrabalho.Commit();
                        for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                        {
                            repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                        }
                        //repAcessoVeiculo.Salvar(acessoVeiculo);

                        break;
                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_ENCONTRADO_BASE_DADOS:
                        acessoVeiculo.StatusAutorizacao = false;
                        sb.Append("negado às " + acessoVeiculo.DataRegistro + " porque o veículo de placa " + placa +
                            " tentou acesso no local da leitora " + acessoVeiculo.Leitora.IDLeitora + ", porém o veículo não foi identificado na base de dados, " +
                            " sendo assim o veículo teve seu acesso não autorizado");

                        var observacaoAcessoNegadoVeiculoInexistente = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoAcessoNegadoVeiculoInexistente;

                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        //UnidadeTrabalho.Commit();
                        for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                        {
                            if (String.IsNullOrEmpty(acessoVeiculo.CamerasImagens[i].PlacaVeiculo))
                            {
                                acessoVeiculo.CamerasImagens[i].PlacaVeiculo = "-";
                            }
                            repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                        }
                        //repAcessoVeiculo.Salvar(acessoVeiculo);

                        break;

                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_CREDENCIAL_NAO_IMPRESSA:
                        acessoVeiculo.StatusAutorizacao = false;
                        sb.Append("negado às " + acessoVeiculo.DataRegistro + " porque o veículo de placa " + placa +
                            " tentou acesso no local da leitora " + acessoVeiculo.Leitora.IDLeitora + ", porém o estado da credencial do veículo não era \"impressa\"" +
                            ", sendo assim o veículo teve seu acesso não autorizado");

                        var observacaoAcessoNegadoCredencialnaoImpressa = sb.ToString();

                        acessoVeiculo.Observacoes = observacaoAcessoNegadoCredencialnaoImpressa;
                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        //UnidadeTrabalho.Commit();
                        for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                        {
                            repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                        }
                        //repAcessoVeiculo.Salvar(acessoVeiculo);

                        break;

                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_ERRO:
                        acessoVeiculo.StatusAutorizacao = false;
                        sb.Append("negado às " + acessoVeiculo.DataRegistro + " houve erro na aplicação \n Erro: " + erro);

                        var observacaoAcessoErro = sb.ToString();

                        acessoVeiculo.Observacoes = observacaoAcessoErro;
                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        //UnidadeTrabalho.Commit();
                        if (acessoVeiculo.CamerasImagens != null)
                        {
                            for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                            {
                                repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                            }
                        }



                        //repAcessoVeiculo.Salvar(acessoVeiculo);

                        break;


                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_CREDENCIAL_FORA_INTERVALO:
                        acessoVeiculo.StatusAutorizacao = false;
                        sb.Append("negado às " + acessoVeiculo.DataRegistro + ", porque a tentativa de acesso ocorreu fora do período de acesso temporário do veículo");

                        var observacaoAcessoTemporario = sb.ToString();

                        acessoVeiculo.Observacoes = observacaoAcessoTemporario;
                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        //UnidadeTrabalho.Commit();
                        if (acessoVeiculo.CamerasImagens != null)
                        {
                            for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                            {
                                repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                            }
                        }



                        //repAcessoVeiculo.Salvar(acessoVeiculo);

                        break;


                    case StatusAcesso.OCO_UR_CONTROLE_ACESSO_VEICULO_NAO_AUTORIZADO_CAMERA_ATIVA_LPR_INATIVO:
                        acessoVeiculo.StatusAutorizacao = false;

                        sb.Append("não autorizado às " + acessoVeiculo.DataRegistro + " no local da leitora " + acessoVeiculo.Leitora.IDLeitora +
                            ", sendo assim seu veículo não foi identificado, " +
                            "porém imagem foi tirada fotografica do carro no momento da passagem do crachá," +
                            " sistema de detecção de placas desligado");

                        var observacaoCamOnLineOCROffNaoAutorizado = sb.ToString();
                        acessoVeiculo.Observacoes = observacaoCamOnLineOCROffNaoAutorizado;

                        repAcessoVeiculo.Salvar(acessoVeiculo);
                        //UnidadeTrabalho.Commit();
                        if (acessoVeiculo.CamerasImagens != null)
                        {
                            for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
                            {
                                repCameraImagem.Salvar(acessoVeiculo.CamerasImagens[i]);
                            }
                        }

                        break;

                }

            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.Message);
                erro = exc.Message;
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="json"></param>
        private ContentResult Json(string json)
        {
            return new ContentResult
            {
                Content = json,
                ContentType = "application/json"
            };

        }


        private bool UploadFTPSecure()
        {
            return true;

        }

        private void m_sharedAcc_NvrListAdd﻿(string uuid)
        {
            nvr = controlCenter.GetNvr(uuid);
            System.Threading.Thread.Sleep(150);
            //nvr.DeviceListAdd += new AvigilonDotNet.UuidHandler(NVR_DeviceListAdd﻿);

            //String username = "administrator";
            //String password = "123@Mudar";

            //var result = nvr.Login(username, password);
            //if (result != AvigilonDotNet.LoginResult.Successful)
            //{
            //    return;
            //}

        }

        private DateTime LastDeviceAdded = DateTime.MinValue;
        private List<AvigilonDotNet.IEntity> dicTLCEntity = new List<AvigilonDotNet.IEntity>();
        private void NVR_DeviceListAdd(string deviceId)
        {
            //Log(LoggingLevel.INFO, "SDK NVR_DeviceListAdd START (Host {0})", Host);

            //LastDeviceAdded = DateTime.Now;

            AvigilonDotNet.IDevice device = nvr.GetDeviceById(deviceId);
            System.Threading.Thread.Sleep(150);
            nvr.Devices.Add(device);
            
            if (device.DisplayName == nomeDevice)
            {
                for (int i = 0; i < device.Entities.Count; i++)
                {
                    if (device.Entities[i].Type == AvigilonDotNet.EntityType.Camera && device.Entities[i].DisplayName == nomeDevice)
                    {
                        dicTLCEntity.Add(device.Entities[i]);
                    }
                }
            }
        }


        /// <summary>
        /// Valida as placas existentes e compara com a placa do veículo Credenciado
        /// </summary>
        /// <param name="acessoVeiculo"></param>
        private bool ValidaPlaca(AcessoVeiculo acessoVeiculo)
        {
            float precisaoAnterior = 0f;
            float precisaoAtual = 0f;

            for (int i = 0; i < acessoVeiculo.CamerasImagens.Count; i++)
            {
                //Verificar precisão das placas
                if (float.Parse(acessoVeiculo.CamerasImagens[i].Camera.Precisao) >= acessoVeiculo.CamerasImagens[i].PrecisaoImage)
                {
                    //Validação da 
                }
                else
                {

                }

                //Verificar se alguma placa passa do 
            }

            return true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// /// <param name="date"></param>
        public void RegistraLogAvigilon(string message, DateTime date)
        {
            IRepositorioLogAvigilon repLogAvigilon = Fabrica.Instancia.ObterRepositorio<IRepositorioLogAvigilon>(UnidadeTrabalho);
            Log_Avigilon log = new Log_Avigilon();
            log.DataRegistro = date;
            log.Message = message;
            log.Url = "";
            log.UserName = "";
            log.StackTrace = "";
            repLogAvigilon.Salvar(log);

        }

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="acessoVeiculo"></param>
        ///// <param name="placa"></param>
        ///// <returns></returns>
        //private bool ValidaAcessoGrupoLeitora(AcessoVeiculo acessoVeiculo)
        //{

        //}


        //private T serializaObjeto<T>(object obj)
        //{            
        //        return (T) obj;            
        //}

        private string serializaObjeto(object obj)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(obj);
        }


        private delegate string TempoHandler(object sender, ValidaTempoEventArgs args);

        private event TempoHandler tempoEvento;

        private string ValidaTempo()
        {
            return tempoEvento(this, new ValidaTempoEventArgs { Tempo = tempo.ElapsedMilliseconds });
        }

        private class ValidaTempoEventArgs : EventArgs
        {
            public long Tempo { get; set; }
        }

        async Task<string> ParametroTempo()
        {
            while (tempo.ElapsedMilliseconds < 1000)
            {

            }

            return "false";


        }

    }
}