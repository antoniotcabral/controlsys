﻿using Controlsys.Infra;
using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Threading;
using Controlsys.Infra;
using Globalsys;
namespace IntegradorAvigilonCC
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Serviços e configuração da API da Web

            // Rotas da API da Web
            //config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "RotaApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );


        }
    }
}
