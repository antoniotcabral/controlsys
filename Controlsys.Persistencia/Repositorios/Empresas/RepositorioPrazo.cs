﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Empresas;
using Controlsys.Repositorios.Empresas;
using Globalsys;
using Globalsys.Exceptions;

namespace Controlsys.Persistencia.Repositorios.Empresas
{
    /// <summary>
    /// Representa um(a) RepositorioPrazo.
    /// </summary>
    public class RepositorioPrazo : Repositorio<Prazo>, IRepositorioPrazo
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Empresas.RepositorioPrazo.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioPrazo(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {

        }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="prazo">
        /// O(a) prazo.
        /// </param>
        /// <param name="estadoObj">
        /// O(a) estado object.
        /// </param>
        public void Validar(Prazo prazo, Globalsys.Validacao.EstadoObjeto estadoObj)
        {
            switch (estadoObj)
            {
                case Globalsys.Validacao.EstadoObjeto.Novo:
                    PedidoCompra pedido = prazo.PedidoCompra.PedidoCompraPai;
                    Projeto projeto = prazo.PedidoCompra.Projeto;
                    Prazo prazoAnterior = null;
                    DateTime? dataInicio = null;
                    DateTime? dataFim = null;

                    if (pedido != null)
                    {
                        prazoAnterior = pedido.Prazos
                                              .OrderByDescending(p => p.DataRegistro)
                                              .FirstOrDefault();
                    }
                    else
                    {
                        dataInicio = projeto.DataInicio;
                        dataFim = projeto.DataFim;
                    }

                    if (prazo.DataInicio > prazo.DataFim)
                        throw new CoreException("A data início é maior que a data fim.");

                    if ((prazo.DataInicio < dataInicio || prazo.DataInicio > dataFim)
                        && (prazo.DataFim < dataInicio || prazo.DataFim > dataFim))
                        throw new CoreException("O prazo informado está fora do prazo para o Contrato/Projeto.");

                    break;
                case Globalsys.Validacao.EstadoObjeto.Removido:
                    break;
                case Globalsys.Validacao.EstadoObjeto.Ativado:
                    break;
                case Globalsys.Validacao.EstadoObjeto.Inativado:
                    break;
                default:
                    break;
            }
        }
    }
}
