﻿using Controlsys.Dominio.Empresas;
using Controlsys.Repositorios.Empresas;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Repositorios.Empresas
{
    /// <summary>
    /// Representa um(a) RepositorioProjeto.
    /// </summary>
    public class RepositorioProjeto : Repositorio<Projeto>, IRepositorioProjeto
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Empresas.RepositorioProjeto.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioProjeto(IUnidadeTrabalho unidadeTrabalho) : base(unidadeTrabalho) { }

        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="projeto">
        /// O(a) projeto.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool Existe(Projeto projeto)
        {
            return ObterTodos().Any(s => s.Nome.ToUpper() == projeto.Nome.ToUpper() && s.Ativo && s.Codigo != projeto.Codigo);
        }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        public void Validar(Projeto entidade, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    if (entidade.Nome == null || entidade.DataInicio == null )
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe um projeto com o nome informado.");
                    }
                    break;
                case EstadoObjeto.Alterado:
                    if (!entidade.Ativo)
                    {
                        throw new CoreException("O projeto está inativo, não é possível alterar");
                    }
                    if (entidade.Nome == null || entidade.DataInicio == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe um projeto com o nome informado.");
                    }
                    break;
                case EstadoObjeto.Ativado:
                    if (!entidade.Ativo && Existe(entidade))
                    {
                        throw new CoreException("Já existe um projeto com o nome informado.");
                    }
                    break;
                default:
                    break;
            }

        }

        /// <summary>
        /// Salvar area projeto.
        /// </summary>
        ///
        /// <param name="areaProjeto">
        /// O(a) area projeto.
        /// </param>
        public void SalvarAreaProjeto(AreaProjeto areaProjeto)
        {
            Repositorio<AreaProjeto> rep = new Repositorio<AreaProjeto>(base.UnidadeTrabalho);
            rep.Salvar(areaProjeto);
        }

        /// <summary>
        /// Salvar grupo trabalho projeto.
        /// </summary>
        ///
        /// <param name="grupoTrabProjeto">
        /// O(a) grupo trab projeto.
        /// </param>
        public void SalvarGrupoTrabalhoProjeto(GrupoTrabalhoProjeto grupoTrabProjeto)
        {
            Repositorio<GrupoTrabalhoProjeto> rep = new Repositorio<GrupoTrabalhoProjeto>(base.UnidadeTrabalho);
            rep.Salvar(grupoTrabProjeto);
        }

        /// <summary>
        /// Salvar treinamento projeto.
        /// </summary>
        ///
        /// <param name="treinamentoProjeto">
        /// O(a) treinamento projeto.
        /// </param>
        public void SalvarTreinamentoProjeto(TreinamentoProjeto treinamentoProjeto)
        {
            Repositorio<TreinamentoProjeto> rep = new Repositorio<TreinamentoProjeto>(base.UnidadeTrabalho);
            rep.Salvar(treinamentoProjeto);
        }
    }
}
