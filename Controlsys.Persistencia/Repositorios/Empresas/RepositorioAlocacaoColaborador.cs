﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Empresas;
using Controlsys.Repositorios.Empresas;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;

namespace Controlsys.Persistencia.Repositorios.Empresas
{
    /// <summary>
    /// Representa um(a) RepositorioAlocacaoColaborador.
    /// </summary>
    public class RepositorioAlocacaoColaborador : Repositorio<AlocacaoColaborador>, IRepositorioAlocacaoColaborador
    {
        /// <summary>
        /// Construtor para
        /// Controlsys.Persistencia.Repositorios.Empresas.RepositorioAlocacaoColaborador.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioAlocacaoColaborador(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho) {}

        /// <summary>
        /// Obter alocacao.
        /// </summary>
        ///
        /// <param name="cod_colaborador">
        /// O(a) cod colaborador.
        /// </param>
        /// <param name="todosStatus">
        /// true to todos status.
        /// </param>
        ///
        /// <returns>
        /// Um(a) AlocacaoColaborador.
        /// </returns>
        public AlocacaoColaborador ObterAlocacao(int cod_colaborador, bool todosStatus = false)
        {
            IQueryable<AlocacaoColaborador> alocacao =  ObterTodos().Where(t => t.Papel.Codigo == cod_colaborador).OrderByDescending(t => t.DataRegistro);

            if (todosStatus == false)
            {
                alocacao = alocacao.Where(a => a.Ativo);
            }

            return alocacao.FirstOrDefault();
        }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        public void Validar(AlocacaoColaborador entidade, EstadoObjeto estadoObjeto)
        {            
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    if(entidade.PedidoCompra.Status != StatusPedido.Ativo)
                        throw new CoreException(string.Format("Não é possível salvar. Pedido de compra inativo."));
                   
                        if (ObterTodos().Any(a => a.Codigo != entidade.Codigo
                                           && a.Papel.Codigo == entidade.Papel.Codigo
                                           && a.Ativo))
                        throw new CoreException(string.Format("Não é possível salvar. O colaborador {0} já está alocado em outra PO.", entidade.Papel.PessoaFisica.Nome));

                    if (entidade
                        .PedidoCompra
                        .Projeto                                    
                        .Treinamentos
                        .Any(tproj => tproj.Ativo && 
                                     !entidade
                                      .Papel                                      
                                      .Treinamentos
                                      .Any(tp => tproj.Treinamento.Codigo == tp.Treinamento.Codigo && tp.DataValidade < DateTime.Today)))
                        throw new CoreException(string.Format("Não é possível salvar. O colaborador {0} não possui algum dos treinamentos necessários ao projeto.", entidade.Papel.PessoaFisica.Nome));

                    break;
            }
        }
    }
}
