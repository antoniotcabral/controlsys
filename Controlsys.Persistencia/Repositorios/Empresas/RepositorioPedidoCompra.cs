﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Empresas;
using Controlsys.Repositorios.Empresas;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;

namespace Controlsys.Persistencia.Repositorios.Empresas
{
    /// <summary>
    /// Representa um(a) RepositorioPedidoCompra.
    /// </summary>
    public class RepositorioPedidoCompra : Repositorio<PedidoCompra>, IRepositorioPedidoCompra
    {
        private readonly Acesso.RepositorioSincronizaPessoaAcesso repSincronizaPessoaAcesso;
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Empresas.RepositorioPedidoCompra.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioPedidoCompra(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
            repSincronizaPessoaAcesso = new Acesso.RepositorioSincronizaPessoaAcesso(UnidadeTrabalho);
        }

        /// <summary>
        /// Obter por projeto.
        /// </summary>
        ///
        /// <param name="codigoProjeto">
        /// O(a) codigo projeto.
        /// </param>
        ///
        /// <returns>
        /// Um(a) PedidoCompra.
        /// </returns>
        public PedidoCompra ObterPorProjeto(int codigoProjeto)
        {
            return ObterTodos().Where(p => p.PedidoCompraPai == null && p.Projeto.Codigo == codigoProjeto && p.Status != StatusPedido.Removido)
                                                    .FirstOrDefault();
        }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="pedidoCompra">
        /// O(a) pedido compra.
        /// </param>
        /// <param name="estadoObj">
        /// O(a) estado object.
        /// </param>
        public void Validar(PedidoCompra pedidoCompra, EstadoObjeto estadoObj)
        {
            switch (estadoObj)
            {
                case EstadoObjeto.Novo:
                case EstadoObjeto.Alterado:
                    if (pedidoCompra.Status == StatusPedido.Inativo)
                        throw new CoreException("Não é possível salvar o pedido. O pedido está inativo.");

                    if (string.IsNullOrEmpty(pedidoCompra.Numero))
                        if (pedidoCompra.PedidoCompraPai != null)
                            if (pedidoCompra.PedidoCompraPai.PedidoCompraPai == null)
                                throw new CoreException("Informe o número da PO.");

                    if (string.IsNullOrEmpty(pedidoCompra.Nome))
                        throw new CoreException("Informe o nome.");

                    if (pedidoCompra.Empresa == null)
                        throw new CoreException("Informe a empresa.");

                    if (!pedidoCompra.Prazos.Where(p => p.Ativo).Any())
                        throw new CoreException("Informe ao menos um prazo.");

                    //if (!pedidoCompra.Setores.Any())
                    //    throw new CoreException("Informe ao menos um setor de custo.");

                    if (!pedidoCompra.GestoresResponsaveis.Any())
                        throw new CoreException("Informe ao menos um responsável pela PO.");

                    if (pedidoCompra.PedidoCompraPai != null &&
                            pedidoCompra.PedidoCompraPai.Status == StatusPedido.Inativo)
                        throw new CoreException("Não é possível salvar. O subpedido pois o pedido de compra pai está inativo.");

                    if (Existe(pedidoCompra))
                        throw new CoreException("Já existe um pedido de compra cadastrado com o número informado.");
                    break;
                case EstadoObjeto.Removido:
                    break;
                case EstadoObjeto.Ativado:
                    if (pedidoCompra.PedidoCompraPai != null &&
                            pedidoCompra.PedidoCompraPai.Status == StatusPedido.Inativo)
                        throw new CoreException("Não é possível ativar o subpedido pois o pedido de compra pai está inativo.");
                    break;
                case EstadoObjeto.Inativado:
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="pedido">
        /// O(a) pedido.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool Existe(PedidoCompra pedido)
        {
            return ObterTodos().Any(pc => 
                    pc.Numero == pedido.Numero && 
                    pedido.Numero != null && 
                    pc.Codigo != pedido.Codigo &&
                    pc.Empresa.Codigo == pedido.Empresa.Codigo && // Verifica se já existe o mesmo pedido de compra para a empresa 
                    pc.Status == StatusPedido.Ativo);
        }

        /// <summary>
        /// Inativa o objeto.
        /// </summary>
        ///
        /// <param name="pedidoCompra">
        /// O(a) pedido compra.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;int&gt;
        /// </returns>
        public List<int> Inativar(PedidoCompra pedidoCompra)
        {
            List<int> ids = new List<int>();

           PedidoCompra pedido = ObterPorId(pedidoCompra.Codigo);

           if (pedido.Status.Equals(StatusPedido.Ativo))
           {                             
                ids.Add(pedidoCompra.Codigo);

                pedidoCompra.Status = StatusPedido.Inativo;
                pedidoCompra.DataStatus = DateTime.Now;                
                Atualizar(pedidoCompra);

                // Supender acesso dos colaboradores alocados
                foreach (var colaborador in pedido.ColaboradoresAlocados.Where(p => p.Ativo))
                {
                    repSincronizaPessoaAcesso.Sincronizar(colaborador.Papel, Dominio.Acesso.TipoSincronizacaoPessoa.Suspender, null);
                }                                    

                foreach (PedidoCompra subPedido in pedidoCompra.SubPedidos)
                    ids.AddRange(Inativar(subPedido));

           }

            return ids;
        }

        /// <summary>
        /// Ativa o objeto.
        /// </summary>
        ///
        /// <param name="pedidoCompra">
        /// O(a) pedido compra.
        /// </param>
        public void Ativar(PedidoCompra pedidoCompra)
        {
            pedidoCompra.Status = StatusPedido.Ativo;
            pedidoCompra.DataStatus = DateTime.Now;

            Validar(pedidoCompra, EstadoObjeto.Ativado);
            Atualizar(pedidoCompra);

            // Conceder acesso dos colaboradores alocados
            foreach (var colaborador in pedidoCompra.ColaboradoresAlocados.Where(p => p.Ativo))
                repSincronizaPessoaAcesso.Sincronizar(colaborador.Papel, Dominio.Acesso.TipoSincronizacaoPessoa.Conceder, null);
        }

        /// <summary>
        /// Remover.
        /// </summary>
        ///
        /// <param name="pedidoCompra">
        /// O(a) pedido compra.
        /// </param>
        /// <param name="status">
        /// O(a) status.
        /// </param>
        public void Remover(PedidoCompra pedidoCompra, StatusPedido status)
        {
            pedidoCompra.Status = StatusPedido.Removido;
            pedidoCompra.DataStatus = DateTime.Now;

            foreach (PedidoCompra subPedido in pedidoCompra.SubPedidos)
            {
                Remover(subPedido, StatusPedido.Removido);
            }
        }

        /// <summary>
        /// Obter contratada.
        /// </summary>
        ///
        /// <param name="codigoPedido">
        /// O(a) codigo pedido.
        /// </param>
        ///
        /// <returns>
        /// Um(a) PedidoCompra.
        /// </returns>
        public PedidoCompra ObterContratada(int codigoPedido)
        {
            PedidoCompra pedido = ObterPorId(codigoPedido);

            return pedido.ObterContratada();
        }

        public List<Empresa> ObterEmpresasRelacionadasPO(int codigoPedido = 0)
        {
            if (codigoPedido == 0)
                return new List<Empresa>();
            
            Dictionary<string, object> dictionary = new Dictionary<string, object>();

            List<Empresa> empresas = UnidadeTrabalho.ExecuteSql<Empresa>("EXEC SP_OBTEM_EMPRESAS_RELACIONADAS_PO " + codigoPedido, dictionary).ToList();

            return empresas;
        }
    }
}
