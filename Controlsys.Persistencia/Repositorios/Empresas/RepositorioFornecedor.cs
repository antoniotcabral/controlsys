﻿using Controlsys.Dominio.Empresas;
using Controlsys.Repositorios.Empresas;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Repositorios.Empresas
{
    /// <summary>
    /// Representa um(a) RepositorioFornecedor.
    /// </summary>
    public class RepositorioFornecedor : Repositorio<Fornecedor>, IRepositorioFornecedor
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Empresas.RepositorioFornecedor.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioFornecedor(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
        }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        public void Validar(Fornecedor entidade, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    if (entidade.CNPJ == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe um fornecedor com o CNPJ informado.");
                    }
                    break;
                case EstadoObjeto.Alterado:
                    if (!entidade.Ativo)
                    {
                        throw new CoreException("O fornecedor está inativo, não é possível alterar.");
                    }
                    if (entidade.CNPJ == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe um fornecedor com o CNPJ informado.");
                    }
                    break;
                case EstadoObjeto.Ativado:
                    if (!entidade.Ativo && Existe(entidade))
                    {
                        throw new CoreException("Já existe um fornecedor com o CNPJ informado.");
                    }
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool Existe(Fornecedor entidade)
        {
            return ObterTodos().Any(a => a.CNPJ == entidade.CNPJ && a.Codigo != entidade.Codigo);
        }

        /// <summary>
        /// Salvar fornecedor empresa.
        /// </summary>
        ///
        /// <param name="fe">
        /// O(a) fe.
        /// </param>
        public void SalvarFornecedorEmpresa(FornecedorEmpresa fe)
        {
            Repositorio<FornecedorEmpresa> rep = new Repositorio<FornecedorEmpresa>(base.UnidadeTrabalho);
            rep.Salvar(fe);
        }

        /// <summary>
        /// Obter por cnpj.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="cnpj">
        /// O(a) cnpj.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Fornecedor.
        /// </returns>
        public Fornecedor ObterPorCnpj(string cnpj = null)
        {
            if (string.IsNullOrEmpty(cnpj))
                throw new CoreException("Favor informar cnpj!");

            IQueryable<Fornecedor> fornecedores = ObterTodos();

            if (!string.IsNullOrEmpty(cnpj))
                fornecedores = fornecedores.Where(pf => pf.CNPJ == cnpj);

            return fornecedores.FirstOrDefault();
        }

    }
}
