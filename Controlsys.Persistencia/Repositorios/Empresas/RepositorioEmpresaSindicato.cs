﻿using Controlsys.Dominio.Empresas;
using Controlsys.Repositorio.Empresas;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Controlsys.Persistencia.Repositorios.Empresas
{
    public class RepositorioEmpresaSindicato : Repositorio<EmpresaSindicato>, IRepositorioEmpresaSindicato
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Pessoas.RepositorioEmpresaSindicato.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioEmpresaSindicato(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho) { }

        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="empresaSindicato">
        /// O(a) EmpresaSindicato.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool Existe(EmpresaSindicato empresaSindicato)
        {
            return ObterTodos().Any(s => s.Empresa.Codigo == empresaSindicato.Empresa.Codigo && s.Sindicato.Codigo == empresaSindicato.Sindicato.Codigo && s.Ativo);

        }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        public void Validar(EmpresaSindicato entidade, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    if (entidade.Empresa == null || entidade.Sindicato == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }

                    break;
                case EstadoObjeto.Alterado:
                    if (entidade.Empresa == null || entidade.Sindicato == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
