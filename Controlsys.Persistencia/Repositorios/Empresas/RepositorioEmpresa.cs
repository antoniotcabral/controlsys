﻿using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Enderecos;
using Controlsys.Dominio.Parametros;
using Controlsys.Repositorios.Empresas;
using Controlsys.Repositorios.Parametros;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Repositorios.Empresas
{
    /// <summary>
    /// Representa um(a) RepositorioEmpresa.
    /// </summary>
    public class RepositorioEmpresa : Repositorio<Empresa>, IRepositorioEmpresa
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Empresas.RepositorioEmpresa.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioEmpresa(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
        }

        /// <summary>
        /// Obter todos cnae.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) List&lt;CNAE&gt;
        /// </returns>
        public List<CNAE> ObterTodosCNAE()
        {
            Repositorio<CNAE> cnaes = new Repositorio<CNAE>(base.UnidadeTrabalho);
            return cnaes.ObterTodos().Where(a => a.Ativo == true).ToList();
        }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        public void Validar(Empresa entidade, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    if (entidade.Nome == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe empresa ativa para o CNPJ informado.");
                    }
                    break;
                case EstadoObjeto.Alterado:
                    if (entidade.Nome == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    
                    break;
                case EstadoObjeto.Ativado:
                    if (!entidade.Documentos.Any() || entidade.Documentos.Any(t => t.Documento.Status == StatusDocumento.NaoEntregue ||t.Documento.Status ==  StatusDocumento.Incompleto))
                    {
                        throw new CoreException("Não foi possível ativar. A documentação da empresa está incompleta/não entregue.");
                    }
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe empresa ativa para o CNPJ informado.");
                    }
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        private bool Existe(Empresa entidade)
        {
            return ObterTodos().Any(s => s.CNPJ == entidade.CNPJ && s.Codigo != entidade.Codigo);
        }

        /// <summary>
        /// Salvar empresa cnae.
        /// </summary>
        ///
        /// <param name="empresaCnae">
        /// O(a) empresa cnae.
        /// </param>
        public void SalvarEmpresaCnae(EmpresaCNAE empresaCnae)
        {
            Repositorio<EmpresaCNAE> rep = new Repositorio<EmpresaCNAE>(base.UnidadeTrabalho);
            rep.Salvar(empresaCnae);
        }

        /// <summary>
        /// Salvar endereco.
        /// </summary>
        ///
        /// <param name="end">
        /// O(a) end.
        /// </param>
        public void SalvarEndereco(Endereco end)
        {
            Repositorio<Endereco> rep = new Repositorio<Endereco>(base.UnidadeTrabalho);
            rep.Salvar(end);
        }

        /// <summary>
        /// Obter por cnpj.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="cnpj">
        /// O(a) cnpj.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Empresa.
        /// </returns>
        public Empresa ObterPorCnpj(string cnpj = null)
        {
            if (string.IsNullOrEmpty(cnpj))
                throw new CoreException("Favor informar cnpj!");

            IQueryable<Empresa> empresas = ObterTodos();

            if (!string.IsNullOrEmpty(cnpj))
                empresas = empresas.Where(pf => pf.CNPJ == cnpj);

            return empresas.FirstOrDefault();
        }
    }
}
