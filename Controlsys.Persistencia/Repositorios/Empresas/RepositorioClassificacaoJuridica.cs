﻿using Controlsys.Dominio.Parametros;
using Controlsys.Repositorio.Empresas;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Repositorios.Empresas
{
    public class RepositorioClassificacaoJuridica : Repositorio<ClassificacaoJuridica>, IRepositorioClassificacaoJuridica
    {
        public RepositorioClassificacaoJuridica(IUnidadeTrabalho unidadeTrabalho) : base(unidadeTrabalho) { }

        public bool Existe(ClassificacaoJuridica entidade)
        {
            return ObterTodos().Any(s =>
                (s.Codigo != entidade.Codigo) &&
                (s.Letra.ToUpper() == entidade.Letra.ToUpper()) &&
                (s.Ativo == true)
            );
        }

        public void Validar(ClassificacaoJuridica entidade, string[] letrasFormula)
        {
            if (Existe(entidade))
                throw new CoreException("Não é possível salvar. Já existe um registro ativo com a letra informada.");

            if (string.IsNullOrEmpty(entidade.Letra))
                throw new CoreException("Não é possível salvar registro sem informar letra correspondente.");

            if (letrasFormula.Any())
            {
                foreach (var reg in letrasFormula)
                {
                    if (entidade.Ativo)
                    {
                        if (reg == entidade.Letra) continue;

                        var existe = ObterTodos().Any(c => (c.Ativo) && (c.Letra == reg) && (c.Letra != entidade.Letra));
                        if (!existe)
                            throw new CoreException(
                                string.Format("Não é possível salvar. Não foi encontrado nenhum registro ativo com a letra '{0}', da fórmula '{1}'.", reg, entidade.Formula));
                    }
                    //else
                    //{
                    //    if (reg != entidade.Letra) continue;
                    //
                    //    var item = ObterTodos().FirstOrDefault(c => (c.Ativo) && (c.Formula.Contains(reg)) && (c.Letra != entidade.Letra));
                    //    if (item != null)
                    //        throw new CoreException(
                    //            string.Format("Não é possível salvar. O registro do item '{0}' está ativo e utilizando a letra '{1}'.", 
                    //            item.ItemClassificacao.ObterDescricaoEnum(), reg));
                    //}
                }
            }

            if (!entidade.Ativo) {
                var item = ObterTodos().FirstOrDefault(c => (c.Ativo) && (c.Formula.ToLower().Replace("if","").Contains(entidade.Letra)) && (c.Letra != entidade.Letra));
                if (item != null)
                    throw new CoreException(
                        string.Format("Não é possível salvar. O registro do item '{0}' está ativo e utilizando a letra '{1}'.",
                        item.ItemClassificacao.ObterDescricaoEnum(), entidade.Letra));
            }

            if (entidade.Ativo && entidade.Pontua && string.IsNullOrEmpty(entidade.Formula))
                throw new CoreException("Não é possível ativar um registro que pontue sem informar antes a fórmula.");
        }
    }
}
