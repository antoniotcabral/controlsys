﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Empresas;
using Controlsys.Repositorios.Empresas;
using Globalsys;

namespace Controlsys.Persistencia.Repositorios.Empresas
{
    /// <summary>
    /// Representa um(a) RepositorioSetorCustoPedido.
    /// </summary>
    public class RepositorioSetorCustoPedido : Repositorio<SetorCustoPedido>, IRepositorioSetorCustoPedido
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Empresas.RepositorioSetorCustoPedido.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioSetorCustoPedido(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
            
        }
    }
}
