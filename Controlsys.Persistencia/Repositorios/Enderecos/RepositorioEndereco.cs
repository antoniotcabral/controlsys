﻿using Controlsys.Dominio.Enderecos;
using Controlsys.Repositorios.Parametros;
using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Repositorios.Enderecos;

namespace Controlsys.Persistencia.Repositorios.Enderecos
{
    /// <summary>
    /// Representa um(a) RepositorioEndereco.
    /// </summary>
    public class RepositorioEndereco : Repositorio<Endereco>, IRepositorioEndereco
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Enderecos.RepositorioEndereco.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioEndereco(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        { }

        /// <summary>
        /// Pesquisar.
        /// </summary>
        ///
        /// <param name="cep">
        /// O(a) cep.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Logradouro.
        /// </returns>
        public Logradouro Pesquisar(string cep)
        {
            Repositorio<Logradouro> rep = new Repositorio<Logradouro>(UnidadeTrabalho);
            Logradouro l = rep.ObterTodos().Where(c => c.CEP == cep).FirstOrDefault();
            return l;
        }

        /// <summary>
        /// Obter por identifier.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Cidade.
        /// </returns>
        public Cidade ObterPorId(int codigo)
        {
            Repositorio<Cidade> rep = new Repositorio<Cidade>(UnidadeTrabalho);
            return rep.ObterPorId(codigo);
        }

        /// <summary>
        /// Obter paises.
        /// </summary>
        ///
        /// <param name="descricao">
        /// O(a) descricao.
        /// </param>
        ///
        /// <returns>
        /// Um(a) IQueryable&lt;Pais&gt;
        /// </returns>
        public IQueryable<Pais> ObterPaises(string descricao = null)
        {
            Repositorio<Pais> rep = new Repositorio<Pais>(UnidadeTrabalho);
            return rep.ObterTodos();
        }

        /// <summary>
        /// Obter paise por nome.
        /// </summary>
        ///
        /// <param name="descricao">
        /// O(a) descricao.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Pais.
        /// </returns>
        public Pais ObterPaisePorNome(string descricao)
        {
            Repositorio<Pais> rep = new Repositorio<Pais>(UnidadeTrabalho);
            return rep.ObterTodos().Where(p => p.Nome.ToUpper() == descricao.ToUpper()).First();
        }
        /// <summary>
        /// Obter estado por sigla.
        /// </summary>
        /// <param name="sigla"></param>
        /// <returns></returns>
        public Estado ObterEstadosSigla(string sigla)
        {
            Repositorio<Estado> rep = new Repositorio<Estado>(UnidadeTrabalho);

            IQueryable<Estado> estados = rep.ObterTodos();

            if (!string.IsNullOrEmpty(sigla))
            {
                sigla = sigla.ToUpper();
                estados = estados.Where(e => e.Sigla.ToUpper().Contains(sigla));
            }

            return estados.FirstOrDefault();
        }
        /// <summary>
        /// Obter estados.
        /// </summary>
        ///
        /// <param name="descricao">
        /// O(a) descricao.
        /// </param>
        /// <param name="codigoPais">
        /// O(a) codigo pais.
        /// </param>
        ///
        /// <returns>
        /// Um(a) IQueryable&lt;Estado&gt;
        /// </returns>
        public IQueryable<Estado> ObterEstados(string descricao, int? codigoPais)
        {
            Repositorio<Estado> rep = new Repositorio<Estado>(UnidadeTrabalho);

            IQueryable<Estado> estados = rep.ObterTodos();
            
            if (codigoPais.HasValue)
                estados = estados.Where(e => e.Pais.Codigo == codigoPais);

            if (!string.IsNullOrEmpty(descricao))
            {
                descricao = descricao.ToUpper();
                estados = estados.Where(e => e.Nome.ToUpper().Contains(descricao));
            }

            return estados;
        }

        /// <summary>
        /// Obter estado por nome.
        /// </summary>
        ///
        /// <param name="descricao">
        /// O(a) descricao.
        /// </param>
        /// <param name="pais">
        /// O(a) pais.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Estado.
        /// </returns>
        public Estado ObterEstadoPorNome(string descricao, string pais)
        {
            Repositorio<Estado> rep = new Repositorio<Estado>(UnidadeTrabalho);
            return rep.ObterTodos().Where(p => p.Nome.ToUpper() == descricao.ToUpper() && p.Pais.Nome.ToUpper() == pais.ToUpper()).First();
        }

        /// <summary>
        /// Obter cidades.
        /// </summary>
        ///
        /// <param name="descricao">
        /// O(a) descricao.
        /// </param>
        /// <param name="codigoEstado">
        /// O(a) codigo estado.
        /// </param>
        ///  <param name="apenasAtivos">
        /// O(a) status ativo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) IQueryable&lt;Cidade&gt;
        /// </returns>
        public IQueryable<Cidade> ObterCidades(string descricao, int? codigoEstado, bool apenasAtivos = false)
        {
            Repositorio<Cidade> rep = new Repositorio<Cidade>(UnidadeTrabalho);

            IQueryable<Cidade> cidades = rep.ObterTodos();

            if (codigoEstado.HasValue)
                cidades = cidades.Where(c => c.Estado.Codigo == codigoEstado);

            if (!string.IsNullOrEmpty(descricao))
            {
                descricao = descricao.ToUpper();
                cidades = cidades.Where(c => c.Nome.ToUpper().Contains(descricao));
            }

            if (apenasAtivos)
            {                
                cidades = cidades.Where(c => c.Ativo == apenasAtivos);
            }

            return cidades;
        }

        /// <summary>
        /// Obter cidade por nome.
        /// </summary>
        ///
        /// <param name="descricao">
        /// O(a) descricao.
        /// </param>
        /// <param name="estado">
        /// O(a) estado.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Cidade.
        /// </returns>
        public Cidade ObterCidadePorNome(string descricao, string estado)
        {
            Repositorio<Cidade> rep = new Repositorio<Cidade>(UnidadeTrabalho);
            return rep.ObterTodos().Where(p => p.Nome.ToUpper() == descricao.ToUpper() && p.Estado.Nome.ToUpper() == estado.ToUpper()).First();
        }

        /// <summary>
        /// Obter bairros.
        /// </summary>
        ///
        /// <param name="descricao">
        /// O(a) descricao.
        /// </param>
        /// <param name="codigoCidade">
        /// O(a) codigo cidade.
        /// </param>
        ///
        /// <returns>
        /// Um(a) IQueryable&lt;Bairro&gt;
        /// </returns>
        public IQueryable<Bairro> ObterBairros(string descricao, int? codigoCidade)
        {
            Repositorio<Bairro> rep = new Repositorio<Bairro>(UnidadeTrabalho);

            IQueryable<Bairro> bairros = rep.ObterTodos();

            if (codigoCidade.HasValue)
                bairros = bairros.Where(c => c.Cidade.Codigo == codigoCidade);

            return bairros;
        }

        /// <summary>
        /// Obter bairro por nome.
        /// </summary>
        ///
        /// <param name="descricao">
        /// O(a) descricao.
        /// </param>
        /// <param name="cidade">
        /// O(a) cidade.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Bairro.
        /// </returns>
        public Bairro ObterBairroPorNome(string descricao, string cidade)
        {
            Repositorio<Bairro> rep = new Repositorio<Bairro>(UnidadeTrabalho);
            return rep.ObterTodos().Where(p => p.Nome.ToUpper() == descricao.ToUpper() && p.Cidade.Nome.ToUpper() == cidade.ToUpper()).First();
        }

        /// <summary>
        /// Obter logradouros.
        /// </summary>
        ///
        /// <param name="descricao">
        /// O(a) descricao.
        /// </param>
        /// <param name="codigoBairro">
        /// O(a) codigo bairro.
        /// </param>
        ///
        /// <returns>
        /// Um(a) IQueryable&lt;Logradouro&gt;
        /// </returns>
        public IQueryable<Logradouro> ObterLogradouros(string descricao, int? codigoBairro)
        {
            Repositorio<Logradouro> rep = new Repositorio<Logradouro>(UnidadeTrabalho);

            IQueryable<Logradouro> logradouros = rep.ObterTodos();

            if (codigoBairro.HasValue)
                logradouros = logradouros.Where(c => c.Bairro.Codigo == codigoBairro);

            return logradouros;
        }

        /// <summary>
        /// Obter logradouro por nome.
        /// </summary>
        ///
        /// <param name="descricao">
        /// O(a) descricao.
        /// </param>
        /// <param name="bairro">
        /// O(a) bairro.
        /// </param>
        /// <param name="cidade">
        /// O(a) cidade.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Logradouro.
        /// </returns>
        public Logradouro ObterLogradouroPorNome(string descricao, string bairro, String cidade)
        {
            Repositorio<Logradouro> rep = new Repositorio<Logradouro>(UnidadeTrabalho);
            return rep.ObterTodos().First(p => 
                (p.Nome.ToUpper() == descricao.ToUpper()) && 
                (p.Bairro.Nome.ToUpper() == bairro.ToUpper()) && 
                (p.Bairro.Cidade.Nome.ToUpper() == cidade.ToUpper())
            );
        }

        //public IQueryable<TipoLogradouro> ObterTiposLogradouro(string descricao)
        //{
        //    Repositorio<TipoLogradouro> repTiposLogradouro = new Repositorio<TipoLogradouro>(UnidadeTrabalho);

        //    return repTiposLogradouro.ObterTodos().Where(p => p.Nome == descricao);
        //}

        //public TipoLogradouro ObterTipoLogradouroPorNome(string descricao)
        //{
        //    Repositorio<TipoLogradouro> rep = new Repositorio<TipoLogradouro>(UnidadeTrabalho);
        //    return rep.ObterTodos().Where(p => p.Nome.ToUpper() == descricao.ToUpper()).First();
        //}

        /// <summary>
        /// Existe logradouro.
        /// </summary>
        ///
        /// <param name="logradouro">
        /// O(a) logradouro.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool ExisteLogradouro(Logradouro logradouro)
        {
            return ObterLogradouros("", null).Any(s => s.Nome == logradouro.Nome && s.Bairro.Nome == logradouro.Bairro.Nome && s.CEP == logradouro.CEP);
        }

        /// <summary>
        /// Existe bairro.
        /// </summary>
        ///
        /// <param name="bairro">
        /// O(a) bairro.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool ExisteBairro(Bairro bairro)
        {
            return ObterBairros("", null).Any(s => s.Nome == bairro.Nome && s.Cidade.Nome == bairro.Cidade.Nome);
        }

        /// <summary>
        /// Existe cidade.
        /// </summary>
        ///
        /// <param name="cidade">
        /// O(a) cidade.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool ExisteCidade(Cidade cidade)
        {
            return ObterCidades("", null).Any(s => s.Nome == cidade.Nome && s.Estado.Nome == cidade.Estado.Nome);
        }

        /// <summary>
        /// Existe estado.
        /// </summary>
        ///
        /// <param name="estado">
        /// O(a) estado.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool ExisteEstado(Estado estado)
        {
            return ObterEstados("", null).Any(s => s.Nome == estado.Nome && s.Pais.Nome == estado.Pais.Nome);
        }

        /// <summary>
        /// Existe pais.
        /// </summary>
        ///
        /// <param name="pais">
        /// O(a) pais.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool ExistePais(Pais pais)
        {

            string nome = pais.Nome;
            return ObterPaises("").Any(s => s.Nome == nome);
        }

        /// <summary>
        /// Verificar endereco.
        /// </summary>
        ///
        /// <param name="end">
        /// O(a) end.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Endereco.
        /// </returns>
        public Endereco VerificarEndereco(Endereco end)
        {
            Pais pais = end.Logradouro.Bairro.Cidade.Estado.Pais;
            Estado estado = end.Logradouro.Bairro.Cidade.Estado;
            Cidade cidade = end.Logradouro.Bairro.Cidade;
            Bairro bairro = end.Logradouro.Bairro;
            Logradouro logradouro = end.Logradouro;

            logradouro.CEP = logradouro.CEP.Replace("-", "");

            //TipoLogradouro tipo = end.Logradouro.TipoLogradouro;

            // verifica se país existe e insere caso não exista
            if (!ExistePais(pais))
            {
                Repositorio<Pais> rep = new Repositorio<Pais>(UnidadeTrabalho);
                pais.DataRegistro = DateTime.Now;
                pais.Ativar();
                rep.Salvar(pais);
                end.Logradouro.Bairro.Cidade.Estado.Pais = pais;
            }
            // verifica se estado existe e insere caso não exista
            if (!ExisteEstado(estado))
            {
                Repositorio<Estado> rep = new Repositorio<Estado>(UnidadeTrabalho);
                estado.Pais = ObterPaisePorNome(pais.Nome);
                estado.Ativar();
                estado.DataRegistro = DateTime.Now;
                estado.Ativar();
                rep.Salvar(estado);
                end.Logradouro.Bairro.Cidade.Estado = estado;
            }
            // verifica se cidade existe e insere caso não exista
            if (!ExisteCidade(cidade))
            {
                Repositorio<Cidade> rep = new Repositorio<Cidade>(UnidadeTrabalho);
                cidade.Estado = ObterEstadoPorNome(estado.Nome, pais.Nome);
                cidade.Ativar();
                cidade.DataRegistro = DateTime.Now;
                cidade.Ativar();
                rep.Salvar(cidade);
                end.Logradouro.Bairro.Cidade = cidade;
            }
            // verifica se bairro existe e insere caso não exista
            if (!ExisteBairro(bairro))
            {
                Repositorio<Bairro> rep = new Repositorio<Bairro>(UnidadeTrabalho);
                bairro.Cidade = ObterCidadePorNome(cidade.Nome, estado.Nome);
                bairro.Ativar();
                bairro.DataRegistro = DateTime.Now;
                bairro.Ativar();
                rep.Salvar(bairro);
                end.Logradouro.Bairro = bairro;
            }
            // verifica se logradoura existe e insere caso não exista
            if (!ExisteLogradouro(logradouro))
            {
                Repositorio<Logradouro> rep = new Repositorio<Logradouro>(UnidadeTrabalho);
                logradouro.Bairro = ObterBairroPorNome(bairro.Nome, cidade.Nome);
                logradouro.Ativar();
                logradouro.DataRegistro = DateTime.Now;
                rep.Salvar(logradouro);
                end.Logradouro = logradouro;
            }
            else
            {
                end.Logradouro = ObterLogradouroPorNome(logradouro.Nome, bairro.Nome, cidade.Nome);
            }

            return end;
        }

    }
}
