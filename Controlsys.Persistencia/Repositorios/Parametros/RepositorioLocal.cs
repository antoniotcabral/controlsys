﻿using Controlsys.Dominio.Parametros;
using Controlsys.Persistencia.Repositorios.Ocorrencias;
using Controlsys.Repositorio.Parametros;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Repositorios.Parametros
{
    /// <summary>
    /// 
    /// </summary>
    public class RepositorioLocal : Repositorio<Local>, IRepositorioLocal
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Parametros.RepositorioLocal.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioLocal(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
        }

        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="area">
        /// O(a) area.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool Existe(Local local)
        {
            return ObterTodos().Any(l => l.Nome.ToUpper() == local.Nome.ToUpper() && l.Ativo && l.Codigo != local.Codigo);
        }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        public void Validar(Local entidade, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    if (entidade.Nome == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe um local com o Nome informado.");
                    }
                    break;
                case EstadoObjeto.Alterado:
                    if (!entidade.Ativo)
                    {
                        throw new CoreException("O local está inativo, não é possível alterar");
                    }
                    if (entidade.Nome == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe um local com o Nome informado.");
                    }
                    break;
                case EstadoObjeto.Ativado:
                    if (!entidade.Ativo && Existe(entidade))
                    {
                        throw new CoreException("Já existe um local com o Nome informado.");
                    }
                    break;
                case EstadoObjeto.Inativado:
                case EstadoObjeto.Removido:
                    if (new RepositorioOcorrenciaSeguranca(UnidadeTrabalho).ObterTodos().Where(os => os.Ativo && os.Local.Codigo.Equals(entidade.Codigo)).Any())
                    {
                        throw new CoreException("Não é possível inativar, pois registro está sendo utilizado como parâmetro para outro.");
                    }
                    break;
                default:
                    break;
            }
        }

        public Local ObterPorNomeSigla(string login, bool somenteAtivos = false)
        {
            Local local = ObterTodos().FirstOrDefault(u => u.Sigla + " - " + u.Nome == login );

            if (!somenteAtivos || local == null)
                return local;
           
            if (!local.Ativo)
                return null;

            return local;
        }
    }
}
