﻿using Controlsys.Dominio.CredenciamentoVeiculos;
using Controlsys.Repositorio.Parametros;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Dialect.Function;

namespace Controlsys.Persistencia.Repositorios.Parametros
{
    public class RepositorioCredencialVeiculo : Repositorio<CredencialVeiculo>, IRepositorioCredencialVeiculo
    {

        public RepositorioCredencialVeiculo(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {

        }

        public SituacaoCredencialVeiculo SalvarStatus(SituacaoCredencialVeiculo entidade)
        {
            Repositorio<SituacaoCredencialVeiculo> repPapelLog = new Repositorio<SituacaoCredencialVeiculo>(UnidadeTrabalho);
            if (entidade.DataRegistro > DateTime.Now)
                throw new CoreException("Não é permitido informar uma data futura para Status");

            var credencial = ObterPorId(entidade.Credencial.Codigo);
            var statusCredencialRemota = credencial.Situacoes.OrderByDescending(c => c.Codigo).Select(s => s.Status).FirstOrDefault();
            StatusCredencial statusCredencial = entidade.Status;

            if (statusCredencial != StatusCredencial.Recebida &&  statusCredencialRemota == statusCredencial)
                throw new CoreException(string.Format("Não é possível salvar. Essa credencial já está '{0}'.", statusCredencial.ObterDescricaoEnum()));

            switch (statusCredencial)
            {
                case StatusCredencial.Recebida:
                    if (credencial.Situacoes.Any())
                        throw new CoreException("Não é possível salvar. O recebimento é informado durante o cadastro da credencial.");
                    break;

                case StatusCredencial.Inspecionada:
                    var statusRecebido = credencial.Situacoes.Where(s => s.Status == StatusCredencial.Recebida).FirstOrDefault();

                    if ((statusRecebido == null) || (statusCredencialRemota != StatusCredencial.Recebida))
                        throw new CoreException("Não é possível salvar. A inspeção só é permitida após documento recebido.");

                    if (statusRecebido.DataRegistro > entidade.DataRegistro)
                        throw new CoreException("Não é possível salvar. A data de inspeção deve ser superior a de recebimento.");
                    break;

                case StatusCredencial.Aprovada:
                    var statusInspecao = credencial.Situacoes.Where(s => s.Status == StatusCredencial.Inspecionada).FirstOrDefault();

                    if ((statusInspecao == null) || (statusCredencialRemota != StatusCredencial.Inspecionada))
                        throw new CoreException("Não é possível salvar. A aprovação só é permitida após inspeção.");

                    if (statusInspecao.DataRegistro > entidade.DataRegistro)
                        throw new CoreException("Não é possível salvar. A data de aprovação deve ser superior a de inspeção.");
                    break;

                case StatusCredencial.Impressa:
                    var statusAprovacao = credencial.Situacoes.Where(s => s.Status == StatusCredencial.Aprovada).FirstOrDefault();

                    if ((statusAprovacao == null) || (statusCredencialRemota != StatusCredencial.Aprovada))
                        throw new CoreException("A impressão é permitida apenas após aprovação.");

                    if (statusAprovacao.DataRegistro > entidade.DataRegistro)
                        throw new CoreException("Não é possível imprimir. A data de impressão deve ser superior a de aprovação.");
                    break;

                case StatusCredencial.Devolvida:
                    var statusImpressa = credencial.Situacoes.Where(s => s.Status == StatusCredencial.Impressa).FirstOrDefault();

                    if ((statusImpressa == null) || (statusCredencialRemota != StatusCredencial.Impressa))
                        throw new CoreException("Não é possível salvar. A devolução só é permitida após documento impresso.");

                    if (statusImpressa.DataRegistro > entidade.DataRegistro)
                        throw new CoreException("Não é possível salvar. A data de devolução deve ser superior a de impressão.");
                    break;

                default:
                    throw new CoreException("Não foi possível identificar o status.");
                    break;
            }

            repPapelLog.Salvar(entidade);
            return entidade;
        }

        public void Validar(CredencialVeiculo entidade, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    CamposObrigatorio(entidade);
                    //ExisteVeiculo(entidade);

                    //var listaCVAux = ObterTodos().Where(cv => cv.Status != StatusCredencial.Devolvida).ToList();
                    //var veiculoCredencialVeiculo = listaCVAux.SelectMany(x => x.Veiculos);
                    ////var placas = veiculoCredencialVeiculo.Any(v => entidade.Veiculos.Select(vc => vc.Veiculo.Placa).Contains(v.Veiculo.Placa));
                    //
                    //foreach (string placa in entidade.Veiculos.Select(cv => cv.Veiculo.Placa))
                    //{
                    //
                    //    //if (listaCVAux.Any(x => x.Veiculos.Select(v => v.Veiculo.Placa) == ""))
                    //    //    return;
                    //    //if (listaCVAux.Select(cv => cv.Veiculos.Select(v => v.Veiculo.Placa)))
                    //    //    throw new CoreException("");
                    //}

                    //if (string.IsNullOrWhiteSpace(entidade.Nome) || entidade.Fabricante == null)
                    //    throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    //if (Existe(entidade))
                    //    throw new CoreException("Já existe um registro com o nome informado.");
                    break;

                case EstadoObjeto.Alterado:
                    CamposObrigatorio(entidade);

                    //ExisteVeiculo(entidade);
                    //if (string.IsNullOrWhiteSpace(entidade.Nome) || entidade.Fabricante == null)
                    //    throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    //if (Existe(entidade))
                    //    throw new CoreException("Já existe um registro com o nome informado.");
                    //if (!entidade.Ativo)
                    //    throw new CoreException("O registro está inativo, não é possível alterar.");
                    break;

                default:
                    break;
            }

        }

        private void CamposObrigatorio(CredencialVeiculo entidade)
        {
            if (entidade.Empresa == null)
                throw new CoreException("Não é possível salvar. Empresa não informada.");
            if (entidade.Condutor == null)
                throw new CoreException("Não é possível salvar. Condutor não informado.");
            if (entidade.ModeloCredencial == null)
                throw new CoreException("Não é possível salvar. Modelo de credencial não informado.");
            if (entidade.TipoValidade == null)
                throw new CoreException("Não é possível salvar. Tipo de validade não informado.");
            if (entidade.DataVencimentoSeguro == null)
                throw new CoreException("Não é possível salvar. Data de vencimento do seguro não informada.");
            //if (!Enumerable.Range(1, 2).Contains(entidade.Veiculos.Where(v => v.Ativo).Count()))
            //    throw new CoreException("Não é possível salvar. É permitido cadastrar um ou até dois veículos por credencial.");
            if (entidade.Usuario == null)
                throw new CoreException("Não foi possível identificar o usuário logado.");
        }

    }
}
