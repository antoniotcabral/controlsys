﻿using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Controlsys.Repositorios.Parametros;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Repositorios.Parametros
{
    /// <summary>
    /// Representa um(a) RepositorioTreinamento.
    /// </summary>
    public class RepositorioTreinamento : Repositorio<Treinamento>, IRepositorioTreinamento
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Parametros.RepositorioTreinamento.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioTreinamento(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {

        }

        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="treinamento">
        /// O(a) treinamento.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool Existe(Treinamento treinamento)
        {
            return ObterTodos().Any(a => a.Nome.ToUpper() == treinamento.Nome.ToUpper() && a.Ativo && a.Codigo != treinamento.Codigo);
        }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        public void Validar(Treinamento entidade, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    if (entidade.Nome == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe um treinamento com o nome informado.");
                    }
                    break;
                case EstadoObjeto.Alterado:
                    if (!entidade.Ativo)
                    {
                        throw new CoreException("O treinamento está inativo, não é possível alterar");
                    }
                    if (entidade.Nome == null )
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe um treinamento com o nome informado.");
                    }
                    break;
                case EstadoObjeto.Ativado:
                    if (!entidade.Ativo && Existe(entidade))
                    {
                        throw new CoreException("Já existe um treinamento com o nome informado.");
                    }
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Salvar treinamento pessoa.
        /// </summary>
        ///
        /// <param name="papel">
        /// O(a) papel.
        /// </param>
        /// <param name="tp">
        /// O(a) TP.
        /// </param>
        public void SalvarTreinamentoPessoa(Papel papel, TreinamentoPessoa tp)
        {
            Repositorio<TreinamentoPessoa> rep = new Repositorio<TreinamentoPessoa>(base.UnidadeTrabalho);
            tp.Papel = papel;
            tp.DataRegistro = DateTime.Now;
            tp.Ativar();
            rep.Salvar(tp);
        }
    }
}
