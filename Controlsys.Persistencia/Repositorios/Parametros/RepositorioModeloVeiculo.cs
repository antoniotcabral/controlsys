﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Parametros;
using Controlsys.Repositorio.Parametros;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;

namespace Controlsys.Persistencia.Repositorios.Parametros
{
    public class RepositorioModeloVeiculo : Repositorio<ModeloVeiculo>, IRepositorioModeloVeiculo
    {

        public RepositorioModeloVeiculo(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {

        }

        public bool Existe(ModeloVeiculo entidade)
        {
            return ObterTodos().Any(l => l.Nome.ToUpper() == entidade.Nome.ToUpper() && l.Ativo && l.Codigo != entidade.Codigo);
        }

        public void Validar(ModeloVeiculo entidade, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    if (string.IsNullOrWhiteSpace(entidade.Nome) || entidade.Fabricante == null)
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    if (Existe(entidade))
                        throw new CoreException("Já existe um registro com o nome informado.");
                    break;

                case EstadoObjeto.Alterado:
                    if (string.IsNullOrWhiteSpace(entidade.Nome) || entidade.Fabricante == null)
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    if (Existe(entidade))
                        throw new CoreException("Já existe um registro com o nome informado.");
                    if (!entidade.Ativo)
                        throw new CoreException("O registro está inativo, não é possível alterar.");
                    break;

                case EstadoObjeto.Ativado:
                    if (!entidade.Ativo && Existe(entidade))
                        throw new CoreException("Já existe um registro ativo com o nome informado.");
                    break;

                default:
                    break;
            }

        }
    }
}
