﻿using Controlsys.Dominio.CredenciamentoVeiculos;
using Controlsys.Dominio.Parametros;
using Controlsys.Repositorio.Parametros;
using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Globalsys.Exceptions;
using Globalsys.Validacao;

namespace Controlsys.Persistencia.Repositorios.Parametros
{
    public class RepositorioVeiculoMidia : Repositorio<VeiculoMidia>, IRepositorioVeiculoMidia
    {
        public RepositorioVeiculoMidia(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho) { }

        public bool Existe(VeiculoMidia entidade)
        {
            return ObterTodos().Any(vm => 
                (vm.Codigo != entidade.Codigo) && 
                (vm.Veiculo.Codigo == entidade.Veiculo.Codigo) &&
                (vm.Midia.Codigo == entidade.Midia.Codigo)
            );
        }

        public void Validar(VeiculoMidia entidade, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                case EstadoObjeto.Alterado:
                    //if (Existe(entidade))
                    //{
                    //    throw new CoreException("Já existe uma Midia com mesmo nome associada ao veículo informado.");
                    //}
                    break;
                default:
                    break;
            }
        }

        //public VeiculoMidia Salvar(Veiculo veiculo, Midia midia)
        //{
        //    Repositorio<VeiculoMidia> rep = new Repositorio<VeiculoMidia>(base.UnidadeTrabalho);
        //
        //    VeiculoMidia obj = new VeiculoMidia();
        //    obj.Veiculo = veiculo;
        //    obj.Midia = midia;
        //    obj.DataRegistro = DateTime.Now;
        //    obj.Ativar();
        //    rep.Salvar(obj);
        //
        //    return obj;
        //}
    }
}
