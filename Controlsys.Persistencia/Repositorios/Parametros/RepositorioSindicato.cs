﻿using Controlsys.Dominio.Parametros;
using Controlsys.Persistencia.Repositorios.Empresas;
using Controlsys.Repositorio.Parametros;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Repositorios.Parametros
{
    public class RepositorioSindicato : Repositorio<Sindicato>, IRepositorioSindicato
    {
        public RepositorioSindicato(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        { }
        public bool Existe(Sindicato sindicato)
        { return ObterTodos().Any(a => a.Nome.ToUpper() == sindicato.Nome.ToUpper() && a.Ativo && a.Codigo != sindicato.Codigo); }
        public void Validar(Sindicato entidade, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    if (entidade.Nome == null)
                    { throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos."); }
                    if (Existe(entidade))
                    { throw new CoreException("Já existe um sindicato com o nome informado."); }
                    break;
                case EstadoObjeto.Alterado:
                    //if(!entidade.Ativo)
                    //{
                    //    throw new CoreException("O sindicato está inativo, não é possível alterar");
                    //}
                    if (entidade.Nome == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe um sindicato com o nome informado.");
                    }
                    break;
                case EstadoObjeto.Ativado:
                    if (!entidade.Ativo && Existe(entidade))
                    {
                        throw new CoreException("Já existe um sindicato com o nome informado.");
                    }
                    break;
                case EstadoObjeto.Inativado:
                    var empresas = new RepositorioEmpresaSindicato(UnidadeTrabalho).ObterTodos().Where(os => os.Ativo && os.Sindicato.Codigo.Equals(entidade.Codigo));
                    if (empresas.Any())
                    {
                        int qtd = empresas.Count();
                        string descricao = string.Format("Não é possível inativar. Registro possui associação ativa com a empresa '{0}'", empresas.First().Empresa.Nome);
                        if (qtd == 1)
                            //descricao += ".";
                            descricao = string.Concat(descricao, ".");
                        else
                            descricao += string.Format(", e com mais {0} {1}.", qtd, qtd == 2 ? "empresa" : "empresas");
                        throw new CoreException(descricao);
                    }
                    break;
                default:
                    break;
            }
        }
    }
}