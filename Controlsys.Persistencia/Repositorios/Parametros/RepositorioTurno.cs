﻿using Controlsys.Dominio.Parametros;
using Controlsys.Repositorios.Parametros;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Repositorios.Parametros
{
    /// <summary>
    /// Representa um(a) RepositorioTurno.
    /// </summary>
    public class RepositorioTurno : Repositorio<Turno>, IRepositorioTurno
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Parametros.RepositorioTurno.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioTurno(IUnidadeTrabalho unidadeTrabalho) : base(unidadeTrabalho) {}

        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="turno">
        /// O(a) turno.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool Existe(Turno turno)
        {
            return ObterTodos().Any(m => m.Nome.ToUpper() == turno.Nome.ToUpper() && m.Ativo && m.Codigo != turno.Codigo);
        }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        public void Validar(Turno entidade, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    if (entidade.Nome == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe um turno com o nome informado.");
                    }
                    if (entidade.Tolerancia.HasValue && (entidade.Tolerancia.Value < 0 || entidade.Tolerancia.Value > 999))
                    {
                        throw new CoreException("A tolerância deve ser de 0 a 999 minutos.");
                    }
                    break;
                case EstadoObjeto.Alterado:
                    if (!entidade.Ativo)
                    {
                        throw new CoreException("O turno está inativado, não é possível alterar");
                    }
                    if (entidade.Nome == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe um turno com o nome informado.");
                    }
                    if (entidade.Tolerancia.HasValue && (entidade.Tolerancia.Value < 0 || entidade.Tolerancia.Value > 999))
                    {
                        throw new CoreException("A tolerância deve ser de 0 a 999 minutos.");
                    }
                    break;
                case EstadoObjeto.Ativado:
                    if (!entidade.Ativo && Existe(entidade))
                    {
                        throw new CoreException("Já existe um turno com o nome informado.");
                    }
                    break;
                case EstadoObjeto.Inativado:
                    if (new Repositorio<GrupoTrabalhoTurno>(base.UnidadeTrabalho).ObterTodos().Where(gtt => gtt.Turno.Codigo == entidade.Codigo && gtt.Ativo && gtt.GrupoTrabalho.Ativo).Count() > 0)
                    {
                        throw new CoreException("Existe um Grupo de Trabalho ativo associado a este turno");
                    }
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Salvar hora turno.
        /// </summary>
        ///
        /// <param name="horaTurno">
        /// O(a) hora turno.
        /// </param>
        public void SalvarHoraTurno(HoraTurno horaTurno)
        {
            Repositorio<HoraTurno> rep = new Repositorio<HoraTurno>(base.UnidadeTrabalho);
            rep.Salvar(horaTurno);
        }

        /// <summary>
        /// Salvar feriado turno.
        /// </summary>
        ///
        /// <param name="feriado">
        /// O(a) feriado.
        /// </param>
        public void SalvarFeriadoTurno(FeriadoTurno feriado)
        {
            Repositorio<FeriadoTurno> rep = new Repositorio<FeriadoTurno>(base.UnidadeTrabalho);
            rep.Salvar(feriado);
        }

    }
}
