﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Parametros;
using Controlsys.Repositorio.Parametros;
using Globalsys;
using Globalsys.Validacao;
using Globalsys.Exceptions;

namespace Controlsys.Persistencia.Repositorios.Parametros
{
    public class RepositorioTipoValidade : Repositorio<TipoValidade>, IRepositorioTipoValidade
    {
        public RepositorioTipoValidade(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
        }


        public bool Existe(TipoValidade entidade)
        {
            return ObterTodos().Any(l => l.Nome.ToUpper() == entidade.Nome.ToUpper() && l.Ativo && l.Codigo != entidade.Codigo);
        }

        public void Validar(TipoValidade entidade, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    ValidarDados(entidade);
                    break;

                case EstadoObjeto.Alterado:
                    ValidarDados(entidade);
                    if (!entidade.Ativo)
                        throw new CoreException("O tipo de validade está inativo, não é possível alterar.");
                    break;

                case EstadoObjeto.Ativado:
                    if (!entidade.Ativo && Existe(entidade))
                        throw new CoreException("Já existe um tipo de validade ativo com o nome informado.");
                    break;

                default:
                    break;
            }
        }

        private void ValidarDados(TipoValidade entidade)
        {
            if (string.IsNullOrEmpty(entidade.Nome))
                throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
            if (entidade.IdadeInicial >= entidade.IdadeFinal)
                throw new CoreException("Não é possível salvar. A idade inicial deve ser menor que a idade final.");
            if (entidade.IdadeInicial < 0 || entidade.IdadeFinal < 0)
                throw new CoreException("Não é possível salvar. A idade não pode ser negativa.");
            if (entidade.DiasValidade <= 0)
                throw new CoreException("Não é possível salvar. A quantidade de dias permitidos deve ser maior que 0.");
            if (Existe(entidade))
                throw new CoreException("Já existe um tipo de validade com o nome informado.");
        }

    }
}
