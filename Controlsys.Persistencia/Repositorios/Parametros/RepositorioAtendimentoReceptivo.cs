﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Parametros;
using Controlsys.Repositorio.Parametros;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;

namespace Controlsys.Persistencia.Repositorios.Parametros
{
    public class RepositorioAtendimentoReceptivo : Repositorio<AtendimentoReceptivo>, IRepositorioAtendimentoReceptivo
    {
        public RepositorioAtendimentoReceptivo(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
        }

        public bool Existe(AtendimentoReceptivo entidade)
        {
            return ObterTodos().Any(dm =>
                (dm.Codigo != entidade.Codigo) &&
                (dm.Midia.NomeArquivo == entidade.Midia.NomeArquivo)
            );
        }

        public void Validar(AtendimentoReceptivo entidade, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                case EstadoObjeto.Alterado:
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe uma Midia associada com mesmo o nome.");
                    }
                    break;

                default:
                    break;
            }
        }

    }
}
