﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Parametros;
using Controlsys.Repositorio.Parametros;
using Globalsys;
using Globalsys.Validacao;
using Globalsys.Exceptions;
using Controlsys.Dominio.Acesso;

namespace Controlsys.Persistencia.Repositorios.Parametros
{
    public class RepositorioTipoAlimentacaoRestaurante : Repositorio<TipoAlimentacaoRestaurante>, IRepositorioTipoAlimentacaoRestaurante
    {

        public RepositorioTipoAlimentacaoRestaurante(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho) { }

        public void Validar(TipoAlimentacaoRestaurante entidade, TipoAlimentacaoRestaurante[] alterados, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    if (verificaConflitoHorario(entidade, alterados))
                        throw new CoreException(string.Format("Não é possível incluir o tipo de alimentação '{0}', pois existe um conflito de horário com outro tipo de alimentação.",
                            entidade.TipoAlimentacao.Nome));
                    break;

                case EstadoObjeto.Alterado:
                    if (verificaConflitoHorario(entidade, alterados))
                        throw new CoreException(string.Format("Não é possível incluir o tipo de alimentação '{0}', pois existe um conflito de horário com outro tipo de alimentação.",
                            entidade.TipoAlimentacao.Nome));
                    break;

                case EstadoObjeto.Inativado:
                    if (entidade.Ativo && verificaDependencia(entidade))
                        throw new CoreException(string.Format("Não é possível inativar o tipo de alimentação '{0}', pois ele está vinculado a um acesso de restaurante.",
                            entidade.TipoAlimentacao.Nome));
                    break;

                default:
                    break;
            }
        }

        //public void ValidarAlteracao(TipoAlimentacaoRestaurante entidade, TipoAlimentacaoRestaurante[] todosGrid, EstadoObjeto estadoObjeto)
        //{
        //    switch (estadoObjeto)
        //    {
        //        case EstadoObjeto.Novo:
        //            if (verificaConflitoHorario(entidade, todosGrid))
        //                throw new CoreException(string.Format("Não é possível incluir o tipo de alimentação '{0}', pois existe um conflito de horário com outro tipo de alimentação.",
        //                    entidade.TipoAlimentacao.Nome));
        //            break;

        //        case EstadoObjeto.Alterado:
        //            if (verificaConflitoHorario(entidade, todosGrid))
        //                throw new CoreException(string.Format("Não é possível incluir o tipo de alimentação '{0}', pois existe um conflito de horário com outro tipo de alimentação.",
        //                    entidade.TipoAlimentacao.Nome));
        //            break;

        //        default:
        //            break;
        //    }
        //}

        private bool verificaDependencia(TipoAlimentacaoRestaurante tipoAlimentacao)
        {
            Repositorio<AcessoRestauranteTipoAlimentacao> rep = new Repositorio<AcessoRestauranteTipoAlimentacao>(base.UnidadeTrabalho);
            return rep.ObterTodos().Any(t => t.Ativo && t.TipoAlimentacao.Ativo &&
                t.TipoAlimentacao.TipoAlimentacao.Codigo == tipoAlimentacao.TipoAlimentacao.Codigo &&
                t.TipoAlimentacao.Restaurante.Codigo == tipoAlimentacao.Restaurante.Codigo);
        }

        private bool verificaConflitoHorario(TipoAlimentacaoRestaurante tipoAlimentacao, TipoAlimentacaoRestaurante[] alterados)
        {
            var temConflito = ObterTodos().Any(t => t.Ativo && t.TipoAlimentacao.Codigo != tipoAlimentacao.TipoAlimentacao.Codigo && (

                    (tipoAlimentacao.HoraInicio > t.HoraInicio && tipoAlimentacao.HoraInicio < t.HoraFim) || //HORA INICIAL ADICIONADA ENTRE AS JA EXISTENTES
                    (tipoAlimentacao.HoraFim > t.HoraInicio && tipoAlimentacao.HoraFim < t.HoraFim) || //HORA FINAL ADICIONADA ENTRE AS JA EXISTENTES

                    ((t.HoraInicio < t.HoraFim) && tipoAlimentacao.HoraInicio <= t.HoraInicio && tipoAlimentacao.HoraFim >= t.HoraFim) || //VERIFICA CONFLITO DE HORA 'ENGOLINDO' HORA EXISTENTE 

                    ((t.HoraInicio > t.HoraFim) && (tipoAlimentacao.HoraInicio > tipoAlimentacao.HoraFim) && //VERIFICA SE VIRA O DIA
                                        (
                                            tipoAlimentacao.HoraInicio >= t.HoraInicio || tipoAlimentacao.HoraInicio < t.HoraFim || tipoAlimentacao.HoraFim <= t.HoraFim || tipoAlimentacao.HoraFim > t.HoraInicio// CONDIÇÕES PARA VIRADA DE DIA
                                        )) ||

                    ((t.HoraInicio > t.HoraFim) && (tipoAlimentacao.HoraInicio > tipoAlimentacao.HoraFim) && (tipoAlimentacao.HoraInicio <= t.HoraInicio && tipoAlimentacao.HoraFim >= t.HoraFim)) ||
                    
                    ((t.HoraInicio > t.HoraFim) && (tipoAlimentacao.HoraInicio >= t.HoraInicio)) ||
                    ((t.HoraInicio > t.HoraFim) && (tipoAlimentacao.HoraFim <= t.HoraFim)) ||

                    ((tipoAlimentacao.HoraInicio > tipoAlimentacao.HoraFim) && (tipoAlimentacao.HoraInicio <= t.HoraInicio)) ||
                    ((tipoAlimentacao.HoraInicio > tipoAlimentacao.HoraFim) && (tipoAlimentacao.HoraFim >= t.HoraFim)) 

                ) && t.Restaurante.Codigo == tipoAlimentacao.Restaurante.Codigo);


            //VERIFICA SE TEM CONFLITO E FORAM ALTERADOS REGISTROS AO MESMO TEMPO
            if (temConflito && alterados != null)
            {
                var listTpAliConflitados = ObterTodos().Where(t => t.Ativo && t.TipoAlimentacao.Codigo != tipoAlimentacao.TipoAlimentacao.Codigo && (

                    (tipoAlimentacao.HoraInicio > t.HoraInicio && tipoAlimentacao.HoraInicio < t.HoraFim) || //HORA INICIAL ADICIONADA ENTRE AS JA EXISTENTES
                    (tipoAlimentacao.HoraFim > t.HoraInicio && tipoAlimentacao.HoraFim < t.HoraFim) || //HORA FINAL ADICIONADA ENTRE AS JA EXISTENTES

                    ((t.HoraInicio < t.HoraFim) && tipoAlimentacao.HoraInicio <= t.HoraInicio && tipoAlimentacao.HoraFim >= t.HoraFim) || //VERIFICA CONFLITO DE HORA 'ENGOLINDO' HORA EXISTENTE 

                    ((t.HoraInicio > t.HoraFim) && (tipoAlimentacao.HoraInicio > tipoAlimentacao.HoraFim) && //VERIFICA SE VIRA O DIA
                                        (
                                            tipoAlimentacao.HoraInicio >= t.HoraInicio || tipoAlimentacao.HoraInicio < t.HoraFim || tipoAlimentacao.HoraFim <= t.HoraFim || tipoAlimentacao.HoraFim > t.HoraInicio// CONDIÇÕES PARA VIRADA DE DIA
                                        )) ||

                    ((t.HoraInicio > t.HoraFim) && (tipoAlimentacao.HoraInicio > tipoAlimentacao.HoraFim) && (tipoAlimentacao.HoraInicio <= t.HoraInicio && tipoAlimentacao.HoraFim >= t.HoraFim)) ||

                    ((t.HoraInicio > t.HoraFim) && (tipoAlimentacao.HoraInicio >= t.HoraInicio)) ||
                    ((t.HoraInicio > t.HoraFim) && (tipoAlimentacao.HoraFim <= t.HoraFim)) ||

                    ((tipoAlimentacao.HoraInicio > tipoAlimentacao.HoraFim) && (tipoAlimentacao.HoraInicio <= t.HoraInicio)) ||
                    ((tipoAlimentacao.HoraInicio > tipoAlimentacao.HoraFim) && (tipoAlimentacao.HoraFim >= t.HoraFim)) 

              ) && t.Restaurante.Codigo == tipoAlimentacao.Restaurante.Codigo).ToList();

                //VERIFICA SE O CONFLITO GERADO FOI ALTERADO
                var listConflitosForamAlterados = alterados.Where(c => listTpAliConflitados.Select(a => a.TipoAlimentacao.Codigo).Contains(c.TipoAlimentacao.Codigo)).ToList();

                if (listConflitosForamAlterados.Count > 0)
                {
                    temConflito = listConflitosForamAlterados.Any(t => t.TipoAlimentacao.Codigo != tipoAlimentacao.TipoAlimentacao.Codigo && (

                                (tipoAlimentacao.HoraInicio > t.HoraInicio && tipoAlimentacao.HoraInicio < t.HoraFim) || //HORA INICIAL ADICIONADA ENTRE AS JA EXISTENTES
                                (tipoAlimentacao.HoraFim > t.HoraInicio && tipoAlimentacao.HoraFim < t.HoraFim) || //HORA FINAL ADICIONADA ENTRE AS JA EXISTENTES

                                ((t.HoraInicio < t.HoraFim) && tipoAlimentacao.HoraInicio <= t.HoraInicio && tipoAlimentacao.HoraFim >= t.HoraFim) || //VERIFICA CONFLITO DE HORA 'ENGOLINDO' HORA EXISTENTE 

                                ((t.HoraInicio > t.HoraFim) && (tipoAlimentacao.HoraInicio > tipoAlimentacao.HoraFim) && //VERIFICA SE VIRA O DIA
                                                    (
                                                        tipoAlimentacao.HoraInicio >= t.HoraInicio || tipoAlimentacao.HoraInicio < t.HoraFim || tipoAlimentacao.HoraFim <= t.HoraFim || tipoAlimentacao.HoraFim > t.HoraInicio// CONDIÇÕES PARA VIRADA DE DIA
                                                    )) ||

                                ((t.HoraInicio > t.HoraFim) && (tipoAlimentacao.HoraInicio > tipoAlimentacao.HoraFim) && (tipoAlimentacao.HoraInicio <= t.HoraInicio && tipoAlimentacao.HoraFim >= t.HoraFim)) ||

                                ((t.HoraInicio > t.HoraFim) && (tipoAlimentacao.HoraInicio >= t.HoraInicio)) ||
                                ((t.HoraInicio > t.HoraFim) && (tipoAlimentacao.HoraFim <= t.HoraFim)) ||

                                ((tipoAlimentacao.HoraInicio > tipoAlimentacao.HoraFim) && (tipoAlimentacao.HoraInicio <= t.HoraInicio)) ||
                                ((tipoAlimentacao.HoraInicio > tipoAlimentacao.HoraFim) && (tipoAlimentacao.HoraFim >= t.HoraFim)) 

                    ) );
                }

                if (!temConflito)
                {
                    //VERIFICA CASOS ENTRE OS ALTERADOS
                    temConflito = alterados.Any(t => t.TipoAlimentacao.Codigo != tipoAlimentacao.TipoAlimentacao.Codigo && (

                                (tipoAlimentacao.HoraInicio > t.HoraInicio && tipoAlimentacao.HoraInicio < t.HoraFim) || //HORA INICIAL ADICIONADA ENTRE AS JA EXISTENTES
                                (tipoAlimentacao.HoraFim > t.HoraInicio && tipoAlimentacao.HoraFim < t.HoraFim) || //HORA FINAL ADICIONADA ENTRE AS JA EXISTENTES

                                ((t.HoraInicio < t.HoraFim) && tipoAlimentacao.HoraInicio <= t.HoraInicio && tipoAlimentacao.HoraFim >= t.HoraFim) || //VERIFICA CONFLITO DE HORA 'ENGOLINDO' HORA EXISTENTE 

                                ((t.HoraInicio > t.HoraFim) && (tipoAlimentacao.HoraInicio > tipoAlimentacao.HoraFim) && //VERIFICA SE VIRA O DIA
                                                    (
                                                        tipoAlimentacao.HoraInicio >= t.HoraInicio || tipoAlimentacao.HoraInicio < t.HoraFim || tipoAlimentacao.HoraFim <= t.HoraFim || tipoAlimentacao.HoraFim > t.HoraInicio// CONDIÇÕES PARA VIRADA DE DIA
                                                    )) ||

                                ((t.HoraInicio > t.HoraFim) && (tipoAlimentacao.HoraInicio > tipoAlimentacao.HoraFim) && (tipoAlimentacao.HoraInicio <= t.HoraInicio && tipoAlimentacao.HoraFim >= t.HoraFim)) ||

                                ((t.HoraInicio > t.HoraFim) && (tipoAlimentacao.HoraInicio >= t.HoraInicio)) ||
                                ((t.HoraInicio > t.HoraFim) && (tipoAlimentacao.HoraFim <= t.HoraFim)) ||

                                ((tipoAlimentacao.HoraInicio > tipoAlimentacao.HoraFim) && (tipoAlimentacao.HoraInicio <= t.HoraInicio)) ||
                                ((tipoAlimentacao.HoraInicio > tipoAlimentacao.HoraFim) && (tipoAlimentacao.HoraFim >= t.HoraFim))  
                    ));

                }
            }

            return temConflito;
        }
    }
}
