﻿using Controlsys.Dominio.Parametros;
using Controlsys.Repositorio.Parametros;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Controlsys.Persistencia.Repositorios.Parametros
{
    /// <summary>
    /// Representa um(a) RepositorioDocumentoMidia.
    /// </summary>
    public class RepositorioDocumentoMidia : Repositorio<DocumentoMidia>, IRepositorioDocumentoMidia
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Parametros.RepositorioDocumentoMidia.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioDocumentoMidia(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
        }

        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="DocumentoMidia">
        /// O(a) midia.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool Existe(DocumentoMidia documentoMidia)
        {
            return ObterTodos().Any(dm => dm.Codigo != documentoMidia.Codigo && dm.Documento.Codigo == documentoMidia.Documento.Codigo && dm.Midia.Codigo == documentoMidia.Midia.Codigo);
        }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        public void Validar(DocumentoMidia entidade, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe uma Midia com mesmo nome associada ao documento do nome informado.");
                    }
                    break;
                case EstadoObjeto.Alterado:
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe uma Midia com mesmo nome associada ao documento do nome informado.");
                    }
                    break;
                default:
                    break;
            }
        }

        public DocumentoMidia SalvarDocMidia(Documento doc, Midia midia)
        {
            Repositorio<DocumentoMidia> repDocMidia = new Repositorio<DocumentoMidia>(base.UnidadeTrabalho);

            DocumentoMidia documentoMidia = new DocumentoMidia();
            documentoMidia.Documento = doc;
            documentoMidia.Midia = midia;
            documentoMidia.DataRegistro = DateTime.Now;
            documentoMidia.Ativar();
            repDocMidia.Salvar(documentoMidia);

            return documentoMidia;
        }
    }
}
