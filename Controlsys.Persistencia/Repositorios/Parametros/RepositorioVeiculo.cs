﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.CredenciamentoVeiculos;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using Controlsys.Repositorio.Parametros;

namespace Controlsys.Persistencia.Repositorios.Parametros
{
    public class RepositorioVeiculo : Repositorio<Veiculo>, IRepositorioVeiculo
    {
        public RepositorioVeiculo(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {

        }

        public void Validar(Veiculo entidade, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                case EstadoObjeto.Alterado:
                    if (entidade.Modelo == null)
                        throw new CoreException("Não é possível salvar. Modelo do veículo informado nulo ou inválido.");

                    if (!Enumerable.Range(DateTime.Now.AddYears(-100).Year, DateTime.Now.Year).Contains(entidade.AnoFabricacao))
                        throw new CoreException("Não é possível salvar. Ano de fabricação do veículo nulo ou inválido.");

                    if (!Enumerable.Range(DateTime.Now.AddYears(-100).Year, DateTime.Now.AddYears(1).Year).Contains(entidade.AnoModelo))
                        throw new CoreException("Não é possível salvar. Ano do modelo de veículo nulo ou inválido.");

                    if(string.IsNullOrEmpty(entidade.Cor))
                        throw new CoreException("Não é possível salvar. Cor de veículo nulo inválido.");

                    if(string.IsNullOrEmpty(entidade.Placa))
                        throw new CoreException("Não é possível salvar. Placa de veículo nula ou inválida.");
                    break;

                default:
                    break;
            }

        }
    }
}
