﻿using Controlsys.Dominio.Empresas;
using Controlsys.Repositorios.Parametros;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Repositorios.Parametros
{
    /// <summary>
    /// Representa um(a) RepositorioSetorCusto.
    /// </summary>
    public class RepositorioSetorCusto : Repositorio<SetorCusto>, IRepositorioSetorCusto
    {
         /// <summary>
         /// Construtor para Controlsys.Persistencia.Repositorios.Parametros.RepositorioSetorCusto.
         /// </summary>
         ///
         /// <param name="unidadeTrabalho">
         /// O(a) unidade trabalho.
         /// </param>
         public RepositorioSetorCusto(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {

        }

        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="setor">
        /// O(a) setor.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool Existe(SetorCusto setor)
        {
            return ObterTodos().Any(s => s.Numero == setor.Numero && s.Ativo && s.Codigo != setor.Codigo);
        }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        public void Validar(SetorCusto entidade, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                  if (entidade.Nome == null || entidade.Numero == null )
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe um setor com o número informado.");
                    }
                    break;
                case EstadoObjeto.Alterado:
                     if(!entidade.Ativo)
                    {
                        throw new CoreException("O setor está inativo, não é possível alterar");
                    }
                    if (entidade.Nome == null || entidade.Numero == null )
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe um setor com o número informado.");
                    }
                    break;
                case EstadoObjeto.Ativado:
                    if (!entidade.Ativo && Existe(entidade))
                    {
                        throw new CoreException("Já existe um setor com o nome informado.");
                    }
                    break;
                default:
                    break;
            }
        }
        }
   
}
