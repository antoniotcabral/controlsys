﻿using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Controlsys.Repositorios.Parametros;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Repositorios.Parametros
{
    /// <summary>
    /// Representa um(a) RepositorioGrupoTrabalho.
    /// </summary>
    public class RepositorioGrupoTrabalho : Repositorio<GrupoTrabalho>, IRepositorioGrupoTrabalho
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Parametros.RepositorioGrupoTrabalho.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioGrupoTrabalho(IUnidadeTrabalho unidadeTrabalho) : base(unidadeTrabalho) { }

        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="grupoTrabalho">
        /// O(a) grupo trabalho.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool Existe(GrupoTrabalho grupoTrabalho)
        {
            return ObterTodos().Any(s => s.Nome.ToUpper() == grupoTrabalho.Nome.ToUpper() && s.Ativo && s.Codigo != grupoTrabalho.Codigo);
        }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        public void Validar(GrupoTrabalho entidade, EstadoObjeto estadoObjeto)
        {

            #region [Desabilitado, pois quando é executado não possui registros de GrupoTrabalhoColab para validar]
            //Repositorio<GrupoTrabalhoColab> rep = new Repositorio<GrupoTrabalhoColab>(base.UnidadeTrabalho);
            //var grpsColabAtivos = rep.ObterTodos().Where(x => x.GrupoTrabalho.Ativo == true).Select(x =>
            //    new
            //    {
            //        GrupoTrabalho = x.GrupoTrabalho.Nome,
            //        Colaborador = x.Papel.PessoaFisica.Codigo
            //    }).ToList();
            //if (entidade.GrupoTrabalhoColaboradores.Count > 0)
            //{
            //    var pf = entidade.GrupoTrabalhoColaboradores.FirstOrDefault().Papel.PessoaFisica;
            //    var xx = grpsColabAtivos.FirstOrDefault(x => x.Colaborador == pf.Codigo);
            //
            //    if (xx != null)
            //    {
            //        string retorno = string.Format("O colaborador '{0}' já é vinculado ao Grupo de Trabalho '{1}'.", pf.Nome, xx.GrupoTrabalho);
            //        throw new CoreException(retorno);
            //    }
            //} 
            #endregion

            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    if (entidade.Nome == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe um grupo de trabalho com o nome informado.");
                    }
                    break;
                case EstadoObjeto.Alterado:
                    if (!entidade.Ativo)
                    {
                        throw new CoreException("O grupo de trabalho está inativo, não é possível alterar");
                    }
                    if (entidade.Nome == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe um grupo de trabalho com o nome informado.");
                    }
                    break;
                case EstadoObjeto.Ativado:
                    if (!entidade.Ativo && Existe(entidade))
                    {
                        throw new CoreException("Já existe um grupo de trabalho com o nome informado.");
                    }
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Salvar grupo trabalho turno.
        /// </summary>
        ///
        /// <param name="grupoTrabalhoTurno">
        /// O(a) grupo trabalho turno.
        /// </param>
        public void SalvarGrupoTrabalhoTurno(GrupoTrabalhoTurno grupoTrabalhoTurno)
        {
            Repositorio<GrupoTrabalhoTurno> rep = new Repositorio<GrupoTrabalhoTurno>(base.UnidadeTrabalho);
            rep.Salvar(grupoTrabalhoTurno);
        }

        /// <summary>
        /// Salvar grupo trabalho colaborador.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="grupoTrabalhoColab">
        /// O(a) grupo trabalho colab.
        /// </param>
        public void SalvarGrupoTrabalhoColaborador(GrupoTrabalhoColab grupoTrabalhoColab)
        {
            if (!(grupoTrabalhoColab.Papel is Colaborador))
                throw new CoreException(string.Format("O papel selecionado para '{0}' não é de um Colaborador.", grupoTrabalhoColab.Papel.PessoaFisica.Nome));

            Repositorio<GrupoTrabalhoColab> rep = new Repositorio<GrupoTrabalhoColab>(base.UnidadeTrabalho);

            var gruposAtivos = rep.ObterTodos()
                                  .Where(x => x.Ativo
                                      && x.Codigo != grupoTrabalhoColab.Codigo
                                      && x.GrupoTrabalho.Ativo )
                                  .Select(x => new
                                         {
                                              GrupoTrabalho = x.GrupoTrabalho.Nome,
                                              Colaborador = x.Papel.Codigo
                                         }).ToList();

            if (gruposAtivos.Any())
            {
                var p = grupoTrabalhoColab.Papel;
                var grupoAtivo = gruposAtivos.FirstOrDefault(x => x.Colaborador == p.Codigo);
                if (grupoAtivo != null)
                    throw new CoreException(string.Format("O colaborador '{0}' já está vinculado ao Grupo de Trabalho '{1}'.", p.PessoaFisica.Nome, grupoAtivo.GrupoTrabalho));
            }

            rep.Salvar(grupoTrabalhoColab);
        }
    }
}
