﻿using Controlsys.Dominio.Parametros;
using Controlsys.Repositorio.Parametros;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Repositorios.Parametros
{
    public class RepositorioTipoAlimentacao : Repositorio<TipoAlimentacao>, IRepositorioTipoAlimentacao
    {

        public RepositorioTipoAlimentacao(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho) { }

        public bool Existe(TipoAlimentacao tipoA)
        {
            return ObterTodos().Any(a => a.Nome.ToUpper() == tipoA.Nome.ToUpper() && a.Ativo && a.Codigo != tipoA.Codigo);
        }

        public void Validar(TipoAlimentacao entidade, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    if (entidade.Nome == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe um tipo de alimentação com o nome informado.");
                    }
                    break;
                case EstadoObjeto.Alterado:
                    if (!entidade.Ativo)
                    {
                        throw new CoreException("O tipo de alimentação está inativo, não é possível alterar.");
                    }
                    if (entidade.Nome == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe um tipo de alimentação com o nome informado.");
                    }
                    break;
                case EstadoObjeto.Ativado:
                    if (!entidade.Ativo && Existe(entidade))
                    {
                        throw new CoreException("Já existe um tipo de alimentação com o nome informado.");
                    }
                    break;

                case EstadoObjeto.Inativado:
                    if (entidade.Ativo && verificaDependencia(entidade))
                        throw new CoreException(string.Format("Não é possível inativar tipo de alimentação '{0}', pois está vinculado a restaurante ativo.", entidade.Nome));
                    break;

                default:
                    break;
            }
        }

        private bool verificaDependencia(TipoAlimentacao entidade)
        {
            Repositorio<TipoAlimentacaoRestaurante> rep = new Repositorio<TipoAlimentacaoRestaurante>(base.UnidadeTrabalho);
            return rep.ObterTodos().Any(r => r.TipoAlimentacao.Codigo == entidade.Codigo && r.Ativo && r.Restaurante.Ativo);
        }

    }
}
