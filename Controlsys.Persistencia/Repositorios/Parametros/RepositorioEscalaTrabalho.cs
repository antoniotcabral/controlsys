﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Parametros;
using Controlsys.Repositorios.Parametros;
using Globalsys;

namespace Controlsys.Persistencia.Repositorios.Parametros
{
    /// <summary>
    /// Representa um(a) RepositorioEscalaTrabalho.
    /// </summary>
    public class RepositorioEscalaTrabalho : Repositorio<EscalaTrabalho>, IRepositorioEscalaTrabalho
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Parametros.RepositorioEscalaTrabalho.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioEscalaTrabalho(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {

        }

        public void GerarEscala(GrupoTrabalho grupoTrabalho)
        {
            Dictionary<string, object> dictionary = new Dictionary<string, object>();
            dictionary.Add("codGrupoTrabalho", grupoTrabalho.Codigo);

            UnidadeTrabalho.ExecuteProcedure("sp_carrega_escala :codGrupoTrabalho", dictionary);
        }
    }
}
