﻿using Controlsys.Dominio.Parametros;
using Controlsys.Repositorios.Parametros;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Repositorios.Parametros
{
    /// <summary>
    /// Representa um(a) RepositorioChecklistDocumento.
    /// </summary>
    public class RepositorioChecklistDocumento : Repositorio<ChecklistDocumento>, IRepositorioChecklistDocumento
    {
        /// <summary>
        /// Construtor para
        /// Controlsys.Persistencia.Repositorios.Parametros.RepositorioChecklistDocumento.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioChecklistDocumento(IUnidadeTrabalho unidadeTrabalho) : base(unidadeTrabalho) { }

        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="checklistDoc">
        /// O(a) checklist document.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool Existe(ChecklistDocumento checklistDoc)
        {
            return ObterTodos().Any(s =>
                (s.Nome.ToUpper() == checklistDoc.Nome.ToUpper() && s.Ativo == true && s.Codigo != checklistDoc.Codigo) ||
                (s.TipoCadastro == checklistDoc.TipoCadastro && s.Ativo == true && s.Codigo != checklistDoc.Codigo));
        }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        public void Validar(ChecklistDocumento entidade, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    if (entidade.Nome == null)
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    if (Existe(entidade))
                        throw new CoreException("Já existe um checklist de documento com o mesmo Nome e/ou Tipo Cadastro informado.");
                    break;

                case EstadoObjeto.Alterado:
                    if (!entidade.Ativo)
                        throw new CoreException("O checklist de documento está inativo, não é possível alterar");
                    if (entidade.Nome == null)
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    if (Existe(entidade))
                        throw new CoreException("Já existe um checklist de documento com o mesmo Nome e/ou Tipo Cadastro informado.");
                    break;

                case EstadoObjeto.Ativado:
                    if (!entidade.Ativo && Existe(entidade))
                        throw new CoreException("Já existe um checklist de documento ativo com o mesmo Nome e/ou Tipo Cadastro.");
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// Salvar check list tipo document.
        /// </summary>
        ///
        /// <param name="checkListTipoDoc">
        /// O(a) check list tipo document.
        /// </param>
        public void SalvarCheckListTipoDoc(ChecklistTipoDocumento checkListTipoDoc)
        {
            Repositorio<ChecklistTipoDocumento> rep = new Repositorio<ChecklistTipoDocumento>(base.UnidadeTrabalho);
            rep.Salvar(checkListTipoDoc);
        }

        /// <summary>
        /// Obter todos por tipo cadastro.
        /// </summary>
        ///
        /// <param name="tipoCadastro">
        /// O(a) tipo cadastro.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ChecklistDocumento.
        /// </returns>
        public ChecklistDocumento ObterTodosPorTipoCadastro(TipoCadastro tipoCadastro)
        {
            Repositorio<ChecklistDocumento> rep = new Repositorio<ChecklistDocumento>(base.UnidadeTrabalho);
            return rep.ObterTodos().Where(p => p.TipoCadastro == tipoCadastro && p.Ativo).FirstOrDefault();
        }
    }
}
