﻿using Controlsys.Dominio.Parametros;
using Controlsys.Repositorios.Parametros;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Repositorios.Parametros
{
    /// <summary>
    /// Representa um(a) RepositorioTipoDocumento.
    /// </summary>
    public class RepositorioTipoDocumento : Repositorio<TipoDocumento>, IRepositorioTipoDocumento
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Parametros.RepositorioTipoDocumento.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioTipoDocumento(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {

        }

        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="tipoDoc">
        /// O(a) tipo document.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool Existe(TipoDocumento tipoDoc)
        {
            return ObterTodos().Any(t => t.Nome.ToUpper() == tipoDoc.Nome.ToUpper() && t.Ativo && t.Codigo != tipoDoc.Codigo);
        }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        public void Validar(TipoDocumento entidade, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    if (entidade.Nome == null || entidade.Sigla == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe um tipo de documento com o nome informado.");
                    }
                    break;
                case EstadoObjeto.Alterado:
                    if (!entidade.Ativo)
                    {
                        throw new CoreException("O tipo de documento está inativo, não é possível alterar");
                    }
                    if (entidade.Nome == null || entidade.Sigla == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe um tipo de documento com o nome informado.");
                    }
                    break;
                case EstadoObjeto.Ativado:
                    if (!entidade.Ativo && Existe(entidade))
                    {
                        throw new CoreException("Já existe um tipo de documento com o nome informado.");
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
