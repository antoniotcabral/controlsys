﻿using Controlsys.Dominio.Parametros;
using Controlsys.Repositorios.Parametros;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Repositorios.Parametros
{
    /// <summary>
    /// Representa um(a) RepositorioParametro.
    /// </summary>
    public class RepositorioParametro : Repositorio<Parametro>, IRepositorioParametro
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Parametros.RepositorioParametro.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioParametro(IUnidadeTrabalho unidadeTrabalho) : base(unidadeTrabalho) { }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        public void Validar(Parametro entidade)
        {
            if (entidade.Valor == null)            
                throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");            

            if (entidade.Nome == ParametroSistema.CatracasAtualizadas && entidade.Valor == "Atualizando")            
                throw new CoreException("Não é possível salvar. Catracas em atualização.");

            List<decimal> lista;
            switch (entidade.Nome) {
                #region [RUIM]
                case ParametroSistema.ClassificacaoJuridicaRuimMinimo:
                    lista = ObterTodos().Where(p => (p.Nome == ParametroSistema.ClassificacaoJuridicaRuimMaximo) && (p.Valor != null))
                        .Select(p => Convert.ToDecimal(p.Valor)).ToList();
                    if (lista.Any(v => v < Convert.ToDecimal(entidade.Valor)))
                        throw new CoreException(string.Format("Não é possível salvar um '{0}' com valor superior ao '{1}'.",
                            ParametroSistema.ClassificacaoJuridicaRuimMinimo.ObterDescricaoEnum(),
                            ParametroSistema.ClassificacaoJuridicaRuimMaximo.ObterDescricaoEnum()));
                    break;

                case ParametroSistema.ClassificacaoJuridicaRuimMaximo:
                    lista = ObterTodos().Where(p => (p.Nome == ParametroSistema.ClassificacaoJuridicaRuimMinimo) && (p.Valor != null))
                        .Select(p => Convert.ToDecimal(p.Valor)).ToList();
                    if (lista.Any(v => v > Convert.ToDecimal(entidade.Valor)))
                        throw new CoreException(string.Format("Não é possível salvar um '{0}' com valor inferior ao '{1}'.",
                            ParametroSistema.ClassificacaoJuridicaRuimMaximo.ObterDescricaoEnum(),
                            ParametroSistema.ClassificacaoJuridicaRuimMinimo.ObterDescricaoEnum()));
                    break;
                #endregion

                #region [MEDIO]
                case ParametroSistema.ClassificacaoJuridicaMedioMinimo:
                    lista = ObterTodos().Where(p => (p.Nome == ParametroSistema.ClassificacaoJuridicaMedioMaximo) && (p.Valor != null))
                        .Select(p => Convert.ToDecimal(p.Valor)).ToList();
                    if (lista.Any(v => v < Convert.ToDecimal(entidade.Valor)))
                        throw new CoreException(string.Format("Não é possível salvar um '{0}' com valor superior ao '{1}'.",
                            ParametroSistema.ClassificacaoJuridicaMedioMinimo.ObterDescricaoEnum(),
                            ParametroSistema.ClassificacaoJuridicaMedioMaximo.ObterDescricaoEnum()));
                    break;

                case ParametroSistema.ClassificacaoJuridicaMedioMaximo:
                    lista = ObterTodos().Where(p => (p.Nome == ParametroSistema.ClassificacaoJuridicaMedioMinimo) && (p.Valor != null))
                        .Select(p => Convert.ToDecimal(p.Valor)).ToList();
                    if (lista.Any(v => v > Convert.ToDecimal(entidade.Valor)))
                        throw new CoreException(string.Format("Não é possível salvar um '{0}' com valor inferior ao '{1}'.",
                            ParametroSistema.ClassificacaoJuridicaMedioMaximo.ObterDescricaoEnum(),
                            ParametroSistema.ClassificacaoJuridicaMedioMinimo.ObterDescricaoEnum()));
                    break;
                #endregion

                #region [MEDIO]
                case ParametroSistema.ClassificacaoJuridicaBomMinimo:
                    lista = ObterTodos().Where(p => (p.Nome == ParametroSistema.ClassificacaoJuridicaBomMaximo) && (p.Valor != null))
                        .Select(p => Convert.ToDecimal(p.Valor)).ToList();
                    if (lista.Any(v => v < Convert.ToDecimal(entidade.Valor)))
                        throw new CoreException(string.Format("Não é possível salvar um '{0}' com valor superior ao '{1}'.",
                            ParametroSistema.ClassificacaoJuridicaBomMinimo.ObterDescricaoEnum(),
                            ParametroSistema.ClassificacaoJuridicaBomMaximo.ObterDescricaoEnum()));
                    break;

                case ParametroSistema.ClassificacaoJuridicaBomMaximo:
                    lista = ObterTodos().Where(p => (p.Nome == ParametroSistema.ClassificacaoJuridicaBomMinimo) && (p.Valor != null))
                        .Select(p => Convert.ToDecimal(p.Valor)).ToList();
                    if (lista.Any(v => v > Convert.ToDecimal(entidade.Valor)))
                        throw new CoreException(string.Format("Não é possível salvar um '{0}' com valor inferior ao '{1}'.",
                            ParametroSistema.ClassificacaoJuridicaBomMaximo.ObterDescricaoEnum(),
                            ParametroSistema.ClassificacaoJuridicaBomMinimo.ObterDescricaoEnum()));
                    break;
                #endregion

                default:
                    break;
            }
        }

        /// <summary>
        /// Recuperar parametro.
        /// </summary>
        ///
        /// <param name="eNum">
        /// Number of.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Parametro.
        /// </returns>
        public Parametro RecuperarParametro(ParametroSistema eNum)
        {
            return ObterTodos().Where(p => p.Nome == eNum).FirstOrDefault();
        }

    }
}
