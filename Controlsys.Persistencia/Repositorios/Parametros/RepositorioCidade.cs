﻿using Controlsys.Dominio.Enderecos;
using Controlsys.Repositorio.Parametros;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Repositorios.Parametros
{
    /// <summary>
    /// Representa um(a) RepositorioCidade.
    /// </summary>
    public class RepositorioCidade : Repositorio<Cidade>, IRepositorioCidade
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Parametros.RepositorioCidade.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioCidade(IUnidadeTrabalho unidadeTrabalho) : base(unidadeTrabalho) { }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        public void Validar(Cidade entidade, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    if (entidade.Nome == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe uma cidade com o nome informado.");
                    }
                    break;
                case EstadoObjeto.Alterado:
                    if (!entidade.Ativo)
                    {
                        throw new CoreException("A cidade está inativa, não é possível alterar.");
                    }
                    if (entidade.Nome == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe uma cidade com o nome informado.");
                    }
                    break;
                case EstadoObjeto.Ativado:
                    if (!entidade.Ativo && Existe(entidade))
                    {
                        throw new CoreException("Já existe uma cidade com o nome informado.");
                    }
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        private bool Existe(Cidade entidade)
        {
            return ObterTodos().Any(a => a.Nome.ToUpper() == entidade.Nome.ToUpper() && a.Estado.Nome == entidade.Estado.Nome && a.Ativo && a.Codigo != entidade.Codigo);
        }
    }
}
