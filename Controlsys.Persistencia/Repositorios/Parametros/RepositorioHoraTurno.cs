﻿using Controlsys.Dominio.Parametros;
using Controlsys.Repositorios.Parametros;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Repositorios.Parametros
{
    /// <summary>
    /// Representa um(a) RepositorioHoraTurno.
    /// </summary>
    public class RepositorioHoraTurno : Repositorio<HoraTurno>, IRepositorioHoraTurno
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Parametros.RepositorioHoraTurno.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioHoraTurno(IUnidadeTrabalho unidadeTrabalho) : base(unidadeTrabalho) { }
    }
}
