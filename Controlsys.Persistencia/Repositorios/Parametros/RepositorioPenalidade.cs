﻿using Controlsys.Dominio.Parametros;
using Controlsys.Persistencia.Repositorios.Ocorrencias;
using Controlsys.Repositorio.Parametros;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Repositorios.Parametros
{
    public class RepositorioPenalidade: Repositorio<Penalidade>, IRepositorioPenalidade
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Parametros.Repositoriopenalidade.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioPenalidade(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
        }

        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="penalidade">
        /// O(a) penalidade.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool Existe(Penalidade penalidade)
        {
            return ObterTodos().Any(i => i.Sigla.ToUpper() == penalidade.Sigla.ToUpper() && i.Ativo && i.Codigo != penalidade.Codigo);
        }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        public void Validar(Penalidade entidade, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    if (entidade.Nome == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe uma penalidade com a sigla informada.");
                    }
                    break;
                case EstadoObjeto.Alterado:
                    if (!entidade.Ativo)
                    {
                        throw new CoreException("A penalidade está inativa, não é possível alterar");
                    }
                    if (string.IsNullOrEmpty(entidade.Nome) || string.IsNullOrEmpty(entidade.Sigla))
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe uma penalidade com a sigla informada.");
                    }
                    break;
                case EstadoObjeto.Ativado:
                    if (!entidade.Ativo && Existe(entidade))
                    {
                        throw new CoreException("Já existe uma penalidade com o nome informado.");
                    }
                    break;
                case EstadoObjeto.Inativado:
                case EstadoObjeto.Removido:
                    if (new RepositorioOcorrenciaTransito(UnidadeTrabalho).ObterTodos().Where(ot => ot.Ativo && ot.Penalidade.Codigo.Equals(entidade.Codigo)).Any())
                    {
                        throw new CoreException("Não é possível inativar, pois registro está sendo utilizado como parâmetro para outro.");
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
