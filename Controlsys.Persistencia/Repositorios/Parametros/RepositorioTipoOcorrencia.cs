﻿using Controlsys.Dominio.Parametros;
using Controlsys.Persistencia.Repositorios.Ocorrencias;
using Controlsys.Repositorio.Parametros;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Repositorios.Parametros
{
    /// <summary>
    /// Representa um RepositorioTipoOcorrencia
    /// </summary>
    public class RepositorioTipoOcorrencia : Repositorio<TipoOcorrencia>, IRepositorioTipoOcorrencia
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="unidadeTrabalho"></param>
        public RepositorioTipoOcorrencia(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
        }

        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="tipoOcorrencia">
        /// O(a) tipo da ocorrência.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool Existe(TipoOcorrencia tipoOcorrencia)
        {
            return ObterTodos().Any(tpO => tpO.Descricao.ToUpper() == tipoOcorrencia.Descricao.ToUpper() && tpO.Ativo && tpO.Codigo != tipoOcorrencia.Codigo);
        }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        public void Validar(TipoOcorrencia entidade, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    if (entidade.Descricao == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe um tipo de ocorrência com o nome informado.");
                    }
                    break;
                case EstadoObjeto.Alterado:
                    if (!entidade.Ativo)
                    {
                        throw new CoreException("O tipo de ocorrência está inativa, não é possível alterar");
                    }
                    if (entidade.Descricao == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe um tipo de ocorrência com o nome informado.");
                    }
                    break;
                case EstadoObjeto.Ativado:
                    if (!entidade.Ativo && Existe(entidade))
                    {
                        throw new CoreException("Já existe um tipo de ocorrência com o nome informado.");
                    }
                    break;
                case EstadoObjeto.Inativado:
                case EstadoObjeto.Removido:
                    if (new RepositorioOcorrenciaSeguranca(UnidadeTrabalho).ObterTodos().Where(os => os.Ativo && os.TipoOcorrencia.Codigo.Equals(entidade.Codigo)).Any())
                    {
                        throw new CoreException("Não é possível inativar, pois registro está sendo utilizado como parâmetro para outro.");
                    }
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nome"></param>
        /// <param name="somenteAtivos"></param>
        /// <returns></returns>
        public TipoOcorrencia ObterPorNome(string nome, bool somenteAtivos = false)
        {
            TipoOcorrencia tipo = ObterTodos().FirstOrDefault(u => u.Descricao == nome);

            if (!somenteAtivos || tipo == null)
                return tipo;

            if (!tipo.Ativo)
                return null;

            return tipo;
        }
    }
}
