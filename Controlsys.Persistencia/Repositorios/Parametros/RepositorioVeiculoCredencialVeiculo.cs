﻿using Controlsys.Dominio.CredenciamentoVeiculos;
using Controlsys.Repositorio.Parametros;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Repositorios.Parametros
{
    public class RepositorioVeiculoCredencialVeiculo : Repositorio<VeiculoCredencialVeiculo>, IRepositorioVeiculoCredencialVeiculo
    {

        public RepositorioVeiculoCredencialVeiculo(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {

        }


        private void ExisteVeiculo(VeiculoCredencialVeiculo entidade)
        {
            // Item removido deacordo com o chamado CH11552 item 86

            //List<Veiculo> listaVeiAtivos = ObterTodos().Where(cv =>
            //    (cv.CredencialVeiculo.Situacoes.OrderByDescending(scv => scv.Codigo).Select(sc => sc.Status).FirstOrDefault() != StatusCredencial.Devolvida) &&
            //    (cv.CredencialVeiculo.Codigo != entidade.CredencialVeiculo.Codigo)
            //).Select(v => v.Veiculo).ToList();

            //if (listaVeiAtivos.Any(v => v.Placa == entidade.Veiculo.Placa))
            //    throw new CoreException(string.Format("Não é possível salvar. A placa {0} possui credencial com devolução pendente.", entidade.Veiculo.Placa));
        }

        public void Validar(VeiculoCredencialVeiculo entidade, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                case EstadoObjeto.Alterado:
                    ExisteVeiculo(entidade);

                    if (entidade.Veiculo == null)
                        throw new CoreException("Não é possível salvar. Veículo nulo inválido.");

                    if (entidade.CredencialVeiculo == null)
                        throw new CoreException("Não é possível salvar. Credencial nula inválido.");
                    break;

                case EstadoObjeto.Ativado:
                    if (!Enumerable.Range(1, 2).Contains(entidade.CredencialVeiculo.Veiculos.Where(v => v.Ativo).Count()))
                        throw new CoreException("Não é possível ativar. É permitido manter ativo um ou até dois veículos por credencial.");

                    break;

                default:
                    break;
            }

        }

    }
}
