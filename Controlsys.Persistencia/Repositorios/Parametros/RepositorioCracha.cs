﻿using Controlsys.Dominio.Pessoas;
using Controlsys.Repositorio.Parametros;
using Globalsys;
using Globalsys.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Repositorios.Parametros
{
    /// <summary>
    /// Representa um(a) RepositorioCracha.
    /// </summary>
    public class RepositorioCracha : Repositorio<Cracha>, IRepositorioCracha
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Parametros.RepositorioCracha.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioCracha(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
        }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="cracha">
        /// O(a) cracha.
        /// </param>
        public void Validar(Cracha cracha)
        {
            if (ObterTodos().Any(p => p.Codigo != cracha.Codigo && p.Numero == cracha.Numero && p.Ativo))
                throw new CoreException("RFID já existe cadastrado e ativo.");

            if (!(cracha.Papel is Visitante) && ObterTodos().Any(p => 
                p.Papel.PessoaFisica.Codigo == cracha.Papel.PessoaFisica.Codigo && p.Ativo))
                throw new CoreException("Já existe um crachá ativo cadastrado para a pessoa selecionada.");            
        }

        /// <summary>
        /// Obter cracha.
        /// </summary>
        ///
        /// <param name="cdPapel">
        /// O(a) CD papel.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Cracha.
        /// </returns>
        public Cracha ObterCracha(int cdPapel)
        {
            return ObterTodos().Where(c => c.Papel.Codigo == cdPapel 
                                        && c.Ativo)
                               .OrderByDescending(c => c.DataRegistro)
                               .FirstOrDefault();            
        }
    }
}
