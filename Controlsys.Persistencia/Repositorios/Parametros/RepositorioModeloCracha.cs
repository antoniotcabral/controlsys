﻿using Controlsys.Dominio.Parametros;
using Controlsys.Repositorio.Parametros;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Repositorios.Parametros
{
    /// <summary>
    /// Representa um(a) RepositorioModeloCracha.
    /// </summary>
    public class RepositorioModeloCracha : Repositorio<ModeloCracha>, IRepositorioModeloCracha
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Parametros.RepositorioModeloCracha.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioModeloCracha(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
        }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        public void Validar(ModeloCracha entidade, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    if (entidade.Nome == null || entidade.Html == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe um modelo de crachá com o nome informado.");
                    }
                    break;
                case EstadoObjeto.Alterado:
                    if (!entidade.Ativo)
                    {
                        throw new CoreException("O modelo de crachá está inativado, não é possível alterar");
                    }
                    if (entidade.Nome == null || entidade.Html == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe um modelo de crachá com o nome informado.");
                    }
                    break;
                case EstadoObjeto.Ativado:
                    if (!entidade.Ativo && Existe(entidade))
                    {
                        throw new CoreException("Já existe um modelo de crachá com o nome informado.");
                    }
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool Existe(ModeloCracha entidade)
        {
            return ObterTodos().Any(m => (m.Nome.ToUpper() == entidade.Nome.ToUpper()) &&
                                         (m.Codigo != entidade.Codigo));
        }
    }
}
