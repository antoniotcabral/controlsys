﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Globalsys;
using Globalsys.Validacao;
using Globalsys.Exceptions;
using Controlsys.Dominio.Parametros;
using Controlsys.Repositorio.Parametros;
using Controlsys.Persistencia.Repositorios.Ocorrencias;

namespace Controlsys.Persistencia.Repositorios.Parametros
{
    /// <summary>
    /// Representa um(a) RepositorioInfracao.
    /// </summary>
    public class RepositorioInfracao : Repositorio<Infracao>, IRepositorioInfracao
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Parametros.RepositorioInfracao.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioInfracao(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
        }

        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="infracao">
        /// O(a) infracao.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool Existe(Infracao infracao)
        {
            return ObterTodos().Any(i => i.Sigla.ToUpper() == infracao.Sigla.ToUpper() && i.Ativo && i.Codigo != infracao.Codigo);
        }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        public void Validar(Infracao entidade, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    if (entidade.Nome == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe uma infração ativa com a mesma sigla informada.");
                    }
                    break;
                case EstadoObjeto.Alterado:
                    if (!entidade.Ativo)
                    {
                        throw new CoreException("A infração está inativa, não é possível alterar");
                    }
                    if (string.IsNullOrEmpty(entidade.Nome) || entidade.Grau == null || string.IsNullOrEmpty(entidade.Sigla))
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe uma infração ativa com a mesma sigla informada.");
                    }
                    break;
                case EstadoObjeto.Ativado:
                    if (!entidade.Ativo && Existe(entidade))
                    {
                        throw new CoreException("Já existe uma infraçãof com o nome informado.");
                    }
                    break;
                case EstadoObjeto.Inativado:
                case EstadoObjeto.Removido:
                    if (new RepositorioOcorrenciaTransito(UnidadeTrabalho).ObterTodos().Where(ot => ot.Ativo && ot.Infracao.Codigo.Equals(entidade.Codigo)).Any())
                    {
                        throw new CoreException("Não é possível inativar, pois registro está sendo utilizado como parâmetro para outro.");
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
