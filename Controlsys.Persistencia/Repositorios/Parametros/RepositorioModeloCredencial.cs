﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Parametros;
using Controlsys.Repositorio.Parametros;
using Globalsys;
using Globalsys.Validacao;
using Globalsys.Exceptions;

namespace Controlsys.Persistencia.Repositorios.Parametros
{
    public class RepositorioModeloCredencial : Repositorio<ModeloCredencial>, IRepositorioModeloCredencial
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Parametros.RepositorioModeloCredencial.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioModeloCredencial(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
        }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        public void Validar(ModeloCredencial entidade, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    if (entidade.Nome == null || entidade.HtmlFrente == null)
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    if (Existe(entidade))
                        throw new CoreException("Já existe um registro com o nome informado.");
                    break;

                case EstadoObjeto.Alterado:
                    if (!entidade.Ativo)
                        throw new CoreException("O registro está inativado, não é possível alterar.");
                    if (entidade.Nome == null || entidade.HtmlFrente == null)
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    if (Existe(entidade))
                        throw new CoreException("Já existe um registro com o nome informado.");
                    break;

                case EstadoObjeto.Ativado:
                    if (!entidade.Ativo && Existe(entidade))
                        throw new CoreException("Já existe um registro com o nome informado.");
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool Existe(ModeloCredencial entidade)
        {
            return ObterTodos().Any(m => (m.Codigo != entidade.Codigo) && (m.Nome.ToUpper() == entidade.Nome.ToUpper()));
        }
    }
}
