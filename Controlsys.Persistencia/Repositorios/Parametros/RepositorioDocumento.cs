﻿using Controlsys.Dominio.AgendamentosPessoa;
using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Controlsys.Repositorios.Parametros;
using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Repositorios.Parametros
{
    /// <summary>
    /// Representa um(a) RepositorioDocumento.
    /// </summary>
    public class RepositorioDocumento : Repositorio<Documento>, IRepositorioDocumento
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Parametros.RepositorioDocumento.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioDocumento(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {

        }

        //public DocumentoPessoa SalvarDocPessoa(Pessoa pessoa, Documento item)
        //{
        //    Repositorio<DocumentoPessoa> rep = new Repositorio<DocumentoPessoa>(base.UnidadeTrabalho);
        //    Documento documento = new Documento();
        //    documento.DataRegistro = DateTime.Now;

        //    if (string.IsNullOrEmpty(item.Descricao))
        //        documento.Status = StatusDocumento.NaoEntregue;
        //    else
        //        documento.Status = item.Status;

        //    documento.NomeDocumento = item.TipoDocumento.Sigla;
        //    documento.Descricao = item.Descricao;
        //    documento.DataValidade = item.DataValidade;
        //    //if (item.DataEmissao.HasValue)
        //    //{
        //    //    documento.DataEmissao = item.DataEmissao;
        //    //    //documento.DataValidade = item.DataEmissao.Value.AddDays(item.TipoDocumento.DiasValidade);
        //    //}
        //    documento.Ativar();
        //    documento.TipoDocumento = item.TipoDocumento;
        //    Salvar(documento);

        //    DocumentoPessoa documentoPessoa = new DocumentoPessoa();
        //    documentoPessoa.Documento = documento;
        //    documentoPessoa.Pessoa = pessoa;
        //    documentoPessoa.DataRegistro = DateTime.Now;    
        //    documentoPessoa.Ativar();
        //    rep.Salvar(documentoPessoa);

        //    return documentoPessoa;
        //}

        /// <summary>
        /// Salvar document papel.
        /// </summary>
        ///
        /// <param name="papel">
        /// O(a) papel.
        /// </param>
        /// <param name="item">
        /// O(a) item.
        /// </param>
        ///
        /// <returns>
        /// Um(a) DocumentoPapel.
        /// </returns>
        public DocumentoPapel SalvarDocPapel(Papel papel, Documento item, PapelAgendamento papelAgendamento = null)
        {
            Repositorio<DocumentoPapel> rep = new Repositorio<DocumentoPapel>(base.UnidadeTrabalho);
            Documento documento = item.Codigo > 0 ? ObterPorId(item.Codigo) : new Documento();

            if (item.Status == 0 || item.Status == StatusDocumento.PreCadastro)
                documento.Status = StatusDocumento.NaoEntregue;
            else
                documento.Status = item.Status;
            DocumentoPapel documentoPapel = null;
            if (documento.Codigo > 0)
            {
                Salvar(documento);

                documentoPapel = rep.ObterTodos().Where(dp => dp.Documento.Codigo == documento.Codigo && dp.PapelAgendamento != null).FirstOrDefault();
                if (documentoPapel != null)
                {
                    documentoPapel.Papel = papel;
                    rep.Salvar(documentoPapel);
                }
            }
            else
            {
                documento.DataRegistro = DateTime.Now;
                documento.NomeDocumento = item.TipoDocumento.Sigla;
                documento.Descricao = item.Descricao;
                documento.DataValidade = item.DataValidade;
                documento.Ativar();
                documento.TipoDocumento = item.TipoDocumento;

                Salvar(documento);

                documentoPapel = new DocumentoPapel();
                documentoPapel.Documento = documento;
                documentoPapel.Papel = papel;
                documentoPapel.PapelAgendamento = papelAgendamento;
                documentoPapel.DataRegistro = DateTime.Now;
                documentoPapel.Ativar();
                rep.Salvar(documentoPapel);
            }

            return documentoPapel;
        }

        /// <summary>
        /// Salvar document empresa.
        /// </summary>
        ///
        /// <param name="empresa">
        /// O(a) empresa.
        /// </param>
        /// <param name="item">
        /// O(a) item.
        /// </param>
        ///
        /// <returns>
        /// Um(a) DocumentoEmpresa.
        /// </returns>
        public DocumentoEmpresa SalvarDocEmpresa(Empresa empresa, Documento item)
        {
            Repositorio<DocumentoEmpresa> rep = new Repositorio<DocumentoEmpresa>(base.UnidadeTrabalho);
            Documento documento = new Documento();
            documento.DataRegistro = DateTime.Now;

            if (item.Status == 0)
                documento.Status = StatusDocumento.NaoEntregue;
            else
                documento.Status = item.Status;

            documento.NomeDocumento = item.TipoDocumento.Sigla;
            documento.Descricao = item.Descricao;
            documento.DataValidade = item.DataValidade;
            //if (item.DataEmissao.HasValue)
            //{
            //    documento.DataEmissao = item.DataEmissao;
            //    //documento.DataValidade = item.DataEmissao.Value.AddDays(item.TipoDocumento.DiasValidade);
            //}
            documento.Ativar();
            documento.TipoDocumento = item.TipoDocumento;
            Salvar(documento);

            DocumentoEmpresa documentoEmpresa = new DocumentoEmpresa();
            documentoEmpresa.Documento = documento;
            documentoEmpresa.Empresa = empresa;
            documentoEmpresa.DataRegistro = DateTime.Now;
            documentoEmpresa.Ativar();
            rep.Salvar(documentoEmpresa);

            return documentoEmpresa;
        }
    }
}
