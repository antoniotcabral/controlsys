﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Parametros;
using Controlsys.Repositorio.Parametros;
using Globalsys;
using Globalsys.Validacao;
using Globalsys.Exceptions;
using Controlsys.Dominio.Acesso;

namespace Controlsys.Persistencia.Repositorios.Parametros
{
    public class RepositorioRestaurante : Repositorio<Restaurante>, IRepositorioRestaurante
    {
        public RepositorioRestaurante(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
        }

        public bool Existe(Restaurante restaurante)
        {
            return ObterTodos().Any(s =>
                (s.Nome.ToUpper() == restaurante.Nome.ToUpper() && s.Ativo == true && s.Codigo != restaurante.Codigo));
        }

        public void Validar(Restaurante entidade, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    if (entidade.Nome == null)
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    if (Existe(entidade))
                        throw new CoreException("Já existe um restaurante com o nome informado.");
                    if (TemConflitoDeGrupoLeitora(entidade))
                        throw new CoreException("Já existe um restaurante ativo para o Grupo Leitora informado.");
                    break;

                case EstadoObjeto.Alterado:
                    if (!entidade.Ativo)
                        throw new CoreException("O restaurante está inativo, não é possível alterar.");
                    if (entidade.Nome == null)
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    if (Existe(entidade))
                        throw new CoreException("Já existe um restaurante com o nome informado.");
                    if (TemConflitoDeGrupoLeitora(entidade))
                        throw new CoreException("Já existe um restaurante ativo para o Grupo Leitora informado.");
                    break;

                case EstadoObjeto.Ativado:
                    if (!entidade.Ativo && Existe(entidade))
                        throw new CoreException("Já existe um restaurante com o nome informado.");
                    if (TemConflitoDeGrupoLeitora(entidade))
                        throw new CoreException("Já existe um restaurante ativo para o Grupo Leitora" + entidade.GrupoLeitora.Nome + ".");
                    break;

                case EstadoObjeto.Inativado:
                    if (entidade.Ativo && verificaDependencia(entidade))
                        throw new CoreException("Não é possível inativar o restaurante " + entidade.Nome + ", pois ele está vinculado a um acesso de restaurante.");
                    break;

                default:
                    break;
            }
        }

        public void SalvarTipoAlimentacaoRestaurante(TipoAlimentacaoRestaurante tipoAlimentacaoRestaurante)
        {
            Repositorio<TipoAlimentacaoRestaurante> rep = new Repositorio<TipoAlimentacaoRestaurante>(base.UnidadeTrabalho);
            rep.Salvar(tipoAlimentacaoRestaurante);
        }

        public TipoAlimentacaoRestaurante TAlimentacaoRestaurante(TipoAlimentacaoRestaurante tAlimRestaurante)
        {
            Repositorio<TipoAlimentacaoRestaurante> rep = new Repositorio<TipoAlimentacaoRestaurante>(base.UnidadeTrabalho);
            var tpAlimentacaoRest = rep.ObterTodos().Where(r => r.Ativo == true && r.TipoAlimentacao.Codigo == tAlimRestaurante.TipoAlimentacao.Codigo && r.Restaurante.Codigo == tAlimRestaurante.Restaurante.Codigo);
            return (tpAlimentacaoRest.OrderByDescending(tpr => tpr.Codigo).FirstOrDefault());
        }

        private bool verificaDependencia(Restaurante restaurante)
        {
            Repositorio<AcessoRestauranteTipoAlimentacao> rep = new Repositorio<AcessoRestauranteTipoAlimentacao>(base.UnidadeTrabalho);
            return rep.ObterTodos().Any(t => t.TipoAlimentacao.Restaurante.Codigo == restaurante.Codigo && t.AcessoRestaurante.Ativo && t.Ativo);
        }
        
        private bool TemConflitoDeGrupoLeitora(Restaurante restaurante)
        {
            var restaurantes = ObterTodos().Where(r => r.Ativo);
            if (restaurantes.Any(r => r.Codigo != restaurante.Codigo && r.GrupoLeitora.Codigo == restaurante.GrupoLeitora.Codigo))
                return true;

            return false;
        }
    }
}
