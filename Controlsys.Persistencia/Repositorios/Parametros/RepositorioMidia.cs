﻿using Controlsys.Dominio.CredenciamentoVeiculos;
using Controlsys.Dominio.Parametros;
using Controlsys.Repositorio.Parametros;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate.Linq;

namespace Controlsys.Persistencia.Repositorios.Parametros
{
    /// <summary>
    /// Representa um(a) RepositorioMidia.
    /// </summary>
    public class RepositorioMidia : Repositorio<Midia>, IRepositorioMidia
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Parametros.RepositorioMidia.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioMidia(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
        }

        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="midia">
        /// O(a) midia.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool Existe(Midia midia)
        {
            return ObterTodos().Any(a => a.NomeArquivo.ToUpper() == midia.NomeArquivo.ToUpper() && a.Codigo != midia.Codigo);
        }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        public void Validar(Midia entidade, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    if (entidade.DataRegistro == DateTime.MinValue || string.IsNullOrEmpty(entidade.Extensao) || string.IsNullOrEmpty(entidade.MimeType) || string.IsNullOrEmpty(entidade.NomeArquivo) || string.IsNullOrEmpty(entidade.Url))
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    //if (Existe(entidade))
                    //{
                    //    throw new CoreException("Já existe uma Midia com o nome informado.");
                    //}
                    break;
                case EstadoObjeto.Alterado:
                    if (entidade.DataRegistro == DateTime.MinValue || string.IsNullOrEmpty(entidade.Extensao) || string.IsNullOrEmpty(entidade.MimeType) || string.IsNullOrEmpty(entidade.NomeArquivo) || string.IsNullOrEmpty(entidade.Url))
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    //if (Existe(entidade))
                    //{
                    //    throw new CoreException("Já existe uma Midia com o nome informado.");
                    //}
                    break;
                default:
                    break;
            }
        }
    }
}
