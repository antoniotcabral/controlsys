﻿using Controlsys.Dominio.SolicitacoesMaterial;
using Controlsys.Repositorio.SolicitacoesMaterial;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;

namespace Controlsys.Persistencia.Repositorios.SolicitacoesMaterial
{
    public class RepositorioItemSolicitacaoMaterial : Repositorio<ItemSolicitacaoMaterial>, IRepositorioItemSolicitacaoMaterial
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Seguranca.RepositorioItemSolicitacaoMaterial.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioItemSolicitacaoMaterial(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
        }

        public void Validar(ItemSolicitacaoMaterial itemSolicitacaoMaterial, Globalsys.Validacao.EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    if (string.IsNullOrEmpty(itemSolicitacaoMaterial.Descricao))
                    {
                        throw new CoreException("Descrição do material obrigatória.");
                    }

                    if (itemSolicitacaoMaterial.Quantidade <= 0)
                    {
                        throw new CoreException(string.Format("O material {0} deve ter quantidade mínima de 1.", itemSolicitacaoMaterial.Descricao));
                    }
                    break;
                case EstadoObjeto.Alterado:
                    break;
                case EstadoObjeto.Removido:
                    break;
                case EstadoObjeto.Ativado:
                    break;
                case EstadoObjeto.Inativado:
                    break;
                default:
                    break;
            }
        }
    }
}
