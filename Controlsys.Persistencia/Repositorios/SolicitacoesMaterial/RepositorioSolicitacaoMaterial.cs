﻿using Controlsys.Dominio.SolicitacoesMaterial;
using Controlsys.Repositorio.SolicitacoesMaterial;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;

namespace Controlsys.Persistencia.Repositorios.SolicitacoesMaterial
{
    /// <summary>
    /// Representa um(a) RepositorioSolicitacaoMaterial.
    /// </summary>
    public class RepositorioSolicitacaoMaterial : Repositorio<SolicitacaoMaterial>, IRepositorioSolicitacaoMaterial
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Seguranca.RepositorioSolicitacaoMaterial.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioSolicitacaoMaterial(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
        }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="solicitacaoMaterial">
        /// O(a) solicitacao Material.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        public void Validar(SolicitacaoMaterial solicitacaoMaterial, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    if (solicitacaoMaterial.Contratada == null)
                    {
                        throw new CoreException("É obrigatória a seleção da empresa contratada.");
                    }

                    if (solicitacaoMaterial.SetorCusto == null)
                    {
                        throw new CoreException("É obrigatória a seleção do setor de custo.");
                    }

                    if (solicitacaoMaterial.ColaboradorResponsavel == null && solicitacaoMaterial.GestorResponsavel == null)
                    {
                        throw new CoreException("É obrigatória a seleção de um responsável.");
                    }

                    if (solicitacaoMaterial.TipoSolicitacao == TipoSolicitacao.Entrada && solicitacaoMaterial.Modalidade.HasValue)
                    {
                        throw new CoreException("Solicitação de Entrada não deve ter uma modalidade selecionada.");
                    }

                    if (solicitacaoMaterial.TipoSolicitacao == TipoSolicitacao.Saida && !solicitacaoMaterial.Modalidade.HasValue)
                    {
                        throw new CoreException("Solicitação de Saída deve ter uma modalidade selecionada.");
                    }

                    if (solicitacaoMaterial.DataEntradaSaida == DateTime.MinValue)
                    {
                        throw new CoreException("Data de Entrada/Saída obrigatória.");
                    }

                    if (solicitacaoMaterial.TipoSolicitacao == TipoSolicitacao.Entrada && solicitacaoMaterial.DataPrevisaoRetorno.HasValue)
                    {
                        throw new CoreException("Data Prevista de Retorno deve ser informada apenas em Solicitações de Saída.");
                    }

                    break;
                case EstadoObjeto.Alterado:
                    break;
                case EstadoObjeto.Removido:
                    break;
                case EstadoObjeto.Ativado:
                    break;
                case EstadoObjeto.Inativado:
                    break;
                default:
                    break;
            }
        }
    }
}
