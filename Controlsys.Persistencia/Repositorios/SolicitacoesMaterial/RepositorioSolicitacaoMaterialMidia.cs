﻿using Controlsys.Dominio.SolicitacoesMaterial;
using Controlsys.Repositorio.SolicitacoesMaterial;
using Globalsys;
using Globalsys.Validacao;

namespace Controlsys.Persistencia.Repositorios.SolicitacoesMaterial
{
    public class RepositorioSolicitacaoMaterialMidia : Repositorio<SolicitacaoMaterialMidia>, IRepositorioSolicitacaoMaterialMidia
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Seguranca.RepositorioStatusSolicitacaoMaterial.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioSolicitacaoMaterialMidia(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
        }

        public void Validar(SolicitacaoMaterialMidia solicitacaoMaterialMidia, Globalsys.Validacao.EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    break;
                case EstadoObjeto.Alterado:
                    break;
                case EstadoObjeto.Removido:
                    break;
                case EstadoObjeto.Ativado:
                    break;
                case EstadoObjeto.Inativado:
                    break;
                default:
                    break;
            }
        }
    }
}
