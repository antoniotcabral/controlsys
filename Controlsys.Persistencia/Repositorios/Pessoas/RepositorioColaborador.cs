﻿using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Pessoas;
using Controlsys.Repositorios.Pessoas;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Persistencia.Repositorios.Empresas;
using Controlsys.Persistencia.Repositorios.Parametros;
using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Parametros;
using NHibernate;
using NHibernate.Criterion;

namespace Controlsys.Persistencia.Repositorios.Pessoas
{
    /// <summary>
    /// Representa um(a) RepositorioColaborador.
    /// </summary>
    public class RepositorioColaborador : Repositorio<Colaborador>, IRepositorioColaborador
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Pessoas.RepositorioColaborador.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioColaborador(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
        }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        /// <param name="colaborador">
        /// O(a) colaborador.
        /// </param>
        public void Validar(EstadoObjeto estadoObjeto, Colaborador colaborador)
        {
            Repositorio<Papel> repPapel = new Repositorio<Papel>(UnidadeTrabalho);

            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                        RepositorioPessoaCritica repPessoaCritica = new RepositorioPessoaCritica(UnidadeTrabalho);
                        RepositorioParametro repParam = new RepositorioParametro(UnidadeTrabalho);
                       
                        
                        if (repPessoaCritica.Existe(colaborador.PessoaFisica.CPF, colaborador.PessoaFisica.Passaporte))
                        {
                            string msgCadastroPessoa = repParam.RecuperarParametro(ParametroSistema.MensagemCadastroPessoaFisica).Valor;
                            throw new CoreException(msgCadastroPessoa);
                        }
                        if (!string.IsNullOrEmpty(colaborador.PessoaFisica.CPF))
                        {
                            //if (!colaborador.PessoaFisica.TituloEleitor.HasValue || !colaborador.PessoaFisica.TituloEleitorZona.HasValue ||
                            //        !colaborador.PessoaFisica.TituloEleitorSecao.HasValue || colaborador.PessoaFisica.TituloEleitorCidade == null)
                            //    throw new CoreException("Não é possível salvar. Preencha corretamente os campos de Título de Eleitor.");

                            //if (!colaborador.PessoaFisica.CTPS.HasValue || string.IsNullOrEmpty(colaborador.PessoaFisica.CTPSSerie) ||
                            //        colaborador.PessoaFisica.CTPSEstado == null || !colaborador.PessoaFisica.CTPSData.HasValue)
                            //    throw new CoreException("Não é possível salvar. Preencha corretamente os campos de Carteira de Trabalho e Previdência Social (CTPS).");

                            //if (colaborador.PessoaFisica.Sexo == Sexo.Masculino
                            //        && (string.IsNullOrEmpty(colaborador.PessoaFisica.CertificadoReservista)
                            //            || string.IsNullOrEmpty(colaborador.PessoaFisica.CertificadoReservistaCat)))
                            //    throw new CoreException("Não é possível salvar. Preencha corretamente os campos de Certificado de Reservista.");
                        }
                        //if (!colaborador.ASOs.Any())
                        //    throw new CoreException("Não é possível salvar. Preencha corretamente os campos de ASOs.");

                        List<Papel> listaPapeisPF = repPapel.ObterTodos().Where(p => p.PessoaFisica.Codigo == colaborador.PessoaFisica.Codigo).ToList();
                        Papel papel = listaPapeisPF.Where(p => p.PapelLogs.Where(pl => pl.DataInicio.Date <= DateTime.Now.Date).OrderByDescending(pl => pl.Codigo).FirstOrDefault().Status == StatusPapelLog.Ativo).FirstOrDefault();
                        if (papel != null)
                        {
                            string tipoPapel = papel is Colaborador ? "Colaborador" : papel is PrestadorServico ? "Prestador de Serviço" : "Visitante";
                            throw new CoreException(string.Format("Pessoa já possui papel ativo de {0} no sistema.", tipoPapel));
                        }

                        /* Validação para garantir o Id Usuario unico para papel ativo. */
                        List<Papel> listaPapeisIdUsuario = repPapel.ObterTodos().Where(p => p.PessoaFisica.Codigo != colaborador.PessoaFisica.Codigo && p.IdUsuario != null && p.IdUsuario == colaborador.IdUsuario).ToList();
                        Papel papelIdUsuario = listaPapeisIdUsuario.Where(p => p.PapelLogs.OrderByDescending(pl => pl.Codigo).FirstOrDefault().Status == StatusPapelLog.Ativo).FirstOrDefault();
                        if (papelIdUsuario != null)
                        {                            
                            throw new CoreException("O Id Usuário já esta sendo utilizado pelo papel ativo: " + papelIdUsuario.PessoaFisica.Nome);
                        }

                        /* Validação para garantir o Código do Empregado SAP unico para a pessoa. */
                        List<Papel> listaPapeisEmpregadoSAP = repPapel.ObterTodos().Where(p => p.PessoaFisica.Codigo != colaborador.PessoaFisica.Codigo && p.EmpregadoSAP != null && p.EmpregadoSAP == colaborador.EmpregadoSAP).ToList();
                        Papel papelEmpregadoSAP = listaPapeisEmpregadoSAP.FirstOrDefault();
                        if (papelEmpregadoSAP != null)
                        {
                            throw new CoreException("O Código do Empregado SAP já esta sendo utilizado pela pessoa: " + papelEmpregadoSAP.PessoaFisica.Nome + " cpf:" + papelEmpregadoSAP.PessoaFisica.CPF);
                        }

                    break;
                case EstadoObjeto.Alterado:
                    RepositorioAlocacaoColaborador repAlocCol = new RepositorioAlocacaoColaborador(UnidadeTrabalho);

                    IQueryable<AlocacaoColaborador> alocCol = repAlocCol.ObterTodos().Where(ac => ac.Ativo && (ac.Papel.Codigo == colaborador.Codigo) && (ac.PedidoCompra.Empresa.Codigo != colaborador.Empresa.Codigo));

                    if (alocCol.Any())
                    {
                        throw new CoreException("Não é possível salvar. Empresa selecionada parao Colaborador é diferente da empresa em que o mesmo está alocado.");
                    }


                    if (!string.IsNullOrEmpty(colaborador.PessoaFisica.CPF))
                    {
                        //if (!colaborador.PessoaFisica.TituloEleitor.HasValue || !colaborador.PessoaFisica.TituloEleitorZona.HasValue ||
                        //        !colaborador.PessoaFisica.TituloEleitorSecao.HasValue || colaborador.PessoaFisica.TituloEleitorCidade == null)
                        //    throw new CoreException("Não é possível salvar. Preencha corretamente os campos de Título de Eleitor.");

                        //if (!colaborador.PessoaFisica.CTPS.HasValue || string.IsNullOrEmpty(colaborador.PessoaFisica.CTPSSerie) ||
                        //        colaborador.PessoaFisica.CTPSEstado == null || !colaborador.PessoaFisica.CTPSData.HasValue)
                        //    throw new CoreException("Não é possível salvar. Preencha corretamente os campos de Carteira de Trabalho e Previdência Social (CTPS).");

                        //if (colaborador.PessoaFisica.Sexo == Sexo.Masculino
                        //        && (string.IsNullOrEmpty(colaborador.PessoaFisica.CertificadoReservista)
                        //            || string.IsNullOrEmpty(colaborador.PessoaFisica.CertificadoReservistaCat)))
                        //    throw new CoreException("Não é possível salvar. Preencha corretamente os campos de Certificado de Reservista.");
                    }
                    //if (colaborador.ASOs.Count() == 0)
                    //    throw new CoreException("Não é possível salvar. Preencha corretamente os campos de ASOs.");

                    /* Validação para garantir o Id Usuario unico para papel ativo. */
                    List<Papel> listaPapeisIdUsuarioAlt = repPapel.ObterTodos().Where(p => p.PessoaFisica.Codigo != colaborador.PessoaFisica.Codigo && p.IdUsuario != null && p.IdUsuario == colaborador.IdUsuario).ToList();
                    Papel papelIdUsuarioAlt = listaPapeisIdUsuarioAlt.Where(p => p.PapelLogs.OrderByDescending(pl => pl.Codigo).FirstOrDefault().Status == StatusPapelLog.Ativo).FirstOrDefault();
                    if (papelIdUsuarioAlt != null)
                    {
                        throw new CoreException("O Id Usuário já esta sendo utilizado pelo papel ativo: " + papelIdUsuarioAlt.PessoaFisica.Nome);
                    }

                    /* Validação para garantir o Código do Empregado SAP unico para a pessoa. */
                    List<Papel> listaPapeisEmpregadoSAPAlt = repPapel.ObterTodos().Where(p => p.PessoaFisica.Codigo != colaborador.PessoaFisica.Codigo && p.EmpregadoSAP != null && p.EmpregadoSAP == colaborador.EmpregadoSAP).ToList();
                    Papel papelEmpregadoSAPAlt = listaPapeisEmpregadoSAPAlt.FirstOrDefault();                    
                    if (papelEmpregadoSAPAlt != null)
                    {
                        throw new CoreException("O Código do Empregado SAP já esta sendo utilizado pela pessoa: " + papelEmpregadoSAPAlt.PessoaFisica.Nome + " cpf:" + papelEmpregadoSAPAlt.PessoaFisica.CPF);
                    }

                    break;
                case EstadoObjeto.Removido:
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="colaborador">
        /// O(a) colaborador.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        private bool Existe(Colaborador colaborador)
        {
            return ObterTodos().Any(u => u.Codigo != colaborador.Codigo);
        }

        public bool Existe(string cpf)
        {
            cpf = string.IsNullOrEmpty(cpf) ? string.Empty : cpf.Replace(".", "").Replace("-", "");
            return ObterTodos().Any(pf => !string.IsNullOrEmpty(cpf) && pf.PessoaFisica.CPF == cpf);
        }
        /// <summary>
        /// Obter lista colaborador.
        /// </summary>
        ///
        /// <param name="obj_colab">
        /// O(a) object colab.
        /// </param>
        /// <param name="consideraAlocados">
        /// true to considera alocados.
        /// </param>
        ///
        /// <returns>
        /// Um(a) IQueryable&lt;Colaborador&gt;
        /// </returns>
        public IQueryable<Colaborador> ObterListaColaborador(Colaborador obj_colab, bool consideraAlocados = false)
        {
            IQueryable<Colaborador> colaborador = ObterTodos();
            RepositorioPedidoCompra repPedido = new RepositorioPedidoCompra(UnidadeTrabalho);

            colaborador = colaborador.Where(t => t.PapelLogs.OrderByDescending(pl => pl.Codigo)
                                                                         .Select(pl => pl.Status)
                                                                         .FirstOrDefault() == StatusPapelLog.Ativo);

            if (obj_colab.PessoaFisica != null && !string.IsNullOrEmpty(obj_colab.PessoaFisica.CPF))
            {
                colaborador = colaborador.Where(t => t.PessoaFisica.CPF == obj_colab.PessoaFisica.CPF);
            }

            if (obj_colab.PessoaFisica != null && !string.IsNullOrEmpty(obj_colab.PessoaFisica.Nome))
            {
                colaborador = colaborador.Where(t => t.PessoaFisica.Nome.ToUpper().Contains(obj_colab.PessoaFisica.Nome.ToUpper()));
            }

            if (obj_colab.Empresa.Codigo != null)
            {
                colaborador = colaborador.Where(t => t.Empresa.Codigo == obj_colab.Empresa.Codigo);
            }

            if (consideraAlocados)
            {
                colaborador = colaborador.Where(c => !repPedido.ObterTodos().Any(p => p.ColaboradoresAlocados.Any(ca => ca.Papel.Codigo == c.Codigo && ca.Ativo)));
            }

            //if (colaborador.Count() == 0)
            //{
            //    throw new CoreException("Não foram encontrados colaboradores ativos com essa descrição.");
            //}

            return colaborador;

        }

        public override void Salvar(Colaborador entidade)
        {
            if (entidade.Codigo <= 0)
                entidade.DataRegistro = DateTime.Now;
            base.Salvar(entidade);
        }

        public IEnumerable<Colaborador> FiltrarColaborador(StatusPapelLog? status = null, string cpf = null, string empresa = null, string nome = null, int cargo = 0, string id = null, string passaporte = null, MaoDeObra? maodeobra = null, string area = null, long? empregadoSap = null, string crachaativo = null)        
        {
            ICriteria criteria = UnidadeTrabalho.Sessao.CreateCriteria<Colaborador>();
            criteria.CreateAlias("PessoaFisica", "pf");            
            criteria.CreateAlias("Empresa", "e");
            criteria.CreateAlias("Cargo", "c");
            if (empregadoSap > 0)
                criteria.CreateAlias("EmpregadoSAP", "es");

                if (!string.IsNullOrEmpty(id))
                criteria.Add(Restrictions.Eq("Codigo", id));

            if (status.HasValue)
            {
                var sql = @"
                    {alias}.CD_PAPEL In (select tab.CD_PAPEL Codigo from (
                        select *, ROW_NUMBER() OVER(PARTITION BY CD_PAPEL ORDER BY CD_PAPEL_LOG DESC) Linha from PAPEL_LOG 
                        where 
	                        1=1 
	                        and DT_INICIO <= getdate() 
	                        and (DT_FIM is null or dt_fim >= CONVERT(DATETIME, CONVERT(DATE, getdate()))) 
                        ) as tab 
                        where tab.Linha = 1 AND tab.TX_STATUS = '" + status.Value + "')";

                DetachedCriteria students = DetachedCriteria.For<Papel>()
                    .SetProjection(Projections.Property("Codigo"))
                    .Add(Expression.Sql(sql));

                criteria.Add(Subqueries.PropertyIn("Codigo", students));
            }

            if (!string.IsNullOrEmpty(nome))
                criteria.Add(Restrictions.Like("pf.Nome", nome, MatchMode.Anywhere));

            if (!string.IsNullOrEmpty(cpf))
            {
                cpf = cpf.Replace(".", "").Replace("-", "");
                criteria.Add(Restrictions.Eq("pf.CPF", cpf));
            }

            if (!string.IsNullOrEmpty(passaporte))
                criteria.Add(Restrictions.Eq("pf.Passaporte", passaporte));

            if (!string.IsNullOrEmpty(empresa))
                criteria.Add(Restrictions.Eq("e.Codigo", empresa));

            if (cargo > 0)
                criteria.Add(Restrictions.Eq("c.Codigo", cargo));

            if (!string.IsNullOrEmpty(maodeobra.ToString()))
                criteria.Add(Restrictions.Eq("MaoDeObra", maodeobra));

            if (!string.IsNullOrEmpty(area))
                criteria.Add(Restrictions.Eq("Area.Codigo", Convert.ToInt32(area)));            

            if (empregadoSap > 0)
                criteria.Add(Restrictions.Eq("es.Codigo", empregadoSap));

            if (!string.IsNullOrEmpty(crachaativo))
            {
                RepositorioCracha repCracha = new RepositorioCracha(UnidadeTrabalho);

                var cracha = repCracha.ObterTodos().Where(x => x.Numero == crachaativo && x.Ativo == true).FirstOrDefault();

                if (cracha != null)               
                    criteria.Add(Restrictions.Eq("Codigo", cracha.Papel.Codigo));
                else
                    criteria.Add(Restrictions.Eq("Codigo", 0));                
            }

            criteria.AddOrder(Order.Asc("pf.Nome"));

            return criteria.Future<Colaborador>();
        }
    }
}
