﻿using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Controlsys.Persistencia.Repositorios.Parametros;
using Controlsys.Repositorios.Pessoas;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Repositorios.Pessoas
{
    /// <summary>
    /// Representa um(a) RepositorioVisitante.
    /// </summary>
    public class RepositorioVisitante : Repositorio<Visitante>, IRepositorioVisitante
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Pessoas.RepositorioVisitante.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioVisitante(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho) { }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        /// <param name="visitante">
        /// O(a) visitante.
        /// </param>
        public void Validar(EstadoObjeto estadoObjeto, Visitante visitante)
        {
            Repositorio<Papel> repPapel = new Repositorio<Papel>(UnidadeTrabalho);

            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    RepositorioPessoaCritica repPessoaCritica = new RepositorioPessoaCritica(UnidadeTrabalho);
                    RepositorioParametro repParam = new RepositorioParametro(UnidadeTrabalho);

                    PessoaCritica pesCritica = new PessoaCritica();
                    pesCritica.PessoaFisica = visitante.PessoaFisica;
                    if (repPessoaCritica.Existe(pesCritica))
                    {
                        string msgCadastroPessoa = repParam.RecuperarParametro(ParametroSistema.MensagemCadastroPessoaFisica).Valor;
                        throw new CoreException(msgCadastroPessoa);
                    }

                    if (visitante.Solicitante == null)
                        throw new CoreException("Não é possível salvar. Preencha corretamente o campo de Solicitante.");

                    if (visitante.ResponsavelVisita == null)
                        throw new CoreException("Não é possível salvar. Preencha corretamente o campo de Responsável.");

                    if (visitante.Empresa == null || visitante.Motivo == null || visitante.Funcao == null
                         || visitante.DataFim == null || visitante.DataInicio == null)
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                                        
                    List<Papel> listaPapeisPF = repPapel.ObterTodos().Where(p => p.PessoaFisica.Codigo == visitante.PessoaFisica.Codigo).ToList();

                    Papel papel = listaPapeisPF.Where(p => p.PapelLogs.OrderByDescending(pl => pl.Codigo).FirstOrDefault().Status == StatusPapelLog.Ativo).FirstOrDefault();

                    if (papel != null)
                    {
                        string tipoPapel = papel is Colaborador ? "Colaborador" : papel is PrestadorServico ? "Prestador de Serviço" : "Visitante";
                        throw new CoreException(string.Format("Pessoa já possui papel ativo de {0} no sistema.", tipoPapel));
                    }

                    /* Validação para garantir o Id Usuario unico para papel ativo. */
                    List<Papel> listaPapeisIdUsuario = repPapel.ObterTodos().Where(p => p.PessoaFisica.Codigo != visitante.PessoaFisica.Codigo && p.IdUsuario != null && p.IdUsuario == visitante.IdUsuario).ToList();
                    Papel papelIdUsuario = listaPapeisIdUsuario.Where(p => p.PapelLogs.OrderByDescending(pl => pl.Codigo).FirstOrDefault().Status == StatusPapelLog.Ativo).FirstOrDefault();
                    if (papelIdUsuario != null)
                    {
                        throw new CoreException("O Id Usuário já esta sendo utilizado pelo papel ativo: " + papelIdUsuario.PessoaFisica.Nome);
                    }

                    break;
                case EstadoObjeto.Alterado:
                    if (visitante.Solicitante == null)
                        throw new CoreException("Não é possível salvar. Preencha corretamente o campo de Solicitante.");

                    if (visitante.ResponsavelVisita == null)
                        throw new CoreException("Não é possível salvar. Preencha corretamente o campo de Responsável.");

                    if (visitante.Empresa == null || visitante.Motivo == null || visitante.Funcao == null
                         || visitante.DataFim == null || visitante.DataInicio == null)
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");

                    /* Validação para garantir o Id Usuario unico para papel ativo. */
                    List<Papel> listaPapeisIdUsuarioAlt = repPapel.ObterTodos().Where(p => p.PessoaFisica.Codigo != visitante.PessoaFisica.Codigo && p.IdUsuario != null && p.IdUsuario == visitante.IdUsuario).ToList();
                    Papel papelIdUsuarioAlt = listaPapeisIdUsuarioAlt.Where(p => p.PapelLogs.OrderByDescending(pl => pl.Codigo).FirstOrDefault().Status == StatusPapelLog.Ativo).FirstOrDefault();
                    if (papelIdUsuarioAlt != null)
                    {
                        throw new CoreException("O Id Usuário já esta sendo utilizado pelo papel ativo: " + papelIdUsuarioAlt.PessoaFisica.Nome);
                    }

                    break;
                case EstadoObjeto.Removido:
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="visitante">
        /// O(a) visitante.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool Existe(Visitante visitante)
        {
            return ObterTodos().Any(u => u.Codigo != visitante.Codigo);
        }


        public override void Salvar(Visitante entidade)
        {
            if (entidade.Codigo <= 0)
                entidade.DataRegistro = DateTime.Now;
            base.Salvar(entidade);
        }
    }
}
