﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Pessoas;
using Controlsys.Repositorios.Pessoas;
using Globalsys;

namespace Controlsys.Persistencia.Repositorios.Pessoas
{
    /// <summary>
    /// Representa um(a) RepositorioGrupoTrabalhoColab.
    /// </summary>
    public class RepositorioGrupoTrabalhoColab : Repositorio<GrupoTrabalhoColab>, IRepositorioGrupoTrabalhoColab
    {
        /// <summary>
        /// Construtor para
        /// Controlsys.Persistencia.Repositorios.Pessoas.RepositorioGrupoTrabalhoColab.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioGrupoTrabalhoColab(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {

        }

        /// <summary>
        /// Obter grupo trabalho.
        /// </summary>
        ///
        /// <param name="codigoColaborador">
        /// O(a) codigo colaborador.
        /// </param>
        ///
        /// <returns>
        /// Um(a) GrupoTrabalhoColab.
        /// </returns>
        public GrupoTrabalhoColab ObterGrupoTrabalho(int codigoColaborador)
        {
            return ObterTodos().Where(gtc => gtc.Papel.Codigo == codigoColaborador && gtc.Ativo).FirstOrDefault();
        }
    }
}
