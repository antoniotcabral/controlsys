﻿using Controlsys.Dominio.Pessoas;
using Controlsys.Repositorios.Parametros;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Repositorios.Pessoas;

namespace Controlsys.Persistencia.Repositorios.Pessoas
{
    /// <summary>
    /// Representa um(a) RepositorioPessoa.
    /// </summary>
    public class RepositorioPessoa : Repositorio<Pessoa>, IRepositorioPessoa
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Pessoas.RepositorioPessoa.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioPessoa(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
        }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        public void Validar(Pessoa entidade, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    if (entidade.Nome == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe um checklist de documento com o nome informado.");
                    }
                    break;
                case EstadoObjeto.Alterado:
                   
                    if (entidade.Nome == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe uma pessoa com o nome informado.");
                    }
                    break;
               
                default:
                    break;
            }
        }

        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool Existe(Pessoa entidade)
        {
            return ObterTodos().Any(s => s.Nome == entidade.Nome && s.Codigo != entidade.Codigo);
        }

    }
}
