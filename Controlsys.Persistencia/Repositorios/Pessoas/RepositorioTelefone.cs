﻿using Controlsys.Dominio.Pessoas;
using Controlsys.Repositorios.Pessoas;
using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Globalsys.Validacao;
using Globalsys.Exceptions;

namespace Controlsys.Persistencia.Repositorios.Pessoas
{
    /// <summary>
    /// Representa um(a) RepositorioTelefone.
    /// </summary>
    public class RepositorioTelefone : Repositorio<Telefone>, IRepositorioTelefone
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Pessoas.RepositorioTelefone.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioTelefone(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
        }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        /// <param name="telefone">
        /// O(a) telefone.
        /// </param>
        public void Validar(EstadoObjeto estadoObjeto, Telefone telefone)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    if (string.IsNullOrEmpty(telefone.TelefoneNum))
                        throw new CoreException("Não é possível inserir o telefone sem informar o número.");
                    break;
                case EstadoObjeto.Alterado:
                    break;
                case EstadoObjeto.Removido:
                    break;
                case EstadoObjeto.Ativado:
                    break;
                case EstadoObjeto.Inativado:
                    break;
                default:
                    break;
            }
        }
    }
}
