﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Pessoas;
using Controlsys.Persistencia.Repositorios.Enderecos;
using Controlsys.Repositorios.Pessoas;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using Controlsys.Persistencia.Repositorios.Seguranca;
using Controlsys.Dominio.Seguranca;
using Controlsys.Repositorio.Acesso;
using Controlsys.Dominio.Acesso;
using Controlsys.Persistencia.Repositorios.Parametros;
using Controlsys.Dominio.Parametros;

namespace Controlsys.Persistencia.Repositorios.Pessoas
{
    /// <summary>
    /// Representa um(a) RepositorioPessoaFisica.
    /// </summary>
    public class RepositorioPessoaFisica : Repositorio<PessoaFisica>, IRepositorioPessoaFisica
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Pessoas.RepositorioPessoaFisica.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioPessoaFisica(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
        }

        /// <summary>
        /// Obtém a pessoa fisica com cpf e/ou passaporte.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="verificarPapelAtivo">
        /// true to verificar papel ativo.
        /// </param>
        /// <param name="cpf">
        /// .
        /// </param>
        /// <param name="passaporte">
        /// .
        /// </param>
        /// <param name="usuario">
        /// true to usuario.
        /// </param>
        ///
        /// <returns>
        /// Um(a) PessoaFisica.
        /// </returns>
        public PessoaFisica ObterPorCpfPassaporte(bool verificarPapelAtivo = false, string cpf = null, string passaporte = null, bool usuario = false)
        {
            if (string.IsNullOrEmpty(cpf) && string.IsNullOrEmpty(passaporte))
                throw new CoreException("Favor informar cpf ou passaporte!");

            IQueryable<PessoaFisica> pessoasFisicas = ObterTodos();

            if (!string.IsNullOrEmpty(cpf))
                pessoasFisicas = pessoasFisicas.Where(pf => pf.CPF == cpf);

            if (!string.IsNullOrEmpty(passaporte))
                pessoasFisicas = pessoasFisicas.Where(pf => pf.Passaporte == passaporte);

            PessoaFisica pessoaFis = pessoasFisicas.FirstOrDefault();

            if (pessoaFis != null && verificarPapelAtivo && pessoaFis.Papeis.Any())
            {
                Papel papel = pessoaFis.Papeis.Where(p => p.PapelLogs.OrderByDescending(pl => pl.Codigo).FirstOrDefault().Status == StatusPapelLog.Ativo).FirstOrDefault();
                if (papel != null)
                {
                    try
                    { 
                        string tipoPapel = papel is Colaborador ? "Colaborador" : papel is PrestadorServico ? "Prestador de Serviço" : "Visitante";
                        throw new CoreException(string.Format("Pessoa já possui papel ativo de {0} no sistema.", tipoPapel));
                    }
                    catch (Exception e) { 
                        throw new CoreException(e.Message);
                    }
                }
            }

            if (usuario && pessoaFis != null)
            {
                RepositorioUsuario repUsuario = new RepositorioUsuario(UnidadeTrabalho);
                pessoaFis.Usuario = repUsuario.ObterPorId(pessoaFis.Codigo);
            }

            return pessoaFis;
        }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        /// <exception cref="Exception">
        /// Lançada quando uma condição de erro exception ocorre.
        /// </exception>
        ///
        /// <param name="estado">
        /// O(a) estado.
        /// </param>
        /// <param name="pessoaFisica">
        /// O(a) pessoa fisica.
        /// </param>
        /// <param name="papel">
        /// O(a) papel.
        /// </param>
        public void Validar(EstadoObjeto estado, PessoaFisica pessoaFisica, Papel papel = null)
        {
            try
            {
                switch (estado)
                {
                    case EstadoObjeto.Novo:                        
                    case EstadoObjeto.Alterado:
                        //if (papel != null && !string.IsNullOrEmpty(pessoaFisica.CPF))
                        //{
                        //    if (string.IsNullOrEmpty(pessoaFisica.RG) || string.IsNullOrEmpty(pessoaFisica.RGOrgaoEmissor) ||
                        //            pessoaFisica.RGOrgaoEmissorUF == null || !pessoaFisica.RGDataEmissao.HasValue)
                        //        throw new CoreException("Não é possível salvar. Preencha corretamente os campos de Registro Geral (RG).");

                        //}

                        if (papel != null)
                            if ((!pessoaFisica.DataNascimento.HasValue ||
                                    string.IsNullOrEmpty(pessoaFisica.NomeMae) ||
                                        string.IsNullOrEmpty(pessoaFisica.NomePai)) && papel == null)
                                throw new CoreException("Não é possível salvar. Preencha corretamente os campos de Data Nascimento, Nome Mãe, Nome Pai e Tipo Sanguíneo.");

                        if (Existe(pessoaFisica))
                            throw new CoreException("Já existe uma pessoa com o CPF ou Passaporte informado.");

                        if (string.IsNullOrEmpty(pessoaFisica.Codigo))
                            throw new Exception("O código da pessoa fisica não foi gerado");

                        //if ((pessoaFisica.Sexo != Sexo.Masculino) && (pessoaFisica.Sexo != Sexo.Feminino))
                        //    throw new Exception("O sexo da pessoa física não pode ser diferente de Masculino ou Feminino.");

                        break;

                    case EstadoObjeto.Removido:
                        break;
                    case EstadoObjeto.Ativado:
                        break;
                    case EstadoObjeto.Inativado:
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                throw new CoreException("Problema ao salvar: " + ex.Message);
            }
            
        }

        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="pessoaFisica">
        /// O(a) pessoa fisica.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool Existe(PessoaFisica pessoaFisica)
        {
            string cpf = !string.IsNullOrEmpty(pessoaFisica.CPF) ? pessoaFisica.CPF.Replace(".", "").Replace("-", "") : null;
            return ObterTodos().Any(pf => ((!string.IsNullOrEmpty(pessoaFisica.CPF) && pf.CPF == cpf) || 
                                           (!string.IsNullOrEmpty(pessoaFisica.Passaporte) && pf.Passaporte == pessoaFisica.Passaporte)) &&
                                          pf.Codigo != pessoaFisica.Codigo);
        }

        /// <summary>
        /// Salvar.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="pessoaFisica">
        /// O(a) pessoa fisica.
        /// </param>
        /// <param name="papel">
        /// O(a) papel.
        /// </param>
        public void Salvar(PessoaFisica pessoaFisica, Papel papel)
        {

            if (string.IsNullOrEmpty(pessoaFisica.CPF) && string.IsNullOrEmpty(pessoaFisica.Passaporte))
                throw new CoreException("Favor informar cpf ou passaporte!");

            RepositorioTelefone repTelefone = new RepositorioTelefone(UnidadeTrabalho);
            Repositorio<Email> repEmail = new Repositorio<Email>(UnidadeTrabalho);

            pessoaFisica.DataRegistro = DateTime.Now;
            pessoaFisica.GerarCodigoPessoa(ObterTodos());
            if (!string.IsNullOrEmpty(pessoaFisica.CPF))
            {
                pessoaFisica.CPF = pessoaFisica.CPF.Replace(".", "").Replace("-", "");
            }
            Validar(EstadoObjeto.Novo, pessoaFisica, papel);
            base.Salvar(pessoaFisica);

            foreach (Telefone telefone in pessoaFisica.Telefones)
            {
                telefone.Pessoa = pessoaFisica;
                telefone.TelefoneNum = telefone.TelefoneNum.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "");
                telefone.DataRegistro = DateTime.Now;
                telefone.Ativar();

                repTelefone.Validar(EstadoObjeto.Novo, telefone);
                repTelefone.Salvar(telefone);
            }
        }

        //public void SalvarPapelLog(PapelLog papelLog)
        //{
        //    Repositorio<PapelLog> repPapelLog = new Repositorio<PapelLog>(UnidadeTrabalho);
        //    repPapelLog.Salvar(papelLog);
        //}

        /// <summary>
        /// Obter lista pessoa.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="pessoaFisica">
        /// O(a) pessoa fisica.
        /// </param>
        /// <param name="verificaStatus">
        /// true to verifica status.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;PessoaFisica&gt;
        /// </returns>
        public List<PessoaFisica> ObterListaPessoa(PessoaFisica pessoaFisica, bool verificaStatus = false)
        {

            if (string.IsNullOrEmpty(pessoaFisica.CPF) && string.IsNullOrEmpty(pessoaFisica.Passaporte) && string.IsNullOrEmpty(pessoaFisica.Nome))
                throw new CoreException("Favor informar cpf/passaporte ou nome.");


            IQueryable<PessoaFisica> pessoasFisicas = ObterTodos();

            if (!string.IsNullOrEmpty(pessoaFisica.Nome))
                pessoasFisicas = pessoasFisicas.Where(pf => pf.Nome.ToUpper().Contains(pessoaFisica.Nome.ToUpper()));

            if (!string.IsNullOrEmpty(pessoaFisica.CPF.Replace(".", "").Replace("-", "")))
                pessoasFisicas = pessoasFisicas.Where(pf => pf.CPF.Contains(pessoaFisica.CPF));

            if (!string.IsNullOrEmpty(pessoaFisica.Passaporte.Replace(".", "").Replace("-", "")))
                pessoasFisicas = pessoasFisicas.Where(pf => pf.Passaporte.Contains(pessoaFisica.Passaporte));

            if (verificaStatus)
            {
                pessoasFisicas = pessoasFisicas.Where(pf => pf.Papeis.Any() && pf.Papeis.Any(p => p.PapelLogs.OrderByDescending(pl => pl.Codigo).Select(pl => pl.Status).FirstOrDefault() != StatusPapelLog.BaixaDefinitiva));
            }


            return pessoasFisicas.Take(100).ToList();
        }


    }
}
