﻿using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Pessoas;
using Controlsys.Repositorio.Acesso;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Repositorios.Pessoas
{
    /// <summary>
    /// Representa um(a) RepositorioPessoaCritica.
    /// </summary>
    public class RepositorioPessoaCritica : Repositorio<PessoaCritica>, IRepositorioPessoaCritica
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Pessoas.RepositorioPessoaCritica.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioPessoaCritica(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho) { }

        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="pessoaCritica">
        /// O(a) pessoa critica.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool Existe(PessoaCritica pessoaCritica)
        {
            return ObterTodos().Any(s => ((!string.IsNullOrEmpty(pessoaCritica.PessoaFisica.CPF) && s.PessoaFisica.CPF == pessoaCritica.PessoaFisica.CPF)
                                          || (!string.IsNullOrEmpty(pessoaCritica.PessoaFisica.Passaporte) && s.PessoaFisica.Passaporte == pessoaCritica.PessoaFisica.Passaporte)) && s.Ativo && s.Codigo != pessoaCritica.Codigo);

        }

        public bool Existe(string cpf = null, string passaporte = null)
        {
            return ObterTodos().Any(s => ((!string.IsNullOrEmpty(cpf) && s.PessoaFisica.CPF == cpf)
                                          || (!string.IsNullOrEmpty(passaporte) && s.PessoaFisica.Passaporte == passaporte)) && s.Ativo);

        }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        public void Validar(PessoaCritica entidade, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    if (entidade.PessoaFisica == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }

                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe uma pessoa crítica com o nome informado.");
                    }
                    break;
                case EstadoObjeto.Alterado:
                    if (!entidade.Ativo)
                    {
                        throw new CoreException("A pessoa crítica está inativo, não é possível alterar.");
                    }
                    if (entidade.PessoaFisica == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe uma pessoa crítica com o nome informado.");
                    }
                    break;
                case EstadoObjeto.Ativado:
                    if (!entidade.Ativo && Existe(entidade))
                    {
                        throw new CoreException("Já existe uma pessoa crítica com o nome informado.");
                    }
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Obter por cpf.
        /// </summary>
        ///
        /// <param name="cpf">
        /// O(a) cpf.
        /// </param>
        ///
        /// <returns>
        /// Um(a) PessoaCritica.
        /// </returns>
        public PessoaCritica ObterPorCPF(string cpf, bool apenasAtivo = false)
        {
            IQueryable<PessoaCritica> pessoaCritica = ObterTodos().Where(p => p.PessoaFisica.CPF == cpf);
            if (apenasAtivo)
                pessoaCritica = pessoaCritica.Where(pc => pc.Ativo);

            return pessoaCritica.FirstOrDefault();
        }
    }
}
