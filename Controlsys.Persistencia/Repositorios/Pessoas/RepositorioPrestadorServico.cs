﻿using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Controlsys.Persistencia.Repositorios.Parametros;
using Controlsys.Repositorios.Pessoas;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Repositorios.Pessoas
{
    /// <summary>
    /// Representa um(a) RepositorioPrestadorServico.
    /// </summary>
    public class RepositorioPrestadorServico : Repositorio<PrestadorServico>, IRepositorioPrestadorServico
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Pessoas.RepositorioPrestadorServico.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioPrestadorServico(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho) { }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        /// <param name="prestador">
        /// O(a) prestador.
        /// </param>
        public void Validar(EstadoObjeto estadoObjeto, PrestadorServico prestador)
        {
            Repositorio<Papel> repPapel = new Repositorio<Papel>(UnidadeTrabalho);

            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    RepositorioPessoaCritica repPessoaCritica = new RepositorioPessoaCritica(UnidadeTrabalho);
                    RepositorioParametro repParam = new RepositorioParametro(UnidadeTrabalho);

                    PessoaCritica pesCritica = new PessoaCritica();
                    pesCritica.PessoaFisica = prestador.PessoaFisica;
                    if (repPessoaCritica.Existe(pesCritica))
                    {
                        string msgCadastroPessoa = repParam.RecuperarParametro(ParametroSistema.MensagemCadastroPessoaFisica).Valor;
                        throw new CoreException(msgCadastroPessoa);
                    }

                    if (!string.IsNullOrEmpty(prestador.PessoaFisica.CPF))
                    {
                        //if (!prestador.PessoaFisica.TituloEleitor.HasValue || !prestador.PessoaFisica.TituloEleitorZona.HasValue ||
                        //        !prestador.PessoaFisica.TituloEleitorSecao.HasValue || prestador.PessoaFisica.TituloEleitorCidade == null)
                        //    throw new CoreException("Não é possível salvar. Preencha corretamente os campos de Título de Eleitor.");

                        if (prestador.TipoPrestador != TipoPrestador.Emergencial && (!prestador.PessoaFisica.CTPS.HasValue || string.IsNullOrEmpty(prestador.PessoaFisica.CTPSSerie) ||
                                 prestador.PessoaFisica.CTPSEstado == null || !prestador.PessoaFisica.CTPSData.HasValue))
                            throw new CoreException("Não é possível salvar. Preencha corretamente os campos de Carteira de Trabalho e Previdência Social (CTPS).");

                        //if (prestador.PessoaFisica.Sexo == Sexo.Masculino
                        //        && (string.IsNullOrEmpty(prestador.PessoaFisica.CertificadoReservista)
                        //            || string.IsNullOrEmpty(prestador.PessoaFisica.CertificadoReservistaCat)))
                        //    throw new CoreException("Não é possível salvar. Preencha corretamente os campos de Certificado de Reservista.");
                    }

                    if (prestador.TipoPrestador == TipoPrestador.Emergencial
                            && prestador.Fornecedor == null)
                        throw new CoreException("Não é possível salvar. Preencha corretamente o campo de Fornecedor.");

                    if (prestador.Solicitante == null)
                        throw new CoreException("Não é possível salvar. Preencha corretamente o campo de Solicitante.");

                    if (prestador.TipoPrestador == TipoPrestador.Temporario
                            && (prestador.Empresa == null || prestador.DataAdmissao == null || prestador.ASOs.Count() == 0))
                        throw new CoreException("Não é possível salvar. Preencha corretamente os campos de Empresa, Data Admissão e ASOs.");

                    /*
                    Papel papel = prestador.PessoaFisica.Papeis.Where(p => p.PapelLogs.OrderByDescending(pl => pl.Codigo).FirstOrDefault().Status == StatusPapelLog.Ativo).FirstOrDefault();
                    if (papel != null)
                    {
                        string tipoPapel = papel is Colaborador ? "Colaborador" : papel is PrestadorServico ? "Prestador de Serviço" : "Visitante";
                        throw new CoreException(string.Format("Pessoa já possui papel ativo de {0} no sistema.", tipoPapel));
                    }*/

                    /**/
                     
                    List<Papel> listaPapeisPF = repPapel.ObterTodos().Where(p => p.PessoaFisica.Codigo == prestador.PessoaFisica.Codigo).ToList();
                    Papel papel = listaPapeisPF.Where(p => p.PapelLogs.Where(pl => pl.DataInicio.Date <= DateTime.Now.Date).OrderByDescending(pl => pl.Codigo).FirstOrDefault().Status == StatusPapelLog.Ativo).FirstOrDefault();
                    if (papel != null)
                    {
                        string tipoPapel = papel is Colaborador ? "Colaborador" : papel is PrestadorServico ? "Prestador de Serviço" : "Visitante";
                        throw new CoreException(string.Format("Pessoa já possui papel ativo de {0} no sistema.", tipoPapel));
                    }

                    /* Validação para garantir o Id Usuario unico para papel ativo. */
                    List<Papel> listaPapeisIdUsuario = repPapel.ObterTodos().Where(p => p.PessoaFisica.Codigo != prestador.PessoaFisica.Codigo && p.IdUsuario != null && p.IdUsuario == prestador.IdUsuario).ToList();
                    Papel papelIdUsuario = listaPapeisIdUsuario.Where(p => p.PapelLogs.OrderByDescending(pl => pl.Codigo).FirstOrDefault().Status == StatusPapelLog.Ativo).FirstOrDefault();
                    if (papelIdUsuario != null)
                    {
                        throw new CoreException("O Id Usuário já esta sendo utilizado pelo papel ativo: " + papelIdUsuario.PessoaFisica.Nome);
                    }

                    /**/

                    break;
                case EstadoObjeto.Alterado:
                    if (!string.IsNullOrEmpty(prestador.PessoaFisica.CPF))
                    {
                        //if (!prestador.PessoaFisica.TituloEleitor.HasValue || !prestador.PessoaFisica.TituloEleitorZona.HasValue ||
                        //        !prestador.PessoaFisica.TituloEleitorSecao.HasValue || prestador.PessoaFisica.TituloEleitorCidade == null)
                        //    throw new CoreException("Não é possível salvar. Preencha corretamente os campos de Título de Eleitor.");

                        if (!prestador.PessoaFisica.CTPS.HasValue || string.IsNullOrEmpty(prestador.PessoaFisica.CTPSSerie) ||
                                prestador.PessoaFisica.CTPSEstado == null || !prestador.PessoaFisica.CTPSData.HasValue)
                            throw new CoreException("Não é possível salvar. Preencha corretamente os campos de Carteira de Trabalho e Previdência Social (CTPS).");

                        //if (prestador.PessoaFisica.Sexo == Sexo.Masculino
                        //        && (string.IsNullOrEmpty(prestador.PessoaFisica.CertificadoReservista)
                        //            || string.IsNullOrEmpty(prestador.PessoaFisica.CertificadoReservistaCat)))
                        //    throw new CoreException("Não é possível salvar. Preencha corretamente os campos de Certificado de Reservista.");
                    }

                    if (prestador.TipoPrestador == TipoPrestador.Emergencial
                            && prestador.Fornecedor == null)
                        throw new CoreException("Não é possível salvar. Preencha corretamente o campo de Fornecedor.");

                    if (prestador.Solicitante == null)
                        throw new CoreException("Não é possível salvar. Preencha corretamente o campo de Solicitante.");

                    if (prestador.TipoPrestador == TipoPrestador.Temporario
                            && (prestador.Empresa == null || prestador.DataAdmissao == null || prestador.ASOs.Count() == 0))
                        throw new CoreException("Não é possível salvar. Preencha corretamente os campos de Empresa, Data Admissão e ASOs.");

                    /* Validação para garantir o Id Usuario unico para papel ativo. */
                    List<Papel> listaPapeisIdUsuarioAlt = repPapel.ObterTodos().Where(p => p.PessoaFisica.Codigo != prestador.PessoaFisica.Codigo && p.IdUsuario != null && p.IdUsuario == prestador.IdUsuario).ToList();
                    Papel papelIdUsuarioAlt = listaPapeisIdUsuarioAlt.Where(p => p.PapelLogs.OrderByDescending(pl => pl.Codigo).FirstOrDefault().Status == StatusPapelLog.Ativo).FirstOrDefault();
                    if (papelIdUsuarioAlt != null)
                    {
                        throw new CoreException("O Id Usuário já esta sendo utilizado pelo papel ativo: " + papelIdUsuarioAlt.PessoaFisica.Nome);
                    }

                    break;
                case EstadoObjeto.Removido:
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="prestador">
        /// O(a) prestador.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool Existe(PrestadorServico prestador)
        {
            return ObterTodos().Any(u => u.Codigo != prestador.Codigo);
        }

        public override void Salvar(PrestadorServico entidade)
        {
            if (entidade.Codigo <= 0)
                entidade.DataRegistro = DateTime.Now;
            base.Salvar(entidade);
        }

        //public void SalvarAso(ASO aso, PrestadorServico prestador)
        //{
        //    Repositorio<ASO> rep = new Repositorio<ASO>(base.UnidadeTrabalho);
        //    ASO asoNew = new ASO();
        //    asoNew.DataRealizacao = aso.DataRealizacao;
        //    asoNew.DataVencimento = aso.DataVencimento;
        //    aso.Papel = prestador;
        //    rep.Salvar(aso);
        //}


        //public void RemoverAso(ASO aso)
        //{
        //    Repositorio<ASO> rep = new Repositorio<ASO>(base.UnidadeTrabalho);
        //    rep.Remover(aso);            
        //}

    }
}
