﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Pessoas;
using Controlsys.Repositorios.Pessoas;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using Controlsys.Persistencia.Repositorios.Seguranca;
using Controlsys.Dominio.Seguranca;
using Controlsys.Persistencia.Repositorios.Empresas;
using Controlsys.Persistencia.Repositorios.Parametros;
using Controlsys.Dominio.Empresas;
using Controlsys.Persistencia.Repositorios.Acesso;
using Controlsys.Dominio.Acesso;

namespace Controlsys.Persistencia.Repositorios.Pessoas
{
    /// <summary>
    /// Representa um(a) RepositorioPapel.
    /// </summary>
    public class RepositorioPapel : Repositorio<Papel>, IRepositorioPapel
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Pessoas.RepositorioPapel.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioPapel(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho){ }

        /// <summary>
        /// Salvar papel log.
        /// </summary>
        ///
        /// <param name="papelLog">
        /// O(a) papel log.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Papel.
        /// </returns>
        public Papel SalvarPapelLog(PapelLog papelLog)
        {
            Repositorio<PapelLog> repPapelLog = new Repositorio<PapelLog>(UnidadeTrabalho);
            repPapelLog.Salvar(papelLog);
            return papelLog.Papel;
        }

        /// <summary>
        /// Validar papel log.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="papelLogOld">
        /// O(a) papel log old.
        /// </param>
        /// <param name="papelLog">
        /// O(a) papel log.
        /// </param>
        public void ValidarPapelLog(PapelLog papelLogOld, PapelLog papelLog)
        {            
            switch (papelLogOld.Status)
            {
                case StatusPapelLog.BaixaDefinitiva:
                    throw new CoreException("Não é possível alterar status pois a pessoa não possui mais papel ativo no sistema.");
                //case StatusPapelLog.Ativo:
                //    if (papelLog.Status == StatusPapelLog.Ativo)
                //        throw new CoreException("Não é possível alterar status, a pessoa já está ativa.");
                //    break;

                default:
                    break;
            }

            if (papelLog.Status == StatusPapelLog.Ativo)
            {
                if (ObterTodos().Any(p => p.PessoaFisica.Codigo == papelLog.Papel.PessoaFisica.Codigo
                                       && p.PapelLogs
                                                .OrderByDescending(pl => pl.Codigo)
                                                .Select(pl => pl.Status)
                                                .FirstOrDefault() == StatusPapelLog.Ativo))
                    throw new CoreException("Não possível salvar. A pessoa selecionada está ativa em um outro papel.");
            }
        }

        /// <summary>
        /// Obter lista papel.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="codigos">
        /// O(a) codigos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;Papel&gt;
        /// </returns>
        public List<Papel> ObterListaPapel(int[] codigos)
        {
            if (codigos == null)
                throw new CoreException("Nenhuma pessoa selecionada.");

            IQueryable<Papel> listaPapel = ObterTodos().Where(t => codigos.Contains(t.Codigo));

            return listaPapel.ToList();
        }

        /// <summary>
        /// Salvar aso.
        /// </summary>
        ///
        /// <param name="aso">
        /// O(a) aso.
        /// </param>
        /// <param name="papel">
        /// O(a) papel.
        /// </param>
        public void SalvarAso(ASO aso, Papel papel)
        {
            Repositorio<ASO> rep = new Repositorio<ASO>(base.UnidadeTrabalho);
            ASO asoNew = new ASO();
            asoNew.DataRealizacao = aso.DataRealizacao;
            asoNew.DataVencimento = aso.DataVencimento;
            aso.Papel = papel;
            rep.Salvar(aso);
        }

        /// <summary>
        /// Remover aso.
        /// </summary>
        ///
        /// <param name="aso">
        /// O(a) aso.
        /// </param>
        public void RemoverAso(ASO aso)
        {
            Repositorio<ASO> rep = new Repositorio<ASO>(base.UnidadeTrabalho);
            rep.Remover(aso);
        }

        /// <summary>
        /// Salvar excecao acesso.
        /// </summary>
        ///
        /// <param name="papel">
        /// O(a) papel.
        /// </param>
        /// <param name="excecao">
        /// O(a) excecao.
        /// </param>
        public void SalvarExcecaoAcesso(Papel papel, ExcecaoAcesso excecao)
        {
            Repositorio<ExcecaoAcesso> rep = new Repositorio<ExcecaoAcesso>(base.UnidadeTrabalho);
            ExcecaoAcesso excecaoNew = new ExcecaoAcesso();
            excecaoNew.CriterioBloqueio = excecao.CriterioBloqueio;
            excecaoNew.Ativar();
            excecaoNew.DataRegistro = DateTime.Now;
            excecaoNew.Papel = papel;
            rep.Salvar(excecaoNew);
        }

        /// <summary>
        /// Obter por identifier excecao.
        /// </summary>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) ExcecaoAcesso.
        /// </returns>
        public ExcecaoAcesso ObterPorIdExcecao(int codigo)
        {
            Repositorio<ExcecaoAcesso> rep = new Repositorio<ExcecaoAcesso>(base.UnidadeTrabalho);
            ExcecaoAcesso excecao = rep.ObterPorId(codigo);
            return excecao;
        }

        /// <summary>
        /// Dar baixa definitiva.
        /// </summary>
        ///
        /// <param name="papelLog">
        /// O(a) papel log.
        /// </param>
        public void DarBaixaDefinitiva(PapelLog papelLog)
        {
            RepositorioAlocacaoColaborador repAlocacao = new RepositorioAlocacaoColaborador(UnidadeTrabalho);
            RepositorioGrupoTrabalhoColab repGrupoTrabalhoColab = new RepositorioGrupoTrabalhoColab(UnidadeTrabalho); 
            RepositorioCracha repCracha = new RepositorioCracha(UnidadeTrabalho); 
                        
            if (papelLog.Papel is Colaborador)
            {
                AlocacaoColaborador alocacao = repAlocacao.ObterAlocacao(papelLog.Papel.Codigo);                
                GrupoTrabalhoColab grupoTrabalhoColab = repGrupoTrabalhoColab.ObterGrupoTrabalho(papelLog.Papel.Codigo);

                if (alocacao != null)
                {
                    alocacao.Inativar();
                    repAlocacao.Atualizar(alocacao);
                }                

                if (grupoTrabalhoColab != null)
                {
                    grupoTrabalhoColab.Inativar();
                    repGrupoTrabalhoColab.Atualizar(grupoTrabalhoColab);
                }
            }

            Cracha cracha = repCracha.ObterCracha(papelLog.Papel.Codigo);
            if (cracha != null)
            {
                cracha.Inativar();
                repCracha.Atualizar(cracha);
            }

            RepositorioGrupoAcessoPapel repGrupoAcessoPapel = new RepositorioGrupoAcessoPapel(UnidadeTrabalho);
            List<GrupoAcessoPapel> gruposDeAcesso = repGrupoAcessoPapel.ObterGruposDeAcesso(papelLog.Papel.Codigo);

            foreach (GrupoAcessoPapel grupoAcessoPapel in gruposDeAcesso)
            {
                grupoAcessoPapel.Inativar();
                repGrupoAcessoPapel.Atualizar(grupoAcessoPapel);
            }
        }
    }
}


