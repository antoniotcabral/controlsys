﻿using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Parametros;
using Controlsys.Repositorio.Acesso;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Controlsys.Persistencia.Repositorios.Pessoas
{
    public class RepositorioPessoaCriticaMidia : Repositorio<PessoaCriticaMidia>, IRepositorioPessoaCriticaMidia
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Pessoas.RepositorioPessoaCritica.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioPessoaCriticaMidia(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho) { }

        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="pessoaCriticaMidia">
        /// O(a) pessoa critica midia.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool Existe(PessoaCriticaMidia pessoaCriticaMidia)
        {
            return ObterTodos().Any(s => s.PessoaCritica.Codigo == pessoaCriticaMidia.Midia.Codigo && s.Midia.Codigo == pessoaCriticaMidia.Midia.Codigo);

        }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        public void Validar(PessoaCriticaMidia entidade, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    if (entidade.PessoaCritica == null || entidade.Midia == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }

                    break;
                case EstadoObjeto.Alterado:
                    if (entidade.PessoaCritica == null || entidade.Midia == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    break;
                default:
                    break;
            }
        }

        public PessoaCriticaMidia SalvarPessoaCriticaMidia(PessoaCritica pessoaCritica, Midia midia)
        {
            Repositorio<PessoaCriticaMidia> repPessoaCriticaMidia = new Repositorio<PessoaCriticaMidia>(base.UnidadeTrabalho);

            PessoaCriticaMidia pessoaCriticaMidia = new PessoaCriticaMidia();
            pessoaCriticaMidia.PessoaCritica = pessoaCritica;
            pessoaCriticaMidia.Midia = midia;
            pessoaCriticaMidia.DataRegistro = DateTime.Now;
            pessoaCriticaMidia.Ativar();

            repPessoaCriticaMidia.Salvar(pessoaCriticaMidia);

            return pessoaCriticaMidia;
        }
    }
}
