﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Seguranca;
using Controlsys.Repositorios.Seguranca;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;

namespace Controlsys.Persistencia.Repositorios.Seguranca
{
    /// <summary>
    /// Representa um(a) RepositorioGrupo.
    /// </summary>
    public class RepositorioGrupo : Repositorio<Grupo>, IRepositorioGrupo
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Seguranca.RepositorioGrupo.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioGrupo(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
        }        

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="grupo">
        /// O(a) grupo.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        public void Validar(Grupo grupo, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                case EstadoObjeto.Alterado:
                    if (Existe(grupo))
                        throw new CoreException("Não é possível salvar. Já existe um grupo com o nome informado.");

                    if (!grupo.Ativo)
                        throw new CoreException("Não é possível salvar. O grupo está inativo.");
                    break;
                case EstadoObjeto.Removido:
                    break;
                case EstadoObjeto.Ativado:
                    if (Existe(grupo))
                        throw new CoreException("Não é possível ativar. Já existe um grupo ativo com o nome informado.");
                    break;
                case EstadoObjeto.Inativado:
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="grupo">
        /// O(a) grupo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool Existe(Grupo grupo)
        {
            return ObterTodos().Any(g => g.Nome == grupo.Nome
                                        && g.Ativo
                                        && g.Codigo != grupo.Codigo);
        }
    }
}
