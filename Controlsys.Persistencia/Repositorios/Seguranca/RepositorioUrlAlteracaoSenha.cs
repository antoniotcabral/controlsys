﻿using Controlsys.Dominio.Seguranca;
using Controlsys.Repositorios.Seguranca;
using Globalsys;
using Globalsys.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Repositorios.Seguranca
{
    /// <summary>
    /// Representa um(a) RepositorioUrlAlteracaoSenha.
    /// </summary>
    public class RepositorioUrlAlteracaoSenha : Repositorio<UrlAlteracaoSenha>, IRepositorioUrlAlteracaoSenha
    {
        /// <summary>
        /// Construtor para
        /// Controlsys.Persistencia.Repositorios.Seguranca.RepositorioUrlAlteracaoSenha.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioUrlAlteracaoSenha(IUnidadeTrabalho unidadeTrabalho) : base(unidadeTrabalho) { }

        /// <summary>
        /// Obter por login.
        /// </summary>
        ///
        /// <param name="login">
        /// O(a) login.
        /// </param>
        /// <param name="urlCrip">
        /// O(a) URL crip.
        /// </param>
        /// <param name="somenteAtivo">
        /// true to somente ativo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) UrlAlteracaoSenha.
        /// </returns>
        public UrlAlteracaoSenha ObterPorLogin(string login, string urlCrip, bool somenteAtivo = true)
        {
            UrlAlteracaoSenha url = ObterTodos().Where(u => u.Usuario.Login == login && u.UrlAlteracao == urlCrip).OrderByDescending(t => t.DataRegistro).FirstOrDefault();
            return somenteAtivo ? (url != null && url.Ativo ? url : null) : url;
        }
    }
}
