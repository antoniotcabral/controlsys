﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Seguranca;
using Controlsys.Repositorios.Seguranca;
using Globalsys;

namespace Controlsys.Persistencia.Repositorios.Seguranca
{
    /// <summary>
    /// Representa um(a) RepositorioGrupoUsuario.
    /// </summary>
    public class RepositorioGrupoUsuario : Repositorio<GrupoUsuario>, IRepositorioGrupoUsuario
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Seguranca.RepositorioGrupoUsuario.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioGrupoUsuario(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {

        }
    }
}
