﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Seguranca;
using Controlsys.Repositorios.Seguranca;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using Controlsys.Dominio.Pessoas;
using NHibernate.Engine;

namespace Controlsys.Persistencia.Repositorios.Seguranca
{
    /// <summary>
    /// Representa um(a) RepositorioUsuario.
    /// </summary>
    public class RepositorioUsuario : Repositorio<Usuario>, IRepositorioUsuario
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Seguranca.RepositorioUsuario.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioUsuario(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
        }

        /// <summary>
        /// Obter por login.
        /// </summary>
        ///
        /// <param name="login">
        /// O(a) login.
        /// </param>
        /// <param name="senha">
        /// O(a) senha.
        /// </param>
        /// <param name="somenteAtivos">
        /// true to somente ativos.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Usuario.
        /// </returns>
        public Usuario ObterPorLogin(string login, string senha = null, bool somenteAtivos = false)
        {
            Usuario usuario = ObterTodos().FirstOrDefault(u => u.Login == login && (senha == null || u.Senha == senha));

            if (!somenteAtivos || usuario == null)
                return usuario;

            var papel = usuario.PessoaFisica.ObterPapel();
            if (papel == null) return (usuario.Ativo) ? usuario : null;

            if (!usuario.Ativo)
                return null;

            return usuario;
        }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        /// <param name="usuario">
        /// O(a) usuario.
        /// </param>
        public void Validar(EstadoObjeto estadoObjeto, Usuario usuario)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                case EstadoObjeto.Alterado:
                    if (Existe(usuario))
                        throw new CoreException("Já existe um usuário com o login/CPF informado.");

                    if (!usuario.Ativo)
                        throw new CoreException("O usuário está inativo, não é possível alterar.");
                    break;
                case EstadoObjeto.Removido:
                    break;
                case EstadoObjeto.Ativado:
                    if (!usuario.Ativo && Existe(usuario))
                        throw new CoreException("Não é possível ativar. Já existe um usuário com o login/CPF informado.");
                    break;
                case EstadoObjeto.Inativado:
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="usuario">
        /// O(a) usuario.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool Existe(Usuario usuario)
        {
            return ObterTodos().Any(u => u.Codigo != usuario.Codigo && u.Login == usuario.Login && u.PessoaFisica.CPF == usuario.PessoaFisica.CPF);
        }

        /// <summary>
        /// Alterar senha.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="login">
        /// O(a) login.
        /// </param>
        /// <param name="senhaNova">
        /// O(a) senha nova.
        /// </param>
        /// <param name="senhaNovaConf">
        /// O(a) senha nova conf.
        /// </param>
        /// <param name="senhaAtual">
        /// O(a) senha atual.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Usuario.
        /// </returns>
        public Usuario AlterarSenha(string login, string senhaNova, string senhaNovaConf, string senhaAtual = null)
        {

            Usuario usuario = ObterPorLogin(login);

            if (usuario.AD)
            { throw new CoreException("Usuário cadastrado no AD, alteração de senha não autorizada."); }

            if (senhaAtual != null)
            {

                senhaAtual = Globalsys.Util.Tools.CriptografarMD5(senhaAtual);

                if (senhaAtual != usuario.Senha)
                { throw new CoreException("Senha atual incorreta. Tente novamente."); }

            }

            if (senhaNova != senhaNovaConf)
            { throw new CoreException("As senhas novas não correspondem. Tente novamente."); }

            if (!Globalsys.Util.Tools.ValidarSenha(senhaNova))
            { throw new CoreException("Senha nova é inválida. Tente novamente."); }

            usuario.Senha = Globalsys.Util.Tools.CriptografarMD5(senhaNova);

            return usuario;
        }

        /// <summary>
        /// Obter por email document.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="email">
        /// O(a) email.
        /// </param>
        /// <param name="cpf">
        /// O(a) cpf.
        /// </param>
        /// <param name="passaporte">
        /// O(a) passaporte.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Usuario.
        /// </returns>
        public Usuario ObterPorEmailDoc(string email, string cpf = null, string passaporte = null)
        {
            Usuario usuario = ObterTodos().FirstOrDefault(u => (u.PessoaFisica.CPF == cpf || u.PessoaFisica.Passaporte == passaporte) && u.PessoaFisica.Email == email && u.Ativo);

            if (usuario == null || !usuario.Ativo)
            { throw new CoreException("Não foi possível concluir a ação, usuário inválido."); }

            if (usuario.AD)
            { throw new CoreException("Contate o setor de TI."); }

            return usuario;
        }

        /// <summary>
        /// Obter por pessoa.
        /// </summary>
        ///
        /// <param name="codigoPessoa">
        /// O(a) codigo pessoa.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Usuario.
        /// </returns>
        public Usuario ObterPorPessoa(string codigoPessoa)
        {
            Usuario usuario = ObterTodos().FirstOrDefault(u => u.PessoaFisica.Codigo == codigoPessoa);
            return usuario;
        }
    }
}
