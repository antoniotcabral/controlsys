﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Seguranca;
using Controlsys.Repositorios.Seguranca;
using Globalsys;

namespace Controlsys.Persistencia.Repositorios.Seguranca
{
    /// <summary>
    /// Representa um(a) RepositorioAcao.
    /// </summary>
    public class RepositorioAcao : Repositorio<Acao>, IRepositorioAcao
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Seguranca.RepositorioAcao.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioAcao(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {

        }

        /// <summary>
        /// Obter por URL.
        /// </summary>
        ///
        /// <param name="url">
        /// URL of the document.
        /// </param>
        ///
        /// <returns>
        /// Um(a) Acao.
        /// </returns>
        public Acao ObterPorUrl(string url)
        {
            return ObterTodos().FirstOrDefault(a => a.Url == url);
        }
    }
}
