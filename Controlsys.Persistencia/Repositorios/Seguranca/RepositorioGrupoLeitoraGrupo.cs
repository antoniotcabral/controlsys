﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Seguranca;
using Controlsys.Repositorios.Seguranca;
using Globalsys;

namespace Controlsys.Persistencia.Repositorios.Seguranca
{
    /// <summary>
    /// Representa um(a) RepositorioGrupoLeitoraGrupo.
    /// </summary>
    public class RepositorioGrupoLeitoraGrupo : Repositorio<GrupoLeitoraGrupo>, IRepositorioGrupoLeitoraGrupo
    {
        /// <summary>
        /// Construtor para
        /// Controlsys.Persistencia.Repositorios.Seguranca.RepositorioGrupoLeitoraGrupo.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioGrupoLeitoraGrupo(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
        }

    }
}
