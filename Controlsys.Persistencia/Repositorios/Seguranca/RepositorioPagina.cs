﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Seguranca;
using Controlsys.Repositorios.Seguranca;
using Globalsys;
using NHibernate.Linq;

namespace Controlsys.Persistencia.Repositorios.Seguranca
{
    /// <summary>
    /// Representa um(a) RepositorioPagina.
    /// </summary>
    public class RepositorioPagina : Repositorio<Pagina>, IRepositorioPagina
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Seguranca.RepositorioPagina.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioPagina(IUnidadeTrabalho unidadeTrabalho) :
            base(unidadeTrabalho)
        {
        }        
    }
}
