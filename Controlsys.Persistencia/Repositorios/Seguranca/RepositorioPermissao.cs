﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Seguranca;
using Controlsys.Repositorios.Seguranca;
using Globalsys;

namespace Controlsys.Persistencia.Repositorios.Seguranca
{
    /// <summary>
    /// Representa um(a) RepositorioPermissao.
    /// </summary>
    public class RepositorioPermissao : Repositorio<Permissao>, IRepositorioPermissao
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Seguranca.RepositorioPermissao.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioPermissao(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {

        }

        /// <summary>
        /// Possui permissao pagina.
        /// </summary>
        ///
        /// <param name="usuarioLogado">
        /// O(a) usuario logado.
        /// </param>
        /// <param name="url">
        /// URL of the document.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool PossuiPermissaoPagina(string usuarioLogado, string url)
        {
            RepositorioUsuario repUsuario = new RepositorioUsuario(base.UnidadeTrabalho);

            Usuario usuario = repUsuario.ObterPorLogin(usuarioLogado);

            return ObterTodos().Any(p => p.Acao.Pagina.Url == url
                                      && p.Grupo.GruposUsuarios.Any(gu => gu.Usuario.Codigo == usuario.Codigo)
                                      && p.Ativo);
        }

        /// <summary>
        /// Possui permissao.
        /// </summary>
        ///
        /// <param name="usuarioLogado">
        /// O(a) usuario logado.
        /// </param>
        /// <param name="url">
        /// URL of the document.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool PossuiPermissao(string usuarioLogado, string url)
        {
            Usuario usuario = new RepositorioUsuario(base.UnidadeTrabalho).ObterPorLogin(usuarioLogado);

            return ObterTodos().Any(p => p.Acao.Url == url
                                      && p.Grupo.GruposUsuarios.Any(gu => gu.Usuario.Codigo == usuario.Codigo)
                                      && p.Ativo);
        }

        /// <summary>
        /// Obter acoes usuario.
        /// </summary>
        ///
        /// <param name="usuarioLogado">
        /// O(a) usuario logado.
        /// </param>
        /// <param name="codigoModulo">
        /// O(a) codigo modulo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) IQueryable&lt;Acao&gt;
        /// </returns>
        public IQueryable<Acao> ObterAcoesUsuario(string usuarioLogado, int? codigoModulo = null)
        {
            Usuario usuario = new RepositorioUsuario(UnidadeTrabalho).ObterPorLogin(usuarioLogado);
            RepositorioAcao repAcao = new RepositorioAcao(UnidadeTrabalho);

            IQueryable<Acao> acoes = repAcao.ObterTodos();

            if (codigoModulo.HasValue)
                acoes = acoes.Where(a => a.Pagina.PaginaPai.PaginaPai.Codigo == codigoModulo);

            IQueryable<Permissao> permissoes = ObterTodos();

            permissoes = permissoes.Where(p => p.Ativo && p.Grupo.Ativo && p.Grupo.GruposUsuarios.Any(gu => gu.Usuario.Codigo == usuario.Codigo && gu.Ativo));

            acoes = acoes.Where(a => permissoes.Any(per => per.Acao.Codigo == a.Codigo));

            return acoes;
        }
    }
}
