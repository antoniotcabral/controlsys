﻿using Controlsys.Dominio.Acesso;
using Controlsys.Repositorios.Acesso;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Repositorios.Acesso
{
    /// <summary>
    /// Representa um(a) RepositorioLeitora.
    /// </summary>
    public class RepositorioLeitora: Repositorio<Leitora>, IRepositorioLeitora
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Acesso.RepositorioLeitora.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioLeitora(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho) {}

        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="leitora">
        /// O(a) leitora.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool Existe(Leitora leitora)
        {
            return ObterTodos().Any(s => s.IDLeitora.ToUpper() == leitora.IDLeitora.ToUpper() && s.Ativo && s.Codigo != leitora.Codigo);
        }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        public void Validar(Leitora entidade, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                  if (entidade.IDLeitora == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe uma leitora com o ID informado.");
                    }
                    break;
                case EstadoObjeto.Alterado:
                     if(!entidade.Ativo)
                    {
                        throw new CoreException("A leitora está inativa, não é possível alterar");
                    }
                     if (entidade.IDLeitora == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe uma leitora com o ID informado.");
                    }
                    break;
                case EstadoObjeto.Ativado:
                    if (!entidade.Ativo && Existe(entidade))
                    {
                        throw new CoreException("Já existe uma leitora com o ID informado.");
                    }
                    break;
                default:
                    break;
            }
        
        }
    }
}
