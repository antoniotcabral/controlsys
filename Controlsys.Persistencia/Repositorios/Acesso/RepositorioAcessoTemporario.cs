﻿using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Pessoas;
using Controlsys.Repositorio.Acesso;
using Globalsys;
using Globalsys.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Controlsys.Persistencia.Repositorios.Acesso
{
    /// <summary>
    /// Representa um(a) RepositorioAcessoTemporario.
    /// </summary>
    public class RepositorioAcessoTemporario : Repositorio<AcessoTemporario>, IRepositorioAcessoTemporario
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Acesso.RepositorioAcessoTemporario.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioAcessoTemporario(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho) { }

        public void Validar(AcessoTemporario entidade, Globalsys.Validacao.EstadoObjeto estadoObjeto)
        {
            if (entidade.DataHoraValidadeInicio >= entidade.DataHoraValidadeFim)
                throw new CoreException("Data de Validade Inicial deve ser menor que a final");

            if (entidade.Papel.ObterLogPapel().Status != StatusPapelLog.Ativo)
                throw new CoreException("O Papel da Pessoa selecionada não está ativo.");

            if (!entidade.GrupoLeitora.Ativo)
                throw new CoreException("O Grupo de Leitoras selecionado não está ativo.");
        }
    }
}
