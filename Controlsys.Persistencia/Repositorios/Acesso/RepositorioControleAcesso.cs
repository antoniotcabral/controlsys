﻿using System.CodeDom;
using System.Security.Cryptography.X509Certificates;
using System.Windows.Forms;
using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using Controlsys.Persistencia.Repositorios.Parametros;
using Controlsys.Persistencia.Repositorios.Pessoas;
using Controlsys.Repositorio.Acesso;
using Controlsys.Repositorios.Pessoas;
using FluentNHibernate.Utils;
using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Criterion;
using NHibernate.Hql.Ast.ANTLR;
using NHibernate.Hql.Ast.ANTLR.Tree;
using NHibernate.Linq;
using Controlsys.Persistencia.Repositorios.Seguranca;
using Controlsys.Dominio.Seguranca;
using Controlsys.Dominio.Empresas;
using NHibernate.Mapping;

namespace Controlsys.Persistencia.Repositorios.Acesso
{
    /// <summary>
    /// Representa um(a) RepositorioControleAcesso.
    /// </summary>
    public class RepositorioControleAcesso : Repositorio<ControleAcesso>, IRepositorioControleAcesso
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Acesso.RepositorioControleAcesso.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioControleAcesso(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho) { }

        private class EmpregadoSAPModelView
        {
            /// <summary>
            /// Gets/Sets valor para Codigo.
            /// </summary>
            ///
            /// <value>
            /// Retorna o(a) Codigo.
            /// </value>
            public long Codigo { get; set; }

            /// <summary>
            /// Gets/Sets valor para GestorPonto.
            /// </summary>
            ///
            /// <value>
            /// Retorna o(a) GestorPonto.
            /// </value>
            public long GestorPonto { get; set; }

            /// <summary>
            /// Gets/Sets valor para SuperiorImediato.
            /// </summary>
            ///
            /// <value>
            /// Retorna o(a) SuperiorImediato.
            /// </value>
            public long SuperiorImediato { get; set; }

        }

        private List<EmpregadoSAP> ObterEmpregadosRelacioados(EmpregadoSAP regPai)
        {
            List<EmpregadoSAP> subordinados = new List<EmpregadoSAP>();
            if (regPai != null)
            {                
                //subordinados = ObterSubordinados(regPai, subordinados);
                int pRegPai = (int)regPai.Codigo;

                subordinados = ObterSubordinadoPro(pRegPai);
            }

            return subordinados;
        }

        private List<EmpregadoSAP> ObterSubordinadoPro(int empregadoPai)
        {
            Dictionary<string, object> parametros = new Dictionary<string, object>();
            parametros.Add("codigo", empregadoPai);

            var empregadosSAPModelViewSubordinados = UnidadeTrabalho.ExecuteSqlHHT<EmpregadoSAPModelView>("EXEC sp_ObterSubordinados :codigo", parametros);

            //var empregadoSAPModelView = UnidadeTrabalho.ExecuteSqlHHT<EmpregadoSAPModelView>("EXEC sp_ObterSubordinados :codigo", parametros);
            //List<EmpregadoSAP> listaEmpregadoSAP = 

            var empregadosSubordinados = empregadosSAPModelViewSubordinados.Select(td => new EmpregadoSAP
            {
                Codigo = td.Codigo,
                GestorPonto = new EmpregadoSAP { Codigo = td.GestorPonto },
                SuperiorImediato = new EmpregadoSAP { Codigo = td.SuperiorImediato }
            });

            return empregadosSubordinados.ToList();
        }

        private List<EmpregadoSAP> ObterSubordinados(EmpregadoSAP empregadoPai, List<EmpregadoSAP> empregadosSubordinados)
        {
            Repositorio<EmpregadoSAP> repEmpSap = new Repositorio<EmpregadoSAP>(UnidadeTrabalho);
            IQueryable<EmpregadoSAP> regsFilho = repEmpSap.ObterTodos().Where(es => es.GestorPonto.Codigo == empregadoPai.Codigo || es.SuperiorImediato.Codigo == empregadoPai.Codigo);

            if (regsFilho.Any())
            {
                List<EmpregadoSAP> listAux = regsFilho.ToList();
                listAux = listAux.Where(f => !empregadosSubordinados.Any(e => e.Codigo == f.Codigo)).ToList();
                
                empregadosSubordinados.AddRange(listAux.Select(r => r).ToList());
                empregadosSubordinados = empregadosSubordinados.Distinct().ToList();

                //empregadosSubordinados.AddRange(regsFilho.Where(g => g.GestorPonto.Codigo == empregadoPai.Codigo).Select(r => r.GestorPonto).ToList());
                //empregadosSubordinados.AddRange(regsFilho.Where(g => g.SuperiorImediato.Codigo == empregadoPai.Codigo).Select(r => r.SuperiorImediato).ToList());
                foreach (EmpregadoSAP empregadoSap in listAux)
                {
                    ObterSubordinados(empregadoSap, empregadosSubordinados);
                }
            }

            return empregadosSubordinados;
        }

        /// <summary>
        /// Obter lista acesso.
        /// </summary>
        ///
        /// <param name="status">
        /// O(a) status.
        /// </param>
        /// <param name="cracha">
        /// O(a) cracha.
        /// </param>
        /// <param name="cpf">
        /// O(a) cpfPassaporte.
        /// </param>
        /// <param name="empresa">
        /// O(a) empresa.
        /// </param>
        /// <param name="nome">
        /// O(a) nome.
        /// </param>
        /// <param name="cargo">
        /// O(a) cargo.
        /// </param>
        /// <param name="dataInicio">
        /// O(a) data inicio.
        /// </param>
        /// <param name="dataFim">
        /// O(a) data fim.
        /// </param>
        /// <param name="userLogado">
        /// O(a) user logado.
        /// </param>
        ///
        /// <returns>
        /// Um(a) IQueryable&lt;ControleAcesso&gt;
        /// </returns>
        public IQueryable<ControleAcesso> ObterListaAcesso(List<StatusAcesso> status, string cracha, string cpf, string empresa, string nome, int cargo, DateTime? dataInicio, DateTime? dataFim, string userLogado, List<int> leitorasSelecionadas = null, IQueryable<AlocacaoColaborador> aloc = null, bool validaGestor = false, long? empregadoSap = null, long? gestorPonto = null, long? superiorImediato = null, string codigoPht = null, IList<string> localLeitora = null, string passaporte = null, string maodeobra = null, string fornecedor = null, string contratada = null)
        {
            RepositorioGrupoTrabalhoColab repGrpTrabColab = new RepositorioGrupoTrabalhoColab(UnidadeTrabalho);
            RepositorioPrestadorServico repPrest = new RepositorioPrestadorServico(UnidadeTrabalho);
            RepositorioColaborador repColaborador = new RepositorioColaborador(UnidadeTrabalho);
            RepositorioVisitante repVisitante = new RepositorioVisitante(UnidadeTrabalho);
            RepositorioUsuario repUsuario = new RepositorioUsuario(UnidadeTrabalho);

            Usuario usuario = repUsuario.ObterPorLogin(userLogado);
            int[] leitoras = null;
            if (leitorasSelecionadas == null)
                leitoras = usuario.GruposUsuarios
                                    .SelectMany(gu => gu.Grupo.GruposLeitoras.Select(gr => gr.GrupoLeitora))
                                    .SelectMany(gl => gl.Leitoras.Select(lgl => lgl.Leitora.Codigo))
                                    .Distinct()
                                    .ToArray();
            else
                leitoras = leitorasSelecionadas.ToArray();

            IQueryable<ControleAcesso> controleAcesso = ObterTodos();
            
            //if (validaGestor && usuario != null)
            //{
            //    var acoes = usuario.GruposUsuarios.Where(g => g.Ativo && g.Grupo.Ativo).Select(g => g.Grupo).SelectMany(g => g.Acoes);
            //    if ((acoes.Where(a => a.Ativo && a.Url == "/RelatorioJournal/FiltrarEmpregadoSAP")).Any())
            //    {
            //        var papel = usuario.PessoaFisica.ObterPapel(true);
            //        var subordinados = ObterEmpregadosRelacioados(papel.EmpregadoSAP);
            //        List<long> codsSubordinados = subordinados.Select(e => e.Codigo).ToList().Distinct().ToList();
            //        controleAcesso = controleAcesso.Where(ca => (ca.Papel.EmpregadoSAP != null) && (
            //                (codsSubordinados.Contains(ca.Papel.EmpregadoSAP.Codigo)) ||
            //                (ca.Papel.EmpregadoSAP.GestorPonto != null && codsSubordinados.Contains(ca.Papel.EmpregadoSAP.GestorPonto.Codigo)) ||
            //                (ca.Papel.EmpregadoSAP.SuperiorImediato != null && codsSubordinados.Contains(ca.Papel.EmpregadoSAP.SuperiorImediato.Codigo))
            //            )
            //        );
            //    }
            //}  
           
            if (!string.IsNullOrEmpty(cpf))
                controleAcesso = controleAcesso.Where(pf => pf.Papel.PessoaFisica.CPF == cpf);

            if (!string.IsNullOrEmpty(passaporte))
                controleAcesso = controleAcesso.Where(pf => pf.Papel.PessoaFisica.Passaporte == passaporte);

            if (!string.IsNullOrEmpty(cracha))
                controleAcesso = controleAcesso.Where(pf => pf.Cracha.Numero == cracha || pf.NumeroSerie == cracha);

            if (!string.IsNullOrEmpty(nome))
                controleAcesso = controleAcesso.Where(pf => pf.Papel.PessoaFisica.Nome.Contains(nome));

            if (status != null && status.Any())
                controleAcesso = controleAcesso.Where(t => status.Contains(t.StatusAcesso));

            if (dataInicio.HasValue)
                controleAcesso = controleAcesso.Where(ca => ca.DataAcesso >= dataInicio.Value);

            if (dataFim.HasValue)
            {
                dataFim = dataFim.Value.AddDays(1);
                controleAcesso = controleAcesso.Where(ca => ca.DataAcesso < dataFim.Value);
            }

            if (cargo > 0)
                controleAcesso = controleAcesso.Where(pf => pf.Papel.Cargo.Codigo == cargo);

            if (aloc != null)
                controleAcesso = controleAcesso.Where(k => aloc.Any(a => a.Papel.Codigo == k.Papel.Codigo)); 

            if (empregadoSap != null)
                controleAcesso = controleAcesso.Where(ca => ca.Papel.EmpregadoSAP != null && ca.Papel.EmpregadoSAP.Codigo == empregadoSap);

            if (gestorPonto != null)
                controleAcesso = controleAcesso.Where(ca => ca.Papel.EmpregadoSAP != null && ca.Papel.EmpregadoSAP.GestorPonto != null &&
                                 ca.Papel.EmpregadoSAP.GestorPonto.Codigo == gestorPonto);

            if (superiorImediato != null)
                controleAcesso = controleAcesso.Where(ca => ca.Papel.EmpregadoSAP != null && ca.Papel.EmpregadoSAP.SuperiorImediato != null &&
                                 ca.Papel.EmpregadoSAP.SuperiorImediato.Codigo == superiorImediato);

            if (!string.IsNullOrEmpty(codigoPht))
                controleAcesso = controleAcesso.Where(ca => repGrpTrabColab.ObterTodos().Any(gtb => gtb.Papel.Codigo.Equals(ca.Papel.Codigo) && gtb.GrupoTrabalho.CodigoPHT.Equals(codigoPht)));

            if (localLeitora != null && localLeitora.Any())
            {
                controleAcesso = controleAcesso.Where(ca => localLeitora.Contains(ca.Leitora.Local));
            }

            if ((!string.IsNullOrEmpty(empresa)) || (!string.IsNullOrEmpty(fornecedor)) || (!string.IsNullOrEmpty(maodeobra)))
            {               
                var colaboradores = repColaborador.ObterTodos();
                var prestadores = repPrest.ObterTodos();
                var visitantes = repVisitante.ObterTodos();

                if (!string.IsNullOrEmpty(empresa))
                {
                    colaboradores = colaboradores.Where(p => p.Empresa.Nome == empresa);
                    prestadores = prestadores.Where(p => p.Empresa.Nome == empresa);
                    visitantes = visitantes.Where(p => p.Empresa == empresa);
                }

                if (!string.IsNullOrEmpty(maodeobra))
                {
                    colaboradores = colaboradores.Where(p => p.MaoDeObra.ToString() == maodeobra);
                    prestadores = prestadores.Where(p => p.MaoDeObra.ToString() == maodeobra);
                    visitantes = Enumerable.Empty<Visitante>().AsQueryable();
                }

                if (!string.IsNullOrEmpty(fornecedor))
                {
                    colaboradores = Enumerable.Empty<Colaborador>().AsQueryable();
                    prestadores = prestadores.Where(p => p.Fornecedor.Codigo == Convert.ToInt32(fornecedor));
                    visitantes = Enumerable.Empty<Visitante>().AsQueryable();
                }                
                
                List<int> codColab = new List<int>();
                List<int> codPres = new List<int>();
                List<int> codVis = new List<int>();

                codColab = colaboradores.Select(c => c.Codigo).ToList();
                codPres = prestadores.Select(c => c.Codigo).ToList();
                codVis = visitantes.Select(c => c.Codigo).ToList();

                codColab = codColab.Union(codPres).Union(codVis).ToList();
                int qtdcolab = codColab.Count;

                List<ControleAcesso> listaControleAcesso = new List<ControleAcesso>();

                for (int i = 0; i < qtdcolab; i += 1800)
                {
                    var page = codColab.Skip(i).Take(1800).ToArray();
                    listaControleAcesso.AddRange(controleAcesso.Where(c => page.Contains(c.Papel.Codigo)));
                }

                controleAcesso = listaControleAcesso.AsQueryable();
            }            

            if (gestorPonto != null)
                controleAcesso = controleAcesso.Where(ca => ca.Papel.EmpregadoSAP != null && ca.Papel.EmpregadoSAP.GestorPonto != null &&
                                 ca.Papel.EmpregadoSAP.GestorPonto.Codigo == gestorPonto);

            controleAcesso = controleAcesso.Where(ca => leitoras.Contains(ca.Leitora.Codigo));


            if (validaGestor && usuario != null)
            {
                var acoes = usuario.GruposUsuarios.Where(g => g.Ativo && g.Grupo.Ativo).Select(g => g.Grupo).SelectMany(g => g.Acoes);
                if ((acoes.Where(a => a.Ativo && a.Url == "/RelatorioJournal/FiltrarEmpregadoSAP")).Any())
                {

                    var papel = usuario.PessoaFisica.ObterPapel(true);
                    var subordinados = ObterEmpregadosRelacioados(papel.EmpregadoSAP);
                    List<long> codsSubordinados = subordinados.Select(e => e.Codigo).Distinct().ToList();

                    var controleAcessoTemp = new List<ControleAcesso>();
                    for (int i = 0; i < codsSubordinados.Count; i += 500)
                    {
                        var codsTemp = codsSubordinados.Skip(i).Take(500).ToArray();
                        controleAcessoTemp.AddRange(controleAcesso.Where(ca => (ca.Papel.EmpregadoSAP != null) && (
                            (codsTemp.Contains(ca.Papel.EmpregadoSAP.Codigo)) ||
                            (ca.Papel.EmpregadoSAP.GestorPonto != null && codsTemp.Contains(ca.Papel.EmpregadoSAP.GestorPonto.Codigo)) ||
                            (ca.Papel.EmpregadoSAP.SuperiorImediato != null && codsTemp.Contains(ca.Papel.EmpregadoSAP.SuperiorImediato.Codigo))
                           )
                        ));
                    }
                    controleAcesso = controleAcessoTemp.Distinct().AsQueryable();

                    //controleAcesso = controleAcesso.Where(ca => (ca.Papel.EmpregadoSAP != null) && (
                    //        (codsSubordinados.Contains(ca.Papel.EmpregadoSAP.Codigo)) ||
                    //        (ca.Papel.EmpregadoSAP.GestorPonto != null && codsSubordinados.Contains(ca.Papel.EmpregadoSAP.GestorPonto.Codigo)) ||
                    //        (ca.Papel.EmpregadoSAP.SuperiorImediato != null && codsSubordinados.Contains(ca.Papel.EmpregadoSAP.SuperiorImediato.Codigo))
                    //    )
                    //);
                }
            }

            return controleAcesso;

        }

        public IQueryable<ControleAcesso> ObterListaAcesso(string nome = null, string cpf = null, string passaporte = null, string empresa = null, string cnpj = null, TipoPapel? tipoPapel = null, DateTime? dataInicio = null, DateTime? dataFim = null, long? empregadoSap = null, long? gestorPonto = null, long? superiorImediato = null, string codigoPht = null, IList<string> localLeitora = null)
        {
            RepositorioPrestadorServico repPrest = new RepositorioPrestadorServico(UnidadeTrabalho);
            RepositorioColaborador repColaborador = new RepositorioColaborador(UnidadeTrabalho);
            RepositorioVisitante repVisitante = new RepositorioVisitante(UnidadeTrabalho);
            RepositorioGrupoLeitora repGrupoLeitora = new RepositorioGrupoLeitora(UnidadeTrabalho);

            IQueryable<GrupoLeitora> gruposLeitora = repGrupoLeitora.ObterTodos();
            gruposLeitora = gruposLeitora.Where(gl => gl.Externa == true);
            var listLeitGrupLeito = gruposLeitora.Select(gl => gl.Leitoras);
            var listGrpLeit = new List<int>();
            foreach (var leitGrupLeito in listLeitGrupLeito)
            {
                listGrpLeit.AddRange(leitGrupLeito.Select(x => x.Leitora.Codigo));
            }

            IQueryable<ControleAcesso> controleAcesso = ObterTodos();
            controleAcesso = controleAcesso.Where(ca => listGrpLeit.Contains(ca.Leitora.Codigo));
            controleAcesso = controleAcesso.Where(ca => ca.Papel != null);
            controleAcesso = controleAcesso.Where(ca => ca.StatusAcesso == StatusAcesso.OCO_UR_ENTRADA_COMPLETA ||
                                                        ca.StatusAcesso == StatusAcesso.OCO_UR_SAIDA_COMPLETA ||
                                                        ca.StatusAcesso == StatusAcesso.OCO_UR_ACESSO_LIBERADO);

            if (!string.IsNullOrEmpty(nome))
                controleAcesso = controleAcesso.Where(pf => pf.Papel.PessoaFisica.Nome.Contains(nome));

            if (!string.IsNullOrEmpty(cpf))
            {
                cpf = cpf.Replace(".", "").Replace("-", "");
                controleAcesso = controleAcesso.Where(pf => pf.Papel.PessoaFisica.CPF == cpf);
            }

            if (!string.IsNullOrEmpty(passaporte))
                controleAcesso = controleAcesso.Where(pf => pf.Papel.PessoaFisica.Passaporte == passaporte);

            if (empregadoSap != null && empregadoSap > 0)
                controleAcesso = controleAcesso.Where(ca => ca.Papel.EmpregadoSAP.Codigo.Equals(empregadoSap));

            if (gestorPonto != null && gestorPonto > 0)
                controleAcesso = controleAcesso.Where(ca => ca.Papel.EmpregadoSAP != null && ca.Papel.EmpregadoSAP.GestorPonto != null &&
                                 ca.Papel.EmpregadoSAP.GestorPonto.Codigo.Equals(gestorPonto));

            if (superiorImediato != null && superiorImediato > 0)
                controleAcesso = controleAcesso.Where(ca => ca.Papel.EmpregadoSAP != null && ca.Papel.EmpregadoSAP.SuperiorImediato != null &&
                                 ca.Papel.EmpregadoSAP.SuperiorImediato.Codigo.Equals(superiorImediato));

            if(localLeitora != null && localLeitora.Any())
            {
                controleAcesso = controleAcesso.Where(ca => localLeitora.Contains(ca.Leitora.Local));
            }

            if (tipoPapel != null)
            {
                switch (tipoPapel)
                {
                    case TipoPapel.Colaborador:
                        var colabs = repColaborador.ObterTodos();
                        controleAcesso = controleAcesso.Where(pf => colabs.Select(c => c.Codigo).Contains(pf.Papel.Codigo));
                        break;
                    case TipoPapel.PrestadorServico:
                        var prests = repPrest.ObterTodos();
                        controleAcesso = controleAcesso.Where(pf => prests.Select(c => c.Codigo).Contains(pf.Papel.Codigo));
                        break;
                    case TipoPapel.Visitante:
                        var visits = repVisitante.ObterTodos();
                        controleAcesso = controleAcesso.Where(pf => visits.Select(c => c.Codigo).Contains(pf.Papel.Codigo));
                        break;
                }
            }

            if (dataInicio.HasValue)
                controleAcesso = controleAcesso.Where(ca => ca.DataAcesso >= dataInicio.Value);

            if (dataFim.HasValue)
                controleAcesso = controleAcesso.Where(ca => ca.DataAcesso < dataFim.Value);
            //if (dataFim.HasValue)
            //{
            //    dataFim = dataFim.Value.AddDays(1);
            //    controleAcesso = controleAcesso.Where(ca => ca.DataAcesso < dataFim.Value);
            //}

            if (!string.IsNullOrEmpty(empresa))
            {
                List<int> codColab = repColaborador.ObterTodos().Where(p => p.Empresa.Nome == empresa).Select(c => c.Codigo).ToList();
                List<int> codPres = repPrest.ObterTodos().Where(p => p.Empresa.Nome == empresa).Select(c => c.Codigo).ToList();
                List<int> codVis = repVisitante.ObterTodos().Where(p => p.Empresa == empresa).Select(c => c.Codigo).ToList();

                codColab = codColab.Union(codPres).Union(codVis).ToList();
                int qtdcolab = codColab.Count;

                List<ControleAcesso> listaControleAcesso = new List<ControleAcesso>();

                for (int i = 0; i < qtdcolab; i += 1800)
                {
                    var page = codColab.Skip(i).Take(1800).ToArray();
                    listaControleAcesso.AddRange(controleAcesso.Where(c => page.Contains(c.Papel.Codigo)));
                }

                controleAcesso = listaControleAcesso.AsQueryable();
            }

            if (!string.IsNullOrEmpty(cnpj))
            {
                cnpj = cnpj.Replace(".", "").Replace("/", "").Replace("-", "");

                List<int> codColab = repColaborador.ObterTodos().Where(p => p.Empresa.CNPJ == cnpj).Select(c => c.Codigo).ToList();
                List<int> codPres = repPrest.ObterTodos().Where(p => p.Empresa.CNPJ == cnpj).Select(c => c.Codigo).ToList();

                codColab = codColab.Union(codPres).ToList();
                int qtdcolab = codColab.Count;

                List<ControleAcesso> listaControleAcesso = new List<ControleAcesso>();

                for (int i = 0; i < qtdcolab; i += 2000)
                {
                    var page = codColab.Skip(i).Take(2000).ToArray();
                    listaControleAcesso.AddRange(controleAcesso.Where(c => page.Contains(c.Papel.Codigo)));
                }

                controleAcesso = listaControleAcesso.AsQueryable();
            }

            //controleAcesso = controleAcesso.OrderBy(c => c.Papel.Codigo).ThenBy(c=> c.Codigo);

            return controleAcesso;

        }

        public IQueryable<ControleAcesso> ObterListaAcessoRestaurantePorRefeicao(string nome = null, string cracha = null, string cpfPassaporte = null, string empresa = null, int restaurante = 0, int tipoAlimentacao = 0, int setor = 0, DateTime? dataInicio = null, DateTime? dataFim = null, IQueryable<AlocacaoColaborador> aloc = null)
        {
            RepositorioPrestadorServico repPrest = new RepositorioPrestadorServico(UnidadeTrabalho);
            RepositorioColaborador repColaborador = new RepositorioColaborador(UnidadeTrabalho);
            RepositorioVisitante repVisitante = new RepositorioVisitante(UnidadeTrabalho);
            RepositorioRestaurante repRestaurante = new RepositorioRestaurante(UnidadeTrabalho);
            RepositorioTipoAlimentacao repTipoAlimentacao = new RepositorioTipoAlimentacao(UnidadeTrabalho);
            Repositorio<TipoAlimentacaoRestaurante> repTipoAlimentacaoRestaurante = new Repositorio<TipoAlimentacaoRestaurante>(UnidadeTrabalho);

            IQueryable<ControleAcesso> controleAcesso = ObterTodos();

             #region [Pesquisando os acessos relacionados aos restaurantes]
            var leitorasCods = repRestaurante.ObterTodos().Select(r => r.GrupoLeitora.Leitoras.Select(l => l.Leitora.Codigo));
            List<int> leitorasCodsAux = new List<int>();
            foreach (var leitorasCod in leitorasCods)
            {
                leitorasCodsAux.AddRange(leitorasCod);
            }
            leitorasCodsAux = leitorasCodsAux.Distinct().ToList();
            controleAcesso = controleAcesso.Where(ca => leitorasCodsAux.Contains(ca.Leitora.Codigo));
            controleAcesso = controleAcesso.Where(ca => (ca.StatusAcesso == StatusAcesso.OCO_UR_ACESSO_LIBERADO) ||
                                                        (ca.StatusAcesso == StatusAcesso.OCO_UR_ENTRADA_COMPLETA));
            #endregion

            if (dataInicio.HasValue)
                controleAcesso = controleAcesso.Where(ca => ca.DataAcesso >= dataInicio.Value);

            if (dataFim.HasValue)
            {
                dataFim = dataFim.Value.AddDays(1);
                controleAcesso = controleAcesso.Where(ca => ca.DataAcesso < dataFim.Value);
            }

            if (!string.IsNullOrEmpty(nome))
                controleAcesso = controleAcesso.Where(ca => ca.Papel.PessoaFisica.Nome.Contains(nome));

            if (!string.IsNullOrEmpty(cracha))
                controleAcesso = controleAcesso.Where(pf => pf.Cracha.Numero == cracha || pf.NumeroSerie == cracha);

            if (!string.IsNullOrEmpty(cpfPassaporte))
                controleAcesso = controleAcesso.Where(pf => pf.Papel.PessoaFisica.CPF == cpfPassaporte || pf.Papel.PessoaFisica.Passaporte == cpfPassaporte);

            List<int> codColab = !string.IsNullOrEmpty(empresa) ? repColaborador.ObterTodos().Where(p => p.Empresa.Nome == empresa).Select(c => c.Codigo).ToList()
                                                            : repColaborador.ObterTodos().Where(p => p.Empresa.Nome != empresa).Select(c => c.Codigo).ToList();

            List<int> codPres = !string.IsNullOrEmpty(empresa) ? repPrest.ObterTodos().Where(p => p.Empresa.Nome == empresa).Select(c => c.Codigo).ToList()
                                                                : repPrest.ObterTodos().Where(p => p.Empresa.Nome != empresa).Select(c => c.Codigo).ToList();

            List<int> codVis = !string.IsNullOrEmpty(empresa) ? repVisitante.ObterTodos().Where(p => p.Empresa == empresa).Select(c => c.Codigo).ToList()
                                                                : repVisitante.ObterTodos().Where(p => p.Empresa != empresa).Select(c => c.Codigo).ToList();

            codColab = codColab.Union(codPres).Union(codVis).ToList();
            int qtdcolab = codColab.Count;

            List<ControleAcesso> listaControleAcesso = new List<ControleAcesso>();

            for (int i = 0; i < qtdcolab; i += 2000)
            {
                var page = codColab.Skip(i).Take(2000).ToArray();
                listaControleAcesso.AddRange(controleAcesso.Where(c => page.Contains(c.Papel.Codigo)));
            }

            controleAcesso = listaControleAcesso.AsQueryable();

            if (restaurante > 0)
            {
                Restaurante restauranteAux = repRestaurante.ObterTodos().FirstOrDefault(r => r.Codigo == restaurante);
                if (restauranteAux != null)
                {
                    var leitoras = restauranteAux.GrupoLeitora.Leitoras.Where(lgl => lgl.Ativo).Select(l => l.Leitora.Codigo).ToArray();
                    controleAcesso = controleAcesso.Where(ca => leitoras.Contains(ca.Leitora.Codigo));
                }
            }


            //Setor de custo
            if (aloc != null)
                controleAcesso = controleAcesso.Where(k => aloc.Any(a => a.Papel.Codigo == k.Papel.Codigo));

            //if (tipoAlimentacao > 0)
            //{

                Dictionary<string, object> parametros = new Dictionary<string, object>();
                parametros.Add("tipoAlimentacao", tipoAlimentacao);
                parametros.Add("dataInicio", dataInicio);
                parametros.Add("dataFim", dataFim);

                var acessosRest = UnidadeTrabalho.ExecuteSqlHHT<AcessoRestauranteView>("EXEC sp_obtem_acessos_tipoAlimentacao :tipoAlimentacao, :dataInicio, :dataFim ", parametros);

                if(acessosRest != null) controleAcesso = controleAcesso.Where(ca => acessosRest.Any(p => p.Codigo == ca.Codigo));
            //}
            return controleAcesso;
        }

        public IQueryable<ControleAcesso> ObterListaAcessoAlertaTempoPermanencia(TipoPapel? tipoPapel = null, DateTime? dataInicio = null, DateTime? dataFim = null, IList<string> localLeitora = null)
        {
            RepositorioPrestadorServico repPrest = new RepositorioPrestadorServico(UnidadeTrabalho);
            RepositorioColaborador repColaborador = new RepositorioColaborador(UnidadeTrabalho);
            RepositorioVisitante repVisitante = new RepositorioVisitante(UnidadeTrabalho);
            RepositorioGrupoLeitora repGrupoLeitora = new RepositorioGrupoLeitora(UnidadeTrabalho);

            IQueryable<GrupoLeitora> gruposLeitora = repGrupoLeitora.ObterTodos();
            gruposLeitora = gruposLeitora.Where(gl => gl.Externa == true);
            var listLeitGrupLeito = gruposLeitora.Select(gl => gl.Leitoras);
            var listGrpLeit = new List<int>();
            foreach (var leitGrupLeito in listLeitGrupLeito)
            {
                listGrpLeit.AddRange(leitGrupLeito.Select(x => x.Leitora.Codigo));
            }

            IQueryable<ControleAcesso> controleAcesso = ObterTodos();
            controleAcesso = controleAcesso.Where(ca => listGrpLeit.Contains(ca.Leitora.Codigo));
            controleAcesso = controleAcesso.Where(ca => ca.Papel != null);
            controleAcesso = controleAcesso.Where(ca => ca.StatusAcesso == StatusAcesso.OCO_UR_ENTRADA_COMPLETA ||
                                                        ca.StatusAcesso == StatusAcesso.OCO_UR_SAIDA_COMPLETA ||
                                                        ca.StatusAcesso == StatusAcesso.OCO_UR_ACESSO_LIBERADO);

            if (localLeitora != null && localLeitora.Any())
            {
                controleAcesso = controleAcesso.Where(ca => localLeitora.Contains(ca.Leitora.Local));
            }

            if (tipoPapel != null)
            {
                switch (tipoPapel)
                {
                    case TipoPapel.Colaborador:
                        var colabs = repColaborador.ObterTodos();
                        controleAcesso = controleAcesso.Where(pf => colabs.Select(c => c.Codigo).Contains(pf.Papel.Codigo));
                        break;
                    case TipoPapel.PrestadorServico:
                        var prests = repPrest.ObterTodos();
                        controleAcesso = controleAcesso.Where(pf => prests.Select(c => c.Codigo).Contains(pf.Papel.Codigo));
                        break;
                    case TipoPapel.Visitante:
                        var visits = repVisitante.ObterTodos();
                        controleAcesso = controleAcesso.Where(pf => visits.Select(c => c.Codigo).Contains(pf.Papel.Codigo));
                        break;
                }
            }

            if (dataInicio.HasValue)
                controleAcesso = controleAcesso.Where(ca => ca.DataAcesso >= dataInicio.Value);

            if (dataFim.HasValue)
                controleAcesso = controleAcesso.Where(ca => ca.DataAcesso < dataFim.Value);

            return controleAcesso;
        }

    }
}