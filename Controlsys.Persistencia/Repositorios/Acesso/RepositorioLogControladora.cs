﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Acesso;
using Controlsys.Repositorios.Acesso;
using Globalsys;

namespace Controlsys.Persistencia.Repositorios.Acesso
{
    public class RepositorioLogControladora : Repositorio<LogControladora>, IRepositorioLogControladora
    {
        public RepositorioLogControladora(UnidadeTrabalho unidadeTrabalho)
            :base(unidadeTrabalho)
        {            
        }

        public IQueryable<LogControladora> ObterPorControladora(int codigoControladora)
        {
            return ObterTodos().Where(lc => lc.Controladora.Codigo == codigoControladora);
        }
    }
}
