﻿using Controlsys.Dominio.Acesso;
using Controlsys.Repositorio.Acesso;
using Globalsys;
using Globalsys.Exceptions;
using System;

namespace Controlsys.Persistencia.Repositorios.Acesso
{
    /// <summary>
    /// Representa um(a) RepositorioAcessoTemporario.
    /// </summary>
    public class RepositorioAcessoTemporarioVeiculo : Repositorio<AcessoTemporarioVeiculo>, IRepositorioAcessoTemporarioVeiculo
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Acesso.RepositorioAcessoTemporario.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioAcessoTemporarioVeiculo(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho) { }

        public void Validar(AcessoTemporarioVeiculo entidade, Globalsys.Validacao.EstadoObjeto estadoObjeto)
        {
            if (entidade.DataHoraValidadeInicio >= entidade.DataHoraValidadeFim)
            {
                throw new CoreException("Data de Validade Inicial deve ser menor que a final");           
            }
            
            if (entidade.DataHoraValidadeInicio < DateTime.Today && entidade.Codigo == default(int))
            {
                throw new CoreException("Data de Validade Inicial deve ser maior que a data atual");
            }

            var dataVencimentoCredencial = entidade.VeiculoCredencialVeiculo.CredencialVeiculo.DataVencimento;
            if (dataVencimentoCredencial != null && (entidade.DataHoraValidadeInicio > dataVencimentoCredencial || entidade.DataHoraValidadeFim > dataVencimentoCredencial))
            {
                throw new CoreException(string.Format("Período do acesso temporário está fora do período de validade da credencial que expira em {0}", dataVencimentoCredencial.Value.ToString("dd/MM/yyyy HH:mm")));
            }

            //validar data da credencial

            if(entidade.VeiculoCredencialVeiculo.CredencialVeiculo.ObterSituacao().Status != Dominio.CredenciamentoVeiculos.StatusCredencial.Impressa)
            {
                throw new CoreException("A credencial não é valida");
            }

            if (!entidade.GrupoLeitora.Ativo)
            {
                throw new CoreException("O Grupo de Leitoras selecionado não está ativo.");
            }
        }
    }
}
