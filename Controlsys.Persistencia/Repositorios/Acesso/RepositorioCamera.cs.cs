﻿using Controlsys.Dominio.Acesso;
using Controlsys.Persistencia.Repositorios.Ocorrencias;
using Controlsys.Repositorio.Acesso;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Repositorios.Acesso
{    
    /// <summary>
    /// 
    /// </summary>
    public class RepositorioCamera : Repositorio<Camera>, IRepositorioCamera
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Parametros.RepositorioLocal.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioCamera(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
        }

        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="camera">
        /// O(a) area.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool Existe(Camera camera)
        {
            return ObterTodos().Any(l => l.Nome.ToUpper() == camera.Nome.ToUpper() && l.Ativo && l.Codigo != camera.Codigo);
        }

        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param descricao="camera">
        /// O(a) area.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool ExisteDescricao(Camera camera)
        {
            return ObterTodos().Any(l => l.Descricao.ToUpper() == camera.Descricao.ToUpper() && l.Ativo && l.Codigo != camera.Codigo);
        }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        public void Validar(Camera entidade, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    if ((entidade.Nome == null) || (entidade.Modelo == null) || (entidade.Descricao == null) || (entidade.DataRegistro == null) || (entidade.Precisao == null))
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    if (Existe(entidade))
                    {
                        throw new CoreException("Não é possivel salvar. Este nome descrito já está em uso por outra câmera.");
                    }
                    if (ExisteDescricao(entidade))
                    {
                        throw new CoreException("Não é possivel salvar. Esta descrição já está em uso por outra câmera.");
                    }
                    break;
                case EstadoObjeto.Alterado:
                    if (!entidade.Ativo)
                    {
                        throw new CoreException("A camera está inativa, não é possível alterar");
                    }
                    if ((entidade.Nome == null) || (entidade.Modelo == null) || (entidade.Descricao == null) || (entidade.DataRegistro == null) || (entidade.Precisao == null))
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }

                    if (Existe(entidade))
                    {
                        throw new CoreException("Não é possivel salvar. Este nome descrito já está em uso por outra câmera.");
                    }
                    if (ExisteDescricao(entidade))
                    {
                        throw new CoreException("Não é possivel salvar. Esta descrição já está em uso por outra câmera.");
                    }
                    break;
                case EstadoObjeto.Ativado:
                    if (!entidade.Ativo && Existe(entidade))
                    {
                        throw new CoreException("Não é possivel ativar. Este nome descrito já está em uso por outra câmera.");
                    }
                    if (!entidade.Ativo && ExisteDescricao(entidade))
                    {
                        throw new CoreException("Não é possivel ativar. Esta descrição já está em uso por outra câmera.");
                    }
                    break;
                case EstadoObjeto.Inativado:
                    if (entidade.Controladora != null)
                    {
                        throw new CoreException("Não é possível inativar, pois registro está sendo utilizado na controladora.");
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
