﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Acesso;
using Controlsys.Repositorios.Acesso;
using Globalsys;
using Globalsys.Validacao;
using Globalsys.Exceptions;

namespace Controlsys.Persistencia.Repositorios.Acesso
{
    /// <summary>
    /// Representa um(a) RepositorioGrupoLeitora.
    /// </summary>
    public class RepositorioGrupoLeitora : Repositorio<GrupoLeitora>, IRepositorioGrupoLeitora
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Acesso.RepositorioGrupoLeitora.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioGrupoLeitora(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho) {}

       /// <summary>
       /// Verifica se um objeto existe.
       /// </summary>
       ///
       /// <param name="grpLeitora">
       /// O(a) group leitora.
       /// </param>
       ///
       /// <returns>
       /// Um(a) bool.
       /// </returns>
       public bool Existe(GrupoLeitora grpLeitora)
        {
            return ObterTodos().Any(s => s.Nome.ToUpper() == grpLeitora.Nome.ToUpper() && s.Ativo && s.Codigo != grpLeitora.Codigo);
        }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        public void Validar(GrupoLeitora entidade, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    if (entidade.Nome == null || entidade.HoraInicio == null || entidade.HoraFim == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe um grupo leitora com o nome informado.");
                    }
                    break;
                case EstadoObjeto.Alterado:
                    if (!entidade.Ativo)
                    {
                        throw new CoreException("O Grupo Leitora está inativo, não é possível alterar");
                    }
                    if (entidade.Nome == null || entidade.HoraInicio == null || entidade.HoraFim == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe um grupo leitora com o nome informado.");
                    }
                    break;
                case EstadoObjeto.Ativado:
                    if (!entidade.Ativo && Existe(entidade))
                    {
                        throw new CoreException("Já existe um grupo leitora com o nome informado.");
                    }
                    break;
                default:
                    break;
            }

        }

        /// <summary>
        /// Salvar leitora grupo leitora.
        /// </summary>
        ///
        /// <param name="leitorasGrpLeitora">
        /// O(a) leitoras group leitora.
        /// </param>
        public void SalvarLeitoraGrupoLeitora(LeitoraGrupoLeitora leitorasGrpLeitora)
        {
            Repositorio<LeitoraGrupoLeitora> rep = new Repositorio<LeitoraGrupoLeitora>(base.UnidadeTrabalho);
            rep.Salvar(leitorasGrpLeitora);
        }

        /// <summary>
        /// Salva grupo leitora.
        /// </summary>
        ///
        /// <param name="grpLeitora">
        /// O(a) group leitora.
        /// </param>
        public void SalvaGrupoLeitora(GrupoLeitora grpLeitora)
        {
            Repositorio<GrupoLeitora> rep = new Repositorio<GrupoLeitora>(base.UnidadeTrabalho);
            rep.Salvar(grpLeitora);
        }

        /// <summary>
        /// Salvar grupo leitora treinamento.
        /// </summary>
        ///
        /// <param name="treinamentoGrpLeitora">
        /// O(a) treinamento group leitora.
        /// </param>
        public void SalvarGrupoLeitoraTreinamento(GrupoLeitoraTreinamento treinamentoGrpLeitora)
        {
            Repositorio<GrupoLeitoraTreinamento> rep = new Repositorio<GrupoLeitoraTreinamento>(base.UnidadeTrabalho);
            rep.Salvar(treinamentoGrpLeitora);
        }

        /// <summary>
        /// Salva grupo leitora hierarq.
        /// </summary>
        ///
        /// <param name="grupoLeitoraHirar">
        /// O(a) grupo leitora hirar.
        /// </param>
        public void SalvaGrupoLeitoraHierarq(GrupoLeitoraHierarq grupoLeitoraHirar)
        {
            Repositorio<GrupoLeitoraHierarq> rep = new Repositorio<GrupoLeitoraHierarq>(base.UnidadeTrabalho);
            rep.Salvar(grupoLeitoraHirar);
        }

    }
}
