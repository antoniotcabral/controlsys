﻿using Controlsys.Dominio.Acesso;
using Controlsys.Persistencia.Repositorios.Ocorrencias;
using Controlsys.Repositorio.Acesso;
using Controlsys.Repositorios.Acesso;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Controlsys.Persistencia.Repositorios.Acesso
{
    /// <summary>
    /// 
    /// </summary>
    public class RepositorioCameraImagem : Repositorio<CameraImagem>, IRepositorioCameraImagem
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Parametros.RepositorioLocal.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioCameraImagem(IUnidadeTrabalho unidadeTrabalho)
             : base(unidadeTrabalho)
        {
        }

        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="cameraImagem">
        /// O(a) area.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool Existe(CameraImagem cameraImagem)
        {
            return ObterTodos().Any(l => l.Codigo == cameraImagem.Codigo);
        }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        public void Validar(CameraImagem entidade, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    if (entidade.Codigo == 0)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe uma câmera com o nome informado.");
                    }
                    break;
               
                default:
                    break;
            }
        }
      

        /*
public Local ObterPorNomeSigla(string login, bool somenteAtivos = false)
{
Local local = ObterTodos().FirstOrDefault(u => u.Sigla + " - " + u.Nome == login);

if (!somenteAtivos || local == null)
return local;

if (!local.Ativo)
return null;

return local;
}*/
    }
}
