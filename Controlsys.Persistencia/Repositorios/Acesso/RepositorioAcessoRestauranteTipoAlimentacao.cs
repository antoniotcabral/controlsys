﻿using Controlsys.Dominio.Acesso;
using Controlsys.Repositorio.Acesso;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Repositorios.Acesso
{
    public class RepositorioAcessoRestauranteTipoAlimentacao : Repositorio<AcessoRestauranteTipoAlimentacao>, IRepositorioAcessoRestauranteTipoAlimentacao
    {

        public RepositorioAcessoRestauranteTipoAlimentacao(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho) { }

        public void Validar(AcessoRestauranteTipoAlimentacao entidade, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {

                case EstadoObjeto.Novo:
                    if (verificaConflitoHorario(entidade))
                        throw new CoreException(string.Format("Não é possível incluir o tipo de alimentação '{0}' do restaurante '{1}', pois existe um conflito de horário com outro tipo de alimentação.",
                            entidade.TipoAlimentacao.TipoAlimentacao.Nome, entidade.TipoAlimentacao.Restaurante.Nome));
                    break;

                default:
                    break;
            }
        }


        private bool verificaConflitoHorario(AcessoRestauranteTipoAlimentacao tipoAlimentacao)
        {
            var acRestTpAl = tipoAlimentacao.AcessoRestaurante.TiposAlimentacao.Where(t => t.Ativo &&
                t.TipoAlimentacao.Restaurante.Codigo == tipoAlimentacao.TipoAlimentacao.Restaurante.Codigo);
            var aux = acRestTpAl.Any(t => t.TipoAlimentacao.Ativo && t.TipoAlimentacao.Codigo == tipoAlimentacao.TipoAlimentacao.Codigo && (
                                (t.TipoAlimentacao.HoraInicio < tipoAlimentacao.TipoAlimentacao.HoraInicio && t.TipoAlimentacao.HoraFim > tipoAlimentacao.TipoAlimentacao.HoraInicio) ||
                                (t.TipoAlimentacao.HoraInicio < tipoAlimentacao.TipoAlimentacao.HoraFim && t.TipoAlimentacao.HoraFim > tipoAlimentacao.TipoAlimentacao.HoraFim) ||
                                (t.TipoAlimentacao.HoraInicio > tipoAlimentacao.TipoAlimentacao.HoraInicio && t.TipoAlimentacao.HoraFim < tipoAlimentacao.TipoAlimentacao.HoraFim)
                            ));

/*
            var aux = acRestTpAl.Any(t => t.TipoAlimentacao.Ativo && (
                (t.TipoAlimentacao.HoraInicio < tipoAlimentacao.TipoAlimentacao.HoraInicio && t.TipoAlimentacao.HoraFim > tipoAlimentacao.TipoAlimentacao.HoraInicio) ||
                (t.TipoAlimentacao.HoraInicio < tipoAlimentacao.TipoAlimentacao.HoraFim && t.TipoAlimentacao.HoraFim > tipoAlimentacao.TipoAlimentacao.HoraFim) ||
                (t.TipoAlimentacao.HoraInicio > tipoAlimentacao.TipoAlimentacao.HoraInicio && t.TipoAlimentacao.HoraFim < tipoAlimentacao.TipoAlimentacao.HoraFim)
            ));
*/
            return aux;
        }
    }
}
