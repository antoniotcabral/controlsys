﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Acesso;
using Controlsys.Repositorio.Acesso;
using Globalsys;
using Globalsys.Validacao;
using Globalsys.Exceptions;

namespace Controlsys.Persistencia.Repositorios.Acesso
{
    /// <summary>
    /// Representa um(a) RepositorioGrupoAcessoPapel.
    /// </summary>
    public class RepositorioGrupoAcessoPapel : Repositorio<GrupoAcessoPapel>, IRepositorioGrupoAcessoPapel
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Acesso.RepositorioGrupoAcessoPapel.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioGrupoAcessoPapel(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
        }

        /// <summary>
        /// Obter grupos de acesso.
        /// </summary>
        ///
        /// <param name="codigoPapel">
        /// O(a) codigo papel.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;GrupoAcessoPapel&gt;
        /// </returns>
        public List<GrupoAcessoPapel> ObterGruposDeAcesso(int codigoPapel)
        {
            return ObterTodos().Where(gap => gap.Papel.Codigo == codigoPapel && gap.Ativo).ToList();
        }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <param name="estado">
        /// O(a) estado.
        /// </param>
        /// <param name="GrupoAcessoPapel">
        /// O(a) papel.
        /// </param>
        public void Validar(EstadoObjeto estado, GrupoAcessoPapel entidade)
        {
            switch (estado)
            {
                case EstadoObjeto.Novo:
                case EstadoObjeto.Alterado:
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe o papel informado: " + entidade.Papel.PessoaFisica.Nome + '.');
                    }
                    break;
                default:
                    break;
            }
        }
        
        public bool Existe(GrupoAcessoPapel grupoAcessoPapel)
        {
            return ObterTodos().Any(l => l.Papel.Codigo == grupoAcessoPapel.Papel.Codigo && l.Ativo && l.GrupoAcesso.Codigo == grupoAcessoPapel.GrupoAcesso.Codigo);
        }
    }
}
