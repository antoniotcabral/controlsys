﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Seguranca;
using Controlsys.Repositorios.Acesso;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;

namespace Controlsys.Persistencia.Repositorios.Acesso
{
    /// <summary>
    /// Representa um(a) RepositorioControladora.
    /// </summary>
    public class RepositorioControladora : Repositorio<Controladora>, IRepositorioControladora
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Acesso.RepositorioControladora.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioControladora(IUnidadeTrabalho unidadeTrabalho) : base(unidadeTrabalho) { }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="controladora">
        /// O(a) controladora.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        public void Validar(Controladora controladora, Globalsys.Validacao.EstadoObjeto estadoObjeto)
        {
            if (!controladora.Leitoras.Any())
                throw new CoreException("Não é possível salvar. Informe ao menos uma leitora para a controladora em questão.");

            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                case EstadoObjeto.Alterado:
                    if (string.IsNullOrEmpty(controladora.Modelo))
                        throw new CoreException("Não é possível salvar. Informe o modelo.");

                    if (string.IsNullOrEmpty(controladora.IP))
                        throw new CoreException("Não é possível salvar. Informe o IP");

                    if (ObterTodos().Any(c => c.IP == controladora.IP && c.Codigo != controladora.Codigo))
                        throw new CoreException("Não é possível salvar. Já existe uma controladora com o IP informado.");

                    break;
                case EstadoObjeto.Removido:
                    break;
                case EstadoObjeto.Ativado:
                    break;
                case EstadoObjeto.Inativado:
                    break;
                default:
                    break;
            }
        }

        public void AlterarStatus(Controladora controladora, StatusControladora status, Usuario usuario)
        {
            LogControladora logAtual = controladora.ObterLog();

            controladora.AlterarStatus(status);

            Atualizar(controladora);

            #region Descomentar para gerar registros de sincronização de acionamento direto
            if (status == StatusControladora.Liberado)
                new RepositorioSincronizaPessoaAcesso(UnidadeTrabalho).LiberarControladoraAcessoDireto(controladora, TipoSincronizacaoPessoa.Conceder);

            // o acionamento direto da controladora só deve desativado se o status atual da controladora for igual a interditado.
            if (status != StatusControladora.Liberado && logAtual.Status == StatusControladora.Liberado)
                new RepositorioSincronizaPessoaAcesso(UnidadeTrabalho).LiberarControladoraAcessoDireto(controladora, TipoSincronizacaoPessoa.Suspender);
            #endregion

            LogControladora logControladora = new LogControladora
            {
                Controladora = controladora,
                Status = status,
                DataHora = DateTime.Now,
                Usuario = usuario,
                Observacoes = "Status alterado pelo usuário no sistema."
            };

            new RepositorioLogControladora(UnidadeTrabalho).Salvar(logControladora);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="controladora"></param>
        /// <param name="status"></param>
        /// <param name="usuario"></param>
        public void AlterarStatusControladoraIntegrador(Controladora controladora, StatusControladora status, Usuario usuario)
        {
            LogControladora logAtual = controladora.ObterLog();

            controladora.AlterarStatus(status);

            Atualizar(controladora);

            #region Descomentar para gerar registros de sincronização de acionamento direto
            if (status == StatusControladora.Liberado)
                new RepositorioSincronizaPessoaAcesso(UnidadeTrabalho).LiberarControladoraAcessoDireto(controladora, TipoSincronizacaoPessoa.Conceder);

            // o acionamento direto da controladora só deve desativado se o status atual da controladora for igual a interditado.
            if (status != StatusControladora.Liberado && logAtual.Status == StatusControladora.Liberado)
                new RepositorioSincronizaPessoaAcesso(UnidadeTrabalho).LiberarControladoraAcessoDireto(controladora, TipoSincronizacaoPessoa.Suspender);
            #endregion

            LogControladora logControladora = new LogControladora
            {
                Controladora = controladora,
                Status = status,
                DataHora = DateTime.Now,
                Usuario = usuario,
                Observacoes = "Status alterado pelo usuário no sistema."
            };

            //new RepositorioLogControladora(UnidadeTrabalho).Salvar(logControladora);
        }
    }
}
