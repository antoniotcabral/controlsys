﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Acesso;
using Controlsys.Repositorio.Acesso;
using Globalsys;

namespace Controlsys.Persistencia.Repositorios.Acesso
{
    /// <summary>
    /// Representa um(a) RepositorioGrupoLeitoraAcesso.
    /// </summary>
    public class RepositorioGrupoLeitoraAcesso : Repositorio<GrupoLeitoraAcesso>, IRepositorioGrupoLeitoraAcesso
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Acesso.RepositorioGrupoLeitoraAcesso.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioGrupoLeitoraAcesso(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
        }        
    }
}
