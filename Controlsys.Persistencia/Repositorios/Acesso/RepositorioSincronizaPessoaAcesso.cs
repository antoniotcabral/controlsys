﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Pessoas;
using Controlsys.Persistencia.Repositorios.Parametros;
using Controlsys.Repositorios.Acesso;
using Globalsys;
using Globalsys.Exceptions;

namespace Controlsys.Persistencia.Repositorios.Acesso
{
    /// <summary>
    /// Representa um(a) RepositorioSincronizaPessoaAcesso.
    /// </summary>
    public class RepositorioSincronizaPessoaAcesso : Controlsys.Persistencia.Repositorio<SincronizaPessoaAcesso>, IRepositorioSincronizaPessoaAcesso
    {
        /// <summary>
        /// Construtor para
        /// Controlsys.Persistencia.Repositorios.Acesso.RepositorioSincronizaPessoaAcesso.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioSincronizaPessoaAcesso(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {

        }

        /// <summary>
        /// Sincronizar.
        /// </summary>
        ///
        /// <param name="papel">
        /// O(a) papel.
        /// </param>
        /// <param name="tipoSincronizacao">
        /// O(a) tipo sincronizacao.
        /// </param>
        /// <param name="dataInicio">
        /// O(a) data inicio.
        /// </param>
        /// <param name="controladoras">
        /// O(a) controladoras.
        /// </param>
        public void Sincronizar(Dominio.Pessoas.Papel papel, TipoSincronizacaoPessoa tipoSincronizacao, DateTime? dataInicio, List<Controladora> controladoras = null)
        {
            RepositorioCracha repCracha = new RepositorioCracha(UnidadeTrabalho);
            RepositorioGrupoAcesso repGrupoAcesso = new RepositorioGrupoAcesso(UnidadeTrabalho);

            Cracha cracha = null;

            if (papel != null)
                cracha = repCracha.ObterCracha(papel.Codigo);

            if (controladoras == null)
                controladoras = repGrupoAcesso.ObterPermissoes(papel);

            foreach (Controladora controladora in controladoras.Where(c => c.Ativo))
            {
                SincronizaPessoaAcesso sincronizaPessoaAcesso = new SincronizaPessoaAcesso
                {
                    Controladora = controladora,
                    Cracha = cracha,
                    DataRegistro = DateTime.Now,
                    TipoSincronizacao = tipoSincronizacao,
                    DataInicio = dataInicio,
                    Status = StatusSincronizacao.Aguardando,
                    Porcentagem = 0
                };

                DateTime agora = DateTime.Now;

                if ((papel == null && dataInicio == null) && controladora.HoraSincronizacao.HasValue)
                {
                    var horaPrevista = DateTime.Today.Add(controladora.HoraSincronizacao.Value);
                    if (horaPrevista < agora)
                        sincronizaPessoaAcesso.DataInicio = null;
                    else
                        sincronizaPessoaAcesso.DataInicio = DateTime.Now;

                }
                else if (sincronizaPessoaAcesso.DataInicio.HasValue && sincronizaPessoaAcesso.DataInicio.Value < agora)
                    sincronizaPessoaAcesso.DataInicio = null;

                if (papel == null || Valido(sincronizaPessoaAcesso, papel))
                    Salvar(sincronizaPessoaAcesso);
                //Atualizar(sincronizaPessoaAcesso);
            }
        }

        /// <summary>
        /// Valido.
        /// </summary>
        ///
        /// <param name="sincroniza">
        /// O(a) sincroniza.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool Valido(SincronizaPessoaAcesso sincroniza, Papel papel = null)
        {
            RepositorioGrupoAcesso repGrupoAcesso = new RepositorioGrupoAcesso(UnidadeTrabalho);

            if (!sincroniza.Controladora.Ativo)
                return false;

            if (sincroniza.Cracha == null)
                return false;

            if (papel == null)
                papel = sincroniza.Cracha.Papel;

            if (sincroniza.TipoSincronizacao == TipoSincronizacaoPessoa.Conceder)
            {
                if (sincroniza.Cracha == null && papel != null)
                    return false;

                papel = sincroniza.Cracha.Papel;

                ASO aso = papel.ASOs.OrderByDescending(a => a.Codigo).FirstOrDefault();

                if (aso != null && aso.DataVencimento.Date < DateTime.Today.Date)
                    return false;

                PapelLog papelLog = papel.ObterLogPapel();

                if (papelLog.Status != StatusPapelLog.Ativo)
                    return false;

                /* incluir alocação
                 * 
                 * 
                 * 
                 * 
                 */

                if (!sincroniza.Cracha.Ativo)
                    return false;

                if (papel is PrestadorServico)
                {
                    PrestadorServico prestador = (papel as PrestadorServico);

                    if (prestador.DataValidade.Date < DateTime.Today.Date)
                        return false;
                }

                if (papel is Visitante)
                {
                    Visitante visitante = (papel as Visitante);

                    if (visitante.DataInicio > DateTime.Today || visitante.DataFim < DateTime.Today)
                        return false;
                }
            }

            if (ObterTodos().Any(s => sincroniza.Codigo != s.Codigo
                                             && sincroniza.Cracha.Codigo == s.Cracha.Codigo
                                             && sincroniza.Controladora.Codigo == s.Controladora.Codigo
                                             && sincroniza.TipoSincronizacao == s.TipoSincronizacao
                                             && (s.DataInicio == null || s.DataInicio == sincroniza.DataInicio)
                                             && s.DataSincronizacao == null))
                return false;

            return true;
        }

        /// <summary>
        /// Retransmitir.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="codigo">
        /// O(a) codigo.
        /// </param>
        /// <param name="dataInicio">
        /// O(a) data inicio.
        /// </param>
        ///
        /// <returns>
        /// Um(a) SincronizaPessoaAcesso.
        /// </returns>
        public SincronizaPessoaAcesso Retransmitir(int codigo, DateTime? dataInicio)
        {
            SincronizaPessoaAcesso sincroniza = ObterPorId(codigo);

            if (sincroniza.Cracha != null)
                throw new CoreException("Não é possível retransmitir. A retransmissão só é permitida a sincronizações de lista.");

            if (sincroniza.Status != StatusSincronizacao.NaoConcluido && sincroniza.Status != StatusSincronizacao.Aguardando)
                throw new CoreException("Não é possível retransmitir. A retransmissão só é permitida a sincronizações nos status \"Não concluído\" ou \"Aguardando\".");

            DateTime dataAtual = DateTime.Today.AddHours(DateTime.Now.Hour)
                                               .AddMinutes(DateTime.Now.Minute);

            if (dataInicio < dataAtual)
                throw new CoreException("Não é possível retransmitir. O horário da retransmissão deve ser maior ou igual ao atual.");

            
            sincroniza.Status = StatusSincronizacao.Aguardando;
            sincroniza.DataInicio = (dataInicio == null ? dataAtual : dataInicio);

            Atualizar(sincroniza);

            return sincroniza;
        }
        
        public SincronizaPessoaAcesso ObterUltimaSincronizacao(int codigoControladora)
        {
            return ObterTodos().Where(s => s.Controladora.Codigo == codigoControladora
                                 && s.Cracha == null
                                 && s.DataSincronizacao.HasValue)
                        .OrderByDescending(sa => sa.DataSincronizacao)
                        .FirstOrDefault();
        }

        public SincronizaPessoaAcesso LiberarControladoraAcessoDireto(Controladora controladora, TipoSincronizacaoPessoa tipoSincronizacao)
        {
            SincronizaPessoaAcesso sincronizaPessoaAcesso = new SincronizaPessoaAcesso
            {
                Controladora = controladora,
                Cracha = null,
                DataRegistro = DateTime.Now,
                TipoSincronizacao = tipoSincronizacao,
                DataInicio = null,
                Status = StatusSincronizacao.Aguardando,
                Porcentagem = 0,
                Tarefa = 8
            };

            Salvar(sincronizaPessoaAcesso);

            return sincronizaPessoaAcesso;
        }
    }
}
