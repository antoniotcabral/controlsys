﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Pessoas;
using Controlsys.Repositorio.Acesso;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;

namespace Controlsys.Persistencia.Repositorios.Acesso
{
    /// <summary>
    /// Representa um(a) RepositorioGrupoAcesso.
    /// </summary>
    public class RepositorioGrupoAcesso : Repositorio<GrupoAcesso>, IRepositorioGrupoAcesso
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Acesso.RepositorioGrupoAcesso.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioGrupoAcesso(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho) { }

        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="grupoAcesso">
        /// O(a) grupo acesso.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool Existe(GrupoAcesso grupoAcesso)
        {
            return ObterTodos().Any(s => s.Nome.ToUpper() == grupoAcesso.Nome.ToUpper() && s.Ativo && s.Codigo != grupoAcesso.Codigo);
        }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        public void Validar(GrupoAcesso entidade, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    if (entidade.Nome == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe um grupo com o nome informado.");
                    }
                    break;
                case EstadoObjeto.Alterado:
                    if (!entidade.Ativo)
                    {
                        throw new CoreException("O grupo está inativo, não é possível alterar.");
                    }
                    if (entidade.Nome == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe um grupo com o nome informado.");
                    }
                    if (ObterPorId(entidade.Codigo).Nome.Equals("Administradores"))
                    {
                        throw new CoreException("Não é possível alterar o nome deste grupo por ser um grupo interno do sistema. ");
                    }
                    break;
                case EstadoObjeto.Ativado:
                    if (!entidade.Ativo && Existe(entidade))
                    {
                        throw new CoreException("Já existe um grupo com o nome informado.");
                    }
                    break;
                default:
                    break;
            }
        }

        //public void SalvaGrupoAcesso(GrupoAcesso grupoAcesso)
        //{
        //    Repositorio<GrupoAcesso> rep = new Repositorio<GrupoAcesso>(base.UnidadeTrabalho);
        //    rep.Salvar(grupoAcesso);
        //}

        /// <summary>
        /// Salvar grupo acesso papel.
        /// </summary>
        ///
        /// <param name="grupoAcessoPapel">
        /// O(a) grupo acesso papel.
        /// </param>
        public void SalvarGrupoAcessoPapel(GrupoAcessoPapel grupoAcessoPapel)
        {
            Repositorio<GrupoAcessoPapel> rep = new Repositorio<GrupoAcessoPapel>(base.UnidadeTrabalho);
            rep.Salvar(grupoAcessoPapel);
        }

        /// <summary>
        /// Salvar grupo leitora acesso.
        /// </summary>
        ///
        /// <param name="grupoLeitoraAcesso">
        /// O(a) grupo leitora acesso.
        /// </param>
        public void SalvarGrupoLeitoraAcesso(GrupoLeitoraAcesso grupoLeitoraAcesso)
        {
            Repositorio<GrupoLeitoraAcesso> rep = new Repositorio<GrupoLeitoraAcesso>(base.UnidadeTrabalho);
            rep.Salvar(grupoLeitoraAcesso);
        }

        /// <summary>
        /// Obter permissoes.
        /// </summary>
        ///
        /// <param name="papel">
        /// O(a) papel.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;Controladora&gt;
        /// </returns>
        public List<Controladora> ObterPermissoes(Papel papel)
        {
            IQueryable<GrupoAcesso> gruposAcesso = ObterTodos().Where(ga => ga.Pessoas.Any(p => p.Papel.Codigo == papel.Codigo) && ga.Ativo);

            List<GrupoLeitora> lGrupoL = new List<GrupoLeitora>();
            lGrupoL.AddRange(gruposAcesso.ToList().SelectMany(ga => ga.GruposLeitora.Select(ggl => ggl.GrupoLeitora)));

            RepositorioAcessoRestaurante repAcessoRestaurante = new RepositorioAcessoRestaurante(UnidadeTrabalho);
            AcessoRestaurante aRestaurante = repAcessoRestaurante.ObterTodos().Where(r => r.Ativo && r.Papel.Codigo == papel.Codigo).OrderByDescending(ar => ar.Codigo).FirstOrDefault();
            if (aRestaurante != null)
            {
                foreach (var item in aRestaurante.TiposAlimentacao.Where(arta => arta.Ativo))
                {
                    lGrupoL.Add(item.TipoAlimentacao.Restaurante.GrupoLeitora);
                }
            }

            List<Controladora> listaControladoras = new List<Controladora>();
            listaControladoras.AddRange(ObterControladoras(lGrupoL));

            return listaControladoras;
        }

        /// <summary>
        /// Obter controladoras.
        /// </summary>
        ///
        /// <param name="gruposLeitoras">
        /// O(a) grupos leitoras.
        /// </param>
        /// <param name="validaCNH">
        /// true to valida cnh.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;Controladora&gt;
        /// </returns>
        public List<Controladora> ObterControladoras(IEnumerable<GrupoLeitora> gruposLeitoras, bool validaCNH = false)
        {
            List<Controladora> controladoras = new List<Controladora>();

            foreach (GrupoLeitora grupoLeitora in gruposLeitoras.Distinct())
            {
                controladoras.AddRange(grupoLeitora.Leitoras.Select(l => l.Leitora.Controladora).Distinct().ToList());

                if (!validaCNH)
                    controladoras.AddRange(ObterControladoras(grupoLeitora.GrupoLeitoraFilho.Select(ggl => ggl.GrupoLeitoraFilho).Distinct()));
            }

            return controladoras.Where(c => c.Ativo).Distinct().ToList();
        }

        /// <summary>
        /// Obter controladoras cnh.
        /// </summary>
        ///
        /// <param name="papel">
        /// O(a) papel.
        /// </param>
        ///
        /// <returns>
        /// Um(a) List&lt;Controladora&gt;
        /// </returns>
        public List<Controladora> ObterControladorasCNH(Papel papel)
        {
            IQueryable<GrupoAcesso> gruposAcesso = ObterTodos().Where(ga => ga.Pessoas.Any(p => p.Papel.Codigo == papel.Codigo) && ga.Ativo);

            return ObterControladoras(gruposAcesso.ToList().SelectMany(ga => ga.GruposLeitora.Select(ggl => ggl.GrupoLeitora)).Where(p => p.ValidaCNH), true).ToList();
        }
    }
}
