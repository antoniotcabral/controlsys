﻿using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Pessoas;
using Controlsys.Dominio.Seguranca;
using Controlsys.Persistencia.Repositorios.Ocorrencias;
using Controlsys.Persistencia.Repositorios.Pessoas;
using Controlsys.Persistencia.Repositorios.Seguranca;
using Controlsys.Repositorio.Acesso;
using Controlsys.Repositorios.Acesso;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Controlsys.Persistencia.Repositorios.Acesso
{
    /// <summary>
    /// 
    /// </summary>
    public class RepositorioAcessoVeiculo : Repositorio<AcessoVeiculo>, IRepositorioAcessoVeiculo
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Parametros.RepositorioLocal.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioAcessoVeiculo(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
        }

        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="camera">
        /// O(a) area.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool Existe(AcessoVeiculo acessoVeiculo)
        {
            return ObterTodos().Any(l => l.Codigo != acessoVeiculo.Codigo);
        }

        public bool Existe(Camera camera)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        public void Validar(AcessoVeiculo entidade, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    /*if ((entidade.NumeroIP == null) || (entidade.NumeroIP == null) || (entidade.Modelo == null) || (entidade.Local == null) || (entidade.DataRegistro == null))
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe uma câmera com o número de IP informado.");
                    }*/
                    break;
                case EstadoObjeto.Alterado:
                    //if (!entidade.Ativo)
                    //{
                    //    throw new CoreException("Não é possível alterar acesso");
                    //}
                    /*
                    if ((entidade.NumeroIP == null) || (entidade.NumeroIP == null) || (entidade.Modelo == null) || (entidade.Local == null) || (entidade.DataRegistro == null))
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }*/
                    if (Existe(entidade))
                    {
                        throw new CoreException("Já existe um acesso cadastrado.");
                    }
                    break;

                //case EstadoObjeto.Inativado:
                //case EstadoObjeto.Removido:
                //    if (new RepositorioOcorrenciaSeguranca(UnidadeTrabalho).ObterTodos().Where(os => os.Ativo && os.Local.Codigo.Equals(entidade.Codigo)).Any())
                //    {
                //        throw new CoreException("Não é possível inativar, pois registro está sendo utilizado como parâmetro para outro.");
                //    }
                //    break;
                default:
                    break;
            }
        }

        public void Validar(Camera entidade, EstadoObjeto estadoObjeto)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Obter lista acesso.
        /// </summary>
        ///
        /// <param name="status">
        /// O(a) status.
        /// </param>
        /// <param name="cracha">
        /// O(a) cracha.
        /// </param>
        /// <param name="cpf">
        /// O(a) cpfPassaporte.
        /// </param>
        /// <param name="empresa">
        /// O(a) empresa.
        /// </param>
        /// <param name="nome">
        /// O(a) nome.
        /// </param>
        /// <param name="cargo">
        /// O(a) cargo.
        /// </param>
        /// <param name="dataInicio">
        /// O(a) data inicio.
        /// </param>
        /// <param name="dataFim">
        /// O(a) data fim.
        /// </param>
        /// <param name="userLogado">
        /// O(a) user logado.
        /// </param>
        ///
        /// <returns>
        /// Um(a) IQueryable&lt;ControleAcesso&gt;
        /// </returns>
        public IQueryable<AcessoVeiculo> ObterListaAcesso(List<StatusAcesso> status, string cracha, string cpf, string empresa, string nome, int cargo, DateTime? dataInicio, DateTime? dataFim, string userLogado, List<int> leitorasSelecionadas = null, IQueryable<AlocacaoColaborador> aloc = null, bool validaGestor = false, long? empregadoSap = null, long? gestorPonto = null, long? superiorImediato = null, string codigoPht = null, IList<string> localLeitora = null, string passaporte = null, string maodeobra = null, string fornecedor = null, string contratada = null, string placa = null, string modeloCredencial = null, string cor = null, string modeloVeiculo = null, string fabricante = null, string nomeCamera = null)
        {
            RepositorioGrupoTrabalhoColab repGrpTrabColab = new RepositorioGrupoTrabalhoColab(UnidadeTrabalho);
            RepositorioPrestadorServico repPrest = new RepositorioPrestadorServico(UnidadeTrabalho);
            RepositorioColaborador repColaborador = new RepositorioColaborador(UnidadeTrabalho);
            RepositorioVisitante repVisitante = new RepositorioVisitante(UnidadeTrabalho);
            RepositorioUsuario repUsuario = new RepositorioUsuario(UnidadeTrabalho);

       
            Usuario usuario = repUsuario.ObterPorLogin(userLogado);
            int[] leitoras = null;
            if (leitorasSelecionadas == null)
                leitoras = usuario.GruposUsuarios
                                    .SelectMany(gu => gu.Grupo.GruposLeitoras.Select(gr => gr.GrupoLeitora))
                                    .SelectMany(gl => gl.Leitoras.Select(lgl => lgl.Leitora.Codigo))
                                    .Distinct()
                                    .ToArray();
            else
                leitoras = leitorasSelecionadas.ToArray();

            IQueryable<AcessoVeiculo> controleAcesso = ObterTodos();
            var total = controleAcesso.Where(pf => pf.Papel.PessoaFisica.Nome.Contains(nome)).ToList<AcessoVeiculo>();
            List<AcessoVeiculo> listACessos01 = controleAcesso.ToList();

            
           
            //if (validaGestor && usuario != null)
            //{
            //    var acoes = usuario.GruposUsuarios.Where(g => g.Ativo && g.Grupo.Ativo).Select(g => g.Grupo).SelectMany(g => g.Acoes);
            //    if ((acoes.Where(a => a.Ativo && a.Url == "/RelatorioJournal/FiltrarEmpregadoSAP")).Any())
            //    {
            //        var papel = usuario.PessoaFisica.ObterPapel(true);
            //        var subordinados = ObterEmpregadosRelacioados(papel.EmpregadoSAP);
            //        List<long> codsSubordinados = subordinados.Select(e => e.Codigo).ToList().Distinct().ToList();
            //        controleAcesso = controleAcesso.Where(ca => (ca.Papel.EmpregadoSAP != null) && (
            //                (codsSubordinados.Contains(ca.Papel.EmpregadoSAP.Codigo)) ||
            //                (ca.Papel.EmpregadoSAP.GestorPonto != null && codsSubordinados.Contains(ca.Papel.EmpregadoSAP.GestorPonto.Codigo)) ||
            //                (ca.Papel.EmpregadoSAP.SuperiorImediato != null && codsSubordinados.Contains(ca.Papel.EmpregadoSAP.SuperiorImediato.Codigo))
            //            )
            //        );
            //    }
            //}

          

                if (!string.IsNullOrEmpty(cpf))
                controleAcesso = controleAcesso.Where(pf => pf.Papel.PessoaFisica.CPF == cpf);

            if (!string.IsNullOrEmpty(passaporte))
                controleAcesso = controleAcesso.Where(pf => pf.Papel.PessoaFisica.Passaporte == passaporte);

            if (!string.IsNullOrEmpty(cracha))
            {                
                controleAcesso = controleAcesso.Where(pf => pf.Cracha.Numero == cracha);
            }
                

            if (!string.IsNullOrEmpty(nome))
            {
                var totalNomes = controleAcesso.Where(pf => pf.Papel.PessoaFisica.Nome.Contains(nome)).ToList();
                controleAcesso = controleAcesso.Where(pf => pf.Papel.PessoaFisica.Nome.Contains(nome));
               
            }

          

            if (status != null && status.Any())
                controleAcesso = controleAcesso.Where(t => status.Contains(t.StatusAcesso));

         

            if (cargo > 0)
                controleAcesso = controleAcesso.Where(pf => pf.Papel.Cargo.Codigo == cargo);



         
            if (!string.IsNullOrEmpty(fabricante))
            {
                List<string> fabricantes = fabricante.Split(',').ToList<string>();
                for (int i = 0; i < fabricantes.Count; i++)
                {
                    var codigoFabricante = Int32.Parse(fabricantes[i]);

                    controleAcesso = controleAcesso.Where(pf => pf.VeiculoCredencialVeiculo != null && pf.VeiculoCredencialVeiculo.Veiculo != null &&  pf.VeiculoCredencialVeiculo.Veiculo.Modelo != null && pf.VeiculoCredencialVeiculo.Veiculo.Modelo.Fabricante != null && pf.VeiculoCredencialVeiculo.Veiculo.Modelo.Fabricante.Codigo == codigoFabricante);

                }
                
            }
                
            
            if (!string.IsNullOrEmpty(modeloCredencial))
                controleAcesso = controleAcesso.Where(t => t.VeiculoCredencialVeiculo != null && t.VeiculoCredencialVeiculo.Veiculo != null  && t.VeiculoCredencialVeiculo.CredencialVeiculo.ModeloCredencial.Codigo.ToString() == modeloCredencial);

            if (!string.IsNullOrEmpty(modeloVeiculo))
                controleAcesso = controleAcesso.Where(t => t.VeiculoCredencialVeiculo != null && t.VeiculoCredencialVeiculo.Veiculo != null && t.VeiculoCredencialVeiculo.Veiculo.Modelo.Codigo.ToString() == modeloVeiculo);

            if (!string.IsNullOrEmpty(cor))
                controleAcesso = controleAcesso.Where(t => t.VeiculoCredencialVeiculo != null && t.VeiculoCredencialVeiculo.Veiculo != null && t.VeiculoCredencialVeiculo.Veiculo.Cor == cor);

            if (aloc != null)
                controleAcesso = controleAcesso.Where(k => aloc.Any(a => a.Papel.Codigo == k.Papel.Codigo));

            if (empregadoSap != null)
                controleAcesso = controleAcesso.Where(ca => ca.Papel.EmpregadoSAP != null && ca.Papel.EmpregadoSAP.Codigo == empregadoSap);

            if (gestorPonto != null)
                controleAcesso = controleAcesso.Where(ca => ca.Papel.EmpregadoSAP != null && ca.Papel.EmpregadoSAP.GestorPonto != null &&
                                 ca.Papel.EmpregadoSAP.GestorPonto.Codigo == gestorPonto);

            if (superiorImediato != null)
                controleAcesso = controleAcesso.Where(ca => ca.Papel.EmpregadoSAP != null && ca.Papel.EmpregadoSAP.SuperiorImediato != null &&
                                 ca.Papel.EmpregadoSAP.SuperiorImediato.Codigo == superiorImediato);

            if (!string.IsNullOrEmpty(codigoPht))
                controleAcesso = controleAcesso.Where(ca => repGrpTrabColab.ObterTodos().Any(gtb => gtb.Papel.Codigo.Equals(ca.Papel.Codigo) && gtb.GrupoTrabalho.CodigoPHT.Equals(codigoPht)));

            if (localLeitora != null && localLeitora.Any())
            {
                controleAcesso = controleAcesso.Where(ca => localLeitora.Contains(ca.Leitora.Local));
            }
             if(!string.IsNullOrEmpty(maodeobra))
             {
                var listaPapel = controleAcesso.ToList();
                var listaPapelFiltrado = new List<AcessoVeiculo>();

                for (int i = 0; i < listaPapel.Count; i++)
                {
                    if (listaPapel[i].Papel is Colaborador)
                    {
                        var papel = (Colaborador)listaPapel[i].Papel;
                        var IntMaoDeObra = (int) papel.MaoDeObra;

                        if (IntMaoDeObra == Int32.Parse(maodeobra))
                        {
                            listaPapelFiltrado.Add(listaPapel[i]);
                        }
                        else
                        {
                            listaPapel.RemoveAt(i);
                        }
                    }
                    else if (listaPapel[i].Papel is PrestadorServico)
                    {
                        var papel = (PrestadorServico)listaPapel[i].Papel;
                        if (papel.MaoDeObra.ToString() == maodeobra)
                        {
                            listaPapelFiltrado.Add(listaPapel[i]);
                        }
                        else
                        {
                            listaPapel.RemoveAt(i);
                        }
                    }
                    else if (listaPapel[i].Papel is Visitante)
                    {
                        listaPapel.RemoveAt(i);
                    }
                    i = i;

                }
                controleAcesso = listaPapelFiltrado.AsQueryable();
            }
            if (!string.IsNullOrEmpty(empresa))
            {
                var listaPapel = controleAcesso.ToList();
                var listaPapelFiltrado = new List<AcessoVeiculo>();


                for (int i = 0; i < listaPapel.Count; i++)
                {
                    if (listaPapel[i].Papel is Colaborador)
                    {
                        var papel = (Colaborador)listaPapel[i].Papel;
                        if (papel.Empresa.Nome == empresa)
                        {
                            listaPapelFiltrado.Add(listaPapel[i]);
                        }
                    }
                    else if (listaPapel[i].Papel is PrestadorServico)
                    {
                        var papel = (PrestadorServico)listaPapel[i].Papel;
                        if (papel.Empresa.Nome == empresa)
                        {
                            listaPapelFiltrado.Add(listaPapel[i]);
                        }
                    }
                    else if (listaPapel[i].Papel is Visitante)
                    {
                        var papel = (Visitante)listaPapel[i].Papel;
                        if (papel.Empresa == empresa)
                        {
                            listaPapelFiltrado.Add(listaPapel[i]);
                        }
                       
                    }
                    i = i;
                }
                controleAcesso = listaPapelFiltrado.AsQueryable();

            }

            if (!string.IsNullOrEmpty(fornecedor))
                {
                    var listaPapel = controleAcesso.ToList();
                    var listaPapelFiltrado = new List<AcessoVeiculo>();
                    
                    for (int i = 0; i < listaPapel.Count; i++)
                    {
                        if (listaPapel[i].Papel is PrestadorServico)
                        {
                            var papel = (PrestadorServico)listaPapel[i].Papel;
                        
                            if (papel.Fornecedor.Codigo == Convert.ToInt32(fornecedor))
                            {
                                listaPapelFiltrado.Add(listaPapel[i]);
                            }

                        }
                        i = i;
                    }
                    controleAcesso = listaPapelFiltrado.AsQueryable();
                }

            if (leitoras != null && controleAcesso.ToList() != null)
            {
                var listaAcessos = controleAcesso.ToList();
                listaAcessos.RemoveAll(l => l.Leitora == null);
                var NovaListaAcesso = new List<AcessoVeiculo>();


                for (int i = 0; i < listaAcessos.Count; i++)
                {

                    var leitoraInclusa = false;
                    for (int ix = 0; ix < leitoras.Count(); ix++)
                    {
                        var codigo = leitoras[ix];
                        if (listaAcessos[i].Leitora.Codigo == codigo)
                        {
                            leitoraInclusa = true;
                            NovaListaAcesso.Add(listaAcessos[i]);
                        }
                    }
                }
                controleAcesso = NovaListaAcesso.AsQueryable();

                //controleAcesso = controleAcesso.Where(l => leitoras.Any(lei => lei == l.Codigo));

            }
            List<AcessoVeiculo> listACessos = controleAcesso.ToList();
            listACessos = listACessos.OrderByDescending(c => c.DataRegistro).ToList();
            controleAcesso = listACessos.AsQueryable();

            List<int> aRemover = new List<int>();
            if (dataInicio.HasValue)
            {
                
                controleAcesso = listACessos.Where(ca => ca.DataRegistro >= dataInicio.Value).ToList().AsQueryable();
               // controleAcesso = controleAcesso.Where(ca => ca.DataRegistro >= dataInicio.Value);
               //listACessos = controleAcesso.ToList();

            }
                

            if (dataFim.HasValue)
            {
                dataFim = dataFim.Value.AddDays(1);
                controleAcesso = controleAcesso.Where(ca => ca.DataRegistro < dataFim.Value).ToList().AsQueryable();
                
                //controleAcesso = controleAcesso.Where(p => p.CamerasImagens.Any(ci => ci.PlacaVeiculo.Replace("-", "").ToLowerInvariant().Contains(placa.ToLowerInvariant())));

            }

            List<AcessoVeiculo> listACessos1 = controleAcesso.ToList();


            for (int i = 0; i < listACessos1.Count; i++)
            {
                RepositorioCameraImagem repAcessoVeiculo = new RepositorioCameraImagem(UnidadeTrabalho);
                listACessos1[i].CamerasImagens = repAcessoVeiculo.ObterTodos().Where(c => c.AcessoVeiculo.Codigo == listACessos1[i].Codigo).ToList();
            }

            for (int i = 0; i < listACessos1.Count; i++)
            {
                RepositorioCameraImagem repAcessoVeiculo = new RepositorioCameraImagem(UnidadeTrabalho);
                for (int ix = 0; ix < listACessos1[i].CamerasImagens.Count; ix++)
                {
                    if (listACessos1[i].CamerasImagens.Count == 0)
                    {
                        listACessos1[i].CamerasImagens = repAcessoVeiculo.ObterTodos().Where(c => c.AcessoVeiculo.Codigo == listACessos[i].Codigo).ToList();
                    }
                }
            }
            controleAcesso = listACessos1.AsQueryable();
            
            listACessos = controleAcesso.ToList();
                       
            if (!string.IsNullOrEmpty(placa))
            {

                //controleAcesso = controleAcesso.Where(pf => pf.CamerasImagens.Any(p => p.PlacaVeiculo == placa));
                //var Vci = controleAcesso.ToList();

                listACessos.RemoveAll(c => c.CamerasImagens == null || c.CamerasImagens.Count == 0);
                listACessos.RemoveAll(c => c.CamerasImagens.All(ci => ci.PrecisaoImage == 0f));

                List<AcessoVeiculo> novaListAcessos = new List<AcessoVeiculo>();
                //listACessos.OrderByDescending(c => c.DataRegistro);
                //List<int> aRemover = new List<int>();
                var placaLower = placa.Replace("-", "").ToLower();
                //listACessos.RemoveAll(c => c.CamerasImagens == null || c.CamerasImagens.Count == 0);
                //listACessos.RemoveAll(c => c.CamerasImagens.All(ci => ci.PrecisaoImage == 0f));
                for (int i = 0; i < listACessos.Count; i++)
                {

                    bool possuiPlaca = false;
                    

                    for (int ix = 0; ix < listACessos[i].CamerasImagens.Count; ix++)
                    {
                        possuiPlaca = false;
                        if (listACessos[i].CamerasImagens[ix].PrecisaoImage > 0f)
                        {

                            var placaComparada = listACessos[i].CamerasImagens[ix].PlacaVeiculo.Replace("-", "").ToLower();
                            
                            if (placaComparada.Contains(placaLower) && placaComparada != "")
                            {
                                possuiPlaca = true;
                            }
                        }
                    }
                    if(possuiPlaca == true)
                    {
                        novaListAcessos.Add(listACessos[i]);
                    }
                    else
                    {
                        i = i;
                        listACessos.RemoveAt(i);
                    }
                }


                controleAcesso = novaListAcessos.AsQueryable();
                
            }
            listACessos = controleAcesso.ToList();

            if (!string.IsNullOrEmpty(nomeCamera))
            {
               
                listACessos.RemoveAll(c => c.CamerasImagens == null || c.CamerasImagens.Count == 0);


                List<AcessoVeiculo> novalistAcessosCamera = new List<AcessoVeiculo>();
                //listACessos.OrderByDescending(c => c.DataRegistro);
                //List<int> aRemover = new List<int>();

                //listACessos.RemoveAll(c => c.CamerasImagens == null || c.CamerasImagens.Count == 0);
                //listACessos.RemoveAll(c => c.CamerasImagens.All(ci => ci.PrecisaoImage == 0f));
                for (int i = 0; i < listACessos.Count; i++)
                {

                    bool combina = false;


                    for (int ix = 0; ix < listACessos[i].CamerasImagens.Count; ix++)
                    {
                        combina = false;
                        if (listACessos[i].CamerasImagens[ix].PrecisaoImage > 0f)
                        {

                            var nomeComparada = listACessos[i].CamerasImagens[ix].Camera.Nome.ToLower();
                            var nomeLower = nomeCamera.ToLower();
                            if (nomeComparada == nomeLower && nomeComparada != "")
                            {
                                combina = true;
                            }
                        }
                    }
                    if (combina == true)
                    {
                        novalistAcessosCamera.Add(listACessos[i]);
                    }
                }

                controleAcesso = novalistAcessosCamera.AsQueryable();

            }




            //if ((!string.IsNullOrEmpty(empresa)) || (!string.IsNullOrEmpty(fornecedor)) || (!string.IsNullOrEmpty(maodeobra)))
            //{
            //    var colaboradores = repColaborador.ObterTodos();
            //    var listColaboradores = colaboradores.ToList();
            //    var prestadores = repPrest.ObterTodos();
            //    var visitantes = repVisitante.ObterTodos();


            //    if (!string.IsNullOrEmpty(empresa))
            //    {
            //        colaboradores = colaboradores.Where(p => p.Empresa.Nome == empresa);
            //        prestadores = prestadores.Where(p => p.Empresa.Nome == empresa);
            //        visitantes = visitantes.Where(p => p.Empresa == empresa);
            //    }

            //    if (!string.IsNullOrEmpty(maodeobra))
            //    {

            //        colaboradores = colaboradores.Where(p => p.MaoDeObra.ToString() == maodeobra);
            //        var listaColaboradores = colaboradores.Where(p => p.MaoDeObra.ToString() == maodeobra).ToList();
            //        prestadores = prestadores.Where(p => p.MaoDeObra.ToString() == maodeobra);
            //        visitantes = Enumerable.Empty<Visitante>().AsQueryable();
            //    }

            //    if (!string.IsNullOrEmpty(fornecedor))
            //    {
            //        colaboradores = Enumerable.Empty<Colaborador>().AsQueryable();
            //        prestadores = prestadores.Where(p => p.Fornecedor.Codigo == Convert.ToInt32(fornecedor));
            //        visitantes = Enumerable.Empty<Visitante>().AsQueryable();
            //    }

            //    List<int> codColab = new List<int>();
            //    List<int> codPres = new List<int>();
            //    List<int> codVis = new List<int>();

            //    codColab = colaboradores.Select(c => c.Codigo).ToList();
            //    codPres = prestadores.Select(c => c.Codigo).ToList();
            //    codVis = visitantes.Select(c => c.Codigo).ToList();

            //    codColab = codColab.Union(codPres).Union(codVis).ToList();
            //    int qtdcolab = codColab.Count;

            //    List<AcessoVeiculo> listaControleAcesso = new List<AcessoVeiculo>();

            //    for (int i = 0; i < qtdcolab; i += 1800)
            //    {
            //        var page = codColab.Skip(i).Take(1800).ToArray();
            //        listaControleAcesso.AddRange(controleAcesso.Where(c => page.Contains(c.Cracha.Papel.Codigo)));
            //    }

            //    controleAcesso = listaControleAcesso.AsQueryable();
            //}

            if (gestorPonto != null)
                controleAcesso = controleAcesso.Where(ca => ca.Papel.EmpregadoSAP != null && ca.Papel.EmpregadoSAP.GestorPonto != null &&
                                 ca.Papel.EmpregadoSAP.GestorPonto.Codigo == gestorPonto);

            //var total2 = controleAcesso.Where(ca => leitoras != null && leitoras.Contains(ca.Leitora.Codigo)).ToList();
            
            //controleAcesso = controleAcesso.Where(ca => leitoras.Contains(ca.Leitora.Codigo));
           // IQueryable<AcessoVeiculo> controleCameraImagem = ObterTodos();
           
            return controleAcesso;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nome"></param>
        /// <param name="cpf"></param>
        /// <param name="passaporte"></param>
        /// <param name="empresa"></param>
        /// <param name="cnpj"></param>
        /// <param name="tipoPapel"></param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <param name="empregadoSap"></param>
        /// <param name="gestorPonto"></param>
        /// <param name="superiorImediato"></param>
        /// <param name="codigoPht"></param>
        /// <param name="localLeitora"></param>
        /// <returns></returns>
        public IQueryable<AcessoVeiculo> ObterListaAcesso(string nome = null, string cpf = null, string passaporte = null, string empresa = null, string cnpj = null, TipoPapel? tipoPapel = null, DateTime? dataInicio = null, DateTime? dataFim = null, long? empregadoSap = null, long? gestorPonto = null, long? superiorImediato = null, string codigoPht = null, IList<string> localLeitora = null, string placa = null,string modeloCredencial = null, string cor = null, string modeloVeiculo = null, string fabricante = null, string nomeCamera = null)
        {
            RepositorioPrestadorServico repPrest = new RepositorioPrestadorServico(UnidadeTrabalho);
            RepositorioColaborador repColaborador = new RepositorioColaborador(UnidadeTrabalho);
            RepositorioVisitante repVisitante = new RepositorioVisitante(UnidadeTrabalho);
            RepositorioGrupoLeitora repGrupoLeitora = new RepositorioGrupoLeitora(UnidadeTrabalho);

            IQueryable<GrupoLeitora> gruposLeitora = repGrupoLeitora.ObterTodos();
            gruposLeitora = gruposLeitora.Where(gl => gl.Externa == true);
            var listLeitGrupLeito = gruposLeitora.Select(gl => gl.Leitoras);
            var listGrpLeit = new List<int>();
            foreach (var leitGrupLeito in listLeitGrupLeito)
            {
                listGrpLeit.AddRange(leitGrupLeito.Select(x => x.Leitora.Codigo));
            }

            IQueryable<AcessoVeiculo> controleAcesso = ObterTodos();
            var total = controleAcesso.Where(pf => pf.Papel.PessoaFisica.Nome.Contains(nome)).ToList();
            controleAcesso = controleAcesso.Where(ca => listGrpLeit.Contains(ca.Leitora.Codigo));
            controleAcesso = controleAcesso.Where(ca => ca.Papel != null);
            controleAcesso = controleAcesso.Where(ca => ca.StatusAcesso == StatusAcesso.OCO_UR_ENTRADA_COMPLETA ||
                                                        ca.StatusAcesso == StatusAcesso.OCO_UR_SAIDA_COMPLETA ||
                                                        ca.StatusAcesso == StatusAcesso.OCO_UR_ACESSO_LIBERADO);

            if (!string.IsNullOrEmpty(nome))
                controleAcesso = controleAcesso.Where(pf => pf.Papel.PessoaFisica.Nome.Contains(nome));

            if (!string.IsNullOrEmpty(cpf))
            {
                cpf = cpf.Replace(".", "").Replace("-", "");
                controleAcesso = controleAcesso.Where(pf => pf.Papel.PessoaFisica.CPF == cpf);
            }


           
            if (!string.IsNullOrEmpty(modeloCredencial))
                controleAcesso = controleAcesso.Where(t => t.VeiculoCredencialVeiculo.CredencialVeiculo.ModeloCredencial.Codigo.ToString() == modeloCredencial);

            if (!string.IsNullOrEmpty(passaporte))
                controleAcesso = controleAcesso.Where(pf => pf.Papel.PessoaFisica.Passaporte == passaporte);

            if (empregadoSap != null && empregadoSap > 0)
                controleAcesso = controleAcesso.Where(ca => ca.Papel.EmpregadoSAP.Codigo.Equals(empregadoSap));

            if (gestorPonto != null && gestorPonto > 0)
                controleAcesso = controleAcesso.Where(ca => ca.Papel.EmpregadoSAP != null && ca.Papel.EmpregadoSAP.GestorPonto != null &&
                                 ca.Papel.EmpregadoSAP.GestorPonto.Codigo.Equals(gestorPonto));

            if (superiorImediato != null && superiorImediato > 0)
                controleAcesso = controleAcesso.Where(ca => ca.Papel.EmpregadoSAP != null && ca.Papel.EmpregadoSAP.SuperiorImediato != null &&
                                 ca.Papel.EmpregadoSAP.SuperiorImediato.Codigo.Equals(superiorImediato));


            //if (!string.IsNullOrEmpty(nomeCamera))
            //{
            //    controleAcesso = controleAcesso.Where(pf => pf.CamerasImagens.All(p => p.Camera.Nome == nomeCamera));
            //}

            if (localLeitora != null && localLeitora.Any())
            {
                controleAcesso = controleAcesso.Where(ca => localLeitora.Contains(ca.Leitora.Local));
            }

            if (tipoPapel != null)
            {
                switch (tipoPapel)
                {
                    case TipoPapel.Colaborador:
                        var colabs = repColaborador.ObterTodos();
                        controleAcesso = controleAcesso.Where(pf => colabs.Select(c => c.Codigo).Contains(pf.Papel.Codigo));
                        break;
                    case TipoPapel.PrestadorServico:
                        var prests = repPrest.ObterTodos();
                        controleAcesso = controleAcesso.Where(pf => prests.Select(c => c.Codigo).Contains(pf.Papel.Codigo));
                        break;
                    case TipoPapel.Visitante:
                        var visits = repVisitante.ObterTodos();
                        controleAcesso = controleAcesso.Where(pf => visits.Select(c => c.Codigo).Contains(pf.Papel.Codigo));
                        break;
                }
            }

            if (dataInicio.HasValue)
                controleAcesso = controleAcesso.Where(ca => ca.DataRegistro >= dataInicio.Value);

            if (dataFim.HasValue)
                controleAcesso = controleAcesso.Where(ca => ca.DataRegistro < dataFim.Value);

            if (placa != null && placa != "")
            {
                controleAcesso = controleAcesso.Where(pf => pf.CamerasImagens != null && pf.CamerasImagens.Count > 0 && pf.CamerasImagens.Any(p => p.PlacaVeiculo == placa && p.PrecisaoImage > 0f && p != null && p.AcessoVeiculo.VeiculoCredencialVeiculo != null));
                var c = controleAcesso.ToList();
            }


            //if (dataFim.HasValue)
            //{
            //    dataFim = dataFim.Value.AddDays(1);
            //    controleAcesso = controleAcesso.Where(ca => ca.DataAcesso < dataFim.Value);
            //}

            if (!string.IsNullOrEmpty(empresa))
            {
                List<int> codColab = repColaborador.ObterTodos().Where(p => p.Empresa.Nome == empresa).Select(c => c.Codigo).ToList();
                List<int> codPres = repPrest.ObterTodos().Where(p => p.Empresa.Nome == empresa).Select(c => c.Codigo).ToList();
                List<int> codVis = repVisitante.ObterTodos().Where(p => p.Empresa == empresa).Select(c => c.Codigo).ToList();

                codColab = codColab.Union(codPres).Union(codVis).ToList();
                int qtdcolab = codColab.Count;

                List<AcessoVeiculo> listaControleAcesso = new List<AcessoVeiculo>();

                for (int i = 0; i < qtdcolab; i += 2000)
                {
                    var page = codColab.Skip(i).Take(2000).ToArray();
                    listaControleAcesso.AddRange(controleAcesso.Where(c => page.Contains(c.Cracha.Papel.Codigo)));
                }

                controleAcesso = listaControleAcesso.AsQueryable();
            }

            if (!string.IsNullOrEmpty(cnpj))
            {
                cnpj = cnpj.Replace(".", "").Replace("/", "").Replace("-", "");

                List<int> codColab = repColaborador.ObterTodos().Where(p => p.Empresa.CNPJ == cnpj).Select(c => c.Codigo).ToList();
                List<int> codPres = repPrest.ObterTodos().Where(p => p.Empresa.CNPJ == cnpj).Select(c => c.Codigo).ToList();

                codColab = codColab.Union(codPres).ToList();
                int qtdcolab = codColab.Count;

                List<AcessoVeiculo> listaControleAcesso = new List<AcessoVeiculo>();

                for (int i = 0; i < qtdcolab; i += 2000)
                {
                    var page = codColab.Skip(i).Take(2000).ToArray();
                    listaControleAcesso.AddRange(controleAcesso.Where(c => page.Contains(c.Cracha.Papel.Codigo)));
                }

                controleAcesso = listaControleAcesso.AsQueryable();
            }

            //controleAcesso = controleAcesso.OrderBy(c => c.Papel.Codigo).ThenBy(c=> c.Codigo);

            return controleAcesso;

        }

        private List<EmpregadoSAP> ObterEmpregadosRelacioados(EmpregadoSAP regPai)
        {
            List<EmpregadoSAP> subordinados = new List<EmpregadoSAP>();
            if (regPai != null)
            {
                ObterSubordinados(regPai, subordinados);
            }

            return subordinados;
        }


        private List<EmpregadoSAP> ObterSubordinados(EmpregadoSAP empregadoPai, List<EmpregadoSAP> empregadosSubordinados)
        {
            Repositorio<EmpregadoSAP> repEmpSap = new Repositorio<EmpregadoSAP>(UnidadeTrabalho);
            IQueryable<EmpregadoSAP> regsFilho = repEmpSap.ObterTodos().Where(es => es.GestorPonto.Codigo == empregadoPai.Codigo || es.SuperiorImediato.Codigo == empregadoPai.Codigo);

            if (regsFilho.Any())
            {
                List<EmpregadoSAP> listAux = regsFilho.ToList();
                listAux = listAux.Where(f => !empregadosSubordinados.Any(e => e.Codigo == f.Codigo)).ToList();

                empregadosSubordinados.AddRange(listAux.Select(r => r).ToList());
                empregadosSubordinados = empregadosSubordinados.Distinct().ToList();

                //empregadosSubordinados.AddRange(regsFilho.Where(g => g.GestorPonto.Codigo == empregadoPai.Codigo).Select(r => r.GestorPonto).ToList());
                //empregadosSubordinados.AddRange(regsFilho.Where(g => g.SuperiorImediato.Codigo == empregadoPai.Codigo).Select(r => r.SuperiorImediato).ToList());
                foreach (EmpregadoSAP empregadoSap in listAux)
                {
                    ObterSubordinados(empregadoSap, empregadosSubordinados);
                }
            }

            return empregadosSubordinados;
        }

      

        /*
public Local ObterPorNomeSigla(string login, bool somenteAtivos = false)
{
   Local local = ObterTodos().FirstOrDefault(u => u.Sigla + " - " + u.Nome == login);

   if (!somenteAtivos || local == null)
       return local;

   if (!local.Ativo)
       return null;

   return local;
}*/
    }

}