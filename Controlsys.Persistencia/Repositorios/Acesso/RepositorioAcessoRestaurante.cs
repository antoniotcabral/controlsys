﻿using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Parametros;
using Controlsys.Repositorio.Acesso;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Repositorios.Acesso
{
    public class RepositorioAcessoRestaurante : Repositorio<AcessoRestaurante>, IRepositorioAcessoRestaurante
    {
        public RepositorioAcessoRestaurante(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho) { }


        public void SalvarTipoAlimentacaoRestaurante(TipoAlimentacaoRestaurante tipoAlimentacaoRestaurante)
        {
        }

        public void Validar(AcessoRestaurante entidade, EstadoObjeto estadoObjeto, bool alteracaoSistema = false)
        {
            Repositorio<LogAcessoRestaurante> repRest = new Repositorio<LogAcessoRestaurante>(base.UnidadeTrabalho);

            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    if (entidade.Papel == null)
                        throw new CoreException("Não é possível salvar. Informe a pessoa que receberá o Acesso Restaurante.");
                    if (Existe(entidade))
                        throw new CoreException(string.Format("'{0}' já possui um registro de Acesso Restaurante ativo.", entidade.Papel.PessoaFisica.Nome));
                    break;

                case EstadoObjeto.Alterado:
                    if (!alteracaoSistema && !entidade.Ativo)
                            throw new CoreException("O registro está inativo, não é possível alterar.");
                    if (entidade.Papel == null)
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    if (Existe(entidade))
                        throw new CoreException(string.Format("'{0}' já possui um registro de Acesso Restaurante ativo.", entidade.Papel.PessoaFisica.Nome));
                    break;

                case EstadoObjeto.Ativado:
                    if (!entidade.Ativo && Existe(entidade))
                        throw new CoreException(string.Format("'{0}' já possui um registro de Acesso Restaurante ativo.", entidade.Papel.PessoaFisica.Nome));

                    Restaurante restaurante = entidade.TiposAlimentacao.Where(artp => artp.TipoAlimentacao.Ativo).Select(x => x.TipoAlimentacao.Restaurante).FirstOrDefault(r => !r.Ativo);
                    if (restaurante != null && restaurante.Codigo > 0)
                        throw new CoreException(string.Format("Não é possível ativar este acesso, pois o restaurante associado, '{0}', está inativo.", restaurante.Nome));

                    TipoAlimentacao tipoAlimentacao = entidade.TiposAlimentacao.Where(artp => artp.TipoAlimentacao.Ativo).Select(x => x.TipoAlimentacao.TipoAlimentacao).FirstOrDefault(r => !r.Ativo);
                    if (tipoAlimentacao != null && tipoAlimentacao.Codigo > 0)
                        throw new CoreException(string.Format("Não é possível ativar um acesso a restaurante, o tipo de alimentação '{0}' está inativo.", tipoAlimentacao.Nome));
                    break;

                default:
                    break;
            }
        }

        public bool Existe(AcessoRestaurante acRestaurante)
        {
            return ObterTodos().Any(s =>
                (s.Ativo == true) &&
                (s.Codigo != acRestaurante.Codigo) &&
                (s.Papel.Codigo == acRestaurante.Papel.Codigo)
            );
            //(s.Papel.PessoaFisica.Nome.ToUpper() == acRestaurante.Papel.PessoaFisica.Nome.ToUpper() && s.Ativo == true && s.Codigo != acRestaurante.Codigo));
        }

        public void SalvarTipoAlimentacaoRestaurante(AcessoRestauranteTipoAlimentacao tipoAlimentacaoRestaurante)
        {
            Repositorio<AcessoRestauranteTipoAlimentacao> rep = new Repositorio<AcessoRestauranteTipoAlimentacao>(base.UnidadeTrabalho);
            rep.Salvar(tipoAlimentacaoRestaurante);
        }

        public void SalvarLogAcessoRestaurante(LogAcessoRestaurante logAcessoRestaurante)
        {
            Repositorio<LogAcessoRestaurante> rep = new Repositorio<LogAcessoRestaurante>(base.UnidadeTrabalho);
            rep.Salvar(logAcessoRestaurante);
        }
    }
}
