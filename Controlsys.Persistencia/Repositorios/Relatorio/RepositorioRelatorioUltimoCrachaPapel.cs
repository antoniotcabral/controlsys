﻿using Controlsys.Dominio.Relatorio;
using Controlsys.Repositorio.Relatorio;
using Globalsys;
using System.Linq;

namespace Controlsys.Persistencia.Repositorios.Relatorio
{
    public class RepositorioRelatorioUltimoCrachaPapel : Repositorio<RelatorioUltimoCrachaPapel>, IRepositorioRelatorioUltimoCrachaPapel
    {
        public RepositorioRelatorioUltimoCrachaPapel(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
        }

        public IQueryable<RelatorioUltimoCrachaPapel> ObterDados()
        {
            IQueryable<RelatorioUltimoCrachaPapel> lista = ObterTodos();
            return lista;

        } 
    }
}
