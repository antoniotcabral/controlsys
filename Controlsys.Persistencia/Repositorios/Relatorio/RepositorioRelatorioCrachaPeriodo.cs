﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Relatorio;
using Controlsys.Repositorio.Relatorio;
using Globalsys;

namespace Controlsys.Persistencia.Repositorios.Relatorio
{
    public class RepositorioRelatorioCrachaPeriodo : Repositorio<RelatorioCrachaPeriodo>, IRepositorioRelatorioCrachaPeriodo
    {
        public RepositorioRelatorioCrachaPeriodo(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
        }

        public IQueryable<RelatorioCrachaPeriodo> ObterDados()
        {
            IQueryable<RelatorioCrachaPeriodo> lista = ObterTodos();
            return lista;
        
        } 
    }
}
