﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Relatorio;
using Controlsys.Repositorio.Relatorio;
using Globalsys;

namespace Controlsys.Persistencia.Repositorios.Relatorio
{
    public class RepositorioRelatorioRestauranteCatraca : Repositorio<RelatorioRestauranteCatraca>, IRepositorioRelatorioRestauranteCatraca
    {
        public RepositorioRelatorioRestauranteCatraca(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
        }

        public IQueryable<RelatorioRestauranteCatraca> ObterDados()
        {
            IQueryable<RelatorioRestauranteCatraca> lista = ObterTodos();
            return lista;

        } 
    }
}
