﻿using Controlsys.Dominio.Audit;
using Controlsys.Repositorio.Audit;
using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Repositorios.Audit
{
    /// <summary>
    /// Representa um(a) RepositorioLogError.
    /// </summary>
    public class RepositorioLogErrorIntegracaoSAP : Repositorio<LogErrorIntegracaoSAP>, IRepositorioLogErrorIntegracaoSAP
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Audit.RepositorioLogError.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioLogErrorIntegracaoSAP(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {

        }
    }
}
