﻿using Controlsys.Dominio.Audit;
using Controlsys.Repositorio.Audit;
using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Repositorios.Audit
{
    public class RepositorioIntegracaoSAPAcesso : Repositorio<IntegracaoSAPAcesso>, IRepositorioIntegracaoSAPAcesso
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Audit.IntegracaoSAPAcesso.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioIntegracaoSAPAcesso(IUnidadeTrabalho unidadeTrabalho)
            : base (unidadeTrabalho)
        {

        }
    }
}
