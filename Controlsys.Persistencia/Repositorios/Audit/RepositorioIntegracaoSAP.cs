﻿using Controlsys.Dominio.Audit;
using Controlsys.Repositorio.Audit;
using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Repositorios.Audit
{
    public class RepositorioIntegracaoSAP : Repositorio<IntegracaoSAP>, IRepositorioIntegracaoSAP
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Audit.IntegracaoSAP.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioIntegracaoSAP(IUnidadeTrabalho unidadeTrabalho)
            : base (unidadeTrabalho)
        {

        }
    }
}
