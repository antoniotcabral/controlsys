﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Audit;
using Controlsys.Repositorios.Audit;
using Globalsys;

namespace Controlsys.Persistencia.Repositorios.Audit
{
    /// <summary>
    /// Representa um(a) RepositorioAuditoria.
    /// </summary>
    public class RepositorioAuditoria : Repositorio<Auditoria>, IRepositorioAuditoria
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Audit.RepositorioAuditoria.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioAuditoria(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
        }
    }
}
