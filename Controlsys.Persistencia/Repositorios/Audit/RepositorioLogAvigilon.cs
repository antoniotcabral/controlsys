﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Audit;
using Controlsys.Repositorio.Audit;
using Globalsys;

namespace Controlsys.Persistencia.Repositorios.Audit
{
    /// <summary>
    /// Representa um(a) RepositorioLogError.
    /// </summary>
    public class RepositorioLogAvigilon : Repositorio<Log_Avigilon>,  IRepositorioLogAvigilon
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Audit.RepositorioLogError.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioLogAvigilon(IUnidadeTrabalho unidadeTrabalho)
            : base (unidadeTrabalho)
        {

        }
    }
}
