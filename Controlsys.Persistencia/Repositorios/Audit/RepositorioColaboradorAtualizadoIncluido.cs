﻿using Controlsys.Dominio.Audit;
using Controlsys.Repositorio.Audit;
using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Repositorios.Audit
{
    /// <summary>
    /// Representa um RepositorioColaboradorAtualizadoIncluido
    /// </summary>
    public class RepositorioColaboradorAtualizadoIncluido : Repositorio<ColaboradorAtualizadoIncluido>, IRepositorioColaboradorAtualizadoIncluido
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Audit.RepositorioAuditoria.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioColaboradorAtualizadoIncluido(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {

        }
    }
}
