﻿using Controlsys.Dominio.AgendamentosPessoa;
using Controlsys.Dominio.Pessoas;
using Controlsys.Repositorio.Agendamentos;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Repositorios.Agendamentos
{
    public class RepositorioPessoaAgendamento : Repositorio<PessoaAgendamento>, IRepositorioPessoaAgendamento
    {
        public RepositorioPessoaAgendamento(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
        }

        public void Validar(EstadoObjeto estado, PessoaAgendamento pessoa, bool visitante = false)
        {
            switch (estado)
            {
                case EstadoObjeto.Novo:
                case EstadoObjeto.Alterado:
                    if (string.IsNullOrEmpty(pessoa.Nome))
                        throw new CoreException("Informe o Nome.");
                    if (pessoa.Nome.Length > 150)
                        throw new CoreException("Nome com mais de 150 caracteres.");
                    if (string.IsNullOrEmpty(pessoa.Apelido))
                        throw new CoreException("Informe o Nome de Guerra.");
                    if (pessoa.Apelido.Length > 150)
                        throw new CoreException("Nome de Guerra com mais de 150 caracteres.");
                    if (string.IsNullOrEmpty(pessoa.CPF) && string.IsNullOrEmpty(pessoa.Passaporte))
                        throw new CoreException("Informe o CPF e/ou Passaporte.");
                    if (!string.IsNullOrEmpty(pessoa.CPF))
                    {
                        if (!PessoaFisica.CPFValido(pessoa.CPF))
                            throw new CoreException("CPF inválido.");
                        if (string.IsNullOrEmpty(pessoa.RG) || string.IsNullOrEmpty(pessoa.RGOrgaoEmissor) ||
                                pessoa.RGOrgaoEmissorUF == null || (!visitante && !pessoa.RGDataEmissao.HasValue))
                            throw new CoreException("Dados de Registro Geral (RG) nulo ou inválido.");
                    }
                    else if (!string.IsNullOrEmpty(pessoa.Passaporte))
                    {
                        if (pessoa.Passaporte.Length > 50)
                            throw new CoreException("Passaporte com mais de 50 caracteres.");
                    }
                    else throw new CoreException("CPF e Passaporte nulo.");
                    if (string.IsNullOrEmpty(pessoa.Email))
                        throw new CoreException("Email nulo.");
                    else
                    {
                        if (pessoa.Email.Length > 255)
                            throw new CoreException("Email com mais de 255 caracteres");
                        if (!Pessoa.EmailValido(pessoa.Email))
                            throw new CoreException("Email inválido.");
                    }
                    if (!string.IsNullOrEmpty(pessoa.TelefoneNumCelular))
                    {
                        var telefone = pessoa.TelefoneNumCelular.Trim().Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "");
                        if (!(telefone.Length == 10 || telefone.Length == 11))
                            throw new CoreException("Formato do Celular inválido, requerido: (00) 0?0000-0000.");
                    }
                    if (!string.IsNullOrEmpty(pessoa.TelefoneNumEmergencial))
                    {
                        var telefone = pessoa.TelefoneNumEmergencial.Trim().Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "");
                        if (!(telefone.Length == 10 || telefone.Length == 11))
                            throw new CoreException("Formato do Telefone Emergencial inválido, requerido: (00) 0?0000-0000.");
                    }
                    if (!string.IsNullOrEmpty(pessoa.TelefoneNumResidencial))
                    {
                        var telefone = pessoa.TelefoneNumResidencial.Trim().Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "");
                        if (!(telefone.Length == 10))
                            throw new CoreException("Formato do Telefone Residencial inválido, requerido: (00) 0000-0000.");
                    }
                    if (!string.IsNullOrEmpty(pessoa.RG))
                    {
                        if (pessoa.RG.Length > 50) throw new CoreException("RG com mais de 50 caracteres.");
                    }
                    if(!string.IsNullOrEmpty(pessoa.RGOrgaoEmissor))
                    {
                        if (pessoa.RGOrgaoEmissor.Length > 50) throw new CoreException("RG Órgão Emissor com mais de 50 caracteres.");
                    }
                    break;
                case EstadoObjeto.Removido:
                    break;
                case EstadoObjeto.Ativado:
                    break;
                case EstadoObjeto.Inativado:
                    break;
                default:
                    break;
            }
        }

        public bool Existe(PessoaAgendamento pessoa)
        {
            return ObterTodos().Any(pf => (
                (!string.IsNullOrEmpty(pessoa.CPFFormatado()) && pf.CPF == pessoa.CPF) ||
                (!string.IsNullOrEmpty(pessoa.Passaporte) && pf.Passaporte == pessoa.Passaporte)) &&
                pf.Codigo != pessoa.Codigo);
        }

        public PessoaAgendamento ObterPorCpfPassaporte(string cpf = null, string passaporte = null)
        {
            if (string.IsNullOrEmpty(cpf) && string.IsNullOrEmpty(passaporte))
                throw new CoreException("Favor informar cpf ou passaporte!");

            IQueryable<PessoaAgendamento> pessoaAg = ObterTodos();

            if (!string.IsNullOrEmpty(cpf))
                pessoaAg = pessoaAg.Where(pf => pf.CPF == cpf);

            if (!string.IsNullOrEmpty(passaporte))
                pessoaAg = pessoaAg.Where(pf => pf.Passaporte == passaporte);

            return pessoaAg.FirstOrDefault();
        }
    }
}
