﻿using Controlsys.Dominio.AgendamentosPessoa;
using Controlsys.Dominio.Pessoas;
using Controlsys.Repositorios.Agendamentos;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Repositorio.Agendamentos
{
    public class RepositorioAgendamentoVisitante : Repositorio<VisitanteAgendamento>, IRepositorioAgendamentoVisitante
    {
        public RepositorioAgendamentoVisitante(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
        }

        public void Validar(EstadoObjeto estadoObjeto, VisitanteAgendamento visitante)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    if (visitante.DataInicio.Date < DateTime.Now.Date) throw new CoreException("Data de início menor que a data atual.");
                    if (visitante.DataFim.Date < DateTime.Now.Date) throw new CoreException("Data fim menor que a data atual.");
                    break;
                case EstadoObjeto.Alterado:
                    if (visitante.Solicitante == null)
                        throw new CoreException("Não foi possível identificar o Solicitante logado.");

                    if (estadoObjeto == EstadoObjeto.Novo)
                    {
                        var registroParalelo = ObterTodos().FirstOrDefault(ag => !string.IsNullOrEmpty(visitante.PessoaAg.CPF) && visitante.PessoaAg.CPF == ag.PessoaAg.CPF);
                        if (registroParalelo != null)
                        {
                            var status = registroParalelo.ObterStatusAtual().StatusAgendamento;
                            if (status == StatusAgendamento.AguardandoAprovacao || status == StatusAgendamento.AguardandoAutorizacao)
                                throw new CoreException($"Já existe um registro {status.ObterDescricaoEnum()} para o CPF {registroParalelo.PessoaAg.CPFFormatado()} ");
                        }
                        else
                        {
                            registroParalelo = ObterTodos().FirstOrDefault(ag => !string.IsNullOrEmpty(visitante.PessoaAg.Passaporte) && visitante.PessoaAg.Passaporte == ag.PessoaAg.Passaporte);
                            if (registroParalelo != null)
                            {
                                var status = registroParalelo.ObterStatusAtual().StatusAgendamento;
                                if (status == StatusAgendamento.AguardandoAprovacao || status == StatusAgendamento.AguardandoAutorizacao)
                                    throw new CoreException($"Já existe um registro {status.ObterDescricaoEnum()} para o Passaporte {registroParalelo.PessoaAg.Passaporte} ");
                            }
                        }

                        var solicPapel = visitante.Solicitante.PessoaFisica.ObterPapel(buscarPrest: false, buscarVisit: false);
                        if (solicPapel == null) throw new CoreException("O solicitante não é um colaborador.");

                        if (visitante.DataInicio.Date < DateTime.Now.Date) throw new CoreException("Data de início menor que a data atual.");
                        if (visitante.DataFim.Date < DateTime.Now.Date) throw new CoreException("Data fim menor que a data atual.");
                    }

                    if (visitante.DataInicio.Date > visitante.DataFim.Date) throw new CoreException("Data de início maior que a data fim.");

                    if (string.IsNullOrEmpty(visitante.Empresa)) throw new CoreException("Empresa nula inválida.");
                    else if (visitante.Empresa.Length > 150) throw new CoreException("Nome da Empresa com mais de 150 caracteres.");

                    if (string.IsNullOrEmpty(visitante.Motivo)) throw new CoreException("Motivo da visita nulo inválido.");
                    else if (visitante.Motivo.Length > 1000) throw new CoreException("Motivo da visita com mais de 1000 caracteres.");

                    if (string.IsNullOrEmpty(visitante.Funcao)) throw new CoreException("Função nulo inválido.");
                    else if (visitante.Funcao.Length > 150) throw new CoreException("Função com mais de 150 caracteres.");
                    
                    if (!string.IsNullOrEmpty(visitante.ResponsavelVisita.PessoaFisica.CPF))
                    {
                        if (!PessoaFisica.CPFValido(visitante.ResponsavelVisita.PessoaFisica.CPF))
                            throw new CoreException("O número do CPF do Responsável não é válido.");
                    }
                    
                    if (visitante.ResponsavelVisita == null) throw new CoreException("Informe o Responsável.");

                    if (visitante.Empresa == null || visitante.Motivo == null || visitante.Funcao == null
                         || visitante.DataFim == null || visitante.DataInicio == null)
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    break;
                case EstadoObjeto.Removido:
                    break;
                default:
                    break;
            }
        }


    }
}
