﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.AgendamentosPessoa;
using Controlsys.Repositorio.Agendamentos;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;

namespace Controlsys.Persistencia.Repositorios.Agendamentos
{
    public class RepositorioPreCadastroPrestador : Repositorio<PrestadorServicoAgendamento>, IRepositorioPreCadastroPrestador
    {
        public RepositorioPreCadastroPrestador(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
        }

        public void Validar(EstadoObjeto estadoObjeto, PrestadorServicoAgendamento entidade)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    if (!string.IsNullOrEmpty(entidade.PessoaAg.CPF))
                    {
                        if (!entidade.PessoaAg.CTPS.HasValue || string.IsNullOrEmpty(entidade.PessoaAg.CTPSSerie) ||
                            entidade.PessoaAg.CTPSEstado == null || !entidade.PessoaAg.CTPSData.HasValue)
                            throw new CoreException(
                                "Não é possível salvar. Preencha corretamente os campos de Carteira de Trabalho e Previdência Social (CTPS).");
                    }

                    break;
                case EstadoObjeto.Alterado:
                    if (!string.IsNullOrEmpty(entidade.PessoaAg.CPF))
                    {
                        if (!entidade.PessoaAg.CTPS.HasValue || string.IsNullOrEmpty(entidade.PessoaAg.CTPSSerie) ||
                                entidade.PessoaAg.CTPSEstado == null || !entidade.PessoaAg.CTPSData.HasValue)
                            throw new CoreException("Não é possível salvar. Preencha corretamente os campos de Carteira de Trabalho e Previdência Social (CTPS).");
                    }

                    if (entidade.ObterStatusAtual().StatusAgendamento == StatusAgendamento.CadastroAprovado)
                        throw new CoreException("Não é possível alterar, esse registro já foi aprovado.");

                    break;
                case EstadoObjeto.Removido:
                    break;
                default:
                    break;
            }
        }
    }
}
