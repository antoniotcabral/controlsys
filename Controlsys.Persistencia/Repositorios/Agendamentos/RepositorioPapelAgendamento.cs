﻿using Controlsys.Dominio.AgendamentosPessoa;
using Controlsys.Repositorio.Agendamentos;
using Globalsys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Repositorios.Agendamentos
{
    public class RepositorioPapelAgendamento: Repositorio<PapelAgendamento>, IRepositorioPapelAgendamento
    {
        public RepositorioPapelAgendamento(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
        }


        public PapelAgendamento SalvarStatusPapelAg(StatusPapelAgendamento statusPapelAg)
        {
            Repositorio<StatusPapelAgendamento> repStatusAg = new Repositorio<StatusPapelAgendamento>(UnidadeTrabalho);
            repStatusAg.Salvar(statusPapelAg);
            return statusPapelAg.PapelAgendamento;
        }
    }

    
}
