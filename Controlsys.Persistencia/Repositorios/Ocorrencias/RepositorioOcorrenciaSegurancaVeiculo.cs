﻿using Controlsys.Dominio.CredenciamentoVeiculos;
using Controlsys.Dominio.Ocorrencias;
using Controlsys.Repositorio.Ocorrencias;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Controlsys.Persistencia.Repositorios.Ocorrencias
{
    public class RepositorioOcorrenciaSegurancaVeiculo : Repositorio<OcorrenciaSegurancaVeiculo>, IRepositorioOcorrenciaSegurancaVeiculo
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Parametros.RepositorioArea.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioOcorrenciaSegurancaVeiculo(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
        }

        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="ocorrencia">
        /// O(a) Ocorrencia Seguranca Veiculo.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool Existe(OcorrenciaSegurancaVeiculo ocorrenciaSegVeiculo)
        {
            return ObterTodos().Any(a => a.OcorrenciaSeg.Codigo == ocorrenciaSegVeiculo.OcorrenciaSeg.Codigo && a.Veiculo.Codigo == ocorrenciaSegVeiculo.Veiculo.Codigo && a.Ativo == true);
        }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        public void Validar(OcorrenciaSegurancaVeiculo entidade, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                case EstadoObjeto.Alterado:
                    if (entidade.Veiculo == null || entidade.OcorrenciaSeg == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    break;
                case EstadoObjeto.Ativado:
                    if (entidade.Ativo)
                    {
                        throw new CoreException("Agente já está ativo.");
                    }
                    break;
                case EstadoObjeto.Inativado:
                    if (!entidade.Ativo)
                    {
                        throw new CoreException("Agente já está inativo.");
                    }
                    break;
                default:
                    break;
            }
        }

        public OcorrenciaSegurancaVeiculo SalvarOcorrenciaSegurancaVeiculo (OcorrenciaSeguranca ocorrencia, Veiculo Veiculo)
        {
            Repositorio<OcorrenciaSegurancaVeiculo> repOcSegVeiculo = new Repositorio<OcorrenciaSegurancaVeiculo>(base.UnidadeTrabalho);

            OcorrenciaSegurancaVeiculo ocorrenciaSegVeiculo = new OcorrenciaSegurancaVeiculo();
            ocorrenciaSegVeiculo.OcorrenciaSeg = ocorrencia;
            ocorrenciaSegVeiculo.Veiculo = Veiculo;
            ocorrenciaSegVeiculo.DataRegistro = DateTime.Now;
            ocorrenciaSegVeiculo.Ativar();

            Validar(ocorrenciaSegVeiculo, EstadoObjeto.Novo);
            repOcSegVeiculo.Salvar(ocorrenciaSegVeiculo);

            return ocorrenciaSegVeiculo;
        }
    }
}
