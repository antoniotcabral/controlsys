﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Globalsys;
using Globalsys.Validacao;
using Globalsys.Exceptions;
using Controlsys.Dominio.Ocorrencias;
using Controlsys.Repositorio.Ocorrencias;

namespace Controlsys.Persistencia.Repositorios.Ocorrencias
{
    /// <summary>
    /// Representa um(a) RepositorioArea.
    /// </summary>
    public class RepositorioOcorrenciaSeguranca : Repositorio<OcorrenciaSeguranca>, IRepositorioOcorrenciaSeguranca
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Parametros.RepositorioArea.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioOcorrenciaSeguranca(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
        }

        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="ocorrencia">
        /// O(a) area.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool Existe(OcorrenciaSeguranca ocorrencia)
        {
            return ObterTodos().Any(a => a.Codigo != ocorrencia.Codigo);
        }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        public void Validar(OcorrenciaSeguranca entidade, EstadoObjeto estadoObjeto)
        {
            //switch (estadoObjeto)
            //{
            //    case EstadoObjeto.Novo:                   
            //        if (entidade.Descricao == null)
            //        {
            //            throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
            //        }                  
            //        if (Existe(entidade))
            //        {
            //            throw new CoreException("Já existe uma área com o nome informado.");
            //        }
            //        break;
            //    case EstadoObjeto.Alterado:
            //        if(!entidade.Ativo)
            //        {
            //            throw new CoreException("A área está inativa, não é possível alterar");
            //        }
            //        if (entidade.Descricao == null)
            //        {
            //            throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
            //        }
            //        if (Existe(entidade))
            //        {
            //            throw new CoreException("Já existe uma área com o nome informado.");
            //        }
            //        break;
            //    case EstadoObjeto.Ativado:
            //        if (!entidade.Ativo && Existe(entidade))
            //        {
            //            throw new CoreException("Já existe uma área com o nome informado.");
            //        }
            //        break;
            //    default:
            //        break;
            //}
        }
    }
}
