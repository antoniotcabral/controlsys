﻿using Controlsys.Dominio.Ocorrencias;
using Controlsys.Dominio.Parametros;
using Controlsys.Repositorio.Ocorrencias;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Controlsys.Persistencia.Repositorios.Ocorrencias
{
    public class RepositorioOcorrenciaSegurancaMidia : Repositorio<OcorrenciaSegurancaMidia>, IRepositorioOcorrenciaSegurancaMidia
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Parametros.RepositorioArea.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioOcorrenciaSegurancaMidia(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
        }

        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="ocorrencia">
        /// O(a) area.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool Existe(OcorrenciaSegurancaMidia ocorrenciaSegMidia)
        {
            return ObterTodos().Any(a => a.Ocorrencia.Codigo == ocorrenciaSegMidia.Ocorrencia.Codigo && a.Midia.Codigo == ocorrenciaSegMidia.Midia.Codigo && a.Ativo == true);
        }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        public void Validar(OcorrenciaSegurancaMidia entidade, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                case EstadoObjeto.Alterado:
                    if (entidade.Midia == null || entidade.Ocorrencia == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    break;
                case EstadoObjeto.Ativado:
                    if (entidade.Ativo)
                    {
                        throw new CoreException("Agente já está ativo.");
                    }
                    break;
                case EstadoObjeto.Inativado:
                    if (!entidade.Ativo)
                    {
                        throw new CoreException("Agente já está inativo.");
                    }
                    break;
                default:
                    break;
            }
        }

        public OcorrenciaSegurancaMidia SalvarOcorrenciaSegurancaMidia(OcorrenciaSeguranca ocorrencia, Midia midia)
        {
            Repositorio<OcorrenciaSegurancaMidia> repOcSegMidia= new Repositorio<OcorrenciaSegurancaMidia>(base.UnidadeTrabalho);

            OcorrenciaSegurancaMidia ocorrenciaSegMidia = new OcorrenciaSegurancaMidia();
            ocorrenciaSegMidia.Ocorrencia = ocorrencia;
            ocorrenciaSegMidia.Midia = midia;
            ocorrenciaSegMidia.DataRegistro = DateTime.Now;
            ocorrenciaSegMidia.Ativar();

            repOcSegMidia.Salvar(ocorrenciaSegMidia);

            return ocorrenciaSegMidia;
        }
    }
}
