﻿using Controlsys.Dominio.Ocorrencias;
using Controlsys.Repositorio.Ocorrencias;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Repositorios.Ocorrencias
{
    public class RepositorioTreinamentoOcorrenciaTransito : Repositorio<TreinamentoOcorrenciaTransito>, IRepositorioTreinamentoOcorrenciaTransito
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Parametros.RepositorioArea.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioTreinamentoOcorrenciaTransito(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
        }

        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="ocorrencia">
        /// O(a) area.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool Existe(TreinamentoOcorrenciaTransito treinamentoOcorrenciaTransito)
        {
            return ObterTodos().Any(a => a.Ativo && a.Codigo != treinamentoOcorrenciaTransito.Codigo && a.OcorrenciaTransito.Codigo == treinamentoOcorrenciaTransito.OcorrenciaTransito.Codigo);
        }


        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        public void Validar(TreinamentoOcorrenciaTransito entidade, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    if (Existe(entidade))
                    {
                        throw new CoreException("Não é possível salvar. Já existe um treinamento cadastrado para a Ocorrência.");
                    }

                    if (entidade.DataAgendamento == null && entidade.CompareceuTreinamento != null)
                    {
                        throw new CoreException("Não é possível salvar. Data de agendamento deve ser preenchida antes de selecionar a opção de \"Compareceu ao Treinamento\".");
                    }
                    break;
                case EstadoObjeto.Alterado:
                    if (Existe(entidade))
                    {
                        throw new CoreException("Não é possível salvar. Já existe um treinamento cadastrado para a Ocorrência.");
                    }

                    if (!entidade.DataAgendamento.HasValue && entidade.CompareceuTreinamento.HasValue)
                    {
                        throw new CoreException("Não é possível salvar. Data de agendamento deve ser preenchida antes de selecionar a opção de \"Compareceu ao Treinamento\".");
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
