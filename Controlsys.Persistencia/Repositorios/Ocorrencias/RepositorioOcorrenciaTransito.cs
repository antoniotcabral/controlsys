﻿using Controlsys.Dominio.Ocorrencias;
using Controlsys.Repositorio.Ocorrencias;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Repositorios.Ocorrencias
{
    public class RepositorioOcorrenciaTransito : Repositorio<OcorrenciaTransito>, IRepositorioOcorrenciaTransito
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Parametros.RepositorioArea.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioOcorrenciaTransito(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
        }


        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        public void Validar(OcorrenciaTransito entidade, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    if (entidade.Infracao == null || entidade.Local == null || entidade.PessoaFisica == null || entidade.Penalidade == null) //if (entidade.Infracao == null || entidade.Local == null || entidade.Papel == null || entidade.Penalidade == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    break;
                case EstadoObjeto.Alterado:
                    if (entidade.Infracao == null || entidade.Local == null || string.IsNullOrEmpty(entidade.NumeroCracha) || entidade.PessoaFisica == null || entidade.Penalidade == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
