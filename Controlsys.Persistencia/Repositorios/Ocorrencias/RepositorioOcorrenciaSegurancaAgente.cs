﻿using Controlsys.Dominio.Ocorrencias;
using Controlsys.Repositorio.Ocorrencias;
using Globalsys;
using Globalsys.Exceptions;
using Globalsys.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Controlsys.Persistencia.Repositorios.Ocorrencias
{
    public class RepositorioOcorrenciaSegurancaAgente : Repositorio<OcorrenciaSegurancaAgente>, IRepositorioOcorrenciaSegurancaAgente
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorios.Parametros.RepositorioArea.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public RepositorioOcorrenciaSegurancaAgente(IUnidadeTrabalho unidadeTrabalho)
            : base(unidadeTrabalho)
        {
        }

        /// <summary>
        /// Verifica se um objeto existe.
        /// </summary>
        ///
        /// <param name="ocorrencia">
        /// O(a) area.
        /// </param>
        ///
        /// <returns>
        /// Um(a) bool.
        /// </returns>
        public bool Existe(OcorrenciaSegurancaAgente ocorrenciaAgente)
        {
            return ObterTodos().Any(a => 
                a.Ocorrencia.Codigo == ocorrenciaAgente.Ocorrencia.Codigo && 
                a.PessoaFisica.Codigo == ocorrenciaAgente.PessoaFisica.Codigo &&
                a.Ativo
            );
        }

        /// <summary>
        /// Valida a modificação no objeto.
        /// </summary>
        ///
        /// <exception cref="CoreException">
        /// Lançada quando uma condição de erro Core ocorre.
        /// </exception>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="estadoObjeto">
        /// O(a) estado objeto.
        /// </param>
        public void Validar(OcorrenciaSegurancaAgente entidade, EstadoObjeto estadoObjeto)
        {
            switch (estadoObjeto)
            {
                case EstadoObjeto.Novo:
                    if (entidade.PessoaFisica == null || entidade.Ocorrencia == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }

                    if (Existe(entidade))
                    {
                        throw new CoreException("A Pessoa Cadastrada já foi incluida como agente para essa ocorrência.");
                    }
                    break;
                case EstadoObjeto.Alterado:
                    if (entidade.PessoaFisica == null || entidade.Ocorrencia == null)
                    {
                        throw new CoreException("Não é possível salvar. O formulário possui campos não preenchidos.");
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
