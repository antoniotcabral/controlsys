﻿using Controlsys.Dominio.Parametros;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Pessoas
{
    /// <summary>
    /// Representa um(a) CargoMap.
    /// </summary>
    public class CargoMap : ClassMap<Cargo>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Pessoas.CargoMap.
        /// </summary>
        public CargoMap()
        {
            Table("CARGO");

            Id(b => b.Codigo, "CD_CARGO");

            Map(b => b.Nome, "TX_NOME").Not.Nullable().Length(100);
            Map(b => b.Descricao, "TX_DESCRICAO").Length(500);

            Map(b => b.Ativo, "BL_ATIVO").Not.Nullable();
            Map(b => b.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(b => b.DataDesativacao, "DT_DESATIVACAO");
            Map(b => b.CargoSAP, "CD_CARGO_SAP").Nullable();
        }
    }
}
