﻿using Controlsys.Dominio.Pessoas;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Pessoas
{
    /// <summary>
    /// Representa um(a) GrupoTrabalhoColabMap.
    /// </summary>
    public class GrupoTrabalhoColabMap : ClassMap<GrupoTrabalhoColab>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Pessoas.GrupoTrabalhoColabMap.
        /// </summary>
        public GrupoTrabalhoColabMap()
        {
            Table("GRUPO_TRAB_COLAB");

            Id(gtc => gtc.Codigo, "CD_GRUPO_TRAB_COLAB");

            References(gtc => gtc.GrupoTrabalho, "CD_GRUPO").Not.Nullable();
            References(gtc => gtc.Papel, "CD_PAPEL").LazyLoad(Laziness.False).Not.Nullable();

            Map(gtc => gtc.Ativo, "BL_ATIVO").Not.Nullable();
            Map(gtc => gtc.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(gtc => gtc.DataDesativacao, "DT_DESATIVACAO");
        }
    }
}
