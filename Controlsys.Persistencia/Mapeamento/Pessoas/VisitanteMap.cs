﻿using Controlsys.Dominio.Pessoas;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Pessoas
{
    /// <summary>
    /// Representa um(a) VisitanteMap.
    /// </summary>
    public class VisitanteMap : SubclassMap<Visitante>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Pessoas.VisitanteMap.
        /// </summary>
        public VisitanteMap()
        {
            Table("VISITANTE");

            KeyColumn("CD_PAPEL");

            References(p => p.ResponsavelVisita, "CD_RESPONSAVEL");
            References(p => p.Solicitante, "CD_SOLICITANTE");

            Map(p => p.Motivo, "TX_MOTIVO").Length(1000);
            Map(p => p.TipoVisitante, "NU_VISITA");
            Map(p => p.AcessoPorto, "BL_PORTO");
            Map(p => p.Empresa, "TX_EMPRESA").Length(150).Not.Nullable();;
            Map(p => p.Funcao, "TX_FUNCAO").Length(150);
            Map(p => p.DataInicio, "DT_INICIO").Not.Nullable(); ;
            Map(p => p.DataFim, "DT_FIM").Not.Nullable(); ;


        }
    }
}
