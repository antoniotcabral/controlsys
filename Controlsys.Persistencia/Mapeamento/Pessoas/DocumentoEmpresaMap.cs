﻿using Controlsys.Dominio.Pessoas;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Pessoas
{
    /// <summary>
    /// Representa um(a) DocumentoEmpresaMap.
    /// </summary>
    public class DocumentoEmpresaMap : ClassMap<DocumentoEmpresa>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Pessoas.DocumentoEmpresaMap.
        /// </summary>
        public DocumentoEmpresaMap()
        {
            Table("DOCUMENTO_EMPRESA");

            Id(b => b.Codigo, "CD_DOCUMENTO_EMPRESA").Not.Nullable();

            References(b => b.Empresa, "CD_EMPRESA").Not.Nullable();
            References(b => b.Documento, "CD_DOCUMENTO").Not.Nullable();

            Map(b => b.Ativo, "BL_ATIVO").Not.Nullable();
            Map(b => b.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(b => b.DataDesativacao, "DT_DESATIVACAO");
        }

    }
}
