﻿using Controlsys.Dominio.Parametros;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Pessoas
{
    /// <summary>
    /// Representa um(a) TreinamentoMap.
    /// </summary>
    public class TreinamentoMap : ClassMap<Treinamento>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Pessoas.TreinamentoMap.
        /// </summary>
        public TreinamentoMap()
        {
            Table("TREINAMENTO");

            Id(p => p.Codigo, "CD_TREINAMENTO");

            Map(p => p.DiasValidade, "NU_DIAS");
            Map(p => p.Numero, "CD_SAP");
            Map(f => f.Nome, "TX_NOME").Length(100).Not.Nullable();
            Map(f => f.Descricao, "TX_DESCRICAO").Length(250);

            Map(f => f.Ativo, "BL_ATIVO").Not.Nullable();
            Map(f => f.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(f => f.DataDesativacao, "DT_DESATIVACAO");

        }
    }
}
