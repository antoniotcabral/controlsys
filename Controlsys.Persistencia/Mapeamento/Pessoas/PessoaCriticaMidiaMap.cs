﻿using Controlsys.Dominio.Acesso;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Controlsys.Persistencia.Mapeamento.Pessoas
{
    public class PessoaCriticaMidiaMap : ClassMap<PessoaCriticaMidia>
    {
        public PessoaCriticaMidiaMap()
        {
            Table("PESSOA_CRITICA_MIDIA");

            Id(p => p.Codigo, "CD_PESSOA_CRITICA_MIDIA");

            References(p => p.PessoaCritica, "CD_PESSOA_CRITICA").Not.Nullable();
            References(p => p.Midia, "CD_MIDIA").Not.Nullable();

            Map(p => p.Ativo, "BL_ATIVO").Not.Nullable();
            Map(p => p.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(p => p.DataDesativacao, "DT_DESATIVACAO").Nullable();

        }
    }
}
