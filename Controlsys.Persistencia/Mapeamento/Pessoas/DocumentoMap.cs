﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Parametros;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Pessoas
{
    /// <summary>
    /// Representa um(a) DocumentoMap.
    /// </summary>
    public class DocumentoMap : ClassMap<Documento>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Pessoas.DocumentoMap.
        /// </summary>
        public DocumentoMap()
        {
            Table("DOCUMENTO");

            Id(d => d.Codigo, "CD_DOCUMENTO");

            References(d => d.TipoDocumento, "CD_TIPO_DOC");

            HasMany(p => p.Midias).Table("DOCUMENTO_MIDIA").KeyColumn("CD_DOCUMENTO")
               .Where("DT_DESATIVACAO IS NULL").Not.LazyLoad();

            Map(d => d.NomeDocumento, "TX_NOME").Not.Nullable().Length(50);            
            Map(d => d.Descricao, "TX_DESCRICAO").Length(300);            
            Map(d => d.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(d => d.DataDesativacao, "DT_DESATIVACAO");
            Map(d => d.Ativo, "BL_ATIVO").Not.Nullable();
            Map(d => d.DataValidade, "DT_VALIDADE");
            Map(d => d.DataEmissao, "DT_DOCUMENTO");
            Map(d => d.Status, "TX_STATUS").Not.Nullable();
        }
    }
}
