﻿using Controlsys.Dominio.Pessoas;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Pessoas
{
    /// <summary>
    /// Representa um(a) PapelMap.
    /// </summary>
    public class PapelMap : ClassMap<Papel>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Pessoas.PapelMap.
        /// </summary>
        public PapelMap()
        {
            Table("PAPEL");

            Id(p => p.Codigo, "CD_PAPEL");

            References(p => p.PessoaFisica, "CD_PESSOA_FISICA");
            References(p => p.Cargo, "CD_CARGO");
            References(p => p.ModeloCracha, "CD_MODELO");
            References(p => p.Area, "CD_AREA");
            References(p => p.EmpregadoSAP, "CD_EMPREGADO_SAP");

            Map(p => p.SenhaLeitora, "TX_SENHA");
            Map(p => p.PessoaJuridica, "BL_JURIDICO").Not.Nullable();
            Map(p => p.AntiPassBack, "BL_PASSBACK").Not.Nullable();
            Map(p => p.DataRegistro, "DT_REGISTRO");
            Map(p => p.SobAnalise, "BL_SOB_ANALISE").Not.Nullable();
            Map(p => p.Supervisor, "BL_SUPERVISOR").Not.Nullable();
            Map(p => p.Supervisionado, "BL_SUPERVISIONADO").Not.Nullable();
            Map(p => p.IdUsuario, "ID_USUARIO");

            HasMany(p => p.Documentos)
                .KeyColumn("CD_PAPEL")
                .Where("DT_DESATIVACAO IS NULL");

            HasMany(p => p.ASOs).KeyColumn("CD_PAPEL").Inverse();            

            HasMany(p => p.PapelLogs)
              .KeyColumn("CD_PAPEL");

            HasMany(p => p.ExcecaoAcesso)
              .KeyColumn("CD_PAPEL");

            HasMany(pf => pf.Treinamentos)
                .KeyColumn("CD_PAPEL").Where("DT_DESATIVACAO IS NULL");

            HasMany(p => p.AtendimentosReceptivo)
                .KeyColumn("CD_PAPEL").Where("DT_DESATIVACAO IS NULL");

            //HasOne(p => p.EmpregadoSAP)
            //    .ForeignKey("CD_PAPEL");
        }

    }
}
