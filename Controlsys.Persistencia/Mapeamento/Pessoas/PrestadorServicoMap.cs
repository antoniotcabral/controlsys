﻿using Controlsys.Dominio.Pessoas;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Pessoas
{
    /// <summary>
    /// Representa um(a) PrestadorServicoMap.
    /// </summary>
    public class PrestadorServicoMap : SubclassMap<PrestadorServico>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Pessoas.PrestadorServicoMap.
        /// </summary>
        public PrestadorServicoMap()
        {
            Table("PRESTADOR");

            KeyColumn("CD_PAPEL");

            References(p => p.Solicitante, "CD_SOLICITANTE").Not.Nullable();
            References(p => p.Empresa, "CD_EMPRESA");
            References(p => p.Fornecedor, "CD_FORNECEDOR");

            References(p => p.PedidoCompra, "CD_PEDIDO");
            References(p => p.SetorCusto, "CD_SETOR");

            Map(p => p.DataAdmissao, "DT_ADMISSAO").Nullable();
            Map(p => p.DataValidade, "DT_VALIDADE").Not.Nullable();
            Map(p => p.TipoPrestador, "TX_PRESTADOR").Length(50);
            Map(p => p.Observacao, "TX_OBSERVACAO").Length(1000);
            Map(b => b.MaoDeObra, "TXT_MAODEOBRA").Not.Nullable();
            Map(b => b.GrupoPessoa, "NU_GRUPOPESSOA").CustomType<int>().Nullable().Length(20);

            Map(b => b.AcessoPorto, "BL_PORTO");
        }
    }
}
