﻿using Controlsys.Dominio.Pessoas;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Pessoas
{
    /// <summary>
    /// Representa um(a) ExcecaoAcessoMap.
    /// </summary>
    public class ExcecaoAcessoMap : ClassMap<ExcecaoAcesso>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Pessoas.ExcecaoAcessoMap.
        /// </summary>
        public ExcecaoAcessoMap()
        {
            Table("PAPEL_EXCECAO_ACESSO");

            Id(p => p.Codigo, "CD_PAPEL_EXCECAO_ACESSO");
            References(p => p.Papel, "CD_PAPEL").Not.Nullable();
            Map(p => p.CriterioBloqueio, "TX_CRITERIO_BLOQUEIO").Not.Nullable().Length(100);
            Map(p => p.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(p => p.DataDesativacao, "DT_DESATIVACAO");
            Map(p => p.Ativo, "BL_ATIVO").Not.Nullable();
        }

    }
}
