﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Pessoas
{
    public class AtendimentoReceptivoMap : ClassMap<AtendimentoReceptivo>
    {
        public AtendimentoReceptivoMap()
        {
            Table("ATENDIMENTO_RECEPTIVO");

            Id(b => b.Codigo, "CD_ATENDIMENTO_RECEPTIVO");

            References(b => b.Midia, "CD_MIDIA").Not.Nullable();
            References(b => b.Papel, "CD_PAPEL").Nullable();
            References(b => b.Usuario, "CD_USUARIO").Not.Nullable();

            Map(b => b.Descricao, "TX_DESCRICAO").Nullable();
            Map(b => b.Ativo, "BL_ATIVO").Not.Nullable();
            Map(b => b.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(b => b.DataDesativacao, "DT_DESATIVACAO").Nullable();
        }
    }
}
