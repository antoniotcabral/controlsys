﻿using Controlsys.Dominio.Acesso;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Pessoas
{
    /// <summary>
    /// Representa um(a) PessoaCriticaMap.
    /// </summary>
    public class PessoaCriticaMap : ClassMap<PessoaCritica>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Pessoas.PessoaCriticaMap.
        /// </summary>
        public PessoaCriticaMap()
        {
            Table("PESSOA_CRITICA");

            Id(p => p.Codigo, "CD_PESSOA_CRITICA");

            References(p => p.PessoaFisica, "CD_PESSOA");
            References(p => p.MotivoBloqueio, "CD_MOTIVO");
            References(p => p.Responsavel, "CD_RESPONSAVEL");

            Map(p => p.DataBloqueio, "DT_BLOQUEIO");
            Map(p => p.Observacao, "TX_OBSERVACAO").Length(1000);
            Map(p => p.Ativo, "BL_ATIVO").Not.Nullable();
            Map(p => p.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(p => p.DataDesativacao, "DT_DESATIVACAO");

            HasMany(os => os.Midias)
                .KeyColumn("CD_PESSOA_CRITICA")
                .Where("DT_DESATIVACAO IS NULL");

            //HasManyToMany(p => p.Documentos)
            //   .Table("DOCUMENTO_CRITICA")
            //   .ChildKeyColumn("CD_LISTA_CRITICA")
            //   .ParentKeyColumn("CD_DOCUMENTO")
            //   .Where("DT_DESATIVACAO IS NULL");

        }
    }
}
