﻿using Controlsys.Dominio.Parametros;
using Controlsys.Dominio.Pessoas;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Pessoas
{
    /// <summary>
    /// Representa um(a) DocumentoPapelMap.
    /// </summary>
    public class DocumentoPapelMap : ClassMap<DocumentoPapel>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Pessoas.DocumentoPapelMap.
        /// </summary>
        public DocumentoPapelMap()
        {
          Table("DOCUMENTO_PAPEL");

            Id(b => b.Codigo, "CD_DOCUMENTO_PAPEL").Not.Nullable();

            References(b => b.Papel, "CD_PAPEL").Nullable();
            References(b => b.Documento, "CD_DOCUMENTO").Not.Nullable();
            References(b => b.PapelAgendamento, "CD_PAPEL_AGENDAMENTO").Nullable();

            Map(b => b.Ativo, "BL_ATIVO").Not.Nullable();
            Map(b => b.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(b => b.DataDesativacao, "DT_DESATIVACAO");
        }

    }
}
