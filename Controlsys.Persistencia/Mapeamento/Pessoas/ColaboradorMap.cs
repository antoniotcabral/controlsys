﻿using Controlsys.Dominio.Pessoas;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Pessoas
{
    /// <summary>
    /// Representa um(a) ColaboradorMap.
    /// </summary>
    public class ColaboradorMap : SubclassMap<Colaborador>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Pessoas.ColaboradorMap.
        /// </summary>
        public ColaboradorMap()
        {
            Table("COLABORADOR");

            KeyColumn("CD_PAPEL");

            References(b => b.Empresa, "CD_EMPRESA").Not.Nullable();
            References(b => b.Area, "CD_AREA").Nullable();
            References(b => b.SetorCusto, "CD_SETOR").Nullable();

            Map(b => b.PIS, "NU_PIS");
            //Alterado por Antonio Cabral 15/04/20, atendimento chamado-16828-tx1362323-item-289-mudança-webservice-novidadespessoasap
            Map(b => b.Cnpj_SubContratista, "CNPJ_SUBCONTRATISTA");
            Map(b => b.SubContratista, "NOME_SUBCONTRATISTA");
            //-----------------------------------------------------------------------------
            Map(b => b.HoristaMensalista, "TX_MENSALISTA");
            Map(b => b.DataAdmissao, "DT_ADMISSAO");
            Map(b => b.DataValidade, "DT_VALIDADE");

            Map(b => b.Salario, "NU_SALARIO");
            Map(b => b.Observacao, "TX_OBSERVACAO");

            Map(b => b.ConselhoProfissional, "TX_CONSELHOPROF");
            Map(b => b.RegistroProfissional, "TX_REGISTROPROF");
            Map(b => b.MaoDeObra, "TXT_MAODEOBRA").Not.Nullable();
            Map(b => b.GrupoPessoa, "NU_GRUPOPESSOA").CustomType<int>().Nullable().Length(20);

            HasMany(c => c.GruposTrabalho)
                .KeyColumn("CD_PAPEL").Where("DT_DESATIVACAO IS NULL");

            Map(b => b.AcessoPorto, "BL_PORTO");

            Map(b => b.IdMotivo, "TX_IDMOTIVO");
            Map(b => b.DescMotivo, "TX_DESCMOTIVO");
            Map(b => b.Processado, "TX_PROCESSADO");
            Map(b => b.TipoAcesso, "TX_TIPOACESSO");            

        }
    }
}
