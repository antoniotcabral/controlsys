﻿using Controlsys.Dominio.Pessoas;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Pessoas
{
    /// <summary>
    /// Representa um(a) ASOMap.
    /// </summary>
    public class ASOMap : ClassMap<ASO>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Pessoas.ASOMap.
        /// </summary>
        public ASOMap()
        {
            Table("ASO");
            
            Id(b => b.Codigo, "CD_ASO");

            References(b => b.Papel, "CD_PAPEL").Not.Nullable();

            Map(b => b.DataRealizacao, "DT_REALIZACAO").Not.Nullable();
            Map(b => b.DataVencimento, "DT_VALIDADE").Not.Nullable();

        }
    }
}
