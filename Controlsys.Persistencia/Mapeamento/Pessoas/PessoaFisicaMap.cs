﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Controlsys.Dominio.Pessoas;

namespace Controlsys.Persistencia.Mapeamento.Pessoas
{
    /// <summary>
    /// Representa um(a) PessoaFisicaMap.
    /// </summary>
    public class PessoaFisicaMap : SubclassMap<PessoaFisica>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Pessoas.PessoaFisicaMap.
        /// </summary>
        public PessoaFisicaMap()
        {
            Table("PESSOA_FISICA");

            KeyColumn("CD_PESSOA_FISICA");

            Map(pf => pf.CPF, "TX_CPF").Length(11);

            References(pf => pf.Naturalidade, "CD_NATURAL").Nullable();

            Map(pf => pf.Passaporte, "TX_PASSAPORTE");
            Map(pf => pf.RG, "TX_RG");
            Map(pf => pf.RGOrgaoEmissor, "TX_RG_ORGAO");
            Map(pf => pf.RGDataEmissao, "DT_RG_EMISSAO");

            References(pf => pf.RGOrgaoEmissorUF, "CD_RG_UF").Nullable();

            Map(pf => pf.DataNascimento, "DT_NASCIMENTO");
            Map(pf => pf.Nacionalidade, "TX_NACIONALIDADE").Length(100);
            Map(pf => pf.NomeMae, "TX_MAE").Length(150);
            Map(pf => pf.NomePai, "TX_PAI").Length(150);
            Map(pf => pf.TipoSanguineo, "TX_SANGE");
            Map(pf => pf.Sexo, "TX_SEXO").Not.Nullable();
            Map(pf => pf.Foto, "ME_FOTO").CustomSqlType("VARBINARY(MAX)").Length(int.MaxValue).LazyLoad();
            Map(pf => pf.DataVisto, "DT_VISTO");
            Map(pf => pf.DataValidadeVisto, "DT_VALIDADE_VISTO");

            Map(pf => pf.RNE, "TX_RNE");
            Map(pf => pf.DataExpedicaoRNE, "DT_EXPEDICAO_RNE");
            Map(pf => pf.EstadoCivil, "TX_ESTADOCIVIL").Length(25);
            Map(pf => pf.Escolaridade, "TX_ESCOLARIDADE").Length(25);

            Map(b => b.CTPS, "NU_CT");
            Map(b => b.CTPSSerie, "TX_CT_SERIE");
            Map(b => b.CTPSData, "DT_CT_DT").Precision(4);

            References(b => b.CTPSEstado, "CD_CT_ID_UF");
            
            Map(b => b.CertificadoReservista, "NU_CR_NR");
            Map(b => b.CertificadoReservistaCat, "NU_CR_TIPO");
            Map(b => b.CNH, "TX_CNH");
            Map(b => b.CNHCategoria, "TX_CNH_TP");
            Map(b => b.CNHDataValidade, "DT_CNH_VALIDADE");
            Map(b => b.TituloEleitor, "NU_TE");
            Map(b => b.TituloEleitorZona, "NU_TE_ZONA");
            Map(b => b.TituloEleitorSecao, "NU_TE_SECAO");
            References(b => b.TituloEleitorCidade, "CD_MUNICIPIO");

            HasMany(pf => pf.Papeis)
                .KeyColumn("CD_PESSOA_FISICA");       
        }
    }
}
