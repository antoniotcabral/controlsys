﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Parametros;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Pessoas
{
    /// <summary>
    /// Representa um(a) EscalaTrabalhoMap.
    /// </summary>
    public class EscalaTrabalhoMap : ClassMap<EscalaTrabalho>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Pessoas.EscalaTrabalhoMap.
        /// </summary>
        public EscalaTrabalhoMap()
        {
            Table("ESCALA");

            Id(e => e.Codigo, "CD_ESCALA");

            References(e => e.HoraTurno, "CD_HORA_TURNO");
            References(e => e.GrupoTrabalho, "CD_GRUPO_TRABALHO");

            Map(e => e.DataEscala, "DT_ESCALA");
            Map(e => e.DataRegistro, "DT_REGISTRO");
        }
    }
}
