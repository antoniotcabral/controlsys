﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Pessoas;
using FluentNHibernate.Conventions.Helpers;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Pessoas
{
    public class EmpregadoSAPMap : ClassMap<EmpregadoSAP>
    {
        public EmpregadoSAPMap()
        {
            Table("EMPREGADO_SAP");

            Id(e => e.Codigo, "CD_EMPREGADO_SAP").GeneratedBy.Assigned();            
            References(e => e.GestorPonto, "CD_GESTOR_PONTO").LazyLoad(Laziness.False).Nullable();
            References(e => e.SuperiorImediato, "CD_SUPERIOR_IMEDIATO").LazyLoad(Laziness.False).Nullable();
        }
    }
}
