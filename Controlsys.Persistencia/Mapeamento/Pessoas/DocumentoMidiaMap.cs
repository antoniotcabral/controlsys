﻿using Controlsys.Dominio.Parametros;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Pessoas
{
    /// <summary>
    /// Representa um(a) DocumentoMidiaMap.
    /// </summary>
    public class DocumentoMidiaMap : ClassMap<DocumentoMidia>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Pessoas.DocumentoMidiaMap.
        /// </summary>
        public DocumentoMidiaMap()
        {
            Table("DOCUMENTO_MIDIA");

            Id(b => b.Codigo, "CD_DOCUMENTO_MIDIA").Not.Nullable();

            References(b => b.Midia, "CD_MIDIA").Not.Nullable();
            References(b => b.Documento, "CD_DOCUMENTO").Not.Nullable();

            Map(b => b.Ativo, "BL_ATIVO").Not.Nullable();
            Map(b => b.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(b => b.DataDesativacao, "DT_DESATIVACAO");
        }
    }
}
