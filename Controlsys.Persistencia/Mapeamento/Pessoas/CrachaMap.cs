﻿using Controlsys.Dominio.Pessoas;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Pessoas
{
    /// <summary>
    /// Representa um(a) CrachaMap.
    /// </summary>
    public class CrachaMap : ClassMap<Cracha>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Pessoas.CrachaMap.
        /// </summary>
        public CrachaMap()
        {
            Table("CRACHA");

            Id(b => b.Codigo, "CD_CRACHA");

            References(b => b.Papel, "CD_PAPEL").LazyLoad(Laziness.False).Not.Nullable();
            References(b => b.ModeloCracha, "CD_MODELO").Not.Nullable();

            Map(b => b.Numero, "TX_RFID").CustomSqlType("VARCHAR(50)");
            Map(b => b.DataDescarte, "DT_DESCARTE");
            Map(b => b.DataImpressao, "DT_IMPRESSAO");

            Map(b => b.Ativo, "BL_ATIVO").Not.Nullable();
            Map(b => b.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(b => b.DataDesativacao, "DT_DESATIVACAO");

        }
    }
}
