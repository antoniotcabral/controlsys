﻿using Controlsys.Dominio.Pessoas;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Pessoas
{
    /// <summary>
    /// Representa um(a) EmailMap.
    /// </summary>
    public class EmailMap : ClassMap<Email>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Pessoas.EmailMap.
        /// </summary>
        public EmailMap()
        {
            Table("EMAIL");

            Id(b => b.Codigo, "CD_EMAIL");

            References(c => c.Pessoa, "CD_PESSOA");

            Map(b => b.EmailDesc, "TX_EMAIL");

            Map(b => b.Ativo, "BL_ATIVO").Not.Nullable();
            Map(b => b.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(b => b.DataDesativacao, "DT_DESATIVACAO");
        }
    }
}
