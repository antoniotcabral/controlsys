﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Pessoas;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Pessoas
{
    /// <summary>
    /// Representa um(a) PessoaMap.
    /// </summary>
    public class PessoaMap : ClassMap<Pessoa>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Pessoas.PessoaMap.
        /// </summary>
        public PessoaMap()
        {
            Table("PESSOA");

            Id(p => p.Codigo, "CD_PESSOA").GeneratedBy.Assigned().Length(10);

            References(p => p.Endereco, "CD_ENDERECO");

            HasMany(p => p.Telefones).KeyColumn("CD_PESSOA");

            Map(p => p.Nome, "TX_NOME").Not.Nullable();
            Map(p => p.Email, "TX_EMAIL");
            Map(p => p.Apelido, "TX_APELIDO").Not.Nullable();
            Map(p => p.DataRegistro, "DT_REGISTRO").Not.Nullable();
        }
    }
}
