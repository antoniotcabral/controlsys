﻿using Controlsys.Dominio.Pessoas;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Pessoas
{
    /// <summary>
    /// Representa um(a) PapelLogMap.
    /// </summary>
    public class PapelLogMap : ClassMap<PapelLog>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Pessoas.PapelLogMap.
        /// </summary>
        public PapelLogMap()
        {
            Table("PAPEL_LOG");

            Id(p => p.Codigo, "CD_PAPEL_LOG");

            References(p => p.Papel, "CD_PAPEL");
            
            Map(p => p.Motivo, "TX_MOTIVO");
            Map(p => p.Status, "TX_STATUS");
            Map(p => p.DataInicio, "DT_INICIO");
            Map(p => p.DataFim, "DT_FIM");
            Map(p => p.DataRegistro, "DT_REGISTRO");

        }
    }
}
