﻿using Controlsys.Dominio.Empresas;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Pessoas
{
    /// <summary>
    /// Representa um(a) TreinamentoProjetoMap.
    /// </summary>
    public class TreinamentoProjetoMap : ClassMap<TreinamentoProjeto>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Pessoas.TreinamentoProjetoMap.
        /// </summary>
        public TreinamentoProjetoMap()
        {
            Table("TREINAMENTO_PROJETO");

            Id(p => p.Codigo, "CD_TREINAMENTO_PROJETO");

            References(p => p.Treinamento, "CD_TREINAMENTO");
            References(p => p.Projeto, "CD_PROJETO");

            Map(p => p.Obrigatorio, "BL_OBRIGATORIO");
            Map(p => p.ImpressoCracha, "BL_CRACHA");
            Map(p => p.Tag, "BI_TAG");
            Map(p => p.Observacao, "TX_OBSERVACAO");

            Map(f => f.Ativo, "BL_ATIVO").Not.Nullable();
            Map(f => f.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(f => f.DataDesativacao, "DT_DESATIVACAO");
        }
    }
}
