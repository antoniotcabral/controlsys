﻿using Controlsys.Dominio.Pessoas;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Pessoas
{
    /// <summary>
    /// Representa um(a) TreinamentoPessoaMap.
    /// </summary>
    public class TreinamentoPessoaMap : ClassMap<TreinamentoPessoa>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Pessoas.TreinamentoPessoaMap.
        /// </summary>
        public TreinamentoPessoaMap()
        {
            Table("TREINAMENTO_PESSOA");

            Id(p => p.Codigo, "CD_TREINAMENTO_PESSOA");

            References(p => p.Papel, "CD_PAPEL");
            References(p => p.Treinamento, "CD_TREINAMENTO");

            Map(p => p.DataRealizacao, "DT_REALIZADO").Precision(4);
            Map(p => p.DataValidade, "DT_VALIDADE").Precision(4);

            Map(f => f.Ativo, "BL_ATIVO").Not.Nullable();
            Map(f => f.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(f => f.DataDesativacao, "DT_DESATIVACAO");
        }
    }
}
