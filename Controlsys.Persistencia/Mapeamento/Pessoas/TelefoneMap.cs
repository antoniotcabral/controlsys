﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Pessoas;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Pessoas
{
    /// <summary>
    /// Representa um(a) TelefoneMap.
    /// </summary>
    public class TelefoneMap : ClassMap<Telefone>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Pessoas.TelefoneMap.
        /// </summary>
        public TelefoneMap()
        {
            Table("TELEFONE");

            Id(t => t.Codigo, "CD_TELEFONE");

            Map(t => t.TelefoneNum, "NU_TELEFONE").Not.Nullable();
            Map(t => t.TipoTelefone, "TX_TIPO").Not.Nullable();
            Map(t => t.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(t => t.DataDesativacao, "DT_DESATIVACAO");
            Map(t => t.Ativo, "BL_ATIVO").Not.Nullable();

            References(t => t.Pessoa, "CD_PESSOA").Not.Nullable();
        }
    }
}
