﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Audit;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Audit
{
    /// <summary>
    /// Representa um(a) LogErrorMap.
    /// </summary>
    public class LogErrorMap : ClassMap<LogError>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Audit.LogErrorMap.
        /// </summary>
        public LogErrorMap()
        {
            Table("LOG_ERROR");

            Id(lg => lg.Codigo, "CD_LOG_ERROR");

            Map(lg => lg.Message, "TX_MESSAGE");
            Map(lg => lg.StackTrace, "TX_STACK");
            Map(lg => lg.Url, "TX_URL");
            Map(lg => lg.UserName, "TX_USER");
            Map(lg => lg.DataRegistro, "DT_REGISTRO");
        }
    }
}
