﻿using Controlsys.Dominio.Audit;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Audit
{
    /// <summary>
    /// Representa um(a) IntegracaoSAPAcessoMap.
    /// </summary>
    public class IntegracaoSAPAcessoMap : ClassMap<IntegracaoSAPAcesso>
    {
        public IntegracaoSAPAcessoMap()
        {
            Table("INTEGRACAO_SAP_ACESSO");

            Id(lg => lg.Codigo, "CD_INTEGRACAO_SAP_ACESSO");
            
            References(lg => lg.ControleAcesso, "CD_ACESSO").Not.Nullable();
            References(lg => lg.IntegracaoSAP, "CD_INTEGRACAO_SAP").Not.Nullable();
        }
    }
}
