﻿using Controlsys.Dominio.Audit;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Audit
{
    /// <summary>
    ///  Representa um(a) ColaboradorAtualizadoIncluidoMap.
    /// </summary>
    public class ColaboradorAtualizadoIncluidoMap : ClassMap<ColaboradorAtualizadoIncluido>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Audit.ColaboradorAtualizadoIncluidoMap.
        /// </summary>
        public ColaboradorAtualizadoIncluidoMap()
        {
            Table("COLABORADOR_ATUALIZADO_INCLUIDO");

            Id(x => x.CDColaborador,"CD_CBR_ATUALIZADO_INCLUIDO");

            Map(x => x.CDPapel, "CD_PAPEL").Not.Nullable();
            Map(x => x.DataRegistro, "DT_REGISTRO");
            Map(x => x.MensagemRecebida, "TX_RECEBIDO");
            Map(x => x.DataEnvio, "DT_ENVIO");
            Map(x => x.MensagemLog, "TX_LOG");
        }
    }
}
