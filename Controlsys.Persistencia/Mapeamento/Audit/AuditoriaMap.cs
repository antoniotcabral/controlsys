﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Audit;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Audit
{
    /// <summary>
    /// Representa um(a) AuditoriaMap.
    /// </summary>
    public class AuditoriaMap : ClassMap<Auditoria>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Audit.AuditoriaMap.
        /// </summary>
        public AuditoriaMap()
        {
            Table("SISTEMA_AUDITORIA");

            Id(a => a.Codigo, "CD_AUDIT");

            References(a => a.Usuario, "CD_PESSOA");
            References(a => a.Acao, "NU_ACAO");

            Map(a => a.DataRegistro, "DATA_DA_OCORRENCIA");
            Map(a => a.Descricao, "TX_DESCRICAO");
        }
    }
}
