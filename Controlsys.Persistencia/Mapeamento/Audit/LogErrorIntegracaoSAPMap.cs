﻿using Controlsys.Dominio.Audit;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Audit
{
    /// <summary>
    /// Representa um(a) LogErrorMap.
    /// </summary>
    public class LogErrorIntegracaoSAPMap : ClassMap<LogErrorIntegracaoSAP>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Audit.LogErrorMap.
        /// </summary>
        public LogErrorIntegracaoSAPMap()
        {
            Table("LOG_ERROR_INTEGRACAOSAP");

            Id(lg => lg.Codigo, "CD_LOG_ERROR_INTEGRACAOSAP");

            Map(lg => lg.Message, "TX_MESSAGE");            
            Map(lg => lg.Url, "TX_URL");
            Map(lg => lg.UserName, "TX_USER");
            Map(lg => lg.DataRegistro, "DT_REGISTRO");
        }
    }
}
