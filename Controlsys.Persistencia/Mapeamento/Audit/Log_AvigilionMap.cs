﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Audit;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Audit
{
    /// <summary>
    /// Representa um(a) LogErrorMap.
    /// </summary>
    public class Log_AvigilonMap : ClassMap<Log_Avigilon>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Audit.LogErrorMap.
        /// </summary>
        public Log_AvigilonMap()
        {
            Table("LOG_AVIGILON");

            Id(lg => lg.Codigo, "CD_LOG_AVIGILON");

            Map(lg => lg.Message, "TX_MESSAGE");
            Map(lg => lg.StackTrace, "TX_STACK");
            Map(lg => lg.Url, "TX_URL");
            Map(lg => lg.UserName, "TX_USER");
            Map(lg => lg.DataRegistro, "DT_REGISTRO");
        }
    }
}
