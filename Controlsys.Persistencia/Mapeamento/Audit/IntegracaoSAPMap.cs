﻿using Controlsys.Dominio.Audit;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Controlsys.Persistencia.Mapeamento.Audit
{
    /// <summary>
    /// Representa um(a) IntegracaoSAPMap.
    /// </summary>
    public class IntegracaoSAPMap : ClassMap<IntegracaoSAP>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Audit.IntegracaoSAPMap.
        /// </summary>
        public IntegracaoSAPMap()
        {
            Table("INTEGRACAO_SAP");

            Id(lg => lg.Codigo, "CD_INTEGRACAO_SAP");

            Map(lg => lg.DataIntegracao, "DT_INTEGRACAO_SAP").Nullable();
            Map(lg => lg.Status, "TX_STATUS").Not.Nullable();
            Map(lg => lg.Mensagem, "TX_MENSAGEM").Nullable();
            Map(lg => lg.DataAtualizacao, "DT_ATUALIZACAO").Not.Nullable();
            Map(lg => lg.TipoAtualizacao, "TX_TIPO_ATUALIZACAO").Not.Nullable();
        }
    }
}