﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.CredenciamentoVeiculos;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.CredenciamentoVeiculos
{
    public class SituacaoCredencialVeiculoMap : ClassMap<SituacaoCredencialVeiculo>
    {
        public SituacaoCredencialVeiculoMap()
        {
            Table("SITUACAO_CREDENCIAL");

            Id(scv => scv.Codigo, "CD_SITUACAO_CREDENCIAL");

            Map(scv => scv.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(scv => scv.Status, "TX_SITUACAO").Length(100).Not.Nullable();
            Map(scv => scv.Observacao, "TX_OBS").Length(300).Nullable();

            References(scv => scv.Credencial, "CD_CREDENCIAL_VEICULO");
        }
    }
}
