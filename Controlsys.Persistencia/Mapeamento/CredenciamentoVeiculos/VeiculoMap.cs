﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.CredenciamentoVeiculos;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.CredenciamentoVeiculos
{
    public class VeiculoMap : ClassMap<Veiculo>
    {
        public VeiculoMap()
        {
            Table("VEICULO");

            Id(v => v.Codigo, "CD_VEICULO");

            References(v => v.Modelo, "CD_MODELO");

            Map(v => v.Cor, "TX_COR").Length(50).Not.Nullable();
            Map(v => v.Placa, "TX_PLACA").Length(10).Not.Nullable();
            Map(v => v.AnoFabricacao, "NU_ANOFAB").Not.Nullable();
            Map(v => v.AnoModelo, "NU_ANOMODELO").Not.Nullable();
            Map(v => v.DataRegistro, "DT_REGISTRO").Not.Nullable();

            HasMany(cv => cv.Midias)
                .KeyColumn("CD_VEICULO")
                .Where("DT_DESATIVACAO IS NULL").Not.LazyLoad();
        }
    }
}
