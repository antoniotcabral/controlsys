﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.CredenciamentoVeiculos;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.CredenciamentoVeiculos
{
    public class DocumentoCredencialVeiculoMap : ClassMap<DocumentoCredencialVeiculo>
    {
        public DocumentoCredencialVeiculoMap()
        {
            Table("DOCUMENTO_CREDENCIAL_VEICULO");

            Id(dcv => dcv.Codigo, "CD_DOCUMENTO_CREDENCIAL");

            Map(dcv => dcv.Ativo, "BL_ATIVO");
            Map(dcv => dcv.DataRegistro, "DT_REGISTRO");
            Map(dcv => dcv.DataDesativacao, "DT_DESATIVACAO");

            References(dcv => dcv.CredencialVeiculo, "CD_CREDENCIAL_VEICULO");
            References(dcv => dcv.Documento, "CD_DOCUMENTO");
        }
    }
}
