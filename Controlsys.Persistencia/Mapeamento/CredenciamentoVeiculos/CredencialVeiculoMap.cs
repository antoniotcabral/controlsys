﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.CredenciamentoVeiculos;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.CredenciamentoVeiculos
{
    public class CredencialVeiculoMap : ClassMap<CredencialVeiculo>
    {
        public CredencialVeiculoMap()
        {
            Table("CREDENCIAL_VEICULO");

            Id(cv => cv.Codigo, "CD_CREDENCIAL_VEICULO");

            References(cv => cv.ModeloCredencial, "CD_MODELO_CREDENCIAL");
            References(cv => cv.Condutor, "CD_PAPEL").Not.LazyLoad();
            References(cv => cv.Empresa, "CD_EMPRESA");
            References(cv => cv.Usuario, "CD_USUARIO");
            References(cv => cv.PedidoCompra, "CD_PEDIDO").Nullable();
            References(cv => cv.Area, "CD_AREA").Nullable();
            References(cv => cv.SubContratada, "CD_SUBCONTRATADA").Nullable();
            References(cv => cv.TipoValidade, "CD_TIPO_VALIDADE");

            Map(cv => cv.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(cv => cv.DataValidadeLit, "DT_VALIDADELIT").Nullable();
            Map(cv => cv.DataVencimentoSeguro, "DT_VENCIMENTO_SEG").Not.Nullable();
            Map(cv => cv.ValidadeDiasCredencial, "NU_DIASVALIDADECRED").Nullable();
            Map(cv => cv.DataVencimento, "DT_VENCIMENTO").Nullable();
            Map(cv => cv.Fornecedor, "TX_FORNECEDOR").Length(100).Nullable();
            Map(cv => cv.Observacao, "TX_OBSERVACAO").Length(1000).Nullable();
            Map(cv => cv.Corporativo, "BL_VEICULO_CORPORATIVO").Not.Nullable();

            HasMany(cv => cv.Veiculos)
                .KeyColumn("CD_CREDENCIAL_VEICULO")
                .Where("DT_DESATIVACAO IS NULL");

            HasMany(cv => cv.Situacoes)
                .KeyColumn("CD_CREDENCIAL_VEICULO");
        }
    }
}
