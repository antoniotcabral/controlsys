﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.CredenciamentoVeiculos;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.CredenciamentoVeiculos
{
    public class VeiculoCredencialVeiculoMap : ClassMap<VeiculoCredencialVeiculo>
    {
        public VeiculoCredencialVeiculoMap()
        {
            Table("VEICULO_CREDENCIAL_VEICULO");

            Id(v => v.Codigo, "CD_VEICULO_CREDENCIAL_VEICULO");

            References(v => v.CredencialVeiculo, "CD_CREDENCIAL_VEICULO");
            References(v => v.Veiculo, "CD_VEICULO").Not.LazyLoad();

            Map(v => v.Ativo, "BL_ATIVO");
            Map(v => v.DataRegistro, "DT_REGISTRO");
            Map(v => v.DataDesativacao, "DT_DESATIVACAO");
        }
    }
}
