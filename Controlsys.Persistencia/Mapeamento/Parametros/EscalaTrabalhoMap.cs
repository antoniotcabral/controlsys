﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Parametros;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Parametros
{
    /// <summary>
    /// Representa um(a) EscalaTrabalhoMap.
    /// </summary>
    public class EscalaTrabalhoMap : ClassMap<EscalaTrabalho>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Parametros.EscalaTrabalhoMap.
        /// </summary>
        public EscalaTrabalhoMap()
        {
            Table("ESCALA");

            Id(e => e.Codigo, "CD_ESCALA");

            Map(e => e.DataEscala, "DT_ESCALA");

            Map(e => e.DataRegistro, "DT_REGISTRO");

            References(e => e.GrupoTrabalho, "CD_GRUPO_TRABALHO");

            References(e => e.HoraTurno, "CD_HORA_TURNO");
        }
    }
}


