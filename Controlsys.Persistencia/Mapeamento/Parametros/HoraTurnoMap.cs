﻿using Controlsys.Dominio.Parametros;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Parametros
{
    /// <summary>
    /// Representa um(a) HoraTurnoMap.
    /// </summary>
    public class HoraTurnoMap : ClassMap<HoraTurno>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Parametros.HoraTurnoMap.
        /// </summary>
        public HoraTurnoMap()
        {
            Table("HORA_TURNO");

            Id(ht => ht.Codigo, "CD_HORA_TURNO");

            References(ht => ht.Turno, "CD_TURNO");

            Map(ht => ht.Ordem, "NU_SEQ").Not.Nullable();
            Map(ht => ht.HoraInicio, "HR_INICIO").Precision(7).CustomType("TimeAsTimeSpan");
            Map(ht => ht.HoraFim, "HR_FIM").Precision(7).CustomType("TimeAsTimeSpan");
            Map(ht => ht.Trabalha, "BL_TRABALHA");
            Map(ht => ht.Maior24h, "BL_24H");

            Map(b => b.Ativo, "BL_ATIVO").Not.Nullable();
            Map(b => b.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(b => b.DataDesativacao, "DT_DESATIVACAO");

            Map(ht => ht.DiaSemana, "NU_DIA_SEMANA").CustomType<DiaSemana?>().Nullable();
        }
    }
}
