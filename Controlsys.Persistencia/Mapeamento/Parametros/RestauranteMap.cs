﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Parametros;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Parametros
{
    public class RestauranteMap : ClassMap<Restaurante>
    {
        public RestauranteMap()
        {
            Table("RESTAURANTE");

            Id(r => r.Codigo, "CD_RESTAURANTE");

            References(r => r.GrupoLeitora, "CD_GRUPO_LEITORA");
            Map(r => r.Nome, "TX_NOME");
            Map(r => r.Ativo, "BL_ATIVO");
            Map(r => r.DataRegistro, "DT_REGISTRO");
            Map(r => r.DataDesativacao, "DT_DESATIVACAO");

            HasMany(r => r.TiposAlimentacao)
                .Table("RESTAURANTE_TIPOALIMENTACAO")
               .KeyColumn("CD_RESTAURANTE")
               .Where("DT_DESATIVACAO IS NULL");
        }
    }
}
