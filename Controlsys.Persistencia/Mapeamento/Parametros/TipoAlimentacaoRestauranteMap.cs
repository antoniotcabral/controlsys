﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Parametros;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Parametros
{
    public class TipoAlimentacaoRestauranteMap : ClassMap<TipoAlimentacaoRestaurante>
    {
        public TipoAlimentacaoRestauranteMap()
        {
            Table("RESTAURANTE_TIPOALIMENTACAO");

            Id(tar => tar.Codigo, "CD_RESTAURANTE_TIPOALIMENTACAO");

            References(tar => tar.Restaurante, "CD_RESTAURANTE");
            References(tar => tar.TipoAlimentacao, "CD_TIPOALIMENTACAO");

            Map(tar => tar.Ativo, "BL_ATIVO");
            Map(tar => tar.DataRegistro, "DT_REGISTRO");
            Map(tar => tar.DataDesativacao, "DT_DESATIVACAO");
            Map(tar => tar.HoraInicio, "HR_INICIO").CustomType("TimeAsTimeSpan");
            Map(tar => tar.HoraFim, "HR_FIM").CustomType("TimeAsTimeSpan");

        }
    }
}
