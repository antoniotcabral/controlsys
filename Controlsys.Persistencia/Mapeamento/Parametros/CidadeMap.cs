﻿using Controlsys.Dominio.Enderecos;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Parametros
{
    /// <summary>
    /// Representa um(a) CidadeMap.
    /// </summary>
    public class CidadeMap : ClassMap<Cidade>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Parametros.CidadeMap.
        /// </summary>
        public CidadeMap()
        {
            Table("CIDADE");

            Id(f => f.Codigo, "CD_CIDADE");

            References(f => f.Estado, "CD_ESTADO");

            Map(f => f.Nome, "TX_NOME").Length(100).Not.Nullable();

            Map(f => f.Ativo, "BL_ATIVO").Not.Nullable();
            Map(f => f.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(f => f.DataDesativacao, "DT_DESATIVACAO");


        }
    }
}
