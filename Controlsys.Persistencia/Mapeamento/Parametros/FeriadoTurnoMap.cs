﻿using Controlsys.Dominio.Parametros;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Parametros
{
    /// <summary>
    /// Representa um(a) FeriadoTurnoMap.
    /// </summary>
    public class FeriadoTurnoMap : ClassMap<FeriadoTurno>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Parametros.FeriadoTurnoMap.
        /// </summary>
        public FeriadoTurnoMap()
        {
            Table("FERIADO_TURNO");

            Id(ft => ft.Codigo, "CD_FERIADO_TURNO").Not.Nullable();

            References(ft => ft.Feriado, "CD_FERIADO").Not.Nullable();
            References(ft => ft.Turno, "CD_TURNO").Not.Nullable();

            Map(b => b.Ativo, "BL_ATIVO").Not.Nullable();
            Map(b => b.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(b => b.DataDesativacao, "DT_DESATIVACAO");
        }
    }
}
