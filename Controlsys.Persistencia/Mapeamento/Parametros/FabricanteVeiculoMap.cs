﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Parametros;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Parametros
{
    public class FabricanteVeiculoMap : ClassMap<FabricanteVeiculo>
    {
        public FabricanteVeiculoMap()
        {
            Table("FABRICANTE_VEICULO");

            Id(fv => fv.Codigo, "CD_FABRICANTE_VEICULO");

            Map(fv => fv.Ativo, "BL_ATIVO").Not.Nullable();
            Map(fv => fv.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(fv => fv.DataDesativacao, "DT_DESATIVACAO").Nullable();

            Map(fv => fv.Nome, "TX_NOME").Length(100).Not.Nullable();
        }
    }
}
