﻿using Controlsys.Dominio.Parametros;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Parametros
{
    /// <summary>
    /// Representa um(a) GrupoTrabalhoMap.
    /// </summary>
    public class GrupoTrabalhoMap : ClassMap<GrupoTrabalho>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Parametros.GrupoTrabalhoMap.
        /// </summary>
        public GrupoTrabalhoMap()
        {
            Table("GRUPO_TRABALHO");

            Id(gt => gt.Codigo, "CD_GRUPO_TRABALHO");

            Map(gt => gt.CodigoPHT, "NU_PHT");
            Map(gt => gt.Nome, "TX_NOME").Length(50).Not.Nullable();
            Map(gt => gt.Ativo, "BL_ATIVO").Not.Nullable();
            Map(gt => gt.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(gt => gt.DataDesativacao, "DT_DESATIVACAO");

            HasMany(gtt => gtt.GrupoTrabalhoTurno)
               .Table("GRUPO_TRABALHO_TURNO")
               .KeyColumn("CD_GRUPO_TRABALHO")
               .Where("DT_DESATIVACAO IS NULL");

            HasMany(gtt => gtt.GrupoTrabalhoColaboradores)
               .Table("GRUPO_TRAB_COLAB")
               .KeyColumn("CD_GRUPO")
               .Where("DT_DESATIVACAO IS NULL");
        }
    }
}
