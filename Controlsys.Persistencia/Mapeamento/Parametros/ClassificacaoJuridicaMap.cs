﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Parametros;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Parametros
{
    public class ClassificacaoJuridicaMap : ClassMap<ClassificacaoJuridica>
    {
        public ClassificacaoJuridicaMap()
        {
            Table("CLASSIFICACAO_JURIDICA");

            Id(cj => cj.Codigo, "CD_CLASSIFICACAO_JURIDICA");

            Map(cj => cj.ItemClassificacao, "TX_ITEM_CLASSIFICACAO").Not.Nullable();
            Map(cj => cj.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(cj => cj.DataDesativacao, "DT_DESATIVACAO").Nullable();
            Map(cj => cj.Letra, "TX_LETRA").Not.Nullable();
            Map(cj => cj.Formula, "TX_FORMULA").Nullable();
            Map(cj => cj.Peso, "NU_PESO").Nullable();
            Map(cj => cj.Pontua, "BL_PONTUA").Not.Nullable();
            Map(cj => cj.Ativo, "BL_ATIVO").Not.Nullable();

        }
    }
}

