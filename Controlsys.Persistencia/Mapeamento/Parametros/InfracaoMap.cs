﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Parametros;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Parametros
{
    public class InfracaoMap : ClassMap<Infracao>
    {
        public InfracaoMap()
        {
            Table("INFRACAO");

            Id(i => i.Codigo, "CD_INFRACAO");

            Map(i => i.Nome, "TX_NOME");
            Map(i => i.DataRegistro, "DT_REGISTRO");
            Map(i => i.DataDesativacao, "DT_DESATIVACAO");
            Map(i => i.Ativo, "BL_ATIVO");
            Map(i => i.Sigla, "TX_SIGLA");
            Map(i => i.Grau, "TX_GRAU");
        }
    }
}
