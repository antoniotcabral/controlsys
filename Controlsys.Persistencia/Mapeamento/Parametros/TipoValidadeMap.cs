﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Parametros;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Parametros
{
    public class TipoValidadeMap : ClassMap<TipoValidade>
    {
        public TipoValidadeMap()
        {
            Table("TIPO_VALIDADE");

            Id(tv => tv.Codigo, "CD_TIPO_VALIDADE");

            Map(tv => tv.Nome, "TX_NOME").Not.Nullable().Length(100);
            Map(tv => tv.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(tv => tv.DataDesativacao, "DT_DESATIVACAO").Nullable();
            Map(tv => tv.Ativo, "BL_ATIVO").Not.Nullable();
            Map(tv => tv.DiasValidade, "NU_VALIDADE").Not.Nullable();
            Map(tv => tv.IdadeInicial, "NU_IDADE_INI").Not.Nullable();
            Map(tv => tv.IdadeFinal, "NU_IDADE_FIM").Not.Nullable();
        }
    }
}
