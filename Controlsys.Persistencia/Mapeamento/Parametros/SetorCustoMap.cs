﻿using Controlsys.Dominio.Empresas;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Parametros
{
    /// <summary>
    /// Representa um(a) SetorCustoMap.
    /// </summary>
    public class SetorCustoMap : ClassMap<SetorCusto>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Parametros.SetorCustoMap.
        /// </summary>
        public SetorCustoMap()
        {
            Table("SETOR_CUSTO");

            Id(b => b.Codigo, "CD_SETOR");

            Map(b => b.Nome, "TX_NOME").Not.Nullable().Length(50);
            Map(b => b.Descricao, "TX_DESCRICAO").Length(250);
            Map(b => b.TipoSetor, "TX_TIPO").Length(15);
            Map(b => b.Numero, "NU_SETOR").Not.Nullable().Length(30);
            Map(b => b.Ativo, "BL_ATIVO").Not.Nullable();
            Map(b => b.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(b => b.DataDesativacao, "DT_DESATIVACAO");
            Map(b => b.IDResponsavelSAP, "TX_ID_RESPONSAVEL_SAP");
            References(b => b.Responsavel, "CD_RESPONSAVEL");
        }
    }
}
