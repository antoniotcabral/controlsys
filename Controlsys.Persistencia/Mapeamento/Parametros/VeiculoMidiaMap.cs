﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Parametros;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Parametros
{
    public class VeiculoMidiaMap : ClassMap<VeiculoMidia>
    {
        public VeiculoMidiaMap()
        {
            Table("VEICULO_MIDIA");

            Id(v => v.Codigo, "CD_VEICULO_MIDIA");

            References(v => v.Midia, "CD_MIDIA").Not.Nullable();
            References(v => v.Veiculo, "CD_VEICULO").Not.Nullable();

            Map(v => v.Ativo, "BL_ATIVO").Not.Nullable();
            Map(v => v.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(v => v.DataDesativacao, "DT_DESATIVACAO").Nullable();
        }
    }
}
