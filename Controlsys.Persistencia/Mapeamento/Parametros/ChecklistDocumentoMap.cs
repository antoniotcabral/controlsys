﻿using Controlsys.Dominio.Parametros;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Parametros
{
    /// <summary>
    /// Representa um(a) ChecklistDocumentoMap.
    /// </summary>
    public class ChecklistDocumentoMap : ClassMap<ChecklistDocumento>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Parametros.ChecklistDocumentoMap.
        /// </summary>
        public ChecklistDocumentoMap()
        {
            Table("CHECKLIST");

            Id(b => b.Codigo, "CD_CHECKLIST");

            Map(b => b.Nome, "TX_NOME").Not.Nullable().Length(50);
            Map(b => b.Descricao, "TX_DESCRICAO").Length(500);
            Map(b => b.TipoCadastro, "TX_DOCUMENTO").Length(50).Not.Nullable();

            Map(b => b.Ativo, "BL_ATIVO").Not.Nullable();
            Map(b => b.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(b => b.DataDesativacao, "DT_DESATIVACAO");

            HasMany(p => p.TiposDocumento)
                .KeyColumn("CD_CHECKLIST")
                .Where("DT_DESATIVACAO IS NULL");
        }
    }
}
