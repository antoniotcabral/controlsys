﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Parametros;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Parametros
{
    public class TipoOcorrenciaMap : ClassMap<TipoOcorrencia>
    {
        public TipoOcorrenciaMap()
        {
            Table("TIPO_OCORRENCIA");

            Id(to => to.Codigo, "CD_TIPO_OCORRENCIA");

            Map(to => to.DataRegistro, "DT_REGISTRO");
            Map(to => to.DataDesativacao, "DT_DESATIVACAO");
            Map(to => to.Descricao, "TX_DESCRICAO");
            Map(to => to.Ativo, "BL_ATIVO");
        }
    }
}
