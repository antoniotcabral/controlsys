﻿using Controlsys.Dominio.Acesso;
using Controlsys.Dominio.Parametros;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Parametros
{
    /// <summary>
    /// Representa um(a) MotivoBloqueioMap.
    /// </summary>
    public class MotivoBloqueioMap : ClassMap<MotivoBloqueio>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Parametros.MotivoBloqueioMap.
        /// </summary>
        public MotivoBloqueioMap()
        {
            Table("MOTIVO_BLOQUEIO");

            Id(mb => mb.Codigo, "CD_MOTIVO_BLOQUEIO");

            Map(mb => mb.Nome, "TX_NOME").Length(50).Not.Nullable();
            Map(mb => mb.Descricao, "TX_DESCRICAO").Length(500);

            Map(mb => mb.Ativo, "BL_ATIVO").Not.Nullable();
            Map(mb => mb.DataDesativacao, "DT_DESATIVACAO");
        }
    }
}
