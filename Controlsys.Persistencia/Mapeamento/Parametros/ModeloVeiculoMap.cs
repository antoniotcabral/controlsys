﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Parametros;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Parametros
{
    public class ModeloVeiculoMap : ClassMap<ModeloVeiculo>
    {
        public ModeloVeiculoMap()
        {
            Table("MODELO_VEICULO");

            Id(mv => mv.Codigo, "CD_MODELO_VEICULO");

            References(mv => mv.Fabricante, "CD_FABRICANTE");

            Map(mv => mv.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(mv => mv.DataDesativacao, "DT_DESATIVACAO").Nullable();
            Map(mv => mv.Nome, "TX_NOME").Length(100).Not.Nullable();
            Map(mv => mv.Ativo, "BL_ATIVO").Not.Nullable();
        }
    }
}
