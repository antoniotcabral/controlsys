﻿using Controlsys.Dominio.Parametros;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Parametros
{
    /// <summary>
    /// Representa um(a) GrupoTrabalhoTurnoMap.
    /// </summary>
    public class GrupoTrabalhoTurnoMap : ClassMap<GrupoTrabalhoTurno>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Parametros.GrupoTrabalhoTurnoMap.
        /// </summary>
        public GrupoTrabalhoTurnoMap()
        {
            Table("GRUPO_TRABALHO_TURNO");

            Id(gtt => gtt.Codigo, "CD_GRUPO_TRABALHO_TURNO");

            References(gtt => gtt.Turno, "CD_TURNO").Not.Nullable();
            References(gtt => gtt.GrupoTrabalho, "CD_GRUPO_TRABALHO").Not.Nullable();

            Map(gtt => gtt.DataInicio, "DT_INICIO");
            Map(gtt => gtt.DataFim, "DT_FIM");
            Map(gtt => gtt.Ativo, "BL_ATIVO").Not.Nullable();
            Map(gtt => gtt.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(gtt => gtt.DataDesativacao, "DT_DESATIVACAO");

        }
    }
}
