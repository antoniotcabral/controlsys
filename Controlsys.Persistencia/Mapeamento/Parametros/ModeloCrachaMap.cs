﻿using Controlsys.Dominio.Parametros;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Parametros
{
    /// <summary>
    /// Representa um(a) ModeloCrachaMap.
    /// </summary>
    public class ModeloCrachaMap : ClassMap<ModeloCracha>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Parametros.ModeloCrachaMap.
        /// </summary>
        public ModeloCrachaMap()
        {
            Table("MODELO_CRACHA");

            Id(mc => mc.Codigo, "CD_MODELO");

            Map(mc => mc.Nome, "TX_MODELO").Length(25).Not.Nullable();
            //Map(mc => mc.Descricao, "TX_DESCRICAO").Length(150);
            Map(mc => mc.TipoPapel, "TX_LAYOUT");
            Map(mc => mc.Html, "TX_HTML").Length(150);
            Map(mc => mc.Fundo, "ME_FUNDO").CustomSqlType("VARBINARY(MAX)").Length(int.MaxValue);
            Map(mc => mc.Ativo, "BL_ATIVO").Not.Nullable();
            Map(mc => mc.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(mc => mc.DataDesativacao, "DT_DESATIVACAO");
            Map(b => b.Horizontal, "BL_HORIZONTAL").Nullable();
        }
    }
}
