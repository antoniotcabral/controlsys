﻿using Controlsys.Dominio.Parametros;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Parametros
{
    /// <summary>
    /// Representa um(a) ChecklistTipoDocumentoMap.
    /// </summary>
    public class ChecklistTipoDocumentoMap : ClassMap<ChecklistTipoDocumento>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Parametros.ChecklistTipoDocumentoMap.
        /// </summary>
        public ChecklistTipoDocumentoMap()
        {
            Table("CHECKLIST_DOC");

            Id(b => b.Codigo, "CD_CHECKLIST_DOC").Not.Nullable();

            References(b => b.TipoDocumento, "CD_TIPO_DOC").Not.Nullable();
            References(b => b.ChecklistDocumento, "CD_CHECKLIST").Not.Nullable();

            Map(b => b.Ativo, "BL_ATIVO").Not.Nullable();
            Map(b => b.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(b => b.DataDesativacao, "DT_DESATIVACAO");
        }
    }
}
