﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Parametros;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Parametros
{
    public class ModeloCredencialMap : ClassMap<ModeloCredencial>
    {
        public ModeloCredencialMap()
        {
            Table("MODELO_CREDENCIAL");

            Id(mc => mc.Codigo, "CD_MODELO_CREDENCIAL");

            Map(mc => mc.Nome, "TX_NOME").Length(100).Not.Nullable();
            Map(mc => mc.Ativo, "BL_ATIVO").Not.Nullable();
            Map(mc => mc.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(mc => mc.DataDesativacao, "DT_DESATIVACAO").Nullable();
            Map(mc => mc.FundoFrente, "ME_FUNDO_FRENTE").CustomSqlType("VARBINARY(MAX)").Length(int.MaxValue).Nullable();
            Map(mc => mc.FundoVerso, "ME_FUNDO_VERSO").CustomSqlType("VARBINARY(MAX)").Length(int.MaxValue).Nullable();
            Map(mc => mc.HtmlFrente, "ME_HTML_FRENTE").CustomSqlType("VARBICHAR(MAX)").Length(int.MaxValue).Not.Nullable();
            Map(mc => mc.HtmlVerso, "ME_HTML_VERSO").CustomSqlType("VARBICHAR(MAX)").Length(int.MaxValue).Nullable();
            Map(mc => mc.TipoCredencial, "TX_TIPO").Length(100).Not.Nullable();
            Map(mc => mc.Altura, "NM_ALTURA").Not.Nullable();
            Map(mc => mc.Largura, "NM_LARGURA").Not.Nullable();
        }
    }
}
