﻿using Controlsys.Dominio.Parametros;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Parametros
{
    /// <summary>
    /// Representa um(a) FeriadoMap.
    /// </summary>
    public class FeriadoMap : ClassMap<Feriado>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Parametros.FeriadoMap.
        /// </summary>
        public FeriadoMap()
        {
            Table("FERIADO");

            Id(f => f.Codigo, "CD_FERIADO");
            
            Map(f => f.DataFeriado, "DT_FERIADO").Not.Nullable(); 

            Map(f => f.Nome, "TX_NOME").Length(100).Not.Nullable();
            Map(f => f.Descricao, "TX_DESCRICAO").Length(250);

            Map(f => f.Ativo, "BL_ATIVO").Not.Nullable();
            Map(f => f.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(f => f.DataDesativacao, "DT_DESATIVACAO");


        }
    }
}
