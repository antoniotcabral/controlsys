﻿using Controlsys.Dominio.Parametros;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Parametros
{
    /// <summary>
    /// Representa um(a) ParametroMap.
    /// </summary>
    public class ParametroMap : ClassMap<Parametro>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Parametros.ParametroMap.
        /// </summary>
        public ParametroMap()
        {
            Table("PARAMETRO");

            Id(p => p.Codigo, "CD_PARAMETRO");

            Map(p => p.Nome, "TX_NOME").Not.Nullable();
            Map(p => p.Valor, "TX_VALOR").Length(500).Not.Nullable();
            Map(p => p.TipoParametro, "TX_TIPO").Length(50).Not.Nullable();
            Map(p => p.Url, "TX_URL").Length(100);
            Map(p => p.ExecutaProcedimento, "BL_EXECUTA_PROCEDIMENTO");
        }
    }
}
