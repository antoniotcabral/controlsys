﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Parametros;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Parametros
{
    /// <summary>
    /// Representa um(a) TipoDocumentoMap.
    /// </summary>
    public class TipoDocumentoMap : ClassMap<TipoDocumento>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Parametros.TipoDocumentoMap.
        /// </summary>
        public TipoDocumentoMap()
        {
            Table("TIPO_DOCUMENTO");

            Id(tp => tp.Codigo, "CD_TIPO_DOC");

            Map(tp => tp.Nome, "TX_NOME").Not.Nullable().Length(300);
            Map(tp => tp.Sigla, "TX_SIGLA").Not.Nullable().Length(10);
            //Map(tp => tp.DiasValidade, "NU_DIAS").Not.Nullable();
            Map(tp => tp.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(tp => tp.DataDesativacao, "DT_DESATIVACAO");
            Map(tp => tp.Ativo, "BL_ATIVO").Not.Nullable();
            Map(tp => tp.Bloqueio, "BL_BLOQUEIO").Not.Nullable();
        }
    }
}
