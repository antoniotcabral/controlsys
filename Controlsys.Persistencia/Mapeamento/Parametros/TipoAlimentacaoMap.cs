﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Parametros;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Parametros
{
    public class TipoAlimentacaoMap : ClassMap<TipoAlimentacao>
    {
        public TipoAlimentacaoMap()
        {
            Table("TIPO_ALIMENTACAO");

            Id(ta => ta.Codigo, "CD_TIPOALIMENTACAO");

            Map(ta => ta.Nome, "TX_NOME");
            Map(ta => ta.Ativo, "BL_ATIVO");
            Map(ta => ta.DataRegistro, "DT_REGISTRO");
            Map(ta => ta.DataDesativacao, "DT_DESATIVACAO");
        }
    }
}
