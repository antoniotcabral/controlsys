﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Parametros;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Parametros
{
    public class SindicatoMap : ClassMap<Sindicato>
    {
        public SindicatoMap()
        {
            Table("SINDICATO");

            Id(s => s.Codigo, "CD_SINDICATO");

            Map(s => s.Nome, "TX_NOME");
            Map(s => s.DataRegistro, "DT_REGISTRO");
            Map(s => s.DataDesativacao, "DT_DESATIVACAO");
            Map(s => s.Ativo, "BL_ATIVO");
            Map(s => s.Razao, "TX_RAZAO");
            Map(s => s.Cnpj, "TX_CNPJ");
            Map(s => s.Email, "TX_EMAIL");
            Map(s => s.Telefone, "TX_TELEFONE");
            Map(s => s.TelEmergencia, "TX_TELEMERGENCIA");
            Map(s => s.Celular, "TX_CELULAR");
        }
    }
}
