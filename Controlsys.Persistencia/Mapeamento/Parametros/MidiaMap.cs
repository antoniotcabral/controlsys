﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Parametros;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Parametros
{
    public class MidiaMap : ClassMap<Midia>
    {
        public MidiaMap()
        {
            Table("MIDIA");

            Id(m => m.Codigo, "CD_MIDIA");

            Map(m => m.DataRegistro, "DT_REGISTRO");

            Map(m => m.Extensao, "TX_EXTENSAO");
            Map(m => m.MimeType, "TX_MYME");
            Map(m => m.NomeArquivo, "TX_NOMEARQUIVO");
        }
    }
}
