﻿using Controlsys.Dominio.Parametros;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Parametros
{
    /// <summary>
    /// Representa um(a) TurnoMap.
    /// </summary>
    public class TurnoMap : ClassMap<Turno>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Parametros.TurnoMap.
        /// </summary>
        public TurnoMap()
        {
            Table("TURNO");

            Id(p => p.Codigo, "CD_TURNO");

            Map(p => p.Nome, "TX_NOME").Not.Nullable();
            Map(p => p.ObedeceEscala, "BL_ESCALA").Not.Nullable();
            Map(p => p.ConsideraFeriado, "BL_FERIADO").Not.Nullable();
            
            Map(f => f.Ativo, "BL_ATIVO").Not.Nullable();
            Map(f => f.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(f => f.DataDesativacao, "DT_DESATIVACAO");

            Map(f => f.Tolerancia, "NU_TOLERANCIA");

            HasMany(p => p.HorasTurno).KeyColumn("CD_TURNO").Where("DT_DESATIVACAO IS NULL");
            HasMany(p => p.FeriadosTurno).KeyColumn("CD_TURNO").Where("DT_DESATIVACAO IS NULL");
        }
    }
}
