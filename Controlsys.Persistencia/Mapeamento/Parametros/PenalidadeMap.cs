﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Parametros;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Parametros
{
    public class PenalidadeMap : ClassMap<Penalidade>
    {
        public PenalidadeMap()
        {
            Table("PENALIDADE");

            Id(p => p.Codigo, "CD_PENALIDADE");

            Map(p => p.Sigla, "TX_SIGLA");
            Map(p => p.Nome, "TX_PENALIDADE");
            Map(p => p.DataRegistro, "DT_REGISTRO");
            Map(p => p.DataDesativacao, "DT_DESATIVACAO");
            Map(p => p.Ativo, "BL_ATIVO");
        }
    }
}
