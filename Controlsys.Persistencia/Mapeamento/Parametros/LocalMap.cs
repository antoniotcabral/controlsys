﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Parametros;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Parametros
{
    public class LocalMap : ClassMap<Local>
    {
        public LocalMap()
        {
            Table("LOCAL");

            Id(l => l.Codigo, "CD_LOCAL");

            Map(l => l.Nome, "TX_NOME");
            Map(l => l.Sigla, "TX_SIGLA");
            Map(l => l.Ativo, "BL_ATIVO");
            Map(l => l.DataRegistro, "DT_REGISTRO");
            Map(l => l.DataDesativacao, "DT_DESATIVACAO");
        }
    }
}
