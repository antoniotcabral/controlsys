﻿using Controlsys.Dominio.Relatorio;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Relatorio
{
    public class RelatorioCrachaPeriodoMap : ClassMap<RelatorioCrachaPeriodo>
    {
        public RelatorioCrachaPeriodoMap()
        {
            Table("VW_RELATORIO_CRACHAS_EMITIDOS_PERIODO");

            Id(r => r.Codigo, "CD_RELATORIO");
            Map(r => r.CodigoPapel, "CD_PAPEL");
            Map(r => r.CodigoPessoaFisica, "CD_PESSOA_FISICA");
            Map(r => r.CPF, "TX_CPF");
            Map(r => r.Passaporte, "TX_PASSAPORTE");
            Map(r => r.RG, "TX_RG");
            Map(r => r.Nome, "TX_NOME");
            Map(r => r.Cargo, "TX_CARGO");
            Map(r => r.TipoPapel, "TX_TIPO_PAPEL");
            Map(r => r.DataRegistroPessoa, "DT_REGISTRO_PESSOA");
            Map(r => r.DataRegistroPapel, "DT_REGISTRO_PAPEL");
            Map(r => r.StatusPapel, "TX_STATUS_PAPEL");
            Map(r => r.DataRegistroStatusPapel, "DT_REGISTRO_STATUS_PAPEL");
            Map(r => r.DataValidadeASO, "DT_VALIDADE_ASO");
            Map(r => r.MotivoPapelLog, "TX_MOTIVO_PAPEL_LOG");
            Map(r => r.Cracha, "TX_CRACHA");
            Map(r => r.StatusCracha, "BO_STATUS_CRACHA");
            Map(r => r.DataRegistroCracha, "DT_REGISTRO_CRACHA");
            Map(r => r.CNPJ, "CNPJ");
            Map(r => r.RazaoSocial, "TX_RAZAO_SOCIAL");
            Map(r => r.NomeFantasia, "TX_NOME_FANTASIA");
        }
    }
}
