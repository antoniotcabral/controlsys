﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Relatorio;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Relatorio
{
    public class RelatorioUltimoCrachaPapelMap : ClassMap<RelatorioUltimoCrachaPapel>
    {
        public RelatorioUltimoCrachaPapelMap()
        {
            Table("VW_RELATORIO_ULTIMO_CRACHA_PAPEL");

            Id(r => r.CodigoPapel, "CD_PAPEL");
            Map(r => r.TipoPapel, "TX_TIPO_PAPEL");
            Map(r => r.NomePessoa, "TX_NOME_PESSOA");
            Map(r => r.Cargo, "TX_CARGO");
            Map(r => r.CPF, "TX_CPF");
            Map(r => r.Passaporte, "TX_PASSAPORTE");
            Map(r => r.Status, "TX_STATUS");
            Map(r => r.CNPJEmpresa, "TX_CNPJ_EMPRESA");
            Map(r => r.ApelidoEmpresa, "TX_APELIDO_EMPRESA");
            Map(r => r.NomeEmpresa, "TX_NOME_EMPRESA");
            Map(r => r.CNPJEmpresaAlocacao, "TX_CNPJ_EMPRESA_ALOCACAO");
            Map(r => r.ApelidoEmpresaAlocacao, "TX_APELIDO_EMPRESA_ALOCACAO");
            Map(r => r.NomeEmpresaAlocacao, "TX_NOME_EMPRESA_ALOCACAO");
            Map(r => r.CNPJContratada, "TX_CNPJ_CONTRATADA");
            Map(r => r.ApelidoContratada, "TX_APELIDO_CONTRATADA");
            Map(r => r.NomeContratada, "TX_NOME_CONTRATADA");
            Map(r => r.CodigoCracha, "CD_CRACHA");
            Map(r => r.RFID, "TX_RFID");
            Map(r => r.StatusCracha, "BL_STATUS_CRACHA");
            Map(r => r.DataRegistroCracha, "DT_REGISTRO_CRACHA");
            Map(r => r.DataDesativacaoCracha, "DT_DESATIVACAO_CRACHA");
            Map(r => r.DataDescarteCracha, "DT_DESCARTE_CRACHA");
            Map(r => r.DataImpressaoCracha, "DT_IMPRESSAO_CRACHA");
        }
    }
}
