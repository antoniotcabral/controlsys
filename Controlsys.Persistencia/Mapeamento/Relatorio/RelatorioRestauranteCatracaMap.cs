﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Relatorio;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Relatorio
{
    public class RelatorioRestauranteCatracaMap : ClassMap<RelatorioRestauranteCatraca>
    {
        public RelatorioRestauranteCatracaMap()
        {
            Table("VW_RelatorioRestauranteCatraca");            

            Id(r => r.CodigoAcesso, "CD_ACESSO");
            Map(r => r.CodigoPapel, "CD_PAPEL");
            Map(r => r.CodigoEmpresa, "CD_EMPRESA");
            Map(r => r.CodigoTipoAlimentacao, "CD_TIPOALIMENTACAO");
            Map(r => r.CodigoSetorCusto, "CD_SETOR");
            Map(r => r.CodigoRestaurante, "CD_RESTAURANTE");
            Map(r => r.NumeroPO, "NU_PEDIDO");
            Map(r => r.Empresa, "TX_EMPRESA");
            Map(r => r.Restaurante, "TX_RESTAURANTE");
            Map(r => r.TipoAlimentacao, "TX_TIPOALIMENTACAO");
            Map(r => r.DataLeitora, "DT_LEITORA");
        }
    }
}
