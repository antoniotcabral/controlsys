﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.AgendamentosPessoa;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Agendamentos
{
    public class PrestadorServicoAgendamentoMap : SubclassMap<PrestadorServicoAgendamento>
    {
        public PrestadorServicoAgendamentoMap()
        {
            Table("PRESTADOR_AGENDAMENTO");

            KeyColumn("CD_PAPEL_AGENDAMENTO");

            References(ps => ps.Fornecedor, "CD_FORNECEDOR");
            References(pa => pa.Empresa, "CD_EMPRESA");
            References(pa => pa.GestorResponsavel, "CD_RESPONSAVEL");

            Map(ps => ps.DataAdmissao, "DT_ADMISSAO");
            Map(ps => ps.Observacao, "TX_OBS").Length(1000);
            Map(ps => ps.TipoPrestador, "TX_TIPO");            
            
        }
    }
}
