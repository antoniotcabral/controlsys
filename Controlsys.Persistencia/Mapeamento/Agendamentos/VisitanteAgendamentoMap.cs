﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.AgendamentosPessoa;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Agendamentos
{
    public class VisitanteAgendamentoMap : SubclassMap<VisitanteAgendamento>
    {
        public VisitanteAgendamentoMap()
        {
            Table("VISITANTE_AGENDAMENTO");

            KeyColumn("CD_PAPEL_AGENDAMENTO");

            References(c => c.ResponsavelVisita, "CD_RESPONSAVEL");

            Map(c => c.Empresa, "TX_EMPRESA").Length(150).Not.Nullable();
            Map(c => c.AcessoPorto, "BL_ACESSO_PORTO").Not.Nullable();
            Map(c => c.DataInicio, "DT_INICIO").Not.Nullable();
            Map(c => c.DataFim, "DT_FIM").Not.Nullable();
            Map(c => c.Funcao, "TX_FUNCAO").Length(150).Not.Nullable();
            Map(c => c.Motivo, "TX_MOTIVO").Length(1000).Not.Nullable();



            
        }
    }
}
