﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.AgendamentosPessoa;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Agendamentos
{
    public class ColaboradorAgentamentoMap : SubclassMap<ColaboradorAgendamento>
    {
        public ColaboradorAgentamentoMap()
        {
            Table("COLABORADOR_AGENDAMENTO");

            KeyColumn("CD_PAPEL_AGENDAMENTO");

            References(c => c.GrupoTrabalho, "CD_GRUPO_TRABALHO");
            References(pa => pa.Empresa, "CD_EMPRESA");

            Map(c => c.ConselhoProfissional, "TX_CONSPROF");
            Map(c => c.RegistroProfissional, "TX_REGISPROF");
            Map(c => c.DataAdmissao, "DT_ADMISSAO");
            Map(c => c.HoristaMensalista, "TX_HORMENS");
            Map(c => c.Observacao, "TX_OBS");
            Map(c => c.PIS, "TX_PIS");
            Map(c => c.Salario, "DE_SAL");
        }
    }
}
