﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.AgendamentosPessoa;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Agendamentos
{
    public class StatusPapelAgendamentoMap : ClassMap<StatusPapelAgendamento>
    {
        public StatusPapelAgendamentoMap()
        {
            Table("STATUS_PAPELAGENDAMENTO");

            Id(spa => spa.Codigo, "CD_STATUS_PAPELAGENDAMENTO");

            References(spa => spa.PapelAgendamento, "CD_PAPEL_AGENDAMENTO");

            Map(spa => spa.DataRegistro, "DT_REGISTRO");
            Map(spa => spa.Observacao, "TX_OBS").Length(1000).Nullable();
            Map(spa => spa.StatusAgendamento, "TX_STATUS");
        }
    }
}
