﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.AgendamentosPessoa;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Agendamentos
{
    public class PessoaAgendamentoMap : ClassMap<PessoaAgendamento>
    {
        public PessoaAgendamentoMap()
        {
            Table("PESSOA_AGENDAMENTO");

            Id(pa => pa.Codigo, "CD_PESSOA_AGENDAMENTO");

            Map(pf => pf.CPF, "TX_CPF").Length(11);

            References(pf => pf.Naturalidade, "CD_NATURAL").Nullable();

            Map(p => p.DataRegistro, "DT_REGISTRO").Not.Nullable();

            Map(pf => pf.Passaporte, "TX_PASSAPORTE");
            Map(p => p.Nome, "TX_NOME").Not.Nullable();
            Map(p => p.Email, "TX_EMAIL");
            Map(p => p.Apelido, "TX_APELIDO").Not.Nullable();
            Map(pf => pf.RG, "TX_RG").Length(50);
            Map(pf => pf.RGOrgaoEmissor, "TX_RG_ORGAO");
            Map(pf => pf.RGDataEmissao, "DT_RG_EMISSAO");

            References(pf => pf.RGOrgaoEmissorUF, "CD_RG_UF").Nullable();

            Map(pf => pf.DataNascimento, "DT_NASCIMENTO");
            Map(pf => pf.Nacionalidade, "TX_NACIONALIDADE").Length(100);
            Map(pf => pf.NomeMae, "TX_MAE").Length(150);
            Map(pf => pf.NomePai, "TX_PAI").Length(150);
            Map(pf => pf.TipoSanguineo, "TX_SANGE");
            Map(pf => pf.Sexo, "TX_SEXO").Nullable();
            Map(pf => pf.Foto, "ME_FOTO").CustomSqlType("VARBINARY(MAX)").Length(int.MaxValue).LazyLoad();
            Map(pf => pf.DataVisto, "DT_VISTO");
            Map(pf => pf.DataValidadeVisto, "DT_VALIDADE_VISTO");

            Map(pf => pf.RNE, "TX_RNE");
            Map(pf => pf.DataExpedicaoRNE, "DT_EXPEDICAO_RNE");
            Map(pf => pf.EstadoCivil, "TX_ESTADOCIVIL").Length(25);
            Map(pf => pf.Escolaridade, "TX_ESCOLARIDADE").Length(25);

            Map(pf => pf.TelefoneNumResidencial, "NU_TELEFONE_RESID").Length(20);
            Map(pf => pf.TelefoneNumCelular, "NU_TELEFONE_CELULAR").Length(20);
            Map(pf => pf.TelefoneNumEmergencial, "NU_TELEFONE_EMERG").Length(20);

            Map(b => b.CTPS, "NU_CT");
            Map(b => b.CTPSSerie, "TX_CT_SERIE");
            Map(b => b.CTPSData, "DT_CT_DT").Precision(4);

            References(b => b.CTPSEstado, "CD_CT_ID_UF");

            Map(b => b.CertificadoReservista, "NU_CR_NR");
            Map(b => b.CertificadoReservistaCat, "NU_CR_TIPO");
            Map(b => b.CNH, "TX_CNH");
            Map(b => b.CNHCategoria, "TX_CNH_TP");
            Map(b => b.CNHDataValidade, "DT_CNH_VALIDADE");
            Map(b => b.TituloEleitor, "NU_TE");
            Map(b => b.TituloEleitorZona, "NU_TE_ZONA");
            Map(b => b.TituloEleitorSecao, "NU_TE_SECAO");
            References(b => b.TituloEleitorCidade, "CD_MUNICIPIO");

            Map(b => b.EnderecoNumero, "NU_ENDERECO");
            Map(b => b.EnderecoComplemento, "NU_TX_COMPLEMENTO");
            References(b => b.Endereco, "CD_LOGRADOURO").Nullable();
            
        }
    }
}
