﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.AgendamentosPessoa;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Agendamentos
{
    public class PapelAgendamentoMap : ClassMap<PapelAgendamento>
    {
        public PapelAgendamentoMap()
        {
            Table("PAPEL_AGENDAMENTO");

            Id(pa => pa.Codigo, "CD_PAPEL_AGENDAMENTO");

            References(pa => pa.Cargo, "CD_CARGO");
            References(pa => pa.GestorPO, "CD_GESTOR");
            References(pa => pa.Solicitante, "CD_USUARIO");
            References(pa => pa.PessoaAg, "CD_PESSOA_AGENDAMENTO");
            References(pa => pa.Papel, "CD_PAPEL");
            References(pa => pa.EmpregadoSap, "CD_EMPREGADO_SAP");
            References(pa => pa.SetorCusto, "CD_SETOR");
            References(pa => pa.Area, "CD_AREA");

            Map(pa => pa.PessoaJuridica, "BL_JURIDICO").Not.Nullable();
            Map(pa => pa.DataRegistro, "DT_REGISTRO");

            HasMany(pa => pa.Documentos)
                .KeyColumn("CD_PAPEL_AGENDAMENTO")
                .Where("DT_DESATIVACAO IS NULL");

            HasManyToMany(pa => pa.Treinamentos)
                    .Table("TREINAMENTO_PAPEL_AGEND")
                    .ParentKeyColumn("CD_PAPEL_AGENDAMENTO")
                    .ChildKeyColumn("CD_TREINAMENTO");

            HasMany(pa => pa.Status)
                .KeyColumn("CD_PAPEL_AGENDAMENTO");
        }
    }
}
