﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Controlsys.Dominio.Enderecos;

namespace Controlsys.Persistencia.Mapeamento.Enderecos
{
    /// <summary>
    /// Representa um(a) LogradouroMap.
    /// </summary>
    public class LogradouroMap : ClassMap<Logradouro>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Enderecos.LogradouroMap.
        /// </summary>
        public LogradouroMap()
        {
            Table("LOGRADOURO");

            Id(l => l.Codigo, "CD_LOGRADOURO");

            //References(l => l.TipoLogradouro, "CD_TIPO_LOGRADOURO").Not.Nullable();
            References(l => l.Bairro, "CD_BAIRRO").Not.Nullable();

            Map(l => l.Nome, "TX_NOME").Not.Nullable().Length(300);
            Map(l => l.CEP, "NU_CEP").Not.Nullable();
            Map(l => l.Ativo, "BL_ATIVO").Not.Nullable();
            Map(l => l.DataRegistro, "DT_REGISTRO").Not.Nullable().Precision(4);
            Map(l => l.DataDesativacao, "DT_DESATIVACAO").Nullable().Precision(4);
        }
    }
}
