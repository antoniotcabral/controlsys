﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Enderecos;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Enderecos
{
    /// <summary>
    /// Representa um(a) TipoLogradouroMap.
    /// </summary>
    public class TipoLogradouroMap : ClassMap<TipoLogradouro>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Enderecos.TipoLogradouroMap.
        /// </summary>
        public TipoLogradouroMap()
        {
            Table("TIPO_LOGRADOURO");

            Id(t => t.Codigo, "CD_TIPO_LOGRADOURO");

            Map(t => t.Nome, "TX_TIPOLOGRADOURO").Not.Nullable().Length(40);
            Map(t => t.Ativo, "BL_ATIVO").Not.Nullable();
            Map(t => t.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(t => t.DataDesativacao, "DT_DESATIVACAO");
        }
    }
}
