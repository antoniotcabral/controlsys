﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Enderecos;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Enderecos
{
    /// <summary>
    /// Representa um(a) BairroMap.
    /// </summary>
    public class BairroMap : ClassMap<Bairro>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Enderecos.BairroMap.
        /// </summary>
        public BairroMap()
        {
            Table("BAIRRO");

            Id(b => b.Codigo, "CD_BAIRRO");

            References(b => b.Cidade, "CD_CIDADE").Nullable();

            Map(b => b.Nome, "TX_BAIRRO").Not.Nullable().Length(100);
            Map(b => b.DataRegistro, "DT_REGISTRO").Not.Nullable().Precision(4);
            Map(b => b.DataDesativacao, "DT_DESATIVACAO").Precision(4);
        }
    }
}
