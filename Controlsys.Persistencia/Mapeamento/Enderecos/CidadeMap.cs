﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Enderecos;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Enderecos
{
    /// <summary>
    /// Representa um(a) CidadeMap.
    /// </summary>
    public class CidadeMap : ClassMap<Cidade>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Enderecos.CidadeMap.
        /// </summary>
        public CidadeMap()
        {
            Table("CIDADE");

            Id(c => c.Codigo, "CD_CIDADE");

            References(e => e.Estado, "CD_ESTADO");


            Map(c => c.Nome, "TX_CIDADE").Not.Nullable();
            Map(c => c.Ativo, "BL_ATIVO").Not.Nullable();
            Map(c => c.DataRegistro, "DT_REGISTRO").Not.Nullable().Precision(4);
            Map(c => c.DataDesativacao, "DT_DESATIVACAO").Precision(4);


        }
    }
}
