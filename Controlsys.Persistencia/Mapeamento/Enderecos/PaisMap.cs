﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Enderecos;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Enderecos
{
    /// <summary>
    /// Representa um(a) PaisMap.
    /// </summary>
    public class PaisMap : ClassMap<Pais>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Enderecos.PaisMap.
        /// </summary>
        public PaisMap()
        {
            Table("PAIS");

            Id(p => p.Codigo, "CD_PAIS");

            Map(p => p.Nome, "TX_NOME").Not.Nullable().Length(255);
            Map(p => p.Sigla, "TX_PAIS").Not.Nullable().Length(5);
            Map(p => p.Ativo, "BL_ATIVO").Not.Nullable();
            Map(p => p.DataRegistro, "DT_REGISTRO").Precision(4).Not.Nullable();
            Map(p => p.DataDesativacao, "DT_DESATIVACAO").Precision(4);
        }
    }
}
