﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Enderecos;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Enderecos
{
    /// <summary>
    /// Representa um(a) EstadoMap.
    /// </summary>
    public class EstadoMap : ClassMap<Estado>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Enderecos.EstadoMap.
        /// </summary>
        public EstadoMap()
        {
            Table("ESTADO");

            Id(e => e.Codigo, "CD_ESTADO");

            References(l => l.Pais, "CD_PAIS").Not.Nullable();

            Map(e => e.Nome, "TX_ESTADO").Length(50).Not.Nullable();
            Map(e => e.Sigla, "TX_UF").Length(2).Not.Nullable();
            Map(e => e.Ativo, "BL_ATIVO").Not.Nullable();
            Map(e => e.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(e => e.DataDesativacao, "DT_DESATIVACAO").Nullable();
        }
    }
}
