﻿using Controlsys.Dominio.Empresas;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Enderecos
{
    /// <summary>
    /// Representa um(a) CNAEMap.
    /// </summary>
    public class CNAEMap : ClassMap<CNAE>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Enderecos.CNAEMap.
        /// </summary>
        public CNAEMap()
        {
            Table("CNAE");
            Id(b => b.Codigo, "TX_CNAE");

            Map(b => b.Nome, "TX_DESCRICAO").Length(500);

            Map(b => b.Ativo, "BL_ATIVO").Not.Nullable();
            Map(b => b.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(b => b.DataDesativacao, "DT_DESATIVACAO");
        }
    }
}
