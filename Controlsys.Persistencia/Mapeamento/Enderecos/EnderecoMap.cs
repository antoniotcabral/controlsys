﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Enderecos;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Enderecos
{
    /// <summary>
    /// Representa um(a) EnderecoMap.
    /// </summary>
    public class EnderecoMap : ClassMap<Endereco>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Enderecos.EnderecoMap.
        /// </summary>
        public EnderecoMap()
        {
            Table("ENDERECO");

            Id(e => e.Codigo, "CD_ENDERECO");

            References(e => e.Logradouro, "CD_LOGRADOURO");

            Map(e => e.CaixaPostal, "NU_POSTAL").Precision(10);
            Map(e => e.Numero, "NU_ENDERECO").Length(50);
            Map(e => e.Complemento, "TX_COMPLEMENTO").Length(1000);
            Map(e => e.Ativo, "BL_ATIVO").Not.Nullable();
            Map(e => e.DataRegistro, "DT_REGISTRO").Not.Nullable().Precision(4);
            Map(e => e.DataDesativacao, "DT_DESATIVACAO").Precision(4);
        }
    }
}
