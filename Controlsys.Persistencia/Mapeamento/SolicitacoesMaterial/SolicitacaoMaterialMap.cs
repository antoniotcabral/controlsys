﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.SolicitacoesMaterial;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.SolicitacoesMaterial
{
    public class SolicitacaoMaterialMap : ClassMap<SolicitacaoMaterial>
    {
        public SolicitacaoMaterialMap()
        {
            Table("SOLICITACAO_MATERIAL");

            Id(sm => sm.Codigo, "CD_SOLICITACAO_MATERIAL");

            References(sm => sm.Solicitante, "CD_USUARIO").Not.Nullable();
            References(sm => sm.Contratada, "CD_CONTRATADA").Not.Nullable();
            References(sm => sm.SubContratada, "CD_SUBCONTRATADA").Nullable();
            References(sm => sm.SetorCusto, "CD_SETOR_CUSTO").Not.Nullable();
            References(sm => sm.PedidoCompra, "CD_PEDIDO").Nullable();
            References(sm => sm.GestorResponsavel, "CD_GESTOR_RESPONSAVEL").Nullable();
            References(sm => sm.ColaboradorResponsavel, "CD_COLABORADOR_RESPONSAVEL").Nullable();
            References(sm => sm.RecebidoPor, "CD_RECEBIDO_POR").Nullable();

            Map(sm => sm.DataEntradaSaida, "DT_SOLICITACAO").Not.Nullable();
            Map(sm => sm.TipoSolicitacao, "TX_TIPO").Not.Nullable();
            Map(sm => sm.DataPrevisaoRetorno, "DT_PREViSAO_RETORNO").Nullable();
            Map(sm => sm.Modalidade, "TX_MODALIDADE").Nullable();
            Map(sm => sm.Motivo, "TX_MOTIVO").Nullable();
            Map(sm => sm.PlacaVeiculo, "TX_PLACA").Not.Nullable();
            Map(sm => sm.Ativo, "BL_ATIVO").Not.Nullable();
            Map(sm => sm.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(sm => sm.DataDesativacao, "DT_DESATIVACAO").Nullable();
            Map(sm => sm.Destino, "TX_DESTINO").Nullable();
            Map(sm => sm.Portador, "TX_PORTADOR").Nullable();


            HasMany(p => p.Midias).Table("SOCITACAO_MATERIAL_MIDIA").KeyColumn("CD_SOLICITACAO_MATERIAL")
               .Where("DT_DESATIVACAO IS NULL"); ;

            HasMany(sm => sm.Itens)
                .KeyColumn("CD_SOLICITACAO_MATERIAL");

            HasMany(sm => sm.Status)
                .KeyColumn("CD_SOLICITACAO_MATERIAL");
        }
    }
}
