﻿using Controlsys.Dominio.SolicitacoesMaterial;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.SolicitacoesMaterial
{
    public class SolicitacaoMaterialMidiaMap : ClassMap<SolicitacaoMaterialMidia>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Pessoas.SolicitacaoMaterialMidiaMap.
        /// </summary>
        public SolicitacaoMaterialMidiaMap()
        {
            Table("SOLICITACAO_MATERIAL_MIDIA");

            Id(b => b.Codigo, "CD_SOLICITACAO_MATERIAL_MIDIA").Not.Nullable();

            References(b => b.Midia, "CD_MIDIA").Not.Nullable();
            References(b => b.SolicitacaoMaterial, "CD_SOLICITACAO_MATERIAL").Not.Nullable();

            Map(b => b.Ativo, "BL_ATIVO").Not.Nullable();
            Map(b => b.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(b => b.DataDesativacao, "DT_DESATIVACAO");
        }
    }
}
