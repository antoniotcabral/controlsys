﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.SolicitacoesMaterial;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.SolicitacoesMaterial
{
    public class ItemSolicitacaoMaterialMap : ClassMap<ItemSolicitacaoMaterial>
    {
        public ItemSolicitacaoMaterialMap()
        {
            Table("ITEM_SOLICITACAO_MATERIAL");

            Id(ism => ism.Codigo, "CD_ITEM_SOLICITACAO_MATERIAL");

            References(ism => ism.SolicitacaoMaterial, "CD_SOLICITACAO_MATERIAL");

            Map(ism => ism.Descricao, "TX_DESCRICAO");
            Map(ism => ism.NotaFiscalEntrada, "TX_NOTA_FISCAL_ENTRADA");
            Map(ism => ism.NotaFiscalSaida, "TX_NOTA_FISCAL_SAIDA");
            Map(ism => ism.NotaServico, "TX_NOTA_SERVICO");
            Map(ism => ism.NumeroPatrimonio, "TX_NUMERO_PATRIMONIO");
            Map(ism => ism.Quantidade, "NU_QUANTIDADE");
            Map(ism => ism.AutorizacaoVigilante, "BL_AUTORIZACAO_VIGILANTE");
        }
    }
}

