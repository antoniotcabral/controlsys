﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.SolicitacoesMaterial;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.SolicitacoesMaterial
{
    public class StatusSolicitacaoMaterialMap : ClassMap<StatusSolicitacaoMaterial>
    {
        public StatusSolicitacaoMaterialMap()
        {
            Table("STATUS_SOLICITACAO_MATERIAL");

            Id(ssm => ssm.Codigo, "CD_STATUS_SOLICITACAO_MATERIAL");

            References(ssm => ssm.SolicitacaoMaterial, "CD_SOLICITACAO_MATERIAL").Not.Nullable();
            References(ssm => ssm.UsuarioStatus, "CD_USUARIO").Nullable();

            Map(ssm => ssm.Situacao, "TX_SITUACAO").Not.Nullable();
            Map(ssm => ssm.Observacoes, "TX_OBSERVACAO").Nullable();
            Map(ssm => ssm.DataRegistro, "DT_REGISTRO").Not.Nullable();
            //Map(ssm => ssm.UsuarioStatus, "CD_USUARIO").Nullable();
        }
    }
}
