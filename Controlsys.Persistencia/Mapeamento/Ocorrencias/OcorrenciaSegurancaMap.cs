﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Ocorrencias;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Ocorrencias
{
    public class OcorrenciaSegurancaMap : ClassMap<OcorrenciaSeguranca>
    {
        public OcorrenciaSegurancaMap()
        {
            Table("OCORRENCIA_SEGURANCA");

            Id(os => os.Codigo, "CD_OCORRENCIA_SEGURANCA");

            References(os => os.Local, "CD_LOCAL");
            References(os => os.Supervisor, "CD_USUARIO");
            References(os => os.TipoOcorrencia, "CD_TIPO_OCORRENCIA");

            Map(os => os.DataRegistro, "DT_REGISTRO");
            Map(os => os.DataDesativacao, "DT_DESATIVACAO");
            Map(os => os.Ativo, "BL_ATIVO");
            Map(os => os.DataOcorrencia, "DT_OCORRENCIA");
            Map(os => os.HoraOcorrencia, "HR_OCORRENCIA").CustomType("TimeAsTimeSpan");
            Map(os => os.Descricao, "TX_DESCRICAO");
            Map(os => os.AcoesRecomendadas, "TX_ACOES");
            Map(os => os.Procedimentos, "TX_PROCEDIMENTOS");
            Map(os => os.Efetiva, "BL_EFETIVA");
            Map(os => os.Vulnerabilidade, "BL_VULNERABILIDADE");
            Map(os => os.Tentativa, "BL_TENTATIVA");
            Map(os => os.Gravidade, "GRAVIDADE");

            HasMany(os => os.AgentesOcorrencia)
                .KeyColumn("CD_OCORRENCIA_SEGURANCA")
                .Where("DT_DESATIVACAO IS NULL");

            HasMany(os => os.Veiculos)
                .KeyColumn("CD_OCORRENCIA_SEGURANCA")
                .Where("DT_DESATIVACAO IS NULL");

            HasMany(os => os.Midias)
                .KeyColumn("CD_OCORRENCIA_SEGURANCA")
                .Where("DT_DESATIVACAO IS NULL");
        }
    }
}
