﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Ocorrencias;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Ocorrencias
{
    public class TreinamentoOcorrenciaTransitoMap : ClassMap<TreinamentoOcorrenciaTransito>
    {
        public TreinamentoOcorrenciaTransitoMap()
        {
            Table("TREINAMENTO_OCORRENCIA_TRANSITO");

            Id(tot => tot.Codigo, "CD_TREINAMENTO_OCORRENCIA_TRANSITO");

            References(tot => tot.OcorrenciaTransito, "CD_OCORRENCIA_TRANSITO");

            Map(tot => tot.CompareceuNotificacao, "BL_COMPARECEU_NOTIFICACAO");
            Map(tot => tot.DataAgendamento, "DT_AGENDAMENTO").Nullable();
            Map(tot => tot.CompareceuTreinamento, "BL_COMPARECEU_TREINAMENTO").Nullable();
            Map(tot => tot.Apto, "BL_APTO").Nullable();
            Map(tot => tot.Ativo, "BL_ATIVO");
            Map(tot => tot.DataRegistro, "DT_REGISTRO");
            Map(tot => tot.DataDesativacao, "DT_DESATIVACAO").Nullable();
        }
    }
}
