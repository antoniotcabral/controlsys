﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentNHibernate.Mapping;
using Controlsys.Dominio.Ocorrencias;

namespace Controlsys.Persistencia.Mapeamento.Ocorrencias
{
    public class OcorrenciaSegurancaMidiaMap : ClassMap<OcorrenciaSegurancaMidia>
    {
        public OcorrenciaSegurancaMidiaMap()
        {
            Table("OCORRENCIA_SEG_MIDIA");

            Id(a => a.Codigo, "CD_OCORRENCIA_SEG_MIDIA");

            Map(a => a.Ativo, "BL_ATIVO").Not.Nullable();
            Map(a => a.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(a => a.DataDesativacao, "DT_DESATIVACAO").Nullable();

            References(a => a.Ocorrencia, "CD_OCORRENCIA_SEGURANCA").Not.Nullable();
            References(a => a.Midia, "CD_MIDIA").Not.Nullable();
        }
    }
}
