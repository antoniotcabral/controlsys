﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Ocorrencias;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Ocorrencias
{
    public class VeiculoOcorrenciaSegurancaMap : ClassMap<OcorrenciaSegurancaVeiculo>
    {
        public VeiculoOcorrenciaSegurancaMap()
        {
            Table("OCORRENCIA_SEG_VEICULO");

            Id(vos => vos.Codigo, "CD_OCORRENCIA_SEG_VEICULO");

            References(vos => vos.OcorrenciaSeg, "CD_OCORRENCIA_SEGURANCA");
            References(vos => vos.Veiculo, "CD_VEICULO");

            Map(vos => vos.Ativo, "BL_ATIVO");
            Map(vos => vos.DataRegistro, "DT_REGISTRO");
            Map(vos => vos.DataDesativacao, "DT_DESATIVACAO");
        }
    }
}
