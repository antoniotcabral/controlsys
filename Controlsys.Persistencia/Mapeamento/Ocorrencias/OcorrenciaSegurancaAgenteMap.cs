﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Ocorrencias;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Ocorrencias
{
    public class OcorrenciaSegurancaAgenteMap : ClassMap<OcorrenciaSegurancaAgente>
    {
        public OcorrenciaSegurancaAgenteMap()
        {
            Table("OCORRENCIA_SEG_AGENTE");

            Id(a => a.Codigo, "CD_OCORRENCIA_SEG_AGENTE");

            Map(a => a.Cracha, "TX_CRACHA").Nullable();
            Map(a => a.TipoAgente, "TX_TIPO_AGENTE").Not.Nullable();
            Map(a => a.Ativo, "BL_ATIVO").Not.Nullable();
            Map(a => a.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(a => a.DataDesativacao, "DT_DESATIVACAO").Nullable();

            References(a => a.Ocorrencia, "CD_OCORRENCIA_SEGURANCA").Not.Nullable();
            References(a => a.PessoaFisica, "CD_PESSOA_FISICA").Not.Nullable();
        }
    }
}
