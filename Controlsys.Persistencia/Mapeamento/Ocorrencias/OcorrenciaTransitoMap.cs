﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Ocorrencias;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Ocorrencias
{
    public class OcorrenciaTransitoMap : ClassMap<OcorrenciaTransito>
    {
        public OcorrenciaTransitoMap()
        {
            Table("OCORRENCIA_TRANSITO");

            Id(ot => ot.Codigo, "CD_OCORRENCIA_TRANSITO");

            References(ot => ot.Papel, "CD_PAPEL").Not.LazyLoad().Nullable();
            References(ot => ot.PessoaFisica, "CD_PESSOAFISICA").Not.LazyLoad().Not.Nullable();
            References(ot => ot.Local, "CD_LOCAL").Not.Nullable();
            References(ot => ot.Infracao, "CD_INFRACAO").Not.Nullable();
            References(ot => ot.Penalidade, "CD_PENALIDADE").Not.Nullable();

            Map(ot => ot.NumeroCracha, "TX_NUMERO_CRACHA").Nullable();
            Map(ot => ot.DataOcorrencia, "DT_OCORRENCIA");
            Map(ot => ot.Anulada, "BL_ANULADA");
            Map(ot => ot.DataRegistro, "DT_REGISTRO");
            Map(ot => ot.DataDesativacao, "DT_DESATIVACAO");
            Map(ot => ot.Ativo, "BL_ATIVO");

            HasMany(ot => ot.Treinamentos)
                .KeyColumn("CD_OCORRENCIA_TRANSITO")
                .Where("DT_DESATIVACAO IS NULL").Not.LazyLoad();
        }
    }
}
