﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Acesso;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Acesso
{
    public class AcessoRestauranteTipoAlimentacaoMap : ClassMap<AcessoRestauranteTipoAlimentacao>
    {
        public AcessoRestauranteTipoAlimentacaoMap()
        {
            Table("ACESSOREST_RESTTIPOALIMENT");

            Id(a => a.Codigo, "CD_ACESSOREST_RESTTIPOALIMENT");

            Map(a => a.Ativo, "BL_ATIVO");
            Map(a => a.DataRegistro, "DT_REGISTRO");
            Map(a => a.DataDesativacao, "DT_DESATIVACAO");

            References(a => a.TipoAlimentacao, "CD_RESTAURANTE_TIPOALIMENTACAO");
            References(a => a.AcessoRestaurante, "CD_ACESSO_RESTAURANTE");
        }
    }
}
