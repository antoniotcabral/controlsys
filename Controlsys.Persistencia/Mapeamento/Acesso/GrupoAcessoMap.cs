﻿using Controlsys.Dominio.Acesso;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Acesso
{
    /// <summary>
    /// Representa um(a) GrupoAcessoMap.
    /// </summary>
    public class GrupoAcessoMap : ClassMap<GrupoAcesso>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Acesso.GrupoAcessoMap.
        /// </summary>
        public GrupoAcessoMap()
        {
            Table("GRUPO_ACESSO");

            Id(ga => ga.Codigo, "CD_GRUPO_ACESSO");

            Map(ga => ga.Nome, "TX_NOME").Not.Nullable();
            Map(ga => ga.Ativo, "BL_ATIVO").Not.Nullable();
            Map(ga => ga.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(ga => ga.DataDesativacao, "DT_DESATIVACAO");

            HasMany(g => g.Pessoas).KeyColumn("CD_GRUPO_ACESSO").Where("DT_DESATIVACAO IS NULL");
            HasMany(g => g.GruposLeitora).KeyColumn("CD_GRUPO_ACESSO").Where("DT_DESATIVACAO IS NULL");
        }
    }
}
