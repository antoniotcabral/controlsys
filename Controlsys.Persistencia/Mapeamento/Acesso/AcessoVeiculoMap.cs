﻿using Controlsys.Dominio.Acesso;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Acesso
{
    class AcessoVeiculoMap : ClassMap<AcessoVeiculo>
    {
        public AcessoVeiculoMap()
        {
            Table("ACESSO_VEICULO");

            Id(l => l.Codigo, "CD_ACESSO_VEICULO");

            References(l => l.Cracha, "CD_CRACHA");

            References(l => l.Papel, "CD_PAPEL").LazyLoad(Laziness.False).Not.Nullable();

            References(l => l.AcessoPapel, "CD_ACESSO");
            References(l => l.VeiculoCredencialVeiculo, "CD_VEICULO_CREDENCIAL_VEICULO");

            References(l => l.Controladora, "CD_CONTROLADORA");

            HasMany(l => l.CamerasImagens)
             .Table("CAMERA_IMAGEM")
              .KeyColumn("CD_CAMERA_IMAGEM").Cascade.All();
              //.LazyLoad(Laziness.NoProxy);

            References(l => l.Leitora, "CD_LEITORA");

            Map(l => l.DataRegistro, "DT_DATA_REGISTRO");

            Map(l => l.Observacoes, "TX_OBSERVACOES");

            Map(l => l.StatusAutorizacao, "BL_AUTORIZACAO");

            Map(l => l.StatusAcesso, "TX_STATUS");

        }
    }
}
