﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Acesso;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Acesso
{
  
        /// <summary>
        /// Representa um(a) ControladoraMap.
        /// </summary>
        public class CameraImagemMap : ClassMap<CameraImagem>
        {
            /// <summary>
            /// Construtor para Controlsys.Persistencia.Mapeamento.Acesso.ControladoraMap.
            /// </summary>
            public CameraImagemMap()
            {
                Table("CAMERA_IMAGEM");

                Id(c => c.Codigo, "CD_CAMERA_IMAGEM");
                References(c => c.AcessoVeiculo, "CD_ACESSO_VEICULO");
                References(c => c.Camera, "CD_CAMERA");
                Map(c => c.CaminhoImagem, "TX_CAMINHO_IMAGEM");
                Map(c => c.PlacaVeiculo, "TX_PLACA");
                Map(c => c.PrecisaoImage, "TX_PRECISAO");
             
            }
        }
    }


