﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Acesso;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Acesso
{
    /// <summary>
    /// Representa um(a) ControladoraMap.
    /// </summary>
    public class ControladoraMap : ClassMap<Controladora>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Acesso.ControladoraMap.
        /// </summary>
        public ControladoraMap()
        {
            Table("CONTROLADORA");

            Id(c => c.Codigo, "CD_CONTROLADORA");

            Map(c => c.Modelo, "TX_MODELO");
            Map(c => c.IP, "TX_IP");
            Map(c => c.TipoControladora, "NU_TIPO_CNTRL").CustomType("int");
            Map(c => c.Ativo, "BL_ATIVO");
            Map(c => c.Online, "BL_ONLINE");
            Map(c => c.DataRegistro, "DT_REGISTRO");
            Map(c => c.DataDesativacao, "DT_DESATIVACAO");
            Map(c => c.HoraSincronizacao, "HR_SINCRONIZACAO").CustomType("TimeAsTimeSpan");
            Map(c => c.Tensao, "DE_TENSAO");
            Map(c => c.Temperatura, "NU_TEMP");
            Map(c => c.Corrente, "DE_CORRENTE");
            Map(c => c.QtdMaxListaOrdenada, "NU_QTDMAXORD");
            Map(c => c.QtdListaOrdenada, "NU_QTDORD");
            Map(c => c.QtdMaxListaDesordenada, "NU_QTDMAXDESORD");
            Map(c => c.QtdListaDesordenada, "NU_QTDDESORD");
            //Map(c => c.Interditado, "BL_INTERDITADO");
            //Map(c => c.DataLiberacaoInterdicao, "DT_INTERDLIBER");

            Map(c => c.NumeroSerial, "NU_SERIAL");
            Map(c => c.VersaoFirmware, "TX_VSFIRMWARE");
            Map(c => c.DataUltimaAlteracaoOnOff, "DT_ULT_ATUALIZACAO");

            Map(c => c.Status, "TX_STATUS").CustomSqlType("VARCHAR(50)");
            Map(c => c.DataStatus, "DT_STATUS");
            Map(c => c.ValidaOCR, "BL_ValidaOCR");

            HasMany(c => c.Logs).KeyColumn("CD_CONTROLADORA");

            HasMany(c => c.Leitoras).KeyColumn("CD_CONTROLADORA").Where("DT_DESATIVACAO IS NULL");
        }
    }
}
