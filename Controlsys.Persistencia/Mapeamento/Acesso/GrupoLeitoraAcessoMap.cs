﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Acesso;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Acesso
{
    /// <summary>
    /// Representa um(a) GrupoLeitoraAcessoMap.
    /// </summary>
    public class GrupoLeitoraAcessoMap : ClassMap<GrupoLeitoraAcesso>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Acesso.GrupoLeitoraAcessoMap.
        /// </summary>
        public GrupoLeitoraAcessoMap()
        {
            Table("GRUPO_LEITORA_ACESSO");

            Id(gle => gle.Codigo, "CD_GRUPO_LEITORA_ACESSO");

            References(gle => gle.GrupoLeitora, "CD_GRUPO_LEITORA").Not.Nullable();
            References(gle => gle.GrupoAcesso, "CD_GRUPO_ACESSO").Not.Nullable();

            Map(gle => gle.Ativo, "BL_ATIVO").Not.Nullable();
            Map(gle => gle.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(gle => gle.DataDesativacao, "DT_DESATIVACAO");
        }
    }
}
