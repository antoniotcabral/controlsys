﻿using Controlsys.Dominio.Acesso;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Acesso
{
    /// <summary>
    /// Representa um(a) LeitoraMap.
    /// </summary>
    public class LeitoraMap : ClassMap<Leitora>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Acesso.LeitoraMap.
        /// </summary>
        public LeitoraMap()
        {
            Table("LEITORA");

            Id(l => l.Codigo, "CD_LEITORA");
            
            Map(l => l.IDLeitora, "TX_SERIE").Length(50).Not.Nullable();
            Map(l => l.Ordem, "NU_ORDEM").Not.Nullable();
            Map(l => l.Senha, "BL_SENHA").Not.Nullable();
            References(l => l.Controladora, "CD_CONTROLADORA").Not.Nullable();
            //Map(l => l.TipoLeitora, "TX_TIPO_LEITORA").Length(25).Not.Nullable();
            Map(l => l.Fabricante, "NU_FABRICANTE").CustomType("int").Not.Nullable();
            Map(l => l.Descricao, "TX_DESCRICAO").Length(500);
            Map(l => l.Direcao, "TX_DIRECAO").Length(20).Not.Nullable();
            Map(l => l.Local, "TX_LOCAL").Length(50);
            Map(l => l.Ativo, "BL_ATIVO").Not.Nullable();
            Map(l => l.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(l => l.DataDesativacao, "DT_DESATIVACAO");
            Map(l => l.EmiteBaixaCracha, "BL_BAIXA_CRACHA");
        }
    }
}
