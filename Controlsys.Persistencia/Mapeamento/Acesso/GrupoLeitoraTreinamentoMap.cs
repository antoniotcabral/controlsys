﻿using Controlsys.Dominio.Acesso;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Acesso
{
    /// <summary>
    /// Representa um(a) GrupoLeitoraTreinamentoMap.
    /// </summary>
    public class GrupoLeitoraTreinamentoMap : ClassMap<GrupoLeitoraTreinamento>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Acesso.GrupoLeitoraTreinamentoMap.
        /// </summary>
        public GrupoLeitoraTreinamentoMap()
        {
            Table("GRUPO_LEITORA_TREIN");

            Id(glt => glt.Codigo, "CD_GRUPO_LEITORA_TREIN");

            References(glt => glt.Treinamento, "CD_TREINAMENTO");
            References(glt => glt.GrupoLeitora, "CD_GRUPO_LEITORA");

            Map(glt => glt.Ativo, "BL_ATIVO").Not.Nullable();
            Map(glt => glt.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(glt => glt.DataDesativacao, "DT_DESATIVACAO");
        }
    }
}
