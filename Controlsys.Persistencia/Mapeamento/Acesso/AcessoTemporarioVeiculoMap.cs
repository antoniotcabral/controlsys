﻿using Controlsys.Dominio.Acesso;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Acesso
{
    public class AcessoTemporarioVeiculoMap : ClassMap<AcessoTemporarioVeiculo>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Acesso.AcessoTemporarioMap.
        /// </summary>
        public AcessoTemporarioVeiculoMap()
        {
            Table("ACESSO_TEMPORARIO_VEICULO");

            Id(atv => atv.Codigo, "CD_ACESSO_TEMPORARIO");

            References(atv => atv.VeiculoCredencialVeiculo, "CD_VEICULO_CREDENCIAL_VEICULO").Not.Nullable();
            References(atv => atv.GrupoLeitora, "CD_GRUPO_LEITORA").Not.Nullable();

            Map(atv => atv.DataHoraValidadeFim, "DT_HR_VALIDADE_FIM").Not.Nullable();
            Map(atv => atv.DataHoraValidadeInicio, "DT_HR_VALIDADE_INICIO").Not.Nullable();
            Map(atv => atv.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(atv => atv.DataDesativacao, "DT_DESATIVACAO").Nullable();
            Map(atv => atv.Ativo, "BL_ATIVO").Not.Nullable();  
        }
    }
}
