﻿using Controlsys.Dominio.Acesso;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Acesso
{
    /// <summary>
    /// Representa um(a) LeitoraGrupoLeitoraMap.
    /// </summary>
    public class LeitoraGrupoLeitoraMap : ClassMap<LeitoraGrupoLeitora>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Acesso.LeitoraGrupoLeitoraMap.
        /// </summary>
        public LeitoraGrupoLeitoraMap()
        {
            Table("LEITORA_GRUPO_LEITOR");

            Id(lgl => lgl.Codigo, "CD_LEITORA_GRUPO_LEITORA");

            References(lgl => lgl.Leitora, "CD_LEITORA");
            References(lgl => lgl.GrupoLeitora, "CD_GRUPO");

            Map(lgl => lgl.Ativo, "BL_ATIVO").Not.Nullable();
            Map(lgl => lgl.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(lgl => lgl.DataDesativacao, "DT_DESATIVACAO");
        }
    }
}
