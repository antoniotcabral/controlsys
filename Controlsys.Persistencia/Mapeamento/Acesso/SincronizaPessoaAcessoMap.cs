﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Acesso;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Acesso
{
    /// <summary>
    /// Representa um(a) SincronizaPessoaAcessoMap.
    /// </summary>
    public class SincronizaPessoaAcessoMap : ClassMap<SincronizaPessoaAcesso>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Acesso.SincronizaPessoaAcessoMap.
        /// </summary>
        public SincronizaPessoaAcessoMap()
        {
            Table("SINCRONIZA_ACESSO");

            Id(s => s.Codigo, "CD_SINCRONIZA_ACESSO");

            References(s => s.Cracha, "CD_CRACHA");
            References(s => s.Controladora, "CD_CONTROLADORA");

            Map(s => s.DataRegistro, "DT_REGISTRO");
            Map(s => s.DataInicio, "DT_INICIO");
            Map(s => s.DataSincronizacao, "DT_SINCRONIZA").Nullable();
            Map(s => s.TipoSincronizacao, "TX_TIPO").CustomSqlType("VARCHAR(50)");
            Map(s => s.Status, "NU_STATUS").CustomType<int>().Nullable();
            Map(s => s.Porcentagem, "NU_PORCENTAGEM").Nullable();
            Map(s => s.Tarefa, "NU_TAREFA").Nullable();
            Map(s => s.Observacoes, "TX_OBSERVACOES").Nullable();
        }
    }
}
