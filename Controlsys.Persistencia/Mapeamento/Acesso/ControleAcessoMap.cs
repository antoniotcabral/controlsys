﻿using Controlsys.Dominio.Acesso;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Acesso
{
    /// <summary>
    /// Representa um(a) ControleAcessoMap.
    /// </summary>
    public class ControleAcessoMap : ClassMap<ControleAcesso>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Acesso.ControleAcessoMap.
        /// </summary>
        public ControleAcessoMap()
        {
            Table("ACESSO");

            Id(a => a.Codigo, "CD_ACESSO");

            References(a => a.Papel, "CD_PAPEL").LazyLoad(Laziness.False).Nullable();
            References(a => a.Leitora, "CD_LEITORA").Not.Nullable();
            References(a => a.Cracha, "CD_CRACHA").Nullable();
            
            Map(a => a.NumeroSerie, "TX_SERIE").Not.Nullable().Length(50);
            Map(a => a.DataRegistro, "DT_REGISTRO").Not.Nullable().Precision(4);
            Map(a => a.DataAcesso, "DT_LEITORA").Not.Nullable().Precision(4);
            Map(a => a.StatusAcesso, "TX_STATUS").CustomType<int>().Not.Nullable().Length(20);
            Map(a => a.Direcao, "TX_DIRECAO").Not.Nullable().Length(50);            
            Map(a => a.Local, "TX_LOCAL").Not.Nullable().Length(50);
            Map(a => a.DataIntegracao, "DT_INTEGRACAO");
        }
    }
}
