﻿using Controlsys.Dominio.Acesso;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Acesso
{
    public class LogAcessoRestauranteMap : ClassMap<LogAcessoRestaurante>
    {

        public LogAcessoRestauranteMap()
        {
            Table("LOG_ACESSO_RESTAURANTE");

            Id(ac => ac.Codigo, "CD_LOG_ACESSO_RESTAURANTE");

            References(ac => ac.AcessoRestaurante, "CD_ACESSO_RESTAURANTE");

            Map(ac => ac.NaoValidaAcessoHierarquico, "BL_NAO_VALIDA_ACESSO_HIERARQUICO");
            Map(ac => ac.CrachaMestre, "BL_CRACHA_MESTRE");
            Map(ac => ac.DataRegistro, "DT_REGISTRO");
        }
    }
}
