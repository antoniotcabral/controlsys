﻿using Controlsys.Dominio.Acesso;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Acesso
{
    /// <summary>
    /// Representa um(a) GrupoAcessoPapelMap.
    /// </summary>
    public class GrupoAcessoPapelMap : ClassMap<GrupoAcessoPapel>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Acesso.GrupoAcessoPapelMap.
        /// </summary>
        public GrupoAcessoPapelMap()
        {
            Table("GRUPO_ACESSO_PAPEL");

            Id(gap => gap.Codigo, "CD_GRUPO_ACESSO_PAPEL");

            References(gap => gap.Papel, "CD_PAPEL").Not.Nullable();
            References(gap => gap.GrupoAcesso, "CD_GRUPO_ACESSO").Not.Nullable();

            Map(gap => gap.Ativo, "BL_ATIVO").Not.Nullable();
            Map(gap => gap.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(gap => gap.DataDesativacao, "DT_DESATIVACAO");
        }
    }
}
