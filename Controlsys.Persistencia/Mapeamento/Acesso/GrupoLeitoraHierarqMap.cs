﻿using Controlsys.Dominio.Acesso;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Acesso
{
    /// <summary>
    /// Representa um(a) GrupoLeitoraHierarqMap.
    /// </summary>
    public class GrupoLeitoraHierarqMap : ClassMap<GrupoLeitoraHierarq>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Acesso.GrupoLeitoraHierarqMap.
        /// </summary>
        public GrupoLeitoraHierarqMap()
        {
            Table("GRUPO_LEITORA_HIERARQ");

            Id(glh => glh.Codigo, "CD_GRUPO_LEITORA_HIERARQ");

            References(glh => glh.GrupoLeitoraPai, "CD_GRUPO_LEITORA_PAI");
            References(glh => glh.GrupoLeitoraFilho, "CD_GRUPO_LEITORA_FILHO");

            Map(glh => glh.Ativo, "BL_ATIVO").Not.Nullable();
            Map(glh => glh.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(glh => glh.DataDesativacao, "DT_DESATIVACAO");
        }
    }
}
