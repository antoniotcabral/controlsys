﻿using Controlsys.Dominio.Acesso;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Acesso
{
    public class CameraMap : ClassMap<Camera>
    {
        public CameraMap()
        {
            Table("CAMERA");

            Id(l => l.Codigo, "CD_CAMERA");

            References(l => l.Controladora, "CD_CONTROLADORA");

            Map(l => l.Nome, "TX_NOME");
            Map(l => l.Modelo, "TX_MODELO");
            Map(l => l.Descricao, "TX_DESCRICAO");
            Map(l => l.DataRegistro, "DT_REGISTRO");
            Map(l => l.Ativo, "BL_ATIVO");            
            Map(l => l.DataStatus, "DT_DATASTATUS");
            Map(l => l.Precisao, "NU_PRECISAO");
        }
    }
}
