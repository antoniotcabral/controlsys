﻿using Controlsys.Dominio.Acesso;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Acesso
{
    public class AcessoTemporarioMap : ClassMap<AcessoTemporario>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Acesso.AcessoTemporarioMap.
        /// </summary>
        public AcessoTemporarioMap()
        {
            Table("ACESSO_TEMPORARIO");

            Id(gat => gat.Codigo, "CD_ACESSO_TEMPORARIO");

            References(gap => gap.Papel, "CD_PAPEL").Not.Nullable();
            References(gap => gap.GrupoLeitora, "CD_GRUPO_LEITORA").Not.Nullable();

            Map(gap => gap.DataHoraValidadeFim, "DT_HR_VALIDADE_FIM").Not.Nullable();
            Map(gap => gap.DataHoraValidadeInicio, "DT_HR_VALIDADE_INICIO").Not.Nullable();
            Map(gap => gap.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(gap => gap.DataDesativacao, "DT_DESATIVACAO").Nullable();
            Map(gap => gap.Ativo, "BL_ATIVO").Not.Nullable();  
        }

    }
}
