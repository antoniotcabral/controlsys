﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Acesso;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Acesso
{
    public class LogControladoraMap : ClassMap<LogControladora>
    {
        public LogControladoraMap()
        {
            Table("LOG_CONTROLADORA");

            Id(lc => lc.Codigo, "CD_LOG_CONTROLADORA");

            Map(lc => lc.DataHora, "DT_REGISTRO");
            Map(lc => lc.Observacoes, "TX_OBS");
            Map(lc => lc.Status, "TX_STATUS");

            References(lc => lc.Controladora, "CD_CONTROLADORA");
            References(lc => lc.Usuario, "CD_USUARIO");
        }
    }
}
