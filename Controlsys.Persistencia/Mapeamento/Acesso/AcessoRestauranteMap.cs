﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Acesso;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Acesso
{
    public class AcessoRestauranteMap : ClassMap<AcessoRestaurante>
    {
        public AcessoRestauranteMap()
        {
            Table("ACESSO_RESTAURANTE");

            Id(ac => ac.Codigo, "CD_ACESSO_RESTAURANTE");

            References(ac => ac.Papel, "CD_PAPEL").Not.LazyLoad();

            Map(ac => ac.Ativo, "BL_ATIVO");
            Map(ac => ac.DataRegistro, "DT_REGISTRO");
            Map(ac => ac.DataDesativacao, "DT_DESATIVACAO");

            HasMany(r => r.TiposAlimentacao)
                .Table("ACESSOREST_RESTTIPOALIMENT")
                .KeyColumn("CD_ACESSO_RESTAURANTE");
               //.Where("DT_DESATIVACAO IS NULL");

            HasMany(r => r.LogAcessoRestaurante)
                .Table("LOG_ACESSO_RESTAURANTE")
               .KeyColumn("CD_ACESSO_RESTAURANTE");

        }
    }
}
