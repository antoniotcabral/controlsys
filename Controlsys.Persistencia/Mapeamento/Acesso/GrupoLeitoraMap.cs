﻿using Controlsys.Dominio.Acesso;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Acesso
{
    /// <summary>
    /// Representa um(a) GrupoLeitoraMap.
    /// </summary>
    public class GrupoLeitoraMap : ClassMap<GrupoLeitora>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Acesso.GrupoLeitoraMap.
        /// </summary>
        public GrupoLeitoraMap()
        {
            Table("GRUPO_LEITORA");

            Id(gl => gl.Codigo, "CD_GRUPO_LEITORA");

            Map(gl => gl.Nome, "TX_NOME");
            Map(gl => gl.HoraInicio, "HR_INICIO").Precision(7).CustomType("TimeAsTimeSpan");
            Map(gl => gl.HoraFim, "HR_FIM").Precision(7).CustomType("TimeAsTimeSpan");
            Map(gl => gl.AcessoMonitorado, "BL_MONITORADO").Not.Nullable();
            Map(gl => gl.ValidaCNH, "BL_CNH").Not.Nullable();
            Map(gl => gl.Externa, "BL_EXTERNA").Not.Nullable();
            Map(gl => gl.Supervisao, "BL_SUPERVISAO").Not.Nullable();
            Map(gl => gl.Ativo, "BL_ATIVO").Not.Nullable();
            Map(gl => gl.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(gl => gl.DataDesativacao, "DT_DESATIVACAO");
            Map(gl => gl.AntiPassBack, "BL_ANTIPASSBACK").Nullable();
            Map(gl => gl.AreaAlfandegada, "BL_AREA_ALFANDEGADA");//.Nullable();

            HasMany(p => p.Leitoras)
               .Table("LEITORA_GRUPO_LEITOR")
               .KeyColumn("CD_GRUPO")
               .Where("DT_DESATIVACAO IS NULL");

            HasMany(p => p.Treinamentos)
              .Table("GRUPO_LEITORA_TREIN")
              .KeyColumn("CD_GRUPO_LEITORA")
              .Where("DT_DESATIVACAO IS NULL");

            HasMany(p => p.GrupoLeitoraPai)
             .Table("GRUPO_LEITORA_HIERARQ")
             .KeyColumn("CD_GRUPO_LEITORA_FILHO")
             .Where("DT_DESATIVACAO IS NULL");

            HasMany(p => p.GrupoLeitoraFilho)
             .Table("GRUPO_LEITORA_HIERARQ")
             .KeyColumn("CD_GRUPO_LEITORA_PAI")
             .Where("DT_DESATIVACAO IS NULL");
            
        }

    }
}
