﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Seguranca;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Seguranca
{
    /// <summary>
    /// Representa um(a) GrupoLeitoraGrupoMap.
    /// </summary>
    public class GrupoLeitoraGrupoMap : ClassMap<GrupoLeitoraGrupo>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Seguranca.GrupoLeitoraGrupoMap.
        /// </summary>
        public GrupoLeitoraGrupoMap()
        {
            Table("LEITORA_ADMIN");            

            Id(g => g.Codigo, "CD_LEITORA_ADMIN");

            References(g => g.Grupo, "CD_GRUPO").Not.Nullable();
            References(gl => gl.GrupoLeitora, "CD_GRUPO_LEITORA").Not.Nullable();

            Map(g => g.DataRegistro, "DT_REGISTRO").Not.Nullable().Precision(4);
            Map(g => g.DataDesativacao, "DT_DESATIVACAO").Precision(4);
            Map(g => g.Ativo, "BL_ATIVO").Not.Nullable();
        }
    }
}
