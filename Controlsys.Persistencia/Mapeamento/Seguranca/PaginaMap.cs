﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Controlsys.Dominio.Seguranca;

namespace Controlsys.Persistencia.Mapeamento.Seguranca
{
    /// <summary>
    /// Representa um(a) PaginaMap.
    /// </summary>
    public class PaginaMap : ClassMap<Pagina>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Seguranca.PaginaMap.
        /// </summary>
        public PaginaMap()
        {
            Table("SISTEMA_PAGINA");

            Id(p => p.Codigo, "CD_PAGINA");

            Map(p => p.Nome, "TX_NOME").Not.Nullable().Length(50);
            Map(p => p.Icone, "TX_ICONE").Length(50);
            Map(p => p.Ordem, "NU_SEQUENCIA").Precision(3);
            Map(p => p.Url, "TX_URL").Length(350);
            Map(p => p.TipoPagina, "TX_TP_PAGINA").Not.Nullable();            
            Map(p => p.Ativo, "BL_ATIVO").Not.Nullable();
            Map(p => p.DataRegistro, "DT_REGISTRO").Not.Nullable().Precision(4);
            Map(p => p.DataDesativacao, "DT_DESATIVACAO").Precision(4);

            References(p => p.PaginaPai, "CD_PAI");

            HasMany(p => p.PaginasFilhas).KeyColumn("CD_PAI");
        }
    }
}
