﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Seguranca;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Seguranca
{
    /// <summary>
    /// Representa um(a) GrupoUsuarioMap.
    /// </summary>
    public class GrupoUsuarioMap : ClassMap<GrupoUsuario>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Seguranca.GrupoUsuarioMap.
        /// </summary>
        public GrupoUsuarioMap()
        {
            Table("GRUPO_USUARIO");

            Id(gu => gu.Codigo, "CD_GRUPO_USUARIO");

            Map(gu => gu.DataRegistro, "DT_REGISTRO").Not.Nullable().Precision(4);
            Map(gu => gu.DataDesativacao, "DT_DESATIVACAO").Nullable().Precision(4);
            Map(gu => gu.Ativo, "BL_ATIVO").Not.Nullable();

            References(gu => gu.Grupo, "CD_GRUPO").Not.Nullable();
            References(gu => gu.Usuario, "CD_USUARIO").Not.Nullable();
        }
    }
}
