﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Seguranca;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Seguranca
{
    /// <summary>
    /// Representa um(a) UsuarioMap.
    /// </summary>
    public class UsuarioMap : ClassMap<Usuario>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Seguranca.UsuarioMap.
        /// </summary>
        public UsuarioMap()
        {
            Table("USUARIO");

            Id(u => u.Codigo, "CD_USUARIO").GeneratedBy.Assigned();

            References(u => u.PessoaFisica, "CD_PESSOA_FISICA").Not.Nullable();

            HasMany(u => u.GruposUsuarios).KeyColumn("CD_USUARIO").Where("DT_DESATIVACAO IS NULL");

            Map(u => u.Login, "TX_LOGIN").Not.Nullable().Length(25);
            Map(u => u.Senha, "TX_SENHA");
            Map(u => u.DataLogin, "DT_LOGIN").Precision(4);
            Map(u => u.DataLogoff, "DT_LOGOFF").Precision(4);
            Map(u => u.Logado, "BL_LOGADO").Not.Nullable();
            Map(u => u.AD, "BL_AD").Not.Nullable();
            Map(u => u.Ativo, "BL_ATIVO").Not.Nullable();
            Map(u => u.DataRegistro, "DT_REGISTRO").Not.Nullable().Precision(4);
            Map(u => u.DataDesativacao, "DT_DESATIVACAO").Precision(4);
        }
    }
}
