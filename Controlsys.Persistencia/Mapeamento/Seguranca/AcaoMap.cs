﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Seguranca;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Seguranca
{
    /// <summary>
    /// Representa um(a) AcaoMap.
    /// </summary>
    public class AcaoMap : ClassMap<Acao>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Seguranca.AcaoMap.
        /// </summary>
        public AcaoMap()
        {
            Table("SISTEMA_ACAO");

            Id(a => a.Codigo, "CD_ACAO");

            References(a => a.Pagina, "CD_PAGINA").Not.Nullable();

            Map(a => a.Nome, "TX_ACAO").Not.Nullable().Length(50);
            Map(a => a.Url, "TX_URL").Not.Nullable();
            Map(a => a.Ativo, "BL_ATIVO").Not.Nullable();
            Map(a => a.DataRegistro, "DT_REGISTRO").Not.Nullable().Precision(4);
            Map(a => a.DataDesativacao, "DT_DESATIVACAO").Nullable().Precision(4);
            Map(a => a.Auditavel, "BL_AUDITAVEL").Not.Nullable();
        }
    }
}
