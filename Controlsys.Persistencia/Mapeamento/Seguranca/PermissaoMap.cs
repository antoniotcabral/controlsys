﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Seguranca;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Seguranca
{
    /// <summary>
    /// Representa um(a) PermissaoMap.
    /// </summary>
    public class PermissaoMap : ClassMap<Permissao>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Seguranca.PermissaoMap.
        /// </summary>
        public PermissaoMap()
        {
            Table("SISTEMA_PERMISSAO");

            Id(p => p.Codigo, "CD_PERMISSAO");

            References(p => p.Grupo, "CD_GRUPO").Not.Nullable();
            References(p => p.Acao, "CD_ACAO").Not.Nullable();

            Map(p => p.Ativo, "BL_ATIVO").Not.Nullable();
            Map(p => p.DataRegistro, "DT_REGISTRO").Not.Nullable().Precision(4);
            Map(p => p.DataDesativacao, "DT_DESATIVACAO").Nullable().Precision(4);
        }
    }
}
