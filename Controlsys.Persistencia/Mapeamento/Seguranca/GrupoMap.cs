﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Seguranca;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Seguranca
{
    /// <summary>
    /// Representa um(a) GrupoMap.
    /// </summary>
    public class GrupoMap : ClassMap<Grupo>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Seguranca.GrupoMap.
        /// </summary>
        public GrupoMap()
        {
            Table("SISTEMA_GRUPO");

            Id(g => g.Codigo, "CD_GRUPO");

            Map(g => g.Nome, "TX_GRUPO").Not.Nullable().Length(50);
            Map(g => g.Descricao, "TX_DESCRICAO").Nullable().Length(500);
            Map(g => g.Ativo, "BL_ATIVO").Not.Nullable();
            Map(g => g.DataRegistro, "DT_REGISTRO").Not.Nullable().Precision(4);
            Map(g => g.DataDesativacao, "DT_DESATIVACAO").Nullable().Precision(4);

            HasMany(g => g.GruposLeitoras).KeyColumn("CD_GRUPO").Where("DT_DESATIVACAO IS NULL");
            //HasMany(g => g.Permissoes).KeyColumn("CD_GRUPO").Where("DT_DESATIVACAO IS NULL");
            HasManyToMany(g => g.Acoes).Not.LazyLoad().Table("SISTEMA_PERMISSAO").ParentKeyColumn("CD_GRUPO").ChildKeyColumn("CD_ACAO").Where("DT_DESATIVACAO IS NULL");

            HasMany(g => g.GruposUsuarios).KeyColumn("CD_GRUPO").Where("DT_DESATIVACAO IS NULL");
        }
    }
}
