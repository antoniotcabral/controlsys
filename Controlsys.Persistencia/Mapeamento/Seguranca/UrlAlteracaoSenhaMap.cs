﻿using Controlsys.Dominio.Seguranca;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Seguranca
{
    /// <summary>
    /// Representa um(a) UrlAlteracaoSenhaMap.
    /// </summary>
    public class UrlAlteracaoSenhaMap : ClassMap<UrlAlteracaoSenha>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Seguranca.UrlAlteracaoSenhaMap.
        /// </summary>
        public UrlAlteracaoSenhaMap()
        {
            Table("URL_ALTERACAO_SENHA");

            Id(u => u.Codigo, "CD_URL_ALTERACAO_SENHA");

            References(u => u.Usuario, "CD_USUARIO").Not.Nullable();

            Map(u => u.UrlAlteracao, "TX_NOME").Not.Nullable().Length(255);
            Map(u => u.Ativo, "BL_ATIVO").Not.Nullable();
            Map(u => u.DataRegistro, "DT_REGISTRO").Not.Nullable().Precision(4);
            Map(u => u.DataDesativacao, "DT_DESATIVACAO").Precision(4);
        }
    }
}
