﻿using Controlsys.Dominio.Empresas;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Empresas
{
    /// <summary>
    /// Representa um(a) SetorCustoPedidoMap.
    /// </summary>
    public class SetorCustoPedidoMap : ClassMap<SetorCustoPedido>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Empresas.SetorCustoPedidoMap.
        /// </summary>
        public SetorCustoPedidoMap()
        {
            Table("SETOR_PEDIDO");

            Id(ec => ec.Codigo, "CD_SETOR_PEDIDO");

            References(ec => ec.Pedido, "CD_PEDIDO").Not.Nullable(); ;
            References(ec => ec.SetorCusto, "CD_SETOR").Not.Nullable(); ;

            Map(ec => ec.Ativo, "BL_ATIVO").Not.Nullable();
            Map(ec => ec.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(ec => ec.DataDesativacao, "DT_DESATIVACAO");
        }
    }
}
