﻿using Controlsys.Dominio.Empresas;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Empresas
{
    /// <summary>
    /// Representa um(a) FornecedorEmpresaMap.
    /// </summary>
    public class FornecedorEmpresaMap : ClassMap<FornecedorEmpresa>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Empresas.FornecedorEmpresaMap.
        /// </summary>
        public FornecedorEmpresaMap()
        {
            Table("EMPRESA_CONTRATANTE");

            Id(ec => ec.Codigo, "CD_EMPRESA_CONTRATANTE");

            References(ec => ec.Empresa, "CD_EMPRESA").Not.Nullable(); ;
            References(ec => ec.Fornecedor, "CD_FORNECEDOR").Not.Nullable(); ;

            Map(ec => ec.Ativo, "BL_ATIVO").Not.Nullable();
            Map(ec => ec.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(ec => ec.DataDesativacao, "DT_DESATIVACAO");
        }
    }
}
