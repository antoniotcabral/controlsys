﻿using Controlsys.Dominio.Empresas;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Empresas
{
    /// <summary>
    /// Representa um(a) PedidoCompraMap.
    /// </summary>
    public class PedidoCompraMap : ClassMap<PedidoCompra>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Empresas.PedidoCompraMap.
        /// </summary>
        public PedidoCompraMap()
        {
            Table("PEDIDO");

            Id(p => p.Codigo, "CD_PEDIDO");

            References(p => p.PedidoCompraPai, "CD_PEDIDO_PAI");
            References(p => p.Projeto, "CD_PROJETO").Not.Nullable();
            References(p => p.CNAE, "TX_CNAE");
            References(p => p.Empresa, "CD_EMPRESA");

            Map(p => p.Numero, "NU_PEDIDO").Length(50);
            Map(p => p.Nome, "TX_NOME").Length(50);
            Map(p => p.Observacao, "TX_OBSERVACAO").Length(1500);

            Map(p => p.DataRegistro, "DT_REGISTRO");
            Map(p => p.Status, "TX_STATUS");
            Map(p => p.DataStatus, "DT_STATUS");

            HasMany(p => p.Setores)
                .KeyColumn("CD_PEDIDO")
                .Where("DT_DESATIVACAO IS NULL");

            HasMany(p => p.Prazos).KeyColumn("CD_PEDIDO");

            HasMany(p => p.GestoresResponsaveis)
                .KeyColumn("CD_PEDIDO")
                .Where("DT_DESATIVACAO IS NULL");

            HasMany(p => p.SubPedidos)
                .KeyColumn("CD_PEDIDO_PAI")
                .Where("TX_STATUS <> 'Removido'");

            HasMany(p => p.ColaboradoresAlocados)
                .KeyColumn("CD_PEDIDO");
        }
    }
}
