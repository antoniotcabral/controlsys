﻿using Controlsys.Dominio.Empresas;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Empresas
{
    /// <summary>
    /// Representa um(a) AlocacaoColaboradorMap.
    /// </summary>
    public class AlocacaoColaboradorMap : ClassMap<AlocacaoColaborador>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Empresas.AlocacaoColaboradorMap.
        /// </summary>
        public AlocacaoColaboradorMap()
        {
            Table("ALOCACAO_COLABORADOR");

            Id(b => b.Codigo, "CD_ALOCACAO_COLABORADOR");

            References(b => b.Papel, "CD_PAPEL").LazyLoad(Laziness.False).Not.Nullable();
            References(b => b.PedidoCompra, "CD_PEDIDO").Not.Nullable();
            References(b => b.SetorCusto, "CD_SETOR");            

            Map(b => b.Ativo, "BL_ATIVO").Not.Nullable();
            Map(b => b.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(b => b.DataDesativacao, "DT_DESATIVACAO");
        }
    }
}
