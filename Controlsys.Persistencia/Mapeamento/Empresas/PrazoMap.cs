﻿using Controlsys.Dominio.Empresas;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Empresas
{
    /// <summary>
    /// Representa um(a) PrazoMap.
    /// </summary>
    public class PrazoMap : ClassMap<Prazo>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Empresas.PrazoMap.
        /// </summary>
        public PrazoMap()
        {
            Table("PRAZO");

            Id(p => p.Codigo, "CD_PRAZO");

            References(p => p.PedidoCompra, "CD_PEDIDO");
            Map(p => p.DataInicio, "DT_INICIO");
            Map(p => p.DataFim, "DT_FIM");
            Map(p => p.Aditivo, "BL_ADITIVO");
            Map(p => p.Observacao, "TX_OBSERVACAO");

            Map(b => b.Ativo, "BL_ATIVO").Not.Nullable();
            Map(b => b.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(b => b.DataDesativacao, "DT_DESATIVACAO");

        }
    }
}
