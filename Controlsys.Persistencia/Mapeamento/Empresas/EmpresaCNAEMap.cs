﻿using Controlsys.Dominio.Empresas;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Empresas
{
    /// <summary>
    /// Representa um(a) EmpresaCNAEMap.
    /// </summary>
    public class EmpresaCNAEMap : ClassMap<EmpresaCNAE>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Empresas.EmpresaCNAEMap.
        /// </summary>
        public EmpresaCNAEMap()
        {
            Table("EMPRESA_CNAE");

            Id(ec => ec.Codigo, "CD_EMPRESA_CNAE");

            References(ec => ec.Empresa, "CD_EMPRESA").Not.Nullable(); ;
            References(ec => ec.CNAE, "TX_CNAE").Not.Nullable(); ;

            Map(ec => ec.Ativo, "BL_ATIVO").Not.Nullable();
            Map(ec => ec.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(ec => ec.DataDesativacao, "DT_DESATIVACAO");
        }
    }
}
