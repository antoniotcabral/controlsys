﻿using Controlsys.Dominio.Empresas;
using Controlsys.Dominio.Pessoas;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Empresas
{
    /// <summary>
    /// Representa um(a) EmpresaMap.
    /// </summary>
    public class EmpresaMap : SubclassMap<Empresa>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Empresas.EmpresaMap.
        /// </summary>
        public EmpresaMap()
        {
            Table("EMPRESA");
            
            KeyColumn("CD_PESSOA");
            
            Map(e => e.CNPJ, "TX_CNPJ");
            Map(e => e.InscEstadual, "TX_IESTADUAL");
            Map(e => e.InscMunicipal, "TX_IMUNICIPAL");
            //Map(e => e.NaturezaJuridica, "TX_NATUREZA");
            Map(e => e.Ativo, "BL_ATIVO");
            Map(e => e.CalculoFte, "BL_CALCULAR_FTE");
            Map(e => e.DataDesativacao, "DT_DESATIVACAO");
            Map(e => e.ExibirRelatorioFTE, "BL_EXIBIR_REL_FTE");
            Map(e => e.BloqColabSemPedido, "BL_BLOQ_COLAB_SEM_PEDIDO");
            Map(e => e.EmpresaSAP, "CD_EMPRESA_SAP");            

            HasMany(p => p.Documentos)
                .KeyColumn("CD_EMPRESA")
                .Where("DT_DESATIVACAO IS NULL");

            HasMany(p => p.CNAEs)
               .KeyColumn("CD_EMPRESA")
               .Where("DT_DESATIVACAO IS NULL");

            HasMany(e => e.Sindicatos)
                .KeyColumn("CD_EMPRESA")
                .Where("DT_DESATIVACAO IS NULL");
        }
    }
}
