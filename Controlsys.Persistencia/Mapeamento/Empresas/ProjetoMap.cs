﻿using Controlsys.Dominio.Empresas;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Empresas
{
    /// <summary>
    /// Representa um(a) ProjetoMap.
    /// </summary>
    public class ProjetoMap : ClassMap<Projeto>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Empresas.ProjetoMap.
        /// </summary>
        public ProjetoMap()
        {
            Table("PROJETO");

            Id(p => p.Codigo, "CD_PROJETO");
            Map(p => p.Nome, "TX_PROJETO").Length(50).Not.Nullable();
            Map(p => p.Descricao, "TX_DESCRICAO").Length(250);
            Map(p => p.DataInicio, "DT_INICIO").Not.Nullable();
            Map(p => p.DataFim, "DT_FIM");

            Map(b => b.Ativo, "BL_ATIVO").Not.Nullable();
            Map(b => b.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(b => b.DataDesativacao, "DT_DESATIVACAO");

            HasMany(p => p.Areas)
                .Table("AREA_PROJETO")
                .KeyColumn("CD_PROJETO")
                .Where("DT_DESATIVACAO IS NULL");

            HasMany(p => p.Treinamentos)
                .Table("TREINAMENTO_PROJETO")
                .KeyColumn("CD_PROJETO")
                .Where("DT_DESATIVACAO IS NULL");

            HasMany(p => p.GruposTrabalho)
                .Table("GRUPO_TRAB_PROJETO")
                .KeyColumn("CD_PROJETO")
                .Where("DT_DESATIVACAO IS NULL");
        }
    }
}
