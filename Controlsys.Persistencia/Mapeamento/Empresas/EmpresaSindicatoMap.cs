﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Dominio.Empresas;
using FluentNHibernate.Mapping;

namespace Controlsys.Persistencia.Mapeamento.Empresas
{
    public class EmpresaSindicatoMap : ClassMap<EmpresaSindicato>
    {
        public EmpresaSindicatoMap()
        {
            Table("EMPRESA_SINDICATO");

            Id(es => es.Codigo, "CD_EMPRESA_SINDICATO");

            References(es => es.Empresa, "CD_EMPRESA").Not.Nullable();
            References(es => es.Sindicato, "CD_SINDICATO").Not.Nullable();

            Map(es => es.Ativo, "BL_ATIVO").Not.Nullable();
            Map(es => es.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(es => es.DataDesativacao, "DT_DESATIVACAO").Nullable();
        }
    }
}
