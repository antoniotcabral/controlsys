﻿using Controlsys.Dominio.Empresas;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Empresas
{
    /// <summary>
    /// Representa um(a) AreaProjetoMap.
    /// </summary>
    public class AreaProjetoMap : ClassMap<AreaProjeto>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Empresas.AreaProjetoMap.
        /// </summary>
        public AreaProjetoMap()
        {
            Table("AREA_PROJETO");

            Id(b => b.Codigo, "CD_AREA_PROJETO");

            References(b => b.Area, "CD_AREA").Not.Nullable();
            References(b => b.Projeto, "CD_PROJETO").Not.Nullable();

            Map(b => b.Ativo, "BL_ATIVO").Not.Nullable();
            Map(b => b.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(b => b.DataDesativacao, "DT_DESATIVACAO");

        }
    }
}
