﻿using Controlsys.Dominio.Pessoas;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Empresas
{
    /// <summary>
    /// Representa um(a) ColaboradorPedidoMap.
    /// </summary>
    public class ColaboradorPedidoMap : ClassMap<ColaboradorPedido>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Empresas.ColaboradorPedidoMap.
        /// </summary>
        public ColaboradorPedidoMap()
        {
            Table("GESTOR");

            Id(cp => cp.Codigo, "CD_GESTOR");

            References(cp => cp.Pedido, "CD_PEDIDO").Not.Nullable();
            References(cp => cp.Colaborador, "CD_COLABORADOR");

            Map(cp => cp.Ativo, "BL_ATIVO").Not.Nullable();
            Map(cp => cp.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(cp => cp.DataDesativacao, "DT_DESATIVACAO");
        }
    }
}
