﻿using Controlsys.Dominio.Empresas;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Empresas
{
    /// <summary>
    /// Representa um(a) AreaMap.
    /// </summary>
    public class AreaMap : ClassMap<Area>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Empresas.AreaMap.
        /// </summary>
        public AreaMap()
        {
            Table("AREA");

            Id(b => b.Codigo, "CD_AREA");

            Map(b => b.Nome, "TX_NOME").Not.Nullable().Length(50);
            Map(b => b.Numero, "TX_NUMERO").Length(150);
            
            Map(b => b.Ativo, "BL_ATIVO").Not.Nullable();
            Map(b => b.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(b => b.DataDesativacao, "DT_DESATIVACAO");
        }
    }
}
