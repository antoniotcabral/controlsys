﻿using Controlsys.Dominio.Empresas;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlsys.Persistencia.Mapeamento.Empresas
{
    /// <summary>
    /// Representa um(a) FornecedorMap.
    /// </summary>
    public class FornecedorMap : ClassMap<Fornecedor>
    {
        /// <summary>
        /// Construtor para Controlsys.Persistencia.Mapeamento.Empresas.FornecedorMap.
        /// </summary>
        public FornecedorMap()
        {
            Table("FORNECEDOR");

            Id(f => f.Codigo, "CD_FORNECEDOR");

            Map(f => f.RazaoSocial, "TX_RAZAO_SOCIAL");
            Map(f => f.NomeFantasia, "TX_NOME_FANTASIA");
            Map(f => f.TelefoneContato, "NU_TELEFONE");
            Map(f => f.EmailContato, "TX_EMAIL");
            Map(f => f.CNPJ, "NU_CNPJ");
            Map(f => f.NomeContato, "TX_CONTATO");

            Map(f => f.Ativo, "BL_ATIVO").Not.Nullable();
            Map(f => f.DataRegistro, "DT_REGISTRO").Not.Nullable();
            Map(f => f.DataDesativacao, "DT_DESATIVACAO");

            HasMany(p => p.Empresas)
               .KeyColumn("CD_FORNECEDOR")
               .Where("DT_DESATIVACAO IS NULL");
        }
    }
}
