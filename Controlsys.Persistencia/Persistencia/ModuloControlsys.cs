﻿using Controlsys.Dominio.Pessoas;
using Controlsys.Persistencia.Repositorio.Agendamentos;
using Controlsys.Persistencia.Repositorios.Acesso;
using Controlsys.Persistencia.Repositorios.Agendamentos;
using Controlsys.Persistencia.Repositorios.Audit;
using Controlsys.Persistencia.Repositorios.Empresas;
using Controlsys.Persistencia.Repositorios.Enderecos;
using Controlsys.Persistencia.Repositorios.Ocorrencias;
using Controlsys.Persistencia.Repositorios.Parametros;
using Controlsys.Persistencia.Repositorios.Pessoas;
using Controlsys.Persistencia.Repositorios.Relatorio;
using Controlsys.Persistencia.Repositorios.Seguranca;
using Controlsys.Persistencia.Repositorios.SolicitacoesMaterial;
using Controlsys.Repositorio.Acesso;
using Controlsys.Repositorio.Agendamentos;
using Controlsys.Repositorio.Audit;
using Controlsys.Repositorio.Empresas;
using Controlsys.Repositorio.Ocorrencias;
using Controlsys.Repositorio.Parametros;
using Controlsys.Repositorio.Relatorio;
using Controlsys.Repositorio.SolicitacoesMaterial;
using Controlsys.Repositorios.Acesso;
using Controlsys.Repositorios.Agendamentos;
using Controlsys.Repositorios.Audit;
using Controlsys.Repositorios.Empresas;
using Controlsys.Repositorios.Enderecos;
using Controlsys.Repositorios.Parametros;
using Controlsys.Repositorios.Pessoas;
using Controlsys.Repositorios.Seguranca;
using Globalsys;
using Ninject.Modules;
using Ninject.Web.Common;

namespace Controlsys.Persistencia
{
    /// <summary>
    /// Representa um(a) ModuloControlsys.
    /// </summary>
    public class ModuloControlsys : NinjectModule
    {
        /// <summary>
        /// Load.
        /// </summary>
        public override void Load()
        {
            base.Bind<IUnidadeTrabalho>().To<UnidadeTrabalho>().InRequestScope();

            base.Bind<IRepositorioPagina>().To<RepositorioPagina>();
            base.Bind<IRepositorioAcao>().To<RepositorioAcao>();
            base.Bind<IRepositorioGrupo>().To<RepositorioGrupo>();
            base.Bind<IRepositorioUsuario>().To<RepositorioUsuario>();
            base.Bind<IRepositorioAuditoria>().To<RepositorioAuditoria>();
            base.Bind<IRepositorioArea>().To<RepositorioArea>();
            base.Bind<IRepositorioFeriado>().To<RepositorioFeriado>();
            base.Bind<IRepositorioSetorCusto>().To<RepositorioSetorCusto>();
            base.Bind<IRepositorioTreinamento>().To<RepositorioTreinamento>();
            base.Bind<IRepositorioMotivo>().To<RepositorioMotivo>();
            base.Bind<IRepositorioTipoDocumento>().To<RepositorioTipoDocumento>();
            base.Bind<IRepositorioChecklistDocumento>().To<RepositorioChecklistDocumento>();
            base.Bind<IRepositorioEndereco>().To<RepositorioEndereco>();
            base.Bind<IRepositorioFornecedor>().To<RepositorioFornecedor>();
            base.Bind<IRepositorioTurno>().To<RepositorioTurno>();
            base.Bind<IRepositorioEmpresa>().To<RepositorioEmpresa>();
            base.Bind<IRepositorioParametro>().To<RepositorioParametro>();
            base.Bind<IRepositorioPessoaFisica>().To<RepositorioPessoaFisica>();
            base.Bind<IRepositorioPessoa>().To<RepositorioPessoa>();
            base.Bind<IRepositorioProjeto>().To<RepositorioProjeto>();
            base.Bind<IRepositorioDocumento>().To<RepositorioDocumento>();
            base.Bind<IRepositorioTelefone>().To<RepositorioTelefone>();
            base.Bind<IRepositorioHoraTurno>().To<RepositorioHoraTurno>();
            base.Bind<IRepositorioGrupoTrabalho>().To<RepositorioGrupoTrabalho>();
            base.Bind<IRepositorioGrupoLeitora>().To<RepositorioGrupoLeitora>();
            base.Bind<IRepositorioPermissao>().To<RepositorioPermissao>();
            base.Bind<IRepositorioGrupoUsuario>().To<RepositorioGrupoUsuario>();
            base.Bind<IRepositorioGrupoLeitoraGrupo>().To<RepositorioGrupoLeitoraGrupo>();
            base.Bind<IRepositorioPapel>().To<RepositorioPapel>();
            base.Bind<IRepositorioColaborador>().To<RepositorioColaborador>();
            base.Bind<IRepositorioUrlAlteracaoSenha>().To<RepositorioUrlAlteracaoSenha>();
            base.Bind<IRepositorioPedidoCompra>().To<RepositorioPedidoCompra>();
            base.Bind<IRepositorioVisitante>().To<RepositorioVisitante>();
            base.Bind<IRepositorioPrestadorServico>().To<RepositorioPrestadorServico>();
            base.Bind<IRepositorioLeitora>().To<RepositorioLeitora>();
            base.Bind<IRepositorioPrazo>().To<RepositorioPrazo>();
            base.Bind<IRepositorioSetorCustoPedido>().To<RepositorioSetorCustoPedido>();
            base.Bind<IRepositorio<ColaboradorPedido>>().To<Repositorio<ColaboradorPedido>>();
            base.Bind<IRepositorioAlocacaoColaborador>().To<RepositorioAlocacaoColaborador>();
            base.Bind<IRepositorioGrupoAcesso>().To<RepositorioGrupoAcesso>();
            base.Bind<IRepositorioGrupoAcessoPapel>().To<RepositorioGrupoAcessoPapel>();
            base.Bind<IRepositorioGrupoLeitoraAcesso>().To<RepositorioGrupoLeitoraAcesso>();
            base.Bind<IRepositorioModeloCracha>().To<RepositorioModeloCracha>();
            base.Bind<IRepositorioCracha>().To<RepositorioCracha>();
            base.Bind<IRepositorioCargo>().To<RepositorioCargo>();
            base.Bind<IRepositorioPessoaCritica>().To<RepositorioPessoaCritica>();
            base.Bind<IRepositorioControladora>().To<RepositorioControladora>();
            base.Bind<IRepositorioControleAcesso>().To<RepositorioControleAcesso>();
            base.Bind<IRepositorioGrupoTrabalhoColab>().To<RepositorioGrupoTrabalhoColab>();
            base.Bind<IRepositorioLogError>().To<RepositorioLogError>();
            base.Bind<IRepositorioCidade>().To<RepositorioCidade>();
            base.Bind<IRepositorioEscalaTrabalho>().To<RepositorioEscalaTrabalho>();
            base.Bind<IRepositorioSincronizaPessoaAcesso>().To<RepositorioSincronizaPessoaAcesso>();
            base.Bind<IRepositorioAcessoTemporario>().To<RepositorioAcessoTemporario>();
            base.Bind<IRepositorioAcessoTemporarioVeiculo>().To<RepositorioAcessoTemporarioVeiculo>();
            base.Bind<IRepositorioTipoAlimentacao>().To<RepositorioTipoAlimentacao>();
            base.Bind<IRepositorioRestaurante>().To<RepositorioRestaurante>();
            base.Bind<IRepositorioAcessoRestaurante>().To<RepositorioAcessoRestaurante>();
            base.Bind<IRepositorioRelatorioRestauranteCatraca>().To<RepositorioRelatorioRestauranteCatraca>();
            base.Bind<IRepositorioTipoAlimentacaoRestaurante>().To<RepositorioTipoAlimentacaoRestaurante>();
            base.Bind<IRepositorioAcessoRestauranteTipoAlimentacao>().To<RepositorioAcessoRestauranteTipoAlimentacao>();
            base.Bind<IRepositorioAgendamentoVisitante>().To<RepositorioAgendamentoVisitante>();
            base.Bind<IRepositorioPessoaAgendamento>().To<RepositorioPessoaAgendamento>();
            base.Bind<IRepositorioPapelAgendamento>().To<RepositorioPapelAgendamento>();
            base.Bind<IRepositorioRelatorioCrachaPeriodo>().To<RepositorioRelatorioCrachaPeriodo>();
            base.Bind<IRepositorioRelatorioUltimoCrachaPapel>().To<RepositorioRelatorioUltimoCrachaPapel>();
            base.Bind<IRepositorioLogControladora>().To<RepositorioLogControladora>();
            base.Bind<IRepositorioPreCadastroColaborador>().To<RepositorioPreCadastroColaborador>();
            base.Bind<IRepositorioPreCadastroPrestador>().To<RepositorioPreCadastroPrestador>();
            base.Bind<IRepositorioMidia>().To<RepositorioMidia>();
            base.Bind<IRepositorioDocumentoMidia>().To<RepositorioDocumentoMidia>();
            base.Bind<IRepositorioLocal>().To<RepositorioLocal>();
            base.Bind<IRepositorioTipoOcorrencia>().To<RepositorioTipoOcorrencia>();
            base.Bind<IRepositorioOcorrenciaSeguranca>().To<RepositorioOcorrenciaSeguranca>();
            base.Bind<IRepositorioAtendimentoReceptivo>().To<RepositorioAtendimentoReceptivo>();
            base.Bind<IRepositorioClassificacaoJuridica>().To<RepositorioClassificacaoJuridica>();
            base.Bind<IRepositorioTipoValidade>().To<RepositorioTipoValidade>();
            base.Bind<IRepositorioFabricanteVeiculo>().To<RepositorioFabricanteVeiculo>();
            base.Bind<IRepositorioModeloVeiculo>().To<RepositorioModeloVeiculo>();
            base.Bind<IRepositorioModeloCredencial>().To<RepositorioModeloCredencial>();
            base.Bind<IRepositorioInfracao>().To<RepositorioInfracao>();
            base.Bind<IRepositorioPenalidade>().To<RepositorioPenalidade>();
            base.Bind<IRepositorioOcorrenciaTransito>().To<RepositorioOcorrenciaTransito>();
            base.Bind<IRepositorioTreinamentoOcorrenciaTransito>().To<RepositorioTreinamentoOcorrenciaTransito>();
            base.Bind<IRepositorioCredencialVeiculo>().To<RepositorioCredencialVeiculo>();
            base.Bind<IRepositorioVeiculo>().To<RepositorioVeiculo>();
            base.Bind<IRepositorioVeiculoCredencialVeiculo>().To<RepositorioVeiculoCredencialVeiculo>();
            base.Bind<IRepositorioSolicitacaoMaterial>().To<RepositorioSolicitacaoMaterial>();
            base.Bind<IRepositorioItemSolicitacaoMaterial>().To<RepositorioItemSolicitacaoMaterial>();
            base.Bind<IRepositorioSolicitacaoMaterialMidia>().To<RepositorioSolicitacaoMaterialMidia>();
            base.Bind<IRepositorioStatusSolicitacaoMaterial>().To<RepositorioStatusSolicitacaoMaterial>();
            base.Bind<IRepositorioOcorrenciaSegurancaAgente>().To<RepositorioOcorrenciaSegurancaAgente>();
            base.Bind<IRepositorioVeiculoMidia>().To<RepositorioVeiculoMidia>();
            base.Bind<IRepositorioOcorrenciaSegurancaMidia>().To<RepositorioOcorrenciaSegurancaMidia>();
            base.Bind<IRepositorioPessoaCriticaMidia>().To<RepositorioPessoaCriticaMidia>();
            base.Bind<IRepositorioSindicato>().To<RepositorioSindicato>();
            base.Bind<IRepositorioEmpresaSindicato>().To<RepositorioEmpresaSindicato>();
            base.Bind<IRepositorioOcorrenciaSegurancaVeiculo>().To<RepositorioOcorrenciaSegurancaVeiculo>();
            base.Bind<IRepositorio<EmpregadoSAP>>().To<Repositorio<EmpregadoSAP>>();
            base.Bind<IRepositorio<PapelLog>>().To<Repositorio<PapelLog>>();
            base.Bind<IRepositorioCamera>().To<RepositorioCamera>();            
            base.Bind<IRepositorioAcessoVeiculo>().To<RepositorioAcessoVeiculo>();
            base.Bind<IRepositorioCameraImagem>().To<RepositorioCameraImagem>();
            base.Bind<IRepositorioLogAvigilon>().To<RepositorioLogAvigilon>();
            
            base.Bind<IRepositorioIntegracaoSAP>().To<RepositorioIntegracaoSAP>();
            base.Bind<IRepositorioIntegracaoSAPAcesso>().To<RepositorioIntegracaoSAPAcesso>();
            base.Bind<IRepositorioLogErrorIntegracaoSAP>().To<RepositorioLogErrorIntegracaoSAP>();
            base.Bind<IRepositorioColaboradorAtualizadoIncluido>().To<RepositorioColaboradorAtualizadoIncluido>();

        }
    }
}
