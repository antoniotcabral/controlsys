﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Globalsys;
using NHibernate;
using System.Configuration;

namespace Controlsys.Persistencia
{
    /// <summary>
    /// Representa um(a) UnidadeTrabalho.
    /// </summary>
    public class UnidadeTrabalho : IUnidadeTrabalho
    {
        /// <summary>
        /// Gets/Sets valor para dateStart.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) dateStart.
        /// </value>
        private DateTime dateStart { get; set; }

        /// <summary>
        /// Gets/Sets valor para Sessao.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Sessao.
        /// </value>
        public ISession Sessao { get; set; }

        /// <summary>
        /// Gets/Sets valor para transaction.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) transaction.
        /// </value>
        private ITransaction transaction { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Persistencia.UnidadeTrabalho.
        /// </summary>
        public UnidadeTrabalho()
        {
            Sessao = SessionFactory.Instancia.ObterSessao();

            dateStart = DateTime.Now;
        }

        /// <summary>
        /// Begin transaction.
        /// </summary>
        public void BeginTransaction()
        {
            transaction = Sessao.BeginTransaction();
        }

        /// <summary>
        /// Commit.
        /// </summary>
        public void Commit()
        {
            transaction.Commit();
        }

        /// <summary>
        /// Rollback.
        /// </summary>
        public void Rollback()
        {
            transaction.Rollback();
        }

        /// <summary>
        /// Dispose.
        /// </summary>
        public void Dispose()
        {
            transaction.Dispose();
        }

        /// <summary>
        /// Execute SQL.
        /// </summary>
        ///
        /// <typeparam name="T">
        /// Generic type parameter.
        /// </typeparam>
        /// <param name="sql">
        /// O(a) SQL.
        /// </param>
        /// <param name="parameters">
        /// Options for controlling the operation.
        /// </param>
        ///
        /// <returns>
        /// Um(a) IList&lt;T&gt;
        /// </returns>
        public IList<T> ExecuteSql<T>(string sql, Dictionary<string, object> parameters)
        {
            ISQLQuery sqlQuery = Sessao.CreateSQLQuery(sql);

            foreach (var item in parameters)
                sqlQuery.SetParameter(item.Key, item.Value, NHibernateUtil.GetSerializable(item.Value.GetType()));

            sqlQuery.SetResultTransformer(NHibernate.Transform.Transformers.AliasToBean<T>());
            sqlQuery.SetTimeout(300);
            return sqlQuery.List<T>();
        }

        /// <summary>
        /// Execute SQL.
        /// </summary>
        ///
        /// <typeparam name="T">
        /// Generic type parameter.
        /// </typeparam>
        /// <param name="sql">
        /// O(a) SQL.
        /// </param>
        /// <param name="parameters">
        /// Options for controlling the operation.
        /// </param>
        ///
        /// <returns>
        /// Um(a) IList&lt;T&gt;
        /// </returns>
        public IList<T> ExecuteSqlHHT<T>(string sql, Dictionary<string, object> parameters)
        {
            ISQLQuery sqlQuery = Sessao.CreateSQLQuery(sql);

            foreach (var item in parameters)
            {
                if (item.Value is DateTime)
                    sqlQuery.SetDateTime(item.Key, Convert.ToDateTime(item.Value));
                else if (item.Value is string)
                    sqlQuery.SetString(item.Key, item.Value as string);
                else if (item.Value is int)
                    sqlQuery.SetInt32(item.Key, Convert.ToInt32(item.Value));
                else
                    sqlQuery.SetParameter(item.Key, item.Value, NHibernateUtil.GetSerializable(item.Value.GetType()));
            }

            sqlQuery.SetResultTransformer(NHibernate.Transform.Transformers.AliasToBean<T>());
            sqlQuery.SetTimeout(3000);
            return sqlQuery.List<T>();
        }

        /// <summary>
        /// Executa query 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql">Query</param>
        /// <param name="parameters">Parametros usados na query</param>
        /// <param name="listaEntities">Entidades Nhibernate usadas para relacionar com as tabelas/colunas do retorno da query</param>
        /// <returns></returns>
        public IList<T> ExecuteSql<T>(string sql, Dictionary<string, object> parameters, List<Type> listaEntities)
        {
            ISQLQuery sqlQuery = Sessao.CreateSQLQuery(sql);

            foreach (var item in parameters)
                sqlQuery.SetParameter(item.Key, item.Value);

            foreach (Type entity in listaEntities)
            {
                sqlQuery.AddEntity(entity);
            }

            return sqlQuery.List<T>();
        }

        /// <summary>
        /// Execute procedure.
        /// </summary>
        ///
        /// <param name="procedure">
        /// O(a) procedure.
        /// </param>
        /// <param name="parameters">
        /// Options for controlling the operation.
        /// </param>
        public void ExecuteProcedure(string procedure, Dictionary<string, object> parameters = null)
        {
            ISQLQuery sqlQuery = Sessao.CreateSQLQuery(procedure);
            if (parameters != null)
                foreach (var item in parameters)
                    sqlQuery.SetParameter(item.Key, item.Value);

            string SQLConnectionString = ConfigurationManager.AppSettings["timeoutNHibernate"];
            if (SQLConnectionString != null && Convert.ToInt32(SQLConnectionString) > 0)
            {
                sqlQuery.SetTimeout(Convert.ToInt32(SQLConnectionString));
            }            

            sqlQuery.ExecuteUpdate();
        }

    }
}
