﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Globalsys;
using NHibernate.Linq;

namespace Controlsys.Persistencia
{
    /// <summary>
    /// Representa um(a) Repositorio.
    /// </summary>
    ///
    /// <typeparam name="T">
    /// Generic type parameter.
    /// </typeparam>
    public class Repositorio<T> : IRepositorio<T>
    {
        /// <summary>
        /// Gets/Sets valor para UnidadeTrabalho.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) UnidadeTrabalho.
        /// </value>
        public UnidadeTrabalho UnidadeTrabalho { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Persistencia.Repositorio&lt;T&gt;.
        /// </summary>
        ///
        /// <param name="unidadeTrabalho">
        /// O(a) unidade trabalho.
        /// </param>
        public Repositorio(IUnidadeTrabalho unidadeTrabalho)
        {
            UnidadeTrabalho = (UnidadeTrabalho)unidadeTrabalho;
        }

        /// <summary>
        /// Obter por identifier.
        /// </summary>
        ///
        /// <param name="id">
        /// O(a) identifier.
        /// </param>
        ///
        /// <returns>
        /// Um(a) T.
        /// </returns>
        public virtual T ObterPorId(object id)
        {
            return UnidadeTrabalho.Sessao.Get<T>(id);
        }

        /// <summary>
        /// Retorna todos os objetos do tipo Repositorio&lt;.
        /// </summary>
        ///
        /// <returns>
        /// Detalhes do tipo de objeto ligado ao controller no formato JSON.
        /// </returns>
        public virtual IQueryable<T> ObterTodos()
        {
            return UnidadeTrabalho.Sessao.Query<T>();
        }

        /// <summary>
        /// Salvar.
        /// </summary>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        public virtual void Salvar(T entidade)
        {
            UnidadeTrabalho.Sessao.Save(entidade);
        }

        /// <summary>
        /// Atualizar.
        /// </summary>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        public virtual void Atualizar(T entidade)
        {
            UnidadeTrabalho.Sessao.Update(entidade);
        }

        /// <summary>
        /// Remover.
        /// </summary>
        ///
        /// <param name="id">
        /// O(a) identifier.
        /// </param>
        public virtual void Remover(int id)
        {
            T entidade = ObterPorId(id);

            Remover(entidade);
        }

        /// <summary>
        /// Remover.
        /// </summary>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        public virtual void Remover(T entidade)
        {
            UnidadeTrabalho.Sessao.Delete(entidade);
        }

        /// <summary>
        /// Obter por identifier.
        /// </summary>
        ///
        /// <param name="entidade">
        /// O(a) entidade.
        /// </param>
        /// <param name="propModificadas">
        /// O(a) property modificadas.
        /// </param>
        ///
        /// <returns>
        /// Um(a) T.
        /// </returns>
        public T ObterPorId(T entidade, string[] propModificadas)
        {
            object codigo = null;

            codigo = entidade.GetType().GetProperty("Codigo").GetValue(entidade);

            T entidadeAux = ObterPorId(codigo);

            if (propModificadas != null)
            {
                foreach (string prop in propModificadas)
                {
                    PropertyInfo property = entidade.GetType().GetProperty(prop);

                    if (property != null)
                        property.SetValue(entidadeAux, property.GetValue(entidade));
                }
            }
            return entidadeAux;
        }
    }
}
