﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controlsys.Persistencia.Mapeamento.Seguranca;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Tool.hbm2ddl;

namespace Controlsys.Persistencia
{
    /// <summary>
    /// Representa um(a) SessionFactory.
    /// </summary>
    public class SessionFactory
    {
        /// <summary>
        /// O(a) instancia.
        /// </summary>
        private static SessionFactory fInstancia;

        /// <summary>
        /// Gets valor para Instancia.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) Instancia.
        /// </value>
        public static SessionFactory Instancia
        {
            get
            {
                if (fInstancia == null)
                    fInstancia = new SessionFactory();

                return fInstancia;
            }
        }

        /// <summary>
        /// Gets/Sets valor para ISessionFactory.
        /// </summary>
        ///
        /// <value>
        /// Retorna o(a) ISessionFactory.
        /// </value>
        private ISessionFactory ISessionFactory { get; set; }

        /// <summary>
        /// Construtor para Controlsys.Persistencia.SessionFactory.
        /// </summary>
        public SessionFactory()
        {
            FluentConfiguration config = obterConfiguracaoFluent();
            this.ISessionFactory = config.BuildSessionFactory();
        }

        /// <summary>
        /// Obter configuracao fluent.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) FluentConfiguration.
        /// </returns>
        private static FluentConfiguration obterConfiguracaoFluent()
        {
            return Fluently.Configure()
                           .Database(MsSqlConfiguration
                                        .MsSql2008
                                        .ShowSql()
                                        .ConnectionString(c => c.FromConnectionStringWithKey("conexao"))
                                        .Driver("NHibernate.Driver.Sql2008ClientDriver")
                                        )
                           .ExposeConfiguration(cfg => cfg.SetProperty("command_timeout", "900"))
                           .Mappings(m => m.FluentMappings.AddFromAssemblyOf<UsuarioMap>());
        }

        /// <summary>
        /// Gerar banco.
        /// </summary>
        public static void GerarBanco()
        {
            obterConfiguracaoFluent().ExposeConfiguration(cfg => new SchemaUpdate(cfg).Execute(true, true)).BuildSessionFactory();            
        }

        /// <summary>
        /// Obter sessao.
        /// </summary>
        ///
        /// <returns>
        /// Um(a) ISession.
        /// </returns>
        public ISession ObterSessao()
        {
            return this.ISessionFactory.OpenSession();
        }
    }
}
